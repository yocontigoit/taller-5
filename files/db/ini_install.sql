-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Sep 06, 2016 at 10:56 PM
-- Server version: 5.6.30
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `appsdutj_pro`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE IF NOT EXISTS `activity_log` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_user` int(6) NOT NULL,
  `activity_user_type` tinyint(4) DEFAULT NULL,
  `activity_module` varchar(45) DEFAULT NULL,
  `activity_details` varchar(120) DEFAULT NULL,
  `activity_date` datetime NOT NULL,
  `activity_module_id` int(6) DEFAULT NULL,
  `activity_status` varchar(20) DEFAULT NULL,
  `activity_event` varchar(120) DEFAULT NULL,
  `activity_icon` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=788 ;

--
-- Table structure for table `attachments`
--

DROP TABLE IF EXISTS `attachments`;
CREATE TABLE IF NOT EXISTS `attachments` (
  `attachment_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `message_id` int(11) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  `path` text NOT NULL,
  `file_name` text NOT NULL,
  `title` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `file_type` varchar(11) NOT NULL,
  `file_size` int(11) NOT NULL,
  `is_image` int(11) NOT NULL,
  `image_width` int(11) NOT NULL,
  `image_height` int(11) NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  `uploader_type` int(2) NOT NULL,
  `upload_date` datetime NOT NULL,
  PRIMARY KEY (`attachment_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;


--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`) VALUES
(1, 'Websites IT & Software'),
(2, 'Mobile'),
(3, 'Writing'),
(4, 'Design'),
(5, 'Data Entry'),
(6, 'Product Sourcing & Manufacturing'),
(7, 'Sales & Marketing'),
(8, 'Business, Accounting & Legal'),
(9, 'Local Jobs & Services');

-- --------------------------------------------------------

--
-- Table structure for table `check_lists`
--

DROP TABLE IF EXISTS `check_lists`;
CREATE TABLE IF NOT EXISTS `check_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `finished` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `list_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

-- --------------------------------------------------------

--
-- Table structure for table `classification`
--

DROP TABLE IF EXISTS `classification`;
CREATE TABLE IF NOT EXISTS `classification` (
  `class_id` int(4) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(120) NOT NULL,
  `is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `classification`
--

INSERT INTO `classification` (`class_id`, `class_name`, `is_deleted`) VALUES
(1, 'IT Department', 0),
(2, 'Meals and Entertainment', 0),
(3, 'Travel', 0),
(4, 'Insurance', 0);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `client_id` int(6) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(45) NOT NULL,
  `client_code` varchar(45) NOT NULL,
  `client_type` int(2) DEFAULT NULL,
  `client_currency` int(6) NOT NULL,
  `client_vat_no` varchar(30) NOT NULL,
  `client_status` int(2) DEFAULT NULL,
  `client_address_1` varchar(40) DEFAULT NULL,
  `client_address_2` varchar(40) DEFAULT NULL,
  `client_address_3` varchar(40) DEFAULT NULL,
  `client_phone_no` varchar(11) NOT NULL,
  `client_email` varchar(45) NOT NULL,
  `client_website` varchar(60) NOT NULL,
  `client_city` varchar(40) DEFAULT NULL,
  `client_state` varchar(40) DEFAULT NULL,
  `client_zip` varchar(10) DEFAULT NULL,
  `client_country` int(6) DEFAULT NULL,
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`client_id`),
  KEY `client_fk1` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;


--
-- Table structure for table `client_status`
--

DROP TABLE IF EXISTS `client_status`;
CREATE TABLE IF NOT EXISTS `client_status` (
  `status_id` int(6) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(60) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `client_status`
--

INSERT INTO `client_status` (`status_id`, `status_name`) VALUES
(1, 'Active'),
(2, 'In Active'),
(3, 'Declined');

-- --------------------------------------------------------

--
-- Table structure for table `client_type`
--

DROP TABLE IF EXISTS `client_type`;
CREATE TABLE IF NOT EXISTS `client_type` (
  `type_id` int(6) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(40) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `client_type`
--

INSERT INTO `client_type` (`type_id`, `type_name`) VALUES
(1, 'Software Company'),
(2, 'IT Consultation Company'),
(3, 'Law Company'),
(4, 'Freelancer');

-- --------------------------------------------------------

--
-- Table structure for table `client_user`
--

DROP TABLE IF EXISTS `client_user`;
CREATE TABLE IF NOT EXISTS `client_user` (
  `cl_user_id` int(6) NOT NULL AUTO_INCREMENT,
  `client_id` int(6) NOT NULL,
  `role_id` int(2) NOT NULL,
  `cl_user_name` varchar(30) DEFAULT NULL,
  `cl_first_name` varchar(60) NOT NULL,
  `cl_user_surname` varchar(30) DEFAULT NULL,
  `cl_user_address_1` varchar(45) DEFAULT NULL,
  `cl_user_address_2` varchar(45) DEFAULT NULL,
  `cl_user_address_3` varchar(45) DEFAULT NULL,
  `cl_user_job_title` varchar(40) DEFAULT NULL,
  `cl_user_email` varchar(40) DEFAULT NULL,
  `cl_user_avatar` varchar(80) NOT NULL,
  `cl_user_primary` tinyint(1) DEFAULT NULL,
  `cl_user_password` varchar(255) DEFAULT NULL,
  `cl_user_phone` varchar(15) DEFAULT NULL,
  `cl_user_type` tinyint(4) DEFAULT '0',
  `cl_user_active` tinyint(4) DEFAULT '2',
  `cl_user_salt` varchar(45) DEFAULT NULL,
  `ip_address` varchar(60) NOT NULL,
  `created_on` datetime NOT NULL,
  `cl_last_login` datetime NOT NULL,
  `forgotten_password_code` varchar(120) NOT NULL,
  `forgotten_password_time` int(12) NOT NULL,
  `remember_code` varchar(45) DEFAULT NULL,
  `cl_user_country` varchar(45) DEFAULT NULL,
  `cl_user_city` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cl_user_id`),
  KEY `client_user_fk1` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;


--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
CREATE TABLE IF NOT EXISTS `company` (
  `company_id` int(6) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Table structure for table `costs`
--

DROP TABLE IF EXISTS `costs`;
CREATE TABLE IF NOT EXISTS `costs` (
  `cost_id` int(6) NOT NULL AUTO_INCREMENT,
  `project_id` int(6) NOT NULL,
  `description` varchar(60) NOT NULL,
  `classification` varchar(60) NOT NULL,
  `date_entry` date NOT NULL,
  `budget_amt` varchar(45) DEFAULT NULL,
  `receipt` varchar(120) NOT NULL,
  `estimate_cost` decimal(10,2) NOT NULL,
  `real_cost` decimal(10,2) NOT NULL,
  `cost_type` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cost_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;


--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `id_country` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_zone` int(10) unsigned NOT NULL,
  `country_name` varchar(64) NOT NULL,
  `id_currency` int(10) unsigned NOT NULL DEFAULT '0',
  `iso_code` varchar(3) NOT NULL,
  `call_prefix` int(10) NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `contains_states` tinyint(1) NOT NULL DEFAULT '0',
  `need_identification_number` tinyint(1) NOT NULL DEFAULT '0',
  `need_zip_code` tinyint(1) NOT NULL DEFAULT '1',
  `zip_code_format` varchar(12) NOT NULL DEFAULT '',
  `display_tax_label` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_country`),
  KEY `country_` (`id_zone`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=245 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id_country`, `id_zone`, `country_name`, `id_currency`, `iso_code`, `call_prefix`, `active`, `contains_states`, `need_identification_number`, `need_zip_code`, `zip_code_format`, `display_tax_label`) VALUES
(1, 1, 'Germany', 0, 'DE', 49, 0, 0, 0, 1, 'NNNNN', 1),
(2, 1, 'Austria', 0, 'AT', 43, 0, 0, 0, 1, 'NNNN', 1),
(3, 1, 'Belgium', 0, 'BE', 32, 0, 0, 0, 1, 'NNNN', 1),
(4, 2, 'Canada', 0, 'CA', 1, 0, 1, 0, 1, 'LNL NLN', 0),
(5, 3, 'China', 0, 'CN', 86, 0, 0, 0, 1, 'NNNNNN', 1),
(6, 1, 'Spain', 0, 'ES', 34, 0, 0, 1, 1, 'NNNNN', 1),
(7, 1, 'Finland', 0, 'FI', 358, 0, 0, 0, 1, 'NNNNN', 1),
(8, 1, 'France', 0, 'FR', 33, 0, 0, 0, 1, 'NNNNN', 1),
(9, 1, 'Greece', 0, 'GR', 30, 0, 0, 0, 1, 'NNNNN', 1),
(10, 1, 'Italy', 0, 'IT', 39, 0, 1, 0, 1, 'NNNNN', 1),
(11, 3, 'Japan', 0, 'JP', 81, 0, 1, 0, 1, 'NNN-NNNN', 1),
(12, 1, 'Luxemburg', 0, 'LU', 352, 0, 0, 0, 1, 'NNNN', 1),
(13, 1, 'Netherlands', 0, 'NL', 31, 0, 0, 0, 1, 'NNNN LL', 1),
(14, 1, 'Poland', 0, 'PL', 48, 0, 0, 0, 1, 'NN-NNN', 1),
(15, 1, 'Portugal', 0, 'PT', 351, 0, 0, 0, 1, 'NNNN-NNN', 1),
(16, 1, 'Czech Republic', 0, 'CZ', 420, 0, 0, 0, 1, 'NNN NN', 1),
(17, 1, 'United Kingdom', 0, 'GB', 44, 0, 0, 0, 1, '', 1),
(18, 1, 'Sweden', 0, 'SE', 46, 0, 0, 0, 1, 'NNN NN', 1),
(19, 7, 'Switzerland', 0, 'CH', 41, 0, 0, 0, 1, 'NNNN', 1),
(20, 1, 'Denmark', 0, 'DK', 45, 0, 0, 0, 1, 'NNNN', 1),
(21, 2, 'United States', 0, 'US', 1, 0, 1, 0, 1, 'NNNNN', 0),
(22, 3, 'HongKong', 0, 'HK', 852, 0, 0, 0, 0, '', 1),
(23, 7, 'Norway', 0, 'NO', 47, 0, 0, 0, 1, 'NNNN', 1),
(24, 5, 'Australia', 0, 'AU', 61, 0, 0, 0, 1, 'NNNN', 1),
(25, 3, 'Singapore', 0, 'SG', 65, 0, 0, 0, 1, 'NNNNNN', 1),
(26, 1, 'Ireland', 0, 'IE', 353, 0, 0, 0, 0, '', 1),
(27, 5, 'New Zealand', 0, 'NZ', 64, 0, 0, 0, 1, 'NNNN', 1),
(28, 3, 'South Korea', 0, 'KR', 82, 0, 0, 0, 1, 'NNN-NNN', 1),
(29, 3, 'Israel', 0, 'IL', 972, 0, 0, 0, 1, 'NNNNNNN', 1),
(30, 4, 'South Africa', 0, 'ZA', 27, 1, 0, 0, 1, 'NNNN', 1),
(31, 4, 'Nigeria', 0, 'NG', 234, 0, 0, 0, 1, '', 1),
(32, 4, 'Ivory Coast', 0, 'CI', 225, 0, 0, 0, 1, '', 1),
(33, 4, 'Togo', 0, 'TG', 228, 0, 0, 0, 1, '', 1),
(34, 6, 'Bolivia', 0, 'BO', 591, 0, 0, 0, 1, '', 1),
(35, 4, 'Mauritius', 0, 'MU', 230, 0, 0, 0, 1, '', 1),
(36, 1, 'Romania', 0, 'RO', 40, 0, 0, 0, 1, 'NNNNNN', 1),
(37, 1, 'Slovakia', 0, 'SK', 421, 0, 0, 0, 1, 'NNN NN', 1),
(38, 4, 'Algeria', 0, 'DZ', 213, 0, 0, 0, 1, 'NNNNN', 1),
(39, 2, 'American Samoa', 0, 'AS', 0, 0, 0, 0, 1, '', 1),
(40, 7, 'Andorra', 0, 'AD', 376, 0, 0, 0, 1, 'CNNN', 1),
(41, 4, 'Angola', 0, 'AO', 244, 0, 0, 0, 0, '', 1),
(42, 8, 'Anguilla', 0, 'AI', 0, 0, 0, 0, 1, '', 1),
(43, 2, 'Antigua and Barbuda', 0, 'AG', 0, 0, 0, 0, 1, '', 1),
(44, 6, 'Argentina', 0, 'AR', 54, 0, 1, 0, 1, 'LNNNN', 1),
(45, 3, 'Armenia', 0, 'AM', 374, 0, 0, 0, 1, 'NNNN', 1),
(46, 8, 'Aruba', 0, 'AW', 297, 0, 0, 0, 1, '', 1),
(47, 3, 'Azerbaijan', 0, 'AZ', 994, 0, 0, 0, 1, 'CNNNN', 1),
(48, 2, 'Bahamas', 0, 'BS', 0, 0, 0, 0, 1, '', 1),
(49, 3, 'Bahrain', 0, 'BH', 973, 0, 0, 0, 1, '', 1),
(50, 3, 'Bangladesh', 0, 'BD', 880, 0, 0, 0, 1, 'NNNN', 1),
(51, 2, 'Barbados', 0, 'BB', 0, 0, 0, 0, 1, 'CNNNNN', 1),
(52, 7, 'Belarus', 0, 'BY', 0, 0, 0, 0, 1, 'NNNNNN', 1),
(53, 8, 'Belize', 0, 'BZ', 501, 0, 0, 0, 0, '', 1),
(54, 4, 'Benin', 0, 'BJ', 229, 0, 0, 0, 0, '', 1),
(55, 2, 'Bermuda', 0, 'BM', 0, 0, 0, 0, 1, '', 1),
(56, 3, 'Bhutan', 0, 'BT', 975, 0, 0, 0, 1, '', 1),
(57, 4, 'Botswana', 0, 'BW', 267, 0, 0, 0, 1, '', 1),
(58, 6, 'Brazil', 0, 'BR', 55, 0, 0, 0, 1, 'NNNNN-NNN', 1),
(59, 3, 'Brunei', 0, 'BN', 673, 0, 0, 0, 1, 'LLNNNN', 1),
(60, 4, 'Burkina Faso', 0, 'BF', 226, 0, 0, 0, 1, '', 1),
(61, 3, 'Burma (Myanmar)', 0, 'MM', 95, 0, 0, 0, 1, '', 1),
(62, 4, 'Burundi', 0, 'BI', 257, 0, 0, 0, 1, '', 1),
(63, 3, 'Cambodia', 0, 'KH', 855, 0, 0, 0, 1, 'NNNNN', 1),
(64, 4, 'Cameroon', 0, 'CM', 237, 0, 0, 0, 1, '', 1),
(65, 4, 'Cape Verde', 0, 'CV', 238, 0, 0, 0, 1, 'NNNN', 1),
(66, 4, 'Central African Republic', 0, 'CF', 236, 0, 0, 0, 1, '', 1),
(67, 4, 'Chad', 0, 'TD', 235, 0, 0, 0, 1, '', 1),
(68, 6, 'Chile', 0, 'CL', 56, 0, 0, 0, 1, 'NNN-NNNN', 1),
(69, 6, 'Colombia', 0, 'CO', 57, 0, 0, 0, 1, 'NNNNNN', 1),
(70, 4, 'Comoros', 0, 'KM', 269, 0, 0, 0, 1, '', 1),
(71, 4, 'Congo, Dem. Republic', 0, 'CD', 242, 0, 0, 0, 1, '', 1),
(72, 4, 'Congo, Republic', 0, 'CG', 243, 0, 0, 0, 1, '', 1),
(73, 8, 'Costa Rica', 0, 'CR', 506, 0, 0, 0, 1, 'NNNNN', 1),
(74, 7, 'Croatia', 0, 'HR', 385, 0, 0, 0, 1, 'NNNNN', 1),
(75, 8, 'Cuba', 0, 'CU', 53, 0, 0, 0, 1, '', 1),
(76, 1, 'Cyprus', 0, 'CY', 357, 0, 0, 0, 1, 'NNNN', 1),
(77, 4, 'Djibouti', 0, 'DJ', 253, 0, 0, 0, 1, '', 1),
(78, 8, 'Dominica', 0, 'DM', 0, 0, 0, 0, 1, '', 1),
(79, 8, 'Dominican Republic', 0, 'DO', 0, 0, 0, 0, 1, '', 1),
(80, 3, 'East Timor', 0, 'TL', 670, 0, 0, 0, 1, '', 1),
(81, 6, 'Ecuador', 0, 'EC', 593, 0, 0, 0, 1, 'CNNNNNN', 1),
(82, 4, 'Egypt', 0, 'EG', 20, 0, 0, 0, 0, '', 1),
(83, 8, 'El Salvador', 0, 'SV', 503, 0, 0, 0, 1, '', 1),
(84, 4, 'Equatorial Guinea', 0, 'GQ', 240, 0, 0, 0, 1, '', 1),
(85, 4, 'Eritrea', 0, 'ER', 291, 0, 0, 0, 1, '', 1),
(86, 1, 'Estonia', 0, 'EE', 372, 0, 0, 0, 1, 'NNNNN', 1),
(87, 4, 'Ethiopia', 0, 'ET', 251, 0, 0, 0, 1, '', 1),
(88, 8, 'Falkland Islands', 0, 'FK', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(89, 7, 'Faroe Islands', 0, 'FO', 298, 0, 0, 0, 1, '', 1),
(90, 5, 'Fiji', 0, 'FJ', 679, 0, 0, 0, 1, '', 1),
(91, 4, 'Gabon', 0, 'GA', 241, 0, 0, 0, 1, '', 1),
(92, 4, 'Gambia', 0, 'GM', 220, 0, 0, 0, 1, '', 1),
(93, 3, 'Georgia', 0, 'GE', 995, 0, 0, 0, 1, 'NNNN', 1),
(94, 4, 'Ghana', 0, 'GH', 233, 0, 0, 0, 1, '', 1),
(95, 8, 'Grenada', 0, 'GD', 0, 0, 0, 0, 1, '', 1),
(96, 7, 'Greenland', 0, 'GL', 299, 0, 0, 0, 1, '', 1),
(97, 7, 'Gibraltar', 0, 'GI', 350, 0, 0, 0, 1, '', 1),
(98, 8, 'Guadeloupe', 0, 'GP', 590, 0, 0, 0, 1, '', 1),
(99, 5, 'Guam', 0, 'GU', 0, 0, 0, 0, 1, '', 1),
(100, 8, 'Guatemala', 0, 'GT', 502, 0, 0, 0, 1, '', 1),
(101, 7, 'Guernsey', 0, 'GG', 0, 0, 0, 0, 1, 'LLN NLL', 1),
(102, 4, 'Guinea', 0, 'GN', 224, 0, 0, 0, 1, '', 1),
(103, 4, 'Guinea-Bissau', 0, 'GW', 245, 0, 0, 0, 1, '', 1),
(104, 6, 'Guyana', 0, 'GY', 592, 0, 0, 0, 1, '', 1),
(105, 8, 'Haiti', 0, 'HT', 509, 0, 0, 0, 1, '', 1),
(106, 5, 'Heard Island and McDonald Islands', 0, 'HM', 0, 0, 0, 0, 1, '', 1),
(107, 7, 'Vatican City State', 0, 'VA', 379, 0, 0, 0, 1, 'NNNNN', 1),
(108, 8, 'Honduras', 0, 'HN', 504, 0, 0, 0, 1, '', 1),
(109, 7, 'Iceland', 0, 'IS', 354, 0, 0, 0, 1, 'NNN', 1),
(110, 3, 'India', 0, 'IN', 91, 0, 0, 0, 1, 'NNN NNN', 1),
(111, 3, 'Indonesia', 0, 'ID', 62, 0, 1, 0, 1, 'NNNNN', 1),
(112, 3, 'Iran', 0, 'IR', 98, 0, 0, 0, 1, 'NNNNN-NNNNN', 1),
(113, 3, 'Iraq', 0, 'IQ', 964, 0, 0, 0, 1, 'NNNNN', 1),
(114, 7, 'Man Island', 0, 'IM', 0, 0, 0, 0, 1, 'CN NLL', 1),
(115, 8, 'Jamaica', 0, 'JM', 0, 0, 0, 0, 1, '', 1),
(116, 7, 'Jersey', 0, 'JE', 0, 0, 0, 0, 1, 'CN NLL', 1),
(117, 3, 'Jordan', 0, 'JO', 962, 0, 0, 0, 1, '', 1),
(118, 3, 'Kazakhstan', 0, 'KZ', 7, 0, 0, 0, 1, 'NNNNNN', 1),
(119, 4, 'Kenya', 0, 'KE', 254, 0, 0, 0, 1, '', 1),
(120, 5, 'Kiribati', 0, 'KI', 686, 0, 0, 0, 1, '', 1),
(121, 3, 'Korea, Dem. Republic of', 0, 'KP', 850, 0, 0, 0, 1, '', 1),
(122, 3, 'Kuwait', 0, 'KW', 965, 0, 0, 0, 1, '', 1),
(123, 3, 'Kyrgyzstan', 0, 'KG', 996, 0, 0, 0, 1, '', 1),
(124, 3, 'Laos', 0, 'LA', 856, 0, 0, 0, 1, '', 1),
(125, 1, 'Latvia', 0, 'LV', 371, 0, 0, 0, 1, 'C-NNNN', 1),
(126, 3, 'Lebanon', 0, 'LB', 961, 0, 0, 0, 1, '', 1),
(127, 4, 'Lesotho', 0, 'LS', 266, 0, 0, 0, 1, '', 1),
(128, 4, 'Liberia', 0, 'LR', 231, 0, 0, 0, 1, '', 1),
(129, 4, 'Libya', 0, 'LY', 218, 0, 0, 0, 1, '', 1),
(130, 1, 'Liechtenstein', 0, 'LI', 423, 0, 0, 0, 1, 'NNNN', 1),
(131, 1, 'Lithuania', 0, 'LT', 370, 0, 0, 0, 1, 'NNNNN', 1),
(132, 3, 'Macau', 0, 'MO', 853, 0, 0, 0, 0, '', 1),
(133, 7, 'Macedonia', 0, 'MK', 389, 0, 0, 0, 1, '', 1),
(134, 4, 'Madagascar', 0, 'MG', 261, 0, 0, 0, 1, '', 1),
(135, 4, 'Malawi', 0, 'MW', 265, 0, 0, 0, 1, '', 1),
(136, 3, 'Malaysia', 0, 'MY', 60, 0, 0, 0, 1, 'NNNNN', 1),
(137, 3, 'Maldives', 0, 'MV', 960, 0, 0, 0, 1, '', 1),
(138, 4, 'Mali', 0, 'ML', 223, 0, 0, 0, 1, '', 1),
(139, 1, 'Malta', 0, 'MT', 356, 0, 0, 0, 1, 'LLL NNNN', 1),
(140, 5, 'Marshall Islands', 0, 'MH', 692, 0, 0, 0, 1, '', 1),
(141, 8, 'Martinique', 0, 'MQ', 596, 0, 0, 0, 1, '', 1),
(142, 4, 'Mauritania', 0, 'MR', 222, 0, 0, 0, 1, '', 1),
(143, 1, 'Hungary', 0, 'HU', 36, 0, 0, 0, 1, 'NNNN', 1),
(144, 4, 'Mayotte', 0, 'YT', 262, 0, 0, 0, 1, '', 1),
(145, 2, 'Mexico', 0, 'MX', 52, 0, 1, 1, 1, 'NNNNN', 1),
(146, 5, 'Micronesia', 0, 'FM', 691, 0, 0, 0, 1, '', 1),
(147, 7, 'Moldova', 0, 'MD', 373, 0, 0, 0, 1, 'C-NNNN', 1),
(148, 7, 'Monaco', 0, 'MC', 377, 0, 0, 0, 1, '980NN', 1),
(149, 3, 'Mongolia', 0, 'MN', 976, 0, 0, 0, 1, '', 1),
(150, 7, 'Montenegro', 0, 'ME', 382, 0, 0, 0, 1, 'NNNNN', 1),
(151, 8, 'Montserrat', 0, 'MS', 0, 0, 0, 0, 1, '', 1),
(152, 4, 'Morocco', 0, 'MA', 212, 0, 0, 0, 1, 'NNNNN', 1),
(153, 4, 'Mozambique', 0, 'MZ', 258, 0, 0, 0, 1, '', 1),
(154, 4, 'Namibia', 0, 'NA', 264, 0, 0, 0, 1, '', 1),
(155, 5, 'Nauru', 0, 'NR', 674, 0, 0, 0, 1, '', 1),
(156, 3, 'Nepal', 0, 'NP', 977, 0, 0, 0, 1, '', 1),
(157, 8, 'Netherlands Antilles', 0, 'AN', 599, 0, 0, 0, 1, '', 1),
(158, 5, 'New Caledonia', 0, 'NC', 687, 0, 0, 0, 1, '', 1),
(159, 8, 'Nicaragua', 0, 'NI', 505, 0, 0, 0, 1, 'NNNNNN', 1),
(160, 4, 'Niger', 0, 'NE', 227, 0, 0, 0, 1, '', 1),
(161, 5, 'Niue', 0, 'NU', 683, 0, 0, 0, 1, '', 1),
(162, 5, 'Norfolk Island', 0, 'NF', 0, 0, 0, 0, 1, '', 1),
(163, 5, 'Northern Mariana Islands', 0, 'MP', 0, 0, 0, 0, 1, '', 1),
(164, 3, 'Oman', 0, 'OM', 968, 0, 0, 0, 1, '', 1),
(165, 3, 'Pakistan', 0, 'PK', 92, 0, 0, 0, 1, '', 1),
(166, 5, 'Palau', 0, 'PW', 680, 0, 0, 0, 1, '', 1),
(167, 3, 'Palestinian Territories', 0, 'PS', 0, 0, 0, 0, 1, '', 1),
(168, 8, 'Panama', 0, 'PA', 507, 0, 0, 0, 1, 'NNNNNN', 1),
(169, 5, 'Papua New Guinea', 0, 'PG', 675, 0, 0, 0, 1, '', 1),
(170, 6, 'Paraguay', 0, 'PY', 595, 0, 0, 0, 1, '', 1),
(171, 6, 'Peru', 0, 'PE', 51, 0, 0, 0, 1, '', 1),
(172, 3, 'Philippines', 0, 'PH', 63, 0, 0, 0, 1, 'NNNN', 1),
(173, 5, 'Pitcairn', 0, 'PN', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(174, 8, 'Puerto Rico', 0, 'PR', 0, 0, 0, 0, 1, 'NNNNN', 1),
(175, 3, 'Qatar', 0, 'QA', 974, 0, 0, 0, 1, '', 1),
(176, 4, 'Reunion Island', 0, 'RE', 262, 0, 0, 0, 1, '', 1),
(177, 7, 'Russian Federation', 0, 'RU', 7, 0, 0, 0, 1, 'NNNNNN', 1),
(178, 4, 'Rwanda', 0, 'RW', 250, 0, 0, 0, 1, '', 1),
(179, 8, 'Saint Barthelemy', 0, 'BL', 0, 0, 0, 0, 1, '', 1),
(180, 8, 'Saint Kitts and Nevis', 0, 'KN', 0, 0, 0, 0, 1, '', 1),
(181, 8, 'Saint Lucia', 0, 'LC', 0, 0, 0, 0, 1, '', 1),
(182, 8, 'Saint Martin', 0, 'MF', 0, 0, 0, 0, 1, '', 1),
(183, 8, 'Saint Pierre and Miquelon', 0, 'PM', 508, 0, 0, 0, 1, '', 1),
(184, 8, 'Saint Vincent and the Grenadines', 0, 'VC', 0, 0, 0, 0, 1, '', 1),
(185, 5, 'Samoa', 0, 'WS', 685, 0, 0, 0, 1, '', 1),
(186, 7, 'San Marino', 0, 'SM', 378, 0, 0, 0, 1, 'NNNNN', 1),
(187, 4, 'São Tomé and Príncipe', 0, 'ST', 239, 0, 0, 0, 1, '', 1),
(188, 3, 'Saudi Arabia', 0, 'SA', 966, 0, 0, 0, 1, '', 1),
(189, 4, 'Senegal', 0, 'SN', 221, 0, 0, 0, 1, '', 1),
(190, 7, 'Serbia', 0, 'RS', 381, 0, 0, 0, 1, 'NNNNN', 1),
(191, 4, 'Seychelles', 0, 'SC', 248, 0, 0, 0, 1, '', 1),
(192, 4, 'Sierra Leone', 0, 'SL', 232, 0, 0, 0, 1, '', 1),
(193, 1, 'Slovenia', 0, 'SI', 386, 0, 0, 0, 1, 'C-NNNN', 1),
(194, 5, 'Solomon Islands', 0, 'SB', 677, 0, 0, 0, 1, '', 1),
(195, 4, 'Somalia', 0, 'SO', 252, 0, 0, 0, 1, '', 1),
(196, 8, 'South Georgia and the South Sandwich Islands', 0, 'GS', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(197, 3, 'Sri Lanka', 0, 'LK', 94, 0, 0, 0, 1, 'NNNNN', 1),
(198, 4, 'Sudan', 0, 'SD', 249, 0, 0, 0, 1, '', 1),
(199, 8, 'Suriname', 0, 'SR', 597, 0, 0, 0, 1, '', 1),
(200, 7, 'Svalbard and Jan Mayen', 0, 'SJ', 0, 0, 0, 0, 1, '', 1),
(201, 4, 'Swaziland', 0, 'SZ', 268, 0, 0, 0, 1, '', 1),
(202, 3, 'Syria', 0, 'SY', 963, 0, 0, 0, 1, '', 1),
(203, 3, 'Taiwan', 0, 'TW', 886, 0, 0, 0, 1, 'NNNNN', 1),
(204, 3, 'Tajikistan', 0, 'TJ', 992, 0, 0, 0, 1, '', 1),
(205, 4, 'Tanzania', 0, 'TZ', 255, 0, 0, 0, 1, '', 1),
(206, 3, 'Thailand', 0, 'TH', 66, 0, 0, 0, 1, 'NNNNN', 1),
(207, 5, 'Tokelau', 0, 'TK', 690, 0, 0, 0, 1, '', 1),
(208, 5, 'Tonga', 0, 'TO', 676, 0, 0, 0, 1, '', 1),
(209, 6, 'Trinidad and Tobago', 0, 'TT', 0, 0, 0, 0, 1, '', 1),
(210, 4, 'Tunisia', 0, 'TN', 216, 0, 0, 0, 1, '', 1),
(211, 7, 'Turkey', 0, 'TR', 90, 0, 0, 0, 1, 'NNNNN', 1),
(212, 3, 'Turkmenistan', 0, 'TM', 993, 0, 0, 0, 1, '', 1),
(213, 8, 'Turks and Caicos Islands', 0, 'TC', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(214, 5, 'Tuvalu', 0, 'TV', 688, 0, 0, 0, 1, '', 1),
(215, 4, 'Uganda', 0, 'UG', 256, 0, 0, 0, 1, '', 1),
(216, 1, 'Ukraine', 0, 'UA', 380, 0, 0, 0, 1, 'NNNNN', 1),
(217, 3, 'United Arab Emirates', 0, 'AE', 971, 0, 0, 0, 1, '', 1),
(218, 6, 'Uruguay', 0, 'UY', 598, 0, 0, 0, 1, '', 1),
(219, 3, 'Uzbekistan', 0, 'UZ', 998, 0, 0, 0, 1, '', 1),
(220, 5, 'Vanuatu', 0, 'VU', 678, 0, 0, 0, 1, '', 1),
(221, 6, 'Venezuela', 0, 'VE', 58, 0, 0, 0, 1, '', 1),
(222, 3, 'Vietnam', 0, 'VN', 84, 0, 0, 0, 1, 'NNNNNN', 1),
(223, 2, 'Virgin Islands (British)', 0, 'VG', 0, 0, 0, 0, 1, 'CNNNN', 1),
(224, 2, 'Virgin Islands (U.S.)', 0, 'VI', 0, 0, 0, 0, 1, '', 1),
(225, 5, 'Wallis and Futuna', 0, 'WF', 681, 0, 0, 0, 1, '', 1),
(226, 4, 'Western Sahara', 0, 'EH', 0, 0, 0, 0, 1, '', 1),
(227, 3, 'Yemen', 0, 'YE', 967, 0, 0, 0, 1, '', 1),
(228, 4, 'Zambia', 0, 'ZM', 260, 0, 0, 0, 1, '', 1),
(229, 4, 'Zimbabwe', 0, 'ZW', 263, 0, 0, 0, 1, '', 1),
(230, 7, 'Albania', 0, 'AL', 355, 0, 0, 0, 1, 'NNNN', 1),
(231, 3, 'Afghanistan', 0, 'AF', 93, 0, 0, 0, 0, '', 1),
(232, 5, 'Antarctica', 0, 'AQ', 0, 0, 0, 0, 1, '', 1),
(233, 1, 'Bosnia and Herzegovina', 0, 'BA', 387, 0, 0, 0, 1, '', 1),
(234, 5, 'Bouvet Island', 0, 'BV', 0, 0, 0, 0, 1, '', 1),
(235, 5, 'British Indian Ocean Territory', 0, 'IO', 0, 0, 0, 0, 1, 'LLLL NLL', 1),
(236, 1, 'Bulgaria', 0, 'BG', 359, 0, 0, 0, 1, 'NNNN', 1),
(237, 8, 'Cayman Islands', 0, 'KY', 0, 0, 0, 0, 1, '', 1),
(238, 3, 'Christmas Island', 0, 'CX', 0, 0, 0, 0, 1, '', 1),
(239, 3, 'Cocos (Keeling) Islands', 0, 'CC', 0, 0, 0, 0, 1, '', 1),
(240, 5, 'Cook Islands', 0, 'CK', 682, 0, 0, 0, 1, '', 1),
(241, 6, 'French Guiana', 0, 'GF', 594, 0, 0, 0, 1, '', 1),
(242, 5, 'French Polynesia', 0, 'PF', 689, 0, 0, 0, 1, '', 1),
(243, 5, 'French Southern Territories', 0, 'TF', 0, 0, 0, 0, 1, '', 1),
(244, 7, 'Åland Islands', 0, 'AX', 0, 0, 0, 0, 1, 'NNNNN', 1);

-- --------------------------------------------------------

--
-- Table structure for table `country_state`
--

DROP TABLE IF EXISTS `country_state`;
CREATE TABLE IF NOT EXISTS `country_state` (
  `id_state` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_country` int(11) unsigned NOT NULL,
  `id_zone` int(11) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `iso_code` varchar(7) NOT NULL,
  `tax_behavior` smallint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_state`),
  KEY `id_country` (`id_country`),
  KEY `name` (`name`),
  KEY `id_zone` (`id_zone`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=313 ;

--
-- Dumping data for table `country_state`
--

INSERT INTO `country_state` (`id_state`, `id_country`, `id_zone`, `name`, `iso_code`, `tax_behavior`, `active`) VALUES
(1, 21, 2, 'Alabama', 'AL', 0, 1),
(2, 21, 2, 'Alaska', 'AK', 0, 1),
(3, 21, 2, 'Arizona', 'AZ', 0, 1),
(4, 21, 2, 'Arkansas', 'AR', 0, 1),
(5, 21, 2, 'California', 'CA', 0, 1),
(6, 21, 2, 'Colorado', 'CO', 0, 1),
(7, 21, 2, 'Connecticut', 'CT', 0, 1),
(8, 21, 2, 'Delaware', 'DE', 0, 1),
(9, 21, 2, 'Florida', 'FL', 0, 1),
(10, 21, 2, 'Georgia', 'GA', 0, 1),
(11, 21, 2, 'Hawaii', 'HI', 0, 1),
(12, 21, 2, 'Idaho', 'ID', 0, 1),
(13, 21, 2, 'Illinois', 'IL', 0, 1),
(14, 21, 2, 'Indiana', 'IN', 0, 1),
(15, 21, 2, 'Iowa', 'IA', 0, 1),
(16, 21, 2, 'Kansas', 'KS', 0, 1),
(17, 21, 2, 'Kentucky', 'KY', 0, 1),
(18, 21, 2, 'Louisiana', 'LA', 0, 1),
(19, 21, 2, 'Maine', 'ME', 0, 1),
(20, 21, 2, 'Maryland', 'MD', 0, 1),
(21, 21, 2, 'Massachusetts', 'MA', 0, 1),
(22, 21, 2, 'Michigan', 'MI', 0, 1),
(23, 21, 2, 'Minnesota', 'MN', 0, 1),
(24, 21, 2, 'Mississippi', 'MS', 0, 1),
(25, 21, 2, 'Missouri', 'MO', 0, 1),
(26, 21, 2, 'Montana', 'MT', 0, 1),
(27, 21, 2, 'Nebraska', 'NE', 0, 1),
(28, 21, 2, 'Nevada', 'NV', 0, 1),
(29, 21, 2, 'New Hampshire', 'NH', 0, 1),
(30, 21, 2, 'New Jersey', 'NJ', 0, 1),
(31, 21, 2, 'New Mexico', 'NM', 0, 1),
(32, 21, 2, 'New York', 'NY', 0, 1),
(33, 21, 2, 'North Carolina', 'NC', 0, 1),
(34, 21, 2, 'North Dakota', 'ND', 0, 1),
(35, 21, 2, 'Ohio', 'OH', 0, 1),
(36, 21, 2, 'Oklahoma', 'OK', 0, 1),
(37, 21, 2, 'Oregon', 'OR', 0, 1),
(38, 21, 2, 'Pennsylvania', 'PA', 0, 1),
(39, 21, 2, 'Rhode Island', 'RI', 0, 1),
(40, 21, 2, 'South Carolina', 'SC', 0, 1),
(41, 21, 2, 'South Dakota', 'SD', 0, 1),
(42, 21, 2, 'Tennessee', 'TN', 0, 1),
(43, 21, 2, 'Texas', 'TX', 0, 1),
(44, 21, 2, 'Utah', 'UT', 0, 1),
(45, 21, 2, 'Vermont', 'VT', 0, 1),
(46, 21, 2, 'Virginia', 'VA', 0, 1),
(47, 21, 2, 'Washington', 'WA', 0, 1),
(48, 21, 2, 'West Virginia', 'WV', 0, 1),
(49, 21, 2, 'Wisconsin', 'WI', 0, 1),
(50, 21, 2, 'Wyoming', 'WY', 0, 1),
(51, 21, 2, 'Puerto Rico', 'PR', 0, 1),
(52, 21, 2, 'US Virgin Islands', 'VI', 0, 1),
(53, 21, 2, 'District of Columbia', 'DC', 0, 1),
(54, 145, 2, 'Aguascalientes', 'AGS', 0, 1),
(55, 145, 2, 'Baja California', 'BCN', 0, 1),
(56, 145, 2, 'Baja California Sur', 'BCS', 0, 1),
(57, 145, 2, 'Campeche', 'CAM', 0, 1),
(58, 145, 2, 'Chiapas', 'CHP', 0, 1),
(59, 145, 2, 'Chihuahua', 'CHH', 0, 1),
(60, 145, 2, 'Coahuila', 'COA', 0, 1),
(61, 145, 2, 'Colima', 'COL', 0, 1),
(62, 145, 2, 'Distrito Federal', 'DIF', 0, 1),
(63, 145, 2, 'Durango', 'DUR', 0, 1),
(64, 145, 2, 'Guanajuato', 'GUA', 0, 1),
(65, 145, 2, 'Guerrero', 'GRO', 0, 1),
(66, 145, 2, 'Hidalgo', 'HID', 0, 1),
(67, 145, 2, 'Jalisco', 'JAL', 0, 1),
(68, 145, 2, 'Estado de México', 'MEX', 0, 1),
(69, 145, 2, 'Michoacán', 'MIC', 0, 1),
(70, 145, 2, 'Morelos', 'MOR', 0, 1),
(71, 145, 2, 'Nayarit', 'NAY', 0, 1),
(72, 145, 2, 'Nuevo León', 'NLE', 0, 1),
(73, 145, 2, 'Oaxaca', 'OAX', 0, 1),
(74, 145, 2, 'Puebla', 'PUE', 0, 1),
(75, 145, 2, 'Querétaro', 'QUE', 0, 1),
(76, 145, 2, 'Quintana Roo', 'ROO', 0, 1),
(77, 145, 2, 'San Luis Potosí', 'SLP', 0, 1),
(78, 145, 2, 'Sinaloa', 'SIN', 0, 1),
(79, 145, 2, 'Sonora', 'SON', 0, 1),
(80, 145, 2, 'Tabasco', 'TAB', 0, 1),
(81, 145, 2, 'Tamaulipas', 'TAM', 0, 1),
(82, 145, 2, 'Tlaxcala', 'TLA', 0, 1),
(83, 145, 2, 'Veracruz', 'VER', 0, 1),
(84, 145, 2, 'Yucatán', 'YUC', 0, 1),
(85, 145, 2, 'Zacatecas', 'ZAC', 0, 1),
(86, 4, 2, 'Ontario', 'ON', 0, 1),
(87, 4, 2, 'Quebec', 'QC', 0, 1),
(88, 4, 2, 'British Columbia', 'BC', 0, 1),
(89, 4, 2, 'Alberta', 'AB', 0, 1),
(90, 4, 2, 'Manitoba', 'MB', 0, 1),
(91, 4, 2, 'Saskatchewan', 'SK', 0, 1),
(92, 4, 2, 'Nova Scotia', 'NS', 0, 1),
(93, 4, 2, 'New Brunswick', 'NB', 0, 1),
(94, 4, 2, 'Newfoundland and Labrador', 'NL', 0, 1),
(95, 4, 2, 'Prince Edward Island', 'PE', 0, 1),
(96, 4, 2, 'Northwest Territories', 'NT', 0, 1),
(97, 4, 2, 'Yukon', 'YT', 0, 1),
(98, 4, 2, 'Nunavut', 'NU', 0, 1),
(99, 44, 6, 'Buenos Aires', 'B', 0, 1),
(100, 44, 6, 'Catamarca', 'K', 0, 1),
(101, 44, 6, 'Chaco', 'H', 0, 1),
(102, 44, 6, 'Chubut', 'U', 0, 1),
(103, 44, 6, 'Ciudad de Buenos Aires', 'C', 0, 1),
(104, 44, 6, 'Córdoba', 'X', 0, 1),
(105, 44, 6, 'Corrientes', 'W', 0, 1),
(106, 44, 6, 'Entre Ríos', 'E', 0, 1),
(107, 44, 6, 'Formosa', 'P', 0, 1),
(108, 44, 6, 'Jujuy', 'Y', 0, 1),
(109, 44, 6, 'La Pampa', 'L', 0, 1),
(110, 44, 6, 'La Rioja', 'F', 0, 1),
(111, 44, 6, 'Mendoza', 'M', 0, 1),
(112, 44, 6, 'Misiones', 'N', 0, 1),
(113, 44, 6, 'Neuquén', 'Q', 0, 1),
(114, 44, 6, 'Río Negro', 'R', 0, 1),
(115, 44, 6, 'Salta', 'A', 0, 1),
(116, 44, 6, 'San Juan', 'J', 0, 1),
(117, 44, 6, 'San Luis', 'D', 0, 1),
(118, 44, 6, 'Santa Cruz', 'Z', 0, 1),
(119, 44, 6, 'Santa Fe', 'S', 0, 1),
(120, 44, 6, 'Santiago del Estero', 'G', 0, 1),
(121, 44, 6, 'Tierra del Fuego', 'V', 0, 1),
(122, 44, 6, 'Tucumán', 'T', 0, 1),
(123, 10, 1, 'Agrigento', 'AG', 0, 1),
(124, 10, 1, 'Alessandria', 'AL', 0, 1),
(125, 10, 1, 'Ancona', 'AN', 0, 1),
(126, 10, 1, 'Aosta', 'AO', 0, 1),
(127, 10, 1, 'Arezzo', 'AR', 0, 1),
(128, 10, 1, 'Ascoli Piceno', 'AP', 0, 1),
(129, 10, 1, 'Asti', 'AT', 0, 1),
(130, 10, 1, 'Avellino', 'AV', 0, 1),
(131, 10, 1, 'Bari', 'BA', 0, 1),
(132, 10, 1, 'Barletta-Andria-Trani', 'BT', 0, 1),
(133, 10, 1, 'Belluno', 'BL', 0, 1),
(134, 10, 1, 'Benevento', 'BN', 0, 1),
(135, 10, 1, 'Bergamo', 'BG', 0, 1),
(136, 10, 1, 'Biella', 'BI', 0, 1),
(137, 10, 1, 'Bologna', 'BO', 0, 1),
(138, 10, 1, 'Bolzano', 'BZ', 0, 1),
(139, 10, 1, 'Brescia', 'BS', 0, 1),
(140, 10, 1, 'Brindisi', 'BR', 0, 1),
(141, 10, 1, 'Cagliari', 'CA', 0, 1),
(142, 10, 1, 'Caltanissetta', 'CL', 0, 1),
(143, 10, 1, 'Campobasso', 'CB', 0, 1),
(144, 10, 1, 'Carbonia-Iglesias', 'CI', 0, 1),
(145, 10, 1, 'Caserta', 'CE', 0, 1),
(146, 10, 1, 'Catania', 'CT', 0, 1),
(147, 10, 1, 'Catanzaro', 'CZ', 0, 1),
(148, 10, 1, 'Chieti', 'CH', 0, 1),
(149, 10, 1, 'Como', 'CO', 0, 1),
(150, 10, 1, 'Cosenza', 'CS', 0, 1),
(151, 10, 1, 'Cremona', 'CR', 0, 1),
(152, 10, 1, 'Crotone', 'KR', 0, 1),
(153, 10, 1, 'Cuneo', 'CN', 0, 1),
(154, 10, 1, 'Enna', 'EN', 0, 1),
(155, 10, 1, 'Fermo', 'FM', 0, 1),
(156, 10, 1, 'Ferrara', 'FE', 0, 1),
(157, 10, 1, 'Firenze', 'FI', 0, 1),
(158, 10, 1, 'Foggia', 'FG', 0, 1),
(159, 10, 1, 'Forlì-Cesena', 'FC', 0, 1),
(160, 10, 1, 'Frosinone', 'FR', 0, 1),
(161, 10, 1, 'Genova', 'GE', 0, 1),
(162, 10, 1, 'Gorizia', 'GO', 0, 1),
(163, 10, 1, 'Grosseto', 'GR', 0, 1),
(164, 10, 1, 'Imperia', 'IM', 0, 1),
(165, 10, 1, 'Isernia', 'IS', 0, 1),
(166, 10, 1, 'L''Aquila', 'AQ', 0, 1),
(167, 10, 1, 'La Spezia', 'SP', 0, 1),
(168, 10, 1, 'Latina', 'LT', 0, 1),
(169, 10, 1, 'Lecce', 'LE', 0, 1),
(170, 10, 1, 'Lecco', 'LC', 0, 1),
(171, 10, 1, 'Livorno', 'LI', 0, 1),
(172, 10, 1, 'Lodi', 'LO', 0, 1),
(173, 10, 1, 'Lucca', 'LU', 0, 1),
(174, 10, 1, 'Macerata', 'MC', 0, 1),
(175, 10, 1, 'Mantova', 'MN', 0, 1),
(176, 10, 1, 'Massa', 'MS', 0, 1),
(177, 10, 1, 'Matera', 'MT', 0, 1),
(178, 10, 1, 'Medio Campidano', 'VS', 0, 1),
(179, 10, 1, 'Messina', 'ME', 0, 1),
(180, 10, 1, 'Milano', 'MI', 0, 1),
(181, 10, 1, 'Modena', 'MO', 0, 1),
(182, 10, 1, 'Monza e della Brianza', 'MB', 0, 1),
(183, 10, 1, 'Napoli', 'NA', 0, 1),
(184, 10, 1, 'Novara', 'NO', 0, 1),
(185, 10, 1, 'Nuoro', 'NU', 0, 1),
(186, 10, 1, 'Ogliastra', 'OG', 0, 1),
(187, 10, 1, 'Olbia-Tempio', 'OT', 0, 1),
(188, 10, 1, 'Oristano', 'OR', 0, 1),
(189, 10, 1, 'Padova', 'PD', 0, 1),
(190, 10, 1, 'Palermo', 'PA', 0, 1),
(191, 10, 1, 'Parma', 'PR', 0, 1),
(192, 10, 1, 'Pavia', 'PV', 0, 1),
(193, 10, 1, 'Perugia', 'PG', 0, 1),
(194, 10, 1, 'Pesaro-Urbino', 'PU', 0, 1),
(195, 10, 1, 'Pescara', 'PE', 0, 1),
(196, 10, 1, 'Piacenza', 'PC', 0, 1),
(197, 10, 1, 'Pisa', 'PI', 0, 1),
(198, 10, 1, 'Pistoia', 'PT', 0, 1),
(199, 10, 1, 'Pordenone', 'PN', 0, 1),
(200, 10, 1, 'Potenza', 'PZ', 0, 1),
(201, 10, 1, 'Prato', 'PO', 0, 1),
(202, 10, 1, 'Ragusa', 'RG', 0, 1),
(203, 10, 1, 'Ravenna', 'RA', 0, 1),
(204, 10, 1, 'Reggio Calabria', 'RC', 0, 1),
(205, 10, 1, 'Reggio Emilia', 'RE', 0, 1),
(206, 10, 1, 'Rieti', 'RI', 0, 1),
(207, 10, 1, 'Rimini', 'RN', 0, 1),
(208, 10, 1, 'Roma', 'RM', 0, 1),
(209, 10, 1, 'Rovigo', 'RO', 0, 1),
(210, 10, 1, 'Salerno', 'SA', 0, 1),
(211, 10, 1, 'Sassari', 'SS', 0, 1),
(212, 10, 1, 'Savona', 'SV', 0, 1),
(213, 10, 1, 'Siena', 'SI', 0, 1),
(214, 10, 1, 'Siracusa', 'SR', 0, 1),
(215, 10, 1, 'Sondrio', 'SO', 0, 1),
(216, 10, 1, 'Taranto', 'TA', 0, 1),
(217, 10, 1, 'Teramo', 'TE', 0, 1),
(218, 10, 1, 'Terni', 'TR', 0, 1),
(219, 10, 1, 'Torino', 'TO', 0, 1),
(220, 10, 1, 'Trapani', 'TP', 0, 1),
(221, 10, 1, 'Trento', 'TN', 0, 1),
(222, 10, 1, 'Treviso', 'TV', 0, 1),
(223, 10, 1, 'Trieste', 'TS', 0, 1),
(224, 10, 1, 'Udine', 'UD', 0, 1),
(225, 10, 1, 'Varese', 'VA', 0, 1),
(226, 10, 1, 'Venezia', 'VE', 0, 1),
(227, 10, 1, 'Verbano-Cusio-Ossola', 'VB', 0, 1),
(228, 10, 1, 'Vercelli', 'VC', 0, 1),
(229, 10, 1, 'Verona', 'VR', 0, 1),
(230, 10, 1, 'Vibo Valentia', 'VV', 0, 1),
(231, 10, 1, 'Vicenza', 'VI', 0, 1),
(232, 10, 1, 'Viterbo', 'VT', 0, 1),
(233, 111, 3, 'Aceh', 'AC', 0, 1),
(234, 111, 3, 'Bali', 'BA', 0, 1),
(235, 111, 3, 'Bangka', 'BB', 0, 1),
(236, 111, 3, 'Banten', 'BT', 0, 1),
(237, 111, 3, 'Bengkulu', 'BE', 0, 1),
(238, 111, 3, 'Central Java', 'JT', 0, 1),
(239, 111, 3, 'Central Kalimantan', 'KT', 0, 1),
(240, 111, 3, 'Central Sulawesi', 'ST', 0, 1),
(241, 111, 3, 'Coat of arms of East Java', 'JI', 0, 1),
(242, 111, 3, 'East kalimantan', 'KI', 0, 1),
(243, 111, 3, 'East Nusa Tenggara', 'NT', 0, 1),
(244, 111, 3, 'Lambang propinsi', 'GO', 0, 1),
(245, 111, 3, 'Jakarta', 'JK', 0, 1),
(246, 111, 3, 'Jambi', 'JA', 0, 1),
(247, 111, 3, 'Lampung', 'LA', 0, 1),
(248, 111, 3, 'Maluku', 'MA', 0, 1),
(249, 111, 3, 'North Maluku', 'MU', 0, 1),
(250, 111, 3, 'North Sulawesi', 'SA', 0, 1),
(251, 111, 3, 'North Sumatra', 'SU', 0, 1),
(252, 111, 3, 'Papua', 'PA', 0, 1),
(253, 111, 3, 'Riau', 'RI', 0, 1),
(254, 111, 3, 'Lambang Riau', 'KR', 0, 1),
(255, 111, 3, 'Southeast Sulawesi', 'SG', 0, 1),
(256, 111, 3, 'South Kalimantan', 'KS', 0, 1),
(257, 111, 3, 'South Sulawesi', 'SN', 0, 1),
(258, 111, 3, 'South Sumatra', 'SS', 0, 1),
(259, 111, 3, 'West Java', 'JB', 0, 1),
(260, 111, 3, 'West Kalimantan', 'KB', 0, 1),
(261, 111, 3, 'West Nusa Tenggara', 'NB', 0, 1),
(262, 111, 3, 'Lambang Provinsi Papua Barat', 'PB', 0, 1),
(263, 111, 3, 'West Sulawesi', 'SR', 0, 1),
(264, 111, 3, 'West Sumatra', 'SB', 0, 1),
(265, 111, 3, 'Yogyakarta', 'YO', 0, 1),
(266, 11, 3, 'Aichi', '23', 0, 1),
(267, 11, 3, 'Akita', '05', 0, 1),
(268, 11, 3, 'Aomori', '02', 0, 1),
(269, 11, 3, 'Chiba', '12', 0, 1),
(270, 11, 3, 'Ehime', '38', 0, 1),
(271, 11, 3, 'Fukui', '18', 0, 1),
(272, 11, 3, 'Fukuoka', '40', 0, 1),
(273, 11, 3, 'Fukushima', '07', 0, 1),
(274, 11, 3, 'Gifu', '21', 0, 1),
(275, 11, 3, 'Gunma', '10', 0, 1),
(276, 11, 3, 'Hiroshima', '34', 0, 1),
(277, 11, 3, 'Hokkaido', '01', 0, 1),
(278, 11, 3, 'Hyogo', '28', 0, 1),
(279, 11, 3, 'Ibaraki', '08', 0, 1),
(280, 11, 3, 'Ishikawa', '17', 0, 1),
(281, 11, 3, 'Iwate', '03', 0, 1),
(282, 11, 3, 'Kagawa', '37', 0, 1),
(283, 11, 3, 'Kagoshima', '46', 0, 1),
(284, 11, 3, 'Kanagawa', '14', 0, 1),
(285, 11, 3, 'Kochi', '39', 0, 1),
(286, 11, 3, 'Kumamoto', '43', 0, 1),
(287, 11, 3, 'Kyoto', '26', 0, 1),
(288, 11, 3, 'Mie', '24', 0, 1),
(289, 11, 3, 'Miyagi', '04', 0, 1),
(290, 11, 3, 'Miyazaki', '45', 0, 1),
(291, 11, 3, 'Nagano', '20', 0, 1),
(292, 11, 3, 'Nagasaki', '42', 0, 1),
(293, 11, 3, 'Nara', '29', 0, 1),
(294, 11, 3, 'Niigata', '15', 0, 1),
(295, 11, 3, 'Oita', '44', 0, 1),
(296, 11, 3, 'Okayama', '33', 0, 1),
(297, 11, 3, 'Okinawa', '47', 0, 1),
(298, 11, 3, 'Osaka', '27', 0, 1),
(299, 11, 3, 'Saga', '41', 0, 1),
(300, 11, 3, 'Saitama', '11', 0, 1),
(301, 11, 3, 'Shiga', '25', 0, 1),
(302, 11, 3, 'Shimane', '32', 0, 1),
(303, 11, 3, 'Shizuoka', '22', 0, 1),
(304, 11, 3, 'Tochigi', '09', 0, 1),
(305, 11, 3, 'Tokushima', '36', 0, 1),
(306, 11, 3, 'Tokyo', '13', 0, 1),
(307, 11, 3, 'Tottori', '31', 0, 1),
(308, 11, 3, 'Toyama', '16', 0, 1),
(309, 11, 3, 'Wakayama', '30', 0, 1),
(310, 11, 3, 'Yamagata', '06', 0, 1),
(311, 11, 3, 'Yamaguchi', '35', 0, 1),
(312, 11, 3, 'Yamanashi', '19', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `country_zone`
--

DROP TABLE IF EXISTS `country_zone`;
CREATE TABLE IF NOT EXISTS `country_zone` (
  `id_zone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_zone`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `country_zone`
--

INSERT INTO `country_zone` (`id_zone`, `name`, `active`) VALUES
(1, 'Europe', 1),
(2, 'North America', 1),
(3, 'Asia', 1),
(4, 'Africa', 1),
(5, 'Oceania', 1),
(6, 'South America', 1),
(7, 'Europe (non-EU)', 1),
(8, 'Central America/Antilla', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE IF NOT EXISTS `employee` (
  `emp_id` int(4) NOT NULL AUTO_INCREMENT,
  `emp_name` varchar(40) NOT NULL,
  `emp_surname` varchar(40) NOT NULL,
  `emp_reg_date` datetime NOT NULL,
  `company_id` int(6) NOT NULL,
  `emp_address_1` varchar(40) DEFAULT NULL,
  `emp_address_2` varchar(40) DEFAULT NULL,
  `emp_address_3` varchar(40) DEFAULT NULL,
  `emp_phone` varchar(32) DEFAULT NULL,
  `emp_city` varchar(40) DEFAULT NULL,
  `emp_country` varchar(40) DEFAULT NULL,
  `emp_language` varchar(40) DEFAULT NULL,
  `emp_job_description` text,
  `emp_cost` decimal(10,2) NOT NULL,
  `emp_hours` decimal(10,2) NOT NULL,
  `emp_avatar` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`emp_id`),
  KEY `employee_fk1` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;



--
-- Table structure for table `estimate`
--

DROP TABLE IF EXISTS `estimate`;
CREATE TABLE IF NOT EXISTS `estimate` (
  `est_id` int(11) NOT NULL AUTO_INCREMENT,
  `est_code` varchar(45) DEFAULT NULL,
  `client_id` int(6) NOT NULL,
  `est_start_date` date NOT NULL,
  `est_due_date` date NOT NULL,
  `est_priority` int(11) NOT NULL,
  `est_status` int(6) NOT NULL,
  `est_visible` tinyint(1) NOT NULL DEFAULT '0',
  `est_invoiced` tinyint(1) NOT NULL DEFAULT '0',
  `est_emailed` tinyint(1) NOT NULL DEFAULT '0',
  `date_email` datetime NOT NULL,
  `est_tax` int(6) NOT NULL,
  `est_discount` int(6) NOT NULL,
  `est_currency` varchar(10) NOT NULL,
  `user_id` int(6) DEFAULT NULL,
  `company_id` int(6) DEFAULT NULL,
  `est_trans_date` datetime NOT NULL,
  `est_notes` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`est_id`),
  KEY `fk_estimate_client1_idx` (`client_id`),
  KEY `fk_estimate_est_status1_idx` (`est_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;


--
-- Table structure for table `est_details`
--

DROP TABLE IF EXISTS `est_details`;
CREATE TABLE IF NOT EXISTS `est_details` (
  `details_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `item_name` varchar(45) DEFAULT NULL,
  `item_description` varchar(45) DEFAULT NULL,
  `details_date` datetime DEFAULT NULL,
  `details_hours` varchar(45) DEFAULT NULL,
  `details_discount` decimal(20,2) NOT NULL,
  `details_tax` float NOT NULL,
  `details_rate` varchar(45) DEFAULT NULL,
  `est_total` decimal(20,2) NOT NULL,
  `est_id` int(11) NOT NULL,
  PRIMARY KEY (`details_id`),
  KEY `fk_est_details_estimate1_idx` (`est_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Table structure for table `est_status`
--

DROP TABLE IF EXISTS `est_status`;
CREATE TABLE IF NOT EXISTS `est_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(45) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `est_status`
--

INSERT INTO `est_status` (`status_id`, `status_name`) VALUES
(1, 'PENDING'),
(2, 'ACCEPTED'),
(3, 'DECLINED'),
(4, 'EMAILED'),
(5, 'INVOICED');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `event_id` int(6) NOT NULL AUTO_INCREMENT,
  `user_id` int(6) NOT NULL,
  `event_title` varchar(45) DEFAULT NULL,
  `event_description` text,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `event_color` varchar(45) DEFAULT NULL,
  `is_task` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
CREATE TABLE IF NOT EXISTS `invoice` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `inv_no` varchar(25) DEFAULT NULL,
  `client_id` bigint(11) DEFAULT NULL,
  `currency` varchar(25) DEFAULT NULL,
  `currency_rate` decimal(13,6) DEFAULT NULL,
  `credit` double(18,2) DEFAULT NULL,
  `refund` double(18,2) DEFAULT NULL,
  `notes` text,
  `status` int(2) DEFAULT '0',
  `is_recurring` tinyint(1) NOT NULL DEFAULT '0',
  `recur_period` varchar(10) NOT NULL,
  `recur_frequency` varchar(12) NOT NULL,
  `recur_start_date` date NOT NULL,
  `recur_end_date` date NOT NULL,
  `recur_next_date` date NOT NULL,
  `approved` tinyint(1) DEFAULT '0',
  `emailed` tinyint(1) DEFAULT '0',
  `due_at` datetime DEFAULT NULL,
  `reminded_at` datetime DEFAULT NULL,
  `paid_at` datetime DEFAULT NULL,
  `payment_method` int(2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `invoice_due` date NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `inv_created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB   DEFAULT CHARSET=utf8 AUTO_INCREMENT=163 ;


--
-- Table structure for table `invoice_item`
--

DROP TABLE IF EXISTS `invoice_item`;
CREATE TABLE IF NOT EXISTS `invoice_item` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint(11) DEFAULT NULL,
  `item_name` varchar(100) DEFAULT NULL,
  `item_desc` varchar(100) DEFAULT NULL,
  `discount` float NOT NULL,
  `tax` float NOT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `price` double(18,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=205 ;


--
-- Table structure for table `invoice_status`
--

DROP TABLE IF EXISTS `invoice_status`;
CREATE TABLE IF NOT EXISTS `invoice_status` (
  `status_id` int(6) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `invoice_status`
--

INSERT INTO `invoice_status` (`status_id`, `status_name`) VALUES
(1, 'PAID'),
(2, 'UNPAID'),
(3, 'PARTIALLY PAID');

-- --------------------------------------------------------

--
-- Table structure for table `issue`
--

DROP TABLE IF EXISTS `issue`;
CREATE TABLE IF NOT EXISTS `issue` (
  `issue_id` int(6) NOT NULL AUTO_INCREMENT,
  `project_id` int(6) NOT NULL,
  `issue_priority` int(2) NOT NULL,
  `issue_status` int(2) NOT NULL,
  `issue_description` text NOT NULL,
  `task_id` int(6) NOT NULL,
  `user_id` int(6) NOT NULL,
  `issue_date` date NOT NULL,
  `entry_date` datetime NOT NULL,
  `issue_files` varchar(120) NOT NULL,
  `issue_by` int(6) NOT NULL,
  PRIMARY KEY (`issue_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;



--
-- Table structure for table `issue_replies`
--

DROP TABLE IF EXISTS `issue_replies`;
CREATE TABLE IF NOT EXISTS `issue_replies` (
  `reply_id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) DEFAULT NULL,
  `reply_date` datetime NOT NULL,
  `reply_message` varchar(45) DEFAULT NULL,
  `reply_user_id` int(6) DEFAULT NULL,
  `reply_user_type` int(2) DEFAULT NULL,
  `reply_attachment` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`reply_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT 'empty',
  `item_desc` longtext COLLATE utf8_unicode_ci,
  `unit_cost` decimal(10,2) DEFAULT '0.00',
  `item_tax_rate` decimal(10,2) DEFAULT '0.00',
  `item_tax_total` decimal(10,2) DEFAULT '0.00',
  `quantity` decimal(10,2) DEFAULT '0.00',
  `deleted` smallint(1) NOT NULL DEFAULT '0',
  `taxable` tinyint(1) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;



--
-- Table structure for table `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) DEFAULT NULL,
  `login` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `msg_id` int(6) NOT NULL AUTO_INCREMENT,
  `user_to` int(6) DEFAULT NULL,
  `client_to` int(6) DEFAULT NULL,
  `user_from` int(6) DEFAULT NULL,
  `client_from` int(6) DEFAULT NULL,
  `subject` varchar(80) NOT NULL,
  `message` text,
  `msg_user_type` int(2) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `attachment` varchar(100) DEFAULT NULL,
  `file_type` varchar(10) DEFAULT NULL,
  `seen_date` datetime NOT NULL,
  `date_sent` datetime NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`msg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=97 ;


--
-- Table structure for table `message_replies`
--

DROP TABLE IF EXISTS `message_replies`;
CREATE TABLE IF NOT EXISTS `message_replies` (
  `replies_id` int(11) NOT NULL AUTO_INCREMENT,
  `messages_replies_project_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `reply_date` datetime NOT NULL,
  `message` text NOT NULL,
  `user_type` int(2) NOT NULL COMMENT 'client/user',
  `replied_by_id` int(11) NOT NULL,
  PRIMARY KEY (`replies_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `milestones`
--

DROP TABLE IF EXISTS `milestones`;
CREATE TABLE IF NOT EXISTS `milestones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `milestone_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `project_id` int(11) DEFAULT NULL,
  `start_date` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `due_date` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `progress` int(11) DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
CREATE TABLE IF NOT EXISTS `modules` (
  `module_id` varchar(255) NOT NULL,
  `module_parent_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `name_lang_key` varchar(255) DEFAULT NULL,
  `desc_lang_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `module_parent_id`, `parent_id`, `sort`, `icon`, `name_lang_key`, `desc_lang_key`) VALUES
('billing', 6, 0, 1, 'money', 'module_billing', 'module_billing_desc'),
('calendar', 11, 0, 0, 'calendar', 'module_calendar', 'module_calendar_desc'),
('client', 2, 0, 5, 'users', 'module_clients', 'module_clients_desc'),
('estimate', 10, 0, 2, 'balance-scale', 'module_estimate', 'module_estimate_desc'),
('expenses', 12, 0, 8, 'money', 'module_expenses', 'module_expenses_desc'),
('item', 9, 0, 9, 'bars', 'module_items', 'module_items_desc'),
('message', 4, 0, 7, 'inbox', 'module_messages', 'module_messages_desc'),
('project', 1, 0, 4, 'paper-plane', 'module_projects', 'module_projects_desc'),
('quotation', 8, 0, 3, 'file-pdf-o', 'module_quotations', 'module_quotations_desc'),
('report', 13, 0, 11, 'area-chart', 'module_report', 'module_report_desc'),
('settings', 5, 0, 12, 'cogs', 'module_settings', 'module_settings_desc'),
('support', 7, 0, 6, 'support', 'module_support', 'module_support_desc'),
('user', 3, 0, 10, 'user', 'module_users', 'module_users_desc');

-- --------------------------------------------------------

--
-- Table structure for table `modules_actions`
--

DROP TABLE IF EXISTS `modules_actions`;
CREATE TABLE IF NOT EXISTS `modules_actions` (
  `action_id` varchar(100) NOT NULL,
  `module_id` varchar(100) NOT NULL,
  `action_name_key` varchar(100) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`action_id`,`module_id`),
  KEY `modules_actions_fk1` (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules_actions`
--

INSERT INTO `modules_actions` (`action_id`, `module_id`, `action_name_key`, `sort`) VALUES
('add_update', 'billing', 'module_action_add_update', 1),
('add_update', 'calendar', 'module_action_add_update', 13),
('add_update', 'client', 'module_action_add_update', 2),
('add_update', 'estimate', 'module_action_add_update', 9),
('add_update', 'message', 'module_action_add_update', 14),
('add_update', 'project', 'module_action_add_update', 6),
('add_update', 'quotation', 'module_action_add_update', 8),
('add_update', 'support', 'module_action_add_update', 11),
('add_update', 'user', 'module_action_add_update', 7),
('delete', 'billing', 'module_action_delete', 4),
('delete', 'calendar', 'module_action_delete', 14),
('delete', 'client', 'module_action_delete', 3),
('delete', 'estimate', 'module_action_delete', 9),
('delete', 'message', 'module_action_delete', 15),
('delete', 'project', 'module_action_delete', 6),
('delete', 'quotation', 'module_action_delete', 10),
('delete', 'support', 'module_action_delete', 12),
('delete', 'user', 'module_action_delete', 5);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE IF NOT EXISTS `notes` (
  `note_id` int(6) NOT NULL AUTO_INCREMENT,
  `project_id` int(6) NOT NULL,
  `note_user_id` int(6) DEFAULT NULL,
  `note_date` datetime NOT NULL,
  `note_message` text,
  PRIMARY KEY (`note_id`),
  KEY `fk_notes_projects1_idx` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `online_clients`
--

DROP TABLE IF EXISTS `online_clients`;
CREATE TABLE IF NOT EXISTS `online_clients` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `cl_user_id` int(6) DEFAULT NULL,
  `last_activity` datetime NOT NULL,
  `ip_address` varchar(60) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `online_clients_fk1` (`cl_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25657 ;



--
-- Table structure for table `online_users`
--

DROP TABLE IF EXISTS `online_users`;
CREATE TABLE IF NOT EXISTS `online_users` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `user_id` int(6) DEFAULT NULL,
  `last_activity` datetime NOT NULL,
  `ip_address` varchar(60) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `online_users_fk1` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8715 ;



--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `module_id` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`module_id`,`role_id`),
  KEY `permissions_fk2` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`module_id`, `role_id`) VALUES
('billing', 1),
('calendar', 1),
('client', 1),
('estimate', 1),
('expenses', 1),
('item', 1),
('message', 1),
('project', 1),
('quotation', 1),
('report', 1),
('settings', 1),
('support', 1),
('user', 1),
('billing', 3),
('calendar', 3),
('client', 3),
('estimate', 3),
('message', 3),
('project', 3),
('quotation', 3),
('support', 3),
('user', 3);

-- --------------------------------------------------------

--
-- Table structure for table `permissions_actions`
--

DROP TABLE IF EXISTS `permissions_actions`;
CREATE TABLE IF NOT EXISTS `permissions_actions` (
  `module_id` varchar(100) NOT NULL,
  `role_id` int(11) NOT NULL,
  `action_id` varchar(100) NOT NULL,
  PRIMARY KEY (`module_id`,`role_id`,`action_id`),
  KEY `permissions_actions_fk2` (`role_id`),
  KEY `permissions_actions_fk3` (`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions_actions`
--

INSERT INTO `permissions_actions` (`module_id`, `role_id`, `action_id`) VALUES
('billing', 1, 'add_update'),
('billing', 1, 'delete'),
('calendar', 1, 'add_update'),
('calendar', 1, 'delete'),
('client', 1, 'add_update'),
('client', 1, 'delete'),
('estimate', 1, 'add_update'),
('estimate', 1, 'delete'),
('message', 1, 'add_update'),
('message', 1, 'delete'),
('project', 1, 'add_update'),
('project', 1, 'delete'),
('quotation', 1, 'add_update'),
('quotation', 1, 'delete'),
('support', 1, 'add_update'),
('support', 1, 'delete'),
('user', 1, 'add_update'),
('user', 1, 'delete'),
('billing', 3, 'add_update'),
('client', 3, 'add_update'),
('estimate', 3, 'add_update'),
('estimate', 3, 'delete');

-- --------------------------------------------------------

--
-- Table structure for table `priorities`
--

DROP TABLE IF EXISTS `priorities`;
CREATE TABLE IF NOT EXISTS `priorities` (
  `priority_id` int(11) NOT NULL AUTO_INCREMENT,
  `priority_name` varchar(25) NOT NULL,
  PRIMARY KEY (`priority_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `priorities`
--

INSERT INTO `priorities` (`priority_id`, `priority_name`) VALUES
(1, 'LOW'),
(2, 'MEDIUM'),
(3, 'HIGH');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `project_id` int(6) NOT NULL AUTO_INCREMENT,
  `project_code` varchar(20) DEFAULT NULL,
  `project_title` varchar(255) DEFAULT NULL,
  `project_desc` text,
  `project_settings` text NOT NULL,
  `project_budget` decimal(10,2) NOT NULL,
  `project_level` int(3) NOT NULL DEFAULT '0',
  `project_use_worklog` tinyint(4) NOT NULL DEFAULT '0',
  `project_progress` int(6) NOT NULL DEFAULT '0',
  `client_id` int(4) NOT NULL,
  `project_start_date` datetime NOT NULL,
  `project_due_date` datetime NOT NULL,
  `project_status` int(1) DEFAULT NULL,
  `project_time_logged` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `user_id` int(6) NOT NULL,
  PRIMARY KEY (`project_id`),
  KEY `projects_fk1` (`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;


--
-- Table structure for table `project_activities`
--

DROP TABLE IF EXISTS `project_activities`;
CREATE TABLE IF NOT EXISTS `project_activities` (
  `activity_id` int(6) NOT NULL,
  `user_id` int(6) NOT NULL DEFAULT '0',
  `project_id` int(6) NOT NULL DEFAULT '0',
  `client_id` int(6) NOT NULL DEFAULT '0',
  `activity_message` varchar(255) NOT NULL,
  `activity_date` datetime NOT NULL,
  `activity_status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`activity_id`),
  KEY `project_id_idx` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `project_assignment`
--

DROP TABLE IF EXISTS `project_assignment`;
CREATE TABLE IF NOT EXISTS `project_assignment` (
  `project_asgn_id` int(6) NOT NULL AUTO_INCREMENT,
  `role` int(6) NOT NULL,
  `hourly_cost` decimal(10,2) NOT NULL,
  `project_id` int(6) NOT NULL,
  `user_id` int(6) DEFAULT NULL,
  PRIMARY KEY (`project_asgn_id`),
  KEY `fk_project_assignment_projects1_idx` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=104 ;


--
-- Table structure for table `project_comments`
--

DROP TABLE IF EXISTS `project_comments`;
CREATE TABLE IF NOT EXISTS `project_comments` (
  `message_id` int(6) NOT NULL AUTO_INCREMENT,
  `message_date` datetime NOT NULL,
  `message_text` longtext,
  `project_id` int(11) NOT NULL,
  `message_by_id` int(6) DEFAULT NULL,
  `user_type` int(2) NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_comments_replies`
--

DROP TABLE IF EXISTS `project_comments_replies`;
CREATE TABLE IF NOT EXISTS `project_comments_replies` (
  `reply_id` int(6) NOT NULL AUTO_INCREMENT,
  `message_id` int(6) NOT NULL,
  `reply_message` text,
  `replied_by` int(6) DEFAULT NULL,
  `user_type` int(2) NOT NULL,
  `reply_date` datetime NOT NULL,
  PRIMARY KEY (`reply_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_risks`
--

DROP TABLE IF EXISTS `project_risks`;
CREATE TABLE IF NOT EXISTS `project_risks` (
  `risk_id` int(6) NOT NULL AUTO_INCREMENT,
  `risk_name` varchar(120) NOT NULL,
  `probability` decimal(10,0) NOT NULL,
  `impact_percentage` decimal(10,0) NOT NULL,
  `project_id` int(6) NOT NULL,
  PRIMARY KEY (`risk_id`),
  KEY `fk_project_risks_projects1_idx` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;


--
-- Table structure for table `project_roles`
--

DROP TABLE IF EXISTS `project_roles`;
CREATE TABLE IF NOT EXISTS `project_roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(62) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_roles`
--

INSERT INTO `project_roles` (`role_id`, `role_name`) VALUES
(1, 'Project Manager'),
(2, 'Team Leader'),
(3, 'Customer'),
(4, 'Stakeholder');

-- --------------------------------------------------------

--
-- Table structure for table `project_settings`
--

DROP TABLE IF EXISTS `project_settings`;
CREATE TABLE IF NOT EXISTS `project_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `project_settings`
--

INSERT INTO `project_settings` (`id`, `setting`, `description`) VALUES
(1, 'view_team_members', 'Allow client to view team members'),
(2, 'view_milestones', 'Allow client to view project milestones'),
(3, 'view_tasks', 'Allow client to view project tasks'),
(4, 'comment_on_tasks', 'Allow client to comment on project tasks'),
(5, 'view_gantt', 'Allow client to view project gantt'),
(6, 'view_project_files', 'Allow client to view project files'),
(7, 'view_project_comments', 'Allow clients to view project comments'),
(8, 'comment_on_project', 'Allow clients to comment on project'),
(9, 'view_issues', 'Allow client to view project issues'),
(10, 'view_timesheets', 'Allow clients to view project timesheet'),
(11, 'show_project_comments', 'Allow clients to view project comments'),
(12, 'view_milestones', 'Allow client to view milestones'),
(13, 'view_costs', 'Allow client to view project costs'),
(14, 'view_expenses', 'Allow client to view project expenses');

-- --------------------------------------------------------

--
-- Table structure for table `project_timer`
--

DROP TABLE IF EXISTS `project_timer`;
CREATE TABLE IF NOT EXISTS `project_timer` (
  `timer_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `timer_by` int(6) DEFAULT NULL,
  `start_timer` int(11) DEFAULT NULL,
  `stop_timer` int(11) DEFAULT NULL,
  `timer_active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`timer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78 ;



--
-- Table structure for table `quotation`
--

DROP TABLE IF EXISTS `quotation`;
CREATE TABLE IF NOT EXISTS `quotation` (
  `quote_project_id` int(11) NOT NULL AUTO_INCREMENT,
  `quote_code` varchar(30) NOT NULL,
  `quote_project_name` varchar(60) NOT NULL,
  `quote_project_date` datetime NOT NULL,
  `quote_start_date` datetime NOT NULL,
  `quote_end_date` datetime NOT NULL,
  `quote_project_cat` int(5) NOT NULL,
  `quote_project_sub_cat` int(5) NOT NULL,
  `quote_project_skills` text NOT NULL,
  `quote_project_desc` text NOT NULL,
  `quote_project_examples` text NOT NULL,
  `quote_project_feature` text NOT NULL,
  `quote_project_budget` decimal(11,2) NOT NULL,
  `quote_project_duration` int(5) NOT NULL,
  `quote_project_status` int(5) NOT NULL,
  `quote_project_assign` int(11) NOT NULL,
  `estimate_reference` varchar(30) NOT NULL,
  `estimated` tinyint(1) NOT NULL DEFAULT '0',
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`quote_project_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `quote_details`
--

DROP TABLE IF EXISTS `quote_details`;
CREATE TABLE IF NOT EXISTS `quote_details` (
  `details_id` int(11) NOT NULL,
  `item_id` varchar(45) DEFAULT NULL,
  `item_name` varchar(45) DEFAULT NULL,
  `item_description` varchar(45) DEFAULT NULL,
  `details_date` varchar(45) DEFAULT NULL,
  `details_hours` varchar(45) DEFAULT NULL,
  `details_rate` varchar(45) DEFAULT NULL,
  `quote_id` int(11) NOT NULL,
  PRIMARY KEY (`details_id`),
  KEY `fk_quote_details_quotation1_idx` (`quote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quote_status`
--

DROP TABLE IF EXISTS `quote_status`;
CREATE TABLE IF NOT EXISTS `quote_status` (
  `status_id` int(6) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `quote_status`
--

INSERT INTO `quote_status` (`status_id`, `status_name`) VALUES
(1, 'PENDING'),
(2, 'REVIEWED'),
(3, 'ACCEPTED'),
(4, 'DECLINED'),
(5, 'ESTIMATED');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) DEFAULT NULL,
  `role_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`, `role_active`) VALUES
(1, 'admin', 1),
(2, 'staff', 1),
(3, 'client', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_2checkout`
--

DROP TABLE IF EXISTS `settings_2checkout`;
CREATE TABLE IF NOT EXISTS `settings_2checkout` (
  `setting_id` int(6) NOT NULL AUTO_INCREMENT,
  `checkout_seller_id` varchar(45) DEFAULT NULL,
  `checkout_private_key` varchar(60) DEFAULT NULL,
  `checkout_public_key` varchar(60) DEFAULT NULL,
  `checkout_active` tinyint(4) DEFAULT NULL,
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `fk_settings_paypal_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `settings_2checkout`
--

INSERT INTO `settings_2checkout` (`setting_id`, `checkout_seller_id`, `checkout_private_key`, `checkout_public_key`, `checkout_active`, `company_id`) VALUES
(2, '', '', '', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_bank`
--

DROP TABLE IF EXISTS `settings_bank`;
CREATE TABLE IF NOT EXISTS `settings_bank` (
  `setting_id` int(6) NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(45) DEFAULT NULL,
  `bank_branch` varchar(45) DEFAULT NULL,
  `bank_branch_code` varchar(45) DEFAULT NULL,
  `bank_acc` varchar(45) DEFAULT NULL,
  `bank_reference` varchar(45) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `fk_settings_bank_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings_bank`
--

INSERT INTO `settings_bank` (`setting_id`, `bank_name`, `bank_branch`, `bank_branch_code`, `bank_acc`, `bank_reference`, `active`, `company_id`) VALUES
(1, '', '', '0000', '000', '', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_bitcoin`
--

DROP TABLE IF EXISTS `settings_bitcoin`;
CREATE TABLE IF NOT EXISTS `settings_bitcoin` (
  `setting_id` int(6) NOT NULL AUTO_INCREMENT,
  `bitcoin_root_url` varchar(45) DEFAULT NULL,
  `bitcoin_root_receive` varchar(45) DEFAULT NULL,
  `bitcoin_secret_key` varchar(45) DEFAULT NULL,
  `bitcoin_address` varchar(45) DEFAULT NULL,
  `bitcoin_api_key` varchar(45) DEFAULT NULL,
  `bitcoin_success` varchar(80) NOT NULL,
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `fk_settings_paypal_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `settings_bitcoin`
--

INSERT INTO `settings_bitcoin` (`setting_id`, `bitcoin_root_url`, `bitcoin_root_receive`, `bitcoin_secret_key`, `bitcoin_address`, `bitcoin_api_key`, `bitcoin_success`, `company_id`) VALUES
(2, 'https://blockchain.info/', 'https://api.blockchain.info/', '', '', '', 'bitcoin/success', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_braintree`
--

DROP TABLE IF EXISTS `settings_braintree`;
CREATE TABLE IF NOT EXISTS `settings_braintree` (
  `setting_id` int(6) NOT NULL AUTO_INCREMENT,
  `braintree_merchant_id` varchar(45) DEFAULT NULL,
  `braintree_private_key` varchar(45) DEFAULT NULL,
  `braintree_public_key` varchar(45) DEFAULT NULL,
  `braintree_active` tinyint(4) DEFAULT NULL,
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `fk_settings_paypal_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `settings_braintree`
--

INSERT INTO `settings_braintree` (`setting_id`, `braintree_merchant_id`, `braintree_private_key`, `braintree_public_key`, `braintree_active`, `company_id`) VALUES
(2, '', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_company`
--

DROP TABLE IF EXISTS `settings_company`;
CREATE TABLE IF NOT EXISTS `settings_company` (
  `setting_id` int(6) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(45) NOT NULL,
  `company_owner` varchar(120) NOT NULL,
  `company_address_1` varchar(45) DEFAULT NULL,
  `company_address_2` varchar(45) DEFAULT NULL,
  `company_address_3` varchar(45) DEFAULT NULL,
  `company_telephone` varchar(45) DEFAULT NULL,
  `company_tax_ref` int(120) NOT NULL,
  `company_reg_no` varchar(60) NOT NULL,
  `company_vat_no` varchar(30) NOT NULL,
  `company_slogan` varchar(60) NOT NULL,
  `company_logo` varchar(45) DEFAULT NULL,
  `company_email` varchar(45) DEFAULT NULL,
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `fk_settings_company_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings_company`
--

INSERT INTO `settings_company` (`setting_id`, `company_name`, `company_owner`, `company_address_1`, `company_address_2`, `company_address_3`, `company_telephone`, `company_tax_ref`, `company_reg_no`, `company_vat_no`, `company_slogan`, `company_logo`, `company_email`, `company_id`) VALUES
(1, '', '', '', '', '', '', 0, '', '', '', 'company_logo.png', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_currency`
--

DROP TABLE IF EXISTS `settings_currency`;
CREATE TABLE IF NOT EXISTS `settings_currency` (
  `currency_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `iso_code` varchar(3) NOT NULL DEFAULT '0',
  `iso_code_num` varchar(3) NOT NULL DEFAULT '0',
  `curreny_symbol` varchar(8) NOT NULL,
  `blank` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `curreny_format` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `decimals` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `conversion_rate` decimal(13,6) NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `settings_currency`
--

INSERT INTO `settings_currency` (`currency_id`, `name`, `iso_code`, `iso_code_num`, `curreny_symbol`, `blank`, `curreny_format`, `decimals`, `conversion_rate`, `deleted`, `active`, `company_id`) VALUES
(1, 'Euro', 'EUR', '978', '€', 1, 2, 1, '1.000000', 0, 1, 1),
(2, 'Dollar', 'USD', '840', '$', 0, 1, 1, '1.312200', 0, 1, 1),
(3, 'Rand', 'ZAR', '', 'R', 0, 0, 1, '1.000000', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_database`
--

DROP TABLE IF EXISTS `settings_database`;
CREATE TABLE IF NOT EXISTS `settings_database` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(60) NOT NULL,
  `format` varchar(10) NOT NULL,
  `upload_path` varchar(120) NOT NULL,
  `upload_date` datetime NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings_email`
--

DROP TABLE IF EXISTS `settings_email`;
CREATE TABLE IF NOT EXISTS `settings_email` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `mail_protocol` varchar(45) DEFAULT NULL,
  `smtp_encryption` varchar(60) NOT NULL,
  `mail_type` varchar(60) NOT NULL,
  `mail_parameters` varchar(45) DEFAULT NULL,
  `smtp_servername` varchar(60) NOT NULL,
  `smtp_hostname` varchar(45) DEFAULT NULL,
  `smtp_username` varchar(45) DEFAULT NULL,
  `smtp_password` varchar(45) DEFAULT NULL,
  `smtp_port` varchar(45) DEFAULT NULL,
  `smtp_tmeout` varchar(45) DEFAULT NULL,
  `mail_outgoing` varchar(120) NOT NULL,
  `test_email` varchar(60) NOT NULL,
  `is_test` tinyint(1) NOT NULL DEFAULT '0',
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `fk_settings_email_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings_email`
--

INSERT INTO `settings_email` (`setting_id`, `mail_protocol`, `smtp_encryption`, `mail_type`, `mail_parameters`, `smtp_servername`, `smtp_hostname`, `smtp_username`, `smtp_password`, `smtp_port`, `smtp_tmeout`, `mail_outgoing`, `test_email`, `is_test`, `company_id`) VALUES
(1, 'php_mail', 'tls', 'text', NULL, 'gmail', '', '', '', '25', NULL, 'zestpro.zestpro.com', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_email_template`
--

DROP TABLE IF EXISTS `settings_email_template`;
CREATE TABLE IF NOT EXISTS `settings_email_template` (
  `template_id` int(6) NOT NULL AUTO_INCREMENT,
  `template_group` varchar(60) NOT NULL,
  `template_email` varchar(60) NOT NULL,
  `template_title` varchar(45) DEFAULT NULL,
  `template_body` text,
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `fk_settings_email_template_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `settings_email_template`
--

INSERT INTO `settings_email_template` (`template_id`, `template_group`, `template_email`, `template_title`, `template_body`, `company_id`) VALUES
(1, 'account', 'registration', 'Registration successful', '                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <div style="height: 7px; background-color: #535353;"></div><div style="background-color:#E8E8E8; margin:0px; padding:55px 20px 40px 20px; font-family:Open Sans, Helvetica, sans-serif; font-size:12px; color:#535353;"><div style="text-align:center; font-size:24px; font-weight:bold; color:#535353;">Welcome to {SITE_NAME}</div><div style="border-radius: 5px 5px 5px 5px; padding:20px; margin-top:45px; background-color:#FFFFFF; font-family:Open Sans, Helvetica, sans-serif; font-size:13px;">\r\nHi, {CLIENT_NAME},<br><br>\r\nThank you for joining {SITE_NAME}. Your company has just been added to {SITE_NAME}.\r\n\r\n<br><br>We listed your sign in details below, remember to keep them safe.<br><br>To open your {SITE_NAME} homepage, please follow this link:<br><br><big><b><a href="{SITE_URL}">{SITE_NAME} Account!</a></b></big><br><br>\r\n\r\n<table cellpadding="8" style="border: 1px solid #DDDDDD; border-collapse: collapse; border-spacing: 0;font-size:13px;" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;" width="150">Username</td>\r\n			<td style="border: 1px solid #DDDDDD;" width="276">{USERNAME}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;">Password</td>\r\n			<td style="border: 1px solid #DDDDDD;">{PASSWORD}</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n\r\n<br>Warm Regards,<br><br>{SITE_NAME} Team.<br><br></div></div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ', 1),
(12, 'project', 'project_complete', 'Project Completed', '<div style="height: 7px; background-color: #535353;"></div>\r\n<div style="background-color:#E8E8E8; margin:0px; padding:55px 20px 40px 20px; font-family:Open Sans, Helvetica, sans-serif; font-size:12px; color:#535353;"><div style="text-align:center; font-size:24px; font-weight:bold; color:#535353;">Project Completed</div>\r\n<div style="border-radius: 5px 5px 5px 5px; padding:20px; margin-top:45px; background-color:#FFFFFF; font-family:Open Sans, Helvetica, sans-serif; font-size:13px;"><p>Hi {CLIENT_NAME}</p>\r\n<p>Project : {PROJECT_TITLE} - {PROJECT_CODE} has been completed. </p>\r\n<p>You can view the project by logging into your portal Account.</p>\r\n<big><b><a href="{PROJECT_URL}">View Project</a></b></big><br><br>--------------------------<br>Project Overview:<br>Hours Logged # :  {PROJECT_HOURS} hours<br>Project Cost : {PROJECT_COST}<br>Regards<br>The {SITE_NAME} Team</div>\r\n</div>', 1),
(15, 'project', 'project_assigned', 'Project assigned', '                                        <div style="height: 7px; background-color: #535353;"></div>\r\n<div style="background-color:#E8E8E8; margin:0px; padding:55px 20px 40px 20px; font-family:Open Sans, Helvetica, sans-serif; font-size:12px; color:#535353;"><div style="text-align:center; font-size:24px; font-weight:bold; color:#535353;">Project assigned</div>\r\n<div style="border-radius: 5px 5px 5px 5px; padding:20px; margin-top:45px; background-color:#FFFFFF; font-family:Open Sans, Helvetica, sans-serif; font-size:13px;"><p>Hi there,</p>\r\n<p>A new project ( {PROJECT_TITLE} ) has been assigned to you by {ASSIGNED_BY}.</p>\r\n<br><br>\r\n<table cellpadding="8" style="border: 1px solid #DDDDDD; border-collapse: collapse; border-spacing: 0;font-size:13px;" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;" width="150">Project ID</td>\r\n			<td style="border: 1px solid #DDDDDD;" width="276">{PROJECT_NO}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;" width="150">Client Name</td>\r\n			<td style="border: 1px solid #DDDDDD;" width="276">{CLIENT_NAME}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;">Date Created</td>\r\n			<td style="border: 1px solid #DDDDDD;">{DATE_CREATED}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;">Completion Date</td>\r\n			<td style="border: 1px solid #DDDDDD;">{DATE_DUE}</td>\r\n		</tr>\r\n	\r\n	</tbody>\r\n</table>\r\n<br>\r\n<p>You can view this project by logging in to the portal using the link below.</p>\r\n-----------------------------------<br><big><b><a href="{PROJECT_URL}">View Project</a></b></big><br><br>Regards<br>The {SITE_NAME} Team</div>\r\n</div>                                    ', 1),
(17, 'invoice', 'invoice_message', 'New Invoice', '<div style="height: 7px; background-color: #535353;"></div><div style="background-color:#E8E8E8; margin:0px; padding:55px 20px 40px 20px; font-family:Open Sans, Helvetica, sans-serif; font-size:12px; color:#535353;"><div style="text-align:center; font-size:24px; font-weight:bold; color:#535353;">INVOICE {REF}</div><div style="border-radius: 5px 5px 5px 5px; padding:20px; margin-top:45px; background-color:#FFFFFF; font-family:Open Sans, Helvetica, sans-serif; font-size:13px;"><span class="style1"><span style="font-weight:bold;">Hello {CLIENT}</span></span><br><br>Here is the invoice of {CURRENCY} {AMOUNT}.<br><br>You can view the invoice online at:<br><big><b><a href="{INVOICE_LINK}">View Invoice</a></b></big><br><br>Best Regards<br><br>The {SITE_NAME} Team</div></div>', 1),
(18, 'invoice', 'invoice_reminder', 'Invoice Reminder', '                                        <div style="height: 7px; background-color: #535353;"></div>\r\n<div style="background-color:#E8E8E8; margin:0px; padding:55px 20px 40px 20px; font-family:Open Sans, Helvetica, sans-serif; font-size:12px; color:#535353;"><div style="text-align:center; font-size:24px; font-weight:bold; color:#535353;">Invoice Reminder - {REF}</div>\r\n<div style="border-radius: 5px 5px 5px 5px; padding:20px; margin-top:45px; background-color:#FFFFFF; font-family:Open Sans, Helvetica, sans-serif; font-size:13px;"><p>Hello {CLIENT}</p>\r\n<br><p>This is a friendly reminder to pay your invoice of {CURRENCY} {AMOUNT}<br>You can view the invoice online at:<br><big><b><a href="{INVOICE_LINK}">View Invoice</a></b></big><br><br>Best Regards,<br>The {SITE_NAME} Team</p>\r\n</div>\r\n</div>                                    ', 1),
(20, 'estimate', 'estimate_mail', 'New Estimate', '                                        <div style="height:7px; background-color:#535353"> </div>\r\n\r\n<div style="background-color:#f5f5f5; margin:0px; padding:55px 20px 40px 20px; font-family:Helvetica, sans-serif; font-size:13px; color:#535353;">\r\n<div style="text-align:center; font-size:34px; font-weight:bold; color:#535353;">ESTIMATE  {ESTIMATE_REF}</div>;\r\n\r\n<div style="border-radius: 5px 5px 5px 5px; padding:20px; margin-top:45px; background-color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:13px;"><strong>Hi {CLIENT}</strong><br>\r\n<br>\r\n<p>Thanks for your business inquiry. </p>.<br>\r\n<br>\r\nPlease find estimate {ESTIMATE_REF} attached to this email:<br>\r\n \r\n<table cellpadding="8" style="border: 1px solid #DDDDDD; border-collapse: collapse; border-spacing: 0;font-size:13px;" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;" width="150">ESTIMATE #</td>\r\n			<td style="border: 1px solid #DDDDDD;" width="276">{ESTIMATE_REF}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;">AMOUNT</td>\r\n			<td style="border: 1px solid #DDDDDD;"><strong>{CURRENCY} {AMOUNT}</strong></td>\r\n		</tr>\r\n		\r\n	</tbody>\r\n</table>\r\n<br>\r\n<br> <br>You can view the estimate online at:<br> <big><b><a href="{ESTIMATE_LINK}">View Estimate</a></b></big><br><br>  Best Regards,<br> The {SITE_NAME} Team</div>\r\n</div>                                                    </div>', 1),
(21, 'ticket', 'ticket_staff_email', 'New Ticket [TICKET_CODE]', '                                        <div style="height: 7px; background-color: #535353;"></div>\r\n<div style="background-color:#E8E8E8; margin:0px; padding:55px 20px 40px 20px; font-family:Open Sans, Helvetica, sans-serif; font-size:12px; color:#535353;"><div style="text-align:center; font-size:24px; font-weight:bold; color:#535353;">New Ticket</div>\r\n<div style="border-radius: 5px 5px 5px 5px; padding:20px; margin-top:45px; background-color:#FFFFFF; font-family:Open Sans, Helvetica, sans-serif; font-size:13px;"><p>Ticket #{TICKET_CODE} has been created by the client.</p>\r\n<p>You may view the ticket by clicking on the following link <br><br>Subject : {SUBJECT}<br>  Client Email : {REPORTER_EMAIL}<br><br> <big><b><a href="{TICKET_LINK}">View Ticket</a></b></big> <br><br>Regards<br><br>{SITE_NAME}</p>\r\n</div>\r\n</div>                                    ', 1),
(22, 'ticket', 'ticket_client_email', 'Ticket [TICKET_CODE] Opened', '                                                                                                                        <div style="height: 7px; background-color: #535353;"></div>\r\n<div style="background-color:#E8E8E8; margin:0px; padding:55px 20px 40px 20px; font-family:Open Sans, Helvetica, sans-serif; font-size:12px; color:#535353;"><div style="text-align:center; font-size:24px; font-weight:bold; color:#535353;">Ticket Opened</div>\r\n<div style="border-radius: 5px 5px 5px 5px; padding:20px; margin-top:45px; background-color:#FFFFFF; font-family:Open Sans, Helvetica, sans-serif; font-size:13px;"><p>Hello {CLIENT_EMAIL},<br><br></p>\r\n<br><p>A new support ticket has been opened for your request</p><br>\r\n<p>The details of your ticket are shown below.<br><br>Ticket #{TICKET_CODE}<br>Subject: {SUBJECT}<br>Status : Open<br><br>Click on the below link to see the ticket details and post additional comments.<br><br><big><b><a href="{TICKET_LINK}">View Ticket</a></b></big><br><br>Regards<br><br>The {SITE_NAME} Team<br></p>\r\n</div>\r\n</div>                                                                                                            ', 1),
(23, 'ticket', 'ticket_reply_email', 'Ticket [TICKET_CODE] Response', '                                        <div style="height: 7px; background-color: #535353;"></div>\r\n<div style="background-color:#E8E8E8; margin:0px; padding:55px 20px 40px 20px; font-family:Open Sans, Helvetica, sans-serif; font-size:12px; color:#535353;"><div style="text-align:center; font-size:24px; font-weight:bold; color:#535353;">Ticket Response</div>\r\n<div style="border-radius: 5px 5px 5px 5px; padding:20px; margin-top:45px; background-color:#FFFFFF; font-family:Open Sans, Helvetica, sans-serif; font-size:13px;"><p>A new response has been added to Ticket #{TICKET_CODE}<br><br> Ticket : #{TICKET_CODE} <br>Subject : {SUBJECT} <br>Status : {TICKET_STATUS} <br><br></p>\r\nTo see the response and post additional comments, click on the link below.<br><br>         <big><b><a href="{TICKET_LINK}">View Reply</a> </b></big><br><br>          Note: Do not reply to this email as this email is not monitored.<br><br>     Regards<br>The {SITE_NAME} Team<br></div>\r\n</div>                                    ', 1),
(30, 'project', 'project_new', 'New Project', '                                                                                                                                                                                                                                                <div style="height:7px; background-color:#535353"> </div>\r\n\r\n<div style="background-color:#f5f5f5; margin:0px; padding:55px 20px 40px 20px; font-family:Helvetica, sans-serif; font-size:13px; color:#535353;">\r\n<div style="text-align:center; font-size:34px; font-weight:bold; color:#535353;">NEW PROJECT</div>\r\n\r\n<div style="text-align:center; font-size:24px; font-weight:bold; color:#535353;">{PROJECT_TITLE}</div>\r\n\r\n<div style="border-radius: 5px 5px 5px 5px; padding:20px; margin-top:45px; background-color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:13px;">\r\n\r\n<br>\r\nA new project has been added to the {SITE_NAME}<br>\r\n \r\n<table cellpadding="8" style="border: 1px solid #DDDDDD; border-collapse: collapse; border-spacing: 0;font-size:13px;" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;" width="150">Project ID</td>\r\n			<td style="border: 1px solid #DDDDDD;" width="276">{PROJECT_NO}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;" width="150">Client Name</td>\r\n			<td style="border: 1px solid #DDDDDD;" width="276">{CLIENT_NAME}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;">Date Created</td>\r\n			<td style="border: 1px solid #DDDDDD;">{DATE_CREATED}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;">Completion Date</td>\r\n			<td style="border: 1px solid #DDDDDD;">{DATE_DUE}</td>\r\n		</tr>\r\n	\r\n	</tbody>\r\n</table>\r\n \r\n\r\n<div style=" border:#CCCCCC solid 1px; padding:8px;"><strong>{SITE_NAME}  Link </strong><span style="font-size:14px;"><a href="{PROJECT_URL}">{PROJECT_ID}</a></span></div>\r\n<br>\r\n\r\n</div>\r\n                                                                                                                                         </div>                                                                                                            ', 1),
(31, 'estimate', 'estimate_assigned', 'Assigned', '                                                                                                                                                             <div style="height: 7px; background-color: #535353;"></div>\r\n<div style="background-color:#E8E8E8; margin:0px; padding:55px 20px 40px 20px; font-family:Open Sans, Helvetica, sans-serif; font-size:12px; color:#535353;"><div style="text-align:center; font-size:24px; font-weight:bold; color:#535353;">Estimate assigned</div>\r\n<div style="border-radius: 5px 5px 5px 5px; padding:20px; margin-top:45px; background-color:#FFFFFF; font-family:Open Sans, Helvetica, sans-serif; font-size:13px;"><p>Hi there,</p>\r\n<p>Estimate ( {ESTIMATE_NO} ) has been assigned to you by {ASSIGNED_BY}.</p>\r\n<br><br>\r\n<table cellpadding="8" style="border: 1px solid #DDDDDD; border-collapse: collapse; border-spacing: 0;font-size:13px;" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;" width="150">Estimate ID</td>\r\n			<td style="border: 1px solid #DDDDDD;" width="276">{ESTIMATE_NO}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;" width="150">Client Name</td>\r\n			<td style="border: 1px solid #DDDDDD;" width="276">{CLIENT_NAME}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;">Date Created</td>\r\n			<td style="border: 1px solid #DDDDDD;">{DATE_CREATED}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;">Completion Date</td>\r\n			<td style="border: 1px solid #DDDDDD;">{DATE_DUE}</td>\r\n		</tr>\r\n	\r\n	</tbody>\r\n</table>\r\n<br>\r\n<p>You can view this project by logging in to the portal using the link below.</p>\r\n-----------------------------------<br><big><b><a href="{ESTIMATE_URL}">View Project</a></b></big><br><br>Regards<br>The {SITE_NAME} Team</div>\r\n</div>                                                                                                                                           ', 1),
(32, 'estimate', 'estimate_status', NULL, NULL, 1),
(33, 'estimate', 'estimate_converted', 'Converted', NULL, 1),
(34, 'quotation', 'quotation_mail', 'New ', '                                                                                                                                                   <div style="height:7px; background-color:#535353"> </div>\r\n\r\n<div style="background-color:#f5f5f5; margin:0px; padding:55px 20px 40px 20px; font-family:Helvetica, sans-serif; font-size:13px; color:#535353;">\r\n<div style="text-align:center; font-size:34px; font-weight:bold; color:#535353;">QUOTATION</div>\r\n\r\n<div style="border-radius: 5px 5px 5px 5px; padding:20px; margin-top:45px; background-color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:13px;"><strong>Hello {CLIENT}</strong><br>\r\n<br>\r\nThank you for you for filling in our Quotation Request Form.<br>\r\n<br>\r\nPlease find below are our quotation:<br>\r\n \r\n<table cellpadding="8" style="border: 1px solid #DDDDDD; border-collapse: collapse; border-spacing: 0;font-size:13px;" width="100%">\r\n	<tbody>\r\n	<tr>\r\n			<td style="border: 1px solid #DDDDDD;" width="150">Quotation ID</td>\r\n			<td style="border: 1px solid #DDDDDD;" width="276">{QUOTATION_NO}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;" width="150">Client Name</td>\r\n			<td style="border: 1px solid #DDDDDD;" width="276">{CLIENT_NAME}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;">Date Created</td>\r\n			<td style="border: 1px solid #DDDDDD;">{DATE_CREATED}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;">Completion Date</td>\r\n			<td style="border: 1px solid #DDDDDD;">{DATE_DUE}</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<br>\r\nThank you and we look forward to working with you.<br>\r\n<br>\r\nThe {SITE_NAME} Team</div>\r\n</div>\r\n                                                                                 ', 1),
(35, 'quotation', 'quotation_assigned', 'Assigned', '                                                                                                                                                             <div style="height: 7px; background-color: #535353;"></div>\r\n<div style="background-color:#E8E8E8; margin:0px; padding:55px 20px 40px 20px; font-family:Open Sans, Helvetica, sans-serif; font-size:12px; color:#535353;"><div style="text-align:center; font-size:24px; font-weight:bold; color:#535353;">Quotation assigned</div>\r\n<div style="border-radius: 5px 5px 5px 5px; padding:20px; margin-top:45px; background-color:#FFFFFF; font-family:Open Sans, Helvetica, sans-serif; font-size:13px;"><p>Hi there,</p>\r\n<p>Quote ( {QUOTATION_NO} ) has been assigned to you by {ASSIGNED_BY}.</p>\r\n<br><br>\r\n<table cellpadding="8" style="border: 1px solid #DDDDDD; border-collapse: collapse; border-spacing: 0;font-size:13px;" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;" width="150">Quotation ID</td>\r\n			<td style="border: 1px solid #DDDDDD;" width="276">{QUOTATION_NO}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;" width="150">Client Name</td>\r\n			<td style="border: 1px solid #DDDDDD;" width="276">{CLIENT_NAME}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;">Date Created</td>\r\n			<td style="border: 1px solid #DDDDDD;">{DATE_CREATED}</td>\r\n		</tr>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;">Completion Date</td>\r\n			<td style="border: 1px solid #DDDDDD;">{DATE_DUE}</td>\r\n		</tr>\r\n	\r\n	</tbody>\r\n</table>\r\n<br>\r\n<p>You can view this project by logging in to the portal using the link below.</p>\r\n-----------------------------------<br><big><b><a href="{QUOTATION_URL}">View Project</a></b></big><br><br>Regards<br>The {SITE_NAME} Team</div>\r\n</div>                      ', 1),
(36, 'quotation', 'quotation_status', 'Status', NULL, 1),
(37, 'quotation', 'quotation_estimated', 'Estimated', NULL, 1),
(38, 'invoice', 'payment_message', 'Payment Notification', '                                                                                                                                                                                                                                            <div style="height:7px; background-color:#535353"> </div>\r\n\r\n<div style="background-color:#f5f5f5; margin:0px; padding:55px 20px 40px 20px; font-family:Helvetica, sans-serif; font-size:13px; color:#535353;">\r\n<div style="text-align:center; font-size:34px; font-weight:bold; color:#535353;">PAYMENT - INVOICE {INVOICE_REF}</div>;\r\n\r\n<div style="border-radius: 5px 5px 5px 5px; padding:20px; margin-top:45px; background-color:#FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:13px;"><strong>DEAR {CLIENT}</strong><br>\r\n\r\n<br>\r\nWe have received your payment:<br>\r\n \r\n<table cellpadding="8" style="border: 1px solid #DDDDDD; border-collapse: collapse; border-spacing: 0;font-size:13px;" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td style="border: 1px solid #DDDDDD;">AMOUNT</td>\r\n			<td style="border: 1px solid #DDDDDD;"><strong>{CURRENCY} {AMOUNT}</strong></td>\r\n		</tr>\r\n		\r\n	</tbody>\r\n</table>\r\n<br><br>\r\n<p>Thank you for your Payment and business. We look forward to working with you again</p>\r\n<br><br>  Best Regards,<br> The {SITE_NAME} Team</div>\r\n</div>                                                                                                                                                                ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_invoice`
--

DROP TABLE IF EXISTS `settings_invoice`;
CREATE TABLE IF NOT EXISTS `settings_invoice` (
  `setting_id` int(6) NOT NULL AUTO_INCREMENT,
  `invoice_prefix` varchar(3) DEFAULT NULL,
  `invoice_terms` text,
  `invoice_logo` varchar(120) DEFAULT NULL,
  `estimate_prefix` varchar(3) DEFAULT NULL,
  `invoice_due_after` int(2) NOT NULL DEFAULT '0',
  `estimate_terms` text,
  `quotation_prefix` varchar(3) DEFAULT NULL,
  `quotation_terms` text,
  `project_prefix` varchar(3) DEFAULT NULL,
  `project_terms` text,
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `fk_settings_invoice_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings_invoice`
--

INSERT INTO `settings_invoice` (`setting_id`, `invoice_prefix`, `invoice_terms`, `invoice_logo`, `estimate_prefix`, `invoice_due_after`, `estimate_terms`, `quotation_prefix`, `quotation_terms`, `project_prefix`, `project_terms`, `company_id`) VALUES
(1, 'INV', '', 'invoice_logo.png', 'EST', 30, NULL, 'QTN', NULL, 'PRO', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_local`
--

DROP TABLE IF EXISTS `settings_local`;
CREATE TABLE IF NOT EXISTS `settings_local` (
  `setting_id` int(6) NOT NULL AUTO_INCREMENT,
  `company_country` int(6) DEFAULT NULL,
  `company_state` int(6) NOT NULL,
  `company_city` varchar(45) DEFAULT NULL,
  `company_zip` varchar(45) DEFAULT NULL,
  `company_language` varchar(45) DEFAULT NULL,
  `company_currency` int(6) DEFAULT NULL,
  `company_tax` int(6) NOT NULL,
  `company_time_zone` int(6) NOT NULL,
  `company_curreny_symbol` varchar(45) DEFAULT NULL,
  `company_company_id` int(6) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `fk_settings_local_company1_idx` (`company_company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings_local`
--

INSERT INTO `settings_local` (`setting_id`, `company_country`, `company_state`, `company_city`, `company_zip`, `company_language`, `company_currency`, `company_tax`, `company_time_zone`, `company_curreny_symbol`, `company_company_id`) VALUES
(1, 0, 0, NULL, '', '', 0, 0, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_payfast`
--

DROP TABLE IF EXISTS `settings_payfast`;
CREATE TABLE IF NOT EXISTS `settings_payfast` (
  `setting_id` int(6) NOT NULL AUTO_INCREMENT,
  `payfast_merchant_id` varchar(60) NOT NULL,
  `payfast_mail` varchar(45) DEFAULT NULL,
  `payfast_merchant_key` varchar(45) DEFAULT NULL,
  `payfast_passphrase` varchar(45) DEFAULT NULL,
  `payfast_ipn` varchar(45) DEFAULT NULL,
  `payfast_cancel` varchar(45) DEFAULT NULL,
  `payfast_success` varchar(45) DEFAULT NULL,
  `payfast_active` tinyint(4) DEFAULT NULL,
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `fk_settings_payfast_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `settings_payfast`
--

INSERT INTO `settings_payfast` (`setting_id`, `payfast_merchant_id`, `payfast_mail`, `payfast_merchant_key`, `payfast_passphrase`, `payfast_ipn`, `payfast_cancel`, `payfast_success`, `payfast_active`, `company_id`) VALUES
(2, '', '', '', '', 'payfast/p_ipn/ipn', 'payfast/cancel', 'payfast/success', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_payment_methods`
--

DROP TABLE IF EXISTS `settings_payment_methods`;
CREATE TABLE IF NOT EXISTS `settings_payment_methods` (
  `setting_id` int(6) NOT NULL AUTO_INCREMENT,
  `payment_code` int(11) DEFAULT NULL,
  `payment_name` varchar(60) NOT NULL,
  `payment_description` varchar(100) DEFAULT NULL,
  `payment_active` tinyint(1) NOT NULL DEFAULT '0',
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `fk_settings_payment_terms_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `settings_payment_methods`
--

INSERT INTO `settings_payment_methods` (`setting_id`, `payment_code`, `payment_name`, `payment_description`, `payment_active`, `company_id`) VALUES
(1, 1, 'paypal', 'Accept payments for your products via paypal ', 1, 1),
(2, 2, 'payfast', 'Accept payments for your products via payfast', 1, 1),
(3, 3, 'skrill', 'Accept payments for your products via skrill\r\n', 1, 1),
(4, 4, 'stripepay', 'Accept payments for your products via stripe', 1, 1),
(5, 5, 'checkout', 'Accept payments for your products via 2checkout', 0, 1),
(6, 6, 'bitcoin', 'Accept payments for your products via bitcoin', 0, 1),
(7, 7, 'braintree', 'Accept payments for your products via braintree', 0, 1),
(8, 8, 'banktransfer', 'Accept bank transfer payments', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_paypal`
--

DROP TABLE IF EXISTS `settings_paypal`;
CREATE TABLE IF NOT EXISTS `settings_paypal` (
  `setting_id` int(6) NOT NULL AUTO_INCREMENT,
  `paypal_mail` varchar(45) DEFAULT NULL,
  `paypal_ipn` varchar(45) DEFAULT NULL,
  `paypal_currency` varchar(45) DEFAULT NULL,
  `paypal_cancel` varchar(45) DEFAULT NULL,
  `paypal_success` varchar(45) DEFAULT NULL,
  `paypal_active` tinyint(4) DEFAULT NULL,
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `fk_settings_paypal_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings_paypal`
--

INSERT INTO `settings_paypal` (`setting_id`, `paypal_mail`, `paypal_ipn`, `paypal_currency`, `paypal_cancel`, `paypal_success`, `paypal_active`, `company_id`) VALUES
(1, '', 'paypal/p_ipn/ipn', NULL, 'paypal/cancel', 'paypal/success', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_skrill`
--

DROP TABLE IF EXISTS `settings_skrill`;
CREATE TABLE IF NOT EXISTS `settings_skrill` (
  `setting_id` int(6) NOT NULL AUTO_INCREMENT,
  `skrill_mail` varchar(45) DEFAULT NULL,
  `skrill_ipn` varchar(45) DEFAULT NULL,
  `skrill_currency` varchar(45) DEFAULT NULL,
  `skrill_cancel` varchar(45) DEFAULT NULL,
  `skrill_success` varchar(45) DEFAULT NULL,
  `skrill_active` tinyint(4) DEFAULT '0',
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `fk_settings_skrill_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `settings_skrill`
--

INSERT INTO `settings_skrill` (`setting_id`, `skrill_mail`, `skrill_ipn`, `skrill_currency`, `skrill_cancel`, `skrill_success`, `skrill_active`, `company_id`) VALUES
(2, '', 'skrill/ipn', NULL, 'skrill/ipn', 'skrill/ipn', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_stripe`
--

DROP TABLE IF EXISTS `settings_stripe`;
CREATE TABLE IF NOT EXISTS `settings_stripe` (
  `setting_id` int(6) NOT NULL AUTO_INCREMENT,
  `stripe_private_key` varchar(65) DEFAULT NULL,
  `stripe_public_key` varchar(65) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `fk_settings_stripe_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `settings_stripe`
--

INSERT INTO `settings_stripe` (`setting_id`, `stripe_private_key`, `stripe_public_key`, `is_active`, `company_id`) VALUES
(2, '', '', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_support`
--

DROP TABLE IF EXISTS `settings_support`;
CREATE TABLE IF NOT EXISTS `settings_support` (
  `department_id` int(6) NOT NULL AUTO_INCREMENT,
  `department_desc` varchar(45) DEFAULT NULL,
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`department_id`),
  KEY `fk_settings_support_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `settings_support`
--

INSERT INTO `settings_support` (`department_id`, `department_desc`, `company_id`) VALUES
(1, 'Support', 1),
(2, 'General', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_system`
--

DROP TABLE IF EXISTS `settings_system`;
CREATE TABLE IF NOT EXISTS `settings_system` (
  `settings_id` int(6) NOT NULL AUTO_INCREMENT,
  `max_file_size` varchar(45) DEFAULT NULL,
  `allowed_files` varchar(45) DEFAULT NULL,
  `currency_symbol_placement` varchar(30) NOT NULL,
  `company_thousand_separator` varchar(10) NOT NULL,
  `currency_decimal_point` varchar(10) NOT NULL,
  `cron_key` varchar(120) NOT NULL,
  `is_cron_active` tinyint(1) NOT NULL DEFAULT '0',
  `create_recur` tinyint(1) NOT NULL DEFAULT '0',
  `email_recur` tinyint(1) NOT NULL DEFAULT '0',
  `email_overdue` tinyint(1) DEFAULT '0',
  `auto_db_backup` tinyint(1) NOT NULL DEFAULT '0',
  `company_google_analytics` text NOT NULL,
  `is_demo` tinyint(4) NOT NULL DEFAULT '0',
  `client_registration` tinyint(1) NOT NULL DEFAULT '0',
  `purchase_code` text NOT NULL,
  `gcal_api_key` text CHARACTER SET utf8 NOT NULL,
  `gcal_id` text CHARACTER SET utf8 NOT NULL,
  `build` text NOT NULL,
  `last_check` text NOT NULL,
  `version` varchar(11) NOT NULL,
  `company_company_id` int(6) NOT NULL,
  PRIMARY KEY (`settings_id`),
  KEY `fk_settings_system_company1_idx` (`company_company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings_system`
--

INSERT INTO `settings_system` (`settings_id`, `max_file_size`, `allowed_files`, `currency_symbol_placement`, `company_thousand_separator`, `currency_decimal_point`, `cron_key`, `is_cron_active`, `create_recur`, `email_recur`, `email_overdue`, `auto_db_backup`, `company_google_analytics`, `is_demo`, `client_registration`, `purchase_code`, `gcal_api_key`, `gcal_id`, `build`, `last_check`, `version`, `company_company_id`) VALUES
(1, '80000', 'gif|jpg|png|pdf|doc|txt|docx|xls|zip|rar', 'before', ',', '.', '', 1, 1, 1, 1, 1, '', 0, 1, 'xxxxxxxxxxxx', 'xx', 'xx', '1234', '1473108366', '1.0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_systypes`
--

DROP TABLE IF EXISTS `settings_systypes`;
CREATE TABLE IF NOT EXISTS `settings_systypes` (
  `typeid` smallint(6) NOT NULL DEFAULT '0',
  `typename` char(50) NOT NULL DEFAULT '',
  `prefix` varchar(3) NOT NULL,
  `typeno` varchar(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`typeid`,`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings_systypes`
--

INSERT INTO `settings_systypes` (`typeid`, `typename`, `prefix`, `typeno`, `company_id`) VALUES
(10, 'invoice', 'INV', '', 1),
(11, 'estimate', 'EST', '', 1),
(12, 'quotation', 'QTN', '', 1),
(13, 'project', 'PRO', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_tax`
--

DROP TABLE IF EXISTS `settings_tax`;
CREATE TABLE IF NOT EXISTS `settings_tax` (
  `tax_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `rate` decimal(10,3) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tax_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `settings_tax`
--

INSERT INTO `settings_tax` (`tax_id`, `name`, `rate`, `active`, `deleted`, `company_id`) VALUES
(1, 'asdasd', '10.500', 1, 0, 1),
(2, 'dsad', '14.600', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings_theme`
--

DROP TABLE IF EXISTS `settings_theme`;
CREATE TABLE IF NOT EXISTS `settings_theme` (
  `setting_id` int(6) NOT NULL AUTO_INCREMENT,
  `site_title` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `site_favicon` varchar(120) CHARACTER SET latin1 DEFAULT NULL,
  `profile_background` varchar(120) CHARACTER SET latin1 DEFAULT NULL,
  `login_background` varchar(120) CHARACTER SET latin1 DEFAULT NULL,
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings_theme`
--

INSERT INTO `settings_theme` (`setting_id`, `site_title`, `site_favicon`, `profile_background`, `login_background`, `company_id`) VALUES
(1, '', 'site_favicon.png', 'profile_background.png', 'login_background.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE IF NOT EXISTS `status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(25) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status_id`, `status_name`) VALUES
(1, 'PENDING'),
(2, 'ACCEPTED'),
(3, 'REVIEWED'),
(4, 'DECLINED');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

DROP TABLE IF EXISTS `sub_categories`;
CREATE TABLE IF NOT EXISTS `sub_categories` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_name` varchar(255) NOT NULL,
  `cat_id` int(11) NOT NULL,
  PRIMARY KEY (`sub_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`sub_id`, `sub_name`, `cat_id`) VALUES
(1, 'Build a Website', 1),
(2, 'Build an Online Store', 1),
(3, 'Get Traffic to my Website', 1),
(4, 'Write some Software', 1),
(5, 'Convert a Template to a Website', 1),
(6, 'Create a Wordpress Template', 1),
(7, 'Create a Joomla Template', 1),
(8, 'Create a Drupal Template', 1),
(9, 'Develop a Mac Application', 1);

-- --------------------------------------------------------

--
-- Table structure for table `system_updates`
--

DROP TABLE IF EXISTS `system_updates`;
CREATE TABLE IF NOT EXISTS `system_updates` (
  `build` int(11) NOT NULL DEFAULT '0',
  `code` varchar(50) DEFAULT NULL,
  `tran_date` timestamp NULL DEFAULT NULL,
  `version` varchar(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `filename` varchar(255) DEFAULT NULL,
  `installed` int(11) DEFAULT '0',
  `is_downloaded` tinyint(1) NOT NULL DEFAULT '0',
  `sql_scritp` text,
  PRIMARY KEY (`build`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE IF NOT EXISTS `tasks` (
  `task_id` int(6) NOT NULL AUTO_INCREMENT,
  `project_id` int(6) NOT NULL,
  `task_visible` tinyint(4) DEFAULT '1',
  `task_description` text,
  `task_requirements` text,
  `task_use_checklist` tinyint(1) NOT NULL DEFAULT '0',
  `task_progress` int(6) NOT NULL,
  `task_name` varchar(45) DEFAULT NULL,
  `task_code` varchar(20) DEFAULT NULL,
  `task_level` int(6) NOT NULL,
  `task_milestone_id` int(11) NOT NULL,
  `task_depends` varchar(20) NOT NULL,
  `task_status` varchar(30) NOT NULL,
  `task_start` varchar(64) DEFAULT NULL,
  `task_priority` int(6) NOT NULL,
  `task_duration` int(6) NOT NULL,
  `task_est_hours` int(11) NOT NULL,
  `task_end` varchar(64) DEFAULT NULL,
  `task_startIsMilestone` tinyint(4) NOT NULL,
  `task_endIsMilestone` tinyint(4) NOT NULL,
  `task_collapsed` tinyint(4) NOT NULL,
  `task_assigs` varchar(255) DEFAULT NULL,
  `task_earlyStart` int(11) NOT NULL,
  `task_earlyFinish` int(11) NOT NULL,
  `task_latestStart` int(11) NOT NULL,
  `task_latestFinish` int(11) NOT NULL,
  `task_criticalCost` int(11) NOT NULL,
  `task_isCritical` tinyint(4) NOT NULL,
  `task_hasChild` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;


--
-- Table structure for table `task_assignment`
--

DROP TABLE IF EXISTS `task_assignment`;
CREATE TABLE IF NOT EXISTS `task_assignment` (
  `task_asgn_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `task_id` int(6) NOT NULL,
  `user_id` int(6) NOT NULL,
  PRIMARY KEY (`task_asgn_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;


--
-- Table structure for table `task_messages`
--

DROP TABLE IF EXISTS `task_messages`;
CREATE TABLE IF NOT EXISTS `task_messages` (
  `message_id` int(6) NOT NULL AUTO_INCREMENT,
  `message_date` datetime NOT NULL,
  `message_text` text,
  `task_id` int(11) NOT NULL,
  `message_by_id` int(6) DEFAULT NULL,
  PRIMARY KEY (`message_id`),
  KEY `task_messages_fk1` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `task_message_replies`
--

DROP TABLE IF EXISTS `task_message_replies`;
CREATE TABLE IF NOT EXISTS `task_message_replies` (
  `reply_id` int(6) NOT NULL AUTO_INCREMENT,
  `message_id` int(6) NOT NULL,
  `reply_message` text,
  `replied_by` int(6) DEFAULT NULL,
  `reply_date` datetime NOT NULL,
  PRIMARY KEY (`reply_id`),
  KEY `task_message_replies_fk1` (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `task_status`
--

DROP TABLE IF EXISTS `task_status`;
CREATE TABLE IF NOT EXISTS `task_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(25) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `task_status`
--

INSERT INTO `task_status` (`status_id`, `status_name`) VALUES
(1, 'ACTIVE'),
(2, 'DONE'),
(3, 'FAILED'),
(4, 'SUSPENDED'),
(5, 'UNDEFINED');

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE IF NOT EXISTS `ticket` (
  `ticket_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_department` int(11) DEFAULT NULL,
  `ticket_assigned_to` int(6) DEFAULT NULL,
  `ticket_subject` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `ticket_message` longtext COLLATE utf8_unicode_ci,
  `ticket_priority` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `ticket_status` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `ticket_attachment` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `ticket_date` datetime NOT NULL,
  `ticket_user_type` int(2) NOT NULL,
  `ticket_client_id` int(6) DEFAULT NULL,
  `ticket_by` int(6) NOT NULL,
  `company_id` int(6) NOT NULL,
  PRIMARY KEY (`ticket_id`),
  KEY `fk_ticket_company1_idx` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=38 ;



--
-- Table structure for table `ticket_replies`
--

DROP TABLE IF EXISTS `ticket_replies`;
CREATE TABLE IF NOT EXISTS `ticket_replies` (
  `reply_id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) DEFAULT NULL,
  `reply_date` datetime NOT NULL,
  `reply_message` text CHARACTER SET utf8,
  `reply_user_id` int(6) DEFAULT NULL,
  `reply_user_type` int(2) DEFAULT NULL,
  `reply_attachment` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`reply_id`),
  KEY `fk_ticket_replies_ticket1_idx` (`ticket_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_status`
--

DROP TABLE IF EXISTS `ticket_status`;
CREATE TABLE IF NOT EXISTS `ticket_status` (
  `status_id` int(6) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(60) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `ticket_status`
--

INSERT INTO `ticket_status` (`status_id`, `status_name`) VALUES
(1, 'open'),
(2, 'in progress'),
(3, 'answered'),
(4, 'closed');

-- --------------------------------------------------------

--
-- Table structure for table `timer_entries`
--

DROP TABLE IF EXISTS `timer_entries`;
CREATE TABLE IF NOT EXISTS `timer_entries` (
  `timer_id` int(6) NOT NULL AUTO_INCREMENT,
  `task_id` int(6) NOT NULL,
  `user_id` int(6) DEFAULT NULL,
  `start_timer` varchar(64) DEFAULT NULL,
  `stop_timer` varchar(64) DEFAULT NULL,
  `entry_date` datetime NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `manual_entry` int(1) NOT NULL DEFAULT '0',
  `entry_by` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`timer_id`),
  KEY `fk_timer_entry_tasks1_idx` (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;


--
-- Table structure for table `timezone`
--

DROP TABLE IF EXISTS `timezone`;
CREATE TABLE IF NOT EXISTS `timezone` (
  `timezone_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`timezone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=561 ;

--
-- Dumping data for table `timezone`
--

INSERT INTO `timezone` (`timezone_id`, `name`) VALUES
(1, 'Africa/Abidjan'),
(2, 'Africa/Accra'),
(3, 'Africa/Addis_Ababa'),
(4, 'Africa/Algiers'),
(5, 'Africa/Asmara'),
(6, 'Africa/Asmera'),
(7, 'Africa/Bamako'),
(8, 'Africa/Bangui'),
(9, 'Africa/Banjul'),
(10, 'Africa/Bissau'),
(11, 'Africa/Blantyre'),
(12, 'Africa/Brazzaville'),
(13, 'Africa/Bujumbura'),
(14, 'Africa/Cairo'),
(15, 'Africa/Casablanca'),
(16, 'Africa/Ceuta'),
(17, 'Africa/Conakry'),
(18, 'Africa/Dakar'),
(19, 'Africa/Dar_es_Salaam'),
(20, 'Africa/Djibouti'),
(21, 'Africa/Douala'),
(22, 'Africa/El_Aaiun'),
(23, 'Africa/Freetown'),
(24, 'Africa/Gaborone'),
(25, 'Africa/Harare'),
(26, 'Africa/Johannesburg'),
(27, 'Africa/Kampala'),
(28, 'Africa/Khartoum'),
(29, 'Africa/Kigali'),
(30, 'Africa/Kinshasa'),
(31, 'Africa/Lagos'),
(32, 'Africa/Libreville'),
(33, 'Africa/Lome'),
(34, 'Africa/Luanda'),
(35, 'Africa/Lubumbashi'),
(36, 'Africa/Lusaka'),
(37, 'Africa/Malabo'),
(38, 'Africa/Maputo'),
(39, 'Africa/Maseru'),
(40, 'Africa/Mbabane'),
(41, 'Africa/Mogadishu'),
(42, 'Africa/Monrovia'),
(43, 'Africa/Nairobi'),
(44, 'Africa/Ndjamena'),
(45, 'Africa/Niamey'),
(46, 'Africa/Nouakchott'),
(47, 'Africa/Ouagadougou'),
(48, 'Africa/Porto-Novo'),
(49, 'Africa/Sao_Tome'),
(50, 'Africa/Timbuktu'),
(51, 'Africa/Tripoli'),
(52, 'Africa/Tunis'),
(53, 'Africa/Windhoek'),
(54, 'America/Adak'),
(55, 'America/Anchorage '),
(56, 'America/Anguilla'),
(57, 'America/Antigua'),
(58, 'America/Araguaina'),
(59, 'America/Argentina/Buenos_Aires'),
(60, 'America/Argentina/Catamarca'),
(61, 'America/Argentina/ComodRivadavia'),
(62, 'America/Argentina/Cordoba'),
(63, 'America/Argentina/Jujuy'),
(64, 'America/Argentina/La_Rioja'),
(65, 'America/Argentina/Mendoza'),
(66, 'America/Argentina/Rio_Gallegos'),
(67, 'America/Argentina/Salta'),
(68, 'America/Argentina/San_Juan'),
(69, 'America/Argentina/San_Luis'),
(70, 'America/Argentina/Tucuman'),
(71, 'America/Argentina/Ushuaia'),
(72, 'America/Aruba'),
(73, 'America/Asuncion'),
(74, 'America/Atikokan'),
(75, 'America/Atka'),
(76, 'America/Bahia'),
(77, 'America/Barbados'),
(78, 'America/Belem'),
(79, 'America/Belize'),
(80, 'America/Blanc-Sablon'),
(81, 'America/Boa_Vista'),
(82, 'America/Bogota'),
(83, 'America/Boise'),
(84, 'America/Buenos_Aires'),
(85, 'America/Cambridge_Bay'),
(86, 'America/Campo_Grande'),
(87, 'America/Cancun'),
(88, 'America/Caracas'),
(89, 'America/Catamarca'),
(90, 'America/Cayenne'),
(91, 'America/Cayman'),
(92, 'America/Chicago'),
(93, 'America/Chihuahua'),
(94, 'America/Coral_Harbour'),
(95, 'America/Cordoba'),
(96, 'America/Costa_Rica'),
(97, 'America/Cuiaba'),
(98, 'America/Curacao'),
(99, 'America/Danmarkshavn'),
(100, 'America/Dawson'),
(101, 'America/Dawson_Creek'),
(102, 'America/Denver'),
(103, 'America/Detroit'),
(104, 'America/Dominica'),
(105, 'America/Edmonton'),
(106, 'America/Eirunepe'),
(107, 'America/El_Salvador'),
(108, 'America/Ensenada'),
(109, 'America/Fort_Wayne'),
(110, 'America/Fortaleza'),
(111, 'America/Glace_Bay'),
(112, 'America/Godthab'),
(113, 'America/Goose_Bay'),
(114, 'America/Grand_Turk'),
(115, 'America/Grenada'),
(116, 'America/Guadeloupe'),
(117, 'America/Guatemala'),
(118, 'America/Guayaquil'),
(119, 'America/Guyana'),
(120, 'America/Halifax'),
(121, 'America/Havana'),
(122, 'America/Hermosillo'),
(123, 'America/Indiana/Indianapolis'),
(124, 'America/Indiana/Knox'),
(125, 'America/Indiana/Marengo'),
(126, 'America/Indiana/Petersburg'),
(127, 'America/Indiana/Tell_City'),
(128, 'America/Indiana/Vevay'),
(129, 'America/Indiana/Vincennes'),
(130, 'America/Indiana/Winamac'),
(131, 'America/Indianapolis'),
(132, 'America/Inuvik'),
(133, 'America/Iqaluit'),
(134, 'America/Jamaica'),
(135, 'America/Jujuy'),
(136, 'America/Juneau'),
(137, 'America/Kentucky/Louisville'),
(138, 'America/Kentucky/Monticello'),
(139, 'America/Knox_IN'),
(140, 'America/La_Paz'),
(141, 'America/Lima'),
(142, 'America/Los_Angeles'),
(143, 'America/Louisville'),
(144, 'America/Maceio'),
(145, 'America/Managua'),
(146, 'America/Manaus'),
(147, 'America/Marigot'),
(148, 'America/Martinique'),
(149, 'America/Mazatlan'),
(150, 'America/Mendoza'),
(151, 'America/Menominee'),
(152, 'America/Merida'),
(153, 'America/Mexico_City'),
(154, 'America/Miquelon'),
(155, 'America/Moncton'),
(156, 'America/Monterrey'),
(157, 'America/Montevideo'),
(158, 'America/Montreal'),
(159, 'America/Montserrat'),
(160, 'America/Nassau'),
(161, 'America/New_York'),
(162, 'America/Nipigon'),
(163, 'America/Nome'),
(164, 'America/Noronha'),
(165, 'America/North_Dakota/Center'),
(166, 'America/North_Dakota/New_Salem'),
(167, 'America/Panama'),
(168, 'America/Pangnirtung'),
(169, 'America/Paramaribo'),
(170, 'America/Phoenix'),
(171, 'America/Port-au-Prince'),
(172, 'America/Port_of_Spain'),
(173, 'America/Porto_Acre'),
(174, 'America/Porto_Velho'),
(175, 'America/Puerto_Rico'),
(176, 'America/Rainy_River'),
(177, 'America/Rankin_Inlet'),
(178, 'America/Recife'),
(179, 'America/Regina'),
(180, 'America/Resolute'),
(181, 'America/Rio_Branco'),
(182, 'America/Rosario'),
(183, 'America/Santarem'),
(184, 'America/Santiago'),
(185, 'America/Santo_Domingo'),
(186, 'America/Sao_Paulo'),
(187, 'America/Scoresbysund'),
(188, 'America/Shiprock'),
(189, 'America/St_Barthelemy'),
(190, 'America/St_Johns'),
(191, 'America/St_Kitts'),
(192, 'America/St_Lucia'),
(193, 'America/St_Thomas'),
(194, 'America/St_Vincent'),
(195, 'America/Swift_Current'),
(196, 'America/Tegucigalpa'),
(197, 'America/Thule'),
(198, 'America/Thunder_Bay'),
(199, 'America/Tijuana'),
(200, 'America/Toronto'),
(201, 'America/Tortola'),
(202, 'America/Vancouver'),
(203, 'America/Virgin'),
(204, 'America/Whitehorse'),
(205, 'America/Winnipeg'),
(206, 'America/Yakutat'),
(207, 'America/Yellowknife'),
(208, 'Antarctica/Casey'),
(209, 'Antarctica/Davis'),
(210, 'Antarctica/DumontDUrville'),
(211, 'Antarctica/Mawson'),
(212, 'Antarctica/McMurdo'),
(213, 'Antarctica/Palmer'),
(214, 'Antarctica/Rothera'),
(215, 'Antarctica/South_Pole'),
(216, 'Antarctica/Syowa'),
(217, 'Antarctica/Vostok'),
(218, 'Arctic/Longyearbyen'),
(219, 'Asia/Aden'),
(220, 'Asia/Almaty'),
(221, 'Asia/Amman'),
(222, 'Asia/Anadyr'),
(223, 'Asia/Aqtau'),
(224, 'Asia/Aqtobe'),
(225, 'Asia/Ashgabat'),
(226, 'Asia/Ashkhabad'),
(227, 'Asia/Baghdad'),
(228, 'Asia/Bahrain'),
(229, 'Asia/Baku'),
(230, 'Asia/Bangkok'),
(231, 'Asia/Beirut'),
(232, 'Asia/Bishkek'),
(233, 'Asia/Brunei'),
(234, 'Asia/Calcutta'),
(235, 'Asia/Choibalsan'),
(236, 'Asia/Chongqing'),
(237, 'Asia/Chungking'),
(238, 'Asia/Colombo'),
(239, 'Asia/Dacca'),
(240, 'Asia/Damascus'),
(241, 'Asia/Dhaka'),
(242, 'Asia/Dili'),
(243, 'Asia/Dubai'),
(244, 'Asia/Dushanbe'),
(245, 'Asia/Gaza'),
(246, 'Asia/Harbin'),
(247, 'Asia/Ho_Chi_Minh'),
(248, 'Asia/Hong_Kong'),
(249, 'Asia/Hovd'),
(250, 'Asia/Irkutsk'),
(251, 'Asia/Istanbul'),
(252, 'Asia/Jakarta'),
(253, 'Asia/Jayapura'),
(254, 'Asia/Jerusalem'),
(255, 'Asia/Kabul'),
(256, 'Asia/Kamchatka'),
(257, 'Asia/Karachi'),
(258, 'Asia/Kashgar'),
(259, 'Asia/Kathmandu'),
(260, 'Asia/Katmandu'),
(261, 'Asia/Kolkata'),
(262, 'Asia/Krasnoyarsk'),
(263, 'Asia/Kuala_Lumpur'),
(264, 'Asia/Kuching'),
(265, 'Asia/Kuwait'),
(266, 'Asia/Macao'),
(267, 'Asia/Macau'),
(268, 'Asia/Magadan'),
(269, 'Asia/Makassar'),
(270, 'Asia/Manila'),
(271, 'Asia/Muscat'),
(272, 'Asia/Nicosia'),
(273, 'Asia/Novosibirsk'),
(274, 'Asia/Omsk'),
(275, 'Asia/Oral'),
(276, 'Asia/Phnom_Penh'),
(277, 'Asia/Pontianak'),
(278, 'Asia/Pyongyang'),
(279, 'Asia/Qatar'),
(280, 'Asia/Qyzylorda'),
(281, 'Asia/Rangoon'),
(282, 'Asia/Riyadh'),
(283, 'Asia/Saigon'),
(284, 'Asia/Sakhalin'),
(285, 'Asia/Samarkand'),
(286, 'Asia/Seoul'),
(287, 'Asia/Shanghai'),
(288, 'Asia/Singapore'),
(289, 'Asia/Taipei'),
(290, 'Asia/Tashkent'),
(291, 'Asia/Tbilisi'),
(292, 'Asia/Tehran'),
(293, 'Asia/Tel_Aviv'),
(294, 'Asia/Thimbu'),
(295, 'Asia/Thimphu'),
(296, 'Asia/Tokyo'),
(297, 'Asia/Ujung_Pandang'),
(298, 'Asia/Ulaanbaatar'),
(299, 'Asia/Ulan_Bator'),
(300, 'Asia/Urumqi'),
(301, 'Asia/Vientiane'),
(302, 'Asia/Vladivostok'),
(303, 'Asia/Yakutsk'),
(304, 'Asia/Yekaterinburg'),
(305, 'Asia/Yerevan'),
(306, 'Atlantic/Azores'),
(307, 'Atlantic/Bermuda'),
(308, 'Atlantic/Canary'),
(309, 'Atlantic/Cape_Verde'),
(310, 'Atlantic/Faeroe'),
(311, 'Atlantic/Faroe'),
(312, 'Atlantic/Jan_Mayen'),
(313, 'Atlantic/Madeira'),
(314, 'Atlantic/Reykjavik'),
(315, 'Atlantic/South_Georgia'),
(316, 'Atlantic/St_Helena'),
(317, 'Atlantic/Stanley'),
(318, 'Australia/ACT'),
(319, 'Australia/Adelaide'),
(320, 'Australia/Brisbane'),
(321, 'Australia/Broken_Hill'),
(322, 'Australia/Canberra'),
(323, 'Australia/Currie'),
(324, 'Australia/Darwin'),
(325, 'Australia/Eucla'),
(326, 'Australia/Hobart'),
(327, 'Australia/LHI'),
(328, 'Australia/Lindeman'),
(329, 'Australia/Lord_Howe'),
(330, 'Australia/Melbourne'),
(331, 'Australia/North'),
(332, 'Australia/NSW'),
(333, 'Australia/Perth'),
(334, 'Australia/Queensland'),
(335, 'Australia/South'),
(336, 'Australia/Sydney'),
(337, 'Australia/Tasmania'),
(338, 'Australia/Victoria'),
(339, 'Australia/West'),
(340, 'Australia/Yancowinna'),
(341, 'Europe/Amsterdam'),
(342, 'Europe/Andorra'),
(343, 'Europe/Athens'),
(344, 'Europe/Belfast'),
(345, 'Europe/Belgrade'),
(346, 'Europe/Berlin'),
(347, 'Europe/Bratislava'),
(348, 'Europe/Brussels'),
(349, 'Europe/Bucharest'),
(350, 'Europe/Budapest'),
(351, 'Europe/Chisinau'),
(352, 'Europe/Copenhagen'),
(353, 'Europe/Dublin'),
(354, 'Europe/Gibraltar'),
(355, 'Europe/Guernsey'),
(356, 'Europe/Helsinki'),
(357, 'Europe/Isle_of_Man'),
(358, 'Europe/Istanbul'),
(359, 'Europe/Jersey'),
(360, 'Europe/Kaliningrad'),
(361, 'Europe/Kiev'),
(362, 'Europe/Lisbon'),
(363, 'Europe/Ljubljana'),
(364, 'Europe/London'),
(365, 'Europe/Luxembourg'),
(366, 'Europe/Madrid'),
(367, 'Europe/Malta'),
(368, 'Europe/Mariehamn'),
(369, 'Europe/Minsk'),
(370, 'Europe/Monaco'),
(371, 'Europe/Moscow'),
(372, 'Europe/Nicosia'),
(373, 'Europe/Oslo'),
(374, 'Europe/Paris'),
(375, 'Europe/Podgorica'),
(376, 'Europe/Prague'),
(377, 'Europe/Riga'),
(378, 'Europe/Rome'),
(379, 'Europe/Samara'),
(380, 'Europe/San_Marino'),
(381, 'Europe/Sarajevo'),
(382, 'Europe/Simferopol'),
(383, 'Europe/Skopje'),
(384, 'Europe/Sofia'),
(385, 'Europe/Stockholm'),
(386, 'Europe/Tallinn'),
(387, 'Europe/Tirane'),
(388, 'Europe/Tiraspol'),
(389, 'Europe/Uzhgorod'),
(390, 'Europe/Vaduz'),
(391, 'Europe/Vatican'),
(392, 'Europe/Vienna'),
(393, 'Europe/Vilnius'),
(394, 'Europe/Volgograd'),
(395, 'Europe/Warsaw'),
(396, 'Europe/Zagreb'),
(397, 'Europe/Zaporozhye'),
(398, 'Europe/Zurich'),
(399, 'Indian/Antananarivo'),
(400, 'Indian/Chagos'),
(401, 'Indian/Christmas'),
(402, 'Indian/Cocos'),
(403, 'Indian/Comoro'),
(404, 'Indian/Kerguelen'),
(405, 'Indian/Mahe'),
(406, 'Indian/Maldives'),
(407, 'Indian/Mauritius'),
(408, 'Indian/Mayotte'),
(409, 'Indian/Reunion'),
(410, 'Pacific/Apia'),
(411, 'Pacific/Auckland'),
(412, 'Pacific/Chatham'),
(413, 'Pacific/Easter'),
(414, 'Pacific/Efate'),
(415, 'Pacific/Enderbury'),
(416, 'Pacific/Fakaofo'),
(417, 'Pacific/Fiji'),
(418, 'Pacific/Funafuti'),
(419, 'Pacific/Galapagos'),
(420, 'Pacific/Gambier'),
(421, 'Pacific/Guadalcanal'),
(422, 'Pacific/Guam'),
(423, 'Pacific/Honolulu'),
(424, 'Pacific/Johnston'),
(425, 'Pacific/Kiritimati'),
(426, 'Pacific/Kosrae'),
(427, 'Pacific/Kwajalein'),
(428, 'Pacific/Majuro'),
(429, 'Pacific/Marquesas'),
(430, 'Pacific/Midway'),
(431, 'Pacific/Nauru'),
(432, 'Pacific/Niue'),
(433, 'Pacific/Norfolk'),
(434, 'Pacific/Noumea'),
(435, 'Pacific/Pago_Pago'),
(436, 'Pacific/Palau'),
(437, 'Pacific/Pitcairn'),
(438, 'Pacific/Ponape'),
(439, 'Pacific/Port_Moresby'),
(440, 'Pacific/Rarotonga'),
(441, 'Pacific/Saipan'),
(442, 'Pacific/Samoa'),
(443, 'Pacific/Tahiti'),
(444, 'Pacific/Tarawa'),
(445, 'Pacific/Tongatapu'),
(446, 'Pacific/Truk'),
(447, 'Pacific/Wake'),
(448, 'Pacific/Wallis'),
(449, 'Pacific/Yap'),
(450, 'Brazil/Acre'),
(451, 'Brazil/DeNoronha'),
(452, 'Brazil/East'),
(453, 'Brazil/West'),
(454, 'Canada/Atlantic'),
(455, 'Canada/Central'),
(456, 'Canada/East-Saskatchewan'),
(457, 'Canada/Eastern'),
(458, 'Canada/Mountain'),
(459, 'Canada/Newfoundland'),
(460, 'Canada/Pacific'),
(461, 'Canada/Saskatchewan'),
(462, 'Canada/Yukon'),
(463, 'CET'),
(464, 'Chile/Continental'),
(465, 'Chile/EasterIsland'),
(466, 'CST6CDT'),
(467, 'Cuba'),
(468, 'EET'),
(469, 'Egypt'),
(470, 'Eire'),
(471, 'EST'),
(472, 'EST5EDT'),
(473, 'Etc/GMT'),
(474, 'Etc/GMT+0'),
(475, 'Etc/GMT+1'),
(476, 'Etc/GMT+10'),
(477, 'Etc/GMT+11'),
(478, 'Etc/GMT+12'),
(479, 'Etc/GMT+2'),
(480, 'Etc/GMT+3'),
(481, 'Etc/GMT+4'),
(482, 'Etc/GMT+5'),
(483, 'Etc/GMT+6'),
(484, 'Etc/GMT+7'),
(485, 'Etc/GMT+8'),
(486, 'Etc/GMT+9'),
(487, 'Etc/GMT-0'),
(488, 'Etc/GMT-1'),
(489, 'Etc/GMT-10'),
(490, 'Etc/GMT-11'),
(491, 'Etc/GMT-12'),
(492, 'Etc/GMT-13'),
(493, 'Etc/GMT-14'),
(494, 'Etc/GMT-2'),
(495, 'Etc/GMT-3'),
(496, 'Etc/GMT-4'),
(497, 'Etc/GMT-5'),
(498, 'Etc/GMT-6'),
(499, 'Etc/GMT-7'),
(500, 'Etc/GMT-8'),
(501, 'Etc/GMT-9'),
(502, 'Etc/GMT0'),
(503, 'Etc/Greenwich'),
(504, 'Etc/UCT'),
(505, 'Etc/Universal'),
(506, 'Etc/UTC'),
(507, 'Etc/Zulu'),
(508, 'Factory'),
(509, 'GB'),
(510, 'GB-Eire'),
(511, 'GMT'),
(512, 'GMT+0'),
(513, 'GMT-0'),
(514, 'GMT0'),
(515, 'Greenwich'),
(516, 'Hongkong'),
(517, 'HST'),
(518, 'Iceland'),
(519, 'Iran'),
(520, 'Israel'),
(521, 'Jamaica'),
(522, 'Japan'),
(523, 'Kwajalein'),
(524, 'Libya'),
(525, 'MET'),
(526, 'Mexico/BajaNorte'),
(527, 'Mexico/BajaSur'),
(528, 'Mexico/General'),
(529, 'MST'),
(530, 'MST7MDT'),
(531, 'Navajo'),
(532, 'NZ'),
(533, 'NZ-CHAT'),
(534, 'Poland'),
(535, 'Portugal'),
(536, 'PRC'),
(537, 'PST8PDT'),
(538, 'ROC'),
(539, 'ROK'),
(540, 'Singapore'),
(541, 'Turkey'),
(542, 'UCT'),
(543, 'Universal'),
(544, 'US/Alaska'),
(545, 'US/Aleutian'),
(546, 'US/Arizona'),
(547, 'US/Central'),
(548, 'US/East-Indiana'),
(549, 'US/Eastern'),
(550, 'US/Hawaii'),
(551, 'US/Indiana-Starke'),
(552, 'US/Michigan'),
(553, 'US/Mountain'),
(554, 'US/Pacific'),
(555, 'US/Pacific-New'),
(556, 'US/Samoa'),
(557, 'UTC'),
(558, 'W-SU'),
(559, 'WET'),
(560, 'Zulu');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
CREATE TABLE IF NOT EXISTS `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) DEFAULT NULL,
  `trans_id` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `payment_method` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'received',
  `ip` varchar(45) DEFAULT NULL,
  `notes` text,
  `creator_email` varchar(100) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;


--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(6) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `ip_address` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(120) DEFAULT NULL,
  `forgotten_password_time` int(12) NOT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `last_login` datetime NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `user_type` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`user_id`),
  KEY `users_fk2` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `client_fk1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`);

--
-- Constraints for table `client_user`
--
ALTER TABLE `client_user`
  ADD CONSTRAINT `client_user_fk1` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`);

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_fk1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`);

--
-- Constraints for table `est_details`
--
ALTER TABLE `est_details`
  ADD CONSTRAINT `fk_est_details_estimate1` FOREIGN KEY (`est_id`) REFERENCES `estimate` (`est_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `modules_actions`
--
ALTER TABLE `modules_actions`
  ADD CONSTRAINT `modules_actions_fk1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`);

--
-- Constraints for table `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `fk_notes_projects1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `online_clients`
--
ALTER TABLE `online_clients`
  ADD CONSTRAINT `online_clients_fk1` FOREIGN KEY (`cl_user_id`) REFERENCES `client_user` (`cl_user_id`);

--
-- Constraints for table `online_users`
--
ALTER TABLE `online_users`
  ADD CONSTRAINT `online_users_fk1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_fk1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`),
  ADD CONSTRAINT `permissions_fk2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);

--
-- Constraints for table `permissions_actions`
--
ALTER TABLE `permissions_actions`
  ADD CONSTRAINT `permissions_actions_fk1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`),
  ADD CONSTRAINT `permissions_actions_fk2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
  ADD CONSTRAINT `permissions_actions_fk3` FOREIGN KEY (`action_id`) REFERENCES `modules_actions` (`action_id`);

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_fk1` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`);

--
-- Constraints for table `project_activities`
--
ALTER TABLE `project_activities`
  ADD CONSTRAINT `project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `project_assignment`
--
ALTER TABLE `project_assignment`
  ADD CONSTRAINT `fk_project_assignment_projects1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `project_risks`
--
ALTER TABLE `project_risks`
  ADD CONSTRAINT `fk_project_risks_projects1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `settings_company`
--
ALTER TABLE `settings_company`
  ADD CONSTRAINT `fk_settings_company_company1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `settings_email`
--
ALTER TABLE `settings_email`
  ADD CONSTRAINT `fk_settings_email_company1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `settings_email_template`
--
ALTER TABLE `settings_email_template`
  ADD CONSTRAINT `fk_settings_email_template_company1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `settings_local`
--
ALTER TABLE `settings_local`
  ADD CONSTRAINT `fk_settings_local_company1` FOREIGN KEY (`company_company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `settings_payment_methods`
--
ALTER TABLE `settings_payment_methods`
  ADD CONSTRAINT `fk_settings_payment_terms_company1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `settings_paypal`
--
ALTER TABLE `settings_paypal`
  ADD CONSTRAINT `fk_settings_paypal_company1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `settings_support`
--
ALTER TABLE `settings_support`
  ADD CONSTRAINT `fk_settings_support_company1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `settings_system`
--
ALTER TABLE `settings_system`
  ADD CONSTRAINT `fk_settings_system_company1` FOREIGN KEY (`company_company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `task_message_replies`
--
ALTER TABLE `task_message_replies`
  ADD CONSTRAINT `task_message_replies_fk1` FOREIGN KEY (`message_id`) REFERENCES `task_messages` (`message_id`);

--
-- Constraints for table `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `fk_ticket_company1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ticket_replies`
--
ALTER TABLE `ticket_replies`
  ADD CONSTRAINT `fk_ticket_replies_ticket1` FOREIGN KEY (`ticket_id`) REFERENCES `ticket` (`ticket_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_fk1` FOREIGN KEY (`user_id`) REFERENCES `employee` (`emp_id`),
  ADD CONSTRAINT `users_fk2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
