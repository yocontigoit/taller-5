
	$(function() {
		tisa_icheck.init();
		// mailbox tables
		zest_mail.inbox();
		zest_mail.outbox();
        zest_mail.trash();
	})
	
	// mailbox
	zest_mail = {
		count_rows: function(active_table) {
			return active_table.find('tbody tr').length
		},
		inbox: function() {
			if($('#mail_inbox').length) {
				
				var $inbox_table = $('#mail_inbox');
				
				$inbox_table.on({
					'footable_paging': function(e) {
						var allRows = zest_mail.count_rows($inbox_table),
							page_size = e.size,
							act_page = e.page,
							pages = Math.ceil(allRows / page_size);
							first_visble = (page_size * act_page)+1;
						
						if(pages == act_page+1) {
							var last_visible = allRows;
						} else {
							var last_visible = (page_size * act_page) + page_size;
						};
						
						$('.page_start_row').text(first_visble);
						$('.page_end_row').text(last_visible);
					},
					'footable_initialized': function(e) {
						$('.all_rows').text(zest_mail.count_rows($inbox_table));
						
					}
				}).on('footable_breakpoint', function() {
					$inbox_table.trigger('footable_expand_all');
				}).footable({
					breakpoints: {
						phone: 640,
						tablet: 778
					},
					addRowToggle: false,
					firstText: '<span class="fa fa-angle-double-left">',
					previousText: '<span class="fa fa-angle-left">',
					nextText: '<span class="fa fa-angle-right">',
					lastText: '<span class="fa fa-angle-double-right">'
				});
			}
		},
		outbox: function() {
			if($('#mail_outbox').length) {
				
				var $outbox_table = $('#mail_outbox');
				
				$outbox_table.on({
					'footable_paging': function(e) {
						var allRows = zest_mail.count_rows($outbox_table),
							page_size = e.size,
							act_page = e.page,
							pages = Math.ceil(allRows / page_size);
							first_visble = (page_size * act_page)+1;
						
						if(pages == act_page+1) {
							var last_visible = allRows;
						} else {
							var last_visible = (page_size * act_page) + page_size;
						};
						
						$('.page_start_row').text(first_visble);
						$('.page_end_row').text(last_visible);
					},
					'footable_initialized': function(e) {
						$('.all_rows').text(zest_mail.count_rows($outbox_table));
						
					}
				}).on('footable_breakpoint', function() {
					$outbox_table.trigger('footable_expand_all');
				}).footable({
					breakpoints: {
						phone: 640,
						tablet: 778
					},
					addRowToggle: false,
					firstText: '<span class="fa fa-angle-double-left">',
					previousText: '<span class="fa fa-angle-left">',
					nextText: '<span class="fa fa-angle-right">',
					lastText: '<span class="fa fa-angle-double-right">'
				});
			}
		},
        trash: function() {
            if($('#mail_trash').length) {

                var $trash_table = $('#mail_trash');

                $trash_table.on({
                    'footable_paging': function(e) {
                        var allRows = zest_mail.count_rows($trash_table),
                            page_size = e.size,
                            act_page = e.page,
                            pages = Math.ceil(allRows / page_size);
                        first_visble = (page_size * act_page)+1;

                        if(pages == act_page+1) {
                            var last_visible = allRows;
                        } else {
                            var last_visible = (page_size * act_page) + page_size;
                        };

                        $('.page_start_row').text(first_visble);
                        $('.page_end_row').text(last_visible);
                    },
                    'footable_initialized': function(e) {
                        $('.all_rows').text(zest_mail.count_rows($trash_table));

                    }
                }).on('footable_breakpoint', function() {
                    $trash_table.trigger('footable_expand_all');
                }).footable({
                    breakpoints: {
                        phone: 640,
                        tablet: 778
                    },
                    addRowToggle: false,
                    firstText: '<span class="fa fa-angle-double-left">',
                    previousText: '<span class="fa fa-angle-left">',
                    nextText: '<span class="fa fa-angle-right">',
                    lastText: '<span class="fa fa-angle-double-right">'
                });
            }
        }

	}

	// check all, check row
	tisa_icheck = {
		init: function() {
			if ($('.check_all,.check_row').length) {
				$('.check_all,.check_row').prop('checked',false).iCheck({
					checkboxClass: 'icheckbox_minimal-green',
					radioClass: 'iradio_minimal-green'
				});
				
				$('.check_all').on('ifChecked', function(event){
					$('.check_row').iCheck('check'); 
				}).on('ifUnchecked', function(event){
					$('.check_row').iCheck('uncheck'); 
				});
				
				$('.check_row').on('ifChecked', function(event){
					$(this).closest('tr').addClass('active_row')
				}).on('ifUnchecked', function(event){
					$(this).closest('tr').removeClass('active_row')
				});
			}
		}
	}
	

	// select recipients
	tisa_recipients = {
		init: function() {
			if ($('.msg_select').length) {
				$(".msg_select").select2({
					tags: [],
					tokenSeparators: [',', ' ']
				});
			}
		}
	}