/**
 * Created by appsdyne on 2015/02/27.
 */

$(document).ready(function(){

    $('.tr-link').on('click', 'td:not(.tr-link-excluded)', function(){

        var tr_link = $(this).parent().attr("data-link");

        //do we have a valid link
        if (tr_link == '' || typeof tr_link === 'undefined') {
            //do nothing
        }
        else {
            //open the page

            window.location = tr_link;
        }
    });



   /* $(document).on('click','[data-toggle="zestModal"]',function(e){
        e.preventDefault();
        var $this=$(this);
        var $remote=$this.data('remote')||$this.attr('href');
        $modal= $(".modal");
        $modal.modal();
        $modal.load($remote);
    });*/

    $('.datepicker').datepicker({
        // format: "yyyy-mm-dd",
        //  language: "lang",

        todayHighlight: true
    }).on('changeDate', function(e){
        $(this).datepicker('hide');
    });
    /** BEGIN TIMEPICKER **/
    //if ($('.timepicker').length > 0){
    if (jQuery().timepicker) {
        $('.timepicker').timepicker({
            //$('.timepicker-24').timepicker({
            autoclose: true,
            minuteStep: 5,
            showSeconds: true,
            showMeridian: false
            // })

        });
    }
    //}

    /** END TIMEPICKER **/
    $('[data-toggle="tab"]').click(function(e) {

                var $this = $(this),
                    loadurl = $this.attr('href'),
                    targ = $this.attr('data-target');

            $.get(loadurl, function (data) {
                $(targ).html(data);
            });

            $this.tab('show');
            return false;

    });

   $('.ajax-loader').bind( "click", function() {
       $('#zestpage-loader').fadeIn(200);
        //$('#loader-error').delay(10000).fadeIn(200);
    });
    //$('.ajax-loader').click(function(e) {
    //    $('#zestpage-loader').fadeIn(200);
    //});

    $(".module_checkboxes").change(function()
    {
        if ($(this).prop('checked'))
        {
            $(this).parent().find('input[type=checkbox]').not(':disabled').prop('checked', true);
        }
        else
        {
            $(this).parent().find('input[type=checkbox]').not(':disabled').prop('checked', false);
        }
    });








});
