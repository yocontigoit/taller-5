/**
 Core script to handle the entire theme and core functions
 **/

var Zest = function () {

    var handleTasks = function(){

   if ($('.widget-tasksticker').length > 0){
        $('.widget-tasksticker').newsTicker({
            row_height: 41,
            max_rows: 10,
            speed: 500,
            direction: 'up',
            duration: 3000,
            autostart: 1,
            pauseOnHover: 1
        });
    }
  }
    //* BEGIN:CORE HANDLERS *//
    // this function handles responsive layout on screen size resize or mobile device rotate.
    var handleChoosen = function(){
        /** BEGIN CHOSEN JS **/
            "use strict";
            var configChosen = {
                '.chosen-select': {},
                '.chosen-select-deselect': {allow_single_deselect: true},
                '.chosen-select-no-single': {disable_search_threshold: 10},
                '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
                '.chosen-select-width': {width: "100%"}
            }
            for (var selector in configChosen) {
                $(selector).chosen(configChosen[selector]);
            }
    }

    var handleSlider = function(){

        //if ($('#tasks-percent').length > 0){
            $('#tasks-percent').slider({
                formater: function(value) {
                    return value+'% Complete..';
                }
            });
        //}
    }


    var handleModals = function () {

        $(document).on('click','[data-toggle="zestModal"]',function(e){

            $('#zestModal').remove();
            e.preventDefault();
            var $this=$(this),
                $remote=$this.data('remote')||$this.attr('href'),
                $modal=$('<div class="modal" id="zestModal"><div class="modal-body"></div></div>');
            $('body').append($modal);
            //$modal.modal();
            $modal.modal({backdrop: 'static', keyboard: false});
            $modal.load($remote);
        });


    }





    // Set proper height for sidebar and content. The content and sidebar height must be synced always.
    var handleSidebarAndContentHeight = function () {
        var content = $('.page-content');
        var sidebar = $('.sidebar-left');
        var body = $('body');
        var height;

        if (body.hasClass("page-footer-fixed") === true && body.hasClass("page-sidebar-fixed") === false) {
            var available_height = $(window).height() - $('.footer').outerHeight() - $('.header').outerHeight();
            if (content.height() < available_height) {
                content.attr('style', 'min-height:' + available_height + 'px !important');
            }
        } else {
            if (body.hasClass('page-sidebar-fixed')) {
                height = _calculateFixedSidebarViewportHeight();
            } else {
                height = sidebar.height() + 20;
                var headerHeight = $('.header').outerHeight();
                var footerHeight = $('.footer').outerHeight();
                if ($(window).width() > 1024 && (height + headerHeight + footerHeight)  < $(window).height()) {
                    height = $(window).height() - headerHeight - footerHeight;
                }
            }
            if (height >= content.height()) {
                content.attr('style', 'min-height:' + height + 'px !important');
            }
        }
    }

    //Handle scrollers
    var handleflipscroller = function(){


        /** NICESCROLL AND SLIMSCROLL FUNCTION **/
        $(".sidebar-nicescroller").niceScroll({
            cursorcolor: "#121212",
            cursorborder: "0px solid #fff",
            cursorborderradius: "0px",
            cursorwidth: "0px"
        });
        $(".sidebar-nicescroller-visible-scroller").niceScroll({
            cursorcolor: "#121212",
            cursorborder: "0px solid #fff",
            cursorborderradius: "0px",
            cursorwidth: "5px",
            cursoropacitymax: 0.2
        });
        $(".sidebar-nicescroller").getNiceScroll().resize();
        $(".right-sidebar-nicescroller").niceScroll({
            cursorcolor: "#111",
            cursorborder: "0px solid #fff",
            cursorborderradius: "0px",
            cursorwidth: "0px"
        });
        $(".right-sidebar-nicescroller").getNiceScroll().resize();


        var  footerfixed = {
            'bottom':'0'
        }
        $(".footer-widget").css(footerfixed);
        /** END NICESCROLL AND SLIMSCROLL FUNCTION **/


    }

    //Handle toggle
    var handleSidebartoggle = function(){

        $(".footer-widget").show();
        /** SIDEBAR FUNCTION **/
        jQuery('.sidebar-left ul.sidebar-menu li a').click(function() {
            "use strict";
            $('.sidebar-left li').removeClass('active');
            $(this).closest('li').addClass('active');
            var checkElement = $(this).next();
            if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                $(this).closest('li').removeClass('active');
                checkElement.slideUp('fast');
            }
            if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                $('.sidebar-left ul.sidebar-menu ul:visible').slideUp('fast');
                checkElement.slideDown('fast');
            }
            if($(this).closest('li').find('ul').children().length == 0) {
                return true;
            } else {
                return false;
            }
        });

        if ($(window).width() < 1025) {
            $(".sidebar-left").removeClass("sidebar-nicescroller");
            $(".sidebar-right").removeClass("right-sidebar-nicescroller");
            //$(".footer-widget").show();
            $(".nav-dropdown-content").removeClass("scroll-nav-dropdown");
        }
        /** END SIDEBAR FUNCTION **/


        /** BUTTON TOGGLE FUNCTION **/
        $(".btn-collapse-sidebar-left").click(function(){
            "use strict";


            var  barfixed = {
                'position':'fixed',
                'bottom':'0',
                'right':'-260',
                'z-index':'9999'

            }

            // $('#top-inside').css(barfixed);
            $(".top-navbar").toggleClass("toggle");
            $(".sidebar-left").toggleClass("toggle");

            if($(".footer-widget").is(':hidden')) {
                $(".footer-widget").show();
            }else{
                $(".footer-widget").hide();
            }

            $(".page-content").toggleClass("toggle");
            $(".icon-dinamic").toggleClass("rotate-180");

            if ($(window).width() > 991) {

                if($(".sidebar-right").hasClass("toggle-left") === true){
                    $(".sidebar-right").removeClass("toggle-left");
                    $(".top-navbar").removeClass("toggle-left");
                    $(".page-content").removeClass("toggle-left");
                    $(".sidebar-left").removeClass("toggle-left");
                    //$(".footer-widget").show();
                    if($(".sidebar-left").hasClass("toggle") === true){
                        $(".sidebar-left").removeClass("toggle");
                        //$(".eChat").marginRight(0);
                        $('.eChat').css(barfixed);
                    }

                    if($(".page-content").hasClass("toggle") === true){
                        //$(".footer-widget").show();
                        $(".page-content").removeClass("toggle");
                        $('.eChat').css(barfixed);

                    }
                    $('.eChat').css(barfixed);
                }
            }
        });

        $(".btn-collapse-sidebar-right").click(function(){
            "use strict";

            var  barfixed = {
                'position':'fixed',
                'bottom':'0',
                'right':'-260',
                'z-index':'9999'


            }
            $(".top-navbar").toggleClass("toggle-left");
            $(".sidebar-left").toggleClass("toggle-left");
            $('.eChat').css(barfixed);
            if($(".footer-widget").is(':hidden')) {
                $(".footer-widget").show();
            }else{
                $(".footer-widget").hide();
            }

            $(".sidebar-right").toggleClass("toggle-left");
            $(".page-content").toggleClass("toggle-left");

        });

        $(".btn-collapse-nav").click(function(){
            "use strict";
            $(".icon-plus").toggleClass("rotate-45");
        });
        /** END BUTTON TOGGLE FUNCTION **/

    }
    // Handle sidebar menu
    var handleSidebarMenu = function () {
        jQuery('.page-sidebar').on('click', 'li > a', function (e) {
            if ($(this).next().hasClass('sub-menu') == false) {
                if ($('.btn-navbar').hasClass('collapsed') == false) {
                    $('.btn-navbar').click();
                }
                return;
            }

            if ($(this).next().hasClass('sub-menu always-open')) {
                return;
            }

            var parent = $(this).parent().parent();
            var the = $(this);
            var menu = $('.page-sidebar-menu');
            var sub = jQuery(this).next();

            var autoScroll = menu.data("auto-scroll") ? menu.data("auto-scroll") : true;
            var slideSpeed = menu.data("slide-speed") ? parseInt(menu.data("slide-speed")) : 200;

            parent.children('li.open').children('a').children('.arrow').removeClass('open');
            parent.children('li.open').children('.sub-menu:not(.always-open)').slideUp(200);
            parent.children('li.open').removeClass('open');

            var slideOffeset = -200;

            if (sub.is(":visible")) {
                jQuery('.arrow', jQuery(this)).removeClass("open");
                jQuery(this).parent().removeClass("open");
                sub.slideUp(slideSpeed, function () {
                    if (autoScroll == true && $('body').hasClass('page-sidebar-closed') == false) {
                        if ($('body').hasClass('page-sidebar-fixed')) {
                            menu.slimScroll({'scrollTo': (the.position()).top});
                        } else {
                            Zest.scrollTo(the, slideOffeset);
                        }
                    }
                    handleSidebarAndContentHeight();
                });
            } else {
                jQuery('.arrow', jQuery(this)).addClass("open");
                jQuery(this).parent().addClass("open");
                sub.slideDown(slideSpeed, function () {
                    if (autoScroll == true && $('body').hasClass('page-sidebar-closed') == false) {
                        if ($('body').hasClass('page-sidebar-fixed')) {
                            menu.slimScroll({'scrollTo': (the.position()).top});
                        } else {
                            Zest.scrollTo(the, slideOffeset);
                        }
                    }
                    handleSidebarAndContentHeight();
                });
            }

            e.preventDefault();
        });

    }

    // Helper function to calculate sidebar height for fixed sidebar layout.
    var _calculateFixedSidebarViewportHeight = function () {
        var sidebarHeight = $(window).height() - $('.header').height() + 1;
        if ($('body').hasClass("page-footer-fixed")) {
            sidebarHeight = sidebarHeight - $('.footer').outerHeight();
        }

        return sidebarHeight;
    }


    // Handles the go to top button at the footer
    var handleGoTop = function () {
        /* set variables locally for increased performance */
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });

        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

    }


    return {

        //main function to initiate the theme
        init: function () {

            handleflipscroller();
            //handleTasks();
            handleSlider();
            handleModals();
            handleChoosen();
            handleGoTop(); //handles scroll to top functionality in the footer
            //layout handlers
            handleSidebartoggle();
            handleSidebarMenu(); // handles main menu




        },
        scrollTo: function (el, offeset) {
            var pos = (el && el.size() > 0) ? el.offset().top : 0;

            if (el) {
                if ($('body').hasClass('page-header-fixed')) {
                    pos = pos - $('.header').height();
                }
                pos = pos + (offeset ? offeset : -1 * el.height());
            }

            jQuery('html,body').animate({
                scrollTop: pos
            }, 'slow');
        },

        // function to scroll to the top
        scrollTop: function () {
            Zest.scrollTo();
        },

        startPageLoading: function(message) {
            $('.page-loading').remove();
            $('body').Zestend('<div class="page-loading"><img src="assets/img/loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>' + (message ? message : 'Loading...') + '</span></div>');
        },

        stopPageLoading: function() {
            $('.page-loading').remove();
        },


        // check IE8 mode
        isIE8: function () {
            return isIE8;
        },

        // check IE9 mode
        isIE9: function () {
            return isIE9;
        },

        //check RTL mode
        isRTL: function () {
            return isRTL;
        }

    };

}();