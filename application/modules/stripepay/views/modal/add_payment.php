<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/01
 * Time: 03:43 PM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                        class="fa fa-times"></i></span><span class="sr-only"><?php echo "close"; ?></span></button>
            <h4 class="modal-title"><?php echo "edit"; ?></h4>
        </div>
        <?php echo form_open('stripepay/pay', array('id' => 'payment-form')); ?>
        <?php
        if (isset($errors) && !empty($errors) && is_array($errors)) {
            echo '<div class="alert alert-error"><h4>Error!</h4>The following error(s) occurred:<ul>';
            foreach ($errors as $e) {
                echo "<li>$e</li>";
            }
            echo '</ul></div>';
        } ?>
        <div id="payment-errors"></div>
        <div class="alert alert-info">Enter the Card Number without spaces or hyphens.</div>
        <input type="hidden" name="invoice_id" value="<?php echo $invoice_info->item_number; ?>">

        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="taskStart">Card Number</label>
                        <input type="text" size="20" class="form-control card-number" required="" autocomplete="off"
                               placeholder="Card Number"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="taskPriority"><?php echo "CVC" ?></label>
                        <input type="text" size="4" class="form-control card-cvc" required="" autocomplete="off"
                               placeholder="CVC"/>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="taskStatus"><?php echo "Expiration Month"; ?></label>
                        <input type="text" size="2" class="form-control datepicker card-expiry-month" autocomplete="off"
                               data-date-format="mm" placeholder="mm">

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="taskPercent"><?php echo "Expiration Year"; ?></label>
                        <input type="text" size="4" class="form-control datepicker card-expiry-year"
                               data-date-format="yyyy" placeholder="yyyy">

                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="submit" value="addNewTask" class="btn btn-success btn-icon"><i
                    class="fa fa-check-square-o"></i> <?php echo "make payment"; ?></button>
            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i
                    class="fa fa-times-circle-o"></i> <?php echo "cancel"; ?></button>
        </div>
        </form>

    </div>
</div>

<?php if (isset($use_stripe)) { ?>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/stripe/buy.js'); ?>"></script>
<?php } ?>
