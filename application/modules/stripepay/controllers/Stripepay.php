<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/01
 * Time: 03:44 PM
 */
class Stripepay extends Admin_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model("stripe_m");
        $this->data['use_stripe'] = true;
        $this->data['SCRIPT_PAGE'] = true;

    }

    /**
     * add payment
     * @param null $invoice_id
     */
    function  add_payment($invoice_id = null)
    {

        $reference_no = get_invoice_reference_no($invoice_id);
        $invoice_currency = get_invoice_currency($invoice_id);
        //$invoice_amount = invoice_total($invoice_id);
        $invoice_amount = invoice_balance($invoice_id);

        if ($invoice_amount <= 0) {
            $invoice_amount = 0.00;
        }
        $invoice_info = new stdClass();
        $invoice_info->item_name = $reference_no;
        $invoice_info->item_number = $invoice_id;
        $invoice_info->amount = $invoice_amount;
        $invoice_info->currency = $invoice_currency;

        $this->data['invoice_info'] = $invoice_info;

        $this->data['title'] = 'Agregar Pago';

        $this->load->view('modal/add_payment', $this->data);

    }


    function pay()
    {
        // Check for a form submission:
        if ($_POST) {

            // Stores errors:
            $errors = array();

            // Need a payment token:
            if (isset($_POST['stripeToken'])) {

                $token = $_POST['stripeToken'];

                // Check for a duplicate submission, just in case:
                // Uses sessions, you could use a cookie instead.
                if (isset($_SESSION['token']) && ($_SESSION['token'] == $token)) {
                    $errors['token'] = 'You have apparently resubmitted the form. Please do not do that.';
                } else { // New submission.
                    $_SESSION['token'] = $token;
                }

            } else {
                $errors['token'] = 'The order cannot be processed. Please make sure you have JavaScript enabled and try again.';
            }

            // print_r($errors);
            // If no errors, process the order:
            if (empty($errors)) {
                //print_r($_POST);
                // create the charge on Stripe's servers - this will charge the user's card
                try {

                    // Include the Stripe library:

                    require_once APPPATH . '/libraries/stripe/init.php';


                    // set your secret key: remember to change this to your live secret key in production
                    // see your keys here https://manage.stripe.com/account
                    \Stripe\Stripe::setApiKey($this->config->item('stripe')->stripe_private_key);

                    $invoice_id = intval($_POST['invoice_id']);
                    $reference_no = get_invoice_reference_no($invoice_id);
                    $invoice_currency = get_invoice_currency($invoice_id);
                    //$invoice_amount = invoice_total($invoice_id);
                    $invoice_amount = invoice_balance($invoice_id);
                    if ($invoice_amount <= 0) {
                        $invoice_amount = 0.00;
                    }

                    $invoice = $invoice_id;
                    $invoice_ref = $reference_no;
                    $currency = $invoice_currency;
                    $paid_by = client_company($this->CompanyClient_id);
                    $amount = $invoice_amount * 100;


                    $payer_email = $this->CompanyClient_email;

                    $metadata = array(
                        'invoice_id' => $invoice,
                        'payer' => $this->CompanyUser_FullName,
                        'payer_email' => $payer_email,
                        'invoice_ref' => $invoice_ref
                    );

                    // Charge the order:
                    $charge = \Stripe\Charge::create(array(
                            "amount" => $amount, // amount in cents
                            "currency" => $currency,
                            "card" => $token,
                            "metadata" => $metadata,
                            "description" => "Payment for Invoice " . $invoice_ref
                        )
                    );

                    // Check that it was paid:
                    if ($charge->paid == true) {
                        $metadata = $charge->metadata;

                        $transaction = array(
                            'invoice_id' => $metadata->invoice_id,
                            'trans_id' => $charge->balance_transaction,
                            'amount' => $charge->amount / 100,
                            'payment_method' => 4,
                            'ip' => $this->input->ip_address(),
                            'notes' => $charge->description,
                            'creator_email' => $metadata->payer_email,
                            'created_by' => $paid_by,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );

                        if ($this->stripe_m->add_payment($transaction)) {
                            $this->notifications->send_payment_notification($this->CompanyClient_id, $invoice,
                                ($charge->amount / 100));
                            $this->session->set_flashdata('msg_status', 'success');
                            $this->session->set_flashdata('message',
                                'Payment received and applied to Invoice ' . $invoice_ref);
                            redirect('billing/invoice/' . $invoice);

                        } else {
                            $this->session->set_flashdata('msg_status', 'success');
                            $this->session->set_flashdata('message',
                                'Payment not recorded in the database. Please contact the system Admin.');
                            redirect('billing/invoice/' . $invoice);
                        }


                    } else { // Charge was not paid!
                        $this->session->set_flashdata('msg_status', 'error');
                        $this->session->set_flashdata('message',
                            'Your payment could NOT be processed (i.e., you have not been charged) because the payment system rejected the transaction. You can try again or use another card.');
                        redirect('billing/invoice/' . $invoice);
                    }

                } catch (Stripe_CardError $e) {
                    // Card was declined.
                    $e_json = $e->getJsonBody();
                    $err = $e_json['error'];
                    $errors['stripe'] = $err['message'];
                    echo "<br />" . $e_json;
                    print_r($e_json);
                } catch (Stripe_ApiConnectionError $e) {
                    // Network problem, perhaps try again.

                } catch (Stripe_InvalidRequestError $e) {
                    // You screwed up in your programming. Shouldn't happen!
                    echo "You screwed up in your programming. Shouldn't happen!!";
                } catch (Stripe_ApiError $e) {
                    // Stripe's servers are down!
                    echo "Stripe's servers are down!";

                } catch (Stripe_CardError $e) {
                    // Something else that's not the customer's fault.
                    echo "Something else that's not the customer's fault";
                }

            } // A user form submission error occurred, handled below.


        } // Form submission.

    }

}
