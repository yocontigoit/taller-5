<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/10
 * Time: 01:51 PM
 */
class Settings extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();
        $this->load->model('setting_m');

        $this->data['active'] = ($this->uri->segment(2)) ? $this->uri->segment(2) : 'general';
        $this->data['show_setting_menu'] = true;
        $this->data['SCRIPT_PAGE'] = true;

    }

    /**
     * list all settings
     */
    function index()
    {

        $this->data['content'] = 'settings/settings';
        $this->data['title'] = 'Configuraciones | Generales';
        $this->data['_PAGE'] = $this->load->view('pages/general', $this->data, true);
        $this->load->view('_main_layout', $this->data);

    }

    /**
     * list all general settings and update
     */
    function general()
    {

        $this->data['title'] = 'General | Settings';

        if ($this->input->post()) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $setting_id = $this->input->post('general_id');

            $file_name = '';

            if (file_exists($_FILES['logo_file']['tmp_name']) || is_uploaded_file($_FILES['logo_file']['tmp_name'])) {
                $file_name = $this->_upload_logo($_POST);

            }

            $param = array(
                "company_name" => $this->input->post('config_name'),
                "company_owner" => $this->input->post('config_owner'),
                "company_address_1" => $this->input->post('config_address_1'),
                "company_address_2" => $this->input->post('config_address_2'),
                "company_address_3" => $this->input->post('config_address_3'),
                "company_telephone" => $this->input->post('config_telephone'),
                "company_reg_no" => $this->input->post('config_reg'),
                "company_vat_no" => $this->input->post('config_vat'),
                "company_slogan" => $this->input->post('config_slogan'),
                "company_email" => $this->input->post('config_email')
            );

            if ($file_name != '') {
                $attach_data = array("company_logo" => $file_name);
                $param = array_merge($param, $attach_data);
            }

            if ($this->setting_m->update_general($param, $setting_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('message_general_save_success'));
                redirect(current_url());
            }

        } else {

            $this->data['content'] = 'settings/settings';
            $this->data['title'] = 'General | Settings';

            $this->data['_PAGE'] = $this->load->view('pages/general', $this->data, true);
            $this->load->view('_main_layout', $this->data);

        }

    }

    /**
     * save company logo to directory
     * @param null $image_file
     * @return null|string
     */
    function _upload_logo($image_file = null)
    {

        $file_name = null;
        $success = false;
        if ($image_file) {
            $config['upload_path'] = './files/company_logo/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['max_width'] = '300';
            $config['max_height'] = '60';
            $config['remove_spaces'] = true;
            $config['file_name'] = 'company_logo';
            $config['overwrite'] = true;
            $config['max_size'] = '300';
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('logo_file')) {
                $data = $this->upload->data();
                $file_name = $data['file_name'];
                $success = true;
            } else {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', lang('messages_logo_add_error'));
                redirect('settings/general');
            }
        } else {
            $success = false;
        }

        return ($success) ? $file_name : '';
    }

    /**
     * view local setting and include update
     */
    function local()
    {
        $this->data['title'] = 'Local | Settings';

        if ($this->input->post()) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $param = array(
                "company_country" => $this->input->post('country_name'),
                "company_state" => $this->input->post('country_state'),
                "company_language" => $this->input->post('language'),
                "company_currency" => $this->input->post('currency_name'),
                "company_tax" => $this->input->post('tax_name'),
                "company_time_zone" => $this->input->post('time_zone'),
                "company_company_id" => $this->Company_id
            );

            if ($this->setting_m->update_local($param)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('message_local_save_success'));
                redirect('settings/local');
            }

        } else {

            $this->data['content'] = 'settings/settings';
            $this->data['title'] = 'General | Settings';
            $this->data['countries'] = $this->setting_m->get_countries();
            $this->data['currencies'] = $this->setting_m->get_currencies();
            $this->data['time_zones'] = $this->setting_m->get_time_zones();
            $this->data['taxes'] = $this->setting_m->get_taxes();
            $this->data['_PAGE'] = $this->load->view('pages/local', $this->data, true);
            $this->data['show_countries'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);

        }
    }

    /*
     * view and edit email settings
     */

    /**
     * select country states
     */
    function get_states()
    {

        if ($this->input->post('country_id')) {
            $country_id = intval($this->input->post('country_id'));
            $country_states = array();
            $country_states = $this->setting_m->get_country_states($country_id);
            echo json_encode($country_states);
            exit();
        }
    }

    function email()
    {
        $this->data['title'] = 'Email | Settings';

        if ($this->input->post()) {

            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $is_test = ($this->input->post('is_test') == 'on') ? 1 : 0;
            $setting_id = $this->input->post('email_id');
            $param = array(
                "mail_protocol" => $this->input->post('smtp_protocol'),
                "mail_type" => $this->input->post('email_format'),
                "smtp_servername" => $this->input->post('smtp_server'),
                "smtp_hostname" => $this->input->post('smtp_domain'),
                "smtp_username" => $this->input->post('smtp_username'),
                "smtp_password" => $this->input->post('smtp_password'),
                "smtp_encryption" => $this->input->post('smtp_encryption'),
                "smtp_port" => $this->input->post('smtp_port'),
                "mail_outgoing" => $this->input->post('mail_outgoing'),
                "is_test" => $is_test,
                "test_email" => $this->input->post('test_email')
            );

            if ($this->setting_m->update_emails($param, $setting_id)) {
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('message_email_save_success'));
                redirect('settings/email');

            }

        } else {

            $this->data['show_emails'] = true;
            $this->data['SCRIPT_PAGE'] = true;

            $this->data['content'] = 'settings/settings';
            $this->data['title'] = 'General | Settings';
            $this->data['_PAGE'] = $this->load->view('pages/email', $this->data, true);
            $this->load->view('_main_layout', $this->data);

        }
    }

    /**
     * list finance settings
     */
    function finance()
    {

        $this->data['title'] = 'Finance | Settings';

        $active_page = $this->input->get('page', true) ? $this->input->get('page', true) : 'currency';

        switch ($active_page) {

            case 'currency':
                $this->data['active_page'] = $active_page;
                $this->data['currencies'] = $this->setting_m->get_currencies();
                $this->data['_FIN_PAGE'] = $this->load->view('group/currency', $this->data, true);
                break;

            case 'tax':
                $this->data['active_page'] = $active_page;
                $this->data['taxes'] = $this->setting_m->get_taxes();
                $this->data['_FIN_PAGE'] = $this->load->view('group/tax', $this->data, true);
                break;
            case 'expense':
                $this->data['active_page'] = $active_page;
                $this->data['categories'] = $this->setting_m->get_categories();
                $this->data['_FIN_PAGE'] = $this->load->view('group/categories', $this->data, true);
                break;
            default:
                $this->data['active_page'] = $active_page;
                $this->data['currencies'] = $this->setting_m->get_currencies();
                $this->data['_FIN_PAGE'] = $this->load->view('group/currency', $this->data, true);

                break;
        }

        $this->data['content'] = 'settings/settings';
        $this->data['_PAGE'] = $this->load->view('pages/finance', $this->data, true);
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * list all templates settings
     */
    function templates()
    {

        $this->data['title'] = 'Template | Settings';
        $this->data['page_title'] = 'Account Templates';
        $this->data['active_page'] = 'account';

        $template_email = $this->input->get('email', true) ? $this->input->get('email', true) : '';
        $this->data['email_details'] = $this->setting_m->get_email_details($template_email);
        $this->data['_temp'] = $this->load->view('templates/account', $this->data, true);


        if ($this->input->post()) {

        } elseif ($this->input->get('page')) {

            $template_page = $this->input->get('page', true) ? $this->input->get('page', true) : 'account';

            $this->data['active_page'] = $template_page;


            switch ($template_page) {


                case 'account':

                    $this->data['page_title'] = 'Account Templates';
                    $this->data['_temp'] = $this->load->view('templates/account', $this->data, true);
                    break;

                case 'ticket':
                    $this->data['page_title'] = 'Ticket Templates';
                    $this->data['_temp'] = $this->load->view('templates/ticket', $this->data, true);
                    break;
                case 'invoice':

                    $this->data['page_title'] = 'Invoice Templates';
                    $this->data['_temp'] = $this->load->view('templates/invoice', $this->data, true);

                    break;
                case 'issues':

                    $this->data['page_title'] = 'Issues Templates';
                    $this->data['_temp'] = $this->load->view('templates/issues', $this->data, true);

                    break;

                case 'quotation':

                    $this->data['page_title'] = 'Quotation Templates';
                    $this->data['_temp'] = $this->load->view('templates/quotation', $this->data, true);
                    break;
                case 'estimates':

                    $this->data['page_title'] = 'Estimates Templates';
                    $this->data['_temp'] = $this->load->view('templates/estimates', $this->data, true);
                    break;
                case 'project':

                    $this->data['page_title'] = 'Project Templates';
                    $this->data['_temp'] = $this->load->view('templates/project', $this->data, true);
                    break;

            }

        }

        $this->data['show_editor'] = true;

        $this->data['content'] = 'settings/settings';
        $this->data['title'] = 'General | Settings';

        $this->data['email'] = $template_email;
        $this->data['_PAGE'] = $this->load->view('pages/templates', $this->data, true);
        $this->load->view('_main_layout', $this->data);

    }

    /**
     * update template settings
     */
    function update_templates()
    {

        $template_email = $this->input->post('email');

        if ($this->input->post() && $template_email) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $param = array(
                "template_title" => $this->input->post('email_subject'),
                "template_body" => $this->input->post('email_body')
            );

            if ($this->setting_m->update_templates($param, $template_email)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_template_save_success'));
                redirect('settings/templates');
            }

        }
    }

    /**
     * edit tax data
     * @param null $tax_id
     */
    function edit_tax($tax_id = null)
    {

        if ($this->input->post() && $tax_id) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $tax_enable = ($this->input->post('tax_enable') == 'on') ? 1 : 0;
            $param = array(
                "name" => $this->input->post('tax_name'),
                "rate" => $this->input->post('tax_rate'),
                "active" => $tax_enable,
                "company_id" => $this->Company_id
            );

            if ($this->setting_m->update_tax($param, $tax_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_tax_save_success'));
                redirect('settings/finance');
            }

        } elseif ($this->input->post() && !$tax_id) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $tax_enable = ($this->input->post('tax_enable') == 'on') ? 1 : 0;
            $param = array(
                "name" => $this->input->post('tax_name'),
                "rate" => $this->input->post('tax_rate'),
                "active" => $tax_enable,
                "company_id" => $this->Company_id
            );

            if ($this->setting_m->add_tax($param)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_tax_add_success'));
                redirect('settings/finance');
            }
        } elseif ($tax_id) {

            $this->data['tax'] = $this->setting_m->get_selected_tax($tax_id);
            $this->load->view('modal/edit_tax', $this->data);
        } else {//add
            $this->load->view('modal/add_tax', $this->data);
        }

    }

    /**
     * view, edit and add support
     */
    function support()
    {

        $this->data['title'] = 'Support | Settings';


        if ($this->input->get('option')) {
            $option_page = $this->input->get('option', true) ? $this->input->get('option', true) : '';

            switch ($option_page) {

                case 'add':
                    $this->load->view('modal/add_department', $this->data);
                    break;
            }
        }

        $this->data['content'] = 'settings/settings';
        if ($this->input->post()) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }

            $department_data = array(
                "department_desc" => $this->input->post('department_name'),
                "company_id" => $this->Company_id
            );

            if ($this->setting_m->add_department($department_data)) {
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('message_department_add_success'));
                redirect('settings/support');
            }

        } elseif (!$this->input->get('option')) {

            $this->data['departments'] = $this->setting_m->get_support_department();
            $this->data['config_location'] = null;

            $this->data['_PAGE'] = $this->load->view('pages/support', $this->data, true);
            $this->load->view('_main_layout', $this->data);

        }
    }

    /**
     * save invoice, quote and project settings
     */
    function invoice()
    {

        $this->data['content'] = 'settings/settings';
        $this->data['title'] = 'Invoice | Settings';

        if ($this->input->post()) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }

            $setting_id = $this->input->post('setting_id');
            $file_name = '';

            if (file_exists($_FILES['logo_file']['tmp_name']) || is_uploaded_file($_FILES['logo_file']['tmp_name'])) {
                $file_name = $this->_upload_invoice_logo($_POST);
            }

            if ($setting_id) {

                $invoice_data = array(
                    "invoice_prefix" => $this->input->post('invoice_prefix'),
                    "invoice_due_after" => $this->input->post('invoice_due_after'),
                    "invoice_terms" => $this->input->post('invoice_terms'),
                    "estimate_prefix" => $this->input->post('estimate_prefix'),
                    "quotation_prefix" => $this->input->post('quotation_prefix'),
                    "project_prefix" => $this->input->post('project_prefix'),
                    "company_id" => $this->Company_id
                );

                if ($file_name != '') {
                    $attach_data = array("invoice_logo" => $file_name);
                    $invoice_data = array_merge($invoice_data, $attach_data);
                }
                if ($this->setting_m->update_trans_settings($invoice_data, $setting_id)) {
                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('message_transaction_settings_save_success'));
                    redirect(current_url());
                }

            } else {

                $invoice_data = array(
                    "invoice_prefix" => $this->input->post('invoice_prefix'),
                    "invoice_terms" => $this->input->post('invoice_terms'),
                    "invoice_logo" => $file_name,
                    "estimate_prefix" => $this->input->post('estimate_prefix'),
                    "quotation_prefix" => $this->input->post('quotation_prefix'),
                    "project_prefix" => $this->input->post('project_prefix'),
                    "company_id" => $this->Company_id
                );
                if ($this->setting_m->add_trans_settings($invoice_data)) {
                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('message_transaction_settings_add_success'));
                    redirect(current_url());
                }
            }

        } else {
            $this->data['locations'] = null;
            $this->data['config_location'] = null;
            $this->data['_PAGE'] = $this->load->view('pages/invoice', $this->data, true);
            $this->load->view('_main_layout', $this->data);

        }
    }


    /*
     * update roles
     */

    function _upload_invoice_logo($image_file = null)
    {

        $file_name = null;
        $success = false;
        if ($image_file) {
            $config['upload_path'] = './files/transaction_logos/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['remove_spaces'] = true;
            $config['file_name'] = 'invoice_logo';
            $config['overwrite'] = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('logo_file')) {
                $data = $this->upload->data();
                $file_name = $data['file_name'];
                $success = true;
            } else {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', lang('messages_logo_add_error'));
                redirect('settings/invoice');
            }
        } else {
            $success = false;
        }

        return ($success) ? $file_name : '';
    }

    /**
     * view role page
     */
    function role()
    {

        $this->data['content'] = 'settings/settings';
        $this->data['title'] = 'Permission | Settings';

        if ($this->input->post()) {

        } else {

            $this->data['locations'] = null;
            $this->data['config_location'] = null;
            $this->data['user_roles'] = $this->setting_m->get_roles($this->Company_id);
            $this->data['_PAGE'] = $this->load->view('pages/roles', $this->data, true);
            $this->load->view('_main_layout', $this->data);
        }
    }

    function edit_roles($role_id = null)
    {

        $this->data['content'] = 'settings/settings';
        $this->data['title'] = 'Permission | Settings';

        if ($this->input->post()) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $permission_data = $this->input->post("permissions") != false ? $this->input->post("permissions") : array();
            $permission_action_data = $this->input->post("permissions_actions") != false ? $this->input->post("permissions_actions") : array();
            if ($this->setting_m->update_role($role_id, $this->Company_id, $permission_data, $permission_action_data)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('message_role_save_success'));
                redirect('settings/edit_roles/' . $role_id);
            }


        } elseif (!$role_id) {

            $this->data['role_id'] = -5;
            $this->data['user_role_info'] = $this->setting_m->get_new_role_info();
            $this->data['all_modules'] = $this->module_m->get_all_modules();
            $this->load->view('modal/add_role', $this->data);

        } else {

            $this->data['role_id'] = $role_id;
            $this->data['show_roles_edit'] = true;
            $this->data['user_role_info'] = $this->setting_m->get_role_info($role_id);
            $this->data['all_modules'] = $this->module_m->get_all_modules();

            $this->data['_PAGE'] = $this->load->view('modify/roles', $this->data, true);
            $this->load->view('_main_layout', $this->data);
        }
    }

    /**
     * list payments modules
     * @param null $option
     */
    function payment($option = null)
    {

        $this->data['content'] = 'settings/settings';
        $this->data['title'] = 'Payment | Settings';
        $this->data['locations'] = null;
        $this->data['config_location'] = null;

        switch ($option) {
            case 'paypal':
                $this->data['_PAGE'] = $this->load->view('gateways/paypal', $this->data, true);
                break;
            case 'payfast':
                $this->data['_PAGE'] = $this->load->view('gateways/payfast', $this->data, true);
                break;
            case 'stripepay':
                $this->data['_PAGE'] = $this->load->view('gateways/stripe', $this->data, true);
                break;
            case 'skrill':
                $this->data['_PAGE'] = $this->load->view('gateways/skrill', $this->data, true);
                break;
            case 'braintree':
                $this->data['_PAGE'] = $this->load->view('gateways/braintree', $this->data, true);
                break;
            case 'bitcoin':
                $this->data['_PAGE'] = $this->load->view('gateways/bitcoin', $this->data, true);
                break;
            case 'checkout':
                $this->data['_PAGE'] = $this->load->view('gateways/checkout', $this->data, true);
                break;
            case 'banktransfer':
                $this->data['_PAGE'] = $this->load->view('gateways/banktransfer', $this->data, true);
                break;

            default:
                $this->data['_PAGE'] = $this->load->view('pages/payments', $this->data, true);
        }


        $this->load->view('_main_layout', $this->data);

    }

    /**
     * edit/add category
     * @param null $category_id
     */
    function edit_category($category_id = null)
    {

        if ($this->input->post() && $category_id) {//update
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $param = array(
                "class_name" => $this->input->post('class_name')
            );

            if ($this->setting_m->update_category($param, $category_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_category_save_success'));
                redirect('settings/finance');

            }

        } elseif ($this->input->post() && !$category_id) {

            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $param = array(
                "class_name" => $this->input->post('class_name')
            );

            if ($this->setting_m->add_category($param)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_category_add_success'));
                redirect('settings/finance');

            }

        } elseif ($category_id) {

            $this->data['category'] = $this->setting_m->get_selected_category($category_id);
            $this->load->view('modal/edit_category', $this->data);
        } else {
            $this->load->view('modal/add_category', $this->data);
        }
    }

    /**
     * edit/ add currency
     * @param null $currency_id
     */
    function edit_currency($currency_id = null)
    {

        if ($this->input->post() && $currency_id) {//update
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $currency_enable = ($this->input->post('currency_enable') == 'on') ? 1 : 0;

            $param = array(
                "name" => $this->input->post('currency_name'),
                "iso_code" => $this->input->post('iso_code'),
                "iso_code_num" => $this->input->post('numeric_iso'),
                "curreny_symbol" => $this->input->post('currency_symbol'),
                "conversion_rate" => $this->input->post('exchange_rate'),
                "active" => $currency_enable,
                "company_id" => $this->Company_id
            );

            if ($this->setting_m->update_currency($param, $currency_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_currency_save_success'));
                redirect('settings/finance');

            }

        } elseif ($this->input->post() && !$currency_id) {

            $currency_enable = ($this->input->post('currency_enable') == 'on') ? 1 : 0;

            $param = array(
                "name" => $this->input->post('currency_name'),
                "iso_code" => $this->input->post('iso_code'),
                "iso_code_num" => $this->input->post('numeric_iso'),
                "curreny_symbol" => $this->input->post('currency_symbol'),
                "conversion_rate" => $this->input->post('exchange_rate'),
                "active" => $currency_enable,
                "company_id" => $this->Company_id
            );

            if ($this->setting_m->add_currency($param)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_currency_add_success'));
                redirect('settings/finance');

            }

        } elseif ($currency_id) {

            $this->data['currency'] = $this->setting_m->get_selected_currency($currency_id);
            $this->load->view('modal/edit_currency', $this->data);
        } else {
            $this->load->view('modal/add_currency', $this->data);
        }
    }

    /**
     * load system setting and saving changes
     */
    function system()
    {

        $this->data['content'] = 'settings/settings';
        $this->data['title'] = 'System | Settings';

        if ($this->input->post()) {


            $demo_enable = ($this->input->post('demo_enable') == 'on') ? 1 : 0;
            $allow_client_registration = ($this->input->post('client_registration') == 'on') ? 1 : 0;
            $system_id = $this->input->post('settings_id');
            $param = array(
                "purchase_code" => $this->input->post('purchase_code'),
                "max_file_size" => $this->input->post('file_max_size'),
                "allowed_files" => $this->input->post('allowed_files'),
                "currency_symbol_placement" => $this->input->post('symbol_position'),
                "company_thousand_separator" => $this->input->post('thousand_separator'),
                "currency_decimal_point" => $this->input->post('decimal_separator'),
                "cron_key" => $this->input->post('cron_key'),
                "company_google_analytics" => $this->input->post('google_analytics'),
                "is_demo" => $demo_enable,
                "client_registration" => $allow_client_registration
            );

            if ($this->setting_m->update_system($param, $system_id)) {
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_system_save_success'));
                redirect('settings/system');
            }

        } else {

            $this->data['locations'] = null;
            $this->data['config_location'] = null;
            $this->data['_PAGE'] = $this->load->view('pages/system', $this->data, true);
            $this->load->view('_main_layout', $this->data);
        }
    }

    /*
     * set cron jobs settings
     */

    /**
     *change theme settings and logos
     */
    function theme()
    {

        $this->data['content'] = 'settings/settings';
        $this->data['title'] = 'Theme | Settings';

        if ($this->input->post()) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $system_id = $this->input->post('settings_id');

            $site_favicon = '';
            $profile_background = '';
            $login_background = '';
            if (file_exists($_FILES['site_favicon']['tmp_name']) || is_uploaded_file($_FILES['site_favicon']['tmp_name'])) {
                $site_favicon = $this->_upload_favicon($this->input->post());

                if ($site_favicon == null) {
                    $this->session->set_flashdata('msg_status', 'error');
                    $this->session->set_flashdata('message', lang('messages_favicon_add_error'));
                    redirect('settings/theme');
                }
            }
            if (file_exists($_FILES['profile_background']['tmp_name']) || is_uploaded_file($_FILES['profile_background']['tmp_name'])) {
                $profile_background = $this->_upload_user_bg($this->input->post());

                if ($profile_background == null) {
                    $this->session->set_flashdata('msg_status', 'error');
                    $this->session->set_flashdata('message', lang('messages_banner_add_error'));
                    redirect('settings/theme');
                }
            }
            if (file_exists($_FILES['login_background']['tmp_name']) || is_uploaded_file($_FILES['login_background']['tmp_name'])) {
                $login_background = $this->_upload_login_bg($this->input->post());

                if ($login_background == null) {
                    $this->session->set_flashdata('msg_status', 'error');
                    $this->session->set_flashdata('message', lang('messages_background_add_error'));
                    redirect('settings/theme');
                }
            }


            $param = array(
                "site_title" => $this->input->post('site_title')
            );

            if ($site_favicon != '') {
                $attach_data = array("site_favicon" => $site_favicon);
                $param = array_merge($param, $attach_data);
            }

            if ($profile_background != '') {
                $attach_data = array("profile_background" => $profile_background);
                $param = array_merge($param, $attach_data);
            }

            if ($login_background != '') {
                $attach_data = array("login_background" => $login_background);
                $param = array_merge($param, $attach_data);
            }

            if ($this->setting_m->update_theme($param, $system_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_theme_save_success'));
                redirect('settings/theme');

            }

        } else {

            $this->data['_PAGE'] = $this->load->view('pages/theme', $this->data, true);
            $this->data['sort_menu'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);
        }

    }

    function _upload_favicon($files)
    {
        $file_name = null;
        if ($files) {
            $config['upload_path'] = './assets/images/';
            $config['allowed_types'] = 'jpg|jpeg|png|ico';
            $config['max_width'] = '300';
            $config['max_height'] = '300';
            $config['file_name'] = 'site_favicon';
            $config['overwrite'] = true;
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('site_favicon')) {
                $data = $this->upload->data();
                $file_name = $data['file_name'];

                return $file_name;
            } else {
                return $file_name;
            }
        } else {
            return $file_name;
        }
    }

    /**
     * upload user profile background image to server
     * @param $files
     * @return null
     */
    function _upload_user_bg($files)
    {
        $file_name = null;
        if ($files) {
            $config['upload_path'] = './assets/images/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['max_width'] = '1920';
            $config['max_height'] = '503';
            $config['file_name'] = 'profile_background';
            $config['overwrite'] = true;
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('profile_background')) {
                $data = $this->upload->data();
                $file_name = $data['file_name'];

                return $file_name;
            } else {
                return $file_name;
            }
        } else {
            return $file_name;
        }
    }

    /**
     * upload login image to server
     * @param $files
     * @return null
     */
    function _upload_login_bg($files)
    {

        $file_name = null;
        if ($files) {
            $config['upload_path'] = './assets/images/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['max_width'] = '0';
            $config['max_height'] = '0';
            $config['file_name'] = 'login_background';
            $config['remove_spaces'] = true;
            $config['overwrite'] = true;
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('login_background')) {
                $filedata = $this->upload->data();
                $file_name = $filedata['file_name'];

                return $file_name;
            } else {
                return $file_name;
            }
        } else {
            return $file_name;
        }
    }

    /*
     * update stripe payments settings
     */

    /**
     * order sidebar menus
     */
    public function order_ajax()
    {
        // Save order from ajax call
        if (isset($_POST['sortable'])) {
            $this->module_m->save_order($_POST['sortable']);
        }

        // Fetch all pages
        $this->data['pages'] = $this->module_m->get_nested();

        // Load view
        $this->load->view('pages/order_ajax', $this->data);

    }

    function cron()
    {
        $this->data['content'] = 'settings/settings';
        $this->data['title'] = 'Cron | Settings';

        if ($this->input->post()) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }

            $is_cron_active = ($this->input->post('cron_enable') == 'on') ? 1 : 0;
            $create_recur = ($this->input->post('create_recur') == 'on') ? 1 : 0;
            $email_recur = ($this->input->post('email_recur') == 'on') ? 1 : 0;
            $auto_db_backup = ($this->input->post('auto_db_backup') == 'on') ? 1 : 0;
            $email_overdue = ($this->input->post('email_overdue') == 'on') ? 1 : 0;

            $system_id = $this->input->post('settings_id');

            $param = array(
                "is_cron_active" => $is_cron_active,
                "create_recur" => $create_recur,
                "email_recur" => $email_recur,
                "email_overdue" => $email_overdue,
                "auto_db_backup" => $auto_db_backup
            );

            if ($this->setting_m->update_cron($param, $system_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_system_save_success'));
                redirect('settings/system');

            }

        } else {
            $this->data['_PAGE'] = $this->load->view('pages/cron', $this->data, true);
            $this->load->view('_main_layout', $this->data);
        }
    }

    /**
     * update paypal settings
     */
    function update_paypal_payment()
    {

        if ($this->input->post()) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $rules = $this->form_validator->paypal_add_rules;
            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == false) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'Make sure you fill all required fields');
                redirect($_SERVER['HTTP_REFERER']);
            } else {

                $paypal_id = intval($this->input->post('paypal_id'));
                $data_array = array(
                    "paypal_mail" => $this->input->post('paypal_mail'),
                    "paypal_ipn" => $this->input->post('paypal_ipn'),
                    "paypal_success" => $this->input->post('paypal_success'),
                    "paypal_cancel" => $this->input->post('paypal_cancel'),
                    "paypal_active" => $this->input->post('paypal_active')
                );

                if ($this->setting_m->update_paypal($data_array, $paypal_id)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_paypal_save_success'));
                    redirect('settings/payment/paypal');
                }
            }
        } else {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', lang('messages_paypal_save_error'));
            redirect('settings/payment/paypal');
        }

    }

    /**
     * update bank payments settings
     */
    function update_bank_payment()
    {

        if ($this->input->post()) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $rules = $this->form_validator->bank_add_rules;
            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == false) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', validation_errors());
                redirect($_SERVER['HTTP_REFERER']);
            } else {

                $bank_id = intval($this->input->post('bank_id'));
                $data_array = array(
                    "bank_name" => $this->input->post('bank_name'),
                    "bank_branch" => $this->input->post('bank_branch'),
                    "bank_branch_code" => $this->input->post('bank_branch_code'),
                    "bank_acc" => $this->input->post('bank_acc'),
                    "bank_reference" => $this->input->post('bank_reference'),
                    "active" => $this->input->post('active')
                );

                if ($this->setting_m->update_bank($data_array, $bank_id)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_bank_save_success'));
                    redirect('settings/payment/bank');
                }
            }
        } else {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', lang('messages_bank_save_error'));
            redirect('settings/payment/bank');
        }

    }

    /*
     * save invoice image to directory
     */

    /**
     * update payfast payments settings
     */
    function update_payfast_payment()
    {

        if ($this->input->post()) {

            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $rules = $this->form_validator->payfast_add_rules;
            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == false) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('form_error',
                    validation_errors('<span style="color:red">', '</span><br>'));
                $this->session->set_flashdata('message', 'Make sure you fill all required fields');
                redirect($_SERVER['HTTP_REFERER']);
            } else {


                $payfast_id = intval($this->input->post('payfast_id'));
                $data_array = array(
                    "payfast_merchant_id" => $this->input->post('payfast_merchant_id'),
                    "payfast_merchant_key" => $this->input->post('payfast_merchant_key'),
                    "payfast_passphrase" => $this->input->post('payfast_passphrase'),
                    "payfast_success" => $this->input->post('payfast_success'),
                    "payfast_cancel" => $this->input->post('payfast_cancel'),
                    "payfast_ipn" => $this->input->post('payfast_ipn'),
                    "payfast_active" => $this->input->post('payfast_active'),
                );

                if ($this->setting_m->update_payfast($data_array, $payfast_id)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_payfast_save_success'));
                    redirect('settings/payment/payfast');
                }
            }
        } else {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', lang('messages_payfast_save_error'));
            redirect('settings/payment/payfast');
        }

    }

    function update_stripe_payment()
    {

        if ($this->input->post()) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $rules = $this->form_validator->stripe_add_rules;
            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == false) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('form_error',
                    validation_errors('<span style="color:red">', '</span><br>'));
                $this->session->set_flashdata('message', 'Make sure you fill all required fields');
                redirect($_SERVER['HTTP_REFERER']);
            } else {


                $stripe_id = intval($this->input->post('stripe_id'));
                $data_array = array(
                    "stripe_private_key" => $this->input->post('stripe_private_key'),
                    "stripe_public_key" => $this->input->post('stripe_public_key')
                );

                if ($this->setting_m->update_stripe($data_array, $stripe_id)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_stripe_save_success'));
                    redirect('settings/payment/stripe');
                }
            }
        } else {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', lang('messages_stripe_save_error'));
            redirect('settings/payment/stripe');
        }
    }

    /**
     * update skrill payments settings
     */
    function  update_skrill_payment()
    {

        if ($this->input->post()) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }

            $rules = $this->form_validator->skrill_add_rules;
            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == false) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('form_error',
                    validation_errors('<span style="color:red">', '</span><br>'));
                $this->session->set_flashdata('message', 'Make sure you fill all required fields');
                redirect($_SERVER['HTTP_REFERER']);
            } else {


                $skrill_id = intval($this->input->post('skrill_id'));
                $data_array = array(
                    "skrill_mail" => $this->input->post('skrill_mail'),
                    "skrill_ipn" => $this->input->post('skrill_ipn'),
                    "skrill_success" => $this->input->post('skrill_success'),
                    "skrill_cancel" => $this->input->post('skrill_cancel'),
                    "skrill_active" => $this->input->post('skrill_active')

                );


                if ($this->setting_m->update_skrill($data_array, $skrill_id)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_skrill_save_success'));
                    redirect('settings/payment/skrill');
                }
            }
        } else {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', lang('messages_skrill_save_error'));
            redirect('settings/payment/skrill');
        }
    }

    /**
     * update bitcoin payments settings
     */
    function  update_bitcoin_payment()
    {

        if ($this->input->post()) {
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }

            $rules = $this->form_validator->bitcoin_add_rules;
            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == false) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('form_error',
                    validation_errors('<span style="color:red">', '</span><br>'));
                $this->session->set_flashdata('message', 'Make sure you fill all required fields');
                redirect($_SERVER['HTTP_REFERER']);
            } else {

                $bitcoin_id = intval($this->input->post('bitcoin_id'));
                $data_array = array(
                    "bitcoin_root_url" => $this->input->post('bitcoin_root_url'),
                    "bitcoin_root_receive" => $this->input->post('bitcoin_root_receive'),
                    "bitcoin_secret_key" => $this->input->post('bitcoin_secret_key'),
                    "bitcoin_address" => $this->input->post('bitcoin_address'),
                    "bitcoin_api_key" => $this->input->post('bitcoin_api_key'),
                    "bitcoin_success" => $this->input->post('bitcoin_success')

                );

                if ($this->setting_m->update_bitcoin($data_array, $bitcoin_id)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_bitcoin_save_success'));
                    redirect('settings/payment/bitcoin');
                }
            }
        } else {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', lang('messages_bitcoin_save_error'));
            redirect('settings/payment/bitcoin');
        }
    }

    /**
     *update checkout payments
     */
    function update_checkout_payment()
    {

        if ($this->input->post()) {

            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }

            $rules = $this->form_validator->checkout_add_rules;
            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == false) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('form_error',
                    validation_errors('<span style="color:red">', '</span><br>'));
                $this->session->set_flashdata('message', 'Make sure you fill all required fields');
                redirect($_SERVER['HTTP_REFERER']);
            } else {

                $checkout_id = intval($this->input->post('checkout_id'));
                $data_array = array(
                    "checkout_seller_id" => $this->input->post('checkout_seller_id'),
                    "checkout_private_key" => $this->input->post('checkout_private_key'),
                    "checkout_public_key" => $this->input->post('checkout_public_key'),
                    "checkout_active" => $this->input->post('checkout_active')

                );

                if ($this->setting_m->update_checkout($data_array, $checkout_id)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_checkout_save_success'));
                    redirect('settings/payment/checkout');
                }
            }
        } else {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', lang('messages_checkout_save_error'));
            redirect('settings/payment/checkout');
        }

    }

    /**
     * list all database backups
     */
    function backup()
    {
        $this->data['content'] = 'settings/settings';
        $this->data['title'] = 'Backup | Settings';

        if ($this->input->post()) {

        } else {

            $this->data['locations'] = null;
            $this->data['database_backups'] = $this->setting_m->company_backups($this->Company_id);


            $this->data['_PAGE'] = $this->load->view('pages/backup', $this->data, true);
            $this->load->view('_main_layout', $this->data);
        }
    }

    /**
     * download backup
     * @param $file_name
     */
    public function download_file($file_name)
    {
        $this->load->helper('download');

        $fullpath = './files/backup/' . $file_name;

        if ($fullpath) {
            $data = file_get_contents($fullpath); // Read the file's contents
            force_download($file_name, $data);
        } else {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', 'Database download error.');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    /**
     * Delete database backup
     * @param null $file_name
     * @param null $backup_id
     */
    function delete_backup($file_name = null, $backup_id = null)
    {

        if ($this->config->item('system')->is_demo == 1) {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', 'function disabled in demo mode');
            redirect($_SERVER['HTTP_REFERER']);
        }
        $Path = "";
        if ($this->setting_m->remove_backup($file_name, $backup_id)) {
            $Path = './files/backup/' . $file_name;
            if (file_exists($Path)) {
                unlink($Path);
            }
            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', 'Database backup deleted successfully.');
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', 'Database could not delete database.');
            redirect($_SERVER['HTTP_REFERER']);
        }

    }

    /**
     * restore backed up database
     */
    function mysql_restore()
    {
        if ($_POST) {

            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $this->load->helper('file');
            $this->load->helper('unzip');
            $this->load->database();

            $config['upload_path'] = './files/backup/';
            $config['allowed_types'] = 'zip';
            $config['max_size'] = '9000';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors('', ' ');
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', $error);
                redirect('settings/backup');
            } else {
                $data = array('upload_data' => $this->upload->data());
                $backup = "files/backup/" . $data['upload_data']['file_name'];

            }


            if (!unzip($backup, "files/backup/", true, true)) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'Failed to unzip mysql backup.');

            } else {
                $this->load->dbforge();
                $backup = str_replace('.zip', '', $backup);
                $file_content = file_get_contents($backup . ".sql");
                $this->db->query('USE ' . $this->db->database . ';');
                foreach (explode(";\n", $file_content) as $sql) {
                    $sql = trim($sql);
                    if ($sql) {
                        $this->db->query($sql);
                    }
                }

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', 'Database restored successfully.');


            }
            unlink($backup . ".sql");
            unlink($backup . ".zip");
            redirect('settings/backup');
        } else {

            redirect('settings/backup');

        }
    }

    /**
     * lists and checks for updates
     */
    function system_update()
    {

        $this->data['content'] = 'settings/settings';
        $this->data['title'] = 'System | Updates';
        $this->data['updates'] = check_updates();

        $this->data['_PAGE'] = $this->load->view('pages/updates', $this->data, true);
        $this->load->view('_main_layout', $this->data);

    }

    /**
     * Download update from the server
     * @return bool
     */
    function download_update()
    {
        if ($this->config->item('system')->is_demo == 1) {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', 'function disabled in demo mode');
            redirect($_SERVER['HTTP_REFERER']);
        }
        $purchase_code = $this->config->item('system')->purchase_code;

        $build = $this->uri->segment(3);

        if (!isset($build)) {
            return false;
        }

        $path = "./files/updates/zest-" . $build . "-update.zip";
        $url = UPDATE_URL . "updates/updates.php?build=" . $build . "&code=" . $purchase_code;

        $fp = fopen($path, "w+");
        $ch = curl_init(str_replace(" ", "%20", $url));
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        $curldata = curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        $size = filesize($path);

        $this->db->where('build', $build)->update('system_updates', array(
            'is_downloaded' => 1
        ));


        $this->session->set_flashdata('msg_status', 'success');
        $this->session->set_flashdata('message', "File downloaded successfully");
        redirect('settings/system_update');
    }

    /**
     * install downloaded update/ this replaces old file with new ones
     * @return bool
     */
    function install_update()
    {
        if ($this->config->item('system')->is_demo == 1) {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', 'function disabled in demo mode');
            redirect($_SERVER['HTTP_REFERER']);
        }
        $build = $this->uri->segment(3);
        $version = $this->uri->segment(4);

        if (!isset($build)) {
            return false;
        }

        $zip = new ZipArchive;
        if ($zip->open("./file/updates/zest-" . $build . "-update.zip") === true) {
            $res = $zip->extractTo('./');
            $zip->close();
            $data['json'] = array(
                "result" => $res,
                "build" => $build,
                "message" => "File extracted successfully",
            );
            $this->db->where('settings_id', 1)->update('settings_system',
                array('build' => $build, 'version' => $version));
            $this->db->where('build', $build)->update('system_updates', array(
                'installed' => 1
            ));

            $this->upgrate_database($build);
            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', $data);
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', "Could not open file");
            redirect($_SERVER['HTTP_REFERER']);
        }

    }

    /**
     * downloads database update and updates database
     * @param $build
     * @return bool
     */
    function upgrade_database($build)
    {
        $purchase_code = $this->config->item('system')->purchase_code;
        $this->load->dbforge();
        $file_content = remote_get_contents(UPDATE_URL . "db/upgrade.php?build=" . $build . "code=" . $purchase_code);
        if ($file_content) {
            $this->db->query('USE ' . $this->db->database . ';');
            foreach (explode(";\n", $file_content) as $sql) {
                $sql = trim($sql);
                if ($sql) {
                    $this->db->query($sql);
                }
            }
        }

        return true;
    }

    /**
     * backup application
     */
    function app_backup()
    {
        if ($this->config->item('system')->is_demo == 1) {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', 'function disabled in demo mode');
            redirect($_SERVER['HTTP_REFERER']);
        }
        $this->mysql_backup(false);
        if (!is_dir('./files/backup/')) {

            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', 'Please create backup folder under files directory.');
            redirect($_SERVER['HTTP_REFERER']);

        }
        if (!is_writeable("./files/backup/")) {

            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', 'Unable to write to backup folder.');
            redirect($_SERVER['HTTP_REFERER']);

        }

        $this->load->library('zip');
        $path = './';
        $this->zip->read_dir($path);
        if (!$this->zip->archive('./files/backup/zest_full_backup_' . date('Y-m-d') . '.zip')) {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', 'Could not backup.');
            redirect($_SERVER['HTTP_REFERER']);
        }

        $this->session->set_flashdata('msg_status', 'success');
        $this->session->set_flashdata('message', 'Backup created successfully.');

        redirect($_SERVER['HTTP_REFERER']);

    }

    /**
     * backup app database
     * @param bool $store_backup
     */
    function mysql_backup($store_backup = true)
    {
        if ($this->config->item('system')->is_demo == 1) {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', 'function disabled in demo mode');
            redirect($_SERVER['HTTP_REFERER']);
        }
        $this->load->helper('file');
        $this->load->dbutil();

        $file_name = 'database-full-backup_' . date('Y-m-d_H-i') . '.zip';
        $prefs = array(
            'format' => 'zip',             // gzip, zip, txt
            'filename' => $file_name,
            'add_drop' => true,              // Whether to add DROP TABLE statements to backup file
            'add_insert' => true,              // Whether to add INSERT data to backup file
            'newline' => "\n"               // Newline character used in backup file
        );
        $backup = $this->dbutil->backup($prefs);

        if (!write_file('./files/backup/' . $file_name, $backup)) {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', 'The resource/backup folder is not writable.');
            redirect($_SERVER['HTTP_REFERER']);
        } elseif ($store_backup) {
            //Save to database..
            $data_array = array(
                'filename' => $file_name,
                'format' => $prefs['format'],
                'upload_path' => './files/backup/' . $file_name,
                'upload_date' => date('Y-m-d H:i:s'),
                'company_id' => $this->Company_id,
            );

            if ($this->setting_m->database_backup($data_array)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', 'Database backup successfully.');
                redirect($_SERVER['HTTP_REFERER']);

            }

        }
    }

}