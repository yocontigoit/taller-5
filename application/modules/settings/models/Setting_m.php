<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/19
 * Time: 11:39 AM
 */
class Setting_M extends CI_Model
{

    function get_roles()
    {
        return $this->db->get('roles')->result();
    }


    function get_role_info($role_id)
    {

        $this->db->from('roles');
        $this->db->where('role_id', $role_id);

        return $this->db->get()->row();


    }

    function get_new_role_info()
    {
        $role = new stdClass();

        $fields = $this->db->list_fields('roles');

        foreach ($fields as $field) {
            $role->$field = '';
        }

        return $role;

    }

    function update_role($role_id, $company_id, $permission_data, $permission_action_data)
    {

        $success = false;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();
        //First lets clear out any permissions the employee currently has.
        $success = $this->db->delete('permissions', array('role_id' => $role_id));

        //Now insert the new permissions
        if ($success) {
            foreach ($permission_data as $allowed_module) {
                $success = $this->db->insert('permissions',
                    array(
                        'module_id' => $allowed_module,
                        'role_id' => $role_id
                    ));
            }
        }

        //First lets clear out any permissions actions the employee currently has.
        $success = $this->db->delete('permissions_actions', array('role_id' => $role_id));

        //Now insert the new permissions actions
        if ($success) {
            foreach ($permission_action_data as $permission_action) {
                list($module, $action) = explode('|', $permission_action);
                $success = $this->db->insert('permissions_actions',
                    array(
                        'module_id' => $module,
                        'action_id' => $action,
                        'role_id' => $role_id
                    ));
            }
        }
        $this->db->trans_complete();

        return $success;
    }

    function get_countries()
    {
        return $this->db->get('country')->result();
    }

    function get_currencies()
    {

        return $this->db->get('settings_currency')->result();
    }

    function get_categories()
    {

        //is_deleted
        return $this->db->get('classification')->result();
    }

    function get_selected_category($category_id)
    {

        return $this->db->where('class_id', $category_id)->get('classification')->row();
    }

    function get_selected_currency($currency_id)
    {

        return $this->db->where('currency_id', $currency_id)->get('settings_currency')->row();
    }

    function get_selected_tax($tax_id)
    {

        return $this->db->where('tax_id', $tax_id)->get('settings_tax')->row();
    }

    function  get_time_zones()
    {

        return $this->db->get('timezone')->result();

    }


    function  get_taxes()
    {

        return $this->db->get('settings_tax')->result();
    }


    function get_country_states($country_id = null)
    {


        return $this->db->where('id_country', $country_id)->get('country_state')->result();
    }

    function  get_email_details($email_template)
    {


        $row = $this->db->where('template_email', $email_template)->get('settings_email_template')->row();
        if ($row) {
            return $row;
        } else {

            $template_obj = new stdClass();

            $fields = $this->db->list_fields('settings_email_template');

            foreach ($fields as $field) {
                $template_obj->$field = '';
            }

            return $template_obj;
        }


    }

    function add_department($data = array())
    {
        $success = false;
        $success = $this->db->insert('settings_support', $data);

        return $success;
    }

    function add_trans_settings($data = array())
    {

        $success = false;
        $success = $this->db->insert('settings_invoice', $data);

        return $success;

    }

    function update_paypal($data = array(), $setting_id = null)
    {

        $success = false;
        $success = $this->db->update('settings_paypal', $data, array('setting_id' => intval($setting_id)));

        return $success;
    }

    function update_bank($data = array(), $setting_id = null)
    {

        $success = false;
        $success = $this->db->update('settings_bank', $data, array('setting_id' => intval($setting_id)));

        return $success;
    }

    function update_stripe($data = array(), $setting_id = null)
    {
        $success = false;
        $success = $this->db->update('settings_stripe', $data, array('setting_id' => intval($setting_id)));

        return $success;
    }

    function update_payfast($data = array(), $setting_id = null)
    {

        $success = false;
        $success = $this->db->update('settings_payfast', $data, array('setting_id' => intval($setting_id)));

        return $success;
    }

    function update_checkout($data = array(), $setting_id = null)
    {

        $success = false;
        $success = $this->db->update('settings_2checkout', $data, array('setting_id' => intval($setting_id)));

        return $success;
    }

    function update_skrill($data = array(), $setting_id = null)
    {

        $success = false;
        $success = $this->db->update('settings_skrill', $data, array('setting_id' => intval($setting_id)));

        return $success;
    }

    function update_bitcoin($data = array(), $setting_id = null)
    {

        $success = false;
        $success = $this->db->update('settings_bitcoin', $data, array('setting_id' => intval($setting_id)));

        return $success;
    }

    function update_trans_settings($data = array(), $setting_id = null)
    {

        $success = false;
        $success = $this->db->update('settings_invoice', $data, array('setting_id' => intval($setting_id)));

        return $success;

    }

    function get_support_department()
    {

        return $this->db->get('settings_support')->result();
    }

    function update_currency($data = array(), $currency_id = null)
    {
        $success = false;
        $success = $this->db->update('settings_currency', $data, array('currency_id' => intval($currency_id)));

        return $success;
    }

    function update_category($data = array(), $category_id = null)
    {
        $success = false;
        $success = $this->db->update('classification', $data, array('class_id' => intval($category_id)));

        return $success;
    }

    function update_system($data = array(), $system_id = null)
    {

        $success = false;
        $success = $this->db->update('settings_system', $data, array('settings_id' => intval($system_id)));

        return $success;
    }

    function update_cron($data = array(), $system_id = null)
    {

        $success = false;
        $success = $this->db->update('settings_system', $data, array('settings_id' => intval($system_id)));

        return $success;
    }

    function add_currency($data = array())
    {

        $success = false;
        $success = $this->db->insert('settings_currency', $data);

        return $success;

    }

    function add_category($data = array())
    {

        $success = false;
        $success = $this->db->insert('classification', $data);

        return $success;

    }

    function  add_tax($data = array())
    {

        $success = false;
        $success = $this->db->insert('settings_tax', $data);

        return $success;
    }

    function update_tax($data = array(), $tax_id = null)
    {
        $success = false;
        $success = $this->db->update('settings_tax', $data, array('tax_id' => intval($tax_id)));

        return $success;

    }

    function update_emails($data = array(), $email_id = null)
    {

        $success = false;
        $success = $this->db->update('settings_email', $data, array('setting_id' => intval($email_id)));

        return $success;
    }

    function update_templates($data = array(), $email_group = null)
    {

        $success = false;
        $success = $this->db->update('settings_email_template', $data, array('template_email' => $email_group));

        return $success;
    }

    function update_local($data = array())
    {


        $success = false;
        $success = $this->db->update('settings_local', $data, array('setting_id' => 1));

        if ($success) {
            return $success;

        }
        $success = $this->db->insert('settings_local', $data);

        return $success;


    }

    function update_general($data = array(), $general_id = null)
    {

        $success = false;
        $success = $this->db->update('settings_company', $data, array('setting_id' => intval($general_id)));

        return $success;

    }


    function update_theme($data = array(), $theme_id = null)
    {

        $success = false;
        $success = $this->db->update('settings_theme', $data, array('setting_id' => intval($theme_id)));

        return $success;

    }

    function company_backups()
    {

        return $this->db->get('settings_database')->result();
    }


    function database_backup($data = array())
    {

        $success = false;
        $success = $this->db->insert('settings_database', $data);

        return $success;
    }

    function remove_backup($file_name = null, $backup_id = null)
    {
        $success = false;
        $success = $this->db->delete('settings_database', array('filename' => $file_name, 'setting_id' => $backup_id));

        return $success;
    }

}