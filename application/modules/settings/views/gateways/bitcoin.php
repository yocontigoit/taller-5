<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/03
 * Time: 11:58 AM
 */
?>
<fieldset>
    <legend><i class="fa fa-payfast"></i><?php echo "Bitcoin"; ?></legend>
    <?php echo form_open('settings/update_bitcoin_payment',
        array('id' => 'payfast_form', 'class' => 'panel form-horizontal form-bordered')); ?>
    <?php echo $this->session->flashdata('form_errors'); ?>
    <div class="panel-body">
        <div class="form-group header bgcolor-default">
            <div class="col-md-12">
                <h4><?php echo "Bitcoin Settings"; ?></h4>
            </div>
        </div>
        <input class="form-control" type="hidden" name="bitcoin_id"
               value="<?php echo $this->config->item('bitcoin')->setting_id; ?>"/>

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo "Root Url"; ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="bitcoin_root_url"
                       value="<?php echo $this->config->item('bitcoin')->bitcoin_root_url; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo "Api Receive Url"; ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="bitcoin_root_receive"
                       value="<?php echo $this->config->item('bitcoin')->bitcoin_root_receive; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo "Secret Key"; ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="bitcoin_secret_key"
                       value="<?php echo $this->config->item('bitcoin')->bitcoin_secret_key; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo "Address"; ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="bitcoin_address"
                       value="<?php echo $this->config->item('bitcoin')->bitcoin_address; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo "Api Key"; ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="bitcoin_api_key"
                       value="<?php echo $this->config->item('bitcoin')->bitcoin_api_key; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo "IPN Url"; ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="bitcoin_success"
                       value="<?php echo $this->config->item('bitcoin')->bitcoin_success; ?>"/>
            </div>
        </div>

    </div>
    <hr/>
    <button type="submit" name="submit" value="editTask" class="btn btn-success btn-lg btn-icon mt-10"><i
            class="fa fa-check-square-o"></i> <?php echo lang('form_button_save'); ?></button>
    </form>


</fieldset>