<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/03
 * Time: 11:58 AM
 */
?>
<fieldset>
    <legend><i class="fa fa-payfast"></i><?php echo "Payfast"; ?></legend>
    <?php echo form_open('settings/update_payfast_payment',
        array('id' => 'payfast_form', 'class' => 'panel form-horizontal form-bordered')); ?>
    <?php echo $this->session->flashdata('form_errors'); ?>
    <div class="panel-body">
        <div class="form-group header bgcolor-default">
            <div class="col-md-12">
                <h4><?php echo "Payfast Settings"; ?></h4>
            </div>
        </div>
        <input class="form-control" type="hidden" name="payfast_id"
               value="<?php echo $this->config->item('payfast')->setting_id; ?>"/>

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo "Merchant Id"; ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="payfast_merchant_id"
                       value="<?php echo $this->config->item('payfast')->payfast_merchant_id; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo "Merchant Key"; ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="payfast_merchant_key"
                       value="<?php echo $this->config->item('payfast')->payfast_merchant_key; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo "Passphrase"; ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="payfast_passphrase"
                       value="<?php echo $this->config->item('payfast')->payfast_passphrase; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo "Return Url"; ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="payfast_success"
                       value="<?php echo $this->config->item('payfast')->payfast_success; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo "Cancel Url "; ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="payfast_cancel"
                       value="<?php echo $this->config->item('payfast')->payfast_cancel; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo "Notify url "; ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="payfast_ipn"
                       value="<?php echo $this->config->item('payfast')->payfast_ipn; ?>"/>
            </div>
        </div>


        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo "Gateway Active*"; ?></label>

            <div class="col-sm-8">
                <input type="checkbox" name="payfast_active"
                       class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($this->config->item('payfast')->payfast_active == 1) ? 'checked' : ''; ?> />
            </div>
        </div>


    </div>
    <hr/>
    <button type="submit" name="submit" value="editTask" class="btn btn-success btn-lg btn-icon mt-10"><i
            class="fa fa-check-square-o"></i> <?php echo lang('form_button_save'); ?></button>
    </form>


</fieldset>