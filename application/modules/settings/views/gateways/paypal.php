<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2/12/2015
 * Time: 9:13 PM
 */
?>
<fieldset>
    <legend><i class="fa fa-paypal"></i><?php echo lang('label_paypal'); ?></legend>
    <?php echo form_open('settings/update_paypal_payment',
        array('id' => 'paypal_form', 'class' => 'panel form-horizontal form-bordered')); ?>
    <div class="panel-body">
        <div class="form-group header bgcolor-default">
            <div class="col-md-12">
                <h4><?php echo lang('label_paypal_settings'); ?></h4>
            </div>
        </div>
        <input class="form-control" type="hidden" name="paypal_id"
               value="<?php echo $this->config->item('paypal')->setting_id; ?>"/>

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo lang('label_paypal_email'); ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="paypal_mail"
                       value="<?php echo $this->config->item('paypal')->paypal_mail; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo lang('label_paypal_ipn'); ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="paypal_ipn"
                       value="<?php echo $this->config->item('paypal')->paypal_ipn; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo lang('label_paypal_success'); ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="paypal_success"
                       value="<?php echo $this->config->item('paypal')->paypal_success; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo lang('label_paypal_cancel'); ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="paypal_cancel"
                       value="<?php echo $this->config->item('paypal')->paypal_cancel; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo lang('label_paypal_active'); ?></label>

            <div class="col-sm-8">
                <input type="checkbox" name="paypal_active"
                       class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($this->config->item('paypal')->paypal_active == 1) ? 'checked' : ''; ?> />
            </div>
        </div>


    </div>
    <hr/>
    <button type="submit" name="submit" value="editTask" class="btn btn-success btn-lg btn-icon mt-10"><i
            class="fa fa-check-square-o"></i> <?php echo lang('form_button_save'); ?></button>
    </form>

</fieldset>

