<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/03
 * Time: 12:25 PM
 */

?>
<fieldset>
    <legend><i class="fa fa-paypal"></i><?php echo lang('label_stripe'); ?></legend>
    <?php echo form_open('settings/update_stripe_payment',
        array('id' => 'stripe_form', 'class' => 'panel form-horizontal form-bordered')); ?>
    <div class="panel-body">
        <div class="form-group header bgcolor-default">
            <div class="col-md-12">
                <h4><?php echo lang('label_stripe_settings'); ?></h4>
            </div>
        </div>
        <input class="form-control" type="hidden" name="stripe_id"
               value="<?php echo $this->config->item('stripe')->setting_id; ?>"/>

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo lang('label_stripe_private_key'); ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="stripe_private_key"
                       value="<?php echo $this->config->item('stripe')->stripe_private_key; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo lang('label_stripe_stripe_public_key'); ?></label>

            <div class="col-sm-8">
                <input class="form-control" type="text" name="stripe_public_key"
                       value="<?php echo $this->config->item('stripe')->stripe_public_key; ?>"/>
            </div>
        </div>
    </div>
    <hr/>
    <button type="input" name="submit" class="btn btn-success btn-lg btn-icon mt-10"><i
            class="fa fa-check-square-o"></i> <?php echo lang('form_button_save'); ?></button>
    </form>

</fieldset>