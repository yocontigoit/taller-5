<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/11/08
 * Time: 10:56 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                        class="fa fa-times"></i></span><span class="sr-only"><?php echo "close"; ?></span></button>
            <h4 class="modal-title"><?php echo lang('label_add_currency'); ?></h4>
        </div>
        <?php echo form_open('settings/edit_currency', array('id' => 'add-tax')); ?>
        <div class="modal-body">
            <!---start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Currency name </label>
                        <input type="text" class="form-control " name="currency_name" placeholder="name">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="rate">ISO code </label>
                        <input type="text" class="form-control " name="iso_code" placeholder="rate">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="rate">Numeric ISO code</label>
                        <input type="text" class="form-control " name="numeric_iso" placeholder="rate">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="rate">Symbol</label>
                        <input type="text" class="form-control " name="currency_symbol" placeholder="rate">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="rate">Exchange rate </label>
                        <input type="text" class="form-control " name="exchange_rate" placeholder="rate">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label><?php echo 'Enable'; ?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="checkbox" name="currency_enable"
                                       class="ios-switch ios-switch-success ios-switch-lg"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--end -->

        </div>
        <div class="modal-footer">
            <button type="input" name="submit" class="btn btn-success btn-icon"><i
                    class="fa fa-check-square-o"></i><?php echo lang('form_button_save'); ?></button>
            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i
                    class="fa fa-times-circle-o"></i><?php echo lang('form_button_cancel'); ?></button>
        </div>
        </form>
    </div>
</div>
<script type="text/javascript">

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

</script>
