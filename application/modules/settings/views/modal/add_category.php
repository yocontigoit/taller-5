<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/11/08
 * Time: 10:56 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                        class="fa fa-times"></i></span><span class="sr-only"><?php echo "close"; ?></span></button>
            <h4 class="modal-title"><?php echo lang('label_add_category'); ?></h4>
        </div>
        <?php echo form_open('settings/edit_category', array('id' => 'add-category')); ?>
        <div class="modal-body">
            <!---start-->
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="class_name">Category Name </label>
                        <input type="text" class="form-control " name="class_name" placeholder="name">
                    </div>
                </div>
            </div>

            <!--end -->

        </div>
        <div class="modal-footer">
            <button type="input" name="submit" class="btn btn-success btn-icon"><i
                    class="fa fa-check-square-o"></i><?php echo lang('form_button_save'); ?></button>
            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i
                    class="fa fa-times-circle-o"></i><?php echo lang('form_button_cancel'); ?></button>
        </div>
        </form>
    </div>
</div>

