<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/03/24
 * Time: 03:03 PM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                        class="fa fa-times"></i></span><span class="sr-only"><?php echo "close"; ?></span></button>
            <h4 class="modal-title"><?php echo lang('label_add_roles'); ?></h4>
        </div>
        <?php echo form_open('settings/edit_roles', array('id' => 'edit-roles')); ?>
        <div class="modal-body">
            <!---start-->

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control " name="tax_name" placeholder="name">
                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Role Name:
													<span class="required">
														 *
													</span>
                        </label>


                        <?php echo form_input('RoleName', set_value('role_name', $user_role_info->role_name),
                            'placeholder="role name" class="form-control"'); ?>


                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Description:
													<span class="required">
														 *
													</span>
                        </label>

                        <?php echo form_input('RoleDescription',
                            set_value('role_description', $user_role_info->role_name),
                            'placeholder="role name" class="form-control"'); ?>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="col-md-2 control-label">Modules Permissions:
													<span class="required">
														 *
													</span>
                    </label>

                    <div class="col-md-10">
                        <div class="form-control height-auto">
                            <div class="scroller" style="height:350px;" data-always-visible="1">
                                <ul class="list-unstyled">
                                    <?php
                                    if (count($all_modules)) {
                                        foreach ($all_modules->result() as $module) {
                                            $checkbox_options = array(
                                                'name' => 'permissions[]',
                                                'value' => $module->module_id,
                                                'checked' => $this->ion_auth_model->has_module_permission($module->module_id,
                                                    $role_id),
                                                'class' => 'module_checkboxes '
                                            );

                                            ?>
                                            <li>
                                                <?php echo form_checkbox($checkbox_options); ?>
                                                <span
                                                    class="text-info"><?php echo $this->lang->line('module_' . $module->module_id);?>
                                                    :</span>
                                                <span
                                                    class="text-warning"><?php echo $this->lang->line('module_' . $module->module_id . '_desc');?></span>

                                                <ul class="list-unstyled">
                                                    <?php
                                                    foreach ($this->module_act_m->get_module_actions($module->module_id)->result() as $module_action) {
                                                        $checkbox_options = array(
                                                            'name' => 'permissions_actions[]',
                                                            'value' => $module_action->module_id . "|" . $module_action->action_id,
                                                            'checked' => $this->ion_auth_model->has_module_action_permission($module->module_id,
                                                                $module_action->action_id, $role_id)
                                                        );

                                                        ?>
                                                        <li>
                                                            <?php echo form_checkbox($checkbox_options); ?>
                                                            <span
                                                                class="text-info"><?php echo $this->lang->line($module_action->action_name_key);?></span>
                                                        </li>
                                                    <?php
                                                    }
                                                    ?>

                                                </ul>
                                            </li>
                                        <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
										<span class="help-block text-red">
															 select one or more modules/permissions...
										</span>
                    </div>
                </div>

            </div>
            <!--end -->

        </div>
        <div class="modal-footer">
            <button type="input" name="submit" class="btn btn-success btn-icon"><i
                    class="fa fa-check-square-o"></i><?php echo lang('form_button_save'); ?></button>
            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i
                    class="fa fa-times-circle-o"></i><?php echo lang('form_button_cancel'); ?></button>
        </div>
        </form>
    </div>
</div>
<script type="text/javascript">

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

</script>