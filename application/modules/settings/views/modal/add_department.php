<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2/18/2016
 * Time: 7:32 PM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                        class="fa fa-times"></i></span><span class="sr-only"><?php echo "close"; ?></span></button>
            <h4 class="modal-title"><?php echo lang('label_add_department'); ?></h4>
        </div>

        <?php echo form_open('settings/support', array('id' => 'add-support')); ?>
        <div class="modal-body">
            <!---start-->

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="department">Department</label>
                        <input type="text" class="form-control " name="department_name" placeholder="support">
                    </div>
                </div>

            </div>

            <!--end -->

        </div>
        <div class="modal-footer">
            <button type="input" name="submit" class="btn btn-success btn-icon"><i
                    class="fa fa-check-square-o"></i><?php echo lang('form_button_save'); ?></button>
            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i
                    class="fa fa-times-circle-o"></i><?php echo lang('form_button_cancel'); ?></button>
        </div>
        </form>
    </div>
</div>
