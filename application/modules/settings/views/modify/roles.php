<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/03/24
 * Time: 09:54 AM
 */
?>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <fieldset>
            <legend><i class="fa fa-cog"></i>Role Settings
            </legend>
            <?php echo form_open('settings/edit_roles/' . $user_role_info->role_id,
                'class="form-horizontal form-row-seperated"'); ?>

            <div class="form-group">
                <label class="col-md-2 control-label">Role Name:
													<span class="required">
														 *
													</span>
                </label>

                <div class="col-md-10">
                    <?php echo form_input('RoleName', set_value('RoleName', $user_role_info->role_name),
                        'placeholder="role name" class="form-control"'); ?>

                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Description:
													<span class="required">
														 *
													</span>
                </label>

                <div class="col-md-10">
                    <?php echo form_input('RoleDescription', set_value('RoleDescription', $user_role_info->role_name),
                        'placeholder="role name" class="form-control"'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Modules Permissions:
													<span class="required">
														 *
													</span>
                </label>

                <div class="col-md-10">
                    <div class="form-control height-auto">
                        <div class="scroller category-wrap" data-always-visible="1">
                            <ul class="list-unstyled">
                                <?php
                                if (count($all_modules)) {
                                    foreach ($all_modules->result() as $module) {
                                        $checkbox_options = array(
                                            'name' => 'permissions[]',
                                            'value' => $module->module_id,
                                            'checked' => $this->ion_auth_model->has_module_permission($module->module_id,
                                                $role_id),
                                            'class' => 'module_checkboxes '
                                        );

                                        ?>
                                        <li>
                                            <?php echo form_checkbox($checkbox_options); ?>
                                            <span
                                                class="text-info"><?php echo $this->lang->line('module_' . $module->module_id);?>
                                                :</span>
                                            <span
                                                class="text-warning"><?php echo $this->lang->line('module_' . $module->module_id . '_desc');?></span>

                                            <ul class="list-unstyled">
                                                <?php
                                                foreach ($this->module_act_m->get_module_actions($module->module_id)->result() as $module_action) {
                                                    $checkbox_options = array(
                                                        'name' => 'permissions_actions[]',
                                                        'value' => $module_action->module_id . "|" . $module_action->action_id,
                                                        'checked' => $this->ion_auth_model->has_module_action_permission($module->module_id,
                                                            $module_action->action_id, $role_id)
                                                    );

                                                    ?>
                                                    <li>
                                                        <?php echo form_checkbox($checkbox_options); ?>
                                                        <span
                                                            class="text-info"><?php echo $this->lang->line($module_action->action_name_key);?></span>
                                                    </li>
                                                <?php
                                                }
                                                ?>

                                            </ul>
                                        </li>
                                    <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
										<span class="help-block text-red">
															 select one or more modules/permissions...
										</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-success btn-block btn-lg"><i
                            class="fa fa-sign-in"></i> <?php echo lang('form_button_update'); ?></button>
                </div>
            </div>

            <?php echo form_close(); ?>
        </fieldset>
    </div>
</div>