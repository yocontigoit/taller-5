<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 3/10/2016
 * Time: 7:15 PM
 */
?>
<div class="btn-group btn-group-sm pull-right">
    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-tag"></i>
        More Options
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">

        <li>
            <a href="?page=<?php echo $active_page; ?>&email=estimate_mail">Estimate Mail</a>
        </li>
        <li>
            <a href="?page=<?php echo $active_page; ?>&email=estimate_assigned">Estimate Assigned</a>
        </li>
        <li>
            <a href="?page=<?php echo $active_page; ?>&email=estimate_status">Estimate Status</a>
        </li>
        <li>
            <a href="?page=<?php echo $active_page; ?>&email=estimate_converted">Estimate converted to Invoice</a>
        </li>


    </ul>
</div>
