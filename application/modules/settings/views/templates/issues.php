<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 3/10/2016
 * Time: 7:15 PM
 */
?>
<div class="btn-group btn-group-sm pull-right">
    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-tag"></i>
        More Options
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">

        <li>
            <a href="?page=<?php echo $active_page; ?>&email=issue_assigned">Issue Assigned</a>
        </li>
        <li>
            <a href="?page=<?php echo $active_page; ?>&email=issue_status">Issue Status</a>
        </li>
        <li>
            <a href="?page=<?php echo $active_page; ?>&email=issue_comment">Issue Comments</a>
        </li>
        <li>
            <a href="?page=<?php echo $active_page; ?>&email=issue_file">Issue File</a>
        </li>
        <li>
            <a href="?page=<?php echo $active_page; ?>&email=issue_mail">Issue Mail</a>
        </li>

    </ul>
</div>
