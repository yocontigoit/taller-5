<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/09/14
 * Time: 03:01 PM
 */
?>
<div class="btn-group btn-group-sm pull-right">
    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-tag"></i>
        More Options
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">

        <li>
            <a href="?page=<?php echo $active_page; ?>&email=quotation_mail">Quotation Mail</a>
        </li>
        <li>
            <a href="?page=<?php echo $active_page; ?>&email=quotation_assigned">Quotation Assigned</a>
        </li>
        <li>
            <a href="?page=<?php echo $active_page; ?>&email=quotation_status">Quotation Status</a>
        </li>
        <li>
            <a href="?page=<?php echo $active_page; ?>&email=quotation_estimated">Quotation converted to Estimate</a>
        </li>


    </ul>
</div>