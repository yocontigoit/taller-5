<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/09/14
 * Time: 03:01 PM
 */
?>
<div class="btn-group btn-group-sm pull-right">
    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-tag"></i>
        More Options
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">

        <li>
            <a title="Email Invoice" href="?page=<?php echo $active_page; ?>&email=invoice_message">Invoice Message</a>
        </li>
        <li>
            <a title="Send Reminder" href="?page=<?php echo $active_page; ?>&email=invoice_reminder">Invoice
                Reminder</a>
        </li>
        <li>
            <a title="Client Payment" href="?page=<?php echo $active_page; ?>&email=payment_message">Client Payment</a>
        </li>

    </ul>
</div>