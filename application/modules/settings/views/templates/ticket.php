<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 3/10/2016
 * Time: 7:29 PM
 */
?>
<div class="btn-group btn-group-sm pull-right">
    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-tag"></i>
        More Options
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">

        <li>
            <a href="?page=<?php echo $active_page; ?>&email=ticket_client_email">Client Email</a>
        </li>

        <li>
            <a href="?page=<?php echo $active_page; ?>&email=ticket_reply_email">Reply Email</a>
        </li>
        <li>
            <a href="?page=<?php echo $active_page; ?>&email=ticket_staff_email">Staff Email</a>
        </li>


    </ul>
</div>