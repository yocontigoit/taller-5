<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/18
 * Time: 09:28 AM
 */
?>


<div class="the-box">
    <div class="row">
        <div class="col-lg-12">
            <!-- Page tabs -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span><?php echo lang('label_settings'); ?></span>
                    <span class="fa fa-bars fa-lg"></span>

                </button>
            </div>

            <div class="collapse navbar-collapse page-tabs" id="bs-example-navbar-collapse-1">
                <ul class="nav nav-tabs nav-settings">
                    <li class="<?php echo ($active == 'general') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('settings/general'); ?>"><i
                                class="fa fa-plane"></i><?php echo lang('label_general'); ?></a></li>
                    <li class="<?php echo ($active == 'local') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('settings/local'); ?>"><i
                                class="fa fa-wrench"></i><?php echo lang('label_local'); ?></a></li>
                    <li class="<?php echo ($active == 'email') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('settings/email'); ?>"><i
                                class="fa fa-paper-plane"></i><?php echo lang('label_email'); ?></a></li>
                    <li class="<?php echo ($active == 'templates') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('settings/templates'); ?>"><i
                                class="fa fa-sticky-note-o"></i><?php echo lang('label_templates'); ?></a></li>
                    <li class="<?php echo ($active == 'support') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('settings/support'); ?>"><i
                                class="fa fa-life-ring"></i><?php echo lang('label_support'); ?></a></li>
                    <li class="<?php echo ($active == 'invoice') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('settings/invoice'); ?>"><i
                                class="fa fa-usd"></i><?php echo lang('label_invoices'); ?></a></li>
                    <li class="<?php echo ($active == 'role') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('settings/role'); ?>"><i
                                class="fa fa-level-down"></i><?php echo lang('label_role'); ?></a></li>
                    <li class="<?php echo ($active == 'finance') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('settings/finance'); ?>"><i
                                class="fa fa-money"></i><?php echo lang('label_finance'); ?></a></li>
                    <li class="<?php echo ($active == 'payment') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('settings/payment'); ?>"><i
                                class="fa fa-sort-amount-desc"></i><?php echo lang('label_payments'); ?></a></li>
                    <li class="<?php echo ($active == 'theme') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('settings/theme'); ?>"><i
                                class="fa fa-code"></i><?php echo lang('label_theme'); ?></a></li>
                    <li class="<?php echo ($active == 'system') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('settings/system'); ?>"><i
                                class="fa fa-server"></i><?php echo lang('label_system'); ?></a></li>
                    <li class="<?php echo ($active == 'backup') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('settings/backup'); ?>"><i
                                class="fa fa-cloud-download"></i><?php echo lang('label_backup'); ?></a></li>
                    <li class="<?php echo ($active == 'cron') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('settings/cron'); ?>"><i
                                class="fa fa-rocket"></i><?php echo lang('label_cron'); ?></a></li>
                    <li class="<?php echo ($active == 'system_update') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('settings/system_update'); ?>"><i
                                class="fa fa-rocket"></i><?php echo lang('label_updates'); ?></a></li>


                </ul>

            </div>
            <!-- /page tabs -->


        </div>


    </div>
    <!-- /.row -->

    <div>
        <?php echo ($_PAGE) ? $_PAGE : null; ?>
    </div>
</div>

