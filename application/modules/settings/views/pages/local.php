<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/18
 * Time: 09:29 AM
 */
?>
<div class="row">
    <div class="col-sm-12">
        <fieldset>
            <legend><i class="fa fa-cog"></i><?php echo lang('label_local_setting'); ?>
            </legend>
            <?php echo form_open('settings/local',
                array('id' => 'generalForm', 'role' => 'form', 'class' => 'panel form-horizontal')); ?>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-country"><?php echo lang('label_country'); ?></label>

                <div class="col-sm-6">
                    <select class="form-control chosen-select" id="country_id" name="country_name">
                        <optgroup label="<?= lang('label_selected_country') ?>">
                            <option
                                value="<?php echo $this->config->item('local')->company_country; ?>"><?php echo get_country_name($this->config->item('local')->company_country); ?></option>
                        </optgroup>
                        <optgroup label="<?= lang('label_other_countries') ?>">
                            <?php foreach ($countries as $country): ?>
                                <option
                                    value="<?php echo $country->id_country; ?>"><?php echo $country->country_name; ?></option>
                            <?php endforeach; ?>
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-zone"><?php echo lang('label_state'); ?></label>

                <div class="col-sm-6">
                    <select id="states" class="form-control" name="country_state">

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-currency">
                    <span><?php echo lang('label_default_currency'); ?></span>
                </label>

                <div class="col-sm-6">


                    <select class="form-control" name="currency_name">
                        <optgroup label="<?php echo lang('label_selected_currency'); ?>">
                            <option
                                value="<?php echo $this->config->item('local')->company_currency; ?>"><?php echo get_currency_name($this->config->item('local')->company_currency); ?></option>
                        </optgroup>
                        <optgroup label="<?php echo lang('label_currency'); ?>">
                            <?php foreach ($currencies as $currency): ?>
                                <option
                                    value="<?php echo $currency->currency_id; ?>"><?php echo $currency->name; ?></option>
                            <?php endforeach; ?>
                        </optgroup>
                    </select>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-2 control-label" for="input-currency">
                    <span><?php echo lang('label_default_tax'); ?></span>
                </label>

                <div class="col-sm-6">


                    <select class="form-control" name="tax_name">
                        <optgroup label="<?php echo lang('label_selected_tax'); ?>">
                            <option
                                value="<?php echo $this->config->item('local')->company_tax; ?>"><?php echo get_tax_value($this->config->item('local')->company_tax); ?></option>
                        </optgroup>
                        <optgroup label="<?php echo lang('label_currency'); ?>">
                            <option value="">-none</option>
                            <?php foreach ($taxes as $tax): ?>
                                <option value="<?php echo $tax->tax_id; ?>"><?php echo $tax->rate; ?></option>
                            <?php endforeach; ?>
                        </optgroup>
                    </select>
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-sm-2"><?php echo lang('label_default_language'); ?></label>

                <div class="col-sm-6">
                    <?php $options = array();
                    if ($handle = opendir('application/language/')) {

                        while (false !== ($entry = readdir($handle))) {
                            if ($entry != "." && $entry != ".." && $entry != "index.html") {
                                $options[$entry] = ucwords($entry);
                            }
                        }
                        closedir($handle);
                    }
                    echo form_dropdown('language', $options, $this->config->item('local')->company_language,
                        'style="width:250px" class="chosen-select"'); ?>
                </div>

            </div>


            <div class="form-group">
                <label class="control-label col-sm-2"><?php echo lang('label_time_zone'); ?></label>

                <div class="col-sm-6">

                    <select class="form-control" name="time_zone">
                        <optgroup label="<?php echo lang('label_selected_time_zone'); ?>">
                            <option
                                value="<?php echo $this->config->item('local')->company_time_zone; ?>"><?php echo get_time_zone($this->config->item('local')->company_time_zone); ?></option>
                        </optgroup>
                        <optgroup label="<?php echo lang('label_time_zone'); ?>">

                            <?php foreach ($time_zones as $time_zone): ?>
                                <option
                                    value="<?php echo $time_zone->timezone_id; ?>"><?php echo $time_zone->name; ?></option>
                            <?php endforeach; ?>
                        </optgroup>
                    </select>
                </div>

            </div>


            <div class="row">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-success btn-block btn-lg"><i
                            class="fa fa-sign-in"></i> <?php echo lang('form_button_update'); ?></button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </fieldset>
    </div>
</div>

