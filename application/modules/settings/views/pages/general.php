<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/18
 * Time: 09:26 AM
 */
?>
<div class="row">
    <div class="col-sm-12">
        <fieldset>
            <legend><i class="fa fa-cog"></i><?php echo lang('label_general_setting'); ?>
            </legend>
            <?php echo form_open_multipart('settings/general',
                array('id' => 'generalForm', 'role' => 'form', 'class' => 'panel form-horizontal')); ?>
            <input type="hidden" name="general_id" value="<?php echo $this->config->item('company')->setting_id; ?>"
                   class="form-control"/>

            <div class="form-group">
                <label class="col-lg-3 control-label" for="input-name"><?php echo lang('label_company_name'); ?></label>

                <div class="col-lg-5">
                    <input type="text" name="config_name"
                           value="<?php echo $this->config->item('company')->company_name; ?>" id="input-name"
                           class="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label" for="input-owner"><?php echo lang('label_owner'); ?></label>

                <div class="col-lg-5">
                    <input type="text" name="config_owner"
                           value="<?php echo $this->config->item('company')->company_owner; ?>" id="input-owner"
                           class="form-control"/>

                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"
                       for="input-address"><?php echo lang('label_location_address_1'); ?></label>

                <div class="col-lg-5">
                    <input type="text" name="config_address_1"
                           value="<?php echo $this->config->item('company')->company_address_1; ?>" rows="5"
                           id="input-address" class="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"
                       for="input-address"><?php echo lang('label_location_address_2');; ?></label>

                <div class="col-lg-5">
                    <input type="text" name="config_address_2"
                           value="<?php echo $this->config->item('company')->company_address_2; ?>" rows="5"
                           id="input-address" class="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"
                       for="input-address"><?php echo lang('label_location_address_3');; ?></label>

                <div class="col-lg-5">
                    <input type="text" name="config_address_3"
                           value="<?php echo $this->config->item('company')->company_address_3; ?>" rows="5"
                           id="input-address" class="form-control"/>
                </div>
            </div>

            <div class="form-group ">
                <label class="col-lg-3 control-label" for="input-email"><?php echo lang('label_email'); ?></label>

                <div class="col-lg-5">
                    <input type="text" name="config_email"
                           value="<?php echo $this->config->item('company')->company_email; ?>"
                           placeholder="<?php echo ""; ?>" id="input-email" class="form-control"/>
                </div>
            </div>
            <div class="form-group ">
                <label class="col-lg-3 control-label" for="input-telephone"><?php echo lang('label_phone'); ?></label>

                <div class="col-lg-5">
                    <input type="text" name="config_telephone"
                           value="<?php echo $this->config->item('company')->company_telephone; ?>"
                           placeholder="<?php echo ""; ?>" id="input-telephone" class="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_registration_no'); ?></label>

                <div class="col-lg-5">
                    <input type="text" name="config_reg"
                           value="<?php echo $this->config->item('company')->company_reg_no; ?>"
                           placeholder="<?php echo ""; ?>" id="input-fax" class="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_vat_no'); ?></label>

                <div class="col-lg-5">
                    <input type="text" name="config_vat"
                           value="<?php echo $this->config->item('company')->company_vat_no; ?>"
                           placeholder="<?php echo ""; ?>" id="input-fax" class="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_slogan'); ?></label>

                <div class="col-lg-5">
                    <input type="text" name="config_slogan"
                           value="<?php echo $this->config->item('company')->company_slogan; ?>"
                           placeholder="<?php echo ""; ?>" id="input-fax" class="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"
                       for="input-image"><?php echo lang('label_company_logo'); ?></label>

                <div class="col-lg-5">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <input type="text" class="form-control" readonly>
                                                            <span class="input-group-btn">
                                                                <span class="btn btn-primary btn-file">
                                                                    Browse files<input type="file" multiple
                                                                                       name="logo_file">
                                                                </span>
                                                            </span>
                            </div>
                            <!-- /.input-group -->
                        </div>
                    </div>
                    <?php if ($this->config->item('company')->company_logo != '') : ?>
                        <div class="row" style="height: 150px; margin-bottom:15px;">
                            <div class="col-sm-12">
                                <div class="invoice_logo" style="height: 151px">
                                    <img
                                        src="<?php echo base_url('files/company_logo/' . $this->config->item('company')->company_logo); ?>"/>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>


                </div>
            </div>


            <div class="row">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-success btn-block btn-lg"><i
                            class="fa fa-sign-in"></i> <?php echo lang('form_button_update'); ?></button>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
</div>
