<div class="row">
    <div class="col-sm-8">
        <fieldset>
            <legend><i class="fa fa-code"></i><?php echo lang('label_theme_setting'); ?>
            </legend>
            <?php echo form_open_multipart('settings/theme',
                array('id' => 'generalForm', 'role' => 'form', 'class' => 'panel form-horizontal')); ?>

            <input type="hidden" name="settings_id" value="<?php echo $this->config->item('theme')->setting_id; ?>"
                   class="form-control"/>

            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_site_title'); ?></label>

                <div class="col-lg-5">
                    <input type="text" name="site_title" value="<?php echo $this->config->item('theme')->site_title; ?>"
                           class="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label"
                       for="input-image"><?php echo lang('label_site_favicon'); ?></label>

                <div class="col-lg-5">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <input type="text" class="form-control" readonly>
                                                            <span class="input-group-btn">
                                                                <span class="btn btn-primary btn-file">
                                                                    Browse files<input type="file" multiple
                                                                                       name="site_favicon">
                                                                </span>
                                                            </span>
                            </div>
                            <!-- /.input-group -->
                        </div>
                    </div>
                    <?php if ($this->config->item('theme')->site_favicon != '') : ?>
                        <div class="row" style="height: 150px; margin-bottom:15px;">
                            <div class="col-sm-12">
                                <div class="invoice_logo" style="height: 151px">
                                    <img
                                        src="<?php echo base_url('assets/images/' . $this->config->item('theme')->site_favicon); ?>"/>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label"
                       for="input-image"><?php echo lang('label_profile_background'); ?></label>

                <div class="col-lg-5">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <input type="text" class="form-control" readonly>
                                                            <span class="input-group-btn">
                                                                <span class="btn btn-primary btn-file">
                                                                    Browse files<input type="file" multiple
                                                                                       name="profile_background">
                                                                </span>
                                                            </span>
                            </div>
                            <!-- /.input-group -->
                        </div>
                    </div>
                    <?php if ($this->config->item('theme')->profile_background != '') : ?>
                        <div class="row" style="height: 150px; margin-bottom:15px;">
                            <div class="col-sm-12">
                                <div class="invoice_logo" style="height: 151px">
                                    <img
                                        src="<?php echo base_url('assets/images/' . $this->config->item('theme')->profile_background); ?>"/>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>


            <div class="form-group">
                <label class="col-lg-3 control-label" for="input-image"><?php echo lang('label_login_image'); ?></label>

                <div class="col-lg-5">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <input type="text" class="form-control" readonly>
                                                            <span class="input-group-btn">
                                                                <span class="btn btn-primary btn-file">
                                                                    Browse files<input type="file" multiple
                                                                                       name="login_background">
                                                                </span>
                                                            </span>
                            </div>
                            <!-- /.input-group -->
                        </div>
                    </div>
                    <?php if ($this->config->item('theme')->login_background != '') : ?>
                        <div class="row" style="height: 150px; margin-bottom:15px;">
                            <div class="col-sm-12">
                                <div class="invoice_logo" style="height: 151px">
                                    <img
                                        src="<?php echo base_url('assets/images/' . $this->config->item('theme')->login_background); ?>"/>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-success btn-block btn-lg"><i
                            class="fa fa-sign-in"></i> <?php echo lang('form_button_update'); ?></button>
                </div>
            </div>
        </fieldset>
        <?php echo form_close(); ?>
    </div>
    <div class="col-sm-4">
        <fieldset>
            <legend><i class="fa fa-code"></i><?php echo lang('label_menu_setting'); ?>
            </legend>
            <p class="alert alert-info">Drag to order pages and then click 'Save'</p>

            <div id="orderResult" class="dd"></div>
            <input type="button" id="save" value="Save" class="btn btn-primary"/>
        </fieldset>


    </div>
</div>