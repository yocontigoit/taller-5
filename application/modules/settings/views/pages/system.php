<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/09/14
 * Time: 11:40 AM
 */
?>

<div class="row">
    <div class="col-sm-12">
        <fieldset>
            <legend><i class="fa fa-cog"></i><?php echo lang('label_system_setting'); ?>
            </legend>
            <?php echo form_open('settings/system',
                array('id' => 'generalForm', 'role' => 'form', 'class' => 'panel form-horizontal')); ?>

            <input type="hidden" name="settings_id" value="<?php echo $this->config->item('system')->settings_id; ?>"/>

            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_purchase_code'); ?> <span
                        class="text-danger">*</span></label>

                <div class="col-lg-7">
                    <input class="form-control" type="text" name="purchase_code"
                           value="<?php echo $this->config->item('system')->purchase_code; ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label">
                    <?php echo lang('label_upload_size'); ?>
                    <span class="text-danger">*</span>
                </label>

                <div class="col-lg-3">
                    <input class="form-control" type="text" name="file_max_size"
                           value="<?php echo $this->config->item('system')->max_file_size; ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_allowed_files'); ?></label>

                <div class="col-lg-7">
                    <input class="form-control" type="text" name="allowed_files"
                           value="<?php echo $this->config->item('system')->allowed_files; ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_decimal_separator'); ?></label>

                <div class="col-lg-7">
                    <input class="form-control" type="text" name="decimal_separator"
                           value="<?php echo $this->config->item('system')->currency_decimal_point; ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_thousand_separator'); ?></label>

                <div class="col-lg-7">
                    <input class="form-control" type="text" name="thousand_separator"
                           value="<?php echo $this->config->item('system')->company_thousand_separator; ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_symbol_position'); ?></label>

                <div class="col-lg-4">
                    <select class="form-control" name="symbol_position">
                        <option value="">Select</option>
                        <?php if ($this->config->item('system')->currency_symbol_placement == 'before') { ?>
                            <option selected="selected" value="before">X 123.00</option>
                            <option value="afterspace">123.00 X</option>
                        <?php } elseif ($this->config->item('system')->currency_symbol_placement == 'afterspace') { ?>
                            <option value="before">X 123.00</option>
                            <option selected="selected" value="afterspace">123.00 X</option>
                        <?php } else { ?>
                            <option value="before">X 123.00</option>
                            <option value="afterspace">123.00 X</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_demo_mode'); ?></label>

                <div class="col-lg-4">
                    <input type="checkbox" name="demo_enable"
                           class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($this->config->item('system')->is_demo == 1) ? 'checked' : ''; ?> />

                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_client_registration'); ?></label>

                <div class="col-lg-4">
                    <input type="checkbox" name="client_registration"
                           class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($this->config->item('system')->client_registration == 1) ? 'checked' : ''; ?> />

                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label" for="input-google-analytics">
                    <span title=""><?php echo lang('label_google_code'); ?></span>
                </label>

                <div class="col-lg-7">
                    <textarea id="input-google-analytics" class="form-control" placeholder="Google Analytics Code"
                              rows="5"
                              name="google_analytics"><?php echo $this->config->item('system')->company_google_analytics; ?></textarea>
                </div>

            </div>


            <div class="row">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-success btn-block btn-lg"><i
                            class="fa fa-sign-in"></i> <?php echo lang('form_button_update'); ?></button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </fieldset>
    </div>
</div>