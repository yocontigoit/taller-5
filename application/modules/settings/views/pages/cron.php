<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/09/14
 * Time: 11:40 AM
 */
?>

<div class="row">
    <div class="col-sm-12">
        <fieldset>
            <legend><i class="fa fa-cog"></i><?php echo lang('label_cron_setting'); ?>
            </legend>
            <?php echo form_open('settings/cron',
                array('id' => 'generalForm', 'role' => 'form', 'class' => 'panel form-horizontal')); ?>

            <input type="hidden" name="settings_id" value="<?php echo $this->config->item('system')->settings_id; ?>"/>


            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_active_cron'); ?></label>

                <div class="col-lg-4">
                    <input type="checkbox" name="cron_enable"
                           class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($this->config->item('system')->is_cron_active == 1) ? 'checked' : ''; ?> />

                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_create_recur'); ?></label>

                <div class="col-lg-4">
                    <input type="checkbox" name="create_recur"
                           class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($this->config->item('system')->create_recur == 1) ? 'checked' : ''; ?> />

                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_invoice_reminder'); ?></label>

                <div class="col-lg-4">
                    <input type="checkbox" name="email_overdue"
                           class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($this->config->item('system')->email_overdue == 1) ? 'checked' : ''; ?> />

                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_email_recur'); ?></label>

                <div class="col-lg-4">
                    <input type="checkbox" name="email_recur"
                           class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($this->config->item('system')->email_recur == 1) ? 'checked' : ''; ?> />

                </div>
            </div>
            <div class="form-group">

                <label class="col-lg-3 control-label"><?php echo lang('label_auto_database_backup'); ?></label>

                <div class="col-lg-4">
                    <input type="checkbox" name="auto_db_backup"
                           class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($this->config->item('system')->auto_db_backup == 1) ? 'checked' : ''; ?> />

                </div>
            </div>


            <div class="row">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-success btn-block btn-lg"><i
                            class="fa fa-sign-in"></i> <?php echo lang('form_button_update'); ?></button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </fieldset>
    </div>
</div>