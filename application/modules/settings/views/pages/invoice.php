<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/07/09
 * Time: 12:12 PM
 */
?>
<div class="row">
    <div class="col-sm-12">
        <fieldset>
            <legend><i class="fa fa-bar-chart-o"></i><?php echo strtoupper(lang('label_transactions')); ?></legend>


            <div class="tab-content">
                <!--Start adding new estimate -->
                <div class="tab-pane fade in active" id="invoice">

                    <?php echo form_open_multipart('settings/invoice',
                        array('class' => 'panel form-horizontal form-bordered')); ?>
                    <div class="panel-body">
                        <input class="form-control" type="hidden" name="setting_id"
                               value="<?php echo $this->config->item('invoice')->setting_id; ?>"/>

                        <div class="form-group header bgcolor-default">
                            <div class="col-md-12">
                                <h4><?php echo lang('label_invoice_setting'); ?></h4>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo lang('label_invoice_prefix'); ?></label>

                            <div class="col-sm-8">
                                <input class="form-control" type="text" name="invoice_prefix"
                                       value="<?php echo $this->config->item('invoice')->invoice_prefix; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo lang('label_invoice_due_after'); ?></label>

                            <div class="col-sm-8">
                                <input class="form-control" type="text" name="invoice_due_after"
                                       value="<?php echo $this->config->item('invoice')->invoice_due_after; ?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo lang('label_invoice_footer'); ?></label>

                            <div class="col-sm-8">
                                <textarea class="form-control rounded" name="invoice_terms"
                                          style="height:100%"><?php echo $this->config->item('invoice')->invoice_terms; ?></textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo lang('label_invoice_logo'); ?></label>

                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control" readonly>
                                                            <span class="input-group-btn">
                                                                <span class="btn btn-primary btn-file">
                                                                    Browse files<input type="file" multiple
                                                                                       name="logo_file">
                                                                </span>
                                                            </span>
                                        </div>
                                        <!-- /.input-group -->
                                    </div>
                                </div>
                                <?php if ($this->config->item('invoice')->invoice_logo != '') : ?>
                                    <div class="row" style="height: 150px; margin-bottom:15px;">
                                        <div class="col-sm-12">
                                            <div class="invoice_logo" style="height: 151px">
                                                <img
                                                    src="<?php echo base_url('files/transaction_logos/' . $this->config->item('invoice')->invoice_logo); ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group header bgcolor-default mt-20">
                            <div class="col-md-12">
                                <h4><?php echo lang('label_estimates_setting'); ?></h4>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo lang('label_estimate_prefix'); ?></label>

                            <div class="col-sm-8">
                                <input class="form-control" type="text" name="estimate_prefix"
                                       value="<?php echo $this->config->item('invoice')->estimate_prefix; ?>"/>
                            </div>
                        </div>

                        <div class="form-group header bgcolor-default mt-20">
                            <div class="col-md-12">
                                <h4><?php echo lang('label_quotation_setting'); ?></h4>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo lang('label_quotation_prefix'); ?></label>

                            <div class="col-sm-8">
                                <input class="form-control" type="text" name="quotation_prefix"
                                       value="<?php echo $this->config->item('invoice')->quotation_prefix; ?>"/>
                            </div>
                        </div>

                        <div class="form-group header bgcolor-default mt-20">
                            <div class="col-md-12">
                                <h4><?php echo lang('label_project_setting'); ?></h4>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo lang('label_project_prefix'); ?></label>

                            <div class="col-sm-8">
                                <input class="form-control" type="text" name="project_prefix"
                                       value="<?php echo $this->config->item('invoice')->project_prefix; ?>"/>
                            </div>
                        </div>


                    </div>
                    <hr/>
                    <button type="input" name="submit" class="btn btn-success btn-lg btn-icon mt-10"><i
                            class="fa fa-check-square-o"></i> <?php echo lang('form_button_save'); ?></button>
                    </form>
                </div>

            </div>


        </fieldset>
    </div>
</div>