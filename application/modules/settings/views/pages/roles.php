<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/18
 * Time: 02:25 PM
 */
?>
<div class="row">
    <div class="col-xs-12">
        <fieldset>
            <legend><i class="fa fa-cog"></i>Role Settings
              <span class="pull-right">
                    <a data-toggle="zestModal" class="text-danger"
                       href="<?php echo base_url('settings/edit_roles'); ?>"><i class="fa fa-plus box-font"></i></a>
              </span>
            </legend>

            <div class="table-responsive">
                <table
                    class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
                    id="sample_2">
                    <thead>
                    <tr>
                        <th><?php echo lang('label_role_id'); ?> </th>
                        <th><?php echo lang('label_role_name'); ?> </th>
                        <th class="hidden-480"><?php echo lang('label_active'); ?></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    if (count($user_roles)): foreach ($user_roles as $user_role): ?>
                        <tr class="tr-link"
                            data-link="<?php echo site_url('settings/edit_roles/' . $user_role->role_id); ?>">

                            <td>
                                <?php echo $user_role->role_id; ?>
                            </td>
                            <td><?php echo $user_role->role_name; ?></td>

                            <td>

                                <?php if (!$user_role->role_active) { ?>

                                    <span class="label label-sm label-danger">
												 Blocked
							  </span>
                                <?php } else { ?>

                                    <span class="label label-sm label-info">
												Active
						</span>


                                <?php } ?>

                            </td>

                        </tr>
                    <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="6">We could not find any users.</td>
                        </tr>
                    <?php endif; ?>


                    </tbody>
                </table>
            </div>

            <fieldset
    </div>
</div>

