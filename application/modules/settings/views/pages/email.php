<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/21
 * Time: 03:42 PM
 */
?>

<div class="row">
    <div class="col-sm-12">
        <fieldset>
            <legend><i class="fa fa-envelope"></i><?php echo strtoupper(lang('label_emails')); ?></legend>

            <div class="tab-content">
                <!--Start adding new estimate -->
                <div class="tab-pane fade in active" id="emails">


                    <?php echo form_open('settings/email',
                        array('id' => 'edit-currency', 'class' => "panel form-horizontal form-bordered")); ?>
                    <div class="panel-body">
                        <input type="hidden" name="email_id"
                               value="<?php echo $this->config->item('company_email')->setting_id; ?>"/>

                        <div class="form-group header bgcolor-default">
                            <div class="col-md-12">
                                <h4><?php echo lang('label_emails_setting'); ?></h4>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-8">
                                <p class="radio">
                                    <label for="php">
                                        <input id="php_mail"
                                               type="radio"  <?php echo ($this->config->item('company_email')->mail_protocol == 'php_mail') ? "checked" : "" ?>
                                               value="php_mail" name="smtp_protocol">
                                        <?php echo lang('label_php_mail'); ?>
                                        <br>
                                        <i>Use PHP's mail() function (recommended; works in most cases)</i>
                                    </label>
                                </p>

                                <p class="radio">
                                    <label for="smtp">
                                        <input id="smtp"
                                               type="radio"  <?php echo ($this->config->item('company_email')->mail_protocol == 'smtp') ? "checked" : "" ?>
                                               value="smtp" name="smtp_protocol">
                                        <?php echo lang('label_smtp'); ?>
                                        <br>
                                        <i>Set my own SMTP parameters (for advanced users ONLY)</i>
                                    </label>
                                </p>

                                <p class="radio">
                                    <label for="never">
                                        <input id="never_send"
                                               type="radio"  <?php echo ($this->config->item('company_email')->mail_protocol == 'never_send') ? "checked" : "" ?>
                                               value="never_send" name="smtp_protocol">
                                        <?php echo lang('label_never'); ?>
                                        <br>
                                        <i>Never send emails (may be useful for testing purposes)</i>
                                    </label>
                                </p>
                            </div>
                        </div>


                        <div class="form-group header bgcolor-default mt-20">
                            <div class="col-md-12">
                                <h4><?php echo lang('label_email_type'); ?></h4>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>

                            <div class="col-sm-8">
                                <p class="radio">
                                    <label for="format_html">
                                        <input id="format_html"
                                               type="radio"  <?php echo ($this->config->item('company_email')->mail_type == 'html') ? "checked" : "" ?>
                                               value="html" name="email_format">
                                        <?php echo lang('label_format_html'); ?>
                                        <br>
                                        <i>Send email in HTML format</i>
                                    </label>
                                </p>

                                <p class="radio">
                                    <label for="format_text">
                                        <input id="format_text"
                                               type="radio"  <?php echo ($this->config->item('company_email')->mail_type == 'text') ? "checked" : "" ?>
                                               value="text" name="email_format">
                                        <?php echo lang('label_format_text'); ?>
                                        <br>
                                        <i> Send email in text format</i>
                                    </label>
                                </p>

                                <p class="radio">
                                    <label for="format_both">
                                        <input id="format_both"
                                               type="radio"  <?php echo ($this->config->item('company_email')->mail_type == 'both') ? "checked" : "" ?>
                                               value="both" name="email_format">
                                        <?php echo lang('label_format_both'); ?>
                                        <br>
                                        <i>Send email in HTML + text format</i>
                                    </label>
                                </p>
                            </div>
                        </div>
                        <div id="configuration_smtp" style="display: none;">
                            <div class="form-group header bgcolor-default mt-20">
                                <div class="col-md-12">
                                    <h4><?php echo lang('label_smtp_setting'); ?></h4>
                                </div>
                            </div>

                            <div class="form-group">
                                <label
                                    class="col-sm-3 control-label"><?php echo lang('label_smtp_domain_name'); ?></label>

                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="smtp_domain"
                                           value="<?php echo $this->config->item('company_email')->smtp_hostname; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo lang('label_smtp_server'); ?></label>

                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="smtp_server"
                                           value="<?php echo $this->config->item('company_email')->smtp_servername; ?>"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo lang('label_smtp_user'); ?></label>

                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="smtp_username"
                                           value="<?php echo $this->config->item('company_email')->smtp_username; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo lang('label_smtp_password'); ?></label>

                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="smtp_password"
                                           value="<?php echo $this->config->item('company_email')->smtp_password; ?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label
                                    class="col-sm-3 control-label"><?php echo lang('label_smtp_encryption'); ?></label>

                                <div class="col-sm-8">
                                    <select class="form-control" name="smtp_encryption">
                                        <option selected="selected" value="off">None</option>
                                        <option <?php echo $this->config->item('company_email')->smtp_encryption == 'tls' ? 'selected="selected"' : ''; ?>
                                            value="tls">TLS
                                        </option>
                                        <option <?php echo $this->config->item('company_email')->smtp_encryption == 'ssl' ? 'selected="selected"' : ''; ?>
                                            value="ssl">SSL
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo lang('label_smtp_port'); ?></label>

                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="smtp_port"
                                           value="<?php echo $this->config->item('company_email')->smtp_port; ?>"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group header bgcolor-default mt-20">
                            <div class="col-md-12">
                                <h4><?php echo lang('label_outgoing_email_setting'); ?></h4>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo lang('label_outgoing_email'); ?><span
                                    class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                <input class="form-control" type="text" name="mail_outgoing"
                                       value="<?php echo $this->config->item('company_email')->mail_outgoing; ?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo lang('label_use_test_mail'); ?></label>

                            <div class="col-sm-8">
                                <input type="checkbox" name="is_test"
                                       class="ios-switch ios-switch-success ios-switch-lg" <?php if ($this->config->item('company_email')->is_test == 1) {
                                    echo "checked=\"checked\"";
                                } ?> id="use_test_mail"/>

                            </div>
                        </div>

                        <div
                            id="test_mail_config" <?php echo ($this->config->item('company_email')->is_test == 0) ? 'style="display:none"' : '' ?>>
                            <div class="form-group">
                                <label
                                    class="col-sm-3 control-label"><?php echo lang('label_test_email_name'); ?></label>

                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="test_email"
                                           value="<?php echo $this->config->item('company_email')->test_email; ?>"/>
                                </div>
                            </div>
                        </div>


                    </div>
                    <hr/>
                    <button type="input" name="submit" value="editTask" class="btn btn-success btn-lg btn-icon mt-10"><i
                            class="fa fa-check-square-o"></i> <?php echo lang('form_button_save'); ?></button>
                    </form>
                </div>

            </div>


        </fieldset>
    </div>
</div>
