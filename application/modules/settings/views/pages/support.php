<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/03/14
 * Time: 02:00 PM
 */
?>
<div class="row">
    <div class="col-lg-12">
        <form id="form-currency" class="form-horizontal clearfix" action="" method="post">
            <div class="panel col-lg-12">
                <fieldset>
                    <legend><i class="fa fa-share-alt"></i><?php echo lang('label_support_setting'); ?>
                        <a class="btn btn-mini btn-danger pull-right" data-toggle="zestModal"
                           href="<?php echo base_url('settings/support/?option=add'); ?>">
                            <i class="fa fa-plus"></i>
                            <?php echo lang('form_button_add_department'); ?>
                        </a>
                    </legend>
                    <div class="row">
                        <div class="col-lg-3">
                            <ul class="ver-inline-menu tabbable">
                                <li class="active"><a href="" data-target="#temp_overview" data-toggle="tab"><i
                                            class="fa fa-sign-in"></i>Departments</a></li>

                            </ul>

                        </div>
                        <div class="col-md-9">

                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-forms">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th><?php echo lang('lang_name'); ?></th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($departments as $department): ?>
                                        <tr>
                                            <td><?php echo $department->department_id; ?></td>
                                            <td><?php echo $department->department_desc; ?></td>
                                            <td><?php echo $department->department_id; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>


                        </div>


                    </div>
                </fieldset>
            </div>
        </form>
    </div>
</div>