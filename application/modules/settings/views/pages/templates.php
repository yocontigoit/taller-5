<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/25
 * Time: 04:37 PM
 */
?>
<div class="row">
    <div class="col-md-12">
        <fieldset>

            <legend><i class="fa fa-bar-chart-o"></i><?php echo lang('label_templates'); ?>
            </legend>
            <div class="col-md-4">
                <ul class="ver-inline-menu tabbable">
                    <li class="<?php echo ($active_page == 'account') ? 'active' : '' ?> "><a
                            href="<?php echo base_url('settings/templates/?page=account'); ?>"><i
                                class="fa fa-sign-in"></i>Account Emails</a></li>
                    <li class="<?php echo ($active_page == 'invoice') ? 'active' : '' ?> "><a
                            href="<?php echo base_url('settings/templates/?page=invoice'); ?>"><i
                                class="fa fa-sign-in"></i>Invoices Email</a></li>
                    <li class="<?php echo ($active_page == 'issues') ? 'active' : '' ?> "><a
                            href="<?php echo base_url('settings/templates/?page=issues'); ?>"><i
                                class="fa fa-sign-in"></i>Issues Email</a></li>
                    <li class="<?php echo ($active_page == 'quotation') ? 'active' : '' ?> "><a
                            href="<?php echo base_url('settings/templates/?page=quotation'); ?>"><i
                                class="fa fa-sign-in"></i> Quotations Email</a></li>
                    <li class="<?php echo ($active_page == 'estimates') ? 'active' : '' ?> "><a
                            href="<?php echo base_url('settings/templates/?page=estimates'); ?>"><i
                                class="fa fa-sign-in"></i> Estimate Email</a></li>
                    <li class="<?php echo ($active_page == 'project') ? 'active' : '' ?> "><a
                            href="<?php echo base_url('settings/templates/?page=project'); ?>"><i
                                class="fa fa-sign-in"></i> Project Email</a></li>
                    <li class="<?php echo ($active_page == 'ticket') ? 'active' : '' ?> "><a
                            href="<?php echo base_url('settings/templates/?page=ticket'); ?>"><i
                                class="fa fa-sign-in"></i> Ticket Email</a></li>
                </ul>
            </div>
            <div class="col-md-8 contentarea">
                <fieldset>

                    <legend><i class="fa fa-bar-chart-o"></i><?php echo $page_title; ?>

                        <?php echo $_temp; ?>
                    </legend>


                    <?php
                    echo form_open('settings/update_templates', array('class' => '')); ?>
                    <input type="hidden" name="email" value="<?php echo $email; ?>"/>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control " name="email_subject" placeholder="subject"
                                       value="<?php echo $email_details->template_title; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12">
                                    <textarea class="form-control temp_editor" name="email_body">
                                        <?php echo $email_details->template_body; ?>
                                    </textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-success btn-block btn-lg"><i
                                        class="fa fa-sign-in"></i> <?php echo lang('form_button_update'); ?></button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>


                </fieldset>
            </div>
            <!-- /.col-sm-10 -->

        </fieldset>
    </div>
</div><!-- /.row -->