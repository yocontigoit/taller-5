<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/09/14
 * Time: 12:46 PM
 */
?>
<div class="row">
    <div class="col-lg-12">
        <fieldset>
            <legend><i class="fa fa-cloud-download"></i><?php echo lang('label_backup'); ?></legend>
            <div class="alert alert-warning">
                <button class="close" type="button" data-dismiss="alert">×</button>
                <h4>Disclaimer before creating a new backup</h4>
                <ol>
                    <li>Zestpro is not responsible for your database, its backups and/or recovery.</li>
                    <li>You should back up your data on a regular basis (both files and database).</li>
                    <li>This function only backs up your database, not your files.</li>

                </ol>
                <form class="form-horizontal" method="post" action="">
                    <a href="<?php echo base_url('settings/mysql_backup'); ?>" class="btn btn-info"> <i
                            class="fa fa-save"></i>
                        I have read the disclaimer. Please create a new backup.
                    </a>
                </form>
            </div>

            <form id="form-backup" class="form-horizontal clearfix" action="" method="post">
                <div class="panel col-lg-12">
                    <div class="table-responsive clearfix">
                        <table class="table backup">
                            <thead>
                            <tr class="nodrag nodrop">
                                <th class="">
                                    <span class="title_box active"><?php echo lang('label_date'); ?></span>
                                </th>
                                <th class="">
                                    <span class="title_box"><?php echo lang('label_file_name'); ?> </span>
                                </th>
                                <th class="fixed-width-sm">
                                    <span class="title_box"><?php echo lang('label_file_format'); ?></span>
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php if (count($database_backups)): foreach ($database_backups as $backup): ?>
                                <tr>
                                    <td class="list-empty">
                                        <?php echo $backup->upload_date; ?>
                                    </td>
                                    <td class="list-empty">
                                        <?php echo $backup->filename; ?>
                                    </td>
                                    <td class="list-empty">
                                        <?php echo $backup->format; ?>
                                    </td>
                                    <td class="list-empty">

                                        <div class="btn-group">

                                            <?php echo anchor('settings/mysql_restore/' . $backup->filename,
                                                '<i class="fa fa-cloud-upload"></i>', array(
                                                    'class' => 'btn btn-xs btn-info',
                                                    'title' => 'Restore',
                                                    'data-toggle' => 'tooltip'
                                                )); ?>
                                            <?php echo anchor('settings/download_file/' . $backup->filename,
                                                '<i class="fa fa-download"></i>', array(
                                                    'class' => 'btn btn-xs btn-primary',
                                                    'title' => 'Download',
                                                    'data-toggle' => 'tooltip'
                                                )); ?>
                                            <?php echo anchor('settings/delete_backup/' . $backup->filename . '/' . $backup->setting_id,
                                                '<i class="fa fa-trash"></i>', array(
                                                    'class' => 'btn btn-xs btn-danger',
                                                    'title' => 'Delete',
                                                    'data-toggle' => 'tooltip'
                                                )); ?>
                                        </div>

                                    </td>
                                </tr>
                            <?php endforeach;endif; ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </form>

        </fieldset>
    </div>
</div>
