<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/09/14
 * Time: 12:46 PM
 */
?>
<div class="row">
    <div class="col-lg-12">
        <fieldset>
            <legend><i class="fa fa-cloud-download"></i><?php echo lang('label_system_updates'); ?></legend>
            <?php if (!valid_license()) { ?>
                <div class="alert alert-warning alert-block fade in alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong><?php echo lang('label_purchase_code'); ?>
                        !</strong><?php echo lang('messages_validate_purchase_code_warning'); ?>
                </div>
            <?php } ?>
            <div class="alert">
                <button class="close" type="button" data-dismiss="alert">×</button>
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="list-group no-radius">
                            <li class="list-group-item"><span
                                    class="pull-right"><?php echo $this->config->item('system')->version; ?></span><?php echo lang('label_version'); ?>
                            </li>
                            <li class="list-group-item"><span
                                    class="pull-right"><?= phpversion() ?></span><?php echo lang('label_php'); ?></li>
                            <li class="list-group-item"><span
                                    class="pull-right"><?= CI_VERSION ?></span><?php echo lang('label_codeigniter_version'); ?>
                            </li>
                        </ul>
                    </div>

                </div>
                <form class="form-horizontal" method="post" action="">
                    <a href="<?php echo base_url('settings/app_backup'); ?>" class="btn btn-info"> <i
                            class="fa fa-save"></i>
                        Please create a new app backup.
                    </a>

                </form>
            </div>
            <div class="panel col-lg-12">
                <div class="table-responsive clearfix">
                    <table class="table backup">
                        <thead>
                        <tr>
                            <th class="">
                                <span class="title_box active"><?php echo lang('label_Build'); ?> </span>
                            </th>
                            <th class="">
                                <span class="title_box"><?php echo lang('label_date'); ?> </span>
                            </th>
                            <th class="fixed-width-sm">
                                <span class="title_box"><?php echo lang('label_version'); ?></span>
                            </th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php if (count($updates)): foreach ($updates as $update): ?>

                            <tr>
                                <td class="list-empty">
                                    <?php echo $update->build; ?>
                                </td>
                                <td class="list-empty">
                                    <?php echo $update->tran_date; ?>
                                </td>

                                <td class="list-empty">
                                    <?php echo $update->version; ?>
                                </td>
                                <td class="list-empty">

                                    <div class="btn-group">
                                        <?php if ($update->installed == 0 && $update->is_downloaded == 1) { ?>
                                            <?php echo anchor('settings/install_update/' . $update->build . '/' . $update->version,
                                                '<i class="fa fa-upload"></i>', array(
                                                    'class' => 'btn btn-xs btn-primary',
                                                    'title' => 'Install',
                                                    'data-toggle' => 'tooltip'
                                                )); ?>
                                        <?php } ?>

                                        <?php if ($update->is_downloaded == 0) { ?>
                                            <?php echo anchor('settings/download_update/' . $update->build,
                                                '<i class="fa fa-download"></i>', array(
                                                    'class' => 'btn btn-xs btn-primary',
                                                    'title' => 'Download',
                                                    'data-toggle' => 'tooltip'
                                                )); ?>
                                            <?php echo anchor('settings/delete_backup/' . $update->build,
                                                '<i class="fa fa-trash"></i>', array(
                                                    'class' => 'btn btn-xs btn-danger',
                                                    'title' => 'Delete',
                                                    'data-toggle' => 'tooltip'
                                                )); ?>
                                        <?php } ?>

                                    </div>

                                </td>
                            </tr>
                        <?php endforeach;endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>


        </fieldset>
    </div>
</div>
