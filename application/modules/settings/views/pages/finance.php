<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/25
 * Time: 04:37 PM
 */
?>
<div class="row">
    <div class="col-md-12">
        <fieldset>
            <legend><i class="fa fa-money"></i><?php echo lang('label_finance_setting'); ?>
            </legend>
            <div class="col-md-4">
                <ul class="ver-inline-menu tabbable">
                    <li class="<?php echo ($active_page == 'currency') ? 'active' : '' ?> "><a
                            href="<?php echo base_url('settings/finance/?page=currency'); ?>"><i
                                class="fa fa-money"></i><?php echo lang('label_currency'); ?></a></li>
                    <li class="<?php echo ($active_page == 'tax') ? 'active' : '' ?> "><a
                            href="<?php echo base_url('settings/finance/?page=tax'); ?>"><i
                                class="fa fa-sign-in"></i><?php echo lang('label_tax'); ?></a></li>
                    <li class="<?php echo ($active_page == 'expense') ? 'active' : '' ?> "><a
                            href="<?php echo base_url('settings/finance/?page=expense'); ?>"><i
                                class="fa fa-sticky-note-o"></i><?php echo lang('label_categories'); ?></a></li>
                </ul>
            </div>
            <div class="col-md-8 contentarea">
                <?php echo $_FIN_PAGE; ?>
            </div>
            <!-- /.col-sm-10 -->
        </fieldset>
    </div>
</div><!-- /.row -->