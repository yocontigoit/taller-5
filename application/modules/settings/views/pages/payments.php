<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/18
 * Time: 09:30 AM
 */
?>
<div class="row">
    <div class="col-lg-12">

        <div class="leadin"></div>
        <div class="panel">
            <h3><i class="icon-list-ul"></i><?php echo lang('label_active_payments_gateways'); ?></h3>

            <div class="modules_list_container_tab row">
                <div class="col-lg-12">
                    <table class="table">

                        <?php foreach ($this->config->item('payment') as $payment): ?>
                            <tr>
                                <td class="fixed-width-sm center">
                                    <img class="img-thumbnail" style="width: 210px;height: 60px;"
                                         alt="<?php echo $payment->payment_name; ?>"
                                         src="<?php echo base_url('files/gateways/' . $payment->payment_name . '.png'); ?>"/>
                                </td>
                                <td>
                                    <div id="anchorBankwire" title="Bank wire">
                                        <div class="module_name">
                                            <?php echo $payment->payment_name; ?>


                                        </div>
                                        <p class="module_description">
                                            <?php echo $payment->payment_description; ?>
                                        </p>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td class="actions">
                                    <div class="btn-group-action">
                                        <div class="btn-group">
                                            <a class=" btn btn-info"
                                               href="<?php echo base_url('settings/payment/' . $payment->payment_name); ?>"
                                               title=""><i class="fa fa-wrench"></i> Configure</a>
                                            <button type="button" class="btn btn-default dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <span class="caret">&nbsp;</span>
                                            </button>
                                            <ul class="dropdown-menu pull-right">

                                                <li><a class="" href="" title=""><i
                                                            class="fa fa-off"></i><?php echo lang('label_disable'); ?>
                                                    </a></li>

                                                <li class="divider"></li>
                                                <li><a class="text-danger" href="" title=""><i
                                                            class="icon-trash"></i> <?php echo lang('label_delete'); ?>
                                                    </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        <?php endforeach; ?>


                    </table>
                </div>
            </div>
        </div>


