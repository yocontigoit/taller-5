<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/09/14
 * Time: 11:56 AM
 */
?>
<fieldset>
    <legend><i class="fa fa-cog"></i><?php echo lang('label_category_setting'); ?>
        <span class="pull-right">
                    <a data-toggle="zestModal" class="text-danger"
                       href="<?php echo base_url('settings/edit_category'); ?>"><i class="fa fa-plus box-font"></i></a>
                </span>

    </legend>
    <form id="form-currency" class="form-horizontal clearfix" action="" method="post">

        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
                   id="sample_2">
                <thead>
                <tr class="">
                    <th class=" center">
                        <?php echo lang('label_id'); ?>
                    </th>
                    <th class="">
                        <?php echo lang('label_category'); ?>
                    </th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php if (count($categories)): foreach ($categories as $category): ?>
                    <tr>

                        <td class="pointer fixed-width-xs center"> <?php echo $category->class_id; ?></td>
                        <td class="pointer"><?php echo $category->class_name; ?></td>

                        <td class="text-right">
                            <div class="btn-group-action">
                                <div class="btn-group t">
                                    <a class="edit btn btn-info" title="Edit" data-toggle="zestModal"
                                       href="<?php echo base_url('settings/edit_category/' . $category->class_id); ?>">
                                        <i class="fa fa-pencil"></i>
                                        <?php echo lang('label_edit'); ?>
                                    </a>
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-caret-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a class="delete" title="Delete"
                                               href="<?php echo base_url('settings/delete_category/' . $category->class_id); ?>">
                                                <i class="icon-trash"></i>
                                                <?php echo lang('label_delete'); ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; endif; ?>
                </tbody>
            </table>
        </div>
    </form>

</fieldset>

