<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/09/14
 * Time: 11:56 AM
 */
?>
<fieldset>
    <legend><i class="fa fa-cog"></i>Currency Settings
                <span class="pull-right">
                    <a data-toggle="zestModal" class="text-danger"
                       href="<?php echo base_url('settings/edit_currency'); ?>"><i class="fa fa-plus box-font"></i></a>
                </span>

    </legend>
    <div id="currency-empty-filters-alert" class="alert alert-warning">Please fill at least one field to perform a
        search in this list.
    </div>
    <form id="form-currency" class="form-horizontal clearfix" action="" method="post">

        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
                   id="sample_2">
                <thead>
                <tr class="">
                    <th class=" center">
                        <?php echo lang('label_id'); ?>
                    </th>
                    <th class="">
                        <?php echo lang('label_currency'); ?>
                    </th>
                    <th class="fixed-width-xs center">
                        <?php echo lang('label_iso_code'); ?>

                    </th>
                    <th class="fixed-width-xs center">
                        <?php echo lang('label_iso_number'); ?>

                    </th>
                    <th class="fixed-width-xs center">
                        <span class="title_box"><?php echo lang('label_symbol'); ?> </span>
                    </th>
                    <th class=" center">

                        <?php echo lang('label_exchange_rate'); ?>

                    </th>
                    <th class="fixed-width-sm center">
                        <?php echo lang('label_enabled'); ?>
                    </th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php if (count($currencies)): foreach ($currencies as $currency): ?>
                    <tr>

                        <td class="pointer fixed-width-xs center"> <?php echo $currency->currency_id; ?></td>
                        <td class="pointer"><?php echo $currency->name; ?></td>
                        <td class="pointer fixed-width-xs center"><?php echo $currency->iso_code; ?> </td>
                        <td class="pointer fixed-width-xs center"><?php echo $currency->iso_code_num; ?></td>
                        <td class="pointer fixed-width-xs center"><?php echo $currency->curreny_symbol; ?></td>
                        <td class="pointer center"><?php echo $currency->conversion_rate; ?></td>
                        <td class="pointer fixed-width-sm center">
                            <a class="list-action-enable action-enabled" title="Enabled" href="">
                                <i class="fa fa-check"></i>
                                <i class="fa fa-remove hidden"></i>
                            </a>
                        </td>
                        <td class="text-right">
                            <div class="btn-group-action">
                                <div class="btn-group t">
                                    <a class="edit btn btn-info" title="Edit" data-toggle="zestModal"
                                       href="<?php echo base_url('settings/edit_currency/' . $currency->currency_id); ?>">
                                        <i class="fa fa-pencil"></i>
                                        <?php echo lang('label_edit'); ?>
                                    </a>
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-caret-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a class="delete" title="Delete" href="">
                                                <i class="icon-trash"></i>
                                                <?php echo lang('label_delete'); ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; endif; ?>
                </tbody>
            </table>
        </div>
    </form>

</fieldset>

