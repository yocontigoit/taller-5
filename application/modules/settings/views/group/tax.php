<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/12/18
 * Time: 10:55 AM
 */

?>
<fieldset>
    <legend><i class="fa fa-cog"></i><?php echo lang('label_tax_setting'); ?>
        <span class="pull-right">
                    <a data-toggle="zestModal" class="text-danger"
                       href="<?php echo base_url('settings/edit_tax'); ?>"><i class="fa fa-plus box-font"></i></a>
                </span>

    </legend>
    <?php echo form_open('settings/email',
        array('id' => 'generalForm', 'role' => 'form', 'class' => 'panel form-horizontal')); ?>

    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
               id="sample_2">
            <thead>
            <tr>
                <th class="center">
                    <span class="title_box active"> <?php echo lang('label_id'); ?></span>
                </th>
                <th class="">
                <th class="center">
                    <span class="title_box"><?php echo lang('label_rate'); ?></span>
                </th>
                <th class=" center">
                    <span class="title_box"><?php echo lang('label_enabled'); ?> </span>
                </th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php if (count($taxes)): foreach ($taxes as $tax): ?>
                <tr>
                    <td class="pointer center"><?php echo $tax->tax_id; ?> </td>
                    <td class="pointer "><?php echo $tax->name; ?>  </td>
                    <td class="pointer  center"><?php echo $tax->rate; ?> %</td>
                    <td class="pointer  center">

                        <?php if ($tax->active == 1) { ?>
                            <a class="list-action-enable action-enabled" title="Enabled" href="">
                                <i class="fa fa-check"></i>
                            </a>
                        <?php } else { ?>
                            <a class="list-action-enable action-disabled" title="Disabled" href="">
                                <i class="fa fa-remove"></i>
                            </a>
                        <?php } ?>
                    </td>
                    <td class="text-right">
                        <div class="btn-group-action">
                            <div class="btn-group">
                                <a class="edit btn btn-info" title="Edit" data-toggle="zestModal"
                                   href="<?php echo base_url('settings/edit_tax/' . $tax->tax_id); ?>">
                                    <i class="fa fa-pencil"></i>
                                    <?php echo lang('label_edit'); ?>
                                </a>
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a class="delete" title="Delete" href="">
                                            <i class="fa fa-trash"></i>
                                            <?php echo lang('label_delete'); ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; endif; ?>
            </tbody>
        </table>
    </div>
    <?php echo form_close(); ?>
</fieldset>
