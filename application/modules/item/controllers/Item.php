<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/01
 * Time: 03:30 PM
 */
class Item extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();
        $this->load->model('item_m');
    }

    /**
     * display all products
     */
    function index()
    {
        //$this->check_action_permission('search');
        $this->data['content'] = 'item/products';
        $this->data['title'] = 'Productos';
        $this->data['items'] = $this->item_m->get_all_items($this->Company_id);
        $this->data['clients'] = $this->item_m->get_all_clients($this->Company_id);
        $this->data['_ITEM_PAGE'] = $this->load->view('pages/all', $this->data, true);
        
        $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }


    /**
     * add new item, save to database
     */
    function add_item()
    {
        $this->data['clients'] = $this->item_m->proveedores();
        $this->data['clients2'] = $this->item_m->contratistas();
        $this->data['content'] = 'item/products';

        if ($this->input->post()) {

            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            
            $param = array(
                "item_name" => $this->input->post('item_name'),
                "item_desc" => $this->input->post('item_description'),
                "item_unit" => $this->input->post('item_unit'),
                "item_category" => $this->input->post('item_category'),
                "unit_cost" => $this->input->post('item_price'),
                "item_tax_rate" => $this->input->post('item_tax_class'),
                "quantity" => $this->input->post('item_quantity'),
                "item_client" => $this->input->post('item_client'),
                "deleted" => 0,
                "taxable" => 1,
                "company_id" => $this->Company_id
            );

            $item_id = $this->item_m->add_item($param);
            if ($item_id) {


                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_item_add_success'));


                $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'items',
                    'activity_details' => 'Adding items',
                    'activity_module_id' => $item_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Agrega un nuevo producto ",
                    'activity_icon' => 'fa fa-times'
                );

                $this->__activity_tracker($log_params);
                //redirect(current_url());
                redirect($_SERVER['HTTP_REFERER']);
            }


        } else {

            $this->data['title'] = 'Agregar producto nuevo';
            $this->data['_ITEM_PAGE'] = $this->load->view('pages/add', $this->data, true);
        }

        $this->load->view('_main_layout', $this->data);
    }

    /**
     * log activities to the database
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->item_m->add_log($activity_data);

    }

    /**
     * add new item
     */
    function add_modal_item()
    {


        $this->data['title'] = 'Editar Estimación';
        $this->data['content'] = 'estimate/estimates';
        $this->load->view('modal/item', $this->data);
    }

    /**
     * delete product
     */
    function delete()
    {

        $this->data['content'] = 'item/products';
        if ($this->input->post()) {
            $item_id = intval($this->input->post('item_id'));
            if ($this->item_m->remove_item($item_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_item_delete_success'));

            } else {

                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'Something went wrong deleting..');
            }
            redirect('item');
        } else {

            $this->data['title'] = 'Eliminar producto';
            $this->data['item_id'] = intval($this->uri->segment(3));
            $this->load->view('modal/delete', $this->data);
        }

    }

    /**
     * edit item
     * @param null $item_id
     */
    function edit_item($item_id = null)
    {
        $item_id= intval($item_id);
        if ($this->input->post()) {
            $param = array(
                "item_name" => $this->input->post('item_name'),
                "item_desc" => $this->input->post('item_description'),
                "item_unit" => $this->input->post('item_unit'),
                "unit_cost" => $this->input->post('item_price')
            );
            
                if($this->item_m->edit_item($param, $item_id)){
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_item_save_success'));
                redirect('item');
            }else{
                redirect('item/edit_item/'.$item_id);
            }

        } else {
            $this->data['content'] = 'item/products';
            $this->data['title'] = 'Editar Producto';
            $this->data['item_details'] = $this->item_m->get_selected_item($item_id);
            $this->data['_ITEM_PAGE'] = $this->load->view('pages/edit', $this->data, true);
            $this->load->view('_main_layout', $this->data);
        }

    }

    /**
     * retrieve all items data
     */
    function get_items()
    {

        $items = array();
        $items = $this->item_m->get_all_items($this->Company_id);
        echo json_encode($items);
        exit();
    }

    /**
     * retrieve an item
     */
    function get_selected_item()
    {

        if ($this->input->post('item_id')) {
            $item_id = intval($this->input->post('item_id'));
            $items = array();
            $items = $this->item_m->get_selected_item($item_id);
            echo json_encode($items);
            exit();
        }
    }
}
