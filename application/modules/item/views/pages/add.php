<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/01/26
 * Time: 08:33 AM
 */
?>
<fieldset>
    <legend><i class="fa fa-bar-chart-o"></i><?php echo strtoupper(lang('label_add_item')); ?></legend>


    <ul class="nav nav-tabs mt-10" role="tablist">
        <li class="active"><a href="#item" role="tab" data-toggle="tab"><i
                    class="fa fa-tasks"></i><strong></strong> <?php echo lang('label_add_item'); ?></stong></a></li>

    </ul>

    <div class="tab-content">
        <!--Start adding new estimate -->
        <div class="tab-pane fade in active" id="item">

            <form action="" method="post" class="panel form-horizontal form-bordered" name="form-account">
                <div class="panel-body">
                    <div class="form-group header bgcolor-default">
                        <div class="col-md-12">
                            <h4><?php echo lang('label_item_setting'); ?></h4>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Proveedor</label>
                        <div class="col-sm-8">
                          <select data-placeholder="Select client..." class="form-control chosen-select"
                                  tabindex="4" name="item_client" id="client_id">
                              <option value="Empty">&nbsp;</option>

                              <optgroup label="Proveedores">
                                  <?php if (count($clients)):foreach ($clients as $client): ?>
                                      <option
                                          value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name) ?></option>
                                  <?php endforeach; endif; ?>
                              </optgroup>
                              <optgroup label="Contratistas">
                                  <?php if (count($clients2)):foreach ($clients2 as $client): ?>
                                      <option
                                          value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name) ?></option>
                                  <?php endforeach; endif; ?>
                              </optgroup>
                          </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo lang('label_item_name'); ?></label>

                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="item_name"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo lang('label_item_description'); ?></label>

                        <div class="col-sm-8">
                            <textarea class="form-control rounded" name="item_description"
                                      style="height:100%"></textarea>
                       
                            <input class="form-control" type="hidden" value="1000000" name="item_quantity"/>
                        </div>
                    </div>
  <div class="form-group">
                        <label class="col-sm-3 control-label">Categoria</label>

                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="item_category"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Unidad de medida</label>

                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="item_unit"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo lang('label_item_price'); ?></label>

                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="item_price"/>
                        </div>
                    </div>


                </div>
                <hr/>
                <button type="input" name="submit" value="editTask" class="btn btn-success btn-lg btn-icon mt-10"><i
                        class="fa fa-check-square-o"></i> <?php echo lang('form_button_add_item'); ?></button>
            </form>
        </div>

    </div>


</fieldset>
