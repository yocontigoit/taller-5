<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/07/07
 * Time: 03:37 PM
 */
?>
<div class="table-toolbar">
    <div class="btn-group">

        <?php echo anchor('item/add_item', lang('common_item_add') . ' <i class="fa fa-plus"></i>',
            array('class' => 'btn btn-info')); ?>

    </div>
</div>
<table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
       id="zest_table">
    <thead class="the-box dark full">
    <tr>

        <th>Concepto</th>
        <th class="hidden-xs hidden-sm hidden-md">Descripción</th>
        <th>Avance</th>
        <th>Proveedor/Contratista</th>
        <th>Unidad</th>

        <th><?php echo lang('label_item_price'); ?></th>
        <th><?php echo lang('label_options'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php if (count($items)) : foreach ($items as $item) { ?>
        <tr>
            <td style="vertical-align:middle;font-size:12px"><?php echo $item->item_name; ?></td>
            <td class="hidden-xs hidden-sm hidden-md"
                style="vertical-align:middle;font-size:12px"><?php echo $item->item_desc; ?></td>
                <td class="hidden-xs hidden-sm hidden-md"
                style="vertical-align:middle;font-size:12px"><?php echo $item->item_category; ?></td>
            <td style="vertical-align:middle;font-size:12px"><strong><?php if( count($clients) ):
                foreach($clients as $cli):
                    if($cli->client_id == $item->item_client):
            ?><a href="client/view/<?php echo $cli->client_id;?>"><?php echo $cli->client_name; ?></a>
            <?php

            endif;endforeach;endif;
            ?></strong></td>
            <td style="vertical-align:middle;font-size:12px"><?php echo $item->item_unit; ?></td>

            <td style="vertical-align:middle;font-size:12px"><strong>$<?php echo format_currency($item->unit_cost); ?></strong></td>
            <td>

                <div class="btn-group-action">
                    <div class="btn-group">
                        <a style="color:#1080d0;font-size:20px;" href="<?php echo base_url("item/edit_item/" . $item->item_id); ?>"
                           title="">
                            <i class="fa fa-edit"></i>  </a>
                        <a style="color:red;font-size:20px;" data-toggle="zestModal"
                                   href="<?php echo base_url('item/delete/' . $item->item_id); ?>" title="">  <i
                                        class="fa fa-trash"></i></a>

                    </div>
                </div>
            </td>

        </tr>
    <?php } endif; ?>
    </tbody>
</table>
