<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/07/01
 * Time: 09:53 AM
 */
?>
<fieldset>
    <legend><i class="fa fa-bar-chart-o"></i><?php echo strtoupper(lang('label_edit_item')); ?></legend>


    <ul class="nav nav-tabs mt-10" role="tablist">
        <li class="active"><a href="#item" role="tab" data-toggle="tab"><i
                    class="fa fa-tasks"></i><strong></strong> <?php echo lang('label_edit_item'); ?></stong></a></li>

    </ul>

    <div class="tab-content">
        <!--Start adding new estimate -->
        <div class="tab-pane fade in active" id="item">

            <form action="" method="post" class="panel form-horizontal form-bordered" name="form-account">
                <div class="panel-body">
                    <div class="form-group header bgcolor-default">
                        <div class="col-md-12">
                            <h4><?php echo lang('label_item_setting'); ?></h4>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo lang('label_item_name'); ?></label>

                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="item_name"
                                   value="<?php echo $item_details->item_name; ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo lang('label_item_description'); ?></label>

                        <div class="col-sm-8">
                            <textarea class="form-control rounded" name="item_description"
                                      style="height:100%"><?php echo $item_details->item_desc; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Unidad de medida</label>

                        <div class="col-sm-8">
                          <input class="form-control" type="text" name="item_unit"
                                 value="<?php echo $item_details->item_unit; ?>"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo lang('label_item_price'); ?></label>

                        <div class="col-sm-8">
                            <input class="form-control" type="text" name="item_price"
                                   value="<?php echo $item_details->unit_cost; ?>"/>
                        </div>
                    </div>


                </div>
                <hr/>
                <button type="input" name="submit" value="editTask" class="btn btn-success btn-lg btn-icon mt-10"><i
                        class="fa fa-check-square-o"></i> <?php echo lang('form_button_save_item'); ?></button>
            </form>
        </div>

    </div>


</fieldset>
