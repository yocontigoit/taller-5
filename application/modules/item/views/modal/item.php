<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/03/31
 * Time: 12:56 PM
 */
?>

<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                        class="fa fa-times"></i></span><span
                    class="sr-only"><?php echo lang('form_button_cancel'); ?></span></button>
            <h4 class="modal-title"><?php echo lang('label_item_add'); ?></h4>
        </div>

        <?php echo form_open('item/add_item', array('id' => 'add-item')); ?>
        <div class="modal-body">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name"><?php echo lang('label_item_name'); ?></label>
                        <input type="text" class="form-control " name="item_name" placeholder="name">
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name"><?php echo lang('label_item_description'); ?></label>
                        <textarea class="form-control rounded" name="item_description" style="height:100%"></textarea>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name"><?php echo lang('label_item_quantity'); ?></label>
                        <input type="text" class="form-control " name="item_quantity" placeholder="0">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name"><?php echo lang('label_item_price'); ?></label>
                        <input type="text" class="form-control " name="item_price">
                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label><?php echo 'Taxable'; ?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="checkbox" name="item_taxable"
                                       class="ios-switch ios-switch-success ios-switch-lg"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="modal-footer">
            <button type="input" name="submit" value="addNewTask" class="btn btn-success btn-icon"><i
                    class="fa fa-check-square-o"></i> <?php echo "Save"; ?></button>
            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i
                    class="fa fa-times-circle-o"></i> <?php echo "cancel"; ?></button>
        </div>
        </form>

    </div>
</div>

