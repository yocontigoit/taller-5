<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/01
 * Time: 03:34 PM
 */
class Item_M extends CI_Model
{


    /**
     * Get all users for a company
     * @param null $id
     * @return mixed
     */
    function get_all_items($id = null)
    {
        $company_id = intval($id);
        $this->db->from('items');

        //$this->db->where("company_id", $company_id);
        return $this->db->get()->result();
    }

    /**
     * Get clients by company
     * @param null $company_id
     * @return mixed
     */
    function proveedores($client_type = 2)
    {

        $this->db->from('client');
        $this->db->where("client_type", $client_type);

        return $this->db->get()->result();
    }

    /**
     * Get clients by company
     * @param null $company_id
     * @return mixed
     */
    function contratistas($client_type = 3)
    {

        $this->db->from('client');
        $this->db->where("client_type", $client_type);

        return $this->db->get()->result();
    }
    
    function get_all_clients($id = null)
    {
        $company_id = intval($id);
        $this->db->from('client');

        return $this->db->get()->result();
    }

    /**
     * add item
     * @param array $data
     * @return null
     */
    function add_item($data = array())
    {

        $success = false;

        $success = $this->db->insert('items', $data);
        $item_id = $this->db->insert_id();

        return ($success) ? $item_id : null;
    }

    /**
     * retrieve slected item data
     * @param $item_id
     * @return mixed
     */
     
     
     function edit_item($data = array(), $item_id)
    {

        $success = false;

        $this->db->where('item_id', $item_id);
        $success = $this->db->update('items', $data);

        return $success;
    }
    
    function get_selected_item($item_id)
    {

        $this->db->from('items');
        $this->db->where("item_id", intval($item_id));

        return $this->db->get()->row();
    }

    /**
     * delete item
     * @param null $item_id
     * @return null
     */
    function remove_item($item_id = null)
    {

        $success = false;
        $success = $this->db->where('item_id', $item_id)->delete('items');

        return ($success) ? $item_id : null;
    }

    /**
     * add product activity
     * @param array $data
     * @return bool
     */
    function add_log($data = array())
    {

        $success = false;
        $success = $this->db->insert("activity_log", $data);

        return $success;
    }

}
