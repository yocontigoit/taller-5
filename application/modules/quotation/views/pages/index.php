<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/30
 * Time: 02:30 PM
 */
?>
<div class="table-toolbar">
    <div class="btn-group">

        <?php echo anchor('quotation/add',  '<h4>Requerir Cuota<i class="fa fa-plus"></i></h4>',
            array('class' => 'btn btn-info')); ?>

    </div>
</div>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
           id="zest_table">
        <thead class="the-box dark full">
        <tr>
            <th class="hidden-xs">#</th>
            <th><?php echo lang("label_quotation_status"); ?></th>
            <th class="hidden-xs"><?php echo lang("label_quotation_created"); ?></th>
            <th><?php echo lang("label_quotation_company"); ?></th>
            <th><?php echo lang("label_quotation_budget"); ?></th>
            <th class="hidden-xs"><?php echo lang("label_quotation_assign"); ?></th>
            <th><?php echo lang("label_quotation_options"); ?></th>
        </tr>
        </thead>
        <tbody>

        <?php if (count($quotes)) : foreach ($quotes as $quote) :


            $quote_status = get_quote_status($quote->quote_project_status);
            if ($quote_status->status_name == 'PENDING') {
                $label = "info";
            } elseif ($quote_status->status_name == 'REVIEWED') {
                $label = "warning";
            } elseif ($quote_status->status_name == 'ACCEPTED') {
                $label = "success";
            } else {
                $label = "danger";
            }
            ?>
            <tr>
                <td class="hidden-xs"><?php echo $quote->quote_project_id; ?></td>
                <td><span class="label label-<?php echo $label;?>"><?php echo $quote_status->status_name; ?></span></td>
                <td class="hidden-xs"><?php echo $quote->quote_project_date; ?></td>
                <td><?php echo client_company($quote->client_id); ?></td>
                <td><?php echo format_currency($quote->quote_project_budget); ?></td>
                <td class="hidden-xs"><?php echo ($quote->quote_project_assign) ? users_details($quote->quote_project_assign,
                        1)->full_name : "--------"; ?></td>
                <td class="text-center">


                    <div class="btn-group-action">
                        <div class="btn-group">
                            <a class=" btn btn-info"
                               href="<?php echo base_url('quotation/preview/' . $quote->quote_project_id); ?>" title=""><i
                                    class="fa fa-eye"></i>Ver más</a>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret">&nbsp;</span>
                            </button>
                            <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                                <ul class="dropdown-menu pull-right">
                                    <li><a class="text-danger" data-toggle="zestModal"
                                           href="<?php echo base_url('quotation/assign/' . $quote->quote_project_id); ?>"><i
                                                class="fa fa-share-square-o"></i> Reasignar</a></li>
                                    <li><a href="<?php echo base_url(); ?>" title=""><i class="fa fa-trash"></i> Eliminar</a>
                                    </li>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>

                </td>
            </tr>
        <?php endforeach; endif; ?>
        </tbody>
    </table>
</div>
