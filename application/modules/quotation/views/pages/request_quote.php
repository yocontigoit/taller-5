<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/07/16
 * Time: 03:23 PM
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="the-box">
            <fieldset>
                <legend><i class="fa fa-bar-chart-o"></i><?php //echo Report ?>
                </legend>
                <?php echo form_open('quotation/add', array('role' => 'form')); ?>

                <input class="form-control" type="hidden" name="quote_number" value="<?php echo $quote_ref; ?>"/>
                <ol class="form-group">

                    <!-- Select Client-->
                    <?php if ($this->CompanyUser_id > -1) {//admin create a quote for a user... ?>
                        <li>
                            <fieldset>
                                <legend>
                                    Selecciona un cliente
                                </legend>
                                <ol>

                                    <li>
                                        <label>Cliente</label>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <select class="form-control" id="client_id" name='client_id' required>
                                                    <option>Selecciona un cliente</option>

                                                    <?php if (count($clients)):foreach ($clients as $client): ?>
                                                        <option
                                                            value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name); ?></option>
                                                    <?php endforeach; endif; ?>

                                                </select>
                                            </div>
                                        </div>
                                    </li>

                                </ol>
                            </fieldset>
                        </li>
                    <?php } else { ?>
                        <input type="hidden" name="client_id" value="<?php echo $this->CompanyClient_id ?>"/>
                    <?php } ?>
                    <!-- CATEGORY & SUB CATEGORY -->
                    <li>
                        <fieldset>
                            <legend>
                                ¿Qué trabajo se requiere?
                            </legend>
                            <ol>

                                <li>
                                    <label>Categoría</label>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <select class="form-control" id="quote_category" name='quote_category'>
                                                <option>Selecciona una categoría de trabajo (opcional)</option>

                                                <?php if (count($categories)):foreach ($categories as $category): ?>
                                                    <option
                                                        value="<?php echo $category->cat_id; ?>"><?php echo ucfirst($category->cat_name); ?></option>
                                                <?php endforeach; endif; ?>

                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li id="subcategory_link" style="display: none;">
                                    <label>Selecciona un trabajo (opcional)</label>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <select data-placeholder="Selecciona un trabajo(opcional)" class="form-control"
                                                    id="subcategory" tabindex="4" name="sub_category">


                                            </select>

                                        </div>
                                    </div>
                                </li>

                            </ol>
                        </fieldset>
                    </li>

                    <!-- PROJECT DETAILS -->
                    <li>

                        <fieldset>
                            <legend>
                                Cuéntanos más sobre el proyecto
                            </legend>
                            <ol>
                                <li>
                                    <label><?php echo lang('label_project_name'); ?>:</label>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <input id='project-name' type="text"
                                                   placeholder="¿Cuál es el título del proyecto?" class="form-control"
                                                   name='project_name' maxlength="150">
                                        </div>
                                        <!-- /.col-xs-3 col-sm-2 -->
                                    </div>
                                    <!-- /.row -->

                                </li>

                                <li>
                                    <label>Ingresa algunas habilidades relacionadas con el proyecto:</label>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <textarea class="form-control rounded bold-border summernote-sm"
                                                      name="skills"
                                                      placeholder="Describe tu proyecto aquí..."></textarea>
                                        </div>
                                        <!-- /.col-xs-3 col-sm-2 -->
                                    </div>
                                    <!-- /.row -->

                                </li>


                                <li>
                                    <label for="">Describe tu proyecto con detalle: </label>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <textarea class="form-control rounded bold-border summernote-sm"
                                                      name="project_description"
                                                      placeholder="Describe tu proyecto aquí..."></textarea>
                                        </div>
                                        <!-- /.col-xs-3 col-sm-2 -->
                                    </div>
                                    <!-- /.row -->

                                </li>

                                <li>
                                    <label>Ejemplo:</label>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <textarea class="form-control rounded bold-border summernote-sm"
                                                      name="examples"
                                                      placeholder="Da ejemplos para tu proyecto..."></textarea>
                                        </div>
                                        <!-- /.col-xs-3 col-sm-2 -->
                                    </div>
                                    <!-- /.row -->

                                </li>


                                <li>
                                    <label>Destacado:</label>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <textarea class="form-control rounded bold-border summernote-sm"
                                                      name="project_feature"
                                                      placeholder="Describe cosas a destacar..."></textarea>
                                        </div>
                                        <!-- /.col-xs-3 col-sm-2 -->
                                    </div>
                                    <!-- /.row -->

                                </li>


                            </ol>
                        </fieldset>
                    </li>


                    <!-- BUDGET -->
                    <li>
                        <fieldset>
                            <legend>
                                ¿Qué presupuesto tienes en mente?
                            </legend>
                            <ol>

                                <li>
                                    <label>Presupuesto:</label>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <select id='project-budget' class="form-control" name='budget'>

                                                <option value='188'>
                                                    Micro Proyecto
                                                    (<?php echo $currency; ?>10-30 )
                                                </option>

                                                <option value='5'>
                                                    Proyecto Simple
                                                    (<?php echo $currency; ?>30-250 )
                                                </option>

                                                <option value='1' selected>
                                                    Proyecto muy pequeño
                                                    (<?php echo $currency; ?> 250-750 )
                                                </option>

                                                <option value='2'>
                                                    Proyecto pequeño
                                                    (<?php echo $currency; ?>750-1500 )
                                                </option>

                                                <option value='3'>
                                                    Proyecto mediano
                                                    (<?php echo $currency; ?> 1500-3000 )
                                                </option>

                                                <option value='4'>
                                                    Proyecto grande
                                                    (<?php echo $currency; ?>3000-5000 )
                                                </option>

                                                <option value='6'>
                                                    Proyecto más grande
                                                    (<?php echo $currency; ?> >5000 )
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="project-duration">Duración del proyecto:</label>
                                            <select id='project-duration' class="form-control" name='duration'>

                                                <option value='1'>
                                                    Menos de una semana
                                                </option>

                                                <option value='2' selected>
                                                    De 1 a 4 semanas
                                                </option>

                                                <option value='3'>
                                                    De 1 a 3 meses
                                                </option>

                                                <option value='4'>
                                                    De 3 a 6 meses
                                                </option>

                                                <option value='5'>
                                                    Más de 6 meses
                                                </option>

                                                <option value='6'>
                                                    No estás seguro
                                                </option>

                                            </select>
                                        </div>
                                    </div>
                                </li>


                            </ol>
                        </fieldset>
                    </li>


                    <!-- Time -->
                    <li>
                        <fieldset>
                            <legend>
                                ¿Para cuándo quieres tu proyecto?
                            </legend>
                            <ol>

                                <li>
                                    <label>Empieza:</label>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control datepicker" name="start_date"
                                                   data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                                   value="<?php echo date('d-m-Y') ?>">
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <label for="project-duration">Termina:</label>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control datepicker" name="end_date"
                                                   data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                                   value="<?php echo date('d-m-Y') ?>">
                                        </div>
                                    </div>
                                </li>


                            </ol>
                        </fieldset>
                    </li>

                </ol>


                <div class="row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-success btn-block btn-lg"><i
                                class="fa fa-sign-in"></i> <?php echo lang('form_button_add_quotation'); ?></button>
                    </div>
                </div>


                <?php echo form_close(); ?>
            </fieldset>
        </div>
    </div>
</div>