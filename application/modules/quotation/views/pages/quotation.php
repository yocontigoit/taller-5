<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/07/16
 * Time: 03:23 PM
 */
?>
<div class="row">
    <div class="col-md-12">
        <?php
        $quote_status = get_quote_status($quote_details->quote_project_status);

        if ($quote_status->status_name == 'PENDING' || $quote_status->status_name == 'REVIEWED') {
            $display = 'disabled="disabled"';
            $editable = '';
        } else {
            $display = "";
            $editable = 'disabled="disabled"';
        }

        $estimate_details = get_quote_estimates($quote_details->estimate_reference);
        ?>
        <?php if ($this->CompanyUserRolePermission_id == 1 && $estimate_details->referece == '') { ?>
            <div class="the-box toolbar no-border no-margin">
                <div class="btn-toolbar" role="toolbar">

                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-tag"></i>
                            Más opciones
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <?php if (count($quotation_status)):foreach ($quotation_status as $status): ?>
                                <?php if ($quote_status->status_name != $status->status_name && $status->status_name != "ESTIMATED") { ?>
                                    <li>
                                        <a href="<?php echo base_url('quotation/update_status/' . $quote_details->quote_project_id . '/' . $status->status_id); ?>"><?php echo ucfirst(strtolower($status->status_name)); ?></a>
                                    </li>
                                <?php } ?>
                            <?php endforeach; endif; ?>
                        </ul>
                    </div>

                    <?php if ($quote_details->estimated == 0) { ?>
                        <div class="btn-group pull-right">

                            <a class="btn btn-small  btn-danger btn-preview-invoice"
                               href="<?php echo base_url('estimate/create_estimate/' . $quote_details->quote_project_id); ?>">
                                <i class="fa fa-plus"></i>
                                Crear estimado
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <!-- /.btn-toolbar -->
            </div>
        <?php } ?>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-4">
            <div class="the-box">

                <fieldset>
                    <?php $customer_details = get_client_details($quote_details->client_id); ?>
                    <?php if(count($customer_details)){ ?>
                    <legend><i class="fa fa-user"></i><?php echo $customer_details->client_name; ?>

                    </legend>

                    <div class="form-horizontal">
                        <div class="row">
                            <label class="control-label col-lg-3"><?php echo lang('label_email'); //Email ?></label>

                            <div class="col-lg-9">
                                <p class="form-control-static"><?php echo $customer_details->client_email; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <label class="control-label col-lg-3"><?php echo lang('label_phone'); //Phone?></label>

                            <div class="col-lg-9">
                                <p class="form-control-static"> <?php echo $customer_details->client_website; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <label class="control-label col-lg-3"><?php echo lang('label_country'); //Country?></label>

                            <div class="col-lg-9">
                                <p class="form-control-static"><?php echo $customer_details->client_country; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <label class="control-label col-lg-3"><?php echo lang('label_city');//City?></label>

                            <div class="col-lg-9">
                                <p class="form-control-static"><?php echo $customer_details->client_city; ?></p>
                            </div>
                        </div>

                    </div>
                    <?php }else{ ?>
                    Por favor agrega el contacto del cliente.
                    <?php } ?>
                </fieldset>

            </div>

            <div class="the-box">
                <fieldset>

                    <legend><i class="fa fa-bar-chart-o"></i><?php if (count($estimate_details)) {
                            echo $estimate_details->referece;
                        } ?>
                    </legend>

                    <div class="row billing_top_row">
                        <div class="billing_settings">
                            <ul class="left_box">

                                <li class="revenue_today">
                                    <?php if ($quote_status->status_name == 'PENDING') {
                                        $label = "info";
                                    } elseif ($quote_status->status_name == 'REVIEWED') {
                                        $label = "warning";
                                    } elseif ($quote_status->status_name == 'ACCEPTED') {
                                        $label = "success";
                                    } else {
                                        $label = "danger";
                                    }
                                    ?>
                                    <div>
                                        <em>Estatus</em>
                                        <span
                                            class="label label-<?php echo $label; ?>"><?php echo $quote_status->status_name; ?></span>
                                    </div>
                                </li>


                                <?php if (count($estimate_details)): ?>
                                    <li class="revenue_today">
                                        <div>
                                            <em>Fecha de vencimiento estimada</em>
                                            <span class="more"><?php echo $estimate_details->due_date; ?></span>
                                        </div>
                                    </li>
                                    <li class="revenue_month">
                                        <div>
                                            <em>Cantidad estimada</em>
                                            <span class="more"><?php echo $estimate_details->amount; ?></span>
                                        </div>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>

                </fieldset>
            </div>
        </div>
        <div class="col-md-8">
            <div class="the-box">
                <fieldset>
                    <legend><i class="fa fa-bar-chart-o"></i><?php echo $quote_details->quote_code; ?>
                    </legend>
                    <?php echo form_open('quotation/add', array('role' => 'form')); ?>

                    <ol class="form-group">
                        <!-- CATEGORY & SUB CATEGORY -->
                        <li>
                            <fieldset>
                                <legend>
                                    ¿Qué trabajo requieres?
                                </legend>
                                <ol>

                                    <li>
                                        <label>Categoría</label>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <select id='project-category' class="form-control" id="quote_category"
                                                        name='quote_category' <?php echo $editable; ?>>
                                                    <option>Selecciona la categoría de trabajo (opcional)</option>

                                                    <?php if (count($categories)):foreach ($categories as $category): ?>
                                                        <option
                                                            value="<?php echo $category->cat_id; ?>"><?php echo ucfirst($category->cat_name); ?></option>
                                                    <?php endforeach; endif; ?>

                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    <li style="display: none;">
                                        <label>Selecciona un trabajo(opcional)</label>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <select data-placeholder="Selecciona un trabajo(opcional)" class="form-control"
                                                        id="subcategory" tabindex="4" name="sub_category">


                                                </select>

                                            </div>
                                        </div>
                                    </li>

                                </ol>
                            </fieldset>
                        </li>

                        <!-- PROJECT DETAILS -->
                        <li>

                            <fieldset>
                                <legend>
                                    Cuéntanos más sobre el proyecto
                                </legend>
                                <ol>
                                    <li>
                                        <label><?php echo lang('label_project_name'); ?>:</label>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <input id='project-name' type="text" class="form-control"
                                                       name='project_name' maxlength="150"
                                                       value="<?php echo $quote_details->quote_project_name; ?>" <?php echo $editable; ?>>
                                            </div>
                                            <!-- /.col-xs-3 col-sm-2 -->
                                        </div>
                                        <!-- /.row -->

                                    </li>

                                    <li>
                                        <label>Ingresa unas habilidades relacionadas con el proyecto:</label>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <textarea class="form-control rounded bold-border summernote-sm" name="skills"
                                                          placeholder="Describe tu proeycto aquí..." <?php echo $editable; ?>><?php echo e($quote_details->quote_project_skills); ?></textarea>
                                            </div>
                                            <!-- /.col-xs-3 col-sm-2 -->
                                        </div>
                                        <!-- /.row -->

                                    </li>


                                    <li>
                                        <label for="">Describe tu proyecto con detalle: </label>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <textarea class="form-control rounded bold-border summernote-sm"
                                                          name="project_description"
                                                          placeholder="Describe tu proyecto aquí..." <?php echo $editable; ?>><?php echo e($quote_details->quote_project_desc); ?></textarea>
                                            </div>
                                            <!-- /.col-xs-3 col-sm-2 -->
                                        </div>
                                        <!-- /.row -->

                                    </li>

                                    <li>
                                        <label>Ejemplos:</label>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <textarea class="form-control rounded bold-border summernote-sm" name="examples"
                                                          placeholder="Da ejemplos para tu proyecto..." <?php echo $editable; ?>><?php echo e($quote_details->quote_project_examples); ?></textarea>
                                            </div>
                                            <!-- /.col-xs-3 col-sm-2 -->
                                        </div>
                                        <!-- /.row -->

                                    </li>


                                    <li>
                                        <label>Destacado:</label>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <textarea class="form-control rounded bold-border summernote-sm"
                                                          name="project_feature"
                                                          placeholder="Describe cosas a destacar..." <?php echo $editable; ?>><?php echo e($quote_details->quote_project_feature); ?></textarea>
                                            </div>
                                            <!-- /.col-xs-3 col-sm-2 -->
                                        </div>
                                        <!-- /.row -->

                                    </li>


                                </ol>
                            </fieldset>
                        </li>

                        <!-- BUDGET -->
                        <li>
                            <fieldset>
                                <legend>
                                    ¿Qué presupuesto tienes en mente?
                                </legend>
                                <ol>

                                    <li>
                                        <label>Presupuesto:</label>


                                        <div class="row">
                                            <div class="col-md-12">
                                                <input id='project-budget' type="text" class="form-control"
                                                       name='project_budget' maxlength="150"
                                                       value="<?php echo $quote_details->quote_project_budget; ?>" <?php echo $editable; ?>>
                                            </div>
                                            <!-- /.col-xs-3 col-sm-2 -->
                                        </div>
                                        <!-- /.row -->

                                    </li>
                                    <li>
                                        <label for="project-duration"><?php echo lang("label_project_duration"); ?>
                                            :</label>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <input id='project-duration' type="text" class="form-control"
                                                       name='project_duration' maxlength="150"
                                                       value="<?php echo $quote_details->quote_project_duration; ?>" <?php echo $editable; ?>>
                                            </div>
                                            <!-- /.col-xs-3 col-sm-2 -->
                                        </div>
                                        <!-- /.row -->


                                    </li>

                                </ol>
                            </fieldset>
                        </li>

                        <!-- Time -->
                        <li>
                            <fieldset>
                                <legend>
                                    ¿Para cuándo quieres el proyecto?
                                </legend>
                                <ol>

                                    <li>
                                        <label>Empieza:</label>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control datepicker" name="start_date"
                                                       data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                                       value="<?php echo date('d-m-Y',
                                                           strtotime($quote_details->quote_start_date)); ?>">
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <label for="project-duration">Termina:</label>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control datepicker" name="end_date"
                                                       data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                                       value="<?php echo date('d-m-Y',
                                                           strtotime($quote_details->quote_end_date)); ?>">
                                            </div>
                                        </div>
                                    </li>


                                </ol>
                            </fieldset>
                        </li>

                    </ol>

                    <?php if ($estimate_details->referece == '') { ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-success btn-block btn-lg"><i
                                        class="fa fa-sign-in"></i> <?php echo lang('form_button_save_quotation'); ?>
                                </button>
                            </div>
                        </div>
                    <?php } ?>


                    <?php echo form_close(); ?>
                </fieldset>
            </div>
        </div>
    </div>
</div>
