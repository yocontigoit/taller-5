<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/04/12
 * Time: 11:08 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                    <i class="fa fa-times"></i>
                    </span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_re_assign'); ?></h4>
        </div>
        <form method="post" action="<?php echo base_url('quotation/assign'); ?>">
            <div class="modal-body">
                <input type="hidden" name="quotation_id" value="<?php echo $quotation_id; ?>">

                <div class="form-group">
                    <label for="worker">Assigned to</label>
                    <select data-placeholder="Assign" class="form-control chosen-select" tabindex="4"
                            name="quote_assign">
                        <optgroup label="<?php echo lang("label_users") ?>">

                            <?php if (count($users)) : foreach ($users as $user): ?>
                                <?php if ($user->user_id == $quote->quote_project_assign) { ?>
                                    <option value="<?php echo $user->user_id; ?>"
                                            selected="selected"> <?php echo ucfirst($user->emp_name) . " " . ucfirst($user->emp_surname); ?>
                                    </option>
                                <?php } else { ?>
                                    <option
                                        value="<?php echo $user->user_id; ?>"><?php echo ucfirst($user->emp_name) . " " . ucfirst($user->emp_surname); ?>
                                    </option>
                                <?php } ?>

                            <?php endforeach; endif; ?>
                        </optgroup>
                    </select>


                </div>
                <div class="modal-footer">
                    <button class="btn btn-success btn-icon" value="addNewTask" name="submit" type="input">
                        <i class="fa fa-check-square-o"></i>
                        <?php echo lang('form_button_save'); ?>
                    </button>
                    <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                        <i class="fa fa-times-circle-o"></i>
                        <?php echo lang('form_button_cancel'); ?>
                    </button>
                </div>


            </div>
        </form>
    </div>


</div>

<script type="text/javascript">
    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }
</script>