<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/11
 * Time: 12:07 PM
 */
class Quotation extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('quotation_m');
    }

    /**
     * list all quotations
     */
    function index()
    {
        //$this->check_action_permission('search');
        $this->data['content'] = 'quotation/quotation_details';
        $this->data['title'] = 'Cotizaciones';


        if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) {//if admin

            $this->data['quotes'] = $this->quotation_m->get_all_quotes();

        } else {

            $this->data['quotes'] = $this->quotation_m->get_client_quotes($this->CompanyClient_id);
        }

        $this->data['_QUOTE_PAGE'] = $this->load->view('pages/index', $this->data, true);
        $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * add quotation
     */
    function add()
    {

        //$this->check_action_permission('add_update');
        $this->data['content'] = 'quotation/quotation_details';
        $this->data['title'] = 'Cotizaciones';
        if ($this->input->post()) {


            $param = array(
                "client_id" => $this->input->post('client_id'),
                "quote_code" => $this->input->post('quote_number'),
                "quote_project_name" => $this->input->post('project_name'),
                "quote_project_cat" => $this->input->post('quote_category'),
                "quote_project_sub_cat" => $this->input->post('sub_category'),
                "quote_project_skills" => $this->input->post('skills'),
                "quote_project_desc" => $this->input->post('project_description'),
                "quote_project_examples" => $this->input->post('examples'),
                "quote_project_feature" => $this->input->post('project_feature'),
                "quote_project_budget" => $this->input->post('budget'),
                "quote_project_duration" => $this->input->post('duration'),
                "quote_project_date" => date('Y-m-d H:i:s'),
                "quote_start_date" => date('Y-m-d', strtotime($this->input->post('start_date'))),
                "quote_end_date" => date('Y-m-d', strtotime($this->input->post('end_date'))),
                "quote_project_status" => 1

            );

            $quote_id = $this->quotation_m->add_quote($param);
            if ($quote_id) {
                $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'quotation',
                    'activity_details' => "new quote",
                    'activity_module_id' => $quote_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Agregar una nueva cita que haya sido creada",
                    'activity_icon' => 'fa fa-clipboard'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_quotation_add_success'));
                redirect('quotation/preview/' . $quote_id);
            }


        } else {
            $client_currency = get_client_details($this->CompanyClient_id);

            if ($client_currency) {
                $this->data['currency'] = get_currency_symbol($client_currency->client_currency);
            } else {
                $this->data['currency'] = get_currency_symbol($this->config->item('local')->company_currency);
            }
            $this->data['quote_ref'] = $this->config->item('invoice')->quotation_prefix . "-" . trans_reference(12,
                    $this->Company_id);
            $this->data['categories'] = $this->quotation_m->get_quote_categories();
            $this->data['clients'] = $this->quotation_m->get_company_clients($this->Company_id);
            $this->data['show_quote_subcategory'] = true;
            $this->data['message'] = true;
            $this->data['datatable'] = true;
            $this->data['show_datepicker'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->data['_QUOTE_PAGE'] = $this->load->view('pages/request_quote', $this->data, true);
        }

        $this->load->view('_main_layout', $this->data);
    }

    /**
     * save quotation activities
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->quotation_m->add_log($activity_data);

    }

    /**
     * view selected quotation
     * @param null $quote_id
     */
    function preview($quote_id = null)
    {

        $this->data['title'] = 'Cotizaciones';
        $this->data['content'] = 'quotation/quotation_details';
        $this->data['quote_details'] = $this->quotation_m->get_quote_details($quote_id);
        $this->data['categories'] = $this->quotation_m->get_quote_categories();
        $this->data['quotation_status'] = $this->quotation_m->get_quote_status();
        $this->data['show_datepicker'] = true;
        $this->data['message'] = true;
        $this->data['SCRIPT_PAGE'] = true;

        $this->data['_QUOTE_PAGE'] = $this->load->view('pages/quotation', $this->data, true);//Code data
        $this->load->view('_main_layout', $this->data);

    }

    /**
     * assign user to a quotation
     * @param null $quotation_id
     */
    function assign($quotation_id = null)
    {

        if ($this->input->post()) {

            $quotation_id = intval($this->input->post('quotation_id'));
            $assigned_to = $this->input->post('quote_assign');
            $param = array(
                "quote_project_assign" => $assigned_to

            );
            if ($this->quotation_m->update_assignment($param, $quotation_id)) {

                //$this->notification->send_quotation_assigned_notification($assigned_to,$quotation_id);
                redirect('quotation');
            }
        } else {


            $this->data['quotation_id'] = $quotation_id;
            $this->data['users'] = $this->quotation_m->get_company_users($this->Company_id);
            $this->data['quote'] = $this->quotation_m->get_quote_details($quotation_id);
            $this->load->view('modal/assign', $this->data);
        }

    }

    /**
     * update quotation  status
     * @param null $quote_id
     * @param null $status_id
     */
    function update_status($quote_id = null, $status_id = null)
    {

        if ($this->quotation_m->update_quote_status($quote_id, $status_id)) {

            $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $log_params = array(
                'activity_user' => $activity_user,
                'activity_user_type' => $this->CompanyUser_type,
                'activity_module' => 'quotation',
                'activity_details' => "quotation update",
                'activity_module_id' => $quote_id,
                'activity_status' => "success",
                'activity_date' => date('Y-m-d,H:i:s'),
                'activity_event' => "Estatus de cita actualizado exitosamente",
                'activity_icon' => 'fa fa-clipboard'
            );

            $this->__activity_tracker($log_params);
            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', lang('messages_quotation_save_success'));
            redirect('quotation/preview/' . $quote_id);

        }
    }

    /**
     * get sub categories
     */
    function get_subcategory()
    {

        if ($this->input->post('category_id')) {
            $category_id = intval($this->input->post('category_id'));
            $subcategories = array();
            $subcategories = $this->quotation_m->get_sub_category($category_id);
            echo json_encode($subcategories);
            exit();
        }
    }
}