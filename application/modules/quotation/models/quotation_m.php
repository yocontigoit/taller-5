<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/01/06
 * Time: 04:20 PM
 */
class Quotation_M extends CI_Model
{


    /**
     * Get all users for a company
     * @param null $id
     * @return mixed
     */
    function get_company_users($id = null)
    {

        $company_id = intval($id);
        $this->db->from('users');
        $this->db->join('employee', 'employee.emp_id  = users.user_id');
        $this->db->where("company_id", $company_id);

        return $this->db->get()->result();

    }

    /**
     * Add new quotation
     * @param $data
     * @return null
     */
    function add_quote($data)
    {

        $success = false;

        $this->db->trans_start();
        $reference_no = explode("-", $data['quote_code']);
        $success = $this->db->insert('quotation', $data);
        $quote_id = $this->db->insert_id();
        if ($success) {

            $success = $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
                array('typeid' => 12, 'company_id' => $this->Company_id));
        }

        $this->db->trans_complete();

        return ($success) ? $quote_id : null;


    }


    /**
     * get all quotations
     * @return mixed
     */
    function get_all_quotes()
    {

        $this->db->from('quotation');

        return $this->db->get()->result();
    }

    /**
     * get client quotations
     * @param null $client_id
     * @return mixed
     */
    function get_client_quotes($client_id = null)
    {

        $this->db->from('quotation');
        $this->db->where("client_id", $client_id);

        return $this->db->get()->result();
    }

    /**
     * get quote categories
     * @return mixed
     */
    function get_quote_categories()
    {

        $this->db->from('categories');

        //$this->db->where("company_id", $company_id);
        return $this->db->get()->result();
    }

    /**
     * get quotation status
     * @return mixed
     */
    function get_quote_status()
    {

        $this->db->from('quote_status');

        return $this->db->get()->result();
    }

    /**
     * update quotation status
     * @param null $quote_id
     * @param null $status_id
     * @return bool
     */
    function  update_quote_status($quote_id = null, $status_id = null)
    {
        $success = false;
        $success = $this->db->update('quotation', array('quote_project_status' => intval($status_id)),
            array('quote_project_id' => intval($quote_id)));

        return $success;
    }

    /**
     * assign user to
     * @param array $data
     * @param null $quote_id
     * @return bool
     */
    function update_assignment($data = array(), $quote_id = null)
    {

        $success = false;
        $success = $this->db->update('quotation', $data, array('quote_project_id' => intval($quote_id)));

        return $success;
    }

    function get_sub_category($category_id = null)
    {

        $this->db->from('sub_categories');
        $this->db->where("cat_id", $category_id);

        return $this->db->get()->result();
    }

    function get_quote_details($quote_id = null)
    {

        $this->db->from('quotation');
        $this->db->where("quote_project_id", $quote_id);

        return $this->db->get()->row();

    }

    /**
     * get client by company
     * @param null $id
     * @return mixed
     */
    function get_company_clients($id = null)
    {

        $company_id = intval($id);
        $this->db->from('client');
        $this->db->where("company_id", $company_id);

        return $this->db->get()->result();

    }

    function add_log()
    {


    }

}