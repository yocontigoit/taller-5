<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/09
 * Time: 02:29 PM
 */
class Expenses extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();
        $this->load->model("expenses_m");
    }

    /**
     * display all expenses and expenses overview
     */
    function index()
    {
        $this->data['content'] = 'expenses/expenses';
        $this->data['title'] = 'Finanzas';

        $expense_page = $this->input->get('status', true) ? $this->input->get('status', true) : 'overview';

        $this->data['active_page'] = $expense_page;
        switch ($expense_page) {

            case 'overview':
                $this->data['page_title'] = 'Descripción de los gastos';
                $this->data['show_expenses_overview'] = true;
                $this->data['_PAGE'] = $this->load->view('pages/overview', $this->data, true);

                break;
                case 'caja':
                $this->data['expenses'] = $this->expenses_m->all_caja(); // all company trans..
                $this->data['expense_details'] = $this->expenses_m->expense_details();
                $this->data['transactions'] = $this->expenses_m->transactions_caja();
                $this->data['realtransactions'] = $this->expenses_m->get_realtransactions();
                $this->data['obras'] = $this->expenses_m->obra();
                $this->data['datatable'] = true;
                $this->data['SCRIPT_PAGE'] = true;
                $this->data['page_title'] = 'Relación Caja Chica';
                $this->data['show_expenses_overview'] = true;
                $this->data['_PAGE'] = $this->load->view('pages/caja', $this->data, true);

                    break;
                    case 'estimaciones':
                    $this->data['expenses'] = $this->expenses_m->all_expenses(); // all company trans..
                    $this->data['projects'] = $this->expenses_m->get_projects();


                    if ($this->CompanyUserRolePermission_id == 1) {//if admin

                        $this->data['transactions'] = $this->expenses_m->all_invoice();
                    } elseif ($this->CompanyUserRolePermission_id == 2) {

                        $this->data['transactions'] = null;
                    } else {

                        $this->data['transactions'] = $this->expenses_m->client_all_invoice($this->CompanyClient_id); // all company trans..

                    }
                        $this->data['page_title'] = 'Estimaciones';
                        $this->data['show_expenses_overview'] = true;
                        $this->data['SCRIPT_PAGE'] = true;
                        $this->data['_PAGE'] = $this->load->view('pages/Estimaciones', $this->data, true);

                        break;
                        case 'Cuentas':
                        $this->data['expenses'] = $this->expenses_m->all_expenses(); // all company trans..
                        $this->data['methods'] = $this->expenses_m->all_methods(); // all company trans..
                        $this->data['transactions'] = $this->expenses_m->get_transactions();

                        if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) {//if admin

                            $this->data['estimates'] = $this->expenses_m->get_all_estimates();

                        } else {
                            $this->data['estimates'] = $this->expenses_m->get_client_estimates($this->CompanyClient_id);

                        }

                            $this->data['page_title'] = 'Cuentas';
                            $this->data['show_expenses_overview'] = true;
                            $this->data['_PAGE'] = $this->load->view('pages/cuentas', $this->data, true);

                            break;
                               case 'Bancos':
                        $this->data['expenses'] = $this->expenses_m->all_expenses(); // all company trans..
                        $this->data['methods'] = $this->expenses_m->all_methods(); // all company trans..
                        $this->data['transactions'] = $this->expenses_m->get_transactions();

                        if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) {//if admin

                            $this->data['estimates'] = $this->expenses_m->get_all_estimates();

                        } else {
                            $this->data['estimates'] = $this->expenses_m->get_client_estimates($this->CompanyClient_id);

                        }

                            $this->data['page_title'] = 'Cuentas';
                            $this->data['show_expenses_overview'] = true;
                            $this->data['_PAGE'] = $this->load->view('pages/bancos', $this->data, true);

                            break;
                        case 'finanzas':
                            $this->data['offices'] = $this->expenses_m->all_office();
                            $this->data['page_title'] = 'Finanzas oficina';
                            $this->data['show_expenses_overview'] = true;
                            $this->data['_PAGE'] = $this->load->view('pages/finanzas', $this->data, true);

                            break;
                            case 'programs':
                                $this->data['programs'] = $this->expenses_m->all_programs();
                            $this->data['datatable'] = true;
                            $this->data['SCRIPT_PAGE'] = true;

                            $this->data['offices'] = $this->expenses_m->all_office();
                            $this->data['page_title'] = 'Programaciones';
                            $this->data['show_expenses_overview'] = true;
                            $this->data['_PAGE'] = $this->load->view('pages/programs', $this->data, true);

                            break;
            case 'expenses':
                $this->data['page_title'] = 'Gastos';
                if ($this->CompanyUserRolePermission_id == 1) {//if admin

                    $this->data['expenses'] = $this->expenses_m->all_expenses(); // all company trans..
                } elseif ($this->CompanyUserRolePermission_id == 2) {

                    $this->data['expenses'] = null;
                } else {

                    $this->data['expenses'] = $this->expenses_m->client_expenses($this->CompanyClient_id); // all client trans..

                }

                $this->data['_PAGE'] = $this->load->view('pages/index', $this->data, true);


                break;

        }
        $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * display expense details
     * @param null $expense_id
     */
    function view($expense_id = null )
    {

        $this->data['expenses'] = $this->expenses_m->caja_details(intval($this->uri->segment(3))); // all company trans..
        $this->data['content'] = 'expenses/expenses';
        $this->data['title'] = 'Gastos';
        $this->data['page_title'] = 'Detalles';
        $this->data['active_page'] = 'index';
        $this->data['expense_details'] = $this->expenses_m->expense_details($expense_id);
        $this->data['_PAGE'] = $this->load->view('pages/details', $this->data, true);
        $this->data['show_expenses_details'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);

    }
     function viewprog($expense_id = null )
    {
$this->data['programs'] = $this->expenses_m->all_programs();
        $this->data['expenses'] = $this->expenses_m->caja_details(intval($this->uri->segment(3))); // all company trans..
        $this->data['content'] = 'expenses/expenses';
        $this->data['title'] = 'Gastos';
        $this->data['page_title'] = 'Detalles';
        $this->data['active_page'] = 'index';
        $this->data['expense_details'] = $this->expenses_m->expense_details($expense_id);
        $this->data['_PAGE'] = $this->load->view('pages/detailsprog', $this->data, true);
        $this->data['show_expenses_details'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);

    }
     function view_statement($setting_id = null)
    {

        $this->data['content'] = 'expenses/expenses';
        $this->data['title'] = 'Gastos';
        $this->data['page_title'] = 'Estado de Cuenta';
        $this->data['active_page'] = 'statement';
        $this->data['setting_id'] = intval($this->uri->segment(3));
        $this->data['transactions'] = $this->expenses_m->get_all_transactions();
        $this->data['methods'] = $this->expenses_m->all_methods($setting_id); // all company trans..
        $this->data['method_details'] = $this->expenses_m->method_details($setting_id);
        $this->data['_PAGE'] = $this->load->view('pages/statement', $this->data, true);
        $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);

    }
    function caja_statement()
    {
        $this->data['content'] = 'pages/caja_statement';
        $this->data['title'] = 'Historial de caja chica';
            $this->data['expenses'] = $this->expenses_m->all_caja(); // all company trans..
                $this->data['expense_details'] = $this->expenses_m->expense_details();
                $this->data['transactions'] = $this->expenses_m->transactions_caja();
                $this->data['realtransactions'] = $this->expenses_m->get_realtransactions();
                $this->data['obras'] = $this->expenses_m->obra();
                $this->data['datatable'] = true;
                $this->data['SCRIPT_PAGE'] = true;
                $this->data['page_title'] = 'Relación Caja Chica';
                $this->data['show_expenses_overview'] = true;
                $this->data['_PAGE'] = $this->load->view('pages/caja_statement', $this->data, true);
                 $this->load->view('_main_layout', $this->data);

    }

    /**
     * display expense details
     * @param null $expense_id
     */
    function view_details($cost_id = null )
    {

        $this->data['expense_detail'] = $this->expenses_m->caja_details(intval($this->uri->segment(3))); // all company trans..
        $this->data['content'] = 'expenses/expenses';
        $this->data['title'] = 'Gastos';
        $this->data['page_title'] = 'Detalles';
        $this->data['active_page'] = 'index';
        $this->data['expenses'] = $this->expenses_m->expense_details($cost_id);
        $this->data['_PAGE'] = $this->load->view('pages/details_vaciado', $this->data, true);
        $this->data['show_expenses_details'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);

    }

    /**
     * display expense details
     * @param null $expense_id
     */
    function view_oficina($id_oficina = null )
    {

        $this->data['expense_detail'] = $this->expenses_m->caja_details(intval($this->uri->segment(3))); // all company trans..
        $this->data['content'] = 'expenses/expenses';
        $this->data['title'] = 'Gastos';
        $this->data['page_title'] = 'Detalles';
        $this->data['active_page'] = 'index';
        $this->data['expenses'] = $this->expenses_m->office_details($id_oficina);
        $this->data['_PAGE'] = $this->load->view('pages/details_oficina', $this->data, true);
        $this->data['show_expenses_details'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);

    }

    /**
     * display prestamo details
     * @param null $expense_id
     */
    function view_prestamo($caja_id = null)
    {

        $this->data['expense_detail'] = $this->expenses_m->caja_details(intval($this->uri->segment(3))); // all company trans..
        $this->data['content'] = 'expenses/expenses';
        $this->data['title'] = 'Prestamos';
        $this->data['page_title'] = 'Detalles';
        $this->data['active_page'] = 'index';
        $this->data['caja_id'] = intval($this->uri->segment(3));
        $this->data['transactions'] = $this->expenses_m->all_caja(intval($this->uri->segment(3)));
             $this->data['realtransactions'] = $this->expenses_m->get_realtransactions(intval($this->uri->segment(3)));
        $this->data['expenses'] = $this->expenses_m->all_caja(intval($this->uri->segment(3)));

        $this->data['_PAGE'] = $this->load->view('pages/prestamo', $this->data, true);
        $this->data['show_expenses_details'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);

    }

    /**
     * add new expense
     */
    function add()
    {

        $this->data['title'] = 'Costos';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['obra'] = $this->expenses_m->obra();
        $caja->data['caja'] = $this->expenses_m->all_caja();
        $this->data['categories'] = $this->expenses_m->categories();
        $this->load->view('modal/expense', $this->data);
    }

    /**
     * add new expense
     */
    function add3()
    {
        $this->data['title'] = 'Prestamos';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['obra'] = $this->expenses_m->obra();
        $caja->data['caja'] = $this->expenses_m->all_caja();
        $this->data['categories'] = $this->expenses_m->categories();
        $this->load->view('modal/expense2', $this->data);
    }



     function prove()
    {
        $caja_id = intval($this->uri->segment(3));
        $project_id = intval($this->uri->segment(4));
        $this->data['caja_id'] = $caja_id;
        $this->data['project_id'] = $project_id;
        $this->data['title'] = 'Costos';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['cajas'] = $this->expenses_m->caja_details($caja_id);
        $this->data['transactions'] = $this->expenses_m->get_realtransactions($caja_id);
        $this->data['estimaciones'] = $this->expenses_m->estimaciones($project_id);
        $this->data['imports'] = $this->expenses_m->imports();
        $this->load->view('modal/prove', $this->data);
    }
    function replace()
    {

        $this->data['title'] = 'Costos';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['expenses'] = $this->expenses_m->all_caja(); // all company trans..
        $this->data['transactions'] = $this->expenses_m->get_realtransactions();
        $this->data['realtransactions'] = $this->expenses_m->transactions_caja();
        $this->data['obra'] = $this->expenses_m->obra_row(intval($this->uri->segment(3)));
        $this->data['project_id'] = intval($this->uri->segment(3));
        $this->data['categories'] = $this->expenses_m->categories();
        $this->load->view('modal/replace', $this->data);
    }

    function replace1()
    {

        $this->data['title'] = 'Costos';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['expenses'] = $this->expenses_m->all_caja(); // all company trans..
        $this->data['transactions'] = $this->expenses_m->get_realtransactions();
        $this->data['realtransactions'] = $this->expenses_m->transactions_caja();
        $this->data['obra'] = $this->expenses_m->obra();
        $this->data['project_id'] = intval($this->uri->segment(3));
        $this->data['categories'] = $this->expenses_m->categories();
        $this->load->view('modal/replace1', $this->data);
    }

    function add2()
    {

        $this->data['title'] = 'Costos';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['obra'] = $this->expenses_m->obra();
        $this->data['categories'] = $this->expenses_m->categories();
        $this->load->view('modal/expense1', $this->data);
    }
    function estimate()
    {

        $this->data['title'] = 'Crear Estimación';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['obra'] = $this->expenses_m->obra();
        $this->data['categories'] = $this->expenses_m->categories();
        $this->load->view('modal/estimate', $this->data);
    }
    function new_account()
    {

        $this->data['title'] = 'Crear Cuenta';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['obra'] = $this->expenses_m->obra();
        $this->data['categories'] = $this->expenses_m->categories();
        $this->load->view('modal/estimate', $this->data);
    }
       function new_payment_method()
    {

        $this->data['title'] = 'Crear Cuenta';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['obra'] = $this->expenses_m->obra();
        $this->data['categories'] = $this->expenses_m->categories();
        $this->load->view('modal/estimate2', $this->data);
    }

     function new_balance()
    {

        $this->data['title'] = 'Balance de cuenta';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['obra'] = $this->expenses_m->obra();
        $this->data['categories'] = $this->expenses_m->categories();
        $this->load->view('modal/balance', $this->data);
    }

     function edit_account()
    {

        $this->data['title'] = 'Editar cuenta';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['obra'] = $this->expenses_m->obra();
        $this->data['categories'] = $this->expenses_m->categories();
        $this->load->view('modal/edit_account', $this->data);
    }

    function edithon()
    {

        $this->data['title'] = 'Crear Estimación';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['obra'] = $this->expenses_m->obra();
        $this->data['categories'] = $this->expenses_m->categories();
        $this->load->view('modal/edithon', $this->data);
    }
    function add_office()
    {

        $this->data['title'] = 'Costos';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['obra'] = $this->expenses_m->obra();
        $this->data['categories'] = $this->expenses_m->categories();
        $this->load->view('modal/expense_office', $this->data);
    }

    /*
     * delete expense
     */
    function deleteperm()
    {


            $this->data['title'] = 'Eliminar Gasto';
            $this->data['cost_id'] = intval($this->uri->segment(3));
            $this->load->view('modal/delete', $this->data);


    }

    /*
     * delete caja_chica
     */
    function delete_caja()
    {


            $this->data['title'] = 'Eliminar Gasto';
            $this->data['caja_id'] = intval($this->uri->segment(3));
            $this->load->view('modal/delete_caja', $this->data);


    }

    function delete()
    {

        $this->data['content'] = 'expenses/expenses';
        if ($this->input->post()) {
            $cost_id = intval($this->input->post('cost_id'));
            if ($this->expenses_m->remove_expense($cost_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_expense_delete_success'));

            } else {

                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'Something went wrong deleting..');
            }
            redirect('expenses/?status=expenses');
        } else {

            $this->data['title'] = 'Eliminar Gasto';
            $this->data['cost_id'] = intval($this->uri->segment(3));
            $this->load->view('modal/delete', $this->data);
        }

    }

    function caja_delete()
    {

        $this->data['content'] = 'expenses/expenses';
        if ($this->input->post()) {
            $caja_id = intval($this->input->post('caja_id'));
            if ($this->expenses_m->remove_caja($caja_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_expense_delete_success'));

            } else {

                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'Something went wrong deleting..');
            }
            redirect('expenses/?status=caja');
        } else {

            $this->data['title'] = 'Eliminar Gasto';
            $this->data['caja_id'] = intval($this->uri->segment(3));
            $this->load->view('modal/delete', $this->data);
        }

    }

    function delete_transaccion_caja()
    {

        $this->data['content'] = 'expenses/expenses';
        if ($this->input->post()) {
            $id_prestamo = intval($this->input->post('id'));
            $caja_id = intval($this->input->post('caja_id'));
            if ($this->expenses_m->remove_transaccion_caja($id_prestamo)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', 'Transaccion eliminada exitosamente !');

            } else {

                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'Something went wrong deleting..');
            }
            redirect('expenses/view_prestamo/'. $caja_id);
        } else {

            $this->data['title'] = 'Eliminar Gasto';
            $this->data['cost_id'] = intval($this->uri->segment(3));
            $this->load->view('modal/delete', $this->data);
        }

    }

    /**
     * edit expenses
     */
    function editperm()
    {
      $this->data['cost_id'] = intval($this->uri->segment(3));

        $expense_id = intval($this->uri->segment(3));
        $this->data['title'] = 'Editar Costos';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['expense_details'] = $this->expenses_m->expense_details($expense_id);
        $this->data['categories'] = $this->expenses_m->categories();
        $this->load->view('modal/permission', $this->data);
    }
    function edit()
    {
$this->data['cost_id'] = intval($this->uri->segment(3));
        $expense_id = intval($this->uri->segment(3));
        $this->data['title'] = 'Editar Costos';
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['expense_details'] = $this->expenses_m->expense_details($expense_id);
        $this->data['categories'] = $this->expenses_m->categories();
        $this->load->view('modal/edit_expense', $this->data);
    }
    /**
     * edit expense and save to database
     */
    function edit_expense_costs()
    {

        if ($this->input->post()) {

            $cost_id = $this->input->post('cost_id');
            $file_name = '';

            if (file_exists($_FILES['expense_receipt']['tmp_name']) || is_uploaded_file($_FILES['expense_receipt']['tmp_name'])) {
                $file_name = $this->_upload_expense_receipt($_POST);

            }

            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $data = array(
                "description" => $this->input->post('cost_description'),
                "date_entry" => date('Y-m-d', strtotime($this->input->post('cost_date'))),
                "classification" => $this->input->post('cost_classification'),
                "estimate_cost" => $this->input->post('cost_estimate'),
                "real_cost" => $this->input->post('cost_actual')
            );

            if ($file_name != '') {
                $attach_data = array("receipt" => $file_name);
                $data = array_merge($data, $attach_data);
            }
            if ($this->expenses_m->update_cost_receipt($data, $cost_id)) {
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_cost_save_success'));

                $log_params = array(
                    'activity_user' => $user_id,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'expenses',
                    'activity_details' => 'Updated expense costs to project',
                    'activity_module_id' => $cost_id,
                    'activity_status' => "info",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => 'Actualizar costos',
                    'activity_icon' => 'fa fa-money'
                );

                $this->__activity_tracker($log_params);
                redirect('expenses/?status=expenses');
            }
        }

    }

    /**
     * save attachment to directory
     * @param null $image_file
     * @return null|string
     */
    function _upload_expense_receipt($image_file = null)
    {

        $file_name = null;
        $success = false;
        if ($image_file) {
            $config['upload_path'] = './files/receipts/';
            $config['allowed_types'] = $this->config->item('system')->allowed_files;
            $config['remove_spaces'] = true;
            $config['overwrite'] = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('expense_receipt')) {
                $data = $this->upload->data();
                $file_name = $data['file_name'];
                $success = true;
            } else {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', lang('messages_receipt_add_error'));
                redirect('expenses');
            }
        } else {
            $success = false;
        }

        return ($success) ? $file_name : '';
    }

    /**
     * log activities to the database
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->expenses_m->add_log($activity_data);

    }

    /**
     * add expense
     */
    function add_expense_costs()
    {

        if ($this->input->post()) {

            $file_name = '';

            if (file_exists($_FILES['expense_receipt']['tmp_name']) || is_uploaded_file($_FILES['expense_receipt']['tmp_name'])) {
                $file_name = $this->_upload_expense_receipt($_POST);

            }

            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $data = array(
                "budget_amt" => 0.00,
                "description" => $this->input->post('cost_description'),
                "date_entry" => date('Y-m-d', strtotime($this->input->post('cost_date'))),
                "client_id" => $this->input->post('cost_classification'),
                "estimate_cost" => $this->input->post('cost_estimate'),
                "payment_type" => 9,
                "project_id" => $this->input->post('obra'),
                "real_cost" => $this->input->post('cost_actual'),
                "cost_type" => 2,
                "receipt" => $file_name
            );


            $expense_id = $this->expenses_m->add_caja($data);
            if ($expense_id) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', 'Prestamo registrado');

                $log_params = array(
                    'activity_user' => $user_id,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'expenses',
                    'activity_details' => 'Salio Prestamo de caja chica',
                    'activity_module_id' => $expense_id,
                    'activity_status' => "info",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => 'Prestamos',
                    'activity_icon' => 'fa fa-money'
                );

                $this->__activity_tracker($log_params);
                redirect('expenses/?status=caja');
            }
        }
    }

    /**
     * update aprove
     */
    function update_prove()
    {

        if ($this->input->post()) {

            $file_name = '';

            if (file_exists($_FILES['expense_receipt']['tmp_name']) || is_uploaded_file($_FILES['expense_receipt']['tmp_name'])) {
                $file_name = $this->_upload_expense_receipt($_POST);

            }

            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $caja_id = $this->input->post('caja_id');
            $caja_proved = $this->expenses_m->get_prove_details($caja_id);

            $data = array(
                "real_cost" => $caja_proved->real_cost + $this->input->post('real_cost'),
                "change_cost" => $caja_proved->change_cost + $this->input->post('change_cost')
            );


            $expense_id = $this->expenses_m->update_prove($data, $caja_id);
            if ($expense_id) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', 'Prestamo registrado');

                $log_params = array(
                    'activity_user' => $user_id,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'expenses',
                    'activity_details' => 'Salio Prestamo de caja chica',
                    'activity_module_id' => $expense_id,
                    'activity_status' => "info",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => 'Prestamos',
                    'activity_icon' => 'fa fa-money'
                );

                $this->__activity_tracker($log_params);
                redirect('expenses/?status=caja');
            }
        }
    }

    /**
     * add expense
     */
    function add_caja()
    {

        if ($this->input->post()) {

            $file_name = '';

            if (file_exists($_FILES['expense_receipt']['tmp_name']) || is_uploaded_file($_FILES['expense_receipt']['tmp_name'])) {
                $file_name = $this->_upload_expense_receipt($_POST);

            }

            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $data = array(
                "budget_amt" => 0.00,
                "description" => $this->input->post('cost_description'),
                "date_entry" => date('Y-m-d', strtotime($this->input->post('cost_date'))),
                "client_id" => $this->input->post('cost_classification'),
                "estimate_cost" => $this->input->post('cost_estimate'),
                "payment_type" => $this->input->post('payment_type'),
                "project_id" => $this->input->post('obra'),
                "real_cost" => $this->input->post('cost_actual'),
                "cost_type" => 2,
                "receipt" => $file_name,
                "caja_concept" => null,
                "caja_comment" => null,
                "classification" => null
            );


            $expense_id = $this->expenses_m->add_caja($data);
            if ($expense_id) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_cost_save_success'));

                $log_params = array(
                    'activity_user' => $user_id,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'expenses',
                    'activity_details' => 'Agregado Concepto a vaciado',
                    'activity_module_id' => $expense_id,
                    'activity_status' => "info",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => 'Costos agregados',
                    'activity_icon' => 'fa fa-money'
                );

                $this->__activity_tracker($log_params);
                redirect('expenses/?status=expenses');
            }
        }
    }

function add_account()
    {

        if ($this->input->post()) {




            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $data = array(

                "payment_name" => $this->input->post('payment_name'),
                "payment_amount" => $this->input->post('payment_amount'),
                "payment_active" => 1,
                "company_id" => 1


            );
 }

            $account_id = $this->expenses_m->add_accounts($data);
            if ($account_id) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', 'Se ha agregado la cuenta');

                $log_params = array(
                    'activity_user' => $user_id,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'expenses',
                    'activity_details' => 'Se ha agregado una cuenta',
                    'activity_module_id' => $expense_id,
                    'activity_status' => "info",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => 'Costos agregados',
                    'activity_icon' => 'fa fa-money'
                );

                $this->__activity_tracker($log_params);
                redirect('expenses');
            }
        }

    /**
     * add expense
     */
    function add_office_cost()
    {

        if ($this->input->post()) {

            $file_name = '';

            if (file_exists($_FILES['expense_receipt']['tmp_name']) || is_uploaded_file($_FILES['expense_receipt']['tmp_name'])) {
                $file_name = $this->_upload_expense_receipt($_POST);

            }

            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $data = array(
                "concepto" => $this->input->post('office_concepto'),
                "descripcion" => $this->input->post('office_descripcion'),
                "date_entry" => date('Y-m-d', strtotime($this->input->post('office_date'))),
                "cantidad" => $this->input->post('office_estimate'),
                "payment_type" => $this->input->post('payment_type'),
                "client_id" => 0,
                "project_id" => $this->input->post('obra'),
                "file" => $file_name
            );


            $expense_id = $this->expenses_m->add_office($data);
            if ($expense_id) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_cost_save_success'));

                $log_params = array(
                    'activity_user' => $user_id,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'expenses',
                    'activity_details' => 'Agregado Concepto a vaciado',
                    'activity_module_id' => $expense_id,
                    'activity_status' => "info",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => 'Costos agregados',
                    'activity_icon' => 'fa fa-money'
                );

                $this->__activity_tracker($log_params);
                redirect('expenses/?status=finanzas');
            }
        }
    }

    /**
     * add transaction
     */
    function add_transaccion()
    {

        if ($this->input->post()) {

            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $data = array(
              "invoice_id" => 164,
              "trans_id" => "",
              "project_id" => $this->input->post('project_id'),
              "amount" => $this->input->post('amount'),
              "reference" => $this->input->post('reference'),
              "payment_method" => $this->input->post('payment_method'),
              "ip" => $this->input->ip_address(),
              "notes" => "Ajuste de Caja por" . $this->CompanyUser_FullName,
              "creator_email" => $this->CompanyUser_email,
              "created_by" => $user_id,
              "created_at" => date('Y-m-d H:i:s'),
              "updated_at" => date('Y-m-d H:i:s'),
            );


            $expense_id = $this->expenses_m->add_transaccion($data);
            if ($expense_id) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_cost_save_success'));

                $log_params = array(
                    'activity_user' => $user_id,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'expenses',
                    'activity_details' => 'Agregado Concepto a vaciado',
                    'activity_module_id' => $expense_id,
                    'activity_status' => "info",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => 'Costos agregados',
                    'activity_icon' => 'fa fa-money'
                );

                $this->__activity_tracker($log_params);
                redirect('expenses/?status=caja');
            }
        }
    }

    /**
     * add transaction_caja
     */
    function transaccion_caja()
    {

        if ($this->input->post()) {

            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $data = array(
              "caja_id" => $this->input->post('caja_id'),
              "project_id" => $this->input->post('project_id'),
              "real_cost" => $this->input->post('real_cost'),
              "change_cost" => $this->input->post('change_cost'),
              "categorie" => $this->input->post('cost_categorie'),
              "estimacion" => $this->input->post('estimate'),
              "created_at" => date('Y-m-d H:i:s'),
            );



            if ($this->expenses_m->transaccion_caja($data)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_cost_save_success'));

                $log_params = array(
                    'activity_user' => $user_id,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'expenses',
                    'activity_details' => 'Agregado Concepto a vaciado',
                    'activity_module_id' => $expense_id,
                    'activity_status' => "info",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => 'Costos agregados',
                    'activity_icon' => 'fa fa-money'
                );

                $this->__activity_tracker($log_params);
                redirect('expenses/?status=caja');
            }
        }
    }

    /**
     * save expense receipt/image
     */
    function upload_image()
    {

        if ($this->input->post()) {

            $expense_id = intval($this->input->post('cost_id'));
            $file_name = '';

            if (file_exists($_FILES['expense_receipt']['tmp_name']) || is_uploaded_file($_FILES['expense_receipt']['tmp_name'])) {
                $file_name = $this->_upload_expense_receipt($_POST);

                $data = array(
                    "receipt" => $file_name
                );
                if ($this->expenses_m->update_cost_receipt($data, $expense_id)) {
                    $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_cost_update_success'));

                    $log_params = array(
                        'activity_user' => $user_id,
                        'activity_user_type' => $this->CompanyUser_type,
                        'activity_module' => 'expenses',
                        'activity_details' => 'Updated expense costs to project',
                        'activity_module_id' => $expense_id,
                        'activity_status' => "info",
                        'activity_date' => date('Y-m-d,H:i:s'),
                        'activity_event' => 'Actualizar costos',
                        'activity_icon' => 'fa fa-money'
                    );

                    $this->__activity_tracker($log_params);
                    redirect('expenses/view/' . $expense_id);
                }
            }
        }

    }

    /**
     * download receipt/attached file to expense
     */
    function download()
    {
        $this->load->helper('download');
        $expense_id = $this->uri->segment(3);
        $file_details = $this->expenses_m->expense_details($expense_id);

        $fullpath = './files/receipts/' . $file_details->receipt;


        if ($fullpath) {
            $data = file_get_contents($fullpath);
            force_download($file_details->receipt, $data);
        } else {
            redirect(current_url());
        }
    }

    function ocultar($setting_id)
    {
          $setting_id = intval($setting_id);
          $param = array(
              "oculta" => 1,
          );


          if ($this->expenses_m->update_settings($param, $setting_id)) {

              $this->session->set_flashdata('msg_status', 'success');
              $this->session->set_flashdata('message', lang('messages_invoice_update_success'));

          redirect('expenses/?status=Cuentas');
      }
    }

    function add_gastos(){

      $this->data['clients'] = $this->expenses_m->get_proveedor_list();
      $this->data['projects'] = $this->expenses_m->get_projects();
      $this->data['milestones'] = $this->expenses_m->get_milestones();
      $this->data['invoice_number'] = $this->config->item('invoice')->gasto_prefix . "-" . trans_reference(18,
              $this->Company_id);
       $this->data['SCRIPT_PAGE'] = true;
       $this->data['show_expenses_overview'] = true;
       $this->load->view('modal/add_gastos', $this->data);
    }

    function add_gasto()
       {

           if ($this->input->post()) {

                       $monto = $this->input->post('amount');
                       $name = $this->input->post('folio');
                       $desc = $this->input->post('desc');

                       $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;

                       $start_date = ($this->input->post('start_date')) ? $this->input->post('start_date') : date('Y-m-d');
                       $end_date = ($this->input->post('end_date')) ? $this->input->post('end_date') : date('Y-m-d');

                       if ($currency_id == 'default') {
                           $client_currency = get_client_details($client_id);
                           $currency = get_currency_name($client_currency->client_currency);
                       } else {

                           $currency = get_currency_name($currency_id);
                       }

                       $param = array(
                           "inv_no" => $this->input->post('folio'),
                           "client_id" => $this->input->post('client_id'),
                           "project_id" => $this->input->post('project'),
                           "status" => 2,
                           "approved" => 1,
                           "currency" => $currency,
                           "solicitante" => " ",
                           "autorizado" => " ",
                           "notes" => " ",
                           "invoice_due" => date('Y-m-d', strtotime($end_date)),
                           "created_at" => date('Y-m-d', strtotime($start_date)),
                           "updated_at" => date('Y-m-d H:i:s'),
                           "inv_created_by" => $user_id,
                           "filter" => 0,
                           "tax" => 0,
                           "type" => 0,
                           "gasto" => 1,
                           "type_pre" => 0

                       );

                   if ($this->expenses_m->add_gasto($param, $monto, $name, $desc)) {

                       $this->session->set_flashdata('msg_status', 'success');
                       $this->session->set_flashdata('message', 'Gasto agregado con éxito !');

                   }
                   redirect('billing/billing');
           }
       }
}
