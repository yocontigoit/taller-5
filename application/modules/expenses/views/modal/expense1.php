<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<link href="http://dci-innovacion.com/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/dashboard.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/sb-admin.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/default.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">concepto</h4>
        </div>
        <?php echo form_open_multipart('expenses/add_caja'); ?>
        <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <label for="taskTitle"><?php echo lang('label_description'); ?></label>
                      <input class="form-control" type="text" name="cost_description" required>

                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                      <label for="taskPercent"><?php echo lang('label_date'); ?></label>
                      <input type="text" class="form-control datepicker" name="cost_date"
                             data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                             value="<?php echo date('d-m-Y') ?>">

                  </div>
              </div>


            </div>
            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <label for="taskPercent"><?php echo lang('label_estimate_cost'); ?></label>
                      <input type="text" class="form-control" name="cost_estimate">

                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                      <label for="taskPercent">Forma de Pago</label>
                      <select class="form-control" name="payment_type">
                        <option value="Efectivo">Efectivo</option>
                        <option value="Transferencia">Transferencia</option>
                      </select>

                  </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent">Proveedor</label>

                        <select class="form-control" name="cost_classification">


                                <?php foreach ($categories as $category): ?>
                                    <option
                                        value="<?php echo $category->client_id; ?>"><?php echo $category->client_name; ?></option>
                                <?php endforeach; ?>

                        </select>

                    </div>
                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="taskPercent">Obra</label>

                                        <select class="form-control" name="obra">


                                                <?php foreach ($obra as $category): ?>
                                                    <option
                                                        value="<?php echo $category->project_id; ?>"><?php echo $category->project_title; ?></option>
                                                <?php endforeach; ?>

                                        </select>

                                    </div>
                                </div>
            </div>
            <div class="row">

  <input type="hidden" class="form-control" name="cost_actual" value="0">


            </div>
            <div class="row">
                <div class="col-md-12">

                    <div class="input-group">
                        <input type="text" class="form-control" readonly>
                                                            <span class="input-group-btn">
                                                                <span class="btn btn-primary btn-file">
                                                                    Browse files<input type="file" multiple
                                                                                       name="expense_receipt">
                                                                </span>
                                                            </span>
                    </div>
                    <!-- /.input-group -->


                </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    <?php echo lang('form_button_save'); ?>
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </button>
            </div>
            </form>
        </div>
    </div>
    <link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet"
          type="text/css"/>
    <script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>
    <script type="text/javascript">


        $("body").delegate(".datepicker", "focusin", function () {
            $(this).datepicker(
                {autoclose: true}
            ).on('changeDate', function (e) {
                    $(this).datepicker('hide');
                });
        });
        /** BEGIN INPUT FILE **/
        if ($('.btn-file').length > 0) {
            $(document)
                .on('change', '.btn-file :file', function () {
                    "use strict";
                    var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [numFiles, label]);
                });
            $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

                if (input.length) {
                    input.val(log);
                } else {
                    // if( log ) alert(log);
                }
            });
        }
    </script>
