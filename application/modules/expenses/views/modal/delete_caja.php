<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/04/12
 * Time: 11:08 AM
 */
?>
<link href="http://dci-innovacion.com/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/dashboard.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/sb-admin.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/default.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header bg-danger">
            <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                    <i class="fa fa-times"></i>
                    </span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_delete'); ?></h4>
        </div>
        <form method="post" action="<?php echo base_url('expenses/caja_delete'); ?>">
            <div class="modal-body">
              <br>
                  <center><p>Necesitas Contraseña del administrador para Eliminar</p>

              <input type="hidden" name="caja_id" value="<?php echo $caja_id; ?>">
                <input type="password" name="password" value="" pattern="[0-5]{5,5}" title="Debe ser del 1 al 5"></center>
<br><br>
                <div class="modal-footer">
                    <button class="btn btn-success btn-icon" value="addNewTask" name="submit" type="input">
                        <i class="fa fa-check-square-o"></i>
                        <?php echo lang('form_button_continue'); ?>
                    </button>
                    <button class="btn btn-default btn-icon" data-dismiss="modal" type="button" onclick="volver()">
                        <i class="fa fa-times-circle-o"></i>
                        <?php echo lang('form_button_cancel'); ?>
                    </button>
                </div>


            </div>
        </form>
    </div>


</div>
<script type="text/javascript">
    function volver() {
        history.back();
    }
</script>
