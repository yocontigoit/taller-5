<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<link href="http://dci-innovacion.com/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/dashboard.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/sb-admin.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/default.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />

<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">$<?php $client = get_client_details($cajas->client_id); $project = get_project_details($cajas->project_id);

            echo format_currency($cajas->estimate_cost)."   ".$client->client_name." (".$project->project_title.")";  ?></h4>
        </div>
        <?php echo form_open_multipart('expenses/transaccion_caja'); ?>
        <input type="hidden" name="project_id" value="<?php echo $project_id; ?>">
        <div class="modal-body">
            <div class="row">

              <div class="col-sm-6">
                  <div class="form-group">
                      <label for="taskPercent"><?php echo lang('label_date'); ?></label>
                      <input type="text" class="form-control datepicker" name="cost_date"
                             data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                             value="<?php echo date('d-m-Y') ?>">

                  </div>
              </div>

              <div class="col-sm-6">
                  <div class="form-group">
                      <label for="taskPercent">Comprobado</label>
                      <?php
                          $transferencia = 0.00;
                          $por_comprobar = 0.00;
                          $cambio = 0.00;
                          if(count($transactions)): foreach($transactions as $transaction): if($transaction->caja_id == $caja_id && $transaction->project_id == $project_id):
                            $transferencia += $transaction->real_cost;
                            $cambio += $transaction->change_cost;
                          endif; endforeach; endif;

                          $subtotal = $cajas->estimate_cost - $transferencia;
                          $por_comprobar = $subtotal - $cambio;
                       ?>

                      <input type="text" class="form-control" name="real_cost" value="<?php echo $por_comprobar; ?>">
                      <input type="hidden" class="form-control" name="caja_id" value="<?php echo $caja_id; ?>">

                  </div>
              </div>
              </div>
              <div class="row">
               <div class="col-sm-6">
                  <div class="form-group">
                      <label for="taskPercent">Cambio</label>
                      <input type="text" class="form-control" name="change_cost" value="<?php echo $cajas->change_cost; ?>">

                  </div>
              </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="taskPercent">Avance</label>

                                        <select class="form-control chosen-select" name="avance">
                                          <option selected="true" disabled="">Selecciona ...</option>
                                          <option value="3%">- 3% -</option>
                                                <?php
                                                foreach ($imports as $import): if ($import->codigo_proyecto == $project->project_code){?>

                                                    <option
                                                        value="<?php echo $import->id; ?>"><?php echo $import->concepto; ?></option>
                                                <?php } endforeach; ?>



                                        </select>

                                    </div>
                                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent">Categoria</label>

                        <select class="form-control chosen-select" name="cost_categorie">



                                    <option value="Mano de Obra">Mano de Obra</option>
                                    <option value="Material">Material</option>
                                    <option value="Proveedor">Proveedor</option>
                                    <option value="Contratista">Contratista</option>


                        </select>
                        <input type="hidden" class="form-control" name="cost_actual" value="0">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent">Estimación</label>
                        <select class="form-control" name="estimate">
                            <optgroup label="Estimaciones">
                                <?php $count = 1; foreach ($estimaciones as $estimate): if ($estimate->project_id == $project_id): ?>
                                    <option
                                        value="<?php echo $estimate->id; ?>">Estimaciones: <?php echo $count++ ?></option>
                                <?php endif; endforeach; ?>
                            </optgroup>
                        </select>

                    </div>
                </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <label for="taskPercent">Recibo</label>
                      <div class="input-group">
                          <input type="text" class="form-control" readonly>
                                                              <span class="input-group-btn">
                                                                  <span class="btn btn-primary btn-file">
                                                                      Buscar<input type="file" multiple
                                                                                         name="expense_receipt">
                                                                  </span>
                                                              </span>
                      </div>
                      <!-- /.input-group -->
                  </div>
            </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" name="submit" type="input" >
                    <i class="fa fa-check-square-o"></i>
                    <?php echo lang('form_button_save'); ?>
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button" onclick="volver()">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </button>
            </div>
            </form>

    </div>
    </div>
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/js/bootstrap-dialog.min.js"></script>
    <link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet"
          type="text/css"/>
    <script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>
    <script type="text/javascript">


        $("body").delegate(".datepicker", "focusin", function () {
            $(this).datepicker(
                {autoclose: true}
            ).on('changeDate', function (e) {
                    $(this).datepicker('hide');
                });
        });
        /** BEGIN INPUT FILE **/
        if ($('.btn-file').length > 0) {
            $(document)
                .on('change', '.btn-file :file', function () {
                    "use strict";
                    var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [numFiles, label]);
                });
            $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

                if (input.length) {
                    input.val(log);
                } else {
                    // if( log ) alert(log);
                }
            });
        }


        function volver() {
            history.back();
        }
    </script>
