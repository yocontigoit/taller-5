<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<link href="http://dci-innovacion.com/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/dashboard.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/sb-admin.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/default.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />

<div class="modal-dialog">
    <div class="modal-content modal-md">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
                <span aria-hidden="true">
                <i class="fa fa-times"></i>
                </span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Caja Chica de <?php echo $obra->project_title ?></h4>
        </div>
        <?php echo form_open_multipart('expenses/add_transaccion'); ?>
        <input type="hidden" name="project_id" value="<?php echo $project_id ?>">
        <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label for="taskTitle">Reponer</label>
                      <?php $total =0;
                      $transac =0;
                      $real = 0.00;
             if (count($transactions)): foreach ($transactions as $transaction): if ($transaction->project_id == $project_id):
                 $real += $transaction->real_cost;
                 endif; endforeach; endif;

             if (count($realtransactions)): foreach ($realtransactions as $transaction): if ($transaction->project_id == $project_id && $transaction->invoice_id ==164):
                 $transac += $transaction->amount;
                 endif; endforeach; endif;

                 $total = $real - $transac;

                 ?>
             <input class="form-control" type="text" name="amount" value="<?php echo $total ?>" required>

                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <label for="taskPercent">Forma de Pago</label>
                      <select class="form-control" name="payment_method" value="1">
                        <option value="1">Cuenta Corriente</option>
                        <option value="2">Transferencia Bancomer</option>
                        <option value="3">Cheque Bancomer</option>
                      </select>

                  </div>
              </div>


              <div class="col-md-6">
                  <div class="form-group">
                      <label for="taskPercent">Cheqe/Referencia</label>
                      <input type="text" name="reference" class="form-control" value="Transferencia">

                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label for="taskPercent"><?php echo lang('label_date'); ?></label>
                      <input type="text" class="form-control datepicker" name="cost_date"
                             data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                             value="<?php echo date('d-m-Y') ?>">

                  </div>
              </div>
            </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    <?php echo lang('form_button_save'); ?>
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button" onclick="volver()">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </button>
            </div>
            </form>

    </div>
</div>
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/js/bootstrap-dialog.min.js"></script>
    <link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet"
          type="text/css"/>
    <script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>
    <script type="text/javascript">


        $("body").delegate(".datepicker", "focusin", function () {
            $(this).datepicker(
                {autoclose: true}
            ).on('changeDate', function (e) {
                    $(this).datepicker('hide');
                });
        });
        /** BEGIN INPUT FILE **/
        if ($('.btn-file').length > 0) {
            $(document)
                .on('change', '.btn-file :file', function () {
                    "use strict";
                    var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [numFiles, label]);
                });
            $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

                if (input.length) {
                    input.val(log);
                } else {
                    // if( log ) alert(log);
                }
            });
        }

        function volver(){
          history.back();
        }
    </script>
