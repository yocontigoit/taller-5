<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<link href="http://dci-innovacion.com/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/dashboard.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/sb-admin.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/default.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />

<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Prestamo</h4>
        </div>
        <?php echo form_open_multipart('expenses/add_expense_costs'); ?>
        <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <label for="taskTitle"><?php echo lang('label_description'); ?></label>
                      <input class="form-control" type="text" name="cost_description" required>

                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                      <label for="taskPercent"><?php echo lang('label_date'); ?></label>
                      <input type="text" class="form-control datepicker" name="cost_date"
                             data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                             value="<?php echo date('d-m-Y') ?>">

                  </div>
              </div>


            </div>
            <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                      <label for="taskPercent">Importe</label>
                      <input type="text" class="form-control" name="cost_estimate">

                  </div>
              </div>
           <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent">Tercero...</label>

                        <select class="form-control" name="cost_classification">


                                <?php foreach ($categories as $category): ?>
                                    <option
                                        value="<?php echo $category->client_id; ?>"><?php echo $category->client_name; ?></option>
                                <?php endforeach; ?>

                        </select>

                    </div>
                </div>
                   <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="taskPercent">Obra</label>

                                        <select class="form-control" name="obra">


                                                <?php foreach ($obra as $category): ?>
                                                    <option
                                                        value="<?php echo $category->project_id; ?>"><?php echo $category->project_title; ?></option>
                                                <?php endforeach; ?>

                                                <option
                                                        value="28">Oficina</option>

                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="taskPercent">Archivo</label>
                                    <div class="form-group">
                                <div class="input-group">
                        <input type="text" class="form-control" readonly>
                                                            <span class="input-group-btn">
                                                                <span class="btn btn-primary btn-file">
                                                                    Buscar<input type="file" multiple
                                                                                       name="expense_receipt">
                                                                </span>
                                                            </span>
                    </div>
                    </div>
            </div>
            </div>

            <div class="row">

  <input type="hidden" class="form-control" name="cost_actual" value="0">


            </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    <?php echo lang('form_button_save'); ?>
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button" onclick="volver()">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </button>
            </div>
            </form>

    </div>
    </div>
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/js/bootstrap-dialog.min.js"></script>
    <link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet"
          type="text/css"/>
    <script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>
    <script type="text/javascript">


        $("body").delegate(".datepicker", "focusin", function () {
            $(this).datepicker(
                {autoclose: true}
            ).on('changeDate', function (e) {
                    $(this).datepicker('hide');
                });
        });
        /** BEGIN INPUT FILE **/
        if ($('.btn-file').length > 0) {
            $(document)
                .on('change', '.btn-file :file', function () {
                    "use strict";
                    var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [numFiles, label]);
                });
            $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

                if (input.length) {
                    input.val(log);
                } else {
                    // if( log ) alert(log);
                }
            });
        }

        function volver() {
            history.back();
        }

    </script>
