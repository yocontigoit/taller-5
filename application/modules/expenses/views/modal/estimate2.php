<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/04/12
 * Time: 11:08 AM
 */
?>

<link href="http://dci-innovacion.com/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/dashboard.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/sb-admin.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/default.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                        class="fa fa-times"></i></span><span
                    class="sr-only"><?php echo lang('form_button_cancel'); ?></span></button>
            <h4 class="modal-title">Agregar Metodo de Pago</h4>
        </div>
        
           <?php echo form_open('expenses/add_account', array('id' => 'add-account')); ?>
        <div class="modal-body">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Nombre del Método de pago</label>
                        <input type="text" class="form-control " name="payment_name" placeholder="nombre">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Cuenta relacionada</label>
                        <select class="form-control"><option>Cuenta Corriente</option>
                        <option>Bancomer</option>
                        </select>
                    </div>
                </div>

            </div>
        
                
                </div>
       <div class="modal-footer">
                    <button class="btn btn-success btn-icon">
                        <i class="fa fa-check-square-o"></i>
                        <?php echo lang('form_button_continue'); ?>
                    </button>
                    <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                        <i class="fa fa-times-circle-o"></i>
                        <?php echo lang('form_button_cancel'); ?>
                    </button>
                </div>


            </div>
        </form>
    </div>


</div>
