<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/25
 * Time: 02:13 PM
 */
?>
<div class="the-box">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-2">
                <ul id="cssmenu" class="ver-inline-menu" style="width:170px;">
                    
                     <li class="<?php echo ($active_page == 'overview') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('expenses/?status=overview'); ?>"><i
                                class="fa fa-bar-chart-o"></i>Reportes</a></li>
                                
                                <li class="<?php echo ($active_page == 'estimaciones') ? "active" : ""; ?>"><a
                                      href="<?php echo base_url('expenses/?status=estimaciones'); ?>"><i
                                          class="fa fa-money"></i>Gastos</a></li>
                    
                    <li class="<?php echo ($active_page == 'caja') ? "active" : ""; ?>"><a
                                      href="<?php echo base_url('expenses/?status=caja'); ?>"><i
                                          class="fa fa-money"></i>Caja chica</a></li>
                    <li class="<?php echo ($active_page == 'Cuentas') ? "active" : ""; ?>"><a
                                      href="<?php echo base_url('expenses/?status=Cuentas'); ?>"><i
                                          class="fa fa-money"></i>Cuentas</a></li>
                  
                   <li class="<?php echo ($active_page == 'programs') ? "active" : ""; ?>"><a
                          href="<?php echo base_url('expenses/?status=programs'); ?>"><i
                              class="fa fa-money"></i>Programaciones</a></li>                       
                                        
                                          
                  <li class="<?php echo ($active_page == 'expenses') ? "active" : ""; ?>"><a
                          href="<?php echo base_url('expenses/?status=expenses'); ?>"><i
                              class="fa fa-money"></i>Vaciado</a></li>


                   



                </ul>
            </div>
            <div class="col-md-10">
                <fieldset>

                    <legend><i class="fa fa-bar-chart-o"></i><?php echo $page_title; ?>

                    </legend>

                    <?php echo ($_PAGE) ? $_PAGE : null; ?>

                </fieldset>

            </div>
        </div>
        <!-- /.row -->
    </div>
</div>
