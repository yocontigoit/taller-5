<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/05/25
 * Time: 02:42 PM
 */
?>

<a data-toggle="zestModal" href="<?php echo base_url('expenses/new_account/'); ?>" class="btn btn-info btn-lg btn-icon mt-10"><i
            class="fa fa-plus"></i>Crear Cuenta</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a data-toggle="zestModal" href="<?php echo base_url('expenses/new_payment_method/'); ?>" class="btn btn-info btn-lg btn-icon mt-10"><i
            class="fa fa-plus"></i>Crear Método de pago</a>
            <br><br>
<div class="table-responsive">
    <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table">
        <thead>
        <tr>
            <th>#</th>
            <th>Nombre de Cuenta</th>
            <th>Metdo de Pago</th>
            <th>Saldo</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($methods)): foreach ($methods as $expense):?>
          <?php if ($expense->oculta == 1 || $expense->setting_id == 9 ){ ?>

          <?php }else{ ?>
            <tr>
              <td>
                  <?php echo $expense->payment_code; ?>
              </td>
                <td><a href="<?php echo base_url('expenses/view_statement/' . $expense->setting_id); ?>" title="">
                  <?php echo $expense->payment_name; ?></a>
                </td>
                <td>
                  <?php echo $expense->payment_metod ?>
                </td>
                <td>
                  $<?php echo format_currency($expense->payment_amount); ?>
                </td>
                <td>
                    <div class="btn-group-action">
                        <div class="btn-group">

                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret">&nbsp;</span>
                            </button>
                            <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                                <ul class="dropdown-menu pull-right">
                                    <li><a class="text-danger" data-toggle="zestModal"
                                           href="<?php echo base_url('expenses/edit_statement/' . $expense->setting_id); ?>"><i
                                                class="fa fa-pencil-square-o"></i> Editar Nombre</a>
                                    </li>
                                    <li><a class="text-danger" data-toggle="zestModal"
                                           href="<?php echo base_url('expenses/balance_statement/' . $expense->setting_id); ?>"><i
                                                class="fa fa-thumbs-up"></i> Ajuste de Cuenta</a>
                                    </li>
                                    <li><a data-toggle="zestModal"
                                           href="<?php echo base_url('expenses/delete_statement/' . $expense->setting_id); ?>"
                                           title=""><i class="fa fa-arrows-h"></i> Transferencia</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('expenses/ocultar/' . $expense->setting_id); ?>">
                                          <i class="fa fa-eye-slash"></i> Ocultar
                                        </a>

                                    </li>
                                </ul>
                            <?php } ?>

                        </div>
                    </div>
                </td>
            </tr>
            <?php } ?>
        <?php  endforeach; endif; ?>

        </tbody>

    </table>


</div>
