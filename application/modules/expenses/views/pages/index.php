<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/05/25
 * Time: 02:42 PM
 */
?>
<div class="row">
  
</div>
<br>
<div class="table-responsive">
    <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table">
        <thead>
        <tr>
            <th>Fecha</th><th>Obra</th><th>Tipo</th><th>Descripcion</th><th>Monto</th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($expenses)): foreach ($expenses as $expense): ?>
            <tr>
                <td style="text-align:center;"><?php echo $expense->date_entry; ?></td>
                <td style="text-align:center;">Penthouse Baeza</td>
              <td style="text-align:center;">
                  Gasto</td><td style="text-align:center;">  <strong><?php echo $expense->description;  ?></strong></td>
                  <td style="text-align:center;">$
                 <?php echo format_currency($expense->estimate_cost);  ?></td>
            </tr>
        <?php endforeach; endif; ?>
        <?php if (count($expenses)): foreach ($expenses as $expense): ?>
            <tr>
                <td style="text-align:center;"><?php echo $expense->date_entry; ?></td>
                <td style="text-align:center;"><strong> Casa Zaragoza</strong></td>
              <td style="text-align:center;">
                 Estimacion </td><td style="text-align:center;"> <?php echo $expense->cost_id; ?></td><td style="text-align:center;">$ <?php echo format_currency($tot = $expense->estimate_cost*125); ?> </td>
            </tr>
        <?php endforeach; endif; ?>
        <?php if (count($expenses)): foreach ($expenses as $expense): ?>
            <tr>
                <td style="text-align:center;"><?php echo $expense->date_entry; ?></td>
                <td style="text-align:center;"><strong> Casa Zaragoza</strong></td>
              <td style="text-align:center;">
                   Orden de compra</td><td style="text-align:center;">  ORD-000<?php echo $expense->classification; ?></td><td style="text-align:center;">$ <?php echo format_currency($tot = $expense->estimate_cost*25); ?> </td>
            </tr>
        <?php endforeach; endif; ?>
         <?php if (count($expenses)): foreach ($expenses as $expense): ?>
            <tr>
                <td style="text-align:center;"><?php echo $expense->date_entry; ?></td>
                <td style="text-align:center;"><strong> Casa Zaragoza</strong></td>
              <td style="text-align:center;">
                   Pago de Factura</td><td style="text-align:center;">  <?php echo $expense->cost_id; ?></td><td style="text-align:center;">$ <?php echo format_currency($tot = $expense->estimate_cost*15); ?> </td>
            </tr>
        <?php endforeach; endif; ?>
         <?php if (count($expenses)): foreach ($expenses as $expense): ?>
            <tr>
                <td style="text-align:center;"><?php echo $expense->date_entry; ?></td>
                <td style="text-align:center;"><strong> Casa Zaragoza</strong></td>
              <td style="text-align:center;">
                   Pago de Cliente</td><td style="text-align:center;">  <?php echo $expense->cost_id; ?></td><td style="text-align:center;">$ <?php echo format_currency($tot = $expense->estimate_cost*30); ?> </td>
            </tr>
        <?php endforeach; endif; ?>
        </tbody>

    </table>
</div>
