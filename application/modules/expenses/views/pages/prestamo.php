<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/05/25
 * Time: 02:42 PM
 */
?>
<div class="row">
  <div class="col-md-12 ">
      <center>
      <table class="table">
          <tr>
            <td>Decripción</td>
            <td>Tercero</td>
            <td>Obra</td>
            <td>Cantidad</td>
            <td>Comprobado</td>
            <td>Cambio</td>
            <td>Saldo</td>
          </tr>
          <?php if(count($expenses)): foreach($expenses as $expense): if($expense->caja_id == $caja_id): ?>
          <tr>
            <th>
                <?php echo $expense->description; ?>
            </th>
            <td>
              <?php
                $customer_details = get_client_details($expense->client_id);
                echo $customer_details->client_name;
              ?>
            </td>
            <td>
              <?php
                $project_info = get_project_details($expense->project_id);
                echo $project_info->project_title;
              ?>
            </td>
            <td>
              $<?php echo format_currency($expense->estimate_cost); ?>
            </td>
            <td class="">
              <?php
                $comprobado2 = 0.00;
                if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $caja_id):
                  $comprobado2 += $transaction->real_cost;
                endif; endforeach; endif;
              ?>
              $<?php echo format_currency($comprobado2); ?>
            </td>
            <td>
              <?php
                $cambio2 = 0.00;
                if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                  $cambio2 += $transaction->change_cost;
                endif; endforeach; endif;
              ?>
              $<?php echo format_currency($cambio2); ?>
            </td>
            <td>
              <?php
                $ticket = 0.00;
                $change = 0.00;
                $sub = 0.00;
                $saldazo = 0.00;
                if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                  $ticket += $transaction->real_cost;
                  $change += $transaction->change_cost;
                endif; endforeach; endif;
                $sub = $ticket + $change;
                $saldazo = $expense->estimate_cost - $sub;
              ?>
              $<?php echo format_currency($saldazo); ?>
            </td>
          </tr>
        <?php endif; endforeach; endif; ?>
      </table>
    </center>
  </div>
</div>
<br>
<div class="row">
  <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
             id="zest_table2">
          <thead class="the-box dark full">
          <tr>

              <th>#</th>
              <th>Descripción</th>
              <th>Comprobado</th>
              <th>Cambio</th>
              <th>Aciones</th>
          </tr>
          </thead>
          <tbody>
              <?php $count=1; if (count($realtransactions)): foreach($realtransactions as $transaction): if ($transaction->caja_id == $caja_id): ?>
                  <tr>
                      <td><?php echo $count++ ?></td>
                       <td><?php echo $transaction->categorie; ?></td>

                      <td><?php echo $transaction->real_cost; ?></td>
                      <td><?php echo $transaction->change_cost; ?></td>
                      <td>
                        <form method="post" action="<?php echo base_url('expenses/delete_transaccion_caja'); ?>">
                          <input type="hidden" name="id" value="<?php echo $transaction->id; ?>">
                          <input type="hidden" name="caja_id" value="<?php echo $caja_id; ?>">
                          <button class="btn btn-success btn-icon" value="addNewTask" name="submit" type="input">
                              <i class="fa fa fa-trash"></i>
                          </button>
                        </form>
                      </td>
                  </tr>

              <?php endif; endforeach; endif;?>
          </tbody>


      </table>
  </div>
</div>
