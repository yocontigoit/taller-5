<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/05/25
 * Time: 02:42 PM
 */
?>

<div class="row">
  <div class="col-sm-2">
    <center>
      <a  class="btn btn-info btn-sm"
      href="<?php echo base_url('expenses/add'); ?>" data-toggle="zestModal">Prestamo<i class="fa fa-plus"></i></a>
    </center>
  </div>
   <div class="col-sm-8">
       <center>
       <table class="table" width="100%">
           <tr>
             <td>Obra</td>
             <td>Monto</td>
             <td>Efectivo</td>
             <td style="width:105px;">Por Comprobar</td>
             <td style="width:88px;">Por Reponer</td>
             <td></td>
           </tr>
           <tr>
          <?php if(count($obras)): foreach($obras as $obra):?>
              <th>
                <?php echo $obra->project_title ?>
              </th>
              <td>$3,000.00</td>
              <td>
                <?php
                     $monto = 3000;
                     $cantidad = 0.00;
                     $cambio = 0.00;
                     $trans_repuestas = 0;
                     $transac = 0.00;
                     $cantidad = 0.00;
                     if (count($expenses)): foreach ($expenses as $expense): if ($expense->project_id == $obra->project_id):
                       $cantidad += $expense->estimate_cost;
                         if(count($realtransactions)): foreach ($realtransactions as $trans): if ($trans->caja_id == $expense->caja_id):
                           $trans_repuestas += $trans->real_cost;
                            $cambio += $trans->change_cost;
                         endif; endforeach; endif;
                     endif; endforeach; endif;

                     if (count($transactions)): foreach ($transactions as $transaction): if ($transaction->project_id == $obra->project_id && $transaction->invoice_id ==164):
                          $transac += $transaction->amount;
                       endif; endforeach; endif;

                 $subtotal = $monto - $cantidad;
                 $total = $subtotal + $cambio + $transac;
                 ?>
                 $<?php echo format_currency($total); ?>
              </td>
              <td>
                <?php
                  $comprobado2 = 0.00;
                  $cambio2 = 0.00;
                  $com = 0.00;
                  $com2 = 0.00;
                  $cantidad2 = 0.00;
                  if (count($expenses)): foreach ($expenses as $expense): if ($expense->project_id == $obra->project_id):
                   $cantidad2 += $expense->estimate_cost;
                      if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                        $comprobado2 += $transaction->real_cost;
                        $cambio2 += $transaction->change_cost;
                      endif; endforeach; endif;
                  endif; endforeach; endif;

                 $com = $cambio2 + $comprobado2;
                 $com2 = $cantidad2 - $com;
                ?>

                $<?php echo format_currency($com2); ?>
              </td>
              <td>
                <?php
                   $costo_estimado = 0.00;
                   $costo_cambio = 0.00;
                   $costo_real = 0.00;
                   $por_reponer = 0.00;
                   $trans_repuestas = 0.00;
                   $change = 0.00;
                   $repuesto =0.00;
                     if (count($expenses)): foreach ($expenses as $expense): if ($expense->project_id == $obra->project_id):
                       $costo_estimado = $expense->estimate_cost;
                         if(count($realtransactions)): foreach ($realtransactions as $trans): if ($trans->caja_id == $expense->caja_id):
                         $trans_repuestas += $trans->real_cost;
                         $change += $trans->change_cost;
                         endif; endforeach; endif;
                     endif; endforeach; endif;


                   if(count($transactions)): foreach ($transactions as $trans): if ($trans->project_id == $obra->project_id && $trans->invoice_id == 164):
                       $repuesto += $trans->amount;
                      endif; endforeach; endif;

                      $por_reponer = $trans_repuestas - $repuesto;

                 ?>
                 $<?php echo format_currency($por_reponer); ?>
              </td>
              <td>
                <div class="col-sm-2"><center>
                  <a data-toggle="zestModal" class="btn btn-info btn-sm"
                  href="<?php echo base_url('expenses/replace/'. $obra->project_id); ?>">Reponer<i class="fa fa-plus"></i></a></center>
                </div>
              </td>
            </tr>
          <tr>
          <?php  endforeach; endif;  ?>
           <tr>
           <th>Total:</th><td>$6,000.00</td>
           <td>
             <?php $total_saldo = 0; $total_saldo += $total; ?>
             $<?php echo format_currency($total_saldo); ?>
           </td>
           <td>
             <?php $por_comprobar = 0; $por_comprobar += $com2; ?>
             $<?php echo format_currency($por_comprobar); ?>
           </td>
           <td>
             <?php $total_reponer = 0; $total_reponer += $por_reponer; ?>
             $<?php echo format_currency($total_reponer); ?>
           <td>
           </td>
         </tr>
         </tr>
       </table>
     </center>
  </div>
</div>
<br>
<div class="table-responsive">
    <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table">
        <thead>
        <tr>
            <th style="width: 64px;"><?php echo lang('label_date'); ?></th>
            <th>Descripcion</th>
            <th>Tercero</th>
            <th>Obra</th>
            <th>Cantidad</th>
            <th>Comprobado</th>
            <th>Cambio</th>
            <th style="width: 45px;">Saldo</th>
            <th>Opciones</th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($expenses)): foreach ($expenses as $expense):


                    $comprobado2 = 0.00;
                    $ticket = 0.00;
                    $change = 0.00;
                    $sub = 0.00;
                    $saldazo = 0.00;
                    $cambio2 = 0.00;
                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $comprobado2 += $transaction->real_cost;
                    endif; endforeach; endif;


                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $cambio2 += $transaction->change_cost;
                    endif; endforeach; endif;


                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $ticket += $transaction->real_cost;
                      $change += $transaction->change_cost;
                      $sub = $ticket + $change;
                    endif; endforeach; endif;
                 $saldazo = $expense->estimate_cost - $sub;

         if ($saldazo !=0):

        ?>
            <tr>
              <td class="">
                  <?php echo $expense->date_entry; ?>
              </td>
                <td>
                  <?php echo $expense->description; ?>
                </td>
                <td class="">
                  <?php  $customer_details = get_client_details($expense->client_id);
                  echo $customer_details->client_name; ?>
                </td>
                  <td>
                  <?php
                  $project_info = get_project_details($expense->project_id);

                     echo $project_info->project_title; ?>
                </td>
                <td class="">$
                    <?php echo format_currency($expense->estimate_cost); ?>

                </td>
                <td class="">
                  <?php
                    $comprobado2 = 0.00;
                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $comprobado2 += $transaction->real_cost;
                    endif; endforeach; endif;
                  ?>
                  $<?php echo format_currency($comprobado2); ?>
                </td>
                <td class="">
                  <?php
                    $cambio2 = 0.00;
                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $cambio2 += $transaction->change_cost;
                    endif; endforeach; endif;
                  ?>
                  $<?php echo format_currency($cambio2); ?>
                </td>
                <td class="">
                  <?php
                    $ticket = 0.00;
                    $change = 0.00;
                    $sub = 0.00;
                    $saldazo = 0.00;
                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $ticket += $transaction->real_cost;
                      $change += $transaction->change_cost;
                      $sub = $ticket + $change;
                    endif; endforeach; endif;
                    $saldazo = $expense->estimate_cost - $sub;
                  ?>
                  $<?php echo format_currency($saldazo); ?>
                </td>



                <td>
                    <div class="btn-group-action">
                        <div class="btn-group">
                            <a class=" btn btn-info"
                               href="<?php echo base_url('expenses/view_prestamo/' . $expense->caja_id); ?>" title=""><i
                                    class="fa fa-eye"></i>ver</a>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret">&nbsp;</span>
                            </button>
                            <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                                <ul class="dropdown-menu pull-right">
                                    <li><a class="text-danger" data-toggle="zestModal" class="btn"
                                           href="<?php echo base_url('expenses/prove/' . $expense->caja_id . '/' . $expense->project_id); ?>"><i
                                                class="fa fa-file"></i>Comprobar</a>
                                    </li>
                                    <li><a data-toggle="zestModal"
                                           href="<?php echo base_url('expenses/delete_caja/' . $expense->caja_id); ?>"
                                           title=""><i class="fa fa-trash"></i> <?php echo lang('label_delete'); ?></a>
                                    </li>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                </td>
            </tr>
<?php endif; endforeach; endif; ?>
        </tbody>
    </table>


        <div class="row">
        <div class="col-sm-12">
    <div class="table-responsive">
    <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table">
        <thead>
        <tr>
            <th>Tercero</th>
            <th>Cantidad</th>
            <th>Comprobado</th>
            <th>Cambio</th>
            <th style="width: 45px;">Saldo</th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($expenses)): foreach ($expenses as $expense):


                    $comprobado2 = 0.00;
                    $ticket = 0.00;
                    $change = 0.00;
                    $sub = 0.00;
                    $saldazo = 0.00;
                    $cambio2 = 0.00;
                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $comprobado2 += $transaction->real_cost;
                    endif; endforeach; endif;


                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $cambio2 += $transaction->change_cost;
                    endif; endforeach; endif;


                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $ticket += $transaction->real_cost;
                      $change += $transaction->change_cost;
                      $sub = $ticket + $change;
                    endif; endforeach; endif;
                     $saldazo = $expense->estimate_cost - $sub;

         if ($saldazo !=0):

        ?>

            <tr>

                <td class="">
                  <?php  $customer_details = get_client_details($expense->client_id);
                  echo $customer_details->client_name; ?>
                </td>
                <td class="">$
                    <?php echo format_currency($expense->estimate_cost); ?>

                </td>
                <td class="">
                  <?php
                    $comprobado2 = 0.00;
                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $comprobado2 += $transaction->real_cost;
                    endif; endforeach; endif;
                  ?>
                  $<?php echo format_currency($comprobado2); ?>
                </td>
                <td class="">
                  <?php
                    $cambio2 = 0.00;
                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $cambio2 += $transaction->change_cost;
                    endif; endforeach; endif;
                  ?>
                  $<?php echo format_currency($cambio2); ?>
                </td>
                <td class="">
                  <?php
                    $ticket = 0.00;
                    $change = 0.00;
                    $sub = 0.00;
                    $saldazo = 0.00;
                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $ticket += $transaction->real_cost;
                      $change += $transaction->change_cost;
                      $sub = $ticket + $change;
                    endif; endforeach; endif;
                    $saldazo = $expense->estimate_cost - $sub;
                  ?>
                  $<?php echo format_currency($saldazo); ?>
                </td>

            </tr>
<?php endif; endforeach; endif; ?>
        </tbody>
    </table>
<a class="btn btn-info" href="<?php echo base_url('expenses/caja_statement/')?>" title="">
                  ver historial</a>
       </div>

    </div>

</div>

<script src="https://proyectosinternos.com/Taller5/assets/libs/jquery-2.0.2.min.js" ></script>
<script src="https://proyectosinternos.com/Taller5/assets/libs/jquery-ui-1.10.3.min.js"></script>
<script src="https://proyectosinternos.com/Taller5/assets/js/jquery-migrate.js"></script>
<link href="https://proyectosinternos.com/Taller5/assets/plugins/datatable/css/bootstrap.datatable.min.css" rel="stylesheet" type="text/css" />
<script src="https://proyectosinternos.com/Taller5/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
<link href="https://proyectosinternos.com/Taller5/assets/plugins/morris-chart/morris.min.css" rel="stylesheet" type="text/css" />
<script src="https://proyectosinternos.com/Taller5/assets/plugins/morris-chart/morris.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {

        var oTable = $('#zest_table').dataTable({
            "aoColumnDefs": [
                {"aTargets": [1, 1]}
            ],
            "aaSorting": [[1, 'asc']],
            "aLengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 20
        });

        $('#inbox-table').dataTable();

    });
</script>
