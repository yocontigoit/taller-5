1<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/05/25
 * Time: 02:42 PM
 */
 $prog_id = 1;
?>

<div class="table-responsive">
    <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table">
        <thead>
        <tr>
            <th><?php echo lang('label_date'); ?></th>
            <th>Subtotal</th>
            <th>Iva</th>
            <th>Total</th>
            <th>Opciones</th>
        </tr>
        </thead>
        <tbody>
            <?php $date1=""; $amount1=0; $iva1=0;$total1=0; $count1=1; if(count($programs)): foreach($programs as $program): if($program->pago_invno == 1):
            $date1= $program->date_save;
            $amount1= $amount1 + $program->pago_amount/1.16;
            $iva1= $iva1 + $program->pago_amount*16;
            $total1= $total1 + $program->pago_amount;
            endif;endforeach; endif;
            
            $date2=""; $amount2=0; $iva2=0;$total2=0; $count2=1; if(count($programs)): foreach($programs as $program): if($program->pago_invno == 2):
            $date2= $program->date_save;
            $amount2= $amount2 + $program->pago_amount/1.16;
            $iva2= $iva2 + $program->pago_amount*16;
            $total2= $total2 + $program->pago_amount;
            endif;endforeach; endif;   
            $date3=""; $amount3=0; $iva3=0;$total3=0; $count3=1; if(count($programs)): foreach($programs as $program): if($program->pago_invno == 3):
            $date3= $program->date_save;
            $amount3= $amount3 + $program->pago_amount/1.16;
            $iva3= $iva3 + $program->pago_amount*16;
            $total3= $total3 + $program->pago_amount;
            endif;endforeach; endif;   
            ?>
            
            
          <tr>
            <td><?php echo $date1; ?>
           
          </td>
          <td><?php echo $amount1; ?>
          </td>
          <td><?php echo $iva1;?>
          </td>
          <td><?php echo $total1 ?>
          </td>
          <td>  <div class="btn-group-action">
                <div class="btn-group">
                    <a class=" btn btn-info"
                       href="<?php echo base_url('expenses/viewprog/1'); ?>" title=""><i
                            class="fa fa-file"></i></a>
                    
                </div>
            </div>
          </td>
        </tr>
        <tr>
            <td><?php echo $date2; ?>
           
          </td>
          <td><?php echo $amount2; ?>
          </td>
          <td><?php echo $iva2;?>
          </td>
          <td><?php echo $total2; ?>
          </td>
          <td>  <div class="btn-group-action">
                <div class="btn-group">
                    <a class=" btn btn-info"
                       href="<?php echo base_url('expenses/viewprog/2'); ?>" title=""><i
                            class="fa fa-file"></i></a>
                    
                </div>
            </div>
          </td>
        </tr>
        <tr>
            <td><?php echo $date3; ?>
           
          </td>
          <td><?php echo $amount3; ?>
          </td>
          <td><?php echo $iva3;?>
          </td>
          <td><?php echo $total3; ?>
          </td>
          <td>  <div class="btn-group-action">
                <div class="btn-group">
                    <a class=" btn btn-info"
                       href="<?php echo base_url('expenses/viewprog/3'); ?>" title=""><i
                            class="fa fa-file"></i></a>
                    
                </div>
            </div>
          </td>
        </tr>
       

              </tbody>

    </table>
</div>
<link href="http://quattrosol.com/NiteDj/assets/datatable/css/bootstrap.datatable.min.css" rel="stylesheet" type="text/css" />
    <script src="http://quattrosol.com/NiteDj/assets/datatable/js/jquery.dataTables.min.js"></script>
    <script src="http://quattrosol.com/NiteDj/assets/datatable/js/bootstrap.datatable.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            var oTable = $('#zest_table').dataTable({
                "aoColumnDefs": [
                    {"aTargets": [4, 1]}
                ],
                "aaSorting": [[0, 'asc']],
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 20
            });

            $('#inbox-table').dataTable();

        });

    </script>
    <style type="text/css">
        .paginate_button{
            color:#red;
        }
        
    </style>
