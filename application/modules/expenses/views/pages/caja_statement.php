<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/05/25
 * Time: 02:42 PM
 */
?>

<div class="the-box">
    <div class="row">
<div class="table-responsive">
    <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table">
        <thead>
        <tr>
            <th style="width: 64px;"><?php echo lang('label_date'); ?></th>
            <th>Descripcion</th>
            <th>Tercero</th>
            <th>Obra</th>
            <th>Cantidad</th>
            <th>Comprobado</th>
            <th>Cambio</th>
            <th style="width: 45px;">Saldo</th>
            <th>Opciones</th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($expenses)): foreach ($expenses as $expense):


                    $comprobado2 = 0.00;
                    $ticket = 0.00;
                    $change = 0.00;
                    $sub = 0.00;
                    $saldazo = 0.00;
                    $cambio2 = 0.00;
                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $comprobado2 += $transaction->real_cost;
                    endif; endforeach; endif;


                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $cambio2 += $transaction->change_cost;
                    endif; endforeach; endif;


                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $ticket += $transaction->real_cost;
                      $change += $transaction->change_cost;
                      $sub = $ticket + $change;
                    endif; endforeach; endif;
                 $saldazo = $expense->estimate_cost - $sub;

         if ($saldazo !=0):

        ?>
            <tr>
              <td class="">
                  <?php echo $expense->date_entry; ?>
              </td>
                <td>
                  <?php echo $expense->description; ?>
                </td>
                <td class="">
                  <?php  $customer_details = get_client_details($expense->client_id);
                  echo $customer_details->client_name; ?>
                </td>
                  <td>
                  <?php
                  $project_info = get_project_details($expense->project_id);

                     echo $project_info->project_title; ?>
                </td>
                <td class="">$
                    <?php echo format_currency($expense->estimate_cost); ?>

                </td>
                <td class="">
                  <?php
                    $comprobado2 = 0.00;
                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $comprobado2 += $transaction->real_cost;
                    endif; endforeach; endif;
                  ?>
                  $<?php echo format_currency($comprobado2); ?>
                </td>
                <td class="">
                  <?php
                    $cambio2 = 0.00;
                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $cambio2 += $transaction->change_cost;
                    endif; endforeach; endif;
                  ?>
                  $<?php echo format_currency($cambio2); ?>
                </td>
                <td class="">
                  <?php
                    $ticket = 0.00;
                    $change = 0.00;
                    $sub = 0.00;
                    $saldazo = 0.00;
                    if(count($realtransactions)): foreach($realtransactions as $transaction): if($transaction->caja_id == $expense->caja_id):
                      $ticket += $transaction->real_cost;
                      $change += $transaction->change_cost;
                      $sub = $ticket + $change;
                    endif; endforeach; endif;
                    $saldazo = $expense->estimate_cost - $sub;
                  ?>
                  $<?php echo format_currency($saldazo); ?>
                </td>



                <td>
                    <div class="btn-group-action">
                        <div class="btn-group">
                            <a class=" btn btn-info"
                               href="<?php echo base_url('expenses/view_prestamo/' . $expense->caja_id); ?>" title=""><i
                                    class="fa fa-eye"></i>ver</a>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret">&nbsp;</span>
                            </button>
                            <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                                <ul class="dropdown-menu pull-right">
                                    <li><a class="text-danger" data-toggle="zestModal" class="btn"
                                           href="<?php echo base_url('expenses/prove/' . $expense->caja_id . '/' . $expense->project_id); ?>"><i
                                                class="fa fa-file"></i>Comprobar</a>
                                    </li>
                                    <li><a data-toggle="zestModal"
                                           href="<?php echo base_url('expenses/delete_caja/' . $expense->caja_id); ?>"
                                           title=""><i class="fa fa-trash"></i> <?php echo lang('label_delete'); ?></a>
                                    </li>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                </td>
            </tr>
<?php endif; endforeach; endif; ?>
        <?php if (count($realtransactions)): foreach ($realtransactions as $expense): ?>
        <tr>
            <td><?php echo $expense->created_at; ?></td>
            <td><?php echo $expense->categorie; ?></td>
         <td></td>
                  <td>
                  <?php
                  $project_info = get_project_details($expense->project_id);

                     echo $project_info->project_title; ?>
                </td>
               
            <td></td>
            <td><?php if($expense->real_cost != NULL) { echo format_currency($expense->real_cost); } ?></td>
            <td><?php  if($expense->change_cost != NULL) { echo format_currency($expense->change_cost); }?></td>
            <td></td>
            <td></td>
        </tr>
      <?php endforeach;endif; ?>  
        </tbody>
    </table>
    </div>
      </div>
        </div>

<script src="https://proyectosinternos.com/Taller5/assets/libs/jquery-2.0.2.min.js" ></script>
<script src="https://proyectosinternos.com/Taller5/assets/libs/jquery-ui-1.10.3.min.js"></script>
<script src="https://proyectosinternos.com/Taller5/assets/js/jquery-migrate.js"></script>
<link href="https://proyectosinternos.com/Taller5/assets/plugins/datatable/css/bootstrap.datatable.min.css" rel="stylesheet" type="text/css" />
<script src="https://proyectosinternos.com/Taller5/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
<link href="https://proyectosinternos.com/Taller5/assets/plugins/morris-chart/morris.min.css" rel="stylesheet" type="text/css" />
<script src="https://proyectosinternos.com/Taller5/assets/plugins/morris-chart/morris.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {

        var oTable = $('#zest_table').dataTable({
            "aoColumnDefs": [
                {"aTargets": [1, 1]}
            ],
            "aaSorting": [[1, 'asc']],
            "aLengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 20
        });

        $('#inbox-table').dataTable();

    });
</script>
