<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/05/25
 * Time: 02:42 PM
 */
?>
<div class="the-box">
 
 
  <br>
    <div class="row">
        <div align="right" class="col-lg-12">
              <a data-toggle="zestModal" class="btn btn-info btn-sm"
    href="<?php echo base_url('expenses/add_office'); ?>">Nueva Orden<i class="fa fa-plus"></i></a>
    <br><br>
 
<div class="col-lg-12">
    <!--Ticket details -->
    <div class="box">
        <!-- BEGIN PANEL-->
        <div class="panel panel-info panel-square panel-no-border">
            <div class="panel-heading">
                <h3 class="panel-title">Ordenes de Compra</h3>
            </div>

        </div>
        <div class="table-responsive">
            <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table">
                <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Concepto</th>
                    <th>Descripción</th>
                    <th>Cantidad</th>
                    <th>Opciones</th>
                </tr>
                </thead>
                <tbody>
                <?php if (count($offices)): foreach ($offices as $office): if($office->project_id == '28'): ?>
                    <tr>
                      <td><?php echo $office->date_entry ?></td>
                      <td class="">
                          <?php echo $office->concepto ?>
                      </td>
                        <td>
                          <?php echo $office->descripcion; ?>
                        </td>
                        <td class="">
                            <?php echo $office->cantidad; ?>

                        </td>
                        <td>
                            <div class="btn-group-action">
                                <div class="btn-group">
                                    <a class=" btn btn-info"
                                       href="<?php echo base_url('expenses/view_oficina/' . $office->id_oficina); ?>" title=""><i
                                            class="fa fa-eye"></i>ver</a>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret">&nbsp;</span>
                                    </button>
                                    <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a class="text-danger" data-toggle="zestModal"
                                                   href="<?php echo base_url('expenses/edit/' . $office->id_oficina); ?>"><i
                                                        class="fa fa-pencil-square-o"></i> <?php echo lang('label_edit'); ?></a>
                                            </li>
                                            <li><a class="text-danger" data-toggle="zestModal"
                                                   href="<?php echo base_url('expenses/edit/' . $office->id_oficina); ?>"><i
                                                        class="fa fa-file"></i> Crear Recibo</a>
                                            </li>
                                            <li><a data-toggle="zestModal"
                                                   href="<?php echo base_url('expenses/delete/' . $office->id_oficina); ?>"
                                                   title=""><i class="fa fa-trash"></i> <?php echo lang('label_delete'); ?></a>
                                            </li>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </td>
                    </tr>

                </tbody>
        <?php  endif; endforeach; endif;?>
            </table>
        </div>
        <!-- END PANEL -->

    </div>
    <!--end details-->
</div>

</div>

</div>
