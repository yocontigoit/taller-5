<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/05/25
 * Time: 02:41 PM
 */

?>
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-12 col-sm-12 text-center">
             <fieldset>
        <legend><i class="fa fa-calendar"></i>Calendar
            <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) { ?>
                <span class="pull-right">
               <a data-toggle="zestModal" class="text-danger" href="<?php echo base_url('calendar/add_event'); ?>">
                   <i class="fa fa-plus"></i>
                   Add Event
               </a>

                     <a data-toggle="zestModal" class="text-danger" href="<?php echo base_url('calendar/settings'); ?>">
                         <i class="fa fa-plus"></i>
                         Settings
                     </a>
             </span>
            <?php } ?>
        </legend>
        <div class="row">
            <div class="col-md-12 col-sm-6">
                <div class="calendar" id="module_calendar"></div>
            </div>
        </div>
    </fieldset>
            
        </div>
        
    </div>
</div>
