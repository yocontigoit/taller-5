<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/05/25
 * Time: 02:42 PM
 */
?>


<div class="table-responsive">

    <a href="<?php echo base_url('expenses/expenses/add_gastos/'); ?>" data-toggle="zestModal" class="btn btn-info">Agregar Gasto <i class="fa fa-money"></i></a>

</div>
<br><br>
<div class="table-responsive">
    <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table">
        <thead>
        <tr>
            <th class="hidden-xs"><?php echo lang('label_date'); ?></th>
            <th>Descripción</th>
            <th>Obra</th>
            <th>Subtotal</th>
            <th>Iva Materiales</th>
            <th>Gran Total</th>
            <th>Opciones</th>
        </tr>
        </thead>
        <tbody>
          <?php $cont = 1; if (count($transactions)) : foreach ($transactions as $transaction) : if ($transaction->gasto == 1):?>
          <tr>
            <td>
              <?php echo $transaction->invoice_due; ?>
            </td>
          <td>
            <?php echo $transaction->inv_no; ?>
          </td>
          <td>
            <a href="project/overview/preview/<?php echo $transaction->project_id; ?>">
              <?php if (count($projects)) : foreach ($projects as $project): if ($project->project_id == $transaction->project_id):  ?>
                  <?php echo $project->project_title ?>
              <?php endif; endforeach; endif; ?></a>
          </td>
          <td>
            $0.00
          </td>
          <td>
            $0.00
          </td>
          <td>
            $<?php echo format_currency(invoice_total($transaction->id)); ?>
          </td>
          <td>  <div class="btn-group-action">
                <div class="btn-group">
                    <a class=" btn btn-info"
                       href="<?php echo base_url('expenses/view/'); ?>" title=""><i
                            class="fa fa-eye"></i>ver</a>
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <span class="caret">&nbsp;</span>
                    </button>
                      <ul class="dropdown-menu pull-right">
                            <li><a class="text-danger" data-toggle="zestModal"
                                   href="<?php echo base_url('expenses/?status=estimado'); ?>"><i
                                        class="fa fa-pencil-square-o"></i> Editar</a>
                            </li>
                            <li><a class="text-danger" data-toggle="zestModal"
                                   href="<?php echo base_url('expenses/?status=estimado'); ?>"><i
                                        class="fa fa-file"></i>Imprimir</a>
                            </li>
                            <li><a data-toggle="zestModal"
                                   href="<?php echo base_url('expenses/delete/'); ?>"
                                   title=""><i class="fa fa-trash"></i> <?php echo lang('label_delete'); ?></a>
                            </li>
                        </ul>
                </div>
            </div>
          </td>
        </tr>
        <?php $cont++; endif; endforeach; endif; ?>
</tbody>

    </table>
</div>
<script src="https://proyectosinternos.com/Taller5/assets/libs/jquery-2.0.2.min.js"></script>
<script src="https://proyectosinternos.com/Taller5/assets/plugins/ios7-switch/ios7.switch.js"></script>
<link href="https://proyectosinternos.com/Taller5/assets/plugins/datatable/css/bootstrap.datatable.min.css" rel="stylesheet" type="text/css">
<script src="https://proyectosinternos.com/Taller5/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="https://proyectosinternos.com/Taller5/assets/plugins/datatable/js/jquery.dataTables.js"></script>
<script src="https://proyectosinternos.com/Taller5/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
<script src="https://proyectosinternos.com/Taller5/assets/libs/jquery-ui-1.10.3.min.js"></script>

<div class="modal" id="zestModal" style="display: none;"><div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Nota de Credito</h4>
        </div>
        <form method="post" action="https://proyectosinternos.com/Taller5/billing/add_nota_credito/">
            <input type="hidden" name="estimacion_id" value""="">
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Obra</label>

                              <select data-placeholder="Select client..." class="form-control  chosen-select" tabindex="-1" name="project" id="project" style="display: none;">
                                  <option value="Empty">&nbsp;</option>
                                  <optgroup label="Obras">
                                                                                <option value="28">Penthouse Baeza</option>
                                                                                <option value="30">CASA ZARAGOZA BARBA</option>
                                                                        </optgroup>
                              </select><div class="chosen-container chosen-container-single" style="width: 419px;" title="" id="project_chosen"><a class="chosen-single" tabindex="-1"><span>&nbsp;</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off" tabindex="4"></div><ul class="chosen-results"></ul></div></div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="taskPriority">Proveedor </label>

                          <select class="form-control chosen-select" name="client_id" style="display: none;">
                              <option value="">Ninguno</option>
                                                                <option value="17">Vimaco</option>
                                                                <option value="29">PLOMERÍA Y SANITARIOS DEL BAJÍO S.A. DE C.V.</option>
                                                                <option value="33">ELEKTROCONTROL</option>
                                                                <option value="34">MATERIALES PARA CONSTRUCCIÓN GRANADA</option>
                                                                <option value="35">SECO SOLUCIONES S.A. DE C.V.</option>
                                                                <option value="36">PLOMERÍA Y FERRETERÍA AZTECA</option>
                                                                <option value="37">MADERAS DE SAN JUAN BOSCO S.A. DE C.V.</option>
                                                                <option value="38">LA INDUSTRIAL MEXICANA S.A. DE C.V. (LIMSA)</option>
                                                                <option value="39">FC FÁCIL DE CONSTRUIR S.A. DE C.V.</option>
                                                                <option value="40">MACRO LOSA</option>
                                                        </select><div class="chosen-container chosen-container-single" style="width: 419px;" title=""><a class="chosen-single" tabindex="-1"><span>Ninguno</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off"></div><ul class="chosen-results"></ul></div></div>
                      </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <h2><p class="text-info">NC-00002</p></h2>
                    <input type="hidden" name="folio" value="NC-00002">
                      <!-- <div class="form-group">
                          <label for="taskPriority">Metodo de Pago </label>

                          <select class="form-control chosen-select" name="method">
                              <option value="">Ninguno</option>

                                  <option
                                      value="1">Efectivo</option>
                                      <option
                                      value="1">Transaccion</option>
                                      <option
                                      value="1">Cheque</option>


                          </select>
                          <span class="help-block">Efectivo, tarjeta de crédito..</span>
                      </div> -->
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="payment_date">Monto</label>
                        <input type="text" class="form-control" name="amount" value="">
                      </div>
                    </div>
                  </div>
                </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    Agregar nota
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    Cancelar                </button>
            </div>
        </form>
    </div>
</div>


<link href="https://proyectosinternos.com/Taller5/assets/plugins/datepicker/datepicker.min.css" rel="stylesheet" type="text/css">
<link href="https://proyectosinternos.com/Taller5/assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
<script src="https://proyectosinternos.com/Taller5/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="https://proyectosinternos.com/Taller5/assets/plugins/timepicker/bootstrap-timepicker.js"></script>

<script type="text/javascript">
    $('#tasks-percent').slider({
        formater: function (value) {
            return value + '% Complete..';
        }
    });

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }
</script>
</div>
