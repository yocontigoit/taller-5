<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/05/25
 * Time: 02:42 PM
 */
?>
<h3><?php echo $method_details->payment_name; ?></h3>

<?php $total=0; if (count($transactions)): foreach($transactions as $transaction): if($transaction->payment_method == $setting_id):
      $project_info = get_project_details($transaction->project_id);
      $customer_details = get_client_details($transaction->client_id);
?>

<?php
    $subtotal =0;
    $subtotal+= $transaction->amount;
    $total += $method_details->payment_amount - $subtotal;
?>
<?php endif; endforeach; endif; ?>
<center><h4>Saldo: $<?php echo format_currency($total); ?></h4></center>
<div class="table-responsive">
    <a  href="" class="btn btn-info btn-md btn-icon mt-10" data-toggle="zestModal">Ajuste de Cuenta <i class="fa fa-money fa-lg" ></i></a>
    <br><br>
    <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
           id="zest_table20">
        <thead class="the-box dark full">
        <tr>

            <th>Fecha</th>
            <th>Referencia</th>
            <!-- <th>Orden/Presupuesto</th> -->
            <th>Obra</th>
            <th>Tercero</th>
            <th>Factura</th>
            <th>Monto</th>
            <th>Saldo</th>
        </tr>
        </thead>
        <tbody>
            <?php //$total=0; $project=0; $client=0;
            if (count($transactions)): foreach($transactions as $transaction): if($transaction->payment_method == $setting_id):

// if ($transaction->invoice_id == 164){
//      $project_info = get_project_details($transaction->proid);
//       $project = $transaction->project_id;
//
// }else{
      $project_info = get_project_details($transaction->project_id);
     $project = $transaction->caja_project;

// if ($transaction->invoice_id != 164){
//      $customer_details = get_client_details($transaction->client_id);
//      $client = $transaction->client_id;
//      $client_name =  $customer_details->client_name;
//      $clienturl = "/Taller5/client/view/$client";
// }else{
//     $customer_details = get_client_details($transaction->client_id);
//      $client = $transaction->caja_client;
//      $client_name = "Caja Chica";
//      $clienturl = "/Taller5/expenses/view_statement/9";
// }



                ?>
                <tr>
                    <td><?php echo  date("Y-m-d", strtotime($transaction->created_at));   ?></td>
                     <td><?php echo $transaction->reference; ?></td>

                    <!-- <td><a href="/Taller5/billing/invoice/<?php echo $transaction->invoice_id; ?>"><?php echo $transaction->inv_no; ?></a></td> -->
                    <?php if($transaction->project_id != 0) { ?>
                      <td><a href="/Taller5/project/overview/preview/<?php  echo $transaction->project_id; ?>"><?php echo $project_info->project_title; ?></td>
                    <?php }else{?>
                      <td></td>
                    <?php } ?>
                    <td><a href="<?php echo $transaction->client_id; ?>"><?php echo client_company($transaction->client_id); ?></td>
                    <td><?php echo $transaction->invoice; ?></td>
                    <?php if($transaction->amount != 0) { ?>
                    <td>$<?php echo format_currency($transaction->amount); ?></td>
                  <?php }else{?>
                    <td></td>
                  <?php } ?>
                    <td>$<?php $total = $method_details->payment_amount - $transaction->amount;
                               echo format_currency($total); ?></td>

            </tr>

            <?php
          endif; endforeach;endif;?>

        </tbody>


    </table>
</div>
