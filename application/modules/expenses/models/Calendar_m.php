<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/09
 * Time: 02:32 PM
 */
class Calendar_M extends CI_Model
{

    /**
     * retrieve all tasks
     * @return mixed
     */
    function tasks()
    {
        $this->db->from('tasks');
        $this->db->join('projects', 'projects.project_id  = tasks.project_id');
        return $this->db->get()->result();
    }


    /**
     * retrieve all paid invoices 
     * @return mixed
     */
    function payments()
    {
        $this->db->select('*,transaction.id as payment_id');
        $this->db->from('transaction');
        $this->db->join('invoice', 'invoice.id  = transaction.invoice_id');

        return $this->db->get()->result();

    }

    /**
     * retrieve all projects 
     * @return mixed
     */
    function projects()
    {

        $this->db->from('projects');
        $this->db->join('client', 'client.client_id  = projects.client_id');

        return $this->db->get()->result();
    }

    function invoices()
    {
        $this->db->from('invoice');
        $this->db->join('client', 'invoice.client_id  = client.client_id');

        return $this->db->get()->result();

    }

    function estimates()
    {

        $this->db->from('estimate');
        $this->db->join('client', 'estimate.client_id  = client.client_id');

        return $this->db->get()->result();
    }

    function quotations()
    {

        $this->db->from('quotation');
        $this->db->join('client', 'quotation.client_id  = client.client_id');

        return $this->db->get()->result();
    }

    function agenda()
    {

        $this->db->from("events");
        $this->db->select("
                event_id,
				user_id,
				start_date,
				DATE_FORMAT(start_date,'%Y-%m-%d') AS startsOnDate,
				DATE_FORMAT(start_date,'%m') AS startMonth,
				DATE_FORMAT(start_date,'%H:%i') AS startTime,
				DATE_FORMAT(start_date,'%H:%i') AS timeStart,
				DATE_FORMAT(start_date,'%l:%i %p') AS displaystart,
				DATE_FORMAT(start_date,'%M %d, %Y') AS displayDate,
				end_date,
				DATE_FORMAT(end_date,'%Y-%m-%d') AS endsOnDate,
				DATE_FORMAT(end_date,'%m') AS endMonth,
				DATE_FORMAT(end_date,'%H:%i') AS endTime,
				DATE_FORMAT(end_date,'%Y, %m, %d') AS dateEnd,
				DATE_FORMAT(end_date,'%l:%i %p') AS displayend,
				event_title,
				event_description,
				event_color,
				is_task", false);

        return $this->db->get()->result();

    }

    function add_event($data = array())
    {
        $success = false;
        $success = $this->db->insert('events', $data);

        return $success;
    }

    function update_event($data = array(), $event_id = null)
    {

        $success = false;
        $success = $this->db->update('events', $data, array('event_id' => intval($event_id)));

        return $success;
    }

    function task_details($task_id = null)
    {

        $this->db->from('tasks');
        $this->db->where("task_id", $task_id);
		$this->db->join('projects','projects.project_id = tasks.project_id');

        return $this->db->get()->row();

    }

    function payment_details($trans_id = null)
    {
        $this->db->from('transaction');
        $this->db->join('invoice', 'invoice.id  = transaction.invoice_id');
        $this->db->where("transaction.id", $trans_id);

        return $this->db->get()->row();
    }

    function project_details($project_id = null)
    {
        $this->db->from('projects');
        $this->db->join('client', 'client.client_id  = projects.client_id');
        $this->db->where("projects.project_id", $project_id);

        return $this->db->get()->row();
    }

    function invoice_details($invoice_id = null)
    {
        $this->db->from('invoice');
        $this->db->join('client', 'invoice.client_id  = client.client_id');
        $this->db->where("invoice.id", $invoice_id);

        return $this->db->get()->row();
    }

    function estimate_details($estimate_id = null)
    {

        $this->db->from('estimate');
        $this->db->join('client', 'estimate.client_id  = client.client_id');
        $this->db->where("estimate.est_id", $estimate_id);

        return $this->db->get()->row();
    }

    function quotation_details($quotation_id = null)
    {
        $this->db->from('quotation');
        $this->db->join('client', 'quotation.client_id  = client.client_id');
        $this->db->where("quotation.quote_project_id", $quotation_id);

        return $this->db->get()->row();
    }

    function agenda_details($event_id = null)
    {
        $this->db->from('events');
        $this->db->where("event_id", $event_id);

        return $this->db->get()->row();
    }

    function calendar_settings($data = array(), $company_id = null)
    {

        $success = false;
        $success = $this->db->update('settings_system', $data, array('company_company_id' => intval($company_id)));

        return $success;
    }

    function add_log($data = array())
    {
        $success = false;
        $success = $this->db->insert("activity_log", $data);

        return $success;
    }
}