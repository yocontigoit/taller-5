<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/09
 * Time: 02:32 PM
 */
class Expenses_M extends CI_Model
{

    /**
     * get all expenses
     * @return mixed
     */
    function all_expenses()
    {

        $this->db->from('costs');

        return $this->db->get()->result();
    }

    function all_methods()
    {

        $this->db->from('settings_payment_methods');

        return $this->db->get()->result();
    }
 function method_details($setting_id = null)
    {
        $this->db->from('settings_payment_methods');
        $this->db->where('setting_id', $setting_id);


        return $this->db->get()->row();

    }

    function get_prove_details($caja_id)
    {
        $this->db->from('caja');
        $this->db->where('caja_id', $caja_id);


        return $this->db->get()->row();

    }
    /**
     * Get all users for a company
     * @param null $id
     * @return mixed
     */
    function get_all_estimates()
    {
        return $this->db->get('estimate')->result();

    }
    function all_programs()
    {
        return $this->db->get('estimate')->result();

    }

    /**
     * get all estimates by client
     * @param $client_id
     * @return mixed
     */
    function get_client_estimates($client_id)
    {

        $this->db->from('estimate');
        $this->db->where("client_id", $client_id);
        $this->db->where("est_visible", 1);

        return $this->db->get()->result();

    }

    function estimaciones($project_id = null)
    {
        $this->db->from('estimaciones');
        $this->db->where("project_id", $project_id);
        return $this->db->get()->result();

    }

    function client_expenses($client_id = null)
    {

        return null;
    }

    /**
     * get all categories
     * @return mixed
     */
    function categories()
    {
        $this->db->from('client');
        $this->db->where("client_type !=", 1);

        return $this->db->get()->result();

    }
    function caja()
    {
        $this->db->from('costs');
        $this->db->where("cost_id", 30);

        return $this->db->get()->result();

    }
    function imports()
    {
        $this->db->from('import');

        return $this->db->get()->result();

    }
    function all_caja()
    {
        $this->db->from('caja');


        return $this->db->get()->result();

    }

    function caja_details($caja_id = null)
    {
        $this->db->from('caja');
        $this->db->where('caja_id', $caja_id);


        return $this->db->get()->row();

    }



    function all_office()
    {
        $this->db->from('gastos_oficina');

        return $this->db->get()->result();

    }

    function obra()
    {
        $this->db->from('projects');

        return $this->db->get()->result();

    }

    function obra_row($project_id = null)
    {
        $this->db->from('projects');
        $this->db->where("project_id", $project_id);

        return $this->db->get()->row();

    }

    function transactions_caja()
    {
        $this->db->from('transaction');

        return $this->db->get()->result();

    }


    /**
     * @param array $data
     * @return null
     */
    function add_cost_plan($data = array())
    {
        $success = false;
        $success = $this->db->insert('costs', $data);
        $expense_id = $this->db->insert_id();

        return ($success) ? $expense_id : null;
    }

      function get_method_transactions($method_id)
    {
        $this->db->select('*, transaction.id AS payment_id, transaction.payment_method AS paymet, transaction.project_id AS proid');
        $this->db->from('transaction');
        $this->db->join('invoice', 'transaction.invoice_id = invoice.id');
        $this->db->where('transaction.payment_method', intval($method_id));

        return $this->db->get()->result();

    }

     function get_transactions()
    {
        $this->db->select('*, transaction.id AS payment_id,invoice.payment_method AS pay_method, transaction.payment_method AS paymet');
        $this->db->from('transaction');
        $this->db->join('invoice', 'transaction.invoice_id = invoice.id');

        return $this->db->get()->result();

    }

    function get_all_transactions()
   {

       $this->db->from('transaction');


       return $this->db->get()->result();

   }

    function get_realtransactions()
   {

       $this->db->from('transaction_caja');


       return $this->db->get()->result();

   }

    /**
     * @param array $data
     * @return null
     */
    function add_caja($data = array())
    {
        $success = false;
        $success = $this->db->insert('caja', $data);
        $expense_id = $this->db->insert_id();

        return ($success) ? $expense_id : null;
    }

     function add_accounts($data = array())
    {
        $success = false;
        $success = $this->db->insert('settings_payment_methods', $data);
        $expense_id = $this->db->insert_id();

        if ($success) {
          $code = array(
              "payment_code" => $expense_id
          );
           $success = $this->db->update('settings_payment_methods', $code, array('setting_id' => $expense_id));
        }

        return ($success) ? $expense_id : null;
    }

    /**
     * @param array $data
     * @return null
     */
    function add_office($data = array())
    {
        $success = false;
        $success = $this->db->insert('gastos_oficina', $data);
        $expense_id = $this->db->insert_id();

        return ($success) ? $expense_id : null;
    }

    function update_cost_receipt($data = array(), $cost_id = null)
    {
        $success = false;
        $success = $this->db->update('costs', $data, array('cost_id' => $cost_id));

        return $success;
    }

    /**
     * remove expense
     * @param null $cost_id
     * @return bool
     */
    function remove_expense($cost_id = null)
    {

        $success = false;
        $success = $this->db->where('cost_id', $cost_id)->delete('costs');

        return $success;

    }

    /**
     * remove caja
     * @param null $cost_id
     * @return bool
     */
    function remove_caja($caja_id = null)
    {

        $success = false;
        $success = $this->db->where('caja_id', $caja_id)->delete('caja');

        return $success;

    }

    /**
     * remove prestamo
     * @param null $id_prestamo
     * @return bool
     */
    function remove_transaccion_caja($id_prestamo = null)
    {

        $success = false;
        $success = $this->db->where('id', $id_prestamo)->delete('transaction_caja');

        return $success;

    }

    /**
     * add activity log
     * @param array $data
     * @return bool
     */
    function add_log($data = array())
    {

        $success = false;
        $success = $this->db->insert("activity_log", $data);

        return $success;
    }

    /**
     * get expense details
     * @param null $expense_id
     * @return mixed
     */
    function expense_details($cost_id = null)
    {
        $this->db->from('costs');
        $this->db->where("cost_id", $cost_id);

        return $this->db->get()->result();
    }

    /**
     * get expense details
     * @param null $expense_id
     * @return mixed
     */
    function office_details($id_oficina = null)
    {
        $this->db->from('gastos_oficina');
        $this->db->where("id_oficina", $id_oficina);

        return $this->db->get()->result();
    }

    /**
     * update settings_payment_methods
     * @param null $id
     * @return bool
     */
    public function update_settings($data = array(), $setting_id = null)
    {
        $success = false;

        $success = $this->db->where('setting_id', $setting_id)->update('settings_payment_methods', $data);

        return $success;
    }

    /**
     * update caja
     * @param null $caja_id
     * @return bool
     */
    public function update_prove($data = array(), $caja_id = null)
    {
        $success = false;

        $success = $this->db->where('caja_id', $caja_id)->update('caja', $data);

        return $success;
    }

    /**
     * add payment to invoice
     * @param array $data
     * @return bool
     */
    function add_transaccion($data = array())
    {

        $success = false;
        $this->db->trans_start();
        $success = $this->db->insert('transaction', $data);

        $this->db->trans_complete();

        return $success;

    }

    /**
     * add payment to invoice
     * @param array $data
     * @return bool
     */
    function transaccion_caja($data = array())
    {

        $success = false;
        $this->db->trans_start();
        $success = $this->db->insert('transaction_caja', $data);

        $this->db->trans_complete();

        return $success;

    }

    function get_proveedor_list()
    {

        $this->db->from("client");
        $this->db->where('client_type', 2);

        return $this->db->get()->result();

    }

    function get_projects()
    {
        $this->db->from('projects');
    return $this->db->get()->result();
    }

    /**
     * add invoice
     * @param array $data
     * @param array $items
     * @return null / invoice_id
     */
    function add_gasto($data = array(), $monto = null, $name = null, $desc = null)
    {

        $success = false;
        $this->db->trans_start();

        if(!$success) {
            $success = $this->db->insert('invoice', $data);
            $invoice_id = $this->db->insert_id();

            if ($success) {
                //add items
                        $param = array(
                            "invoice_id" => $invoice_id,
                            "item_name" => $name,
                            "item_desc" => $desc,
                            "discount" => 0,
                            "tax" => 0,
                            "quantity" => 1,
                            "price" => $monto

                        );
                        $success = $this->db->insert('invoice_item', $param);


                }

                if ($success) {
                    $reference_no = explode("-", $data['inv_no']);
                    $success = $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
                        array('typeid' => 18, 'company_id' => $this->Company_id));
                }
            }

        $items_count = $this->db->where(array('invoice_id' => $invoice_id))->get('invoice_item')->result();

        if(count($items_count)){
            $this->db->trans_complete();
        }else{
            $this->db->trans_rollback();
            $success = false;
        }

        return ($success) ? $invoice_id : null;
    }

    /**
     * get all transaction by client
     * @param $client_id
     * @return mixed
     */
    function client_transaction2()
    {

        $this->db->from('transaction');
    $this->db->join('settings_payment_methods', 'settings_payment_methods.setting_id = transaction.payment_method');

        return $this->db->get()->result();

    }

    /**
     * list all invoices_item
     * @return mixed
     */
    function  all_invoice_item()
    {

        $this->db->from('invoice_item');

        return $this->db->get()->result();
    }

    function get_invo_client(){
      $this->db->select('*');
      $this->db->from('invoice');
      $this->db->group_by('invoice.client_id');
      $this->db->join('client', 'client.client_id = invoice.client_id ');

        return $this->db->get()->result();
    }

    /**
     * list all invoices
     * @return mixed
     */
    function  all_invoice()
    {

        $this->db->from('invoice');

        return $this->db->get()->result();
    }

    /**
     * retrive all client invoices
     * @param $client_id
     * @return mixed
     */
    function client_all_invoice($client_id)
    {

        $this->db->from('invoice');
        $this->db->where("client_id", $client_id);

        return $this->db->get()->result();
    }

    function get_milestones(){

      $this->db->from('milestones');
      return $this->db->get()->result();

    }
}
