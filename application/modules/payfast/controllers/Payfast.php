<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/01
 * Time: 04:22 PM
 */
class Payfast extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();
        $this->load->helper('payfast'); // Load the helper
    }


    function add_payment($invoice_id = null)
    {
        // If file doesn't exist, create it


        $pfHost = (($this->config->item('payfast')->payfast_active == 0) ? 'sandbox' : 'www') . '.payfast.co.za';

        $this->data['sandbox'] = ($this->config->item('payfast')->payfast_active == 0) ? false : true;

        $this->data['payfast_url'] = 'https://' . $pfHost . '/eng/process';

        $reference_no = get_invoice_reference_no($invoice_id);
        $invoice_currency = get_invoice_currency($invoice_id);
        //$invoice_amount = invoice_total($invoice_id);
        $invoice_amount = invoice_balance($invoice_id);

        if ($invoice_amount <= 0) {
            $invoice_amount = 0.00;
        }
        $invoice_info = new stdClass();
        $invoice_info->item_name = $reference_no;
        $invoice_info->item_number = $invoice_id;
        $invoice_info->amount = $invoice_amount;
        $invoice_info->currency = $invoice_currency;


        if ($invoice_info) {


            if ($this->config->item('payfast')->payfast_active == 1) {
                $merchant_id = $this->config->item('payfast')->payfast_merchant_id;
                $merchant_key = $this->config->item('payfast')->payfast_merchant_key;

            } else {
                $merchant_id = '10000100';
                $merchant_key = '46f0cd694581a';

            }

            $return_url = site_url($this->config->item('payfast')->payfast_success);
            $cancel_url = site_url($this->config->item('payfast')->payfast_cancel);
            $notify_url = site_url($this->config->item('payfast')->payfast_ipn);


            list($payment_firstname, $payment_lastname) = explode(" ", $this->CompanyUser_FullName);

            $name_first = html_entity_decode($payment_firstname, ENT_QUOTES, 'UTF-8');
            $name_last = html_entity_decode($payment_lastname, ENT_QUOTES, 'UTF-8');
            $email_address = $this->CompanyClient_email;
            // Username	sbtu01@payfast.co.za
            //Password	clientpass
            $m_payment_id = $invoice_id;
            $amount = $invoice_info->amount;
            $item_name = $invoice_info->item_name;
            $item_description = $invoice_info->item_name . "-" . $invoice_info->item_number;
            $custom_str1 = $invoice_id;

            $payArray = array(
                'merchant_id' => $merchant_id,
                'merchant_key' => $merchant_key,
                'return_url' => $return_url,
                'cancel_url' => $cancel_url,
                'notify_url' => $notify_url,
                'name_first' => $name_first,
                'name_last' => $name_last,
                'email_address' => $email_address,
                'm_payment_id' => $m_payment_id,
                'amount' => $amount,
                'item_name' => html_entity_decode($item_name),
                'item_description' => html_entity_decode($item_description),
                'custom_str1' => $custom_str1
            );

            $secureString = '';
            foreach ($payArray as $k => $v) {
                $secureString .= $k . '=' . urlencode(trim($v)) . '&';
                $this->data[$k] = $v;
            }
            $passphrase = $this->config->item('payfast')->payfast_passphrase;

            if (!empty($passphrase) && $this->config->item('payfast')->payfast_active == 1) {
                $secureString = $secureString . 'passphrase=' . urlencode($this->config->item('payfast')->payfast_passphrase);
            } else {
                $secureString = substr($secureString, 0, -1);
            }

            $securityHash = md5($secureString);
            $this->data['signature'] = $securityHash;

            $this->data['title'] = 'Agregar Pago';

            $this->load->view('modal/add_payment', $this->data);
        }
    }

    function cancel()
    {
        $this->session->set_flashdata('msg_status', 'error');
        $this->session->set_flashdata('message', lang('messages_payment_cancel_success'));
        redirect('client');
    }

    function success()
    {
        if ($_POST) {
            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', lang('messages_payment_add_success'));
            redirect('client');
        } else {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', lang('messages_payment_add_error'));
            redirect('client');
        }
    }


}