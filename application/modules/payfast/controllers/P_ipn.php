<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/05/30
 * Time: 01:39 PM
 */
ini_set('log_errors', true);
ini_set('error_log', dirname(__file__) . '/ipn_errors.log');

class P_ipn extends Admin_Controller
{


    function ipn()
    {

        $this->load->helper('payfast'); // Load the helper
        $this->load->library('email'); // Load the library
        $pfError = false;
        $pfErrMsg = '';
        $pfDone = false;
        $pfData = array();
        $pfParamString = '';
        if (isset($this->input->post['custom_str1'])) {
            $order_id = $this->input->post['custom_str1'];
        } else {
            $order_id = 0;
        }

        //pflog('POST received from payfast.co.za: e' . print_r($this->input->post, true));
        pflog('PayFast ITN call received');

        //// Notify PayFast that information has been received
        if (!$pfError && !$pfDone) {
            header('HTTP/1.0 200 OK');
            flush();
        }

        //// Get data sent by PayFast
        if (!$pfError && !$pfDone) {
            pflog('Get posted data');

            // Posted variables from ITN
            $pfData = pfGetData();
            pflog('POST received from payfast.co.za: ' . print_r($pfData, true));

            $pfData['item_name'] = html_entity_decode($pfData['item_name']);
            $pfData['item_description'] = html_entity_decode($pfData['item_description']);
            pflog('PayFast Data: ' . print_r($pfData, true));

            if ($pfData === false) {
                $pfError = true;
                $pfErrMsg = PF_ERR_BAD_ACCESS;
            }
        }

        //// Verify security signature
        if (!$pfError && !$pfDone) {
            pflog('Verify security signature');
            //$passphrase = $this->config->get('payfast_passphrase');
            //$pfPassphrase = $this->config->get( 'payfast_sandbox' ) ? null : ( !empty( $passphrase ) ? $passphrase : null );
            // If signature different, log for debugging
            /*if( !pfValidSignature( $pfData, $pfParamString, $pfPassphrase ) )
            {
                $pfError = true;
                $pfErrMsg = PF_ERR_INVALID_SIGNATURE;
            }
            */
        }

        //// Verify source IP (If not in debug mode)
        if (!$pfError && !$pfDone && !PF_DEBUG) {
            pflog('Verify source IP');

            if (!pfValidIP($_SERVER['REMOTE_ADDR'])) {
                $pfError = true;
                $pfErrMsg = PF_ERR_BAD_SOURCE_IP;
            }
        }
        //// Get internal cart
        if (!$pfError && !$pfDone) {
            // Get order data
            $this->load->model('checkout/order');
            $order_info = $this->model_checkout_order->getOrder($order_id);

            pflog("Purchase:\n" . print_r($order_info, true));
        }

        //// Verify data received
        if (!$pfError) {
            pflog('Verify data received');

            $pfValid = pfValidData($this->pfHost, $pfParamString);

            if (!$pfValid) {
                $pfError = true;
                $pfErrMsg = PF_ERR_BAD_ACCESS;
            }
        }

        //// Check data against internal order
        if (!$pfError && !$pfDone) {
            pflog('Check data against internal order');

            $amount = $this->currency->format($order_info['total'], 'ZAR', '', false);
            // Check order amount
            if (!pfAmountsEqual($pfData['amount_gross'], $amount)) {
                $pfError = true;
                $pfErrMsg = PF_ERR_AMOUNT_MISMATCH;
            }

        }

        //// Check status and update order
        if (!$pfError && !$pfDone) {
            pflog('Check status and update order');


            $transaction_id = $pfData['pf_payment_id'];

            switch ($pfData['payment_status']) {
                case 'COMPLETE':
                    pflog('- Complete');

                    // Update the purchase status
                    $order_status_id = $this->config->get('payfast_completed_status_id');

                    break;

                case 'FAILED':
                    pflog('- Failed');

                    // If payment fails, delete the purchase log
                    $order_status_id = $this->config->get('payfast_failed_status_id');

                    break;

                case 'PENDING':
                    pflog('- Pending');

                    // Need to wait for "Completed" before processing
                    break;

                default:
                    // If unknown status, do nothing (safest course of action)
                    break;
            }
            if (!$order_info['order_status_id']) {
                $this->model_checkout_order->confirm($order_id, $order_status_id);
            } else {
                $this->model_checkout_order->update($order_id, $order_status_id);
            }
        } else {
            pflog("Errors:\n" . print_r($pfErrMsg, true));
        }

    }
}