<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/01
 * Time: 02:52 PM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                        class="fa fa-times"></i></span><span class="sr-only"><?php echo "close"; ?></span></button>
            <h4 class="modal-title"><?php echo "Purchase Summary"; ?></h4>
        </div>

        <?php echo form_open($payfast_url, array('id' => 'paypal_form', 'class' => 'form-horizontal')); ?>
        <input type="hidden" name="merchant_id" value="<?php echo $merchant_id; ?>"/>
        <input type="hidden" name="merchant_key" value="<?php echo $merchant_key; ?>"/>
        <input type="hidden" name="amount" value="<?php echo $amount; ?>"/>
        <input type="hidden" name="item_name" value="<?php echo $item_name; ?>"/>
        <input type="hidden" name="item_description" value="<?php echo $item_description; ?>"/>
        <input type="hidden" name="name_first" value="<?php echo $name_first; ?>"/>
        <input type="hidden" name="name_last" value="<?php echo $name_last; ?>"/>
        <input type="hidden" name="email_address" value="<?php echo $email_address; ?>"/>
        <input type="hidden" name="return_url" value="<?php echo $return_url; ?>"/>
        <input type="hidden" name="notify_url" value="<?php echo $notify_url; ?>"/>
        <input type="hidden" name="cancel_url" value="<?php echo $cancel_url; ?>"/>
        <input type="hidden" name="custom_str1" value="<?php echo $custom_str1; ?>"/>
        <input type="hidden" name="m_payment_id" value="<?php echo $m_payment_id; ?>"/>
        <input type="hidden" name="signature" value="<?php echo $signature; ?>"/>

        <div class="modal-body">
            <table class="table">

                <tbody>
                <tr>
                    <th>Reference</th>
                    <td><input type="text" class="form-control" readonly value="<?php echo $item_name; ?>"/></td>
                </tr>
                <tr>
                    <th>Amount</th>
                    <td><input type="text" class="form-control" readonly value="<?php echo format_amount($amount); ?>"/>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <input type="image" src="<?php echo base_url('files/gateways/payfast/payfast.png'); ?>" name="submit"
                   title="Pay With PayFast" alt="">
        </div>
        </form>

    </div>
</div>