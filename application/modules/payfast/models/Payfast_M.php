<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/06/06
 * Time: 09:33 AM
 */
class Payfast_M extends CI_Model
{


    function add_payment($data = array())
    {
        $success = false;
        $this->db->trans_start();
        $success = $this->db->insert('transaction', $data);

        if ($success) {
            //check
            $invoice_id = intval($data['invoice_id']);
            $success = $this->set_invoice_status($invoice_id);

        }
        $this->db->trans_complete();

        return $success;
    }

    public function set_invoice_status($invoice_id = null)
    {
        $success = false;
        $invoice_amt = $this->get_invoice_total_amt($invoice_id);
        $paid_amt = $this->get_invoice_paid_amt($invoice_id);
        $invoice_status = ($invoice_amt - $paid_amt < 1) ? 1 : 2;
        $status = array('status' => $invoice_status, 'paid_at' => date('Y-m-d H:i:s'));
        $success = $this->db->update('invoice', $status, array('id' => $invoice_id));

        return $success;
    }

    public function get_invoice_total_amt($invoice_id = null)
    {

        return invoice_sub_total_total($invoice_id);

    }

    public function get_invoice_paid_amt($invoice_id = null)
    {
        return _get_invoice_payments($invoice_id);
    }


}