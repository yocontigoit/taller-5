<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/12
 * Time: 09:32 AM
 */
class Message extends Admin_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model("message_m");
        $this->load->model("estimate_m");

    }

    /**
     * Display all inbox,sent and deleted mails..
     */
    function index()
    {

        $this->data['title'] = 'Mensaje';
        $mail_page = $this->input->get('page', true) ? $this->input->get('page', true) : 'inbox';

        $this->data['estimates'] = $this->estimate_m->get_all_estimates();


        $this->data['_EST_PAGE'] = $this->load->view('mail', $this->data, true);

        $this->data['inbox_datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['content'] = 'message/mail';
        $this->load->view('_main_layout', $this->data);

    }
    function create_estimate($quote_id = null)
    {
        $this->data['content'] = 'estimate/estimates';
        $estimate_id = null;
        $currency = null;
        if ($this->input->post()) {

            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $estimate_visible = ($this->input->post('estimate_visible') == 'on') ? 1 : 0;

            if ($this->input->post('estimate_currency') == 'default') {
                $client_currency = get_client_details(intval($this->input->post('client')));
                $currency = get_currency_name($client_currency->client_currency);
            } else {

                $currency = get_currency_name($this->input->post('estimate_currency'));
            }

            $param = array(
                "client_id" => $this->input->post('client'),
                "est_code" => $this->input->post('est_number'),
              "est_discount" => 0.00,
                "est_currency" => $currency,
                "est_tax" => 0.00,
                "user_id" => $user_id,
                "est_visible" => $estimate_visible,
                "company_id" => $this->Company_id,
                "est_trans_date" => date('Y-m-d H:i:s'),
                "est_start_date" => date('Y-m-d', strtotime($this->input->post('start_date'))),
                "est_notes" => $this->input->post('project'),
                "est_status" => 1
            );


            if ($this->input->post('quote_id')) {
                $quote_id = intval($this->input->post('quote_id'));
                $estimate_id = $this->estimate_m->convert_quote($param, $quote_id);
            } else {
                $estimate_id = $this->estimate_m->add_estimate($param);
            }

            if ($estimate_id) {


                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', 'estimate added successfully');

                $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'estimates',
                    'activity_details' => 'estimates',
                    'activity_module_id' => $estimate_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Agregar nueva estimación",
                    'activity_icon' => 'fa fa-times'
                );

                $this->__activity_tracker($log_params);
                redirect('estimate/preview/' . $estimate_id);
            }


        } else {

            $this->data['title'] = 'Agregar Estimación';
            $this->data['est_ref'] = $this->config->item('invoice')->estimate_prefix . "-" . trans_reference(11,
                    $this->Company_id);
            $this->data['clients'] = $this->estimate_m->get_company_clients($this->Company_id);
            $this->data['projects'] = $this->estimate_m->get_projects();
            $this->data['currencies'] = $this->estimate_m->get_currencies();
            if ($quote_id) {
                $this->load->model('quotation/quotation_m');
                $this->data['quote_id'] = intval($quote_id);
                $this->data['quote'] = $this->quotation_m->get_quote_details(intval($quote_id));
                $this->data['clients'] = $this->estimate_m->get_company_clients($this->Company_id);
                $this->data['projects'] = $this->estimate_m->get_projects();

                $this->data['_EST_PAGE'] = $this->load->view('pages/convert_quote', $this->data, true);
            } else {

                $this->data['_EST_PAGE'] = $this->load->view('pages/add', $this->data, true);
            }

            $this->data['show_datepicker'] = true;
            $this->data['SCRIPT_PAGE'] = true;

        }

        $this->load->view('_main_layout', $this->data);
    }

    /**
     * add estimate activities to database
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->estimate_m->add_log($activity_data);

    }

    /**
     * Line items..
     * @param $est_id
     */
    function update($est_id = null)
    {

        if ($this->input->post()) {

            $estimate_visible = ($this->input->post('estimate_visible') == 'on') ? 1 : 0;

            if ($this->input->post('estimate_currency') == 'default') {
                $client_currency = get_client_details(intval($this->input->post('client')));
                $currency = get_currency_name($client_currency->client_currency);
            } else {
                $currency = get_currency_name($this->input->post('estimate_currency'));
            }

            $param = array(
                "client_id" => $this->input->post('client'),
                "est_code" => $this->input->post('est_number'),
                "est_due_date" => date('Y-m-d', strtotime($this->input->post('end_date'))),
                "est_currency" => $currency,
                "est_visible" => $estimate_visible,
                "est_start_date" => date('Y-m-d', strtotime($this->input->post('start_date'))),
                "est_priority" => $this->input->post('estimate_priority')

            );

            $this->data['title'] = 'Agregar EStimación';
            if ($this->estimate_m->update_estimate($param, $est_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', 'estimate updated successfully');

                $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'estimates',
                    'activity_details' => 'update estimates',
                    'activity_module_id' => $est_id,
                    'activity_status' => "info",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Actualizar estimación ",
                    'activity_icon' => 'fa fa-times'
                );

                $this->__activity_tracker($log_params);
                redirect('estimate/preview/' . $est_id . '/yes');
            }

        } else {

            $this->data['title'] = 'Editar Estimación';
            $this->data['content'] = 'estimate/estimates';
            $this->data['currencies'] = $this->estimate_m->get_currencies();
            $this->data['estimate'] = $this->estimate_m->get_estimate_data($est_id);
            $this->data['clients'] = $this->estimate_m->get_company_clients($this->Company_id);
            $this->data['projects'] = $this->estimate_m->get_projects();
            $this->data['_EST_PAGE'] = $this->load->view('pages/edit_estimate', $this->data, true);
            $this->data['show_datepicker'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);
        }


    }

    /*
     * display estimate data
     */

    /**
     * delete item
     */
    function delete_item()
    {

        if ($this->input->post()) {
            $item_id = intval($this->input->post('detail_id'));
            $est_id = intval($this->input->post('estimate_id'));

            if ($this->estimate_m->delete_item($item_id, $est_id)) {

                $response = array(
                    'success' => 1,
                    'msg_status' => "success",
                    'message' => lang('messages_item_delete_success')
                );

            } else {
                $response = array(
                    'success' => 0,
                    'msg_status' => "error",
                    'message' => lang('messages_item_delete_error')
                );

            }

            echo json_encode($response);
        }
    }

    function preview($est_id = null, $update = null)
    {

        $this->data['title'] = 'Orden de Compra';
        $this->data['content'] = 'estimate/estimates';
        $this->data['taxes'] = $this->estimate_m->get_taxes();
        $this->data['items'] = $this->estimate_m->get_all_items();
        $this->data['families'] = $this->estimate_m->get_all_families();
        $this->data['concepts'] = $this->estimate_m->get_all_concepts();
        if ($update) {

            if ($this->CompanyUserRolePermission_id == 1) {//if admin
                $this->data['estimate'] = $this->estimate_m->get_estimate_data($est_id);
                $this->data['est_items'] = $this->estimate_m->get_estimate_items($est_id);

            } elseif ($this->CompanyUserRolePermission_id == 3) {
                $this->data['estimate'] = $this->estimate_m->get_estimate_data($est_id, $this->CompanyClient_id);
                if (count($this->data['estimate'])) {
                    $this->data['est_items'] = $this->estimate_m->get_estimate_items($est_id, $this->CompanyClient_id);
                } else {

                }
            }

            $this->data['estimate_calculations'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->data['_EST_PAGE'] = $this->load->view('pages/preview', $this->data, true);
        } else {

            $this->data['estimate'] = $this->estimate_m->get_estimate_data($est_id);

            $this->data['estimate_calculations'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->data['_EST_PAGE'] = $this->load->view('pages/create_preview', $this->data, true);

        }
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * print estiamte
     * @param null $estimate_id
     */
    public function print_estimate($estimate_id = null)
    {
        $this->load->helper('pdf');


        $this->data['taxes'] = $this->estimate_m->get_taxes();
        $this->data['items'] = $this->estimate_m->get_all_items($this->Company_id);
        $this->data['estimate'] = $this->estimate_m->get_estimate_data($estimate_id);
        $this->data['est_items'] = $this->estimate_m->get_estimate_items($estimate_id);

        generate_estimate_pdf($estimate_id, $this->data, '', $this->data['estimate']->est_code);
    }

    /**
     * email estimate to client
     * @param null $estimate_id
     */
    function email($estimate_id = null)
    {

        $this->data['taxes'] = $this->estimate_m->get_taxes();
        $this->data['items'] = $this->estimate_m->get_all_items($this->Company_id);
        $this->data['estimate'] = $this->estimate_m->get_estimate_data($estimate_id);
        $this->data['est_items'] = $this->estimate_m->get_estimate_items($estimate_id);
        $this->notifications->send_new_estimate_notification($estimate_id, $this->data);

        $this->db->set('est_emailed', 1);
        $this->db->set('date_email', date("Y-m-d H:i:s"));
        $this->db->where('est_id', $estimate_id)->update('estimate');


        $this->session->set_flashdata('msg_status', 'success');
        $this->session->set_flashdata('message', 'estimate emailed successfully');
        redirect('estimate/preview/' . $estimate_id . '/yes');
    }

    /**
     * update estimate data
     * @param null $est_id
     */
    function update_estimate($est_id = null)
    {


        $this->data['title'] = 'Editar Estimación';
        $this->data['content'] = 'estimate/estimates';
        $this->data['estimate'] = $this->estimate_m->get_estimate_data($est_id);
        $this->data['clients'] = $this->estimate_m->get_company_clients($this->Company_id);
        $this->data['projects'] = $this->estimate_m->get_projects();
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('modal/estimate', $this->data);
    }

    /**
     *
     * Save items to the database...
     */
    function  save()
    {


        if ($this->input->post()) {
            $success = false;
            $est_id = intval($this->input->post('est_id'));

            $items = json_decode($this->input->post('items'));


            foreach ($items as $item) {
                if ($item->item_hours_worked != '' && $item->hourly_rate != '' && $item->item_name != '') {
                    $param = array(
                        "item_id" => $item->item_name,
                        "item_description" => $item->item_description,
                        "details_hours" => $item->item_hours_worked,
                        "details_discount" => $item->item_discount,
                        "details_tax" => $item->item_tax,
                        "details_rate" => $item->hourly_rate,
                        "est_total" => $item->item_subtotal
                    );

                    if ($item->item_id == 0) {
                        $success = $this->estimate_m->add_estimate_item($param, $est_id);
                    } else {
                        $success = $this->estimate_m->update_estimate_item($param, $est_id, $item->item_id);
                    }
                }

            }

            if ($success) {

                $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'estimates',
                    'activity_details' => 'update estimates items',
                    'activity_module_id' => intval($est_id),
                    'activity_status' => "info",
                    'activity_event' => "Actualizar productos de estimación ",
                    'activity_icon' => 'fa fa-times'
                );

                $this->__activity_tracker($log_params);


                $response = array(
                    'success' => 1,
                    'msg_status' => "success",
                    'message' => lang('messages_estimate_item_save_success')
                );
            } else {

                $response = array(
                    'success' => 0,
                    'msg_status' => "error",
                    'message' => "estimate items  not updated"
                );
            }

            echo json_encode($response);

        }
    }

    /**
     * Convert estimate to invoice.
     * @param null $est_id
     *
     */
    function convert($est_id = null)
    {

        $estimate_data = $this->estimate_m->get_estimate_data($est_id);

        $invoice_number = $this->config->item('invoice')->invoice_prefix . "-" . trans_reference(10, $this->Company_id);

        $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;

        $param = array(
            "inv_no" => $invoice_number,
            "client_id" => $estimate_data->client_id,
            "status" => 2,
            "approved" => 1,
            "currency" => $estimate_data->est_currency,
            "invoice_due" => date('Y-m-d', strtotime($estimate_data->est_due_date)),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s'),
            "inv_created_by" => $user_id
        );
        $invoice_id = $this->estimate_m->estimate_to_invoice($param, $est_id);
        if ($invoice_id) {

            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', 'estimate invoiced successfully');

            $log_params = array(
                'activity_user' => $user_id,
                'activity_user_type' => $this->CompanyUser_type,
                'activity_module' => 'estimates',
                'activity_details' => 'Converted EST  to Invoice',
                'activity_module_id' => intval($est_id),
                'activity_status' => "info",
                'activity_date' => date('Y-m-d,H:i:s'),
                'activity_event' => "Estimación convertida a factura ",
                'activity_icon' => 'fa fa-money'
            );

            $this->__activity_tracker($log_params);
            redirect('billing/invoice/' . $invoice_id);
        }

    }

    /**
     * convert estimate to project
     * @param null $est_id
     */
    function convert_to_project($est_id = null)
    {

        //estimate items are tasks...
        $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
        if ($est_id) {
            $est_id = intval($est_id);
            $project_code = $this->config->item('invoice')->project_prefix . "-" . trans_reference(13,
                    $this->Company_id);
            $project_id = $this->estimate_m->estimate_to_project($est_id, $project_code);
        } else {
            $project_id = null;
        }

        if ($project_id) {


            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', 'estimate converted to project successfully');

            $log_params = array(
                'activity_user' => $user_id,
                'activity_user_type' => $this->CompanyUser_type,
                'activity_module' => 'project',
                'activity_details' => 'Converted EST  to Invoice',
                'activity_module_id' => intval($project_id),
                'activity_status' => "info",
                'activity_date' => date('Y-m-d,H:i:s'),
                'activity_event' => "Estimación convertida a proyecto ",
                'activity_icon' => 'fa fa-times'
            );

            $this->__activity_tracker($log_params);
            redirect('project/view/' . $project_id);
        }


    }

    /**
     * update estimate status
     */
    function status()
    {
        $status = $this->uri->segment(3);
        $estimate_id = $this->uri->segment(4);

        if ($this->estimate_m->update_status($status, $estimate_id)) {

            $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $log_params = array(
                'activity_user' => $activity_user,
                'activity_user_type' => $this->CompanyUser_type,
                'activity_module' => 'estimates',
                'activity_details' => 'update estimates status',
                'activity_module_id' => intval($estimate_id),
                'activity_status' => "info",
                'activity_event' => "Actualizar estatus de estimación ",
                'activity_icon' => 'fa-paperclip'
            );

            $this->__activity_tracker($log_params);

            $this->session->set_flashdata('response_status', 'success');
            $this->session->set_flashdata('message', lang('messages_estimate_save_success'));
            redirect('estimate/preview/' . $estimate_id . '/yes');
        }
    }

    /**
     * retrieve chat messages
     */
    function load_messages()
    {

        if ($this->input->post()) {

            $option = substr($this->input->post('receiverUser'), 0, 1);

            header("Content-Type: application/json");
            echo json_encode(get_chat_messages($this->input->post('receiverUser'),
                $this->message_m->getMessages(substr($this->input->post('receiverUser'), 1), $option)));
        }
        exit();
    }

    /**
     * add/send chat message
     */
    function outgoing_message()
    {

        if ($this->input->post()) {

            $option = substr($this->input->post('receiverUser'), 0, 1);

            if ($this->CompanyUser_id > 0) {
                $mSender_data = array(
                    'user_from' => $this->CompanyUser_id,
                    'user_to' => ($option == 'u') ? substr($this->input->post('receiverUser'), 1) : 0,
                    'client_to' => ($option == 'c') ? substr($this->input->post('receiverUser'), 1) : 0,
                    "date_sent" => date('Y-m-d, H:i:s'),
                    'message' => $this->input->post('chatMessage'),
                    "subject" => 'chat message'

                );

            }
            if ($this->CompanyClient_id > 0) {
                $mSender_data = array(
                    'client_from' => $this->CompanyClient_user_id,
                    'user_to' => ($option == 'u') ? substr($this->input->post('receiverUser'), 1) : 0,
                    'client_to' => ($option == 'c') ? substr($this->input->post('receiverUser'), 1) : 0,
                    "date_sent" => date('Y-m-d, H:i:s'),
                    'message' => $this->input->post('chatMessage'),
                    "subject" => 'chat message from client'
                );
            }
            $this->message_m->add_message($mSender_data);
        }

        exit();

    }


    /**
     * view mail compose page
     */
    function send_message()
    {

        $this->data['content'] = 'message/new_message';
        $this->data['title'] = 'Mensaje';
        $this->data['mail_header'] = "Mail Inbox";

        $this->data['users'] = $this->message_m->get_company_users($this->Company_id);
        $this->data['clients'] = $this->message_m->get_company_clients_users($this->Company_id);

        $this->data['_PAGE'] = $this->load->view('emails/add_message', $this->data, true);
        $this->data['message'] = true;
        $this->data['SCRIPT_PAGE'] = true;

        $this->load->view('_main_layout', $this->data);

    }


    /**
     * load inbox page/ read mail
     * @param null $option
     * @param null $message_id
     */
    function read_message($option = null, $message_id = null)
    {

        $this->data['active'] = $option;
        $this->data['content'] = 'message/mail';
        $this->data['title'] = 'Mensaje';
        $this->data['mail_header'] = "Message Details";
        $this->data['message_details'] = $this->message_m->get_mail_messages($message_id);
        $this->data['message_conversations'] = $this->message_m->get_mail_conversations($message_id);
        $this->message_m->update_mail_read($message_id);
        $this->data['message'] = true;

        $this->data['_PAGE'] = $this->load->view('emails/read_message', $this->data, true);
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);


    }

    /**
     * reply to  mail/save a reply
     */
    function send_reply()
    {

        if ($this->input->post()) {

            $reply_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $message_id = intval($this->input->post('message_id'));
            $reply_data = array(
                "message_id" => $message_id,
                "reply_date" => date('Y-m-d,H:i:s'),
                "message" => $this->input->post('reply_message'),
                "replied_by_id" => $reply_user,
                "user_type" => $this->CompanyUser_type
                //"reply_attachment"=>$this->input->post()
            );

            if ($this->message_m->add_message_reply($reply_data, $message_id)) {
                $log_params = array(
                    'activity_user' => $reply_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'mail',
                    'activity_details' => "replied to mail",
                    'activity_module_id' => $message_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Respondido al email" . $message_id,
                    'activity_icon' => 'fa fa-user'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_mail_reply_success'));
                redirect('message/read_message/null/' . $message_id);
            }

        }


    }

    /**
     * log an activity
     * @param array $activity_data
     */


    /**
     * delete message
     */
    function delete_messages()
    {

        if ($this->input->post('id')) {
            foreach ($this->input->post('id') as $id) {
                $this->message_m->mark_deleted($id);
            }
        }

    }

    /**
     * empty trash/bin mail
     */
    function empty_messages()
    {

        if ($this->input->post('id')) {
            foreach ($this->input->post('id') as $id) {
                $this->message_m->complete_delete($id);
            }
        }

    }

    /**
     * send mew mail
     */
    function send()
    {
        //$this->check_action_permission('add_update');
        if ($this->input->post()) {
            $message_id = null;
            $message = $this->input->post('message', true);
            $send_to = $this->input->post('send_to', true);

            foreach ($send_to as $key => $user) {
                $users_to = explode("*", $user);


                $form_data = array(
                    'user_to' => ($users_to[1] == 'u') ? $users_to[0] : 0,
                    'client_to' => ($users_to[1] == 'c') ? $users_to[0] : 0,
                    'user_from' => ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : 0,
                    'client_from' => ($this->CompanyClient_user_id > 0) ? $this->CompanyClient_user_id : 0,
                    "msg_user_type" => $this->CompanyUser_type,
                    "date_sent" => date('Y-m-d, H:i:s'),
                    "status" => 0,
                    'subject' => $this->input->post('subject'),
                    'message' => $this->input->post('summer_message'),
                    //'attachement' => $this->input->post('summer_message'),
                );


                $message_id = $this->message_m->add_message($form_data);

                if ($message_id) {

                    $sender_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                    $log_params = array(
                        'activity_user' => $sender_user,
                        'activity_user_type' => $this->CompanyUser_type,
                        'activity_module' => 'mail',
                        'activity_details' => "New mail",
                        'activity_module_id' => $message_id,
                        'activity_status' => "success",
                        'activity_event' => "Nuevo correo",
                        'activity_date' => date('Y-m-d,H:i:s'),
                        'activity_icon' => 'fa fa-inbox'
                    );
                    $this->__activity_tracker($log_params);
                }

                //$this->_message_notification($user, $message);
            }

            if ($message_id) {//atleast one success

                if (isset($_FILES['document_upload']['name']) && $_FILES['document_upload']['name'] != '') {

                    $this->attach_file('document_upload', $message_id);
                }
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_mail_sent_success'));
                redirect('message/index');
            }

        }

    }

    /**
     * add attachment to an email
     * @param null $document_name
     * @param $message_id
     */
    function attach_file($document_name = null, $message_id)
    {
        $config['upload_path'] = './files/message_attachment/';
        $config['allowed_types'] = 'gif|jpg|png|pdf|doc|docx|xls|ppt|pptx';
        $config['width'] = 150;
        $config['overwrite'] = false;

        $this->load->library('upload');

        $this->upload->initialize($config);

        if (!$this->upload->do_upload($document_name)) {
            echo $this->upload->display_errors();
            exit();
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', '');
            redirect('message/index');
        } else {

            $data = $this->upload->data();
            $data = array(
                'attachment' => $data['file_name'],
                'file_type' => $data['file_ext']
            );
            $this->message_m->add_message_attachment($data, $message_id);

        }

    }

    /**
     * download attachment
     */
    function download()
    {

        $this->load->helper('download');
        $message_id = $this->input->post('message_id');
        $file_name = $this->input->post('attachment');
        if ($file_name == '') {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', lang('messages_attachment_download_unsuccessful'));
            redirect('message/read_message/inbox/' . $message_id);
        }
        if (file_exists('./files/message_attachment/' . $file_name)) {
            $data = file_get_contents('./files/message_attachment/' . $file_name); // Read the file's contents
            force_download($file_name, $data);
        } else {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', lang('messages_attachment_exits_unsuccessful'));
            redirect('message/read_message/inbox/' . $message_id);
        }
    }

    /**
     * get chat users
     */
    function get_user()
    {


        if ($this->input->post()) {
            $this->load->model('user/user_m');
            $option = substr($this->input->post('userId'), 0, 1);

            if ($option == 'u') {

                $user = $this->user_m->get_user_info(substr($this->input->post('userId'), 1));
                $a["fullName"] = $user->emp_name . " " . $user->emp_surname;
                $a["profilePicture"] = $user->emp_avatar;
                $a["profileCover"] = "Test";

            } else {


                $user = $this->user_m->get_cl_user_info(substr($this->input->post('userId'), 1));
                $a["fullName"] = $user->cl_user_name . " " . $user->cl_user_surname;
                $a["profilePicture"] = "Test";
                $a["profileCover"] = "Test";
            }


            header("Content-Type: application/json");
            echo json_encode($a);
        }
        exit();
    }

    /**
     * close chat messages
     */
    function close_chat_windows()
    {

        $mHandleType = $this->input->post("handleType");
        $mWindowUser = $this->input->post("windowUser");
        $mAdditionalContent = $this->input->post("additionalContent");

        if (!isset($_SESSION["chatWindows"])) {
            $_SESSION["chatWindows"] = array();
        }

        $s = $_SESSION["chatWindows"];

        switch ($mHandleType) {
            case "open":
                $s[$mWindowUser] = array("WINDOWUSER" => $mWindowUser, "STATE" => 1);
                break;

            case "close":
                unset($s[$mWindowUser]);
                break;

            case "hide":
                $s[$mWindowUser] = array("WINDOWUSER" => $mWindowUser, "STATE" => $mAdditionalContent == 1 ? 1 : 0);
                break;
        }

        header("Content-Type: application/json");
        echo json_encode(array('outputData' => $s));
        exit();

    }

    /**
     * Load users online..
     */
    function loadUsersList()
    {

        header("Content-Type: application/json");
        echo json_encode(array('htmlContent' => getUsersOnline()));
        exit();

    }

    /**
     * Load notifications
     */
    function messages_notifications()
    {
        header("Content-Type: application/json");
        $message = getMessageNotifications();
        echo json_encode(array('htmlContent' => $message));
        exit();
    }
}
