<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/07/21
 * Time: 09:55 AM
 */
?>
<div class="inbox-nav-bar no-content-padding">

    <h1 class="page-title txt-color-blueDark hidden-tablet"><i
            class="fa fa-fw fa-trash"></i> <?php echo lang('label_email_trash'); ?> &nbsp;

    </h1>

    <div class="inbox-checkbox-triggered">
        <div class="btn-group">
            <a href="javascript:void(0);" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"
               class="emptybutton btn btn-default"><strong><i class="fa fa-trash-o fa-lg"></i></strong></a>
        </div>

    </div>

    <div class="pull-right">
        <input type="text" id="mailbox_search" class="form-control input-sm" placeholder="Find message">
    </div>

</div>
<div class="">

    <table id="mail_trash" class="mail_table table table-hover toggle-arrow-tiny" data-filter="#mailbox_search"
           data-page-size="20">
        <tbody>
        <?php if ($trash_messages) : foreach ($trash_messages as $messages) : ?>

            <?php
            $user_type = ($messages->user_from > 0) ? 1 : 0;
            if ($messages->user_from > 0) {

                $message_details = users_details($messages->user_from, $user_type);
            } else {

                $message_details = users_details($messages->client_from, $user_type);
            }

            ?>
            <tr id="<?php echo $messages->msg_id ?>" class="tr-link"
                data-link="<?php echo site_url('message/read_message/' . $active . "/" . $messages->msg_id); ?>">
                <td class="inbox-table-icon  tr-link-excluded">
                    <div class="checkbox">
                        <input type="checkbox" name="mail_id[]" value="<?php echo $messages->msg_id; ?>"
                               class="i-grey-flat">
                        <label></label>
                    </div>
                </td>

                <td class="inbox-data-from hidden-xs hidden-sm">
                    <div>
                        <img src="<?php echo base_url('files/profile_images/' . $message_details->avatar); ?>"
                             class="avatar img-circle" alt="Avatar">
                        <?php echo $message_details->full_name; ?>
                    </div>

                </td>
                <td class="inbox-data-message">
                    <div>
                        <?php
                        $reply_count = mail_replies_count($messages->msg_id);
                        if ($reply_count > 0) { ?>
                            <span class="label label-info"><?php echo $reply_count; ?></span>
                        <?php } ?>
                        <?php echo $messages->subject; ?>

                    </div>
                </td>
                <td class="inbox-data-attachment hidden-xs">
                    <div>
                        <?php if ($messages->attachment): ?>
                            <a href="javascript:void(0);" rel="tooltip" data-placement="left"
                               class="txt-color-darken"><i class="fa fa-paperclip fa-lg"></i></a>
                        <?php endif; ?>
                    </div>
                </td>
                <td class="inbox-data-date hidden-xs">
                    <div>
                        <span
                            class="time"><?php echo ago(date_to_timestamp($messages->date_sent)) . " " . lang('ago') ?> </span>
                    </div>
                </td>
            </tr>
        <?php endforeach; endif ?>


        </tbody>

        <tfoot>
        <tr>
            <td colspan="4">
                <div class="row">
                    <div class="col-md-4">
                        <span class="mailbox_count_msg"><b class="page_start_row">1</b>-<b class="page_end_row">20</b> of <b
                                class="all_rows">34</b></span>
                    </div>
                    <div class="col-md-8 text-right hide-if-no-paging">
                        <ul class="pagination pagination-sm"></ul>
                    </div>
                </div>
            </td>
        </tr>
        </tfoot>
    </table>


</div>

