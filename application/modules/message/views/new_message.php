<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/05/15
 * Time: 02:27 PM
 */
?>

<!-- BEGIN MAIL APPS NEW MAIL -->
<div class="mail-apps-wrap">
    <div class="the-box bg-success no-border no-margin heading">
        <div class="row">
            <div class="col-sm-6">
                <h1><i class="fa fa-envelope icon-lg icon-circle icon-bordered"></i><?php echo $mail_header; ?></h1>
            </div>
            <!-- /.col-sm-6 -->

        </div>
        <!-- /.row -->
    </div>

    <div class="the-box toolbar no-border no-margin">
        <div class="btn-toolbar" role="toolbar">
            <div class="btn-group">

                <a href="<?php echo site_url('message'); ?>" class="btn btn-danger"><i class="fa fa-arrow-left"></i>
                    Back to inbox</a>

            </div>

        </div>
        <!-- /.btn-toolbar -->
    </div>
    <!-- /.the-box -->


    <div class="the-box no-margin">
        <div class="row">
            <div class="col-sm-12 col-md-12">

                <?php echo (isset($_PAGE)) ? $_PAGE : 'You dont have any new messages'; ?>

            </div>
        </div>
    </div>
    <!-- /.the-box -->


</div><!-- /.mail-apps-wrap -->
<!-- END MAIL APPS NEW MAIL -->

