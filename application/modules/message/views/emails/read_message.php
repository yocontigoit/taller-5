<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/05/20
 * Time: 03:33 PM
 */
?>
<h4><?php echo lang('label_mail_subject'); ?> :<?php echo $message_details->subject; ?></h4>

<?php

$mail_option = $this->uri->segment(3);

if ($mail_option == 'inbox') {
    $user_type = ($message_details->user_from > 0) ? 1 : 0;
    if ($message_details->user_from > 0) {

        $user_details = users_details($message_details->user_from, $user_type);
    } else {

        $user_details = users_details($message_details->client_from, $user_type);
    }
} else {

    $user_type = ($message_details->user_to > 0) ? 1 : 0;
    if ($message_details->user_to > 0) {

        $user_details = users_details($message_details->user_to, $user_type);
    } else {

        $user_details = users_details($message_details->client_to, $user_type);
    }

}

?>
<div class="panel panel-transparent panel-square">
    <div class="panel-heading">
        <h3 class="panel-title">
            <img src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                 class="avatar img-circle" alt="Avatar">
            <strong><?php echo $user_details->full_name; ?></strong>
											<span class="right-content">
												<span
                                                    class="time"><?php echo ago(strtotime($message_details->date_sent)) . " " . lang('ago'); ?></span>
											</span>

        </h3>
    </div>
    <div id="read-mail-example-1" class="collapse in">
        <div class="panel-body">
            <br/>
            <br/>
            <?php echo $message_details->message; ?>

        </div>
        <!-- /.panel-body -->
        <div class="panel-footer">

            <?php if ($mail_option == 'inbox'): ?>
                <p><span><a class="btn btn-xs btn-danger btn-square  pull-right" data-toggle="collapse"
                            href="#read-reply"><?php echo lang('label_reply'); ?><i class="fa fa-reply"></i></a></span>
                </p>
            <?php endif; ?>
            <div id="read-reply" class="collapse">
                <?php echo form_open('message/send_reply', array('role' => 'form')); ?>
                <input class="form-control" type="hidden" readonly name="message_id"
                       value="<?php echo $message_details->msg_id; ?>"/>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="form-group">
                                <textarea class="summernote-sm" name="reply_message"></textarea>
                            </div>
                            <!-- /.input-group -->
                        </div>
                        <!-- /.col-sm-8 -->
                    </div>
                </div>
                <!-- /.panel-body -->
                <button type="submit" class="btn btn-info"><i
                        class="fa fa-cloud-download"></i> <?php echo lang('label_reply'); ?></button>
                <?php echo form_close(); ?>
            </div>
            <!-- /.collapse -->
            <?php if ($message_details->attachment != ''): ?>
                <p><strong><?php echo lang('label_attachment'); ?> :</strong></p>
                <ul class="attachment-list">
                    <li><a href="#fakelink"><?php echo $message_details->attachment; ?></li>
                </ul>
                <?php echo form_open('message/download/', array('role' => 'form')); ?>
                <input class="form-control" type="hidden" readonly name="message_id"
                       value="<?php echo $message_details->msg_id; ?>"/>
                <input class="form-control" type="hidden" readonly name="attachment"
                       value="<?php echo $message_details->attachment; ?>"/>
                <button class="btn btn-info"><i class="fa fa-cloud-download"></i><?php echo lang('label_download'); ?>
                </button>
                <?php echo form_close(); ?>
            <?php endif; ?>
        </div>
        <!-- /.panel-footer -->
    </div>
    <!-- /.collapse in -->


</div><!-- /.panel panel-default -->

<?php if (count($message_conversations)): foreach ($message_conversations as $conversation): ?>

    <?php $user_details = users_details($conversation->replied_by_id, $conversation->user_type); ?>
    <div class="panel panel-transparent panel-square">
        <div class="panel-heading">
            <h3 class="panel-title">
                <a class="block-collapse" data-toggle="collapse"
                   href="#<?php echo $conversation->replies_id . "_reply"; ?>">
                    <img src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                         class="avatar img-circle" alt="Avatar">
                    <strong><?php echo $user_details->full_name; ?></strong>
											<span class="right-content">
												<span class="time"><?php echo $conversation->reply_date; ?></span>
											</span>
                </a>
            </h3>
        </div>
        <div id="<?php echo $conversation->replies_id . "_reply" ?>" class="collapse">
            <div class="panel-body">
                <br/>
                <br/>
                <?php echo $conversation->message; ?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.collapse -->
    </div><!-- /.panel panel-default -->

<?php endforeach; endif; ?>

