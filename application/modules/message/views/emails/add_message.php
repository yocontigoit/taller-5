<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/05/20
 * Time: 03:33 PM
 */
?>
<?php echo form_open_multipart('message/send', array('role' => 'form')); ?>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label><?php echo lang('label_to'); ?></label>
                <select data-placeholder="Select people..." class="form-control chosen-select" multiple tabindex="4"
                        name="send_to[]">
                    <option value="Empty">&nbsp;</option>

                    <optgroup label="<?php echo lang("label_users") ?>">

                        <?php
                        if (count($users)) {
                            foreach ($users as $user): ?>
                                <?php if ($this->CompanyUser_id != $user->user_id): ?>
                                    <option
                                        value="<?php echo $user->user_id . '*u' ?>"><?= ucfirst($user->emp_name . " " . $user->emp_surname) ?></option>
                                <?php endif; endforeach;
                        } ?>

                    </optgroup>

                    <optgroup label="<?php echo lang("label_clients") ?>">

                        <?php
                        if (count($clients)) {
                            foreach ($clients as $client): ?>
                                <?php if ($this->CompanyClient_user_id != $client->cl_user_id): ?>
                                    <option
                                        value="<?php echo $client->cl_user_id . '*c' ?>"><?= ucfirst($client->cl_user_name . " " . $client->cl_user_surname) ?></option>
                                <?php endif; endforeach;
                        } ?>

                    </optgroup>
                </select>
            </div>

            <div class="form-group">
                <label><?php echo lang('label_attachment'); ?></label>

                <div class="input-group">
                    <input type="text" class="form-control" readonly>
                                                            <span class="input-group-btn">
                                                                <span class="btn btn-success btn-file">
                                                                   <?php echo lang('label_browse_files'); ?><input
                                                                        type="file" name="document_upload">
                                                                </span>
                                                            </span>
                </div>
                <!-- /.input-group -->

            </div>
        </div>
        <!-- /.col-sm-4 -->
        <div class="col-sm-8">
            <div class="form-group">
                <label><?php echo lang('label_mail_subject'); ?></label>
                <input type="text" class="form-control input-lg" placeholder="Talk about what..." name="subject"
                       required>
            </div>
            <!-- /.input-group -->
            <div class="form-group">
                <textarea class="summernote-sm" name="summer_message"></textarea>
            </div>
            <!-- /.input-group -->
        </div>
        <!-- /.col-sm-8 -->
    </div><!-- /.row -->
    <div class="form-group">
        <button type="submit" class="btn btn-success"><i class="fa fa-rocket"></i> Send mail</button>
    </div>
<?php echo form_close(); ?>