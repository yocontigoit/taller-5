<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/13
 * Time: 09:34 AM
 */
class Message_M extends CI_Model
{


    /**
     * Get all users for a company
     * @param null $id
     * @return mixed
     */
    function get_company_users($id = null)
    {

        $company_id = intval($id);
        $this->db->from('users');
        $this->db->join('employee', 'employee.emp_id  = users.user_id');
        $this->db->where("company_id", $company_id);

        return $this->db->get()->result();

    }

    /**
     * Get all clients for a company
     * @param null $id
     * @return mixed
     */
    function get_company_clients_users($id = null)
    {

        $company_id = intval($id);
        $this->db->from('client_user');
        $this->db->join('client', 'client.client_id  = client_user.client_id');
        $this->db->where("client.company_id", $company_id);

        return $this->db->get()->result();

    }

    /**
     * get user inbox messages
     * @param null $user_id
     * @return mixed
     */
    function get_user_inbox_messages($user_id = null)
    {

        $user_id = intval($user_id);
        $this->db->from('messages');
        $this->db->where("is_deleted = 0");
        $this->db->where("user_to", $user_id);
        $this->db->order_by('msg_id', 'DESC');

        return $this->db->get()->result();
    }

    /**
     * get client inbox messages
     * @param null $client_id
     * @return mixed
     */
    function get_client_inbox_messages($client_id = null)
    {

        $client_id = intval($client_id);
        $this->db->from('messages');
        $this->db->where("is_deleted = 0");
        $this->db->where("client_to", $client_id);
        $this->db->order_by('msg_id', 'DESC');

        return $this->db->get()->result();

    }

    /**
     * get user deleted messages
     * @param null $user_id
     * @return mixed
     */
    function get_user_deleted_messages($user_id = null)
    {
        $user_id = intval($user_id);
        $this->db->from('messages');
        $this->db->where("is_deleted = 1");
        $this->db->where("user_to", $user_id);

        return $this->db->get()->result();

    }

    /**
     * get client deleted messages
     * @param null $client_id
     * @return mixed
     */
    function get_client_deleted_messages($client_id = null)
    {

        $client_id = intval($client_id);
        $this->db->from('messages');
        $this->db->where("is_deleted = 1");
        $this->db->where("client_to", $client_id);

        return $this->db->get()->result();

    }

    /**
     * add message replies
     * @param array $data
     * @return bool
     */
    function add_message_reply($data = array())
    {

        $success = false;
        $success = $this->db->insert('message_replies', $data);

        return $success;

    }

    /**
     * get mail conversation
     * @param null $msg_id
     * @return mixed
     */
    function get_mail_conversations($msg_id = null)
    {

        $id = intval($msg_id);
        $this->db->from('message_replies');
        $this->db->where("message_id", $id);

        return $this->db->get()->result();

    }

    /**
     * get user semt messages
     * @param null $user_id
     * @return mixed
     */
    function get_user_sent_messages($user_id = null)
    {

        $user_id = intval($user_id);
        $this->db->from('messages');
        $this->db->where("user_from", $user_id);

        return $this->db->get()->result();
    }

    /**
     * get client sent messages
     * @param null $client_id
     * @return mixed
     */
    function get_client_sent_messages($client_id = null)
    {

        $client_id = intval($client_id);
        $this->db->from('messages');
        $this->db->where("client_from", $client_id);

        return $this->db->get()->result();

    }

    /**
     * get mail 
     * @param null $message_id
     * @return mixed
     */
    function get_mail_messages($message_id = null)
    {
        $message_id = intval($message_id);
        $this->db->from('messages');
        $this->db->where("msg_id", $message_id);

        return $this->db->get()->row();

    }

    /**
     * set message read status
     * @param null $message_id
     */
    function update_mail_read($message_id = null)
    {

        $this->db->update('messages', array('status' => 1), array('msg_id' => $message_id));

    }

    /**
     * delete message
     * @param null $message_id
     */
    function mark_deleted($message_id = null)
    {

        $this->db->update('messages', array('is_deleted' => 1), array('msg_id' => $message_id));

    }

    /**
     * remove completely from the system
     * @param null $message_id
     */
    function complete_delete($message_id = null)
    {

        $this->db->where(array('msg_id' => $message_id))->delete('messages');

    }

    /**
     * get messages
     * @param $chat_buddy
     * @param $type_selected
     * @return mixed
     *
     */
    function getMessages($chat_buddy, $type_selected)
    {

        if ($this->CompanyUser_id > 0) {
            $this->db->where('user_from', $this->CompanyUser_id);
            if ($type_selected == 'u') {
                $this->db->where('user_to', $chat_buddy);
                $this->db->or_where('client_from', $chat_buddy);
            } else {
                $this->db->where('client_to', $chat_buddy);
                $this->db->or_where('client_from', $chat_buddy);
            }
            $this->db->where('user_to', $this->CompanyUser_id);

        } elseif ($this->CompanyClient_user_id > 0) {

            $this->db->where('client_from', $this->CompanyClient_user_id);

            if ($type_selected == 'u') {
                $this->db->where('user_to', $chat_buddy);
                $this->db->or_where('user_from', $chat_buddy);
            } else {
                $this->db->where('client_to', $chat_buddy);
                $this->db->or_where('client_from', $chat_buddy);
            }
            $this->db->where('client_to', $this->CompanyClient_user_id);


        }

        $messages = $this->db->get('messages');

        return $messages->result();
    }

    /**
     * add attachment to messages
     * @param array $data
     * @param null $message_id
     */
    function add_message_attachment($data = array(), $message_id = null)
    {

        $this->db->update('messages', $data, array('msg_id' => $message_id));

    }

    /**add mail messages
     * @param array $data
     */
    function add_mail_message($data = array())
    {

        $this->db->insert('messages', $data);
    }

    /**
     * add messages
     * @param array $data
     * @return null
     */
    function add_message($data = array())
    {

        $success = false;
        $success = $this->db->insert('messages', $data);
        $message_id = $this->db->insert_id();

        return ($success) ? $message_id : null;
    }

    /**
     * add message activities
     * @param array $data
     * @return bool
     */
    function add_log($data = array())
    {

        $success = false;
        $success = $this->db->insert("activity_log", $data);

        return $success;
    }

}