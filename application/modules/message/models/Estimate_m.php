<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/12
 * Time: 10:02 PM
 */
class Estimate_M extends CI_Model
{


    /**
     * Get all users for a company
     * @param null $id
     * @return mixed
     */
    function get_all_estimates()
    {
        return $this->db->get('estimate')->result();

    }


    function  get_taxes()
    {

        return $this->db->get('settings_tax')->result();
    }

    function get_currencies()
    {

        return $this->db->get('settings_currency')->result();
    }

    /**
     * Get all users for a company
     * @param null $id
     * @return mixed
     */
    function get_all_items()
    {
        $this->db->from('temp_details');
  return $this->db->get()->result();
    }
    function get_all_families()
    {
        $this->db->from('family');
  return $this->db->get()->result();
    }
    function get_all_concepts()
    {
        $this->db->from('categories');
  return $this->db->get()->result();
    }


    function get_projects()
    {
        $this->db->from('projects');
    return $this->db->get()->result();
    }
    /**
     * get all estimates by client
     * @param $client_id
     * @return mixed
     */
    function get_client_estimates($client_id)
    {

        $this->db->from('estimate');
        $this->db->where("client_id", $client_id);
        $this->db->where("est_visible", 1);

        return $this->db->get()->result();

    }

    /**
     * get clients by company
     * @param null $id
     * @return mixed
     */
    function get_company_clients($id = null)
    {

        $company_id = intval($id);
        $this->db->from('client');
        $this->db->where("company_id", $company_id);

        return $this->db->get()->result();

    }

    /**
     * add estimate
     * @param array $data
     * @return null
     */
    function add_estimate($data = array())
    {

        $success = false;

        $this->db->trans_start();

        $reference_no = explode("-", $data['est_code']);
        $company_id = $data['company_id'];
        $success = $this->db->insert('estimate', $data);
        $estimate_id = $this->db->insert_id();

        if ($success) {
            $success = $this->db->insert('est_details',
                array(
                    'est_id' => intval($estimate_id),
                    'details_date' => date('Y-m-d H:i:s')
                ));

            if ($success) {

                $success = $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
                    array('typeid' => 11, 'company_id' => $company_id));
            }


        }

        $this->db->trans_complete();

        return ($success) ? $estimate_id : null;
    }

    /**
     * convert quotation to estimate
     * @param array $data
     * @param $quote_id
     * @return null
     */
    function convert_quote($data = array(), $quote_id)
    {

        $success = false;

        $this->db->trans_start();

        $reference_no = explode("-", $data['est_code']);
        $company_id = $data['company_id'];
        $success = $this->db->insert('estimate', $data);
        $estimate_id = $this->db->insert_id();

        if ($success) {

            //Update Quote with estimate number/ref..

            $success = $this->db->insert('est_details',
                array(
                    'est_id' => intval($estimate_id),
                    'details_date' => date('Y-m-d H:i:s')
                ));

            if ($success) {

                $success = $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
                    array('typeid' => 11, 'company_id' => $company_id));

                if ($success) {

                    $success = $this->db->update('quotation',
                        array('estimate_reference' => $data['est_code'], 'quote_project_status' => 3, 'estimated' => 1),
                        array('quote_project_id' => $quote_id));

                }
            }

        }

        $this->db->trans_complete();

        return ($success) ? $estimate_id : null;


    }

    /**
     * remove item from estimate
     * @param $item_id
     * @param $est_id
     * @return bool
     */
    function delete_item($item_id, $est_id)
    {

        $success = false;
        $this->db->trans_start();
        $success = $this->db->delete('est_details', array('est_id' => $est_id, 'details_id' => $item_id));
        $this->db->trans_complete();

        return $success;


    }

    /**
     * convert estimate to invoice
     * @param array $data
     * @param $estimate_id
     * @return null
     */
    function estimate_to_invoice($data = array(), $estimate_id)
    {

        $success = false;

        $this->db->trans_start();


        $success = $this->db->insert('invoice', $data);
        $invoice_id = $this->db->insert_id();

        if ($success) {
            $estimate_items = $this->get_estimate_items($estimate_id);

            foreach ($estimate_items as $key => $est_item) {
                $item_name = get_item_name($est_item->item_id);
                $items_data = array(

                    'invoice_id' => $invoice_id,
                    'item_name' => $item_name,
                    'item_desc' => $est_item->item_description,
                    'discount' => $est_item->details_discount,
                    'tax' => $est_item->details_tax,
                    'quantity' => $est_item->details_hours,
                    'price' => $est_item->details_rate
                );
                $success = $this->db->insert('invoice_item', $items_data);
            }

            if ($success) {

                $success = $this->db->update('estimate', array('est_invoiced' => 1, 'est_status' => 5),
                    array('est_id' => $estimate_id));
                if ($success) {
                    $reference_no = explode("-", $data['inv_no']);
                    $success = $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
                        array('typeid' => 10, 'company_id' => $this->Company_id));
                }
            }

        }

        $this->db->trans_complete();

        return ($success) ? $invoice_id : null;

    }

    /**
     * get estimate items
     * @param null $id
     * @return mixed
     */
    function  get_estimate_items($id = null)
    {

        $estimate_id = intval($id);
        $this->db->from('est_details');
        $this->db->where("est_id", $estimate_id);

        return $this->db->get()->result();
    }

    /**
     * convert estimate to project
     * @param null $est_id
     * @param null $project_code
     * @return null
     */
    function estimate_to_project($est_id = null, $project_code = null)
    {

        $success = false;
        $project_id = null;
        $this->db->trans_start();
        if ($est_id && $project_code) {
            $estimate_details = $this->get_estimate_data($est_id);

            $param = array(

                "client_id" => $estimate_details->client_id,
                "project_code" => $project_code,
                "project_title" => $estimate_details->est_code,
                "project_start_date" => $estimate_details->est_start_date,
                "project_due_date" => $estimate_details->est_due_date,
                "project_status" => 1,
                "project_desc" => $estimate_details->est_notes,
                "user_id" => intval($this->CompanyUser_id)
            );

            $success = $this->db->insert('projects', $param);
            $project_id = $this->db->insert_id();
            if ($success) {
                $estimate_items = $this->get_estimate_items($est_id);

                foreach ($estimate_items as $key => $est_item) {

                    $duration = abs($estimate_details->est_start_date - $estimate_details->est_due_date) / 60 / 60 / 24;
                    $param = array(
                        "task_name" => $est_item->item_name,
                        "task_code" => $project_code,
                        "task_level" => 1,
                        "project_id" => intval($project_id),
                        "task_visible" => 1,
                        "task_description" => $est_item->item_description,
                        "task_requirements" => $est_item->item_description,
                        "task_progress" => 0,//"percentage",
                        "task_status" => "STATUS_ACTIVE",
                        "task_start" => strtotime($estimate_details->est_start_date),
                        "task_priority" => 1,
                        "task_duration" => $duration,//2,
                        "task_end" => strtotime($estimate_details->est_due_date),
                        "task_startIsMilestone" => 0,//false,
                        "task_endIsMilestone" => 0,//false,
                        "task_collapsed" => 0,//false,
                        "task_earlyStart" => 0,//2,
                        "task_earlyFinish" => 0,//2,
                        "task_latestStart" => 0,//2,
                        "task_latestFinish" => 0,//2,
                        "task_criticalCost" => 0,//2,
                        "task_isCritical" => 0,//2,
                        "task_hasChild" => 0,//2,
                        "date_added" => date('Y-m-d H:i:s'),
                        "user_id" => intval($this->CompanyUser_id)

                    );
                    $success = $this->db->insert('tasks', $param);
                }

                if ($success) {
                    $reference_no = explode("-", $project_code);
                    $success = $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
                        array('typeid' => 13, 'company_id' => $this->Company_id));
                }

            }
        }
        $this->db->trans_complete();

        return ($success) ? $project_id : null;
    }

    /**
     * get estimate data
     * @param null $id
     * @param null $client_id
     * @return mixed
     */
    function  get_estimate_data($id = null, $client_id = null)
    {

        $estimate_id = intval($id);
        $this->db->from('estimate');
        if ($client_id) {
            $this->db->where("client_id", $client_id);
        }
        $this->db->where("est_id", $estimate_id);

        return $this->db->get()->row();
    }

    /**
     * update estimate
     * @param array $data
     * @param $est_id
     * @return bool
     */
    function update_estimate($data = array(), $est_id)
    {

        $success = false;
        $this->db->where('est_id', $est_id);
        $success = $this->db->update('estimate', $data);

        return $success;

    }

    /**
     * add estimate item
     * @param array $param
     * @param $est_id
     * @return bool
     */
    function add_estimate_item($param = array(), $est_id)
    {

        $success = false;

        $this->db->trans_start();

        $param['est_id'] = intval($est_id);
        $param['details_date'] = date('Y-m-d H:i:s');
        $success = $this->db->insert('est_details', $param);

        $this->db->trans_complete();

        return $success;
    }

    /**
     * update estimate item
     * @param array $param
     * @param $est_id
     * @param $item_id
     * @return bool
     */
    function update_estimate_item($param = array(), $est_id, $item_id)
    {

        $success = false;
        $this->db->where('est_id', intval($est_id));
        $this->db->where('details_id', intval($item_id));
        $success = $this->db->update('est_details', $param);

        return $success;

    }

    /**
     * update estimate status
     * @param null $status
     * @param null $est_id
     * @return bool
     */
    function update_status($status = null, $est_id = null)
    {

        $success = false;
        $this->db->where('est_id', $est_id);
        $success = $this->db->update('estimate', array('est_status' => $status));

        return $success;
    }

    /**
     * add estimate activities
     * @param array $data
     * @return bool
     */
    function add_log($data = array())
    {

        $success = false;
        $success = $this->db->insert("activity_log", $data);

        return $success;
    }

}
