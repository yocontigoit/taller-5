<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/01/12
 * Time: 09:27 AM
 */
?>
<select name="item_name"
        class="form-control required  solsoSelect2 solsoCloneSelect2">
    <option value="" selected>Elegir...</option>

    <?php if (count($items)) : foreach ($items as $item) : ?>
        <option
            value="<?php echo $item->item_id; ?>"> <?php echo $item->item_name; ?> </option>
    <?php endforeach; endif; ?>

</select>
