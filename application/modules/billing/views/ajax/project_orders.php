<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/01/12
 * Time: 09:27 AM
 */
?>
<select name="invoice" class="form-control" id="invoice">
<option selected>Seleccionar factura...</option>
<?php if(count($orders)): foreach( $orders as $order ): ?>
<option value="<?php echo $order->invoice_inv;?>"><?php echo $order->invoice_inv;?></option>
<?php endforeach; endif;?>
</select>

<script type="text/javascript">
$('body').on('change', '#invoice', function(e) {
    e.preventDefault();


    var invoice = $('select[name="invoice"]').val();
    if (invoice == 'Empty') {
        return;
    }
    $.post('<?php echo site_url('billing/ajax_project_diafano_corriente'); ?>', { invoice: invoice }, function(data){
        $('#diafano_corriente').html(data);

    });
});
</script>
