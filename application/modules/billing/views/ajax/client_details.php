<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/01/12
 * Time: 09:27 AM
 */
?>
<input type="hidden" name="client_id" value="<?php echo $client_info->client_id; ?>"/>
<input type="hidden" name="client_type" value="<?php echo $client_info->client_type; ?>">
<h5><strong><?php echo lang('label_estimate_to'); ?>:</strong></h5>
<ul>
    <li><a href="#"><?php echo lang('label_estimate_company'); ?>: <strong
                class="pull-right"><?php echo $client_info->client_name; ?></strong></a></li>
    <li><?php echo lang('label_city'); ?>: <a href="#" class="pull-right"><?php echo $client_info->client_city; ?></a>
    </li>
    <li><?php echo lang('label_phone'); ?>: <strong
            class="pull-right"><?php echo $client_info->client_phone_no; ?></strong></li>


</ul>
