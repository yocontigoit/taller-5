<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/07/08
 * Time: 03:47 PM
 */
?>
<div class="the-box">
    <div class="row">
        <div class="col-md-12">
            <!-- start-->

            <?php echo form_open('billing/create_invoice', array('class' => '', 'id' => 'add-invoice')); ?>

            <div class="full invoice">
                <!-- New invoice template -->
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="row invoice-header">
                            <div class="col-sm-6">
                              <div class="form-group">
                                  <label>Obra</label>

                                      <select data-placeholder="Select client..." class="form-control  chosen-select" tabindex="4"
                                              name="project" id="project">
                                          <option value="Empty">&nbsp;</option>
                                          <optgroup label="Obras">
                                              <?php if (count($projects)):foreach ($projects as $client): ?>
                                                  <option
                                                      value="<?php echo $client->project_id; ?>"><?php echo ucfirst($client->project_title) ?></option>
                                              <?php endforeach; endif; ?>
                                          </optgroup>
                                      </select>
                              </div>

                                <div class="form-group">
                                    <label>Proveedor</label>

                                    <select data-placeholder="Select client..." class="form-control chosen-select"
                                            tabindex="4" name="client" id="client_id" onChange="proveedorOnChange(this)">
                                        <option value="Empty">&nbsp;</option>

                                        <optgroup label="Proveedores y Contratistas">
                                            <?php if (count($clients)):foreach ($clients as $client): if ($client->client_type == 2): ?>
                                                <option
                                                    value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name) ?></option>
                                            <?php endif; endforeach; endif; ?>
                                        </optgroup>
                                    </select>

                                </div>
                                <div class="form-group">
                                            <label for="invDue">Solicitante</label>
                                            <input class="form-control" name="solicitante"></input>

                                        </div>

                                <div class="form-group">
                                    <label>Autorizado por</label>
                      							<select data-placeholder="Select users..."
                      							class="form-control chosen-select" tabindex="4"
                      							name="autorizado" id="autorizado">
                                      <option value="Empty">&nbsp;</option>
                      								<optgroup label="<?php echo lang("label_users") ?>">
                      									<?php if (count($users)) : foreach ($users as $user): ?>
                      										<option value="<?php echo $user->emp_name . " " .$user->emp_surname ?>"><?php echo ucfirst($user->emp_name) . " " . ucfirst($user->emp_surname); ?></option>
                      									<?php endforeach; endif; ?>
                      								</optgroup>
                      							</select>

                                </div>

                            </div>


                            <div class="col-sm-6">
                                <div class="row">

                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="invStart">Fecha de Pedido</label>
                                          <input type="text" class="form-control datepicker" name="invoice_start_date"
                                                 data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" required="required">

                                      </div>
                                  </div>
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label for="invDue">Fecha de Entrega</label>
                                          <input type="text" class="form-control datepicker" name="invoice_due_date"
                                                 data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" required="required">

                                      </div>
                                  </div>

                                  <div class="col-md-12">
                                      <div class="form-group">
                                    <label>Forma de Entrega</label>

                                    <select name="filter" id="filter" data-placeholder="Compra o trabajo" class="form-control  chosen-select" tabindex="4"
                                            name="client">
                                        <option id="0" value="0">Seleccionar...</option>
                                        <option id="1" value="1">Recoger en tienda</option>
                                        <option id="2" value="2">Entregar en Obra</option>
                                    </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="taskPercent">Detalles</label>
                                        <textarea class="form-control" name="details" id="details"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="taskPercent">Observaciones</label>
                                        <textarea class="form-control" name="comment" id="comment"></textarea>
                                    </div>

                                </div>



                                            <input type="hidden" name="invoice_currency" value="1">





                                            <input type="hidden" value="none" name="freq_recurring" class="form-control">






                        </div></div></div>


                        <div class="row">
                            <div class="col-sm-5">
                                <h5><strong><?php echo lang('label_estimate_from'); ?>:</strong></h5>
                                <ul>
                                    <li><?php echo lang('label_company_name'); ?>: <strong><a
                                                href="#"><?php echo $this->config->item('company')->company_name; ?></a></strong>
                                    </li>
                                    <li><?php echo lang('label_location_address_1'); ?>: <strong><a
                                                href="#"><?php echo $this->config->item('company')->company_address_1; ?></a></strong>
                                    </li>

                                    <li><?php echo lang('label_email'); ?>:<strong> <a
                                                href="#"><?php echo $this->config->item('company')->company_email; ?></a></strong>
                                    </li>
                                    <li><?php echo lang('label_phone'); ?>: <strong><a
                                                href="#"><?php echo $this->config->item('company')->company_telephone; ?></a></strong>
                                    </li>
                                </ul>
                            </div>


                            <div class="col-sm-3" id="client_details">


                            </div>
                            <div class="col-sm-3 text-center">

                                <h3 class="bolded tiles-number text-info"><?php echo $invoice_number; ?></h3>

                            </div>
                        </div>

                    </div>
                    <div class="table-responsive">
                        <table id="item_table" class="table table-striped table-bordered">
                            <thead>
                            <tr class="blueheader">
                                <th>Concepto</th>
                                <th><?php echo lang('label_invoice_description'); ?></th>
                                <th>avance</th>
                                <th>Unidades</th>
                                <th>Cantidad</th>
                                <th>Precio Unitario</th>

                                <!-- <th><?php echo lang('label_tax'); ?></th> -->
                                <th><?php echo lang('label_amount'); ?></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody class="solsoParent">
                            <tr style="display: none !important; " class="updatesolsoChild">
                                <input type="hidden" name="item_id" value="0"/>
                                <td style="display: none !important; " class="crt">1</td>

                                <td class="option row" style="text-align:left;" width="20%">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group col-md-12">
                                                    <span class="input-group-addon warning">
                                                    <a titel="Add custom item" data-toggle="zestModal"
                                                       href="<?php echo base_url('item/add_modal_item/'); ?>"><i
                                                            class="fa fa-plus"></i></a>
                                                    </span>


                                                <div id="client_items">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>

                                <td>
                                    <input type="text" name="item_description" class="form-control required solsoEvent"
                                           autocomplete="off">
                                </td>
                                <td>
                                  <div id="project_milestone">

                                  </div>
                                </td>
                                 <td>
                                    <input type="text" name="item_units" class="form-control required solsoEvent"
                                           autocomplete="off">
                                </td>

                                <td>
                                    <input type="text" name="item_hours_worked" class="form-control required solsoEvent"
                                           autocomplete="off">
                                </td>

                                <td>
                                    <input type="text" name="hourly_rate" class="form-control required solsoEvent"
                                           autocomplete="off">

                                    <input type="hidden" name="item_discount" value="0" class="form-control solsoEvent"
                                           autocomplete="off">
                                </td>
                                <td style="display:none">
                                    <input type="text" name="item_tax" class="form-control tax required solsoEvent">
                                </td>
                                <td>
                                    <h4 class="pull-right">
                                        <input name="item_subtotal" value="0.00"
                                               class="item_subtotal no-border text-right form-control" readonly/>

                                    </h4>
                                </td>

                                <td>
                                    <button type="button" class="btn btn-info disabled removeClone"><i
                                            class="fa fa-trash"></i></button>
                                </td>
                            </tr>


                        </table>


                        <table class="table">
                            <tr>
                                <td colspan="3" style="border-right-color: #ddd" width="70%"></td>
                                <td width="12%"><?php echo lang('label_sub_total'); ?><span
                                        class="pull-right"></span></td>
                                <td style="width:15%;" class="text-right"><input name="invoice_subtotal"
                                                                                 id="invoice_subtotal" value=""
                                                                                 class="no-border text-right form-control"
                                                                                 readonly/></td>
                                <td width="3%" class="borderless"></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="border-color:#fff;border-right-color: #ddd" width="70%"></td>
                                <td width="12%">
                                  <select name="tax" class="form-control required solso">
                                    <option value="0">Sin IVA</option>
                                    <option value="16">Con IVA</option>
                                  </select><spanclass="pull-right"></span>
                                </td>
                                <td style="width:15%;" class="text-right"><input name="tax_total" id="tax_total"
                                                                                 value=""
                                                                                 class="no-border text-right form-control"
                                                                                 readonly/></td>
                                <td width="3%" class="borderless"></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="border-color:#fff;border-right-color: #ddd" width="70%"></td>
                                <td width="12%"><strong><?php echo lang('label_total'); ?></strong> <span
                                        class="pull-right"></span></td>
                                <td style="width:15%;" class="text-right"><strong><input name="invoice_total_amount"
                                                                                         id="invoice_total_amount"
                                                                                         value=""
                                                                                         class="no-border text-right form-control"
                                                                                         readonly/></strong></td>
                                <td width="3%" class="borderless"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                        <div class="panel-body">
                            <div class="col-lg-4" id="concept" style="display:none">
                                <a href="javascript:void(0)" id="btn_update_add_item"
                                   class="btn btn-small btn-info"><i class="fa fa-plus"></i>Agregar Concepto</a>
                            </div>
                            <div class="col-lg-4" id="error">
                                <p class="text-danger">Seleccione un proveedor.</p>
                            </div>
                            <div class="col-lg-7 pull-right text-right" style="margin-right: 10%;">
                                <a href="javascript:void(0)" id="save_invoice_items"
                                   class="btn btn-small btn-info">Crear</a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php echo form_close(); ?>


            <!--end-->
        </div>
    </div>
</div>
<script src="https://proyectosinternos.com/Taller5/assets/libs/jquery-2.0.2.min.js" ></script>
<script src="https://proyectosinternos.com/Taller5/assets/js/bootstrap.min.js" ></script>
<link href="https://proyectosinternos.com/Taller5/assets/plugins/summernote/summernote.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="https://proyectosinternos.com/Taller5/assets/plugins/summernote/summernote.min.js"></script>
<script type="text/javascript">
    $('.summernote').summernote({
      height: 50,                 // set editor height
      width: "90%",                 // set editor height
      minHeight: null,             // set minimum height of editor
      maxHeight: null,             // set maximum height of editor
      dialogsInBody: true
    });

    $('.solso').change(function() {
    $('.tax').val(this.value);
    });

    // Start selected title
    function proveedorOnChange(sel) {

      if (sel.value != "Empty"){
          $("#concept").show();
          $("#error").hide();
      }

      if(sel.value == "Empty"){
          $("#concept").hide();
          $("#error").show();
      }

    }
</script>
