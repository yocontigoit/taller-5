<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/25
 * Time: 01:58 PM
 */
?>
<div class="row">
    <?php
    $client_id = ($this->CompanyClient_user_id > 0) ? $this->CompanyClient_id : null;
    $paid_percentage = percentage_paid($client_id);
    $unpaid_percentage = percentage_unpaid($client_id);
    $overdue_percentage = percentage_overdue($client_id);
    $partial_percentage = percentage_partial_paid($client_id);


    ?>
    <div class="col-sm-3 text-center">

        <h4 class="small-heading"><?php echo lang('label_paid'); ?></h4>
										<span class="chart chart-widget-pie billing-chart-1"
                                              data-percent="<?php echo $paid_percentage; ?>">
											<span class="percent"><?php echo $paid_percentage; ?></span>
										</span>
    </div>
    <!-- /.col-sm-3 -->
    <div class="col-sm-3 text-center">
        <h4 class="small-heading"><?php echo lang('label_unpaid'); ?></h4>
										<span class="chart chart-widget-pie billing-chart-2"
                                              data-percent="<?php echo $unpaid_percentage; ?>">
											<span class="percent"><?php echo $unpaid_percentage; ?></span>
										</span>
    </div>
    <!-- /.col-sm-3 -->
    <div class="col-sm-3 text-center">
        <h4 class="small-heading"><?php echo lang('label_overdue'); ?></h4>
										<span class="chart chart-widget-pie billing-chart-3"
                                              data-percent="<?php echo $overdue_percentage; ?>">
											<span class="percent"><?php echo $overdue_percentage; ?></span>
										</span>
    </div>
    <!-- /.col-sm-3 -->
    <div class="col-sm-3 text-center">
        <h4 class="small-heading"><?php echo lang('label_partially_paid'); ?></h4>
										<span class="chart chart-widget-pie billing-chart-3"
                                              data-percent="<?php echo $partial_percentage; ?>">
											<span class="percent"><?php echo $partial_percentage; ?></span>
										</span>
    </div>
    <!-- /.col-sm-3 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div id="overview_graph"></div>

            </div>
        </div>
    </div>

</div>
