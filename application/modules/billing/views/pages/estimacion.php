<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/25
 * Time: 01:58 PM
 */
?>
<div class="the-box">
    <div class="portlet-body">
      <div class="row">
          <div class="col-lg-12">
            <div class="row">
                <!-- <div class="pull-left">
                    <a class="btn btn-info" href="<?php echo base_url('billing/pdf_download_billing/' . $project_id); ?>">Descargar PDF<i class="fa fa-print"></i></a>
                </div> -->
                 <?php echo form_open('billing/add_new_estimate/', array('id' => 'clientForm', 'class' => 'form-horizontal')); ?>
                <div class="col-md-4 col-md-offset-4">
                  <select class="form-control chosen-select" name="finiquito" onChange="finiquitoOnChange(this)">
                    <option value="0">Seleccionar opción</option>
                    <option value="1">Finiquito</option>
                  </select>
                </div>
                </div>
                <div class="row">
                  <div class="col-md-offset-4 no" id="myDIV">
                    <?php if (count($projects)) : foreach ($projects as $project):?>
                    <h1><?php echo $project->project_title ?> #<?php echo $number; ?></h1>
                    <?php $client_user =  $project->client_id; ?>
                  <?php endforeach; endif; ?>
                  </div>
                  <div class="col-md-offset-4 si" style="display:none">
                    <?php if (count($projects)) : foreach ($projects as $project):?>
                    <h1><?php echo $project->project_title ?> Finiquito</h1>
                  <?php endforeach; endif; ?>
                  </div>
            </div>
            <br>
            <div class="table-responsive">
                          <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Mano de Obra</h4></div>
                                <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success items_table_mano">
                      <thead class="the-box light full">
                      <tr>
                        <th></th>
                          <th>
                            Concepto
                          </th>
                          <th>
                            Responsable
                          </th>

                          <th>
                            Presupuestos
                          </th>
                          <th>
                            Importe
                          </th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php
                            $est_pagom = 0;
                            $est_totalm = 0;
                            $est_subtotalm = 0;
                            $qtym = 0;
                            $ivam = 0; $totlam = 0; $pricem = 0; $totalm = 0; $concept = " ";
                        ?>
                      <?php if (count($invoices_items)) : foreach ($invoices_items as $invo) : if($invo->project_id == $project_id && $invo->for_estimate == 1 && $invo->type_pre == 1):?>

                        <tr>
                            <td>
                              <input type="hidden" name="project_id" value="<?php echo $project_id; ?>">
                              <input type="hidden" name="item_project" value="<?php echo $invo->project_id; ?>">
                              <input type="hidden" name="item_client" value="<?php echo $invo->client_id; ?>">
                              <input type="hidden" name="item_invo" value="<?php echo $invo->id; ?>">
                              <input type="hidden" name="item_typre" value="1">
                              <input type="hidden" name="item_invoice" value="<?php echo $invo->inv_no; ?>">
                              <input type="hidden" name="item_number" value="<?php echo $number; ?>">
                              <input type="hidden" name="item_desc" value="0">
                              <?php echo $invo->inv_no ?>
                            </td>
                            <td>
                              <?php if(count($estimaciones)): foreach($estimaciones as $estimacion): if ($invo->id == $estimacion->est_inv): ?>
                                <?php $concept = $estimacion->est_concept; ?>
                              <?php endif; endforeach; endif; ?>
                              <input type="text" class="form-control" name="item_concept" value="<?php echo $concept; ?>">
                            </td>
                            <td>
                              <?php echo client_company($invo->client_id); ?>
                              <input type="hidden" name="item_nameclient" value="<?php echo client_company($invo->client_id); ?>">

                            </td>
                            <td>
                              $ <?php echo invoice_total($invo->id); ?>
                              <input type="hidden" name="item_pre" value="<?php echo invoice_total($invo->id);?>">

                            </td>
                            <td>
                              <?php if($invo->pago_a == 0){ ?>
                              <?php
                                if(count($estimaciones)): foreach($estimaciones as $estimacion): if ($invo->id == $estimacion->est_inv):
                                $est_pagom = $estimacion->est_amount;
                                endif; endforeach; endif;
                                $est_subtotalm = invoice_total($invo->id) - $est_pagom;
                                $totalm += $est_subtotalm;
                               ?>
                              <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                <input type="text" class="form-control amt1" name="item_amount" value="<?php echo $est_subtotalm; ?>">
                              </div>
                            <?php }else{ ?>
                              <?php
                                if(count($estimaciones)): foreach($estimaciones as $estimacion): if ($invo->id == $estimacion->est_inv):
                                $est_pagom = $estimacion->est_amount;
                                endif; endforeach; endif;
                                $est_subtotalm = pago_a_est($invo->client_id,$invo->project_id);
                                $totalm += $est_subtotalm;
                               ?>
                              <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                <input type="text" class="form-control amt1" name="item_amount" value="<?php echo $est_subtotalm; ?>">
                              </div>
                            <?php } ?>
                            </td>
                        </tr>

                      <?  endif; endforeach; endif;?>
                      <tr>
                        <td colspan="4" class="text-right"><strong>Subtotal:</strong></td>
                        <td >
                          <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                              <input type="text" class="form-control" name="totalm" id="inputTotal1" readonly>
                          </div>
                        </td>
                      </tr>
                      </tbody>
                          </table>
                          <br>
                          <br>
                          <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Materiales</h4></div>
                          <table class="items_table_material table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                               <thead class="the-box light full">
                               <tr>
                                   <th></th>
                                   <th>
                                     Proveedor
                                   </th>
                                   <th>Concepto</th>
                                   <th>Descripción</th>
                                   <th>
                                       Importe
                                   </th>

                               </tr>
                               </thead>
                               <tbody>
                                <?php
                                $est_pagoma = 0;
                                $est_totalma = 0;
                                $est_subtotalma = 0;
                                $qtyma = 0;
                                $ivama = 0; $totalma = 0; $pricema = 0; $desc = " "; $concept = " ";
                                ?>
                               <?php if (count($invoices_items)): foreach ($invoices_items as $invo): if($invo->project_id == $project_id && $invo->for_estimate == 1 && $invo->type_pre == 0 && $invo->tax == 0): ?>
                                   <tr>
                                       <td>
                                         <input type="hidden" name="project_id" value="<?php echo $project_id; ?>">
                                         <input type="hidden" name="item_project" value="<?php echo $invo->project_id; ?>">
                                         <input type="hidden" name="item_client" value="<?php echo $invo->client_id; ?>">
                                         <input type="hidden" name="item_typre" value="2">
                                         <input type="hidden" name="item_invo" value="<?php echo $invo->id; ?>">
                                         <input type="hidden" name="item_invoice" value="<?php echo $invo->inv_no; ?>">
                                         <input type="hidden" name="item_number" value="<?php echo $number; ?>">
                                         <?php echo $invo->inv_no;?>
                                       </td>
                                         <td>
                                           <?php echo client_company($invo->client_id); ?>
                                           <input type="hidden" name="item_nameclient" value="<?php echo client_company($invo->client_id); ?>">

                                         </td>
                                       <td>
                                         <?php if(count($estimaciones)): foreach($estimaciones as $estimacion): if ($invo->id == $estimacion->est_inv): ?>
                                           <?php $concept = $estimacion->est_concept; ?>
                                         <?php endif; endforeach; endif; ?>
                                         <input type="text" class="form-control" name="item_concept" value="<?php echo $concept; ?>">
                                       </td>
                                       <td>
                                         <?php if(count($estimaciones)): foreach($estimaciones as $estimacion): if ($invo->id == $estimacion->est_inv): ?>
                                           <?php $desc = $estimacion->est_desc; ?>
                                         <?php endif; endforeach; endif; ?>
                                         <input type="text" class="form-control" name="item_desc" value="<?php echo $desc; ?>">
                                       </td>
                                       <td>
                                         <?php if($invo->pago_a == 0){ ?>
                                         <?php
                                        $est_subtotalma = invoice_total($invo->id); - estimacion_amount($invo->id, $invo->client_id);
                                        $totalma += $est_subtotalma;
                                        ?>
                                         <div class="input-group">
                                           <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                           <input type="text" class="form-control amt2" name="item_amount" value="<?php echo $est_subtotalma; ?>">
                                         </div>
                                       <?php }else{ ?>
                                         <?php
                                        $est_subtotalma = pago_a_est($invo->client_id,$invo->project_id);
                                        $totalma += $est_subtotalma;
                                        ?>
                                         <div class="input-group">
                                           <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                           <input type="text" class="form-control amt2" name="item_amount" value="<?php echo $est_subtotalma; ?>">
                                         </div>
                                         <?php } ?>
                                       </td>
                                   </tr>

                               <? endif; endforeach; endif;  ?>
                               </tbody>
                               <tr>
                                 <td colspan="4" class="text-right"><strong>Subtotal:</strong></td>
                                 <td>
                                   <div class="input-group">
                                     <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                      <input type="text" class="form-control" name="totalma" id="inputTotal2" readonly>
                                  </div>
                                 </td>
                               </tr>
                                   </table>
                                   <br>
                                   <br>
                                   <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Materiales IVA</h4></div>
                                   <table class="items_table_materiali table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                                        <thead class="the-box light full">
                                        <tr>

                                          <th></th>
                                          <th>Proveedor</th>
                                          <th># Factura</th>
                                          <th>
                                              Importe
                                          </th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $est_pagomai = 0;
                                            $est_totalmai = 0;
                                            $est_subtotalmai = 0;
                                            $qtymai = 0;
                                            $ivamai = 0; $totalmai = 0; $pricemai = 0; $invoice_total = 0;
                                        ?>
                                        <?php if (count($invoices_items)) : foreach ($invoices_items as $invo) : if($invo->project_id == $project_id && $invo->for_estimate == 1 && $invo->type_pre == 0 && $invo->tax == 16 ): ?>

                                            <tr>
                                              <td>
                                                <input type="hidden" name="project_id" value="<?php echo $project_id; ?>">
                                                <input type="hidden" name="item_project" value="<?php echo $invo->project_id; ?>">
                                                <input type="hidden" name="item_client" value="<?php echo $invo->client_id; ?>">
                                                <input type="hidden" name="item_typre" value="3">
                                                <input type="hidden" name="item_invo" value="<?php echo $invo->id; ?>">
                                                <input type="hidden" name="item_invoice" value="<?php echo $invo->inv_no;?>">
                                                <input type="hidden" name="item_number" value="<?php echo $number; ?>">
                                                <?php echo $invo->inv_no;?>
                                              </td>
                                              <td>
                                                <?php echo client_company($invo->client_id); ?>
                                                <input type="hidden" name="item_nameclient" value="<?php echo client_company($invo->client_id); ?>">
                                              </td>

                                              <td>
                                                <?php if (count($all_transaction)) : foreach ($all_transaction as $transaction) : if ($transaction->client_id == $invo->client_id):
                                                  echo $transaction->invoice; ?>
                                                  <input type="hidden" name="item_fac" value="<?php echo $transaction->invoice; ?>">
                                                <?php endif; endforeach; endif; ?>
                                              </td>
                                              <td>
                                                <?php if($invo->pago_a == 0){ ?>
                                                <?php
                                               $invoice_total = invoice_total($invo->id);
                                               $est_pagomai = $invoice_total / 1.16;
                                               $est_subtotalmai = $est_pagomai;
                                               $totalmai += $est_subtotalmai;
                                               ?>
                                                <div class="input-group">
                                                  <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                  <input type="text" class="form-control amt3" name="item_amount" value="<?php echo $est_subtotalmai;?>">
                                                </div>
                                              <?php }else{ ?>
                                                    <?php
                                                   $est_subtotalmai = pago_a_diafano_est($invo->client_id,$invo->project_id);
                                                   $totalmai += $est_subtotalmai;
                                                   ?>
                                                    <div class="input-group">
                                                      <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                      <input type="text" class="form-control amt3" name="item_amount" value="<?php echo $est_subtotalmai;?>">
                                                    </div>
                                              <?php } ?>
                                              </td>
                                            </tr>

                                        <? endif; endforeach; endif;  ?>
                                        <tr>
                                          <td colspan="3" class="text-right"><strong>Subtotal:</strong></td>
                                          <td>
                                            <div class="input-group">
                                              <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                <input type="text"  class="form-control" name="totalmai" id="inputTotal3" readonly>
                                            </div>
                                          </td>
                                        </tr>
                                        </tbody>
                                            </table>
                                            <br>
                                            <br>
                                            <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Subcontratos</h4></div>
                                            <table class="items_table_sub table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                                                 <thead class="the-box light full">
                                                 <tr>

                                                     <th></th>
                                                     <th>
                                                       Concepto
                                                     </th>
                                                     <th>
                                                        Proveedor

                                                     </th>

                                                     <th>
                                                         Presupuestos
                                                     </th>
                                                     <th>
                                                         <?php echo lang('label_amount'); ?>

                                                     </th>

                                                 </tr>
                                                 </thead>
                                                 <tbody>
                                                   <?php
                                                     $est_pagosub = 0;
                                                     $est_totalsub = 0;
                                                     $est_subtotalsub = 0;
                                                     $qtysub = 0;
                                                     $ivasub = 0; $totalsub = 0; $pricesub = 0;
                                                    ?>
                                                 <?php if (count($invoices_items)) : foreach ($invoices_items as $invo) : if($invo->project_id == $project_id && $invo->for_estimate == 1 && $invo->type_pre == 2): ?>

                                                     <tr>
                                                         <td>
                                                           <input type="hidden" name="project_id" value="<?php echo $project_id; ?>">
                                                           <input type="hidden" name="item_project" value="<?php echo $invo->project_id; ?>">
                                                           <input type="hidden" name="item_client" value="<?php echo $invo->client_id; ?>">
                                                           <input type="hidden" name="item_typre" value="4">
                                                           <input type="hidden" name="item_invo" value="<?php echo $invo->id; ?>">
                                                           <input type="hidden" name="item_invoice" value="<?php echo $invo->inv_no;?>">
                                                           <input type="hidden" name="item_number" value="<?php echo $number; ?>">
                                                           <?php echo $invo->inv_no; ?>
                                                         </td>
                                                         <td>
                                                           <?php if(count($estimaciones)): foreach($estimaciones as $estimacion): if ($invo->id == $estimacion->est_inv): ?>
                                                             <?php $concept = $estimacion->est_concept; ?>
                                                           <?php endif; endforeach; endif; ?>
                                                          <input type="text" class="form-control" name="item_concept" value="<?php echo $concept; ?>">
                                                         </td>
                                                         <td>
                                                           <?php echo client_company($invo->client_id); ?>
                                                           <input type="hidden" name="item_nameclient" value="<?php echo client_company($invo->client_id); ?>">
                                                         </td>
                                                         <td>
                                                           <?php echo invoice_total($invo->id); ?>
                                                           <input type="hidden" name="item_pre" value="<?php echo invoice_total($invo->id);?>">
                                                         </td>
                                                         <td>
                                                           <?php if($invo->pago_a == 0){ ?>
                                                           <?php
                                                          $est_subtotalsub = invoice_total($invo->id) - estimacion_amount($invo->id, $invo->client_id);
                                                          $totalsub += $est_subtotalsub;
                                                          ?>
                                                           <div class="input-group">
                                                             <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                             <input type="text" class="form-control amt4" name="item_amount" value="<?php echo $est_subtotalsub ?>">
                                                           </div>
                                                         <?php }else{ ?>
                                                           <?php
                                                          $est_subtotalsub = pago_a_est($invo->client_id,$invo->project_id);
                                                          $totalsub += $est_subtotalsub;
                                                          ?>
                                                           <div class="input-group">
                                                             <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                             <input type="text" class="form-control amt4" name="item_amount" value="<?php echo $est_subtotalsub ?>">
                                                           </div>
                                                         <?php } ?>
                                                         </td>
                                                     </tr>

                                                 <?  endif; endforeach; endif;  ?>
                                                 <tr>
                                                   <td colspan="4" class="text-right"><strong>Subtotal:</strong></td>
                                                   <td >
                                                     <div class="input-group">
                                                       <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                        <input type="text" class="form-control amt" name="totalsub" id="inputTotal4" readonly>
                                                    </div>
                                                   </td>
                                                 </tr>
                                                 </tbody>
                                                     </table>
                                                     <br>
                                                     <br>

                      <?php
                          $subtotal =0.00;
                          $subtotal2 = 0;
                          $subtotal1 = 0;
                          $iva =0.00;
                          $iva2 = 0;
                          $honorarios=0.00;
                          $traslados=0.00;
                          $total=0.00;
                          $error = "";
                          $invoice_total1 = 0;
                          $est_pago1 = 0;
                          $totaltotal = 0;
                          $totaltotal2 = 0;
                          if (count($transactions)) : foreach ($transactions as $transaction) : if($transaction->for_estimate == 1):
                            if(count($estimaciones)): foreach($estimaciones as $estimacion): if($transaction->project_id == $estimacion->est_project && $transaction->id == $estimacion->est_inv):
                                $est_pago1 += $estimacion->est_amount;
                            endif; endforeach; endif;
                            $invoice_total1 += invoice_total($transaction->id) / 1.16;
                            $subtotal = $totalm+$totalma+$totalmai+$totalsub;
                            if (count($projects)) : foreach ($projects as $project):
                       ?>
                      <?php


                          $honorario = $project->project_traslado / 100;
                          $honorarios = $honorario * $subtotal;

                          if ($project->project_honorario > 0) {
                              $traslado = $project->project_honorario / 100;
                              $traslados = $traslado * $subtotal;

                          }

                          if ($project->project_cantidad > 0) {
                              $traslado = $project->project_cantidad;
                              $traslados = $traslado;

                          }


                          $total = $subtotal + $honorarios + $traslados;
                      ?>
                      <?php
                            endforeach; endif;
                          endif; endforeach; endif;
                      ?>
                      <?php
                        $tresporciento = 0.00;
                        if (count($transactions)) : foreach ($transactions as $transaction) : if($transaction->for_estimate == '3'):
                          $tresporciento+= invoice_total($transaction->id);
                        endif; endforeach; endif;
                      ?>
                      <div class="col-md-2"></div>
                      <div class="col-md-2"></div>
                      <div class="col-md-2">
                        <select class="form-control" onchange="iva(this)">
                          <option value="0" selected >Selecciona IVA</option>
                          <option value="1">IVA Total</option>
                          <option value="2">IVA Materiales</option>
                        </select>
                        <input type="hidden" name="iva_dif" value="">
                      </div>
                      <div class="col-md-6" style="padding-right:0px;">
                          <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                      <tbody>
                      <tr>

                        <td ><strong>Subtotal:</strong></td>
                        <td >$<?php echo format_currency($subtotal); ?></td>
                      </tr>
                      <tr>
                        <td class="ivamai" style="display:none">
                          <strong>IVA Materiales</strong>
                        </td>
                        <td class="ivamai" style="display:none">
                          <div class="ivamai" style="display:none">
                            <?php
                                 $iva2 = $totalmai * 0.16;
                            ?>
                            $<?php echo format_currency($iva2); ?>
                            <input type="hidden" name="iva2" value="">
                          </div>
                        </td>
                      </tr>
                      <tr>

                        <td >
                          <strong>Traslados, acarreos, herram. y equipo menore, etc:      3% </strong>
                        </td>
                        <td >
                          $<?php echo format_currency($honorarios); ?>
                          <input type="hidden" name="traslado" value="<?php echo $honorarios ?>">
                        </td>
                      </tr>
                      <tr>

                        <td ><strong>Honorarios:</strong></td>
                        <td >$
                          <?php if (count($projects)) : foreach ($projects as $project): ?>
                          <?php if($project->project_honorario and $project->project_cantidad > 0){ ?>
                            <div class="btn-group pull-left">
                                <a class="btn text-green" data-toggle="tooltip" title="Edit Project"
                                   href="<?php echo base_url('project/edit_project/' . $project_id); ?>"><i
                                        class="fa fa-pencil-square-o  fa-2x"></i></a>
                            </div>
                          <?php }else { ?>
                          <?php echo format_currency($traslados); ?>
                          <input type="hidden" name="honorario" value="<?php echo $traslados?>">
                          <?php  } ?>

                          <?php endforeach; endif; ?>
                        </td>
                      </tr>
                      <tr>
                        <td class="ivasub" style="display:none">
                          <strong>IVA Total</strong>
                        </td>
                        <td class="ivasub" style="display:none">
                          <div class="ivasub" style="display:none">
                            <?php
                                  $iva1 = $total * 0.16;
                            ?>
                            $<?php echo format_currency($iva1); ?>
                            <input type="hidden" name="iva1" value="">
                          </div>
                        </td>
                      </tr>
                      <tr>

                        <td ><strong>Total:</strong></td>
                        <td >
                          <div class="ivasub" style="display:none">
                            $<?php
                                $totaltotal = $total + $iva1;
                                echo format_currency($totaltotal);
                            ?>
                            <input type="hidden" name="total1" value="">
                          </div>
                          <div class="ivamai" style="display:none">
                            $<?php
                                $totaltotal2 = $total + $iva2;
                                echo format_currency($subtotal + $honorarios + $traslados);
                            ?>
                            <input type="hidden" name="total2" value="">
                          </div>
                        </td>
                      </tr>
                      <tr style="display:none;">
                        <td><input type="text" name="client_user" value="<?php echo $client_user; ?>"></td>
                      </tr>
                      </tbody>
                  </table>
                  </div>
                </form>
                <div class="pull-right" id="guardar" style="display:none">
                    <a href="javascript:void(0)" id="guardar" class="btn btn-small btn-info">Guardar Estimación</i></a>
                </div>
                <div class="pull-right" id="error">
                    <p class="text-danger">Selecciona tipo de IVA</p>
                </div>
              </div>
          </div>
      </div>

    </div>
</div>
<script src="https://proyectosinternos.com/Taller5/assets/libs/jquery-2.0.2.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // Start save items
        $('#guardar').click(function() {
            var items = [];

            $('.table > tbody >tr').each(function() {
                var row = {};
                $(this).find('input,select,textarea').each(function() {

                    row[$(this).attr('name')] = $(this).val();
                });

                items.push(row);
            });

            var  totalm = $('input[name="totalm"]').val();
            var  totalma = $('input[name="totalma"]').val();
            var  totalmai = $('input[name="totalmai"]').val();
            var  totalsub = $('input[name="totalsub"]').val();
            var  project_id = $('input[name="project_id"]').val();
            var  iva1 = $('input[name="iva1"]').val();
            var  iva2 = $('input[name="iva2"]').val();
            var  traslado = $('input[name="traslado"]').val();
            var  honorario = $('input[name="honorario"]').val();
            var  total1 = $('input[name="total1"]').val();
            var  total2 = $('input[name="total2"]').val();
            var  iva_dif = $('input[name="iva_dif"]').val();
            var client_user = $('input[name="client_user"]').val();

            $.post("<?php echo site_url('billing/save_estimacion'); ?>", {
                    number:'<?php echo $number; ?>',
                    totalm:totalm,
                    totalma:totalma,
                    totalmai:totalmai,
                    totalsub:totalsub,
                    project_id:project_id,
                    iva1:iva1,
                    iva2:iva2,
                    traslado:traslado,
                    honorario:honorario,
                    total1:total1,
                    total2:total2,
                    iva_dif:iva_dif,
                    client_user : client_user,
                    items: JSON.stringify(items)
                },
                function(data) {

                    location.href ="https://proyectosinternos.com/Taller5/billing";

                });
        });
    });
    //Finish save items

    //Start selected iva
    function iva(sel) {

      if (sel.value=="0"){
           $(".Siniva").hide();
           $(".Coniva").hide();
           $("#guardar").hide();
           $("#error").show();
      }

      if (sel.value=="1"){
           $(".ivasub").show();
           $(".ivamai").hide();
           $("#guardar").show();
           $("#error").hide();
           $('input[name="iva1"]').val('<?php echo $iva1; ?>');
           $('input[name="total1"]').val('<?php echo $totaltotal; ?>');
           $('input[name="total2"]').val('0');
           $('input[name="iva2"]').val('0');
           $('input[name="iva_dif"]').val('0');

      }

      if(sel.value=="2"){
           $(".ivamai").show();
           $(".ivasub").hide();
           $("#guardar").show();
           $("#error").hide();
           $('input[name="iva2"]').val('<?php echo $iva2; ?>');
           $('input[name="total2"]').val('<?php echo $totaltotal2; ?>');
           $('input[name="total1"]').val('0');
           $('input[name="iva1"]').val('0');
           $('input[name="iva_dif"]').val('1');

      }

    }
    // Finish selected iva

    // Start selected title
    function finiquitoOnChange(sel) {

      if (sel.value=="0"){
           $(".no").show();
           $(".si").hide();
           document.getElementById("myDIV").style.display = " ";
      }

      if(sel.value=="1"){
           $(".si").show();
           $(".no").hide();
           document.getElementById("myDIV").style.display = "none";
      }

    }
    // Finish selected title

    //suma_mano de obra
    $("#inputTotal1").val(<?php echo $totalm ?>);
    $('.amt1').on('keyup click ', function() {
      var importe_total = 0
      $(".amt1").each(
        function(index, value) {
          if ( $.isNumeric( $(this).val() ) ){
          importe_total = importe_total + eval($(this).val());
          //console.log(importe_total);
          }
        }
      );

          $("#inputTotal1").val(importe_total);
    });
    //suma_materiales
    $("#inputTotal2").val(<?php echo $totalma ?>);
    $('.amt2').on('keyup click ', function() {
      var importe_total = 0
      $(".amt2").each(
        function(index, value) {
          if ( $.isNumeric( $(this).val() ) ){
          importe_total = importe_total + eval($(this).val());
          //console.log(importe_total);
          }
        }
      );

          $("#inputTotal2").val(importe_total);
    });
    //suma_materiales_iva
    $("#inputTotal3").val(<?php echo $totalmai ?>);
    $('.amt3').on('keyup click ', function() {
      var importe_total = 0
      $(".amt3").each(
        function(index, value) {
          if ( $.isNumeric( $(this).val() ) ){
          importe_total = importe_total + eval($(this).val());
          //console.log(importe_total);
          }
        }
      );

          $("#inputTotal3").val(importe_total);
    });
    //suma_subcontrato
    $("#inputTotal4").val(<?php echo $totalsub ?>);
    $('.amt4').on('keyup click ', function() {
      var importe_total = 0
      $(".amt4").each(
        function(index, value) {
          if ( $.isNumeric( $(this).val() ) ){
          importe_total = importe_total + eval($(this).val());
          //console.log(importe_total);
          }
        }
      );
          $("#inputTotal4").val(importe_total);
    });

    // Start selected iva
    function ivaOnChange(sel) {

      if (sel.value != "Empty"){
          $("#concept").show();
          $("#error").hide();
      }

      if(sel.value == "Empty"){
          $("#concept").hide();
          $("#error").show();
      }

    }
</script>
