<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 1/18/2016
 * Time: 7:33 PM
 */
?>
<h3><?php $project = get_project_details($invoice_details->project_id); echo $project->project_title; ?></h3>

<div id="contentarea" class="contentarea">

    <div class="page-tabs">
        <ul class="nav nav-tabs" role="tablist">
            <li class="dropdown pull-right tabdrop hide">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                    <i class="icon-align-justify"></i>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu"></ul>
            </li>
            <li class="active">
                <a id="tabLink1" data-toggle="tab" role="tab" href="#tab1"
                   aria-expanded="true"><?php echo lang('label_summary'); ?></a>
            </li>
            <li class="">
                <a id="tabLink2" data-toggle="tab" role="tab" href="#tab2"
                   aria-expanded="false">Agregar Transacción</a>
            </li>
            <li class="">
                <a id="tabLink3" data-toggle="tab" role="tab" href="#tab3"
                   aria-expanded="false"><?php echo lang('label_transaction'); ?></a>
            </li>
            <li>
                <a id="tabLink4" data-toggle="tab" role="tab" href="#tab4">Editar Orden</a>
            </li>

        </ul>
    </div>

    <div class="tab-content">

        <div id="tab1" class="tab-pane active">

            <?php $invoice_status = get_invoice_status($invoice_details->status);

            if ($invoice_status == "PAGADO") {
                $label_text = "success";
            } else {
                $label_text = "info";
            }

            ?>
            <?php $customer_details = get_client_details($invoice_details->client_id);
            $inv_amount_due = invoice_total($invoice_details->id);
            $inv_amount_balance = invoice_balance($invoice_details->id)

            ?>
            <?php
            if (strtotime($invoice_details->invoice_due) < time() && ($inv_amount_balance > 0 && $inv_amount_balance < $inv_amount_due)) :?>
                <div class="alert alert-warning alert-block fade in alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Atrasado!</strong><?php echo lang('messages_invoice_overdue'); ?>
                </div>
            <?php endif; ?>


            <?php $per = 0; $percal = 0; $total = 0;?>
            <div class="row">
                <div class="col-sm-6">
                    <h6>PARA:</h6>
                    <ul>
                        <?php if($invoice_details->client_id != 0) { ?>
                        <li> Nombre: <a
                                href="<?php echo base_url('client/view/' . $invoice_details->client_id); ?>"><strong><?php echo $customer_details->client_name; ?></strong></a></li>

                                <?php }else{?>

                                 <li>Raya: <a
                                href="#"><strong><?php echo $invoice_details->inv_no; ?></strong></a></li>

                                <?php }?>
                        <li><?php echo lang('label_invoice_date'); ?> :
                            <strong><?php echo $invoice_details->created_at; ?></strong></li>
                        <li>Factura Asignada:
                                <strong><?php echo $invoice_details->invoice_inv; ?></strong></li>
                        <li><?php echo lang('label_due_date'); ?> : <strong
                                style="margin-left: 3%;"><?php echo $invoice_details->invoice_due; ?></strong></li>
                                <!-- <li>Fectura Correspondiente :
                            <strong><?php echo $invoice_details->disc_desc; ?></strong></li> -->
                        <li><?php if ($invoice_details->type == 0){ echo "Monto Cliente :";}
                                  if ($invoice_details->type == 1){echo "Preseupuesto :"; }?> <strong
                                class="text-info">$<?php echo $inv_amount_due; ?></strong></li>
                                   <?php if ($invoice_details->disc_amount != 0) { ?>
                            <li>Descuento : <strong class="text-calm">$<?php echo format_currency($invoice_details->disc_amount); ?></strong></li>
                            <li>Importe de Pago : <strong class="text-calm">$<?php echo format_currency($inv_amount_due - $invoice_details->disc_amount); ?></strong></li>
                        <?php }else{
                          $per =  $invoice_details->disc_amount_per / 100;
                          $percal = $inv_amount_due * $per;
                          $total = $inv_amount_due - $percal;
                          ?>
                          <li>Descuento : <strong class="text-calm">-<?php echo $invoice_details->disc_amount_per; ?>%</strong></li>
                          <li>Importe de Pago : <strong class="text-calm">$<?php echo format_currency($total); ?></strong></li>
                        <?php } ?>
                        <?php if ($invoice_details->paid == 1) { ?>
                            <li>Co : <strong class="text-danger">$0</strong></li>
                        <?php }else { ?>
                            <li>Saldo : <strong class="text-info">$<?php echo $inv_amount_balance - $invoice_details->disc_amount; ?></strong></li>
                        <?php } ?>


                    </ul>
                </div>


                <div class="col-sm-6">
                    <h6>

                            <span style="font-family:Arial;font-size:20px;font-weight:bold;text-transform:uppercase">
                                 <div
                                     class="label label-<?php echo $label_text; ?>"><?php echo $invoice_status; ?></div>

                            </span>
                    </h6>
                    <ul>
                        <?php
                        if ($invoice_status == "PAID" || $invoice_status == "PARTIALLY PAID") {

                            ?>

                            <li><a href="#"><b><?php echo $invoice_details->paid_at; ?></b></a></li>

                            <li><?php echo lang('label_payment_method'); ?> : <strong>
                                    <div
                                        class="label label-info"><?php echo invoice_payment_method($invoice_details->id); ?></div>
                                </strong></li>
                        <?php } ?>
                    </ul>


<a class="btn btn-info btn-sm"  style="font-weight:bold;" data-toggle="zestModal"
                   href="<?php echo base_url('billing/billing/add_discount/' . $invoice_details->id); ?>">
                    Descuento de proveedor
                    <i class="fa fa-plus"></i>
                </a>
                <a class="btn btn-info btn-sm"  style="font-weight:bold;" data-toggle="zestModal"
                                   href="<?php echo base_url('billing/billing/modificar_factura/'.$invoice_details->id); ?>">
                                    Modificar Factura
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>

<?php
$avances_raya = $this->billing_m->get_avances_raya($invoice_details->id);
if(count($avances_raya)): ?><h3>Avances de Raya</h3> <?php foreach($avances_raya as $avance): if($avance->ra_inv_id == $invoice_details->id):?>

<p><?php echo $avance->ra_concepto; ?> : <?php echo $avance->ra_porcentaje; ?>%</p>

<?php
endif;endforeach;endif;?>
                </div>

            </div>

        </div>

        <div id="tab2" class="tab-pane">
            <form method="post" action="<?php echo base_url('billing/add_payment'); ?>">
                <input class="form-control" type="hidden" name="invoice_id"
                       value="<?php echo $invoice_details->id; ?> ">
                <?php
                if ($inv_amount_balance == 0):
                    ?>
                    <div class="infobox">
                        <strong>
                            <span class="title">Estatus de Pago</span>
                        </strong>
                        <br>
                        Este Pedido ya esta marcado como Pagado
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="payment_date"><?php echo lang('label_date'); ?></label>
                            <input type="text" class="form-control datepicker" name="payment_date"
                                   data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                   value="<?php echo date('d-m-Y') ?>">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="payment_amount"><?php echo lang('label_amount'); ?></label>
                            <input class="form-control" type="text" value="" name="payment_amount" required="">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="taskPriority"><?php echo lang('label_payment_method'); ?> </label>

                            <select class="form-control" name="payment_method">
                                <option value="">Ninguno</option>

                                <?php foreach ($this->config->item('payment') as $payment): ?>
                                    <option
                                        value="<?php echo $payment->payment_code; ?>"><?php echo $payment->payment_name; ?></option>
                                <?php endforeach; ?>

                            </select>
                            <span class="help-block">Efectivo, tarjeta de crédito..</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input class="form-control" type="hidden" value="0" name="payment_id" required="">

                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-md-12">
                        <label class="checkbox-inline">
                            <input type="hidden" name="payment_send_mail"
                                   class="ios-switch ios-switch-success ios-switch-lg" value"0" />

                        </label>


                    </div>
                </div>


                <div class="modal-footer">
                    <button class="btn btn-success btn-icon" value="Add Payment" name="submit" type="input">
                        <i class="fa fa-check-square-o"></i>
                        <?php echo lang('label_add_payment'); ?>
                    </button>
                    <button class="btn btn-default btn-icon" type="button">
                        <i class="fa fa-times-circle-o"></i>
                        <?php echo lang('form_button_cancel'); ?>
                    </button>
                </div>
            </form>

        </div>
        <div id="tab3" class="tab-pane">

            <div class="table-responsive">
                <table
                    class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
                    id="">
                    <thead class="the-box dark full">
                    <tr>
                      <th>Transacción</th>
                        <th><?php echo lang('label_date'); ?></th>
                        <th> <?php echo lang('label_payment_method'); ?></th>

                        <th><?php echo lang('label_amount'); ?></th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php if (count($invoice_transactions)): foreach ($invoice_transactions as $transaction): if($transaction->payment_method == 0){}else{?>
                        <tr>
                          <td><?php echo $transaction->id; ?></td>
                            <td><?php echo substr($invoice_details->created_at,0, -8); ?></td>
                            <td><?php echo payment_method($transaction->payment_method); ?></td>
                            <td><?php echo format_currency($transaction->amount); ?></td>


                        </tr>
                    <?php } endforeach; ?>

                    <?php endif; ?>
                    </tbody>
                </table>
            </div>

        </div>



        <div id="tab4" class="tab-pane">
            <div class="btn-group pull-right">
                <a class="btn btn-info btn-sm"   data-toggle="zestModal"
                   href="<?php echo base_url('billing/billing/add_orden/' . $invoice_details->id); ?>">
                    Agregar Concepto
                    <i class="fa fa-plus"></i>
                </a>
            </div>
            <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success">
              <thead class="the-box">
                <tr>
                  <th>Concepto</th>
                  <th>Descripción</th>
                  <th>Avance</th>
                  <th>P.U.</th>
                  <th>Cantidad</th>
                  <th>Monto</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <?php if (count($invoice_items)) : foreach ($invoice_items as $invoice_item) : ?>

              <tbody>
                  <tr>
                    <form method="post" action="<?php echo base_url('billing/billing/update_orden'); ?>">

                      <td style="width:213px">
                        <?php echo $invoice_item->item_name; ?>
                      </td>
                      <td style="width:300px">
                        <?php echo $invoice_item->item_desc; ?>
                      </td>
                      <td>
                        <?php echo $invoice_item->item_avance; ?>
                      </td>
                      <td align="center">
                        $<?php echo $invoice_item->price; ?>
                      </td>
                      <td align="center">
                        <?php echo $invoice_item->quantity; ?>
                      </td>
                      <td align="center">
                        $<?php $total = $invoice_item->price * $invoice_item->quantity;
                        echo $total; ?>
                      </td>
                      <td align="center">
                        <a class="btn btn-info btn-sm" data-toggle="zestModal"
                               href="<?php echo base_url('billing/billing/edit_orden/' . $invoice_item->id . '/' . $invoice_item->invoice_id . '/' . $invoice_item->project_id); ?>"
                               title=""><i class="fa fa-pencil-square-o"></i> Edit</a>
                      </td>
                    </fotm>
                  </tr>
              </tbody>
              <?php endforeach; endif; ?>
            </table>
            <!-- <form method="post" action="<?php echo base_url('billing/add_note'); ?>">
                <input class="form-control" type="hidden" name="invoice_id"
                       value="<?php echo $invoice_details->id; ?> ">

                <div class="form-group">
                    <label for="notes"><?php echo lang('label_notes'); ?></label>
                    <textarea class="form-control" rows="5" name="inv_note"
                              required=""><?php echo $invoice_details->notes; ?></textarea>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-success btn-icon" value="Add Payment" name="submit" type="input">
                        <i class="fa fa-check-square-o"></i>
                        <?php echo lang('label_add_note'); ?>
                    </button>
                </div>
            </form> -->
        </div>

    </div>

    <h2>Detalles de Orden</h2>

    <form action="" method="post">

        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
                   id="sample_2">
                <thead class="the-box dark full">
                <tr>

                    <th>Concepto</th>
                    <th>Descripción</th>
                    <th>Avance</th>
                    <th width="120">P.U.</th>
                    <th width="120"><?php echo lang('label_quantity'); ?></th>
                    <th width="120"><?php echo lang('label_amount'); ?></th>


                </tr>

                </thead>
                <tbody>
                <?php if (count($invoice_items)) : foreach ($invoice_items as $invoice_item) :

if ($invoice_item->price > 0) :
                  ?>
                    <tr>

                        <td><?php echo $invoice_item->item_name; ?>

                        </td>
                        <td><?php echo $invoice_item->item_desc; ?>
                        </td>
                        <td>
                          <?php echo $invoice_item->item_avance ?>
                        </td>
                        <td nowrap="" align="center">
                            $<?php echo $invoice_item->price; ?>
                        </td>
                        <td nowrap="" align="center">
                          <?php echo $invoice_item->quantity; ?>
                        </td>
                        <td align="center">
                          <?php $total = $invoice_item->price * $invoice_item->quantity;
                          echo '$'.$total; ?>
                        </td>

                    </tr>

                <?php endif; endforeach; endif; ?>
                <tr>
                    <td style="text-align:right;" colspan="5">
                        <strong><?php echo lang('label_sub_total'); ?>:</strong>
                    </td>
                    <td style="text-align:center;">
                        <strong><?php echo ($invoice_details->currency) ? get_currency_symbol_by_name($invoice_details->currency) . format_amount(invoice_sub_total_total($invoice_details->id)) : format_currency(invoice_sub_total_total($invoice_details->id)); ?></strong>
                    </td>


                </tr>
                <tr>
                    <td style="text-align:right;" colspan="5"><?php echo lang('label_tax'); ?>:</td>
                    <td style="text-align:center;"><?php echo ($invoice_details->currency) ? get_currency_symbol_by_name($invoice_details->currency) . format_amount(invoice_tax_total($invoice_details->id)) : format_currency(invoice_tax_total($invoice_details->id)); ?></td>


                </tr>
                <tr>

                    <td style="text-align:right;" colspan="5"><?php echo lang('label_total'); ?>:</td>
                    <td style="text-align:center;"><?php echo ($invoice_details->currency) ? get_currency_symbol_by_name($invoice_details->currency) . format_amount(invoice_total($invoice_details->id)) : format_currency(invoice_total($invoice_details->id)); ?></td>


                </tr>
                <tr>

                    <td style="text-align:right;" colspan="5">Saldo:</td>
                    <td style="text-align:center;">$<?php echo format_amount($inv_amount_balance); ?></td>


                </tr>
                </tbody>
            </table>
        </div>
    </form>

</div>
