<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/25
 * Time: 01:58 PM
 */
?>
<div class="the-box">
    <div class="portlet-body">
      <div class="row">
          <div class="col-lg-12">
            <div class="row">
                <div class="pull-left">
                    <a class="btn btn-info" href="<?php echo base_url('billing/pdf_download_billing/'.$project_id.'/'.$est_num); ?>">Descargar PDF<i class="fa fa-print"></i></a>
                </div>
                 <?php echo form_open('billing/add_new_estimate/', array('id' => 'clientForm', 'class' => 'form-horizontal')); ?>
                <!-- <div class="pull-right">
                    <a href="javascript:void(0)" id="guardar" class="btn btn-small btn-info">Guardar Estimado<i class="fa fa-print"></i></a>
                </div> -->
                <div class="col-md-4 col-md-offset-2">
                  <!-- <select class="form-control chosen-select" name="finiquito" onChange="finiquitoOnChange(this)">
                    <option value="0">Selected option</option>
                    <option value="1">Finiquito</option>
                  </select> -->
                </div>
                </div>
                <div class="row">
                  <div class="col-md-offset-4 no" id="myDIV">
                    <?php if (count($projects)) : foreach ($projects as $project):?>
                    <h1><?php echo $project->project_title ?> #<?php echo $est_num; ?></h1>
                  <?php endforeach; endif; ?>
                  </div>
                  <!-- <div class="col-md-offset-4 si" style="display:none">
                    <?php if (count($projects)) : foreach ($projects as $project):?>
                    <h1><?php echo $project->project_title ?> Finiquito</h1>
                  <?php endforeach; endif; ?>
                  </div> -->
            </div>
            <br>
            <div class="table-responsive">
                          <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Mano de Obra</h4></div>
                                <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success items_table_mano">
                      <thead class="the-box light full">
                      <tr>
                        <th></th>
                          <th>
                            Concepto
                          </th>
                          <th>
                            Responsable
                          </th>

                          <th>
                            Presupuestos
                          </th>
                          <th>
                            Importe
                          </th>
                      </tr>
                      </thead>
                      <tbody>
                        <?php
                            $est_pagom = 0;
                            $est_totalm = 0;
                            $est_subtotalm = 0;
                            $qtym = 0;
                            $ivam = 0; $totlam = 0; $pricem = 0; $totalm = 0;
                        ?>
                      <?php if (count($invoices_items)) : foreach ($invoices_items as $invo) : if($invo->est_project == $project_id && $invo->est_num == $est_num && $invo->est_typre == 1):?>

                        <tr>
                            <td>
                              <?php echo $invo->est_invoice; ?>
                            </td>
                            <td>
                              <?php echo $invo->est_concept; ?>
                            </td>
                            <td>
                              <?php echo client_company($invo->est_client); ?>
                            </td>
                            <td>
                              $ <?php echo invoice_total($invo->est_inv); ?>
                            </td>
                            <td>
                              <?php $totalm += $invo->est_amount; ?>
                              $<?php echo format_currency($invo->est_amount); ?>
                            </td>
                        </tr>

                      <?  endif; endforeach; endif;?>
                      <tr>
                        <td colspan="4" class="text-right"><strong>Subtotal:</strong></td>
                        <td >
                          $<?php echo format_currency($totalm); ?>
                        </td>
                      </tr>
                      </tbody>
                          </table>
                          <br>
                          <br>
                          <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Materiales</h4></div>
                          <table class="items_table_material table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                               <thead class="the-box light full">
                               <tr>
                                   <th></th>
                                   <th>
                                     Proveedor
                                   </th>
                                   <th>Concepto</th>
                                   <th>Descripción</th>
                                   <th>
                                       Importe
                                   </th>

                               </tr>
                               </thead>
                               <tbody>
                                <?php
                                $est_pagoma = 0;
                                $est_totalma = 0;
                                $est_subtotalma = 0;
                                $qtyma = 0;
                                $ivama = 0; $totalma = 0; $pricema = 0; $desc = " "; $concept = " ";
                                ?>
                               <?php if (count($invoices_items)): foreach ($invoices_items as $invo): if($invo->est_project == $project_id && $invo->est_num == $est_num && $invo->est_typre == 2): ?>
                                   <tr>
                                       <td>
                                         <?php echo $invo->est_invoice;?>
                                       </td>
                                         <td>
                                           <?php echo client_company($invo->est_client); ?>
                                         </td>
                                       <td>
                                         <?php echo $invo->est_concept; ?>
                                       </td>
                                       <td>
                                        <?php echo $invo->est_desc; ?>
                                       </td>
                                       <td>
                                         <?php
                                          $totalma += $invo->est_amount;
                                         ?>
                                        $<?php echo format_currency($invo->est_amount); ?>
                                       </td>
                                   </tr>

                               <? endif; endforeach; endif;  ?>
                               </tbody>
                               <tr>
                                 <td colspan="4" class="text-right"><strong>Subtotal:</strong></td>
                                 <td>
                                   $<?php echo format_currency($totalma); ?>
                                 </td>
                               </tr>
                                   </table>
                                   <br>
                                   <br>
                                   <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Materiales IVA</h4></div>
                                   <table class="items_table_materiali table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                                        <thead class="the-box light full">
                                        <tr>

                                          <th></th>
                                          <th>Proveedor</th>
                                          <th># Factura</th>
                                          <th>
                                              Importe
                                          </th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $est_pagomai = 0;
                                            $est_totalmai = 0;
                                            $est_subtotalmai = 0;
                                            $qtymai = 0;
                                            $ivamai = 0; $totalmai = 0; $pricemai = 0;
                                        ?>
                                        <?php if (count($invoices_items)) : foreach ($invoices_items as $invo) : if($invo->est_project == $project_id && $invo->est_typre == 3 && $invo->est_num == $est_num): ?>

                                            <tr>
                                              <td>
                                                <?php echo $invo->est_invoice;?>
                                              </td>
                                              <td>
                                                <?php echo client_company($invo->est_client); ?>
                                              </td>

                                              <td>
                                                <?php echo $invo->est_fac; ?>
                                              </td>
                                              <td>
                                                <?php
                                                  $totalmai += $invo->est_amount;
                                                ?>
                                                $<?php echo format_currency($invo->est_amount); ?>
                                              </td>
                                            </tr>

                                        <? endif; endforeach; endif;  ?>
                                        <tr>
                                          <td colspan="3" class="text-right"><strong>Subtotal:</strong></td>
                                          <td>
                                            $<?php echo format_currency($totalmai);?>
                                            <input type="hidden" class="form-control" name="totalmai" value="<?php echo $totalmai; ?>">
                                          </td>
                                        </tr>
                                        </tbody>
                                            </table>
                                            <br>
                                            <br>
                                            <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Subcontratos</h4></div>
                                            <table class="items_table_sub table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                                                 <thead class="the-box light full">
                                                 <tr>

                                                     <th></th>
                                                     <th>
                                                       Concepto
                                                     </th>
                                                     <th>
                                                        Proveedor

                                                     </th>

                                                     <th>
                                                         Presupuestos
                                                     </th>
                                                     <th>
                                                         <?php echo lang('label_amount'); ?>

                                                     </th>

                                                 </tr>
                                                 </thead>
                                                 <tbody>
                                                   <?php
                                                     $est_pagosub = 0;
                                                     $est_totalsub = 0;
                                                     $est_subtotalsub = 0;
                                                     $qtysub = 0;
                                                     $ivasub = 0; $totalsub = 0; $pricesub = 0;
                                                    ?>
                                                 <?php if (count($invoices_items)) : foreach ($invoices_items as $invo) : if($invo->est_project == $project_id && $invo->est_typre == 4 && $invo->est_num == $est_num): ?>

                                                     <tr>
                                                         <td>
                                                           <?php echo $invo->est_invoice; ?>
                                                         </td>
                                                         <td>
                                                          <?php echo $invo->est_concept; ?>
                                                         </td>
                                                         <td>
                                                           <?php echo client_company($invo->est_client); ?>
                                                         </td>
                                                         <td>
                                                           <?php echo invoice_total($invo->est_inv); ?>
                                                         </td>
                                                         <td>
                                                           <?php
                                                            $totalsub += $invo->est_amount;
                                                           ?>
                                                           $<?php echo format_currency($invo->est_amount);?>
                                                         </td>
                                                     </tr>

                                                 <?  endif; endforeach; endif;  ?>
                                                 <tr>
                                                   <td colspan="4" class="text-right"><strong>Subtotal:</strong></td>
                                                   <td >
                                                     $<?php echo format_currency($totalsub);?>
                                                   </td>
                                                 </tr>
                                                 </tbody>
                                                     </table>
                                                     <br>
                                                     <br>

                      <?php
                          $subtotal =0.00;
                          $subtotal2 = 0;
                          $subtotal1 = 0;
                          $iva =0.00;
                          $iva2 = 0;
                          $ivama = 0;
                          $honorarios=0.00;
                          $traslados=0.00;
                          $total=0.00;
                          $error = "";
                          $invoice_total1 = 0;
                          $est_pago1 = 0;
                          $totaltotal = 0;
                          $totaltotal2 = 0;
                          $tresporciento = 0.00;
                          ?>
                          <?php if(count($totals)): foreach($totals as $total): if($total->project_id == $project_id):
                              $subtotal = $total->mano_obra + $total->materiales + $total->materiales_iva + $total->subcont;
                              $ivama = $total->iva;
                              $traslados = $total->traslados;
                              $honorarios = $total->honorarios;
                             endif; endforeach; endif;
                            $totaltotal = $subtotal;
                            $total = $subtotal;
                          ?>

                      <div class="col-md-6"></div>
                      <div class="col-md-6" style="padding-right:0px;">
                          <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                      <tbody>
                      <tr>

                        <td ><strong>Subtotal:</strong></td>
                        <td >$<?php echo format_currency($subtotal); ?></td>
                      </tr>
                      <tr>
                        <td>
                          <strong>IVA Materiales:</strong>
                        </td>
                        <td>
                          <?php echo format_currency($ivama);?>
                        </td>
                      </tr>
                      <tr>

                        <td>
                          <strong>TRASLADOS, ACARREOS, HERRAM Y EQUIPO MENOR, ETC:      3% </strong>
                        </td>
                        <td >
                          $<?php echo format_currency($traslados); ?>
                        </td>
                      </tr>
                      <tr>

                        <td ><strong>Honorarios:</strong></td>
                        <td >
                          $<?php echo format_currency($honorarios); ?>

                        </td>
                      </tr>
                      <tr>

                        <td ><strong>Total:</strong></td>
                        <td >
                          <div>
                            <?php
                                $totaltotal = $subtotal + $ivama + $traslados + $honorarios;
                                echo format_currency($totaltotal);
                            ?>
                          </div>
                        </td>
                      </tr>
                      </tbody>
                  </table>
                  </div>
                </form>

                <div style="background:#d3d3d3;width: 100%;height: 45px;margin-top: 196px;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Resumen</h4></div>
               <table class="items_table_sub table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                    <thead class="the-box light full">
                    <tr>
                        <th>
                          Concepto
                        </th>
                        <th>
                          Importe
                        </th>
                        <th>
                          Anterior
                        </th>
                        <th>
                          Acumulado
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                        $anteriormano = 0; $manototal = 0; $materialestotal = 0; $anteriormateriales = 0; $acumualadomateriales = 0;
                        $materialesivatotal = 0; $anteriormaterialesiva = 0; $acumualadomaterialesiva = 0; $subconttotal = 0;
                        $anteriorsubcont = 0; $acumualadosubcont = 0; $honorariostotal = 0; $anteriorhonorarios = 0;
                        $acumualadohonorarios = 0; $trasladostotal = 0; $anteriortraslados = 0; $acumualadotraslados = 0;
                        $importeiva = 0; $anterioiva = 0; $acumuladoiva = 0; $subtotalimporte = 0; $subtotalacumulado = 0; $acumualadomano = 0;
                        $espaciod = 0; $cuentac = 0; $anti = 0;
                      ?>
                      <?php if(count($totals)): foreach($totals as $total): if($total->project_id == $project_id):  ?>
                        <tr>
                            <td>
                              I. Mano de obra
                            </td>
                            <td>
                              <?php if(count($totals)): foreach($totals as $totalunico): if($totalunico->project_id == $project_id && $totalunico->id_amount == $est_num):  ?>
                                <?php $manototal = $totalunico->mano_obra; ?>
                               $<?php echo format_currency($manototal); ?>
                              <?php endif; endforeach; endif; ?>
                            </td>
                            <td>
                                <?php if($est_num == 1){ ?>
                                  $<?php echo format_currency(0); ?>
                                <?php }elseif($est_num != 1){ ?>
                                  <?php $anteriormano += $total->mano_obra; ?>
                                 $<?php echo format_currency(mano_obra($total->project_id,$est_numA)); ?>
                               <?php } ?>
                            </td>
                            <td>
                              <?php $acumualadomano = $manototal + $anteriormano; ?>
                               $<?php echo format_currency(mano_obra_a($total->project_id)); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              II. Materiales
                            </td>
                            <td>
                              <?php if(count($totals)): foreach($totals as $totalunico): if($totalunico->project_id == $project_id && $totalunico->id_amount == $est_num):  ?>
                                <?php $materialestotal = $totalunico->materiales; ?>
                               $<?php echo format_currency($materialestotal); ?>
                             <?php endif; endforeach; endif; ?>
                            </td>
                            <td>
                              <?php if($est_num == 1){ ?>
                                  $<?php echo format_currency(0); ?>
                              <?php }elseif($est_num != 1){ ?>
                                  <?php $anteriormateriales += $total->materiales; ?>
                                 $<?php echo format_currency(materiales($total->project_id,$est_numA)); ?>
                               <?php } ?>
                            </td>
                            <td>
                              <?php $acumualadomateriales = $materialestotal + $anteriormateriales; ?>
                               $<?php echo format_currency(materiales_a($total->project_id)); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              III. Materiales IVA
                            </td>
                            <td>
                              <?php if(count($totals)): foreach($totals as $totalunico): if($totalunico->project_id == $project_id && $totalunico->id_amount == $est_num):  ?>
                                <?php $materialesivatotal = $totalunico->materiales_iva; ?>
                               $<?php echo format_currency($materialesivatotal); ?>
                             <?php endif; endforeach; endif; ?>
                            </td>
                            <td>
                              <?php if($est_num == 1){ ?>
                                  $<?php echo format_currency(0); ?>
                              <?php }elseif($est_num != 1){ ?>
                                <?php $anteriormaterialesiva = materiales_iva($total->project_id,$est_numA); ?>
                               $<?php echo format_currency(materiales_iva($total->project_id,$est_numA)); ?>
                              <?php } ?>
                            </td>
                            <td>
                              <?php $acumualadomaterialesiva = materiales_iva_a($total->project_id); ?>
                               $<?php echo format_currency(materiales_iva_a($total->project_id)); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              IV. Subcontratos
                            </td>
                            <td>
                              <?php if(count($totals)): foreach($totals as $totalunico): if($totalunico->project_id == $project_id && $totalunico->id_amount == $est_num):  ?>
                                <?php $subconttotal = $totalunico->subcont; ?>
                               $<?php echo format_currency($subconttotal); ?>
                              <?php endif; endforeach; endif; ?>
                            </td>
                            <td>
                              <?php if($est_num == 1){ ?>
                                  $<?php echo format_currency(0); ?>
                              <?php }elseif($est_num != 1){ ?>
                                <?php $anteriorsubcont += $total->subcont; ?>
                               $<?php echo format_currency(subcontrato($total->project_id,$est_numA)); ?>
                              <?php } ?>
                            </td>
                            <td>
                              <?php $acumualadosubcont = subcontrato_a($total->project_id); ?>
                               $<?php echo format_currency(subcontrato_a($total->project_id)); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              V. Honorarios
                            </td>
                            <td>
                              <?php if(count($totals)): foreach($totals as $totalunico): if($totalunico->project_id == $project_id && $totalunico->id_amount == $est_num):  ?>
                                <?php $honorariostotal = $totalunico->honorarios; ?>
                               $<?php echo format_currency($honorariostotal); ?>
                             <?php endif; endforeach; endif; ?>
                            </td>
                            <td>
                              <?php if($total->id_amount == 1){ ?>
                                  $<?php echo format_currency(0); ?>
                              <?php }else{ ?>
                                <?php $anteriorhonorarios += $total->honorarios; ?>
                               $<?php echo format_currency(honorarios($total->project_id,$est_numA)); ?>
                              <?php } ?>
                            </td>
                            <td>
                              <?php $acumualadohonorarios = $honorariostotal + $anteriorhonorarios; ?>
                               $<?php echo format_currency(honorarios_a($total->project_id)); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              VI. Traslados, Acarreos, Etc.
                            </td>
                            <td>
                              <?php if(count($totals)): foreach($totals as $totalunico): if($totalunico->project_id == $project_id && $totalunico->id_amount == $est_num):  ?>
                                <?php $trasladostotal = $totalunico->traslados; ?>
                               $<?php echo format_currency($trasladostotal); ?>
                             <?php endif; endforeach; endif; ?>
                            </td>
                            <td>
                              <?php if($total->id_amount == 1){ ?>
                                  $<?php echo format_currency(0); ?>
                              <?php }else{ ?>
                                <?php $anteriortraslados += $total->traslados; ?>
                               $<?php echo format_currency(traslados($total->project_id,$est_numA)); ?>
                              <?php } ?>
                            </td>
                            <td>
                              <?php $acumualadotraslados = $trasladostotal + $anteriortraslados; ?>
                               $<?php echo format_currency(traslados_a($total->project_id)); ?>
                            </td>
                        </tr>
                      <?php endif; endforeach; endif; ?>
                      <?php if (count($anticipos)): foreach ($anticipos as $anticipo): if ($anticipo->project_id == $project_id && $anticipo->avance == 1): ?>
                          <?php $anti += $anticipo->amount; ?>
                      <?php endif; endforeach; endif; ?>
                        <tr>
                          <td colspan="1" class="text-right"><strong>IVA:</strong></td>
                          <td >
                            <?php $importeiva = $materialesivatotal * 0.16; ?>
                            $ <?php echo format_currency($importeiva); ?>
                          </td>
                          <td >
                            <?php $anterioiva = $anteriormaterialesiva * 0.16;  ?>
                            $ <?php echo format_currency($anterioiva); ?>
                          </td>
                          <td >
                            <?php $acumuladoiva = $acumualadomaterialesiva *0.16; ?>
                            $ <?php echo format_currency($acumuladoiva); ?>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="1" class="text-right"><strong>Subtotal:</strong></td>
                          <td >
                            <?php $subtotalimporte = $manototal + $materialestotal + $materialesivatotal + $subconttotal + $honorariostotal + $trasladostotal + $importeiva; ?>
                            $ <?php echo format_currency($subtotalimporte); ?>
                          </td>
                          <td class="text-right"><strong>Total Acumulado:</strong></td>
                          <td >
                            <?php $subtotalacumulado = $acumualadomano + $acumualadomateriales + $acumualadomaterialesiva + $acumualadosubcont + $acumualadohonorarios + $acumualadotraslados + $acumuladoiva; ?>
                            $ <?php echo format_currency($subtotalacumulado); ?>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="text-right"><strong>Anticipos:</strong></td>
                          <td >$ <?php echo format_currency($anti); ?></td>
                        </tr>
                        <tr>
                          <td colspan="3" class="text-right"><strong>Avance de Obra</strong></td>
                          <?php if (count($projects)) : foreach ($projects as $project):?>
                          <td >$ <?php  $avanc = $project->fondo; echo format_currency($project->fondo); ?></td>
                        <?php endforeach; endif;?>
                        </tr>
                        <tr>
                          <td colspan="3" class="text-right"><strong>Saldo:</strong></td>
                          <td >$ <?php echo format_currency($avanc - $anti + $subtotalacumulado); ?></td>
                        </tr>
                        <tr>
                          <td colspan="3" class="text-right"><strong>Espacio Diafano SA de CV:</strong></td>
                          <td >
                            <?php
                              $espaciod = $materialesivatotal * 0.16;
                              ?>
                            $ <?php echo format_currency($materialesivatotal + $importeiva); ?>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="text-right"><strong>Cuenta Corriente:</strong></td>
                          <td>
                            <?php $cuentac = $subtotalimporte - $materialesivatotal;?>
                            $ <?php echo format_currency($cuentac + $avanc -$importeiva - $anti); ?>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="3" class="text-right"><strong>Paga directo cliente:</strong></td>
                          <td>
                            $
                          <?php
                            if(count($transactions)): foreach($transactions as $transaction): if($transaction->paid == 4):
                              echo format_currency(invoice_total($transaction->id));
                            endif; endforeach; endif;
                           ?>
                          </td>
                        </tr>
                    </tbody>
                    </table>
              </div>
          </div>
      </div>

    </div>
</div>
