<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 1/18/2016
 * Time: 7:33 PM
 */
?>

<div id="contentarea" class="contentarea">

    <div class="row">
        <center>
          <div class="col-md-2">
            <a  href="https://proyectosinternos.com/Taller5/billing" class="btn btn-info btn-md btn-icon mt-10">
                <i class="fa fa-angle-double-left"></i>Programación</a>
          </div>
            <!-- <div class="col-md-1">
                <a  href="https://proyectosinternos.com/Taller5/billing/create_invoice" class="btn btn-info btn-md btn-icon mt-10">
                  Pedir Material<i class="fa fa-plus"></i></a>
            </div>

            <div class="col-md-2" style="margin-left:30px">
                <a  href="https://proyectosinternos.com/Taller5/billing/create_presupuesto" class="btn btn-info btn-md btn-icon mt-10">
                  Pptos Prov.<i class="fa fa-plus"></i></a>
            </div>

            <div class="col-md-1" style="margin-left:-40px">
                <a  href="https://proyectosinternos.com/Taller5/billing/create_raya" class="btn btn-info btn-md btn-icon mt-10">
                  Raya<i class="fa fa-plus"></i></a>
            </div>

            <div class="col-md-1" style="margin-left:-10px">
                <a  href="<?php echo base_url('billing/billing/pagar_factura/'); ?>" class="btn btn-info btn-md btn-icon mt-10" data-toggle="zestModal">A. Facturas <i class="fa fa-file-text-o fa-lg" ></i></a>
            </div>
            <div class="col-md-1" style="margin-left:38px">
                <a class="btn btn-info btn-icon mt-10" href="https://proyectosinternos.com/Taller5/billing/program_todo"
                    title=""><i class="fa fa-document"></i>Programación</a>
            </div>

            <div class="col-md-2 mt-10" style="margin-left:36px">
                <form class="form-inline" action="<?php echo base_url('billing/billing/a_estimar'); ?>" method="post">
                    <div class="form-group">
                      <select  data-placeholder="Select client..." class="form-control chosen-select" tabindex="4"
                      name="project_id" onchange="this.form.submit();">
                        <option value="Empty">nueva estimacion</option>
                          <?php if (count($projects)):foreach ($projects as $client): ?>
                            <option
                            value="<?php echo $client->project_id; ?>"><?php echo ucfirst($client->project_title) ?></option>
                          <?php endforeach; endif; ?>
                      </select>
                    </div>
                </form>
            </div>

            <div class="col-md-1" style="margin-left:38px">
                <a href="<?php echo base_url('billing/billing/nota_credito/'); ?>" data-toggle="zestModal" class="btn btn-info mt-10">N. C. <i class="fa fa-file-text-o fa-lg" ></i></a>
            </div>

            <div class="col-md-1">
              <a href="<?php echo base_url('billing/billing/add_gastos/'); ?>" data-toggle="zestModal" class="btn btn-info mt-10">Agregar Gasto <i class="fa fa-money"></i></a>
            </div> -->
        </center>
    </div><br>

                          <div class="table-responsive">
                            <div class="col-lg-7 pull-right text-right" style="margin-right: 10%;">
                            </div>

 <div class="row">


    <div class="col-sm-8">
        <div class="btn-group-action">
            <div class="btn-group">
               <!-- <a class="btn btn-info" style="width:100px;"
                                title=""><i
                                    class="fa fa-document"></i>Acciones</a>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                            </button>
                    <ul class="dropdown-menu pull-right">
                         <li><a href="javascript:void(0);" rel="tooltip" data-original-title="Delete"
                         class="program btn btn-md"><strong>Programar</strong></a></li>
                         <li><a href="javascript:void(0);" rel="tooltip" data-original-title="Delete"
                         class="for_estimate btn btn-md"><strong>Estimación</strong></a></li> -->
                         <!-- <li><a href="javascript:void(0);" rel="tooltip"  data-original-title="Delete"
                         class="paid1 btn btn-md"><strong>Pago Directo</strong></a></li> -->
                         <!-- <li><a href="javascript:void(0);" rel="tooltip"  data-original-title="Delete"
                         class="paid2 btn btn-md"><strong>Pago</strong></a></li> -->
                         <!-- <li><a href="javascript:void(0);" rel="tooltip"  data-original-title="Delete"
                         class="paid3 btn btn-md"><strong>Pago Cheque</strong></a></li> -->
                         <!-- <li><a href="javascript:void(0);" rel="tooltip"  data-original-title="Delete"
                         class="deliver btn btn-md"><strong>Entregado</strong></a></li> -->
                   </ul>
            </div>
        </div>
    </div>

 <div class="col-sm-4">
    <div class="pull-right">
        <input type="text" id="mailbox_search" class="form-control input-sm" placeholder="Buscar">
    </div>
 </div>
</div>
<div class="">

                              <table id="mail_outbox" class="mail_table table table-hover toggle-arrow-tiny table-striped" data-filter="#mailbox_search"
           data-page-size="20">
                                  <thead class="the-box dark full">
                                      <tr>
                                          <!-- <th style="text-align:center;width: 50px;">

                                          </th> -->
                                          <th style="text-align:center;">
                                              Folio #
                                          </th>
                                          <th style="text-align:center;">
                                            Factura
                                          </th>
                                          <th style="text-align:center;">
                                              Proveedor

                                          </th>
                                          <th style="text-align:center;">
                                           Proyecto
                                          </th>
                                          <th style="text-align:center;width: 100px;">
                                              Fecha
                                          </th>
                                          <th style="text-align:center;width: 120px;">
                                              <?php echo lang('label_amount'); ?>

                                          </th>

                                          <!-- <th>
                                              <?php echo lang('label_amount_due'); ?>

                                          </th> -->
                                          <th style="text-align:center;">
                                              <?php echo lang('label_payment_method'); ?>
                                          </th>
                                          <th style="text-align:center;">
                                              <?php echo lang('label_status'); ?>
                                          </th>


                                      </tr>
                                  </thead>
                                  <tbody>
                                      <?php  $cont = 1; if (count($transactions)) : foreach ($transactions as $transaction) : if($transaction->deliver == 1 && $transaction->est_num != 0 && $transaction->program_fin == 1 ) :
                                      $factura ="";?>
                                          <tr id="<?php echo $transaction->id ?>">
                                              <!-- <td style="text-align:center;padding:3px;">
                                                <div class="checkbox"><?php if ($transaction->deliver == "1" && $transaction->for_estimate == "1" ) {
                                                          ?>
                                                          <i class="fa fa-lock"></i>
                                                          <?php
                                                      }else {
                                                        ?><input type="checkbox" name="mail_id[]" value="<?php echo $transaction->id; ?>"
                                                        class="i-grey-flat" />
                                                        <label></label><?php
                                                      } ?>

                                                    </div>
                                              </td> -->
                                              <td style="text-align:center;padding:3px;">

                                                  <a href="<?php echo base_url('billing/invoice/' . $transaction->id); ?>"><?php echo $transaction->inv_no; ?></a>
                                                   <div class="btn-group btn-group-sm pull-right">
                                    <button type="button" style="background:transparent;padding-top: 0px;padding-left:0px;" class="btn dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-arrow-down"></i>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a title="Email Invoice"
                                               href="<?php echo base_url('billing/email_invoice/' . $transaction->id); ?>">Enviar Correo</a>
                                        </li>



                                        <li>
                                          <?php if($transaction->raya != 1){ ?>
                                            <a href="<?php echo base_url('billing/pdf_download/' . $transaction->id); ?>">Descargar Pdf</a>
                                          <?php }else{ ?>
                                            <a href="<?php echo base_url('billing/pdf_download_raya/' . $transaction->id); ?>">Descargar Pdf</a>
                                          <?php } ?>
                                        </li>

                                    </ul>
                                </div>
                                <?php if ($transaction->avance == 1){?>

                                 <a  href="<?php echo base_url('billing/billing/add_percent/'.$transaction->id.'/'.$transaction->project_id); ?>" data-toggle="zestModal"><i class="fa fa-bars" aria-hidden="true"></i></a>

                                <?php } ?>
                                              <!-- <?php if(count($invoice_item)) : foreach($invoice_item as $item): if ($transaction->id == $item->invoice_id): ?>
                                                <?php if (empty($item->item_avance)) { ?>
                                                    <i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:#f44336;"></i>
                                                <?php }else{}?>
                                              <?php endif; endforeach; endif; ?> -->
                                              </td>
                                              <td style="text-align:center;padding:3px;">
                                                  <?php if (count($transactions1)) : foreach ($transactions1 as $invoice): if ($invoice->invoice_id == $transaction->id) : ?>
                                                    <?php echo $invoice->invoice; ?>

                                              <?php  endif; endforeach; endif; ?>

                                              </td>
                                              <td style="text-align:center;padding:3px;">
                                                  <a href="<?php echo base_url('client/view/' . $transaction->client_id); ?>"><?php echo client_company($transaction->client_id); ?></a>
                                                  <?php if($transaction->trans == 1){?>
                                                    <div class="row">
                                                      <a href="<?php echo base_url('billing/payment_trans_2/' . $transaction->project_id .'/'. $transaction->client_id); ?>" data-toggle="zestModal" rel="tooltip"  data-original-title="Delete"
                                                      class="btn btn-sm btn-light" style="color:rgb(51, 105, 30);" title="Pago"><i class="fa fa-money fa-lg"></i></a>
                                                    </div>
                                                  <?php }else{} ?>
                                              </td>
                                              <td style="text-align:center;"> <a href="project/overview/preview/<?php echo $transaction->project_id; ?>">
                                                <?php if (count($projects)) : foreach ($projects as $project): if ($project->project_id == $transaction->project_id):  ?>
                                                    <?php echo $project->project_title ?>
                                                <?php endif; endforeach; endif; ?></a>
                                                <br>
                                                <?php if($transaction->for_estimate == 2){ ?>
                                                    <a style="color:#428BC8" href="<?php echo base_url('billing/billing/estimar_view/' . $transaction->project_id ."/". $transaction->est_num); ?>">Estimacion <?php echo $transaction->est_num ?></a>
                                                <?php } else{} ?>
                                              </td>
                                              <td style="width: 70px;text-align:center;padding:3px;"><?php echo $transaction->invoice_due; ?></td>
                                              <td>
                                                  <?php if($transaction->type == 0){ ?>
                                                  $<?php echo format_currency(invoice_total($transaction->id)-$transaction->disc_amount); ?>
                                                  <input type="hidden" name="item_monto" value="<?php echo invoice_total($transaction->id) - $transaction->disc_amount; ?>">
                                                  <?php }else{ ?>
                                                  <?php if(count($invoice_item)) : foreach($invoice_item as $item): if ($transaction->id == $item->invoice_id): ?>
                                                  $<?php     echo format_currency($item->price); ?>
                                                <?php  endif; endforeach; endif;?>
                                                <?php   } ?>
                                                <?php if (count($invo_client)): foreach($invo_client as $client): if($transaction->client_id == $client->client_id ):
                                                 if ($client->client_type == 3) { ?>
                                                    <!-- <a  href="<?php echo base_url('billing/billing/add_monto/'.$transaction->id.'/'.$transaction->client_id.'/'.$transaction->project_id.'/'.$transaction->tax); ?>" data-toggle="zestModal"><i class="fa fa-money" aria-hidden="true"></i></a>
                                                    <br>
                                                    Pago autorizado:
                                                    <br> -->
                                                    <?php
                                                      $pagos = 0;
                                                      $pago_id = 0;
                                                      if (count($pago_a)): foreach ($pago_a as $pago): if($transaction->id == $pago->inv_id):
                                                        $pagos += $pago->monto;
                                                        $pago_id = $pago->id;
                                                      endif; endforeach; endif;
                                                    ?>
                                                  <!-- <strong class="text-info"><?php echo format_currency($pagos); ?></strong> -->
                                                  <?php if($pago_id != 0){ ?>
                                                  <!-- <a  href="<?php echo base_url('billing/remove_pago_a/'.$pago_id); ?>"><i class="fa fa-times" aria-hidden="true" style="color:#BD0402"></i></a> -->
                                                  <?php } ?>
                                              <?php } endif; endforeach; endif;?>
                                              </td>
                                              <td style="text-align:center;" >

                                               <a href="/Taller5/expenses/view_statement/<?php echo $transaction->paid; ?>"><span style="margin:0px;" class="label label-success"><?php echo payment_method($transaction->paid); ?></span></a><br>

                                               </td>
                                              <td padding:3px;>
                                                <?php
                                                  $message2 = '';
                                                  $message3 = '';
                                                    $label = "success";
                                                    $message3 = "ninguno";

                                                  if ($transaction->program == "1") {
                                                       $label = "success";
                                                        $message3 = "Programado";
                                                  }
                                                   if ($transaction->paid >= 1 ) {
                                                            $label = "success";
                                                            $message3 = "Pago preparado";
                                                          }

                                                if ($transaction->deliver == "1") {
                                                          $label = "success";
                                                          $message3 = "Entregado";

                                                      }

                                                  if ($transaction->for_estimate == "1") {
                                                    $label = "success";
                                                    $message2 = "Para estimación";
                                                  }
                                               ?>
                                               <span style="margin:0px;" class="label label-<?php echo $label; ?>"><?php echo $message3; ?></span>
                                               <br>
                                               <span style="margin:0px;" class="label label-<?php echo $label; ?>"><?php echo $message2; ?></span>
                                               <br>

                                               <?php if($transaction->deliver == 0){
                                                 if($transaction->paid == 1 || $transaction->paid == 2 || $transaction->paid ==3 ){ ?>
                                                   <?php echo form_open('billing/status_deliver'); ?>
                                                   <div class="col-md-4"></div>
                                                     <input type="hidden" name="id" value="<?php echo $transaction->id; ?>">
                                                     <button  type="submit" name="button" title="Entregado" style="background-color:#1080d000;border-color:#1080d003;"><a style="color: #4CAF51;"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></a></button>
                                                  <?php echo form_close(); ?>
                                             <?php } }else{} ?>
                                             </td>

                                          </tr>
                                      <?php $cont++; endif; endforeach; endif; ?>
                                  </tbody>
                                   <tfooter>
        <tr>
            <td colspan="6">
                <div class="row">
                    <div class="col-md-4">
                        <span class="mailbox_count_msg"><b class="page_start_row">1</b>-<b class="page_end_row">17</b> de <b
                                class="all_rows">20</b></span>
                    </div>
                    <div class="col-md-8 text-right hide-if-no-paging">
                        <ul class="pagination pagination-sm"></ul>
                    </div>
                </div>
            </td>
        </tr>
        </tfooter>
                              </table>

                          </div>

    </div>
    </div>


<script src="https://proyectosinternos.com/Taller5/assets/libs/jquery-2.0.2.min.js"></script>
<script src="https://proyectosinternos.com/Taller5/assets/plugins/ios7-switch/ios7.switch.js"></script>
<link href="https://proyectosinternos.com/Taller5/assets/plugins/datatable/css/bootstrap.datatable.min.css" rel="stylesheet" type="text/css">
<script src="https://proyectosinternos.com/Taller5/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="https://proyectosinternos.com/Taller5/assets/plugins/datatable/js/jquery.dataTables.js"></script>
<script src="https://proyectosinternos.com/Taller5/assets/plugins/datatable/js/bootstrap.datatable.js"></script>
<script src="https://proyectosinternos.com/Taller5/assets/libs/jquery-ui-1.10.3.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<?php $id = null; ?>
<script type="text/javascript">
    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });



    $('#save_items_hola').click(function() {

        var items = [];

        $('.item_table > tbody >tr').each(function() {
            var row = {};
            $(this).find('input:checkbox:checked,select,textarea').each(function() {

                row[$(this).attr('name')] = $(this).val();
            });

            items.push(row);
        });

        console.log(items);

        $.post("<?php echo site_url('billing/save_hola'); ?>", {
                items: JSON.stringify(items)
            },
            function(data) {

            });
        // window.location.replace("https://proyectosinternos.com/Taller5/billing");

    });

    jQuery(document).ready(function() {

        var oTable = $('#1zest_table').dataTable({
            "aoColumnDefs": [
                {"aTargets": [1, 1]}
            ],
            "aaSorting": [[1, 'asc']],
            "aLengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 20
        });

        $('#inbox-table').dataTable();

    });

    jQuery(document).ready(function() {

        var oTable = $('#2zest_table').dataTable({
            "aoColumnDefs": [
                {"aTargets": [1, 1]}
            ],
            "aaSorting": [[1, 'asc']],
            "aLengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 20
        });

        $('#inbox-table').dataTable();

    });

    jQuery(document).ready(function() {

        var oTable = $('#3zest_table').dataTable({
            "aoColumnDefs": [
                {"aTargets": [1, 1]}
            ],
            "aaSorting": [[1, 'asc']],
            "aLengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 20
        });

        $('#inbox-table').dataTable();

    });

    jQuery(document).ready(function() {

        var oTable = $('#4zest_table').dataTable({
            "aoColumnDefs": [
                {"aTargets": [1, 1]}
            ],
            "aaSorting": [[1, 'asc']],
            "aLengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "Todos"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 20
        });

        $('#inbox-table').dataTable();

    });

//     function validacion(obj) {
//   limite=1;
//   num=0;
//   if (obj.checked) {
//     for (i=0; ele=obj.form.elements[i]; i++)
//       if (ele.checked) num++;
//   if (num>limite)
//     obj.checked=false;
//   }
// }
// onchange="validacion(this)"

</script>
