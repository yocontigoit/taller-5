<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/25
 * Time: 01:58 PM
 */
?>
<div class="the-box">
    <div class="portlet-body">
      <div class="row">
          <div class="col-lg-12">
            <div class="row">
                <div class="pull-left">
                    <a class="btn btn-info" href="<?php echo base_url('billing/pdf_download_billing/'); ?>">Descargar PDF<i class="fa fa-print"></i></a>
                </div>
                 <?php echo form_open('billing/add_new_estimate/', array('id' => 'clientForm', 'class' => 'form-horizontal')); ?>
                <input type="hidden" value="<?php echo $project_id; ?>" name="project_id" />
                <div class="pull-right">
                    <button class="btn btn-info" type="submit" >Guardar Estimado<i class="fa fa-print"></i></button>
                </div>

                  <div class="col-md-offset-4">
                    <!-- <?php if (count($projects)) : foreach ($projects as $project):?>
                    <h1><?php echo $project->project_title ?></h1>
                  <?php endforeach; endif; ?> -->
                  </div>
            </div>
            <br>

                                <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                                  <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Programcación Por Proveedor</h4></div>
                      <thead class="the-box light full">
                      <tr>
                          <th>
                              Nombre del Proveedor

                          </th>
                          <th>
                              <?php echo lang('label_amount'); ?>

                          </th>

                      </tr>
                      </thead>
                      <tbody>

                      <?php if (count($transactions)) : foreach ($transactions as $transaction) : if($transaction->program == '1'): if($transaction->client_id == $transaction->client_id):?>

                          <tr>
                              <td>
                                  <input type="text" class="form-control" value="<?php echo client_company($transaction->client_id); ?>">
                              </td>
                              <td>
                                <div class="input-group">
                                  <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                  <input type="text" class="form-control" name="item_amount" value="<?php echo format_currency(invoice_total($transaction->id)); ?>" />
                                </div>
                              </td>
                          </tr>

                      <? endif; endif; endforeach; endif;  ?>
                      <?php if (count($transactions)) : foreach ($transactions as $transaction) : if($transaction->estimate == '1'): if($transaction->client_id == 17):?>

                          <tr>
                              <td>
                                  <a href="<?php echo base_url('client/view/' . $transaction->client_id); ?>"><?php echo client_company($transaction->client_id); ?></a>
                              </td>
                              <td>$
                                  <?php echo invoice_total($transaction->id); ?>
                              </td>
                          </tr>

                      <? endif; endif; endforeach; endif;  ?>
                      <?php if (count($transactions)) : foreach ($transactions as $transaction) : if($transaction->estimate == '1'): if($transaction->client_id == 32):?>

                          <tr>
                              <td>
                                  <a href="<?php echo base_url('client/view/' . $transaction->client_id); ?>"><?php echo client_company($transaction->client_id); ?></a>
                              </td>
                              <td>$
                                  <?php echo invoice_total($transaction->id); ?>
                              </td>
                          </tr>

                      <? endif; endif; endforeach; endif;  ?>
                      </tbody>
                          </table>
                          <div class="pull-right">
                            <a href="javascript:void(0)" id="guardar2" class="btn btn-small btn-info">
                              Guardar
                            </a>
                          </div>
                          <br>
                          <br>
                          <br>
                      <?php
                          $subtotal =0.00;
                          $iva =0.00;
                          $honorarios=0.00;
                          $traslados=0.00;
                          $total=0.00;
                          $error = "";
                          if (count($transactions)) : foreach ($transactions as $transaction) : if($transaction->program == '1'):
                            $subtotal+= invoice_total($transaction->id);
                            if (count($projects)) : foreach ($projects as $project):
                       ?>
                      <?php
                          $total = $subtotal;
                      ?>
                      <?php
                            endforeach; endif;
                          endif; endforeach; endif;
                      ?>
                      <div class="col-md-6"></div>
                      <div class="col-md-6" style="padding-right:0px;">
                          <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                      <tbody>
                      <tr>

                        <td ><strong>IVA:</strong></td>
                        <td >$<?php echo format_currency($iva); ?></td>
                      </tr>
                      <tr>
                        <td ><strong>Total:</strong></td>
                        <td >
                          $<?php
                              echo format_currency($total);
                              $total = $total;
                          ?>
                          <input type="hidden" name="total"  value="<?php echo $total; ?>" readonly>
                        </td>
                      </tr>
                      </tbody>
                  </table>
                  </div>
                </form>
              </div>
          </div>
      </div>

    </div>
</div>
