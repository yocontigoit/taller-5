<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/25
 * Time: 02:33 PM
 */
//@TODO : Show al invoices
?>
<div class="row">
    <div class="col-lg-12">
      <div class="col-sm-offset-10">
        <div class="row">
          <a  href="https://proyectosinternos.com/Taller5/billing/create_invoice" class="btn btn-info btn-lg btn-icon mt-10"><i
            class="fa fa-check-square-o"></i>Crear Pago</a>
          </div>
      </div>
      <br>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
                   id="zest_table">
                <thead class="the-box dark full">
                <tr>
                    <th>
                        <?php echo lang('label_invoice_no'); ?>
                    </th>
                    <th>
                        <?php echo lang('label_client_name'); ?>

                    </th>

                    <th>
                        <?php echo lang('label_due_date'); ?>


                    </th>
                    <th>
                        <?php echo lang('label_amount'); ?>

                    </th>

                    <th>
                        <?php echo lang('label_amount_due'); ?>

                    </th>
                    <th>
                        <?php echo lang('label_payment_method'); ?>
                    </th>
                    <th>
                        <?php echo lang('label_status'); ?>
                    </th>


                </tr>
                </thead>
                <tbody>
                <?php if (count($transactions)) : foreach ($transactions as $transaction) : ?>
                    <tr>
                        <td>
                            <a href="<?php echo base_url('billing/invoice/' . $transaction->id); ?>"><?php echo $transaction->inv_no; ?></a>
                        </td>
                        <td>
                            <a href="<?php echo base_url('client/view/' . $transaction->client_id); ?>"><?php echo client_company($transaction->client_id); ?></a>
                        </td>

                        <td><?php echo $transaction->invoice_due; ?></td>
                        <td>
                            <?php echo invoice_total($transaction->id); ?>
                        </td>
                        <td>
                            <?php echo invoice_balance($transaction->id); ?>
                        </td>
                        <?php $invoice_status = get_invoice_status($transaction->status);

                        if ($invoice_status == "PAID") {
                            $label = "success";
                            $_message = invoice_payment_method($transaction->id);
                        } else {
                            $label = "info";
                            $_message = "No payment received";
                        }


                        ?>
                        <td><span class="label label-<?php echo $label; ?>"><?php echo $_message; ?></span></td>
                        <td>
                            <span class="textgreen"><?php echo $invoice_status; ?></span>
                        </td>


                    </tr>
                <?php endforeach; endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
