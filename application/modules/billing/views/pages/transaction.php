<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/25
 * Time: 02:33 PM
 */

?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
           id="zest_table">
        <thead class="the-box dark full">
        <tr>
            <th>
                <?php echo lang('label_client_no'); ?>
            </th>
            <th>
                <?php echo lang('label_date'); ?>
            </th>
            <th class="hidden-xs">
                <?php echo lang('label_payment_method'); ?>
            </th>
            <th>
                <?php echo lang('label_invoice_no'); ?>
            </th>
            <th>
                <?php echo lang('label_amount'); ?>
            </th>
            <th class="hidden-xs">
                <?php echo lang('label_options'); ?>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($transactions)): foreach ($transactions as $transaction): ?>
            <tr>
                <td>
                    <?php echo client_company($transaction->client_id); ?>
                </td>
                <td>
                    <?php echo $transaction->created_at; ?>
                </td>

                <?php $invoice_status = get_invoice_status($transaction->status);

                if ($invoice_status == "PAID") {
                    $label = "Éxito";
                    $_message = invoice_payment_method($transaction->id);
                } else {
                    $label = "info";
                    $_message = "No hay pagos";
                }


                ?>
                <td><span class="label label-<?php echo $label; ?>"><?php echo $_message; ?></span></td>

                <td class="hidden-xs">
                    <?php echo $transaction->inv_no; ?>
                </td>
                <td>
                    <?php echo format_amount($transaction->amount); ?>
                </td>
                <td class="text-center">
                    <div class="btn-group-action">
                        <div class="btn-group">
                            <a class=" btn btn-info"
                               href="<?php echo base_url("billing/invoice/" . ($transaction->id)); ?>" title=""><i
                                    class="fa fa-eye"></i>Ver factura</a>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret">&nbsp;</span>
                            </button>
                            <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                                <ul class="dropdown-menu pull-right text-danger">
                                    <li><a data-toggle="zestModal"
                                           href="<?php echo base_url('billing/remove_payment/' . ($transaction->payment_id)); ?>"
                                           title=""><i class="fa fa-trash"></i><?php echo lang('label_delete'); ?></a>
                                    </li>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>

        <?php endif; ?>

        </tbody>
    </table>
</div>