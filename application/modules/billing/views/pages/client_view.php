<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 1/18/2016
 * Time: 7:33 PM
 */
?>

<div id="contentarea" class="contentarea">

    <div class="page-tabs">
        <ul class="nav nav-tabs" role="tablist">
            <li class="dropdown pull-right tabdrop hide">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                    <i class="icon-align-justify"></i>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu"></ul>
            </li>
            <li class="active">
                <a id="tabLink1" data-toggle="tab" role="tab" href="#tab1"
                   aria-expanded="true"><?php echo lang('label_summary'); ?></a>
            </li>
            <li class="">
                <a id="tabLink2" data-toggle="tab" role="tab" href="#tab2"
                   aria-expanded="false"><?php echo lang('label_add_payment'); ?></a>
            </li>
            <li class="">
                <a id="tabLink3" data-toggle="tab" role="tab" href="#tab3"
                   aria-expanded="false"><?php echo lang('label_transaction'); ?></a>
            </li>
            <li>
                <a id="tabLink4" data-toggle="tab" role="tab" href="#tab4"><?php echo lang('label_notes'); ?></a>
            </li>

        </ul>
    </div>

    <div class="tab-content">

        <div id="tab1" class="tab-pane active">

            <?php $invoice_status = get_invoice_status($invoice_details->status);

            if ($invoice_status == "PAID") {
                $label_text = "success";
            } else {
                $label_text = "warning";
            }

            ?>
            <?php $customer_details = get_client_details($invoice_details->client_id);
            $inv_amount_due = invoice_total($invoice_details->id);
            $inv_amount_balance = invoice_balance($invoice_details->id)

            ?>
            <?php
            if (strtotime($invoice_details->invoice_due) < time() && ($inv_amount_balance > 0 && $inv_amount_balance < $inv_amount_due)) :?>
                <div class="alert alert-warning alert-block fade in alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Overdue!</strong><?php echo lang('messages_invoice_overdue'); ?>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-sm-6">
                    <h6><?php echo lang('label_invoice_to'); ?>:</h6>
                    <ul>
                        <li><?php echo lang('label_client_name'); ?>: <a
                                href="#"><strong><?php echo $customer_details->client_name; ?></strong></a></li>
                        <li><?php echo lang('label_invoice_date'); ?> :
                            <strong><?php echo $invoice_details->created_at; ?></strong></li>
                        <li><?php echo lang('label_due_date'); ?> : <strong
                                style="margin-left: 3%;"><?php echo $invoice_details->invoice_due; ?></strong></li>
                        <li><?php echo lang('label_amount_due'); ?> : <strong
                                class="text-danger" style="color:#828282;"><?php echo $inv_amount_due; ?></strong></li>
                        <li><?php echo lang('label_balance'); ?> : <strong
                                class="text-danger" style="color:#828282;"><?php echo $inv_amount_balance; ?></strong></li>
                    </ul>
                </div>


                <div class="col-sm-6">
                    <h6>

                            <span style="font-family:Arial;font-size:20px;font-weight:bold;text-transform:uppercase">
                                 <div
                                     class="label label-<?php echo $label_text; ?>"><?php echo $invoice_status; ?></div>

                            </span>
                    </h6>
                    <ul>
                        <?php
                        if ($invoice_status == "PAID" || $invoice_status == "PARTIALLY PAID") {

                            ?>

                            <li><a href="#"><b><?php echo $invoice_details->paid_at; ?></b></a></li>

                            <li><?php echo lang('label_payment_method'); ?> : <strong>
                                    <div
                                        class="label label-danger"><?php echo invoice_payment_method($invoice_details->id); ?></div>
                                </strong></li>
                        <?php } ?>
                    </ul>

                </div>

            </div>

        </div>

        <div id="tab2" class="tab-pane">
            <?php if ($inv_amount_balance > 1): ?>
                <div class="row mix-grid">
                    <!--start -->
                    <?php if (count($payments_methods)): foreach ($payments_methods as $pay): ?>
                        <div class="col-sm-3 col-md-2">

                            <?php if ($pay->payment_name != 'banktransfer'): ?>
                                <a class="btn-group" data-toggle="zestModal"
                                   href="<?php echo base_url($pay->payment_name . '/add_payment/' . $invoice_details->id); ?>">
                                    <img alt=""
                                         src="<?php echo base_url('files/gateways/' . $pay->payment_name . '.png'); ?>"
                                         width="100" height="35">
                                </a>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; endif; ?>
                    <!--end -->
                </div>
            <?php else: ?>
                <div class="infobox">
                    <strong>
                        <span class="title">Invoice in Paid Status</span>
                    </strong>
                    <br>
                    This invoice is already marked paid
                </div>
            <?php endif; ?>
        </div>
        <div id="tab3" class="tab-pane">

            <div class="table-responsive">
                <table
                    class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
                    id="sample_2">
                    <thead class="the-box dark full">
                    <th><?php echo lang('label_date'); ?></th>
                    <th> <?php echo lang('label_payment_method'); ?></th>
                    <th><?php echo lang('label_transaction_reference'); ?></th>
                    <th><?php echo lang('label_amount'); ?></th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <?php if (count($invoice_transactions)): foreach ($invoice_transactions as $transaction): ?>
                    <tr>
                        <td><?php echo $transaction->created_at; ?></td>
                        <td><?php echo payment_method($transaction->payment_method); ?></td>
                        <td><?php echo $transaction->trans_id; ?></td>
                        <td><?php echo $transaction->amount; ?></td>

                    </tr>
                    <?php endforeach; ?>

                    <?php endif; ?>
                    </tbody>
                </table>
            </div>

        </div>
        <div id="tab4" class="tab-pane">
            <form method="post" action="<?php echo base_url('project/tasks/add_new/' . $project_id); ?>">
                <div class="form-group">
                    <label for="taskDesc"><?php echo lang('label_notes'); ?></label>
                    <textarea class="form-control" rows="5" name="task_req" required=""></textarea>
                </div>
            </form>
        </div>

    </div>

    <h2><?php echo lang('label_invoice_items'); ?></h2>

    <form action="" method="post">

        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
                   id="sample_2">
                <thead class="the-box dark full">
                <tr>

                    <th><?php echo lang('label_item_name'); ?></th>
                    <th><?php echo lang('label_item_description'); ?></th>
                    <th width="120"><?php echo lang('label_item_price'); ?></th>
                    <th width="120"><?php echo lang('label_quantity'); ?></th>
                    <th width="120"><?php echo lang('label_amount'); ?></th>
                    <th width="20"></th>

                </tr>

                </thead>
                <tbody>
                <?php if (count($invoice_items)) : foreach ($invoice_items as $invoice_item) : ?>
                    <tr>

                        <td>
                            <textarea class="form-control" rows="1"
                                      name="item_description[<?php echo $invoice_item->id; ?>]"><?php echo $invoice_item->item_name; ?></textarea>
                        </td>
                        <td>
                            <textarea class="form-control" rows="1"
                                      name="item_description[<?php echo $invoice_item->id; ?>]"><?php echo $invoice_item->item_desc; ?></textarea>
                        </td>
                        <td nowrap="" align="center">
                            <input class="form-control" type="text" style="text-align:center"
                                   value="<?php echo $invoice_item->price; ?>"
                                   name="item_price[<?php echo $invoice_item->id; ?>]">
                        </td>
                        <td nowrap="" align="center">
                            <input class="form-control" type="text" style="text-align:center"
                                   value="<?php echo $invoice_item->quantity; ?>"
                                   name="item_quantity[<?php echo $invoice_item->id; ?>]">
                        </td>
                        <td align="center">
                            <input class="form-control" type="text"
                                   value="<?php echo calculate_invoice_line_subtotal($invoice_item->id); ?>"
                                   name="item_total[<?php echo $invoice_item->id; ?>]">
                        </td>
                        <td width="20" align="center">

                                <span>
                                    <a title="delete" href="">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </span>

                        </td>
                    </tr>

                <?php endforeach; endif; ?>
                <tr>

                    <td>
                        <textarea class="form-control" rows="1" name="adddescription"></textarea>
                    </td>
                    <td>
                        <textarea class="form-control" rows="1" name="adddescription"></textarea>
                    </td>
                    <td align="center">
                        <input class="form-control" type="text" style="text-align:center" name="addamount">
                    </td>
                    <td align="center">
                        <input type="checkbox" value="1" name="addtaxed">
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="text-align:right;background-color:#efefef;" colspan="4">
                        <strong><?php echo lang('label_sub_total'); ?>:</strong>
                    </td>
                    <td style="background-color:#efefef;text-align:center;">
                        <strong><?php echo format_currency(invoice_sub_total_total($invoice_details->id)); ?></strong>
                    </td>
                    <td style="background-color:#efefef;"></td>

                </tr>
                <tr>
                    <td style="text-align:right;background-color:#efefef;" colspan="4"><?php echo lang('label_tax'); ?>
                        :
                    </td>
                    <td style="background-color:#efefef;text-align:center;"><?php echo format_currency(invoice_tax_total($invoice_details->id)); ?></td>
                    <td style="background-color:#efefef;"></td>

                </tr>
                <tr>

                    <td style="text-align:right;background-color:#efefef;"
                        colspan="4"><?php echo lang('label_total'); ?>:
                    </td>
                    <td style="background-color:#efefef;text-align:center;"><?php echo format_currency(invoice_total($invoice_details->id)); ?></td>
                    <td style="background-color:#efefef;"></td>

                </tr>
                </tbody>
            </table>
        </div>
    </form>

</div>
