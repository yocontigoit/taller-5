<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/25
 * Time: 01:58 PM
 */
?>

<div class="the-box">
  <div class="portlet-body">
    <div class="row">
      <div class="col-lg-12">
        <div id="contentarea" class="contentarea">
          <div class="page-tabs">
            <ul class="nav nav-tabs" role="tablist">
              <li class="dropdown pull-right tabdrop hide">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                    <i class="icon-align-justify"></i>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu"></ul>
              </li>
              <li class="active">
                  <a id="tabLink1" data-toggle="tab" role="tab" href="#tab1"
                     aria-expanded="true">Por Obra</a>
              </li>
              <li class="">
                  <a id="tabLink2" data-toggle="tab" role="tab" href="#tab2"
                     aria-expanded="false">Por Proveedor/Subcontratista</a>
              </li>
              <li class="">
                  <a id="tabLink3" data-toggle="tab" role="tab" href="#tab3"
                     aria-expanded="false">Por Cuenta</a>
              </li>
            </ul>
          </div>

          <div class="tab-content">
            <div id="tab1" class="tab-pane active">
              <a class="btn btn-info" href="<?php echo base_url('billing/pdf_download_program1/'); ?>">Descargar PDF<i class="fa fa-print"></i></a>
              <div class="table-responsive">
                  <?php if(count($projects)): foreach($projects as $project): ?>
                    <?php
                      $total = 0; $subtotal = 0; $Totals = 0; $totaltrans = 0; $Totalpago = 0; $Totalsaldo = 0; $pagotrans = 0;
                      $pago_a = 0; $Totalprov = 0; $totalstrans = 0; $totalsub = 0; $diafano = 0; $corriente = 0; $pago_total = 0;
                      $descuento_prject = 0; $otrototal = 0; $Totalpro_diafanoprov = 0; $Totalprov_corriente = 0; $tota_diafano = 0;
                      $tota_corriente = 0; $eltotal = 0; $nc = 0; $nc_notax = 0; $nc_tax = 0; $invoice_total_diafano = 0;
                     ?>
                  <div class="col-md-6">
                    <div style="background:#d3d3d3;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;"><?php echo $project->project_title ?></h4></div>
                    <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success items_table_mano display" id="example">
                      <thead class="the-box light full">
                        <tr>
                          <th style="width: 221px;">
                            Proveedor
                          </th>
                          <th style="width: 105px;">
                            Total Presupuesto
                          </th style="width: 84px">
                          <th>
                            Pago autorizado
                          </th>
                          <th>
                            Saldo
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if(count($clients)): foreach($clients as $invo): if($project->project_id == $invo->project_id && $invo->program == 1): ?>
                          <tr>
                            <td>
                              <?php echo client_company($invo->client_id);?>
                            </td>
                            <td>
                              $<?php
                              echo format_currency(invoice_total_proveedor($invo->project_id,$invo->client_id) - discount($invo->project_id,$invo->client_id,$invo->program));
                              ?>
                            </td>
                            <td>
                              <?php if($invo->client_type == 3){?>
                                  <?php $eltotal = pago_a_diafano($invo->client_id, $invo->project_id) + pago_a($invo->client_id, $invo->project_id) ?>
                                $<?php echo format_currency($eltotal); ?>
                              <?php }else{ ?>
                                $ <?php echo format_currency(invoice_total_proveedor($invo->project_id,$invo->client_id) - discount($invo->project_id,$invo->client_id,$invo->program)); ?>
                              <?php } ?>
                            </td>
                            <td>
                                $<?php if($invo->client_type == 3){?>
                                  <?php
                                    $Totalprov_corriente += pago_a($invo->client_id,$invo->project_id);
                                    $Totalpro_diafanoprov += pago_a_diafano($invo->client_id,$invo->project_id);
                                    $Totalprov = $Totalprov_corriente +  $Totalpro_diafanoprov;
                                    if($Totalprov_corriente == 0 && $Totalpro_diafanoprov == 0){
                                      $total = invoice_total_proveedor($invo->project_id,$invo->client_id) - client_payments($invo->client_id,
                                              'year'); ?>
                                    <?php }else{  ?>
                                  <?php $total = invoice_total_proveedor($invo->project_id,$invo->client_id) - $eltotal;
                                      } ?>
                                <?php } else{ ?>
                                <?php $total = invoice_total_proveedor($invo->project_id,$invo->client_id) - invoice_total_proveedor($invo->project_id,$invo->client_id); ?>
                                <?php } ?>
                                <?php  $subtotal = $total; echo format_currency($subtotal - discount($invo->project_id,$invo->client_id,$invo->program)) ; ?>
                                <?php $totalsub += $subtotal - discount($invo->project_id,$invo->client_id,$invo->program)?>
                              </td>
                            </tr>
                            <?php endif; endforeach; endif; ?>
                            <?php
                            if(count($transactions)): foreach($transactions as $totales): if($totales->project_id == $project->project_id && $totales->program == 1 && $totales->type_pre != 5):
                              // $Totals += pago_a($totales->client_id,$totales->project_id);
                              $descuento_prject = discount_project($totales->project_id,$totales->program);
                              $totalstrans += invoice_total($totales->id);
                              $nc = nc($totales->project_id, $totales->client_id);
                              $otrototal = $totalstrans - $descuento_prject - $nc;
                              if ($totales->pago_a != 1 ) {
                                $totaltrans += invoice_total($totales->id) - discount($totales->project_id,$totales->client_id,$totales->program);
                              }else {}

                                $pago_total =  $Totalprov;

                                if ($totales->tax == 16 && $totales->pago_a != 1) {
                                    $nc_tax = nc_tax($totales->project_id, $totales->client_id);
                                    $invoice_total_diafano += invoice_total($totales->id);

                                    $tota_diafano = $invoice_total_diafano - $nc_tax;

                                }
                                if ($totales->tax == 0 && $totales->pago_a != 1){
                                    $nc_notax = nc_notax($totales->project_id, $totales->client_id);
                                    $tota_corriente += invoice_total($totales->id) - $nc_notax;
                                }



                              endif; endforeach; endif;
                              $pago_a =  $Totalprov + $totaltrans - $nc;
                            ?>
                            <tr>
                              <td>
                                <strong><p class="text-right">Totales:</p></strong>
                              </td>
                              <td>
                                $<?php
                                 echo format_currency($otrototal);
                                ?>
                              </td>
                              <td>
                                $ <?php echo format_currency($pago_a); ?>
                              </td>
                              <td>
                                $
                                <?php
                                 echo format_currency($totalsub);
                                 ?>
                              </td>
                            </tr>
                        </tbody>
                      </table>
                      <center>
                        <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success items_table_mano">
                          <tbody>
                            <tr>
                              <th>
                                Total A Pagar:
                              </th>
                              <td>
                                $<?php echo format_currency($pago_a); ?>
                              </td>
                            </tr>
                            <tr>
                              <th>
                                Total Cuenta Corriente:
                              </th>
                              <td>
                               $<?php
                                $corriente = $Totalprov_corriente + $tota_corriente;
                                echo format_currency($corriente);
                               ?>
                              </td>
                            </tr>
                            <tr>
                              <th>
                                Espacio Diafano SA de CV:
                              </th>
                              <td>
                                $<?php
                                  $diafano = $Totalpro_diafanoprov + $tota_diafano;
                                  echo format_currency($diafano);
                                ?>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </center>
                  </div>
                  <?php endforeach; endif; ?>
                </div>
              </div>
              <div id="tab2" class="tab-pane">
                  <!-- <a class="btn btn-info" href="<?php echo base_url('billing/pdf_download_program/' . $project_id); ?>">Descargar PDF<i class="fa fa-print"></i></a> -->
                <!-- <div class="row">
                  <img src="http://quattrosol.com/img2.jpeg" width="100%">
                </div> -->
                <a class="btn btn-info" href="<?php echo base_url('billing/pdf_download_program2/'); ?>">Descargar PDF<i class="fa fa-print"></i></a>
                <div class="table-responsive">
                    <?php if(count($all_clients)): foreach($all_clients as $clients):?>
                      <?php
                        $totalpro = 0; $subtotalpro = 0; $Totalpro = 0; $totaltranspro = 0; $Totalpagopro = 0; $Totalsaldopro = 0; $pagotranspro = 0;
                        $pago_apro = 0; $Totalspro = 0; $totalstranspro = 0; $totalsubpro = 0; $corrientepro = 0; $diafanopro = 0; $pago_totalpro = 0;
                        $invoice_projecto = 0; $invoice_projecto_tax = 0; $eldescuento = 0; $total_a_corriente = 0; $total_a_diafano = 0; $Totalpro_diafano = 0;
                        $Totalpro_corriente = 0; $eltotales = 0; $otrototalpro = 0;
                      ?>
                    <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;"><?php echo $clients->client_name ?></h4></div>
                    <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success items_table_mano">
                      <thead class="the-box light full">
                        <tr>
                          <th width="20%" style="width:20% !important;">
                            Obra
                          </th>
                          <th width="15%" style="width:15% !important;">
                            Total Presupuesto
                          </th>
                          <th width="15%" style="width:15% !important;">
                            Pago autorizado
                          </th>
                          <th width="10%" style="width:10% !important;">
                            Saldo
                          </th>
                          <th width="20%" style="width:20% !important;">
                            Total Cuenta Corriente:
                          </th>
                          <th width="20%" style="width:20% !important;">
                            Espacio Diafano SA de CV:
                          </th>
                        </tr>

                      <tbody>
                        <?php if(count($projectos)): foreach($projectos as $invoice): if($clients->client_id == $invoice->client_id && $invoice->program == 1): ?>
                        <tr>
                          <td>
                            <?php echo project_company($invoice->project_id); ?>
                          </td>
                          <td>
                            $<?php echo format_currency(invoice_total_proveedor($invoice->project_id,$invoice->client_id,$invoice->program) - discount($invoice->project_id,$invoice->client_id,$invoice->program)); ?>

                          </td>
                          <td>
                            $<?php
                              if($invoice->client_type == 3){
                                $eltotales = pago_a($invoice->client_id, $invoice->project_id) + pago_a_diafano($invoice->client_id, $invoice->project_id);
                                 echo format_currency($eltotales);
                               }else{
                                echo format_currency(invoice_total_proveedor($invoice->project_id,$invoice->client_id,$invoice->program) - discount($invoice->project_id,$invoice->client_id,$invoice->program));
                              }
                             ?>
                          </td>
                          <td>
                            $<?php if($invoice->client_type == 3){?>
                              <?php
                                $Totalpro_corriente += pago_a($invoice->client_id,$invoice->project_id);
                                $Totalpro_diafano += pago_a_diafano($invoice->client_id,$invoice->project_id);
                                $Totalpro = $Totalpro_corriente + $Totalpro_diafano;
                                if($Totalpro_corriente == 0 && $Totalpro_diafano == 0){
                                  $totalpro = invoice_total_proveedor($invoice->project_id,$invoice->client_id,$invoice->program) - client_payments($invoice->client_id,
                                          'year'); ?>
                                <?php }else{  ?>
                              <?php $totalpro = invoice_total_proveedor($invoice->project_id,$invoice->client_id,$invoice->program) - $eltotales;

                                  } ?>
                            <?php } else{ ?>
                            <?php $totalpro = invoice_total_proveedor($invoice->project_id,$invoice->client_id,$invoice->program) - invoice_total_proveedor($invoice->project_id,$invoice->client_id,$invoice->program); ?>
                            <?php } ?>
                            <?php $subtotalpro = $totalpro; echo format_currency($subtotalpro - discount($invoice->project_id,$invoice->client_id,$invoice->program));   ?>
                            <?php $totalsubpro += $subtotalpro - discount($invoice->project_id,$invoice->client_id,$invoice->program)?>
                          </td>
                          <td>
                            <div class="col-md-6">
                              $<?php
                              if($Totalpro_corriente == 0){
                                $nc_notax = nc_notax($invoice->project_id, $invoice->client_id);
                                $total_a_corriente = invoice_projecto($invoice->project_id,$invoice->program, $invoice->client_id) - discount($invoice->project_id,$invoice->client_id,$invoice->program) - $nc_notax ;
                              }else {
                                $total_a_corriente += pago_a($invoice->client_id,$invoice->project_id);
                              }
                              echo format_currency($total_a_corriente);
                              ?>
                            </div>
                            <div class="col-md-2">
                                <a style="color: #2196F3;" data-toggle="tooltip" title="PDF" href="<?php echo base_url('billing/pdf_download_program3/'.$invoice->project_id.'/'.$invoice->client_id); ?>"><i class="fa fa-print" aria-hidden="true"></i></a>
                            </div>
                            <div class="col-md-2">
                              <a style="color: #2196F3;" title="Editar Concepto" href="<?php echo base_url('billing/concept_edit_modal/'.$invoice->project_id.'/'.$invoice->client_id); ?>" data-toggle="zestModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            </div>
                          </td>
                          <td>
                            <div class="col-md-6">
                              $<?php
                              if($Totalpro_diafano == 0){
                                $nc_tax = nc_tax($invoice->project_id, $invoice->client_id);
                                $total_a_diafano = invoice_projecto_tax($invoice->project_id,$invoice->program, $invoice->client_id) - discount_tax($invoice->project_id,$invoice->client_id,$invoice->program) - $nc_tax;
                              }else {
                                $total_a_diafano += pago_a_diafano($invoice->client_id,$invoice->project_id);
                              }
                              echo format_currency($total_a_diafano);
                              ?>
                            </div>
                             <div class="col-md-2">
                                 <a style="color: #2196F3;" data-toggle="tooltip" title="PDF" href="<?php echo base_url('billing/pdf_download_program4/'.$invoice->project_id.'/'.$invoice->client_id); ?>"><i class="fa fa-print" aria-hidden="true"></i></a>
                             </div>
                             <div class="col-md-2">
                               <a style="color: #2196F3;" title="Editar Concepto" href="<?php echo base_url('billing/concept_edit_modal_tax/'.$invoice->project_id.'/'.$invoice->client_id); ?>" data-toggle="zestModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                             </div>
                          </td>
                        </tr>
                      <?php endif; endforeach; endif; ?>
                      <?php
                        if(count($transactions)): foreach($transactions as $totalespro): if($totalespro->client_id == $clients->client_id && $totalespro->program == 1 ):
                          // $Totalspro += pago_a($totalespro->client_id,$totalespro->project_id);
                          $eldescuentopro = discount_client($totalespro->client_id,$totalespro->program);
                          $totalstranspro += invoice_total($totalespro->id);
                          $otrototalpro = $totalstranspro - $eldescuentopro;
                          if ($totalespro->pago_a != 1 ) {
                            $totaltranspro += invoice_total($totalespro->id) - discount_client($totalespro->client_id,$totalespro->program);
                          }else {}
                          endif; endforeach; endif;
                          $pago_apro = $Totalpro + $totaltranspro;
                      ?>
                      <tr>
                        <td>
                          <strong><p class="text-right">Totales:</p></strong>
                        </td>
                        <td>
                          $<?php
                           echo format_currency($otrototalpro);
                          ?>
                        </td>
                        <td>
                          <strong><p class="text-info">$<?php echo format_currency($pago_apro); ?></p></strong>
                        </td>
                        <td>
                          $
                          <?php
                           echo format_currency($totalsubpro);
                           ?>
                        </td>
                        <td>
                          <?php $invoice_projecto += $total_a_corriente; ?>
                          $<?php echo format_currency($invoice_projecto); ?>
                        </td>
                        <td>
                          <?php $invoice_projecto_tax += $total_a_diafano; ?>
                          $<?php echo format_currency($invoice_projecto_tax); ?>
                        </td>
                      </tr>
                      </tbody>
                    </table>
                    <br>
                    <br>
                  <?php endforeach; endif; ?>
                  </div>
              </div>
              <div id="tab3" class="tab-pane">
                  <!-- <a class="btn btn-info" href="<?php echo base_url('billing/pdf_download_program_cuentas/' . $project_id); ?>">Descargar PDF<i class="fa fa-print"></i></a> -->
                <!-- <div class="row">
                  <img src="http://quattrosol.com/img2.jpeg" width="100%">
                </div> -->

                    <a class="btn btn-info" href="<?php echo base_url('billing/pdf_download_program_cuentas/'); ?>">Descargar PDF<i class="fa fa-print"></i></a>
                    <div>
                    <div class="table-responsive">

                        <?php
                          $totalpro = 0; $subtotalpro = 0; $Totalpro = 0; $totaltranspro = 0; $Totalpagopro = 0; $Totalsaldopro = 0; $pagotranspro = 0;
                          $pago_apro = 0; $Totalspro = 0; $totalstranspro = 0; $totalsubpro = 0; $corrientepro = 0; $diafanopro = 0; $pago_totalpro = 0;
                          $invoice_projecto = 0; $invoice_projecto_tax = 0; $desc_total = 0; $desc_total = 0; $invoice_projecto_total = 0;
                          $invoice_projecto_subtotal = 0;
                        ?>
                    <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Cuenta Corriente</h4></div>
                    <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success items_table_mano" id="zest_table11">
                      <thead class="the-box light full">
                        <tr>
                            <th style="width:20% !important;">
                            Proveedor
                          </th>
                          <th style="width:20% !important;">
                            Obra
                          </th>
                          <th style="width:14% !important;">
                            Total Presupuesto
                          </th>
                          <th style="width:14% !important;">
                            Pago autorizado
                          </th>
                          <th style="width:14% !important;">
                            Saldo
                          </th>
                          <th style="width:18% !important;">
                            Total Cuenta Corriente:
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if(count($projectos2)): foreach($projectos2 as $invoice): if($invoice->program == 1 && $invoice->tax == 0): ?>
                        <tr>
                          <td>
                            <?php echo project_company($invoice->project_id); ?>
                          </td>
                          <td>
                            <?php echo $name = client_company($invoice->client_id); ?>
                          </td>
                          <td>
                            $<?php echo format_currency(invoice_projecto($invoice->project_id,$invoice->program, $invoice->client_id) - discount($invoice->project_id,$invoice->client_id,$invoice->program)); ?>

                          </td>
                          <td>
                            <?php if($invoice->client_type == 3){?>
                              $<?php echo format_currency(pago_a($invoice->client_id, $invoice->project_id)); ?>
                            <?php }else{ ?>
                            $ <?php echo format_currency(invoice_projecto($invoice->project_id,$invoice->program, $invoice->client_id) - discount($invoice->project_id,$invoice->client_id,$invoice->program)); ?>
                          <?php } ?>
                          </td>
                          <td>
                            $<?php if($invoice->client_type == 3){?>
                              <?php
                                $Totalpro += pago_a($invoice->client_id,$invoice->project_id);
                                if($Totalpro == 0){
                                  $totalpro = invoice_projecto($invoice->project_id,$invoice->program, $invoice->client_id) - client_payments($invoice->client_id,
                                          'year'); ?>
                                <?php }else{  ?>
                              <?php $totalpro = invoice_projecto($invoice->project_id,$invoice->program, $invoice->client_id) - pago_a($invoice->client_id,$invoice->project_id);
                                  } ?>
                            <?php } else{ ?>
                            <?php $totalpro = invoice_projecto($invoice->project_id,$invoice->program, $invoice->client_id) - invoice_projecto($invoice->project_id,$invoice->program, $invoice->client_id); ?>
                            <?php } ?>
                            <?php $subtotalpro = $totalpro; echo format_currency($subtotalpro - discount($invoice->project_id,$invoice->client_id,$invoice->program));   ?>
                            <?php $totalsubpro += $subtotalpro - discount($invoice->project_id,$invoice->client_id,$invoice->program)?>
                          </td>
                          <td>
                            <div class="col-md-6">
                              $<?php if($invoice->client_type == 3){?>
                                <?php echo format_currency(pago_a($invoice->client_id, $invoice->project_id)); ?>
                              <?php }else{ ?>
                                <?php echo format_currency(invoice_projecto($invoice->project_id,$invoice->program, $invoice->client_id) - discount($invoice->project_id,$invoice->client_id,$invoice->program)); ?>
                              <?php } ?>
                            </div>
                          <div class="col-md-2">
                              <a style="color: #2196F3;" data-toggle="tooltip" title="PDF" href="<?php echo base_url('billing/pdf_download_program3/'.$invoice->project_id.'/'.$invoice->client_id); ?>"><i class="fa fa-print" aria-hidden="true"></i></a>
                          </div>
                          <div class="col-md-2">
                            <a style="color: #2196F3;" title="Editar Concepto" href="<?php echo base_url('billing/concept_edit_modal/'.$invoice->project_id.'/'.$invoice->client_id); ?>" data-toggle="zestModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                          </div>
                          </td>

                        </tr>
                      <?php endif; endforeach; endif; ?>
                      <?php
                      if(count($transactions)): foreach($transactions as $totalespro): if($totalespro->program == 1 && $totalespro->tax == 0 && $totalespro->type_pre != 5):
                        // $Totalspro += pago_a($totalespro->client_id,$totalespro->project_id);
                        $desc_cuenta = discount_client($totalespro->client_id,$totalespro->program);
                        $totaltranspro += invoice_total($totalespro->id);
                        $desc_total = $totaltranspro - $desc_cuenta;
                        if ($totalespro->pago_a != 1 ) {
                          $totalstranspro += invoice_total($totalespro->id) - discount($totalespro->project_id,$totalespro->client_id,$totalespro->program);
                          $invoice_projecto_total += invoice_total($totalespro->id);
                        }else {}
                        endif; endforeach; endif;
                          $pago_apro = $Totalpro + $totalstranspro;
                      ?>
                      <tr>
                          <td></td>
                        <td>
                          <strong><p class="text-right">Totales:</p></strong>
                        </td>
                        <td>
                          $<?php
                           echo format_currency($totaltranspro);
                          ?>
                        </td>
                        <td>
                          <strong><p class="text-info">$<?php echo format_currency($pago_apro); ?></p></strong>
                        </td>
                        <td>
                          $
                          <?php
                           echo format_currency($totalsubpro);
                           ?>
                        </td>
                        <td>
                          $<?php echo format_currency($pago_apro); ?>
                        </td>


                      </tr>
                      </tbody>
                    </table>

                  </div>
                  <div class="table-responsive">

                      <?php
                        $totalpro = 0; $subtotalpro = 0; $Totalpro = 0; $totaltranspro = 0; $Totalpagopro = 0; $Totalsaldopro = 0; $pagotranspro = 0;
                        $pago_apro = 0; $Totalspro = 0; $totalstranspro = 0; $totalsubpro = 0; $corrientepro = 0; $diafanopro = 0; $pago_totalpro = 0;
                        $invoice_projecto = 0; $invoice_projecto_tax = 0; $total_cuenta = 0; $total_cuenta = 0; $nc_tax = 0; $nc_notax = 0;
                      ?>
                    <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Espacio Diafano S.A. de C.V.</h4></div>
                    <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success items_table_mano" id="zest_table10">
                      <thead class="the-box light full">
                       <tr>
                            <th style="width:20% !important;">
                            Proveedor
                          </th>
                          <th style="width:20% !important;">
                            Obra
                          </th>
                          <th style="width:14% !important;">
                            Total Presupuesto
                          </th>
                          <th style="width:14% !important;">
                            Pago autorizado
                          </th>
                          <th style="width:14% !important;">
                            Saldo
                          </th>
                          <th style="width:18% !important;">
                            Total Espacio Diafano S.A. de C.V.
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if(count($projectos2)): foreach($projectos2 as $invoice): if($invoice->program == 1 && $invoice->tax == 16): ?>
                        <tr>
                          
                           <td style="width:20% !important;">
                            <?php echo $name = client_company($invoice->client_id); ?>
                          </td>
                          <td style="width:20% !important;">
                            <?php echo project_company($invoice->project_id); ?>
                          </td>
                          <td style="width:15% !important;">
                            <?php $nc_tax = nc_tax($invoice->project_id, $invoice->client_id) ?>
                            $<?php echo format_currency(invoice_projecto_tax($invoice->project_id,$invoice->program, $invoice->client_id) - discount_tax($invoice->project_id,$invoice->client_id,$invoice->program) - $nc_tax); ?>

                          </td>
                          <td style="width:15% !important;">
                            <?php if($invoice->client_type == 3){?>
                              $<?php echo format_currency(pago_a_diafano($invoice->client_id, $invoice->project_id)); ?>
                            <?php }else{ ?>
                            $ <?php echo format_currency(invoice_projecto_tax($invoice->project_id,$invoice->program, $invoice->client_id) - discount_tax($invoice->project_id,$invoice->client_id,$invoice->program) - $nc_tax); ?>
                          <?php } ?>
                          </td>
                          <td style="width:15% !important;">
                            $<?php if($invoice->client_type == 3){?>
                              <?php
                                $Totalpro += pago_a_diafano($invoice->client_id,$invoice->project_id);
                                if($Totalpro == 0){
                                  $totalpro = invoice_projecto_tax($invoice->project_id,$invoice->program, $invoice->client_id) - client_payments($invoice->client_id,
                                          'year'); ?>
                                <?php }else{  ?>
                              <?php $totalpro = invoice_projecto_tax($invoice->project_id,$invoice->program, $invoice->client_id) - pago_a_diafano($invoice->client_id,$invoice->project_id);
                                  } ?>
                            <?php } else{ ?>
                            <?php $totalpro = invoice_projecto_tax($invoice->project_id,$invoice->program, $invoice->client_id) - invoice_projecto_tax($invoice->project_id,$invoice->program, $invoice->client_id); ?>
                            <?php } ?>
                            <?php $subtotalpro = $totalpro; echo format_currency($subtotalpro - discount_tax($invoice->project_id,$invoice->client_id,$invoice->program));   ?>
                            <?php $totalsubpro += $subtotalpro - discount_tax($invoice->project_id,$invoice->client_id,$invoice->program)?>
                          </td>

                          <td style="width:15% !important;">
                            <div class="col-md-6">
                              <?php if($invoice->client_type == 3){?>
                                $<?php echo format_currency(pago_a_diafano($invoice->client_id, $invoice->project_id)); ?>
                              <?php }else{ ?>
                                <?php $nc_tax = nc_tax($invoice->project_id, $invoice->client_id); ?>
                                $ <?php echo format_currency(invoice_projecto_tax($invoice->project_id,$invoice->program, $invoice->client_id) - discount_tax($invoice->project_id,$invoice->client_id,$invoice->program) - $nc_tax); ?>
                              <?php } ?>
                            </div>
                          <div class="col-md-2">
                              <a style="color: #2196F3;" data-toggle="tooltip" title="PDF" href="<?php echo base_url('billing/pdf_download_program4/'.$invoice->project_id.'/'.$invoice->client_id); ?>"><i class="fa fa-print" aria-hidden="true"></i></a>
                          </div>
                          <div class="col-md-2">
                            <a style="color: #2196F3;" title="Editar Concepto" href="<?php echo base_url('billing/concept_edit_modal_tax/'.$invoice->project_id.'/'.$invoice->client_id); ?>" data-toggle="zestModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                          </div>
                          </td>
                        </tr>
                      <?php endif; endforeach; endif; ?>
                      <?php
                        if(count($transactions)): foreach($transactions as $totalespro): if($totalespro->program == 1 && $totalespro->tax == 16 && $totalespro->type_pre != 5):
                          // $Totalspro += pago_a($totalespro->client_id,$totalespro->project_id);
                          $nc_tax = nc_tax($invoice->project_id, $invoice->client_id);
                          $descuento_cuenta = discount_client($totalespro->client_id,$totalespro->program);
                          $totalstranspro += invoice_total($totalespro->id);
                          $total_cuenta = $totalstranspro - $descuento_cuenta - $nc_tax;
                          if ($totalespro->pago_a != 1 ) {
                            $totaltranspro += invoice_total($totalespro->id) - discount($totalespro->project_id,$totalespro->client_id,$totalespro->program);
                          }else {}
                          endif; endforeach; endif;
                          $pago_apro = $Totalpro + $totaltranspro - $nc_tax;
                      ?>
                      <tr>
                          <td></td>
                        <td>
                          <strong><p class="text-right">Totales:</p></strong>
                        </td>
                        <td>
                          $<?php
                           echo format_currency($total_cuenta);
                          ?>
                        </td>
                        <td>
                          <strong><p class="text-info">$<?php echo format_currency($pago_apro); ?></p></strong>
                        </td>
                        <td>
                          $
                          <?php
                           echo format_currency($totalsubpro);
                           ?>
                        </td>
                        <td>
                          $<?php echo format_currency($pago_apro); ?>
                        </td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
