<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/25
 * Time: 02:13 PM
 */
?>

<div class="the-box">
    <div class="row">
        <div class="col-md-12">
            <!-- <div class="col-md-3">
                <ul id="cssmenu" class="ver-inline-menu">
                    <li class="<?php echo ($active_page == 'overview') ? "active" : ""; ?>"><a
                            href="<?php echo base_url('billing/?status=overview'); ?>"><i
                                class="fa fa-bar-chart-o"></i>Reporte</a></li>

                    <li class="<?php echo ($active_page == 'transaction') ? "active" : ""; ?>"><a
                                    href="<?php echo base_url('billing/?status=transaction'); ?>"><i
                                        class="fa fa-money"></i>Transacciones</a></li>


                    <li><a href="javascript:;"><i class="fa fa-plus"></i>Pagos</a>
                        <ul>
                            <li>
                                <a href="<?php echo base_url('billing/?status=all'); ?>"><i
                                        class="fa  fa-briefcase"></i>Todos Los Pagos</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('billing/?status=paid'); ?>"><i
                                        class="fa fa-sign-in"></i><?php echo lang('label_paid'); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('billing/?status=unpaid'); ?>"><i
                                        class="fa fa-sign-in"></i><?php echo lang('label_unpaid'); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('billing/?status=overdue'); ?>"><i
                                        class="fa fa-sign-in"></i><?php echo lang('label_overdue'); ?></a>
                            </li>
                        </ul>


                    </li>


                </ul>
            </div> -->
            <div class="col-md-12">
                <fieldset>

                    <?php if ($show_print) { ?>

                        <legend><i class="fa fa-bar-chart-o"></i><?php echo $page_title; ?>
                            <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) { ?>
                                <div class="btn-group btn-group-sm pull-right">
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-tag"></i>
                                        Más opciones
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a title="Email Invoice"
                                               href="<?php echo base_url('billing/email_invoice/' . $invoice_id); ?>">Enviar Orden por correo</a>
                                        </li>
                                        <li>
                                            <a title="Send Reminder"
                                               href="<?php echo base_url('billing/reminder/' . $invoice_id); ?>">Enviar recordatorio</a>
                                        </li>

                                        <li class="divider"></li>
                                        <li>
                                            <a href="<?php echo base_url('billing/pdf_download/' . $invoice_id); ?>">Descargar Orden</a>
                                        </li>

                                    </ul>
                                </div>
                            <?php } ?>
                            <div class="pull-right">
                                <div class="btn-group btn-group-sm" role="group">
                                    <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) { ?>
                                        <div class="btn-group pull-left">
                                            <a class="btn text-danger" style="color:#828282;"data-toggle="zestModal"
                                               title="<?php echo lang('label_delete'); ?>"
                                               href="<?php echo base_url('billing/delete/' . $invoice_id); ?>"><i
                                                    class="fa fa-trash  fa-lg"></i></a>
                                        </div>
                                    <?php } ?>
                                    <div class="btn-group pull-left">
                                        <a class="btn text-info" data-toggle="tooltip"
                                           title="<?php echo lang('label_download'); ?>"
                                           href="<?php echo base_url('billing/pdf_preview/' . $invoice_id); ?>"><i
                                                class="fa fa-print  fa-lg"></i></a>
                                    </div>
                                </div>
                            </div>


                        </legend>


                    <?php } else { ?>
                        <legend><i class="fa fa-bar-chart-o"></i><?php echo $page_title; ?>


                        </legend>
                        <?php
                        $client_id = ($this->CompanyClient_user_id > 0) ? $this->CompanyClient_id : null;
                        ?>
                        <div class="the-box contentbox" style="font-size:18px;">
                            <?php echo lang('label_paid'); ?>:
                        <span class="textgreen">
                            <b><?php echo format_currency(stats_amount_paid($client_id)); ?></b>
                        </span>
                            <?php echo lang('label_unpaid'); ?>:
                        <span class="textred">
                            <b><?php echo format_currency(stats_amount_unpaid($client_id)); ?></b>
                        </span>
                            <?php echo lang('label_overdue'); ?>:
                        <span class="textblack">
                            <b><?php echo format_currency(stats_amount_overdue($client_id)); ?></b>
                        </span>
                            <?php echo lang('label_partially_paid'); ?>:
                        <span class="textred">
                            <b><?php echo format_currency(stats_amount_partial_paid($client_id)); ?></b>
                        </span>
                            <br>

                        </div>
                    <?php } ?>
                    <?php echo ($_PAGE) ? $_PAGE : null; ?>

                </fieldset>

            </div>
        </div>
        <!-- /.row -->
    </div>
</div>
