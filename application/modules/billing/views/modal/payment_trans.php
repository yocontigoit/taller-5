<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog show" id="myModal" >
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Pago autorizado</h4>
        </div>
        <form method="post" action="<?php echo base_url('billing/created_trans'); ?>">
            <?php
                $monto = 0;
                $project_id = 0;
                $client_id = 0;
                $total = 0;
                if(count($pago_a)): foreach($pago_a as $pago): if($pago->trans == 1 && $pago->old != 1):
                  $total += $pago->monto;
                endif; endforeach; endif;
                if($transactions): foreach($transactions as $transaction): if($transaction->trans == 1):
                    if ($transaction->pago_a == 1 ):
                      $monto = $total;
                      $project_id = $transaction->project_id;
                      $client_id = $transaction->client_id;
                    endif;
                    if ($transaction->pago_a ==  0):
                      $monto += invoice_total($transaction->id);
                      $project_id = $transaction->project_id;
                      $client_id = $transaction->client_id;
                    endif;
                endif; endforeach; endif;
            ?>
                <select data-placeholder="Select users..."
                class="form-control chosen-select" multiple tabindex="4"
                name="factura[]" >
                  <optgroup label="Facturas">
                    <?php if($transactions): foreach($transactions as $transaction): if($transaction->trans == 1):  ?>
                      <option value="<?php echo $transaction->invoice; ?>" selected><?php echo $transaction->invoice; ?></option>
                    <?php endif; endforeach; endif;?>
                  </optgroup>
                </select>
                <div class="" style="display:none">
                  <select data-placeholder="Select users..."
                  class="form-control chosen-select" multiple
                  name="inv_id[]">
                    <optgroup label="inv_id">
                      <?php if($transactions): foreach($transactions as $transaction): if($transaction->trans == 1):  ?>
                        <option value="<?php echo $transaction->id; ?>" selected><?php echo $transaction->id; ?></option>
                      <?php endif; endforeach; endif;?>
                    </optgroup>
                  </select>
                </div>
                <div class="modal-body">
                    <div class="row">
                      <input type="hidden" name="project_id" value="<?php echo $project_id; ?>">
                      <input type="hidden" name="client_id" value="<?php echo $client_id; ?>">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="payment_date"><?php echo lang('label_date'); ?></label>
                            <input type="text" class="form-control datepicker" name="date"
                                   data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                   value="<?php echo date('d-m-Y') ?>">
                          </div>
                        </div>
                        <div class="col-md-6" id="nTrans" style="display:none;">
                          <h3 class="bolded tiles-number text-info"><?php echo $invoice_number; ?></h3>
                          <input type="hidden" id="folio" name="folio" value="">
                        </div>
                        <div class="col-md-6" id="nCheck" style="display:none;">
                          <div class="form-group">
                            <label for="payment_date">Cheque</label>
                            <input type="text" class="form-control" name="cheque" value="">
                          </div>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label for="taskPriority"><?php echo lang('label_payment_method'); ?> </label>

                            <select class="form-control" name="payment_method" onChange="pagoOnChange(this)" required title="Seleccione un metodo de pago !">
                                <option value="">Ninguno</option>

                                <?php foreach ($this->config->item('payment') as $payment): ?>
                                    <option
                                        value="<?php echo $payment->payment_code; ?>"><?php echo $payment->payment_name. " ".$payment->payment_metod; ?></option>
                                <?php endforeach; ?>

                            </select>
                            <span class="help-block">Efectivo, tarjeta de crédito..</span>
                        </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="payment_date">Monto</label>
                            <input type="text" class="form-control" name="amount" value="<?php echo $monto; ?>" readonly>
                          </div>
                        </div>
                      </div>
                    </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    Pagar
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </button>
            </div>
        </form>
    </div>
</div>
</div>

<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>

<script type="text/javascript">
    $('#tasks-percent').slider({
        formater: function (value) {
            return value + '% Complete..';
        }
    });

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }

    function pagoOnChange(sel) {

      if (sel.value=="2"){
           $("#nTrans").show();
           $("#nCheck").hide();
           $("#folio").val("<?php echo $invoice_number; ?>")
      }

      if(sel.value=="3"){
           $("#nCheck").show();
           $("#nTrans").hide();
           $("#folio").val(0)
      }

      if (sel.value != "2" && sel.value != "3" ){
           $("#nTrans").hide();
           $("#nCheck").hide();

      }

    }
</script>
