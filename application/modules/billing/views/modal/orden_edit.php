<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Editar Orden</h4>
        </div>
        <form method="post" action="<?php echo base_url('billing/billing/update_orden'); ?>">
          <input type="hidden" name="id" value="<?php echo $invoice_items->id; ?>">
          <input type="hidden" name="invoice_id" value="<?php echo $invoice_items->invoice_id; ?>">

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="taskTitle">Concepto</label>
                            <input type="text" name="item_name" class="form-control" value="<?php echo $invoice_items->item_name; ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="taskTitle">P.U.</label>
                        <input type="text" name="price" class="form-control" value="<?php echo $invoice_items->price; ?>">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="taskTitle">Avance</label>
                          <select name="avance" class="form-control">
                              <option value="">3%</option>
                              <?php if(count($milestones)): foreach( $milestones as $milestone ):?>
                              <option value="<?php echo $milestone->milestone_name;?>"><?php echo $milestone->milestone_name;?></option> <?php
                              endforeach; endif;?>
                          </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="taskTitle">IVA</label>
                        <select class="form-control" name="tax">
                          <option selected disabled>Seleccione...</option>
                          <option value="16">Con IVA</option>
                          <option value="0">Sin IVA</option>
                        </select>
                      </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="taskTitle">	Descripción</label>
                          <input type="text" name="item_desc" class="form-control" value="<?php echo $invoice_items->item_desc; ?>">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="taskTitle">Cantidad</label>
                          <input type="text" name="quantity" class="form-control" value="<?php echo $invoice_items->quantity; ?>">
                      </div>
                    </div>
                </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" value="addNewTask" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    <?php echo lang('form_button_save'); ?>
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </button>
            </div>
        </form>
    </div>
</div>
</div>

<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>

<script type="text/javascript">
    $('#tasks-percent').slider({
        formater: function (value) {
            return value + '% Complete..';
        }
    });

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }
</script>
