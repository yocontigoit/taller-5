<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-md">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Agregar Descuento</h4>
        </div>
        <form method="post" action="<?php echo base_url('billing/billing/add_new_discount'); ?>">
          <input type="hidden" name="invoice_id" value="<?php echo $invoice_details->id; ?> ">
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-md-12">
                      <div class="form-group">
                          <label for="taskTitle">	Descripción</label>
                          <input type="text" name="disc_desc" class="form-control" value="">
                      </div>
</div></div>
<div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="taskTitle">Monto por cantidad</label>
                        <input type="text" name="disc_amount" class="form-control" value="0">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="taskTitle">Monto por porcentaje</label>
                        <input type="text" name="disc_amount_per" class="form-control" value="0">
                      </div>
                    </div>
                    
                </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon"  name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    Agregar
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    Cancelar
                </button>
            </div>
        </form>
    </div>
</div>
</div>

<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>

<script type="text/javascript">
    $('#tasks-percent').slider({
        formater: function (value) {
            return value + '% Complete..';
        }
    });

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }
</script>
