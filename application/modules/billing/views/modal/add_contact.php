<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/02
 * Time: 11:22 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><?= lang('add_contact') ?></h4>
        </div><?php
        $attributes = array('class' => 'bs-example form-horizontal');
        echo form_open(base_url() . 'auth/register_user', $attributes); ?>

        <?php echo validation_errors(); ?>
        <div class="modal-body">
            <input type="hidden" name="r_url" value="">
            <input type="hidden" name="company" value="">
            <input type="hidden" name="role" value="2">

            <div class="form-group">
                <label class="col-lg-4 control-label">Name <span class="text-danger">*</span></label>

                <div class="col-lg-8">
                    <input type="text" class="form-control" value="" placeholder="E.g John Doe" name="fullname"
                           required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label">Username <span class="text-danger">*</span></label>

                <div class="col-lg-8">
                    <input type="text" class="form-control" value="" placeholder="johndoe" name="username" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label">Email</label>

                <div class="col-lg-8">
                    <input type="email" class="form-control" value="" placeholder="me@domain.com" name="email" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label">Pass</label>

                <div class="col-lg-8">
                    <input type="password" class="form-control" value="" name="password">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label">Pass </label>

                <div class="col-lg-8">
                    <input type="password" class="form-control" value="" name="confirm_password">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label">asdasdas</label>

                <div class="col-lg-8">
                    <input type="text" class="form-control" value="" name="phone" placeholder="+52 782 983 434">
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label">asdasdas </label>

                <div class="col-lg-8">
                    <textarea name="address" class="form-control"></textarea>
                </div>
            </div>

        </div>
        <div class="modal-footer"><a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            <button type="submit" class="btn btn-primary">Add</button>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->