<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<link href="http://dci-innovacion.com/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/dashboard.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/sb-admin.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/default.css" rel="stylesheet" type="text/css" />
<link href="http://dci-innovacion.com/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Agregar Gasto</h4>
        </div>
        <form method="post" action="<?php echo base_url('expenses/add_gasto/'); ?>">
            <input type="hidden" name="estimacion_id"  value"" />
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Obra</label>

                              <select data-placeholder="Select client..." class="form-control  chosen-select" tabindex="4"
                                      name="project" id="project">
                                  <option value="Empty">&nbsp;</option>
                                  <optgroup label="Obras">
                                      <?php if (count($projects)):foreach ($projects as $client): ?>
                                          <option
                                              value="<?php echo $client->project_id; ?>"><?php echo ucfirst($client->project_title) ?></option>
                                      <?php endforeach; endif; ?>
                                  </optgroup>
                              </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="taskPriority">Proveedor </label>

                          <select class="form-control chosen-select" name="client_id">
                              <option value="">Ninguno</option>
                              <?php if (count($clients)) : foreach ($clients as $client) : ?>
                                  <option
                                      value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name) ?></option>
                              <?php endforeach; endif; ?>
                          </select>
                      </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <div id="project_milestone">

                      </div>
                    </div>
                    <h2><p class="text-info"><?php echo $invoice_number; ?></p></h2>
                    <input type="hidden" name="folio" value="<?php echo $invoice_number; ?>">
                      <!-- <div class="form-group">
                          <label for="taskPriority"><?php echo lang('label_payment_method'); ?> </label>

                          <select class="form-control chosen-select" name="method">
                              <option value="">Ninguno</option>

                                  <option
                                      value="1">Efectivo</option>
                                      <option
                                      value="1">Transaccion</option>
                                      <option
                                      value="1">Cheque</option>


                          </select>
                          <span class="help-block">Efectivo, tarjeta de crédito..</span>
                      </div> -->
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="payment_date">Monto</label>
                        <input type="text" class="form-control" name="amount" value="">
                      </div>

                      <div class="form-group">
                        <label for="payment_date">Descripción</label>
                        <input type="text" class="form-control" name="desc" value="">
                      </div>
                    </div>
                  </div>
                </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    Agregar gasto
                </button>
                <a href="https://proyectosinternos.com/Taller5/expenses/?status=estimaciones" class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </a>
            </div>
        </form>
    </div>
</div>
</div>

<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>
<script type="text/javascript">
    $('#tasks-percent').slider({
        formater: function (value) {
            return value + '% Complete..';
        }
    });

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {

        $(selector).chosen(configChosen[selector]);
    }

    $('body').on('change','#project', function(e) {
        e.preventDefault();


        var project_id = $('select[name="project"]').val();
        if (project_id == 'Empty') {
            return;
        }
        $.post('<?php echo site_url('billing/ajax_project_milestone'); ?>', { project_id: project_id }, function(data){
            $('#project_milestone').html(data);

        });
    });
</script>
