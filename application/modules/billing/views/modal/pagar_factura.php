<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Asignar Factura</h4>
        </div>
        <form method="post" action="<?php echo base_url('billing/add_payment2'); ?>">
            <input class="form-control" type="hidden" value="0" name="payment_id" required="">
            <input type="hidden" name="payment_send_mail"  value"0" />
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="taskTitle">Ordenes de Compra</label>
                            <select data-placeholder="Select client..." class="form-control chosen-select"
                                    tabindex="4" name="invoice_id" id="invoice_id">
                                <option value="Empty">&nbsp;</option>
                                <optgroup label="Ordenes">
                                      <?php if (count($transactions)) : foreach ($transactions as $transaction) : if($transaction->factura == 0 && $transaction->type_pre == 0) :

                                      $client = get_client_details($transaction->client_id);
                                       $project = get_project_details($transaction->project_id);

                                      ?>
                                        <option
                                            value="<?php echo $transaction->id; ?>">
                                            <b><?php echo ucfirst($transaction->inv_no); ?></b>, <?php echo $client->client_name; echo "      "; echo $project->project_title; echo "     $";  echo format_currency(invoice_total($transaction->id)); ?></option>
                                    <?php endif; endforeach; endif; ?>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                    <input type="hidden" class="form-control" name="reference" value="0">



                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="payment_date">Factura</label>
                        <input type="text" class="form-control" name="invoice" value="">
                      </div>
                    </div>
                  </div>
                </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    Factura asignada
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </button>
            </div>
        </form>
    </div>
</div>
</div>

<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>

<script type="text/javascript">
    $('#tasks-percent').slider({
        formater: function (value) {
            return value + '% Complete..';
        }
    });

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }
</script>
