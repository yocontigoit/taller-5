<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Pago autorizado</h4>
        </div>
        <form method="post" action="<?php echo base_url('billing/pago_a'); ?>">
            <input class="form-control" type="hidden" value="0" name="payment_id" required="">
            <input type="hidden" name="payment_send_mail"  value"0" />
            <input type="hidden" name="inv_id" value="<?php echo $inv_id ?>">
            <input type="hidden" name="client_id" value="<?php echo $client_id ?>">
            <input type="hidden" name="project_id" value="<?php echo $project_id ?>">
            <input type="hidden" name="tax" value="<?php echo $tax ?>">
            <div class="modal-body">
                <strong><h3><p class="text-center"><?php echo $transactions->inv_no ?></p></h3></strong>
                    <div class="row">
                    <input type="hidden" class="form-control" name="reference" value="0">
                    <?php
                      $pagos = 0;
                      $subtotal = 0;
                      if (count($pago_a)): foreach ($pago_a as $pago): if($pago->client_id == $client_id && $pago->project_id == $project_id):
                        $pagos += $pago->monto;
                      endif; endforeach; endif;
                      $subtotal = invoice_total_proveedor_modal($transactions->project_id,$transactions->client_id);
                      $total = $subtotal - $pagos;
                    ?>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="payment_date">Monto a pagar</label>
                        <input type="text" class="form-control" name="monto" value="<?php echo $total; ?>">
                      </div>
                    </div>
                  </div>
                </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    Guardar
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </button>
            </div>
        </form>
    </div>
</div>
</div>

<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>

<script type="text/javascript">
    $('#tasks-percent').slider({
        formater: function (value) {
            return value + '% Complete..';
        }
    });

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }
</script>
