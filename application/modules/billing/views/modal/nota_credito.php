<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Nota de Credito</h4>
        </div>
        <form method="post" action="<?php echo base_url('billing/add_nota_credito/'); ?>">
            <input type="hidden" name="estimacion_id"  value"" />
            <div class="modal-body">
                <h2><p class="text-info"><?php echo $invoice_number; ?></p></h2>
                <div class="row">

                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Obra</label>

                              <select data-placeholder="Select client..." class="form-control  chosen-select" tabindex="4"
                                      name="project" id="project_id">
                                  <option value="Empty">&nbsp;</option>
                                  <optgroup label="Obras">
                                      <?php if (count($projects)):foreach ($projects as $client): ?>
                                          <option
                                              value="<?php echo $client->project_id; ?>"><?php echo ucfirst($client->project_title) ?></option>
                                      <?php endforeach; endif; ?>
                                  </optgroup>
                              </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="taskPriority">Proveedor </label>

                          <select class="form-control chosen-select" name="client_id" id="client_id">
                              <option value="">Ninguno</option>
                              <?php if (count($clients)) : foreach ($clients as $client) : ?>
                                  <option
                                      value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name) ?></option>
                              <?php endforeach; endif; ?>
                          </select>
                      </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-6">

                    <input type="hidden" name="folio" value="<?php echo $invoice_number; ?>">
                    <div class="form-group">
                          <label for="taskPriority">Factura Asignada</label>

                      <div id="project_orders"></div>
                      </div>
                      <div id="diafano_corriente">

                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="payment_date">Monto</label>
                        <input type="text" class="form-control" name="amount" value="">
                      </div>
                    </div>
                  </div>
                </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    Agregar nota
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </button>
            </div>
        </form>
    </div>
</div>
</div>

<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>

<script type="text/javascript">
    $('#tasks-percent').slider({
        formater: function (value) {
            return value + '% Complete..';
        }
    });

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }

    $('body').on('change', '#client_id', '#project_id', function(e) {
        e.preventDefault();


        var project_id = $('select[name="project"]').val();
        var client_id = $('select[name="client_id"]').val();
        if (project_id == 'Empty' && client_id == 'Empty') {
            return;
        }
        $.post('<?php echo site_url('billing/ajax_project_orders'); ?>', { project_id: project_id, client_id: client_id }, function(data){
            $('#project_orders').html(data);

        });
    });
</script>
