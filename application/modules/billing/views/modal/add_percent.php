<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Asignar Factura</h4>
        </div>
        <form method="post" action="<?php echo base_url('billing/add_payment2'); ?>">
            <input class="form-control" type="hidden" value="0" name="payment_id" required="">
            <input type="hidden" name="payment_send_mail"  value"0" />
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="taskTitle">Avance</label>
                            <select data-placeholder="Select client..." class="form-control  chosen-select" tabindex="4"
                                    name="avance" id="project">
                                <option value="Empty">&nbsp;</option>
                                <optgroup label="Avances">
                                    <?php if (count($milestones)):foreach ($milestones as $mileston): ?>
                                        <option
                                            value="<?php echo $mileston->id; ?>"><?php echo $mileston->milestone_name ?></option>
                                    <?php endforeach; endif; ?>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="taskTitle">Porcentaje</label>
                        <select data-placeholder="Selecciona..." class="form-control chosen-select"
                                    tabindex="4" name="porcentaje" id="invoice_id">
                                <option value="Empty">5%</option>
                                <option value="Empty">10%</option>
                                <option value="Empty">15%</option>
                                <option value="Empty">20%</option>
                                <option value="Empty">25%</option>
                                <option value="Empty">30%</option>
                                <option value="Empty">35%</option>
                                <option value="Empty">40%</option>
                                <option value="Empty">45%</option>
                                <option value="Empty">50%</option>
                                <option value="Empty">55%</option>
                                <option value="Empty">60%</option>
                                <option value="Empty">65%</option>
                                <option value="Empty">70%</option>
                                <option value="Empty">75%</option>
                                <option value="Empty">80%</option>
                                <option value="Empty">85%</option>
                                <option value="Empty">90%</option>
                                <option value="Empty">95%</option>
                                <option value="Empty">100%</option>
                            </select>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="taskTitle">Avance</label>
                            <select data-placeholder="Select client..." class="form-control  chosen-select" tabindex="4"
                                    name="avance" id="project">
                                <option value="Empty">&nbsp;</option>
                                <optgroup label="Obras">
                                    <?php if (count($milestones)):foreach ($milestones as $mileston): ?>
                                        <option
                                            value="<?php echo $mileston->id; ?>"><?php echo $mileston->milestone_name ?></option>
                                    <?php endforeach; endif; ?>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="taskTitle">Porcentaje</label>
                        <select data-placeholder="Selecciona..." class="form-control chosen-select"
                                    tabindex="4" name="invoice_id" id="invoice_id">
                                <option value="Empty">5%</option>
                                <option value="Empty">10%</option>
                                <option value="Empty">15%</option>
                                <option value="Empty">20%</option>
                                <option value="Empty">25%</option>
                                <option value="Empty">30%</option>
                                <option value="Empty">35%</option>
                                <option value="Empty">40%</option>
                                <option value="Empty">45%</option>
                                <option value="Empty">50%</option>
                                <option value="Empty">55%</option>
                                <option value="Empty">60%</option>
                                <option value="Empty">65%</option>
                                <option value="Empty">70%</option>
                                <option value="Empty">75%</option>
                                <option value="Empty">80%</option>
                                <option value="Empty">85%</option>
                                <option value="Empty">90%</option>
                                <option value="Empty">95%</option>
                                <option value="Empty">100%</option>
                            </select>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="taskTitle">Avance</label>
                            <select data-placeholder="Select client..." class="form-control  chosen-select" tabindex="4"
                                    name="avance" id="project">
                                <option value="Empty">&nbsp;</option>
                                <optgroup label="Obras">
                                    <?php if (count($milestones)):foreach ($milestones as $mileston): ?>
                                        <option
                                            value="<?php echo $mileston->id; ?>"><?php echo $mileston->milestone_name ?></option>
                                    <?php endforeach; endif; ?>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="taskTitle">Porcentaje</label>
                        <select data-placeholder="Selecciona..." class="form-control chosen-select"
                                    tabindex="4" name="invoice_id" id="invoice_id">
                                <option value="Empty">5%</option>
                                <option value="Empty">10%</option>
                                <option value="Empty">15%</option>
                                <option value="Empty">20%</option>
                                <option value="Empty">25%</option>
                                <option value="Empty">30%</option>
                                <option value="Empty">35%</option>
                                <option value="Empty">40%</option>
                                <option value="Empty">45%</option>
                                <option value="Empty">50%</option>
                                <option value="Empty">55%</option>
                                <option value="Empty">60%</option>
                                <option value="Empty">65%</option>
                                <option value="Empty">70%</option>
                                <option value="Empty">75%</option>
                                <option value="Empty">80%</option>
                                <option value="Empty">85%</option>
                                <option value="Empty">90%</option>
                                <option value="Empty">95%</option>
                                <option value="Empty">100%</option>
                            </select>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="taskTitle">Avance</label>
                            <select data-placeholder="Select client..." class="form-control  chosen-select" tabindex="4"
                                    name="avance" id="project">
                                <option value="Empty">&nbsp;</option>
                                <optgroup label="Obras">
                                    <?php if (count($milestones)):foreach ($milestones as $mileston): ?>
                                        <option
                                            value="<?php echo $mileston->id; ?>"><?php echo $mileston->milestone_name ?></option>
                                    <?php endforeach; endif; ?>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="taskTitle">Porcentaje</label>
                        <select data-placeholder="Selecciona..." class="form-control chosen-select"
                                    tabindex="4" name="invoice_id" id="invoice_id">
                                <option value="Empty">5%</option>
                                <option value="Empty">10%</option>
                                <option value="Empty">15%</option>
                                <option value="Empty">20%</option>
                                <option value="Empty">25%</option>
                                <option value="Empty">30%</option>
                                <option value="Empty">35%</option>
                                <option value="Empty">40%</option>
                                <option value="Empty">45%</option>
                                <option value="Empty">50%</option>
                                <option value="Empty">55%</option>
                                <option value="Empty">60%</option>
                                <option value="Empty">65%</option>
                                <option value="Empty">70%</option>
                                <option value="Empty">75%</option>
                                <option value="Empty">80%</option>
                                <option value="Empty">85%</option>
                                <option value="Empty">90%</option>
                                <option value="Empty">95%</option>
                                <option value="Empty">100%</option>
                            </select>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="taskTitle">Avance</label>
                            <select data-placeholder="Select client..." class="form-control  chosen-select" tabindex="4"
                                    name="avance" id="project">
                                <option value="Empty">&nbsp;</option>
                                <optgroup label="Obras">
                                    <?php if (count($milestones)):foreach ($milestones as $mileston): ?>
                                        <option
                                            value="<?php echo $mileston->id; ?>"><?php echo $mileston->milestone_name ?></option>
                                    <?php endforeach; endif; ?>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="taskTitle">Porcentaje</label>
                        <select data-placeholder="Selecciona..." class="form-control chosen-select"
                                    tabindex="4" name="invoice_id" id="invoice_id">
                                <option value="Empty">5%</option>
                                <option value="Empty">10%</option>
                                <option value="Empty">15%</option>
                                <option value="Empty">20%</option>
                                <option value="Empty">25%</option>
                                <option value="Empty">30%</option>
                                <option value="Empty">35%</option>
                                <option value="Empty">40%</option>
                                <option value="Empty">45%</option>
                                <option value="Empty">50%</option>
                                <option value="Empty">55%</option>
                                <option value="Empty">60%</option>
                                <option value="Empty">65%</option>
                                <option value="Empty">70%</option>
                                <option value="Empty">75%</option>
                                <option value="Empty">80%</option>
                                <option value="Empty">85%</option>
                                <option value="Empty">90%</option>
                                <option value="Empty">95%</option>
                                <option value="Empty">100%</option>
                            </select>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="taskTitle">Avance</label>
                            <select data-placeholder="Select client..." class="form-control  chosen-select" tabindex="4"
                                    name="avance" id="project">
                                <option value="Empty">&nbsp;</option>
                                <optgroup label="Obras">
                                    <?php if (count($milestones)):foreach ($milestones as $mileston): ?>
                                        <option
                                            value="<?php echo $mileston->id; ?>"><?php echo $mileston->milestone_name ?></option>
                                    <?php endforeach; endif; ?>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="taskTitle">Porcentaje</label>
                        <select data-placeholder="Selecciona..." class="form-control chosen-select"
                                    tabindex="4" name="invoice_id" id="invoice_id">
                                <option value="Empty">5%</option>
                                <option value="Empty">10%</option>
                                <option value="Empty">15%</option>
                                <option value="Empty">20%</option>
                                <option value="Empty">25%</option>
                                <option value="Empty">30%</option>
                                <option value="Empty">35%</option>
                                <option value="Empty">40%</option>
                                <option value="Empty">45%</option>
                                <option value="Empty">50%</option>
                                <option value="Empty">55%</option>
                                <option value="Empty">60%</option>
                                <option value="Empty">65%</option>
                                <option value="Empty">70%</option>
                                <option value="Empty">75%</option>
                                <option value="Empty">80%</option>
                                <option value="Empty">85%</option>
                                <option value="Empty">90%</option>
                                <option value="Empty">95%</option>
                                <option value="Empty">100%</option>
                            </select>
                    </div>
                    </div>

                    <div class="row">
                    <input type="hidden" class="form-control" name="reference" value="0">

                  </div>
                </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    Guardar Avances
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </button>
            </div>
        </form>
    </div>
</div>
</div>

<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>

<script type="text/javascript">
    $('#tasks-percent').slider({
        formater: function (value) {
            return value + '% Complete..';
        }
    });

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }
</script>
