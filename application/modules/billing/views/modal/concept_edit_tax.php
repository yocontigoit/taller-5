<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Editar Orden</h4>
        </div>
            <div class="modal-body">
              <form method="post" action="<?php echo base_url('billing/billing/update_concept'); ?>">
                <input type="hidden" name="client_id" value="<?php echo $client_id; ?>">
                <input type="hidden" name="project_id" value="<?php echo $project_id; ?>">
                <div class="row">
                  <?php if(count($all_clients)): foreach($all_clients as $clients): if ($clients->client_id == $client_id): ?>
                    <?php
                      $totalpro = 0; $subtotalpro = 0; $Totalpro = 0; $totaltranspro = 0; $Totalpagopro = 0; $Totalsaldopro = 0; $pagotranspro = 0;
                      $pago_apro = 0; $Totalspro = 0; $totalstranspro = 0; $totalsubpro = 0; $corrientepro = 0; $diafanopro = 0; $pago_totalpro = 0;
                      $invoice_projecto = 0; $invoice_projecto_tax = 0; $Totalpro_corriente = 0; $total_a_corriente = 0; $nc_notax = 0; $Totalpro_diafano = 0;
                    ?>
                  <div class="col-md-12">
                    <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;"><?php echo $clients->client_name ?></h4></div>
                    <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success items_table_mano">
                      <tbody>
                        <?php if(count($projectos)): foreach($projectos as $invoice): if($invoice->project_id == $project_id && $clients->client_id == $invoice->client_id && $invoice->program == 1): ?>
                        <tr>
                          <td><strong>CONCEPTO</strong></td>
                          <td>
                            <input type="text" class="form-control" name="concepto" value="<?php echo $invoice->concept; ?>">
                          </td>
                        </tr>
                        <tr>
                          <td><strong>METODO DE PAGO</strong></td>
                          <td>
                            CUENTA CORRIENTE
                          </td>
                        </tr>
                        <tr>
                          <td><strong>OBRA</strong></td>
                          <td>
                            <?php echo project_company($invoice->project_id); ?>
                          </td>
                        </tr>
                        <tr>
                          <td><strong>TOTAL</strong></td>
                          <td>
                            <?php $Totalpro_diafano += pago_a_diafano($invoice->client_id,$invoice->project_id); ?>
                            $<?php
                            if($Totalpro_diafano == 0){
                              $nc_tax = nc_tax($invoice->project_id, $invoice->client_id);
                              $total_a_diafano = invoice_projecto_tax($invoice->project_id,$invoice->program, $invoice->client_id) - discount_tax($invoice->project_id,$invoice->client_id,$invoice->program) - $nc_tax;
                            }else {
                              $total_a_diafano += pago_a_diafano($invoice->client_id,$invoice->project_id);
                            }
                            echo format_currency($total_a_diafano);
                            ?>
                          </td>
                        </tr>
                      <?php endif; endforeach; endif; ?>
                      </tbody>
                    </table>
                  <?php endif; endforeach; endif; ?>
                  </div>

            <div class="modal-footer">
                <button class="btn btn-success btn-icon" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    <?php echo lang('form_button_save'); ?>
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </button>
            </div>
        </form>
    </div>
</div>
</div>

<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>

<script type="text/javascript">
    $('#tasks-percent').slider({
        formater: function (value) {
            return value + '% Complete..';
        }
    });

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }
</script>
