<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/02
 * Time: 11:20 AM
 */
class Billing extends Admin_Controller
{

    function __construct()
    {

        parent::__construct();
        $this->data['_PAGE'] = null;
        $this->load->model("billing_m");
        $this->data['show_setting_menu'] = true;



    }

    /**
     * load billing dashboard and billing module sidebar options
     * lists all invoices
     */
    function index()
    {
      $this->data['content'] = 'billing/pages/billingg';
      $this->data['title'] = 'Programación';





      $this->data['projects'] = $this->billing_m->get_projects();
      $this->data['transactions'] = $this->billing_m->transaction(); // all client trans..
      $this->data['invoice_transactions'] = $this->billing_m->client_transaction2(); // all client trans..
      $this->data['projects'] = $this->billing_m->distinct_totals();
      $this->data['proyectos'] = $this->billing_m->get_projects_asc();
      $this->data['invoice_item'] = $this->billing_m->all_invoice_item();
      $this->data['pago_a'] = $this->billing_m->get_pago_a_billing();
      $this->data['invo_client'] = $this->billing_m->get_invo_client();
      $this->data['find'] = $this->billing_m->get_find();


      if ($this->CompanyUserRolePermission_id == 1) {//if admin

          $this->data['transactions'] = $this->billing_m->all_invoice();
      } elseif ($this->CompanyUserRolePermission_id == 2) {

          $this->data['transactions'] = null;
      } else {

          $this->data['transactions'] = $this->billing_m->client_all_invoice($this->CompanyClient_id); // all company trans..

      }
      $this->data['transactions1'] = $this->billing_m->transaction();
      $this->data['inbox_datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;

      $this->data['_EST_PAGE'] = $this->load->view('pages/billingg', $this->data, true);

      $this->data['datatable'] = true;
      $this->data['SCRIPT_PAGE'] = true;
      $this->load->view('_main_layout', $this->data);

    }

    function ajax_id_orders(){

      $id_order = $_POST['id_order'];

      print_r($id_order);

      $this->data['id_order'] = $id_order;

      $this->load->view('ajax/orders_id', $this->data);
    }

    /**
     * load billing dashboard and billing module sidebar options
     * lists all invoices
     */
    function view_billing()
    {
      $this->data['content'] = 'billing/pages/billingg_view';
      $this->data['title'] = 'Programación Entregada';





      $this->data['projects'] = $this->billing_m->get_projects();
      $this->data['transactions'] = $this->billing_m->transaction(); // all client trans..
      $this->data['invoice_transactions'] = $this->billing_m->client_transaction2(); // all client trans..
      $this->data['projects'] = $this->billing_m->get_project_details();
      $this->data['invoice_item'] = $this->billing_m->all_invoice_item();
      $this->data['pago_a'] = $this->billing_m->get_pago_a_billing();
      $this->data['invo_client'] = $this->billing_m->get_invo_client();


      if ($this->CompanyUserRolePermission_id == 1) {//if admin

          $this->data['transactions'] = $this->billing_m->all_invoice();
      } elseif ($this->CompanyUserRolePermission_id == 2) {

          $this->data['transactions'] = null;
      } else {

          $this->data['transactions'] = $this->billing_m->client_all_invoice($this->CompanyClient_id); // all company trans..

      }
      $this->data['transactions1'] = $this->billing_m->transaction();
      $this->data['inbox_datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;

      $this->data['_EST_PAGE'] = $this->load->view('pages/billingg_view', $this->data, true);

      $this->data['datatable'] = true;
      $this->data['SCRIPT_PAGE'] = true;
      $this->load->view('_main_layout', $this->data);

    }

    /**
     * load billing dashboard and billing module sidebar options
     * lists all invoices
     */
    function view()
    {

        $this->data['content'] = 'billing/billing';
        $this->data['title'] = 'Pagos';

        $bill_page = $this->input->get('status', true) ? $this->input->get('status', true) : 'overview';

        $this->data['active_page'] = $bill_page;
        switch ($bill_page) {


            case 'overview':
                $this->data['page_title'] = 'Resumen';
                $this->data['show_billing_overview'] = true;
                $this->data['show_billing_chats'] = true;

                $this->data['_PAGE'] = $this->load->view('pages/overview', $this->data, true);

                break;
            case 'transaction':

                $this->data['page_title'] = 'Transacciones';
                if ($this->CompanyUserRolePermission_id == 1) {//if admin

                    $this->data['transactions'] = $this->billing_m->all_transaction(); // all company trans..
                } elseif ($this->CompanyUserRolePermission_id == 2) {

                    $this->data['transactions'] = null;
                } else {

                    $this->data['transactions'] = $this->billing_m->client_transaction($this->CompanyClient_id); // all client trans..

                }

                $this->data['_PAGE'] = $this->load->view('pages/transaction', $this->data, true);
                $this->data['datatable'] = true;

                break;
            case 'all':

                $this->data['page_title'] = 'Todos los pagos';


                if ($this->CompanyUserRolePermission_id == 1) {//if admin

                    $this->data['transactions'] = $this->billing_m->all_invoice();
                } elseif ($this->CompanyUserRolePermission_id == 2) {

                    $this->data['transactions'] = null;
                } else {

                    $this->data['transactions'] = $this->billing_m->client_all_invoice($this->CompanyClient_id); // all company trans..

                }

                $this->data['_PAGE'] = $this->load->view('pages/invoice', $this->data, true);
                $this->data['datatable'] = true;

                break;

            case 'paid':

                $this->data['page_title'] = 'Pagadas';

                if ($this->CompanyUserRolePermission_id == 1) {//if admin

                    $this->data['transactions'] = $this->billing_m->get_all_paid_invoices();

                } elseif ($this->CompanyUserRolePermission_id == 2) {

                    $this->data['transactions'] = null;
                } else {
                    $this->data['transactions'] = $this->billing_m->get_all_client_paid_invoices($this->CompanyClient_id);
                }
                $this->data['_PAGE'] = $this->load->view('pages/paid', $this->data, true);
                $this->data['datatable'] = true;
                break;

            case 'unpaid':

                $this->data['page_title'] = 'Sin Pagar';

                if ($this->CompanyUserRolePermission_id == 1) {//if admin

                    $this->data['transactions'] = $this->billing_m->get_all_unpaid_invoices();

                } elseif ($this->CompanyUserRolePermission_id == 2) {

                    $this->data['transactions'] = null;
                } else {
                    $this->data['transactions'] = $this->billing_m->get_all_client_unpaid_invoices($this->CompanyClient_id);
                }


                $this->data['_PAGE'] = $this->load->view('pages/unpaid', $this->data, true);
                $this->data['datatable'] = true;
                break;
            case 'overdue':

                $this->data['page_title'] = 'Pendiente';
                if ($this->CompanyUserRolePermission_id == 1) {//if admin

                    $this->data['transactions'] = $this->billing_m->get_all_overdue_invoices();

                } elseif ($this->CompanyUserRolePermission_id == 2) {

                    $this->data['transactions'] = null;
                } else {
                    $this->data['transactions'] = $this->billing_m->get_all_client_overdue_invoices($this->CompanyClient_id);
                }
                $this->data['_PAGE'] = $this->load->view('pages/overdue', $this->data, true);
                $this->data['datatable'] = true;
                break;

        }

        $this->data['show_print'] = false;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * view selected invoice
     * @param null $invoice_id
     */
    function invoice($invoice_id = null)
    {

        $this->data['content'] = 'billing/billing';
        $this->data['title'] = 'Pagos';
        $this->data['page_title'] = 'Pago --';
        $this->data['invoice_id'] = intval($invoice_id);

        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        //check if invoice belongs to logged in client
        if (!inv_belongs_to_client($invoice_id, $client_id)) {
            redirect('no_access');
        }

        if ($this->input->post()) {


        } else {
            $this->data['invoice_details'] = $this->billing_m->client_invoice($invoice_id);
            $this->data['invoice_items'] = $this->billing_m->get_invoice_items($invoice_id);
             $this->data['transactions'] = $this->billing_m->transaction(); // all client trans..
            $this->data['invoice_transactions'] = $this->billing_m->get_invoice_transaction($invoice_id); // all client trans..
            $this->data['page_title'] = $this->data['invoice_details']->inv_no;

            $this->data['active_page'] = null;
        }


        if ($this->CompanyUserRolePermission_id == 1) {//if admin
            $this->data['show_print'] = true;
            $this->data['_PAGE'] = $this->load->view('pages/preview', $this->data, true);
        } else {
            $this->data['show_print'] = true;
            $this->data['payments_methods'] = $this->billing_m->get_payment_methods($invoice_id);
            $this->data['_PAGE'] = $this->load->view('pages/client_view', $this->data, true);
        }
        $this->data['show_datepicker'] = true;
        $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * create new invoice/ retrieve invoice number
     */
    function create_invoice()
    {

        $this->data['content'] = 'billing/invoice';
        $this->data['title'] = 'ORDEN DE COMPRA';
        $this->data['invoice_number'] = $this->config->item('invoice')->invoice_prefix . "-" . trans_reference(10,
                $this->Company_id);
        $this->data['clients'] = $this->billing_m->get_company_clients($this->Company_id);
        $this->data['users'] = $this->billing_m->get_company_users($this->Company_id);
        $this->data['items'] = $this->billing_m->get_all_items($this->Company_id);
        $this->data['avances'] = $this->billing_m->get_avances();
        $this->data['currencies'] = $this->billing_m->get_currencies();
        $this->data['projects'] = $this->billing_m->get_projects();
        $this->data['show_datepicker'] = true;
        $this->data['invoice_calculations'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * create new raya/ retrieve raya number
     */
    function create_raya()
    {

        $this->data['content'] = 'billing/raya';
        $this->data['title'] = 'RAYA';
        $this->data['invoice_number'] = $this->config->item('invoice')->raya_prefix. "-" . trans_reference(15,
                $this->Company_id);
        $this->data['clients'] = $this->billing_m->get_company_clients($this->Company_id);
        $this->data['users'] = $this->billing_m->get_company_users($this->Company_id);
        $this->data['items'] = $this->billing_m->get_all_items($this->Company_id);
        $this->data['currencies'] = $this->billing_m->get_currencies();
        $this->data['projects'] = $this->billing_m->get_projects();
        $this->data['show_datepicker'] = true;
        $this->data['invoice_calculations'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * create new raya/ retrieve raya number
     */
    function create_oficina()
    {

        $this->data['content'] = 'billing/oficina';
        $this->data['title'] = 'NÓMINA';
        $this->data['invoice_number'] = $this->config->item('invoice')->nomina_prefix. "-" . trans_reference(19,
                $this->Company_id);
        $this->data['clients'] = $this->billing_m->get_company_clients($this->Company_id);
        $this->data['users'] = $this->billing_m->get_company_users($this->Company_id);
        $this->data['items'] = $this->billing_m->get_all_items($this->Company_id);
        $this->data['currencies'] = $this->billing_m->get_currencies();
        $this->data['projects'] = $this->billing_m->get_projects();
        $this->data['show_datepicker'] = true;
        $this->data['invoice_calculations'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * create new invoice/ retrieve invoice number
     */

      function programar_ordenes()
    {

        if ($this->input->post('id')) {
            foreach ($this->input->post('id') as $id) {
                $this->message_m->mark_deleted($id);
            }
        }

    }
      function estimar_ordenes()
    {

        if ($this->input->post('id')) {
            foreach ($this->input->post('id') as $id) {
                $this->message_m->mark_deleted($id);
            }
        }
    }

     function pagar_ordenes1()
    {

        if ($this->input->post('id')) {
            foreach ($this->input->post('id') as $id) {
                $this->message_m->mark_deleted($id);
            }
        }
    }

     function pagar_ordenes2()
    {

        if ($this->input->post('id')) {
            foreach ($this->input->post('id') as $id) {
                $this->message_m->mark_deleted($id);
            }
        }
    }

     function pagar_ordenes3()
    {

        if ($this->input->post('id')) {
            foreach ($this->input->post('id') as $id) {
                $this->message_m->mark_deleted($id);
            }
        }
    }

     function entregar_ordenes()
    {

        if ($this->input->post('id')) {
            foreach ($this->input->post('id') as $id) {
                $this->message_m->mark_deleted($id);
            }
        }
    }

    function create_presupuesto()
    {

        $this->data['content'] = 'billing/presupuesto';
        $this->data['title'] = 'PRESUPUESTO CONTRATISTAS';
$this->data['avances'] = $this->billing_m->get_avances();
        $this->data['invoice_number'] = $this->config->item('invoice')->invoice_prefix . "-" . trans_reference(10,
                $this->Company_id);
        $this->data['clients'] = $this->billing_m->get_company_clients($this->Company_id);
        $this->data['items'] = $this->billing_m->get_all_items($this->Company_id);
        $this->data['currencies'] = $this->billing_m->get_currencies();
        $this->data['projects'] = $this->billing_m->get_projects();
        $this->data['show_datepicker'] = true;
        $this->data['invoice_calculations'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }


    /**
     *Add New Invoice
     */
    function  add_invoice()
    {

        if ($this->input->post()) {

            $rules = $this->form_validator->invoice_add_rules;
            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == false) {

                $response = array(
                    'success' => 0,
                    'msg_status' => "error",
                    'message' => (validation_errors()) ? validation_errors() : 'Make sure you fill all required fields'
                );
                echo json_encode($response);
            } else {

                $user_id = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;

                $invoice_number = $this->input->post('invoice_id');
                $currency_id = $this->input->post('currency_id');
                $client_id = intval($this->input->post('client_id'));
                $filter = $this->input->post('filter');
                $project = $this->input->post('project');
                $autorizado = $this->input->post('autorizado');
                $solicitante = $this->input->post('solicitante');
                $tax = $this->input->post('tax');
                $client_type = intval($this->input->post('client_type'));
                $details = $this->input->post('details');
                $comment = $this->input->post('comment');
                if ($client_type == 3) {
                    $pago_a = 1;
                }else {
                    $pago_a = 0;
                }

                $start_date = ($this->input->post('start_date')) ? $this->input->post('start_date') : date('Y-m-d');
                $end_date = ($this->input->post('end_date')) ? $this->input->post('end_date') : date('Y-m-d');

                if ($currency_id == 'default') {
                    $client_currency = get_client_details($client_id);
                    $currency = get_currency_name($client_currency->client_currency);
                } else {

                    $currency = get_currency_name($currency_id);
                }

                $items = json_decode($this->input->post('items'));

                $param = array(
                    "inv_no" => $invoice_number,
                    "client_id" => $client_id,
                    "project_id" => $project,
                    "status" => 2,
                    "approved" => 1,
                    "currency" => $currency,
                    "solicitante" => $solicitante,
                    "autorizado" => $autorizado,
                    "notes" => $details,
                    "invoice_due" => date('Y-m-d', strtotime($end_date)),
                    "created_at" => date('Y-m-d', strtotime($start_date)),
                    "updated_at" => date('Y-m-d H:i:s'),
                    "inv_created_by" => $user_id,
                    "filter" => $filter,
                    "tax" => $tax,
                    "type" => 0,
                    "avance" => 0,
                    "pago_a" => $pago_a,
                    "notes" => $details,
                    "comment" => $comment,
                    "type_pre" => 0,
                    "program_fin" => 1

                );


               $inv_id = $this->billing_m->add_invoice($param, $items, $client_id, $project);

                if ($inv_id) {

                    if ($this->input->post('freq_recurring') != 'none') {
                        $rec_param = array(
                            "freq_recurring" => $this->input->post('freq_recurring'),
                            "recur_start_date" => $start_date,
                            "recur_end_date" => $end_date,
                        );
                        $this->_make_recurring($inv_id, $rec_param);
                    }

                    $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                    $log_params = array(
                        'activity_user' => $activity_user,
                        'activity_user_type' => $this->CompanyUser_type,
                        'activity_module' => 'invoice',
                        'activity_details' => 'New invoice created',
                        'activity_module_id' => intval($inv_id),
                        'activity_status' => "success",
                        'activity_event' => "Invoice no " . $invoice_number . " has been added",
                        'activity_icon' => 'fa fa-money'
                    );

                    $this->__activity_tracker($log_params);


                    $response = array(
                        'success' => 1,
                        'msg_status' => "success",
                        'message' => lang('messages_invoice_add_success')
                    );
                    echo json_encode($response);
                } else {
                    //failed to add invoice
                    $response = array(
                        'success' => 0,
                        'msg_status' => "error",
                        'message' => "Invoice could not be created, make sure you added items"
                    );
                    echo json_encode($response);
                }

            }
        }
    }

    /**
     *Add New Presupuesto
     */
    function  add_presupuesto()
    {

        if ($this->input->post()) {


            $user_id = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;

            $invoice_number = $this->input->post('invoice_id');
            $currency_id = $this->input->post('currency_id');
            $client_id = intval($this->input->post('client_id'));
            $details = $this->input->post('details');
            $project = $this->input->post('project');
            $tipo_pre = $this->input->post('tipo_pre');
            $tax = $this->input->post('tax');
            $client_type = intval($this->input->post('client_type'));
            $details = $this->input->post('details');
            if ($client_type == 3) {
                $pago_a = 1;
            }else {
                $pago_a = 0;
            }


            $start_date = ($this->input->post('start_date')) ? $this->input->post('start_date') : date('Y-m-d');
            $end_date = ($this->input->post('end_date')) ? $this->input->post('end_date') : date('Y-m-d');

            if ($currency_id == 'default') {
                $client_currency = get_client_details($client_id);
                $currency = get_currency_name($client_currency->client_currency);
            } else {

                $currency = get_currency_name($currency_id);
            }

            $items = json_decode($this->input->post('items'));

            $param = array(
                "inv_no" => $invoice_number,
                "client_id" => $client_id,
                "project_id" => $project,
                "status" => 2,
                "approved" => 1,
                "currency" => $currency,
                "notes" => $details,
                "invoice_due" => date('Y-m-d', strtotime($end_date)),
                "created_at" => date('Y-m-d', strtotime($start_date)),
                "updated_at" => date('Y-m-d H:i:s'),
                "inv_created_by" => $user_id,
                "filter" => 0,
                "tax" => $tax,
                "type" => 0,
                "avance" => 0,
                "pago_a" => $pago_a,
                "type_pre" => $tipo_pre,
                "notes" => $details

            );



           $inv_id = $this->billing_m->add_presupuesto($param, $items, $project, $client_id);

            if ($inv_id) {

                if ($this->input->post('freq_recurring') != 'none') {
                    $rec_param = array(
                        "freq_recurring" => $this->input->post('freq_recurring'),
                        "recur_start_date" => $start_date,
                        "recur_end_date" => $end_date,
                    );
                    $this->_make_recurring($inv_id, $rec_param);
                }

                $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'invoice',
                    'activity_details' => 'New invoice created',
                    'activity_module_id' => intval($inv_id),
                    'activity_status' => "success",
                    'activity_event' => "Invoice no " . $invoice_number . " has been added",
                    'activity_icon' => 'fa fa-money'
                );

                $this->__activity_tracker($log_params);


                $response = array(
                    'success' => 1,
                    'msg_status' => "success",
                    'message' => lang('messages_invoice_add_success')
                );
                echo json_encode($response);
            } else {
                //failed to add invoice
                $response = array(
                    'success' => 0,
                    'msg_status' => "error",
                    'message' => "Invoice could not be created, make sure you added items"
                );
                echo json_encode($response);
            }


        }
    }

    /**
     *Add New Presupuesto
     */
    function  add_raya()
    {

        if ($this->input->post()) {


            $user_id = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;

            $invoice_number = $this->input->post('invoice_id');
            $currency_id = $this->input->post('currency_id');
            $client_id = intval($this->input->post('client_id'));
            $details = $this->input->post('details');
            $project = $this->input->post('project');
            $tipo_pre = $this->input->post('tipo_pre');
            $details = $this->input->post('details');
            $autorizado = $this->input->post('autorizado');

            $start_date = ($this->input->post('start_date')) ? $this->input->post('start_date') : date('Y-m-d');
            $end_date = ($this->input->post('end_date')) ? $this->input->post('end_date') : date('Y-m-d');

            if ($currency_id == 'default') {
                $client_currency = get_client_details($client_id);
                $currency = get_currency_name($client_currency->client_currency);
            } else {

                $currency = get_currency_name($currency_id);
            }

            $items = json_decode($this->input->post('items'));

            $param = array(
                "inv_no" => $invoice_number,
                "client_id" => $client_id,
                "project_id" => $project,
                "status" => 2,
                "approved" => 1,
                "autorizado" => $autorizado,
                "currency" => $currency,
                "notes" => $details,
                "invoice_due" => date('Y-m-d', strtotime($end_date)),
                "created_at" => date('Y-m-d', strtotime($start_date)),
                "updated_at" => date('Y-m-d H:i:s'),
                "inv_created_by" => $user_id,
                "filter" => 0,
                "type" => 0,
                "avance" => 1,
                "raya" => 1,
                "type_pre" => 1,
                "notes" => $details,
                "program_fin" => 1

            );



           $inv_id = $this->billing_m->add_raya($param, $items, $project, $client_id);

            if ($inv_id) {

                if ($this->input->post('freq_recurring') != 'none') {
                    $rec_param = array(
                        "freq_recurring" => $this->input->post('freq_recurring'),
                        "recur_start_date" => $start_date,
                        "recur_end_date" => $end_date,
                    );
                    $this->_make_recurring($inv_id, $rec_param);
                }

                $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'invoice',
                    'activity_details' => 'New invoice created',
                    'activity_module_id' => intval($inv_id),
                    'activity_status' => "success",
                    'activity_event' => "Invoice no " . $invoice_number . " has been added",
                    'activity_icon' => 'fa fa-money'
                );

                $this->__activity_tracker($log_params);


                $response = array(
                    'success' => 1,
                    'msg_status' => "success",
                    'message' => lang('messages_invoice_add_success')
                );
                echo json_encode($response);
            } else {
                //failed to add invoice
                $response = array(
                    'success' => 0,
                    'msg_status' => "error",
                    'message' => "Invoice could not be created, make sure you added items"
                );
                echo json_encode($response);
            }


        }
    }

    /**
     *Add New Presupuesto
     */
    function  add_oficina()
    {

        if ($this->input->post()) {


            $user_id = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;

            $invoice_number = $this->input->post('invoice_id');
            $currency_id = $this->input->post('currency_id');
            $client_id = intval($this->input->post('client_id'));
            $details = $this->input->post('details');
            $project = $this->input->post('project');
            $tipo_pre = $this->input->post('tipo_pre');
            $details = $this->input->post('details');
            $autorizado = $this->input->post('autorizado');

            $start_date = ($this->input->post('start_date')) ? $this->input->post('start_date') : date('Y-m-d');
            $end_date = ($this->input->post('end_date')) ? $this->input->post('end_date') : date('Y-m-d');

            if ($currency_id == 'default') {
                $client_currency = get_client_details($client_id);
                $currency = get_currency_name($client_currency->client_currency);
            } else {

                $currency = get_currency_name($currency_id);
            }

            $items = json_decode($this->input->post('items'));

            $param = array(
                "inv_no" => $invoice_number,
                "client_id" => $client_id,
                "project_id" => $project,
                "status" => 2,
                "approved" => 1,
                "autorizado" => $autorizado,
                "currency" => $currency,
                "notes" => $details,
                "invoice_due" => date('Y-m-d', strtotime($end_date)),
                "created_at" => date('Y-m-d', strtotime($start_date)),
                "updated_at" => date('Y-m-d H:i:s'),
                "inv_created_by" => $user_id,
                "filter" => 0,
                "type" => 0,
                "avance" => 1,
                "raya" => 1,
                "type_pre" => 4,
                "notes" => $details,
                "program_fin" => 1

            );



           $inv_id = $this->billing_m->add_oficina($param, $items, $project, $client_id);

            if ($inv_id) {

                if ($this->input->post('freq_recurring') != 'none') {
                    $rec_param = array(
                        "freq_recurring" => $this->input->post('freq_recurring'),
                        "recur_start_date" => $start_date,
                        "recur_end_date" => $end_date,
                    );
                    $this->_make_recurring($inv_id, $rec_param);
                }

                $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'invoice',
                    'activity_details' => 'New invoice created',
                    'activity_module_id' => intval($inv_id),
                    'activity_status' => "success",
                    'activity_event' => "Invoice no " . $invoice_number . " has been added",
                    'activity_icon' => 'fa fa-money'
                );

                $this->__activity_tracker($log_params);


                $response = array(
                    'success' => 1,
                    'msg_status' => "success",
                    'message' => lang('messages_invoice_add_success')
                );
                echo json_encode($response);
            } else {
                //failed to add invoice
                $response = array(
                    'success' => 0,
                    'msg_status' => "error",
                    'message' => "Invoice could not be created, make sure you added items"
                );
                echo json_encode($response);
            }


        }
    }


    /**
     * set invoice as recurring invoice
     * @param null $invoice_id
     * @param array $data
     * @return bool
     */
    function _make_recurring($invoice_id = null, $data = array())
    {

        $recur_days = $this->_calculate_days($data['freq_recurring']);
        $due_date = $data['recur_end_date'];
        $next_date = date("Y-m-d", strtotime($due_date . "+ " . $recur_days . " days"));

        if ($data['recur_end_date'] == '') {
            $recur_end_date = '0000-00-00';
        } else {
            $recur_end_date = $data['recur_end_date'];
        }


        $recur_end_date = date('Y-m-d', strtotime($recur_end_date));
        $recur_start_date = date('Y-m-d', strtotime($data['recur_start_date']));

        $update_invoice = array(
            'is_recurring' => 1,
            'recur_period' => $recur_days,
            'recur_frequency' => $data['freq_recurring'],
            'recur_start_date' => $recur_start_date,
            'recur_end_date' => $recur_end_date,
            'recur_next_date' => $next_date
        );

        if ($this->billing_m->update_recur($update_invoice, $invoice_id)) {
            return true;
        }

        return false;
    }

    /**
     * options for number of days before invoice recur
     * @param $frequency
     * @return int
     */
    function _calculate_days($frequency)
    {
        switch ($frequency) {
            case '7D':
                return 7;
                break;
            case '1M':
                return 31;
                break;
            case '3M':
                return 90;
                break;
            case '6M':
                return 182;
                break;
            case '1Y':
                return 365;
                break;
        }
    }

    /**
     * log an activity
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->billing_m->add_log($activity_data);

    }

    /**
     * retrieves user data, when a new invoice is created..
     */
    function ajax_client_results()
    {

        // Fetch all lists
        if ($_POST['client_id'] != 'Empty') {
            $this->data['client_info'] = $this->billing_m->get_client_details($_POST['client_id']);
        } else {
            $this->data['client_info'] = null;
        }
        // Load view
        $this->load->view('ajax/client_details', $this->data);
    }

    /**
     * retrieves user data, when a new invoice is created..
     */
    function ajax_client_items()
    {

        // Fetch all lists
        if ($_POST['client_id'] != 'Empty') {
            $this->data['items'] = $this->billing_m->get_items_client($_POST['client_id']);
        } else {
            $this->data['items'] = null;
        }
        // Load view
        $this->load->view('ajax/client_items', $this->data);
    }

    /**
     * retrieves user data, when a new invoice is created..
     */
    function ajax_project_milestone()
    {

        // Fetch all lists
        if ($_POST['project_id'] != 'Empty') {
            $this->data['milestones'] = $this->billing_m->get_milestone($_POST['project_id']);
        } else {
            $this->data['milestones'] = null;
        }
        // Load view
        $this->load->view('ajax/project_milestone', $this->data);
    }
     function ajax_project_orders()
    {

        // Fetch all lists
        if ($_POST['project_id'] != 'Empty' && $_POST['client_id'] != 'Empty' ) {
            $this->data['orders'] = $this->billing_m->get_factura($_POST['project_id'],$_POST['client_id']);
        } else {
            $this->data['orders'] = null;
        }
        // Load view
        $this->load->view('ajax/project_orders', $this->data);
    }

    function ajax_project_diafano_corriente()
   {

       // Fetch all lists
       if ($_POST['invoice'] != 'Empty') {
           $this->data['orders'] = $this->billing_m->get_diafano_corriente($_POST['invoice']);
       } else {
           $this->data['orders'] = null;
       }
       // Load view
       $this->load->view('ajax/project_diafano_corriente', $this->data);
   }



    /**
     * Manual payment
     * @param null $invoice_id
     */
    function add_payment($invoice_id = null)
    {

        if ($this->input->post()) {
            $invoice_id = intval($this->input->post('invoice_id'));
            $paid_amount = $this->input->post('payment_amount');


            //get amount due
            $amount_due = invoice_balance($invoice_id);
            $client_details = get_client_invoice_details($invoice_id);
            if ($amount_due >= $paid_amount && $paid_amount > 0) {

                $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $transaction = array(
                    "invoice_id" => $invoice_id,
                    "trans_id" => $this->input->post('payment_id'),
                    "amount" => $this->input->post('payment_amount'),
                    "payment_method" => $this->input->post('payment_method'),
                    "ip" => $this->input->ip_address(),
                    "notes" => "invoice paid by" . $this->CompanyUser_FullName,
                    "creator_email" => $this->CompanyUser_email,
                    "created_by" => $user_id,
                    "created_at" => date('Y-m-d H:i:s'),
                    "updated_at" => date('Y-m-d H:i:s'),
                );


                if ($this->billing_m->add_payment($transaction)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_invoice_add_success'));


                }
                if ($this->input->post('payment_send_mail') == 'on') {

                    $this->notifications->send_payment_notification($client_details->client_id, $invoice_id,
                        $this->input->post('payment_amount'));
                }

                redirect('billing/invoice/' . $invoice_id);
            } else {

                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', lang('messages_invoice_overpaid_error'));
                redirect('billing/invoice/' . $invoice_id);
            }

        }
    }

    /**
     * Manual payment
     * @param null $invoice_id
     */
    function add_payment2($invoice_id = null)
    {

        if ($this->input->post()) {
            $invoice_id = intval($this->input->post('invoice_id'));
            $paid_amount = $this->input->post('payment_amount');
            $payment_method = $this->input->post('payment_method');
            $invoice_details = $this->billing_m->client_invoice($invoice_id);
            $total = invoice_total($invoice_details->id);

            //get amount due
            $amount_due = invoice_balance($invoice_id);
            $client_details = get_client_invoice_details($invoice_id);

                $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $transaction = array(
                    "invoice_id" => $invoice_id,
                    "trans_id" => $this->input->post('payment_id'),
                    "amount" => 0,
                    "payment_method" => 0,
                    "ip" => $this->input->ip_address(),
                    "notes" => "factura y cheque" . $this->CompanyUser_FullName,
                    "creator_email" => $this->CompanyUser_email,
                    "created_by" => $user_id,
                    "created_at" => date('Y-m-d H:i:s'),
                    "updated_at" => date('Y-m-d H:i:s'),
                    "invoice" => $this->input->post('invoice'),
                    "reference" => $this->input->post('reference')
                );

                $invoice = array("invoice_inv" => $this->input->post('invoice'));


                if ($this->billing_m->add_payment($transaction, $invoice_id, $invoice)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', 'El pago ha sido realizado con éxito !');


                }
                if ($this->input->post('payment_send_mail') == 'on') {

                    $this->notifications->send_payment_notification($client_details->client_id, $invoice_id,
                        $this->input->post('payment_amount'));
                }

                redirect('billing/billing/');


        }
    }

    /**
     * Manual payment
     * @param null $invoice_id
     */
    function change_payment2($invoice_id = null)
    {

        if ($this->input->post()) {
            $invoice_id = intval($this->input->post('invoice_id'));
            $paid_amount = $this->input->post('payment_amount');
            $payment_method = $this->input->post('payment_method');
            $invoice_details = $this->billing_m->client_invoice($invoice_id);
            $total = invoice_total($invoice_details->id);

            //get amount due
            $amount_due = invoice_balance($invoice_id);
            $client_details = get_client_invoice_details($invoice_id);

                $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $transaction = array(
                    "invoice_id" => $invoice_id,
                    "trans_id" => $this->input->post('payment_id'),
                    "amount" => 0,
                    "payment_method" => 0,
                    "ip" => $this->input->ip_address(),
                    "notes" => "factura y cheque" . $this->CompanyUser_FullName,
                    "creator_email" => $this->CompanyUser_email,
                    "created_by" => $user_id,
                    "created_at" => date('Y-m-d H:i:s'),
                    "updated_at" => date('Y-m-d H:i:s'),
                    "invoice" => $this->input->post('invoice'),
                    "reference" => $this->input->post('reference')
                );

                $invoice = array("invoice_inv" => $this->input->post('invoice'));


                if ($this->billing_m->change_payment($transaction, $invoice_id, $invoice)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', 'El pago ha sido realizado con éxito !');


                }
                if ($this->input->post('payment_send_mail') == 'on') {

                    $this->notifications->send_payment_notification($client_details->client_id, $invoice_id,
                        $this->input->post('payment_amount'));
                }

                redirect('billing/billing/');


        }
    }

    /**
     * add invoice note
     */
    function add_note()
    {

        if ($this->input->post()) {
            $invoice_id = intval($this->input->post('invoice_id'));
            $note = array(
                "notes" => $this->input->post('inv_note')

            );


            if ($this->billing_m->add_invoice_note($note, $invoice_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_invoice_update_success'));
            }
            redirect('billing/invoice/' . $invoice_id);
        }


    }

    /**
     * delete selected invoice
     * @param null $invoice_id
     */
    function delete($invoice_id = null)
    {

        if ($this->input->post()) {

            $invoice_id = intval($this->input->post('invoice_id'));

            if ($this->billing_m->remove_invoice($invoice_id)) {
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_invoice_delete_success'));
                redirect('billing/?status=all');
            }

        } else {
            $data['invoice_id'] = $invoice_id;
            $this->load->view('modal/delete_invoice', $data);

        }
    }

    /**
     * remove payment transaction
     * @param null $payment_id
     */
    function remove_payment($payment_id = null)
    {

        if ($this->input->post()) {

            $payment_id = intval($this->input->post('payment_id'));

            if ($this->billing_m->remove_payment($payment_id)) {
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_payment_delete_success'));
                redirect('billing/?status=all');
            }

        } else {
            $this->data['payment_id'] = $payment_id;
            $this->load->view('modal/delete_payment', $this->data);

        }
    }

    /**
     * remove payment_a
     * @param null $payment_id
     */
    function remove_pago_a($payment_id = null)
    {

            $payment_id = intval($this->uri->segment(3));

            if ($this->billing_m->remove_pago_a($payment_id)) {
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_payment_delete_success'));
                redirect('billing/billing');
            }
    }

    /**
     * preview invoice
     * @param null $invoice_id
     */
    function pdf_preview($invoice_id = null)
    {
        $this->load->helper('pdf');
        $this->data['invoice_details'] = $this->billing_m->client_invoice($invoice_id);
        $this->data['invoice_items'] = $this->billing_m->get_invoice_items($invoice_id);
        generate_invoice_pdf($invoice_id, $this->data, null, $this->data['invoice_details']->inv_no);
    }

    /**
     * download invoice in pdf
     * @param null $invoice_id
     */
    function pdf_download($invoice_id = null)
    {
        $this->load->helper('pdf');
        $this->data['invoice_details'] = $this->billing_m->client_invoice($invoice_id);
        $this->data['invoice_items'] = $this->billing_m->get_invoice_items($invoice_id);
        generate_invoice_pdf($invoice_id, $this->data, null, $this->data['invoice_details']->inv_no, false);
    }

    /**
     * download invoice in pdf
     * @param null $invoice_id
     */
    function pdf_download_raya($invoice_id = null)
    {
        $this->load->helper('pdf');
        $this->data['invoice_details'] = $this->billing_m->client_invoice($invoice_id);
        $this->data['invoice_items'] = $this->billing_m->get_invoice_items($invoice_id);
        generate_invoice_pdf_raya($invoice_id, $this->data, null, $this->data['invoice_details']->inv_no, false);
    }

    /**
     * send new invoice via email to client
     * @param null $invoice_id
     */
    function email_invoice($invoice_id = null)
    {

        $this->data['invoice_details'] = $this->billing_m->client_invoice($invoice_id);
        $this->data['invoice_items'] = $this->billing_m->get_invoice_items($invoice_id);

        $this->notifications->send_new_invoice_notification($invoice_id, $this->data);
        redirect('billing/invoice/' . $invoice_id);

    }

    /**
     * send invoice reminder
     * @param null $invoice_id
     */
    function reminder($invoice_id = null)
    {

        $this->notifications->send_invoice_reminder_notification($invoice_id);

        redirect('billing/invoice/' . $invoice_id);

    }

    /**
     * load task edit page
     * @param null $project_id
     * @param null $task_id
     */
    function edit_orden($id = null, $invoice_id = null, $project_id = null )
    {
        $this->data['invoice_id'] = $invoice_id;
        $this->data['id'] = $id;
        $this->data['project_id'] = $project_id;
        $this->data['invoice_items'] = $this->billing_m->get_invoice_items2($invoice_id, $id);
        $this->data['milestones'] = $this->billing_m->get_milestone($project_id);
        $this->load->view('modal/orden_edit', $this->data);

    }

    /**
     * update task
     */
    function update_orden()
    {

        if ($this->input->post()) {

            $invoice_id = intval($this->input->post('invoice_id'));
            $id = intval($this->input->post('id'));
            $param = array(
                "invoice_id" => intval($invoice_id),
                "item_name" => $this->input->post('item_name'),
                "item_avance" => $this->input->post('avance'),
                "item_desc" => $this->input->post('item_desc'),
                "price" => $this->input->post('price'),
                "quantity" => $this->input->post('quantity')
            );

            $tax = array(
              "tax" => $this->input->post('tax')
            );

            }

            if ($this->billing_m->update_orden($param, $tax, $id, $invoice_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_invoice_update_success'));
            }
            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', lang('messages_invoice_update_success'));
            redirect('billing/invoice/' . $invoice_id);

        }

        /**
         * load task edit page
         * @param null $project_id
         * @param null $task_id
         */
        function add_orden($invoice_id = null )
        {
            $this->data['id'] = $invoice_id;
            $this->data['invoice_details'] = $this->billing_m->client_invoice($invoice_id);
            $this->data['items'] = $this->billing_m->get_all_items($this->Company_id);

            $this->load->view('modal/orden_add', $this->data);

        }

          function add_discount($invoice_id = null )
        {
            $this->data['id'] = $invoice_id;
            $this->data['invoice_details'] = $this->billing_m->client_invoice($invoice_id);
            $this->data['items'] = $this->billing_m->get_all_items($this->Company_id);

            $this->load->view('modal/add_discount', $this->data);

        }

        /**
         * add invoice orden
         */
        function add_new_orden()
        {

            if ($this->input->post()) {
                $invoice_id = $this->input->post('invoice_id');
                $param = array(
                    "invoice_id" => $this->input->post('invoice_id'),
                    "item_name" => $this->input->post('item_name'),
                    "item_avance" => $this->input->post('item_avance'),
                    "item_desc" => $this->input->post('item_desc'),
                    "price" => $this->input->post('price'),
                    "quantity" => $this->input->post('quantity')
                );


                if ($this->billing_m->add_orden($param)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_invoice_update_success'));
                }
                redirect('billing/invoice/' . $invoice_id);
            }


        }

          function add_new_discount()
        {

            if ($this->input->post()) {
                $invoice_id = $this->input->post('invoice_id');
                $param = array(
                    "disc_desc" => $this->input->post('disc_desc'),
                    "disc_amount" => $this->input->post('disc_amount'),
                    "disc_amount_per" => $this->input->post('disc_amount_per')
                );

                $inv_id = $this->input->post('invoice_id');

                if ($this->billing_m->add_discount($inv_id, $param)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_invoice_update_success'));
                }
                redirect('billing/invoice/' . $invoice_id);
            }


        }

        /**
         * load task edit page
         * @param null $project_id
         * @param null $task_id
         */
        function pagar_factura($invoice_id = null )
        {

            if ($this->CompanyUserRolePermission_id == 1) {//if admin

                $this->data['transactions'] = $this->billing_m->all_invoice();
            } elseif ($this->CompanyUserRolePermission_id == 2) {

                $this->data['transactions'] = null;
            } else {

                $this->data['transactions'] = $this->billing_m->client_all_invoice($this->CompanyClient_id); // all company trans..

            }
            $this->data['SCRIPT_PAGE'] = true;

            $this->load->view('modal/pagar_factura', $this->data);

        }
        /**
         * load task edit page
         * @param null $project_id
         * @param null $task_id
         */
        function modificar_factura($invoice_id = null )
        {

            if ($this->CompanyUserRolePermission_id == 1) {//if admin

                $this->data['transactions'] = $this->billing_m->all_invoice_3($invoice_id);
            } elseif ($this->CompanyUserRolePermission_id == 2) {

                $this->data['transactions'] = null;
            } else {

                $this->data['transactions'] = $this->billing_m->client_all_invoice($this->CompanyClient_id); // all company trans..

            }
            $this->data['SCRIPT_PAGE'] = true;

            $this->load->view('modal/modificar_factura', $this->data);

        }

           function add_percent($invoice_id = null, $project_id = null)
        {
            $project_id = intval($this->uri->segment(5));
            print_r($project_id);
            $this->data['milestones'] = $this->billing_m->get_milestone($project_id);

            if ($this->CompanyUserRolePermission_id == 1) {//if admin

                $this->data['transactions'] = $this->billing_m->all_invoice();
            } elseif ($this->CompanyUserRolePermission_id == 2) {

                $this->data['transactions'] = null;
            } else {

                $this->data['transactions'] = $this->billing_m->client_all_invoice($this->CompanyClient_id); // all company trans..

            }
            $this->data['SCRIPT_PAGE'] = true;

            $this->load->view('modal/add_percent', $this->data);

        }

        /**
         * load task edit page
         * @param null $project_id
         * @param null $task_id
         */
        function add_monto($invoice_id = null, $client_id = null, $project_id = null, $tax = null)
        {

            $inv_id = intval($invoice_id);
            $this->data['inv_id'] = intval($invoice_id);
            $this->data['client_id'] = intval($client_id);
            $this->data['project_id'] = intval($project_id);
            $this->data['tax'] = intval($tax);
            $this->data['pago_a'] = $this->billing_m->get_pago_a($client_id);
            if ($this->CompanyUserRolePermission_id == 1) {//if admin

                $this->data['transactions'] = $this->billing_m->all_invoice_monto($inv_id);
            } elseif ($this->CompanyUserRolePermission_id == 2) {

                $this->data['transactions'] = null;
            } else {

                $this->data['transactions'] = $this->billing_m->all_invoiced_clients_row($this->CompanyClient_id); // all company trans..

            }
            $this->data['SCRIPT_PAGE'] = true;

            $this->load->view('modal/add_monto', $this->data);

        }

        function save_hola()
        {

          if ($this->input->post()) {

              $items = json_decode($this->input->post('items'));



              if ($this->billing_m->update_estimate($items)) {

                  $this->session->set_flashdata('msg_status', 'success');
                  $this->session->set_flashdata('message', lang('messages_invoice_update_success'));
              }
          }
        }

        function save_find()
        {

          if ($this->input->post()) {

              $find = $this->input->post('find');

              if ($this->billing_m->update_find($find)) {

                  $this->session->set_flashdata('msg_status', 'success');
                  $this->session->set_flashdata('message', lang('messages_invoice_update_success'));
              }
          }
        }



        /**
         * load billing dashboard and billing module sidebar options
         * lists all invoices
         */
        function a_estimar()
        {

            $this->data['content'] = 'billing/pages/estimacion';
            $this->data['title'] = 'Pagos';

            $project_id = intval($this->input->post('project_id'));

            $project = get_project_details($this->input->post('project_id'));
            $estimates = get_estimaciones();
            $count = 1;
            if(count($estimates)){
              foreach ($estimates as $estimate){
                if ($estimate->project_id == $project->project_id) {
                  $count++;
                }

              }
            }

            $this->data['number'] = $count;
            $this->data['projects'] = $this->billing_m->get_projects_estimate($project_id);
            $this->data['transactions_caja'] = $this->billing_m->transactions_caja($project_id);
            $this->data['estimaciones'] = $this->billing_m->all_estimacion($project_id);
            $this->data['invoices_items'] = $this->billing_m->all_invoice();
            $this->data['items'] = $this->billing_m->all_invoice_item($project_id);
            $this->data['totals'] = $this->billing_m->get_est_amount();
            $this->data['all_transaction'] = $this->billing_m->all_transaction();


            if ($this->CompanyUserRolePermission_id == 1) {//if admin

                $this->data['transactions'] = $this->billing_m->all_invoice_estimate($project_id);
            } elseif ($this->CompanyUserRolePermission_id == 2) {

                $this->data['transactions'] = null;
            } else {

                $this->data['transactions'] = $this->billing_m->client_all_invoice_estimate($project_id, $this->CompanyClient_id); // all company trans..

            }

            $this->data['project_id'] = $project_id;
            $this->data['_EST_PAGE'] = $this->load->view('pages/estimacion', $this->data, true);
            $this->data['datatable'] = true;
            $this->data['show_datepicker'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);

        }

        /**
         * load billing dashboard and billing module sidebar options
         * lists all invoices
         */
        function estimar_view()
        {

            $this->data['content'] = 'billing/pages/estimacion_view';
            $this->data['title'] = 'Estimacion';

            $project_id = intval($this->uri->segment(4));
            $est_num = intval($this->uri->segment(5));

            $this->data['projects'] = $this->billing_m->get_projects_estimate($project_id);
            $this->data['transactions_caja'] = $this->billing_m->transactions_caja($project_id);
            // $this->data['estimaciones'] = $this->billing_m->all_estimacion($project_id);
            $this->data['invoices_items'] = $this->billing_m->all_estimacion();
            $this->data['items'] = $this->billing_m->all_invoice_item($project_id);
            $this->data['totals'] = $this->billing_m->get_est_amount($project_id,$est_num);
            $this->data['all_transaction'] = $this->billing_m->all_transaction();
            $this->data['anticipos'] = $this->billing_m->get_est_trans($project_id);


            if ($this->CompanyUserRolePermission_id == 1) {//if admin

                $this->data['transactions'] = $this->billing_m->all_invoice_estimate($project_id);
            } elseif ($this->CompanyUserRolePermission_id == 2) {

                $this->data['transactions'] = null;
            } else {

                $this->data['transactions'] = $this->billing_m->client_all_invoice_estimate($project_id, $this->CompanyClient_id); // all company trans..

            }

            $this->data['project_id'] = $project_id;
            $this->data['est_num'] = $est_num;
            $this->data['est_numA'] = $est_num - 1;

            $this->data['_EST_PAGE'] = $this->load->view('pages/estimacion_view', $this->data, true);
            $this->data['datatable'] = true;
            $this->data['show_datepicker'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);

        }

        /**
         * load billing dashboard and billing module sidebar options
         * lists all invoices
         */
        function program_proveedor()
        {

            $this->data['content'] = 'billing/pages/program_p';
            $this->data['title'] = 'Programación Proveedor';


            $this->data['projects'] = $this->billing_m->get_projects();
            $this->data['invoice_transactions'] = $this->billing_m->client_transaction2(); // all client trans..
            $this->data['projects'] = $this->billing_m->get_project_details();
            $this->data['invoice_item'] = $this->billing_m->all_invoice_item();

            if ($this->CompanyUserRolePermission_id == 1) {//if admin

                $this->data['transactions'] = $this->billing_m->all_invoice();
            } elseif ($this->CompanyUserRolePermission_id == 2) {

                $this->data['transactions'] = null;
            } else {

                $this->data['transactions'] = $this->billing_m->client_all_invoice($this->CompanyClient_id); // all company trans..

            }

            $this->data['_EST_PAGE'] = $this->load->view('pages/program_p', $this->data, true);
            $this->data['datatable'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);

        }

        /**
         * load billing dashboard and billing module sidebar options
         * lists all invoices
         */
        function program_obra()
        {

            $this->data['content'] = 'billing/pages/program_o';
            $this->data['title'] = 'Programación Obra';

            $this->data['projects'] = $this->billing_m->get_projects();
            $this->data['invoice_transactions'] = $this->billing_m->client_transaction2(); // all client trans..
            $this->data['projects'] = $this->billing_m->get_project_details();
            $this->data['invoice_item'] = $this->billing_m->all_invoice_item();

            if ($this->CompanyUserRolePermission_id == 1) {//if admin

                $this->data['transactions'] = $this->billing_m->all_invoice();
            } elseif ($this->CompanyUserRolePermission_id == 2) {

                $this->data['transactions'] = null;
            } else {

                $this->data['transactions'] = $this->billing_m->client_all_invoice($this->CompanyClient_id); // all company trans..

            }

            $this->data['_EST_PAGE'] = $this->load->view('pages/program_o', $this->data, true);
            $this->data['datatable'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);

        }

        /**
         * load billing dashboard and billing module sidebar options
         * lists all invoices
         */
        function program_todo()
        {

            $this->data['content'] = 'billing/pages/program_t';
            $this->data['title'] = 'Programaciones';
            //por Proveedor
            $this->data['projects'] = $this->billing_m->get_projects_union();
            $this->data['clients'] = $this->billing_m->all_invoice_distinct();
            $this->data['pagos'] = $this->billing_m->get_pago_a();
            $this->data['all_transactions'] = $this->billing_m->transaction();
            $this->data['totals'] = $this->billing_m->distinct_totals();
            //por Obra
            $this->data['all_clients'] = $this->billing_m->all_client_invoice();
            $this->data['projectos'] = $this->billing_m->all_project_distinct();
            $this->data['projectos2'] = $this->billing_m->all_project_distinct2();


            if ($this->CompanyUserRolePermission_id == 1) {//if admin

                $this->data['transactions'] = $this->billing_m->all_invoice();
            } elseif ($this->CompanyUserRolePermission_id == 2) {

                $this->data['transactions'] = null;
            } else {

                $this->data['transactions'] = $this->billing_m->client_all_invoice($this->CompanyClient_id); // all company trans..

            }


            $this->data['_EST_PAGE'] = $this->load->view('pages/program_t', $this->data, true);
            $this->data['datatable'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);

        }



        /**
         * download invoice in pdf
         * @param null $invoice_id
         */
        function pdf_download_billing($project_id = null, $est_num = null)
        {
            $this->load->helper('pdf');

            $project_id = intval($this->uri->segment(3));
            $est_num = intval($this->uri->segment(4));

            $this->data['projects'] = $this->billing_m->get_projects_estimate($project_id);
            $this->data['transactions_caja'] = $this->billing_m->transactions_caja($project_id);
            // $this->data['estimaciones'] = $this->billing_m->all_estimacion($project_id);
            $this->data['invoices_items'] = $this->billing_m->all_estimacion();
            $this->data['items'] = $this->billing_m->all_invoice_item($project_id);
            $this->data['totals'] = $this->billing_m->get_est_amount($project_id,$est_num);
            $this->data['all_transaction'] = $this->billing_m->all_transaction();


            if ($this->CompanyUserRolePermission_id == 1) {//if admin

                $this->data['transactions'] = $this->billing_m->all_invoice_estimate($project_id);
            } elseif ($this->CompanyUserRolePermission_id == 2) {

                $this->data['transactions'] = null;
            } else {

                $this->data['transactions'] = $this->billing_m->client_all_invoice_estimate($project_id, $this->CompanyClient_id); // all company trans..

            }

            $this->data['project_id'] = $project_id;
            $this->data['est_num'] = $est_num;
            $this->data['est_numA'] = $est_num - 1;

            generate_billing_pdf($project_id, $this->data, '', $this->data['transactions']->project_id);


        }

        /**
         * download invoice in pdf
         * @param null $invoice_id
         */
        function pdf_download_program1()
        {
            $this->load->helper('pdf');


            //por Proveedor
            $this->data['projects'] = $this->billing_m->get_projects_union();
            $this->data['clients'] = $this->billing_m->all_invoice_distinct();
            $this->data['pagos'] = $this->billing_m->get_pago_a();
            $this->data['all_transactions'] = $this->billing_m->transaction();
            $this->data['totals'] = $this->billing_m->distinct_totals();
            //por Obra
            $this->data['all_clients'] = $this->billing_m->all_client_invoice();
            $this->data['projectos'] = $this->billing_m->all_project_distinct();
            $this->data['projectos2'] = $this->billing_m->all_project_distinct2();
            $this->data['transactions'] = $this->billing_m->all_invoice();

            generate_program1_pdf($this->data, '', $this->data['transactions']);


        }
           function pdf_download_program2()
        {
            $this->load->helper('pdf');


            //por Proveedor
            $this->data['projects'] = $this->billing_m->get_projects_union();
            $this->data['clients'] = $this->billing_m->all_invoice_distinct();
            $this->data['pagos'] = $this->billing_m->get_pago_a();
            $this->data['all_transactions'] = $this->billing_m->transaction();
            $this->data['totals'] = $this->billing_m->distinct_totals();
            //por Obra
            $this->data['all_clients'] = $this->billing_m->all_client_invoice();
            $this->data['projectos'] = $this->billing_m->all_project_distinct();
            $this->data['projectos2'] = $this->billing_m->all_project_distinct2();
            $this->data['transactions'] = $this->billing_m->all_invoice();

            generate_program2_pdf($this->data, '', $this->data['transactions']);


        }

        /**
         * download invoice in pdf
         * @param null $invoice_id
         */
        function pdf_download_program3($project_id = null, $client_id = null)
        {
            $this->load->helper('pdf');

            $client_id = intval($this->uri->segment(4));
            $project_id = intval($this->uri->segment(3));
            //por Proveedor
            $this->data['projects'] = $this->billing_m->get_projects_union();
            $this->data['clients'] = $this->billing_m->all_invoice_distinct();
            $this->data['pagos'] = $this->billing_m->get_pago_a();
            $this->data['all_transactions'] = $this->billing_m->transaction();
            $this->data['totals'] = $this->billing_m->distinct_totals();
            //por Obra
            $this->data['all_clients'] = $this->billing_m->all_client_invoice();
            $this->data['projectos'] = $this->billing_m->all_project_distinct_concept();
            $this->data['projectos2'] = $this->billing_m->all_project_distinct2();
            $this->data['transactions'] = $this->billing_m->all_invoice();

            $this->data['client_id'] = $client_id;
            $this->data['project_id'] = $project_id;

            generate_program3_pdf($this->data, '', $this->data['transactions']);


        }

        /**
         * download invoice in pdf
         * @param null $invoice_id
         */
        function pdf_download_program4($project_id = null, $client_id = null)
        {
            $this->load->helper('pdf');

            $client_id = intval($this->uri->segment(4));
            $project_id = intval($this->uri->segment(3));
            //por Proveedor
            $this->data['projects'] = $this->billing_m->get_projects_union();
            $this->data['clients'] = $this->billing_m->all_invoice_distinct();
            $this->data['pagos'] = $this->billing_m->get_pago_a();
            $this->data['all_transactions'] = $this->billing_m->transaction();
            $this->data['totals'] = $this->billing_m->distinct_totals();
            //por Obra
            $this->data['all_clients'] = $this->billing_m->all_client_invoice();
            $this->data['projectos'] = $this->billing_m->all_project_distinct_concept();
            $this->data['projectos2'] = $this->billing_m->all_project_distinct2();
            $this->data['transactions'] = $this->billing_m->all_invoice();

            $this->data['client_id'] = $client_id;
            $this->data['project_id'] = $project_id;

            generate_program4_pdf($this->data, '', $this->data['transactions']);


        }



        /**
         * download invoice in pdf
         * @param null $invoice_id
         */
        function pdf_download_program_cuentas($project_id = null, $client_id = null)
        {
            $this->load->helper('pdf');

            $client_id = intval($this->uri->segment(4));
            $project_id = intval($this->uri->segment(3));
            //por Proveedor
            $this->data['projects'] = $this->billing_m->get_projects_union();
            $this->data['clients'] = $this->billing_m->all_invoice_distinct();
            $this->data['pagos'] = $this->billing_m->get_pago_a();
            $this->data['all_transactions'] = $this->billing_m->transaction();
            $this->data['totals'] = $this->billing_m->distinct_totals();
            //por Obra
            $this->data['all_clients'] = $this->billing_m->all_client_invoice();
            $this->data['projectos'] = $this->billing_m->all_project_distinct();
            $this->data['projectos2'] = $this->billing_m->all_project_distinct2();
            $this->data['transactions'] = $this->billing_m->all_invoice();

            $this->data['client_id'] = $client_id;
            $this->data['project_id'] = $project_id;

            generate_program5_pdf($this->data, '', $this->data['transactions']);


        }

         function add_new_estimate($project_id = null, $invoice_id = null)
        {


                $invoice_id = $this->input->post('invoice_id');
                $project = get_project_details($this->input->post('project_id'));
                $estimates = get_estimaciones();
                $count = 1;
                if(count($estimates)){
                  foreach ($estimates as $estimate){
                    if ($estimate->project_id == $project->project_id) {
                      $count++;
                    }

                  }
                }


                $param = array(
                    "number" => $count,
                    "client_id" => $project->client_id,
                    "project_id" => $this->input->post('project_id'),
                    "amount" => $this->input->post('total'),
                    "date" => date('Y-m-d H:i:s'),
                    "status" => 0
                );


                if ($this->billing_m->add_estimacion($param)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', 'Estimado creado!');
                }
                redirect('billing/billing');



        }

        function get_project_details($project_id)
        {

            $this->db->from("projects");
            return $this->db->get()->result();
        }

        /**
     * delete message
     */
    function status_program()
    {

        if ($this->input->post()) {

            $id = $this->input->post('id');
              $completo = $this->input->post('completo');

            $this->billing_m->status_program($id, $completo);
            redirect('billing/billing');
        }

    }

    /**
 * delete message
 */
function status_desprogram()
{

    if ($this->input->post()) {

        $id = $this->input->post('id');
          $completo = $this->input->post('completo');

        $this->billing_m->status_desprogram($id, $completo);
        redirect('billing/billing');
    }

}


        /**
     * delete message
     */
    function status_stimate()
    {

        if ($this->input->post()) {

            $id = $this->input->post('id');
            $this->billing_m->status_stimate($id);
            redirect('billing/billing');
        }

    }
        /**
     * delete message
     */
    function status_paid1()
    {

        if ($this->input->post('id')) {

            foreach ($this->input->post('id') as $id) {
                $this->billing_m->status_paid1($id);
            }
        }

    }
      function status_paid2()
    {

        if ($this->input->post('id')) {
            foreach ($this->input->post('id') as $id) {
                $this->billing_m->status_paid2($id);
            }
        }

    }
      function status_paid3()
    {

        if ($this->input->post('id')) {
            foreach ($this->input->post('id') as $id) {
                $this->billing_m->status_paid3($id);
            }
        }

    }
        /**
     * delete message
     */
    function status_deliver()
    {

        if ($this->input->post()) {

          $id = $this->input->post('id');
          $this->billing_m->status_deliver($id);
          redirect('billing/billing');

        }

    }

    function save_estimacion() {

      if ($this->input->post()) {

          $items = json_decode($this->input->post('items'));

          $number = $this->input->post('number');
          $totalm = $this->input->post('totalm');
          $totalma = $this->input->post('totalma');
          $totalmai = $this->input->post('totalmai');
          $totalsub = $this->input->post('totalsub');
          $project_id = $this->input->post('project_id');
          $iva1 = $this->input->post('iva1');
          $iva2 = $this->input->post('iva2');
          $traslado = $this->input->post('traslado');
          $honorario = $this->input->post('honorario');
          $total1 = $this->input->post('total1');
          $total2 = $this->input->post('total2');
          $iva_dif = $this->input->post('iva_dif');
          $client_user = $this->input->post('client_user');

          if ($iva1 == 0) {
              $iva = $iva2;
          }else {
              $iva = $iva1;
          }
          $totals = array(
              'id_amount' => $number,
              'project_id' => $project_id,
              'mano_obra' => $totalm,
              'materiales' => $totalma,
              'materiales_iva' => $totalmai,
              'subcont' => $totalsub,
              'iva' => $iva,
              'traslados' => $traslado,
              'honorarios' => $honorario,
              'iva_dif' => $iva_dif
          );

          if ($total1 == 0) {
              $total = $total2;
          }else {
              $total = $total1;
          }
          $est = array(
              "number" => $number,
              "client_id" => $client_user,
              "project_id" => $project_id,
              "amount" => $total,
              "date" => date('Y-m-d H:i:s'),
              "status" => 0
          );

          $save = $this->billing_m->insert_estimacion($items, $totals, $est);

          if ($save) {

              $this->session->set_flashdata('msg_status', 'success');
              $this->session->set_flashdata('message', 'Estimado creado!');

        } else {
            //failed to add invoice
            $response = array(
                'success' => 0,
                'msg_status' => "error",
                'message' => "Invoice could not be created, make sure you added items"
            );
            echo json_encode($response);
        }

      }
    }

    function save_pago() {

      if ($this->input->post()) {

          $items = json_decode($this->input->post('items'));

          $listo = $this->billing_m->insert_pago($items);

          if ($listo) {

            $response = array(
                'success' => 1,
                'msg_status' => "success",
                'message' => "Estimacion guardada exitosamente !"
            );
            echo json_encode($response);

        } else {
            //failed to add invoice
            $response = array(
                'success' => 0,
                'msg_status' => "error",
                'message' => "Invoice could not be created, make sure you added items"
            );
            echo json_encode($response);
        }

      }
    }

    function pago_a(){

      if ($this->input->post()) {

          $invo_id = $this->input->post('inv_id');

          $param = array(
              "inv_id" => $this->input->post('inv_id'),
              "client_id" => $this->input->post('client_id'),
              "project_id" => $this->input->post('project_id'),
              "monto" => $this->input->post('monto'),
              "tax" => $this->input->post('tax'),
              "date" => date('Y-m-d H:i:s')
          );


          if ($this->billing_m->add_pago_a($param, $invo_id)) {

              $this->session->set_flashdata('msg_status', 'success');
              $this->session->set_flashdata('message', lang('messages_invoice_update_success'));
          }
          redirect('billing/billing');
      }

    }


    function payment_trans()
    {

      $this->data['invoice_number'] = $this->config->item('invoice')->trans_prefix . "-" . trans_reference(16,
              $this->Company_id);

      $this->data['pago_a'] = $this->billing_m->get_pago_all();

      if ($this->CompanyUserRolePermission_id == 1) {//if admin

          $this->data['transactions'] = $this->billing_m->all_transaction();

      } elseif ($this->CompanyUserRolePermission_id == 2) {

          $this->data['transactions'] = null;

      } else {

          $this->data['transactions'] = $this->billing_m->client_all_invoice($this->CompanyClient_id); // all company trans..

      }
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('modal/payment_trans', $this->data);
    }

    function payment_trans_2($project_id = null, $client_id = null)
    {
        $client_id = intval($this->uri->segment(4));
        $project_id = intval($this->uri->segment(3));

      $this->data['invoice_number'] = $this->config->item('invoice')->trans_prefix . "-" . trans_reference(16,
              $this->Company_id);

      $this->data['pago_a'] = $this->billing_m->get_pago_all_2($project_id,$client_id);

      if ($this->CompanyUserRolePermission_id == 1) {//if admin

          $this->data['transactions'] = $this->billing_m->all_invoice_2($project_id,$client_id);

      } elseif ($this->CompanyUserRolePermission_id == 2) {

          $this->data['transactions'] = null;

      } else {

          $this->data['transactions'] = $this->billing_m->all_invoice_2($this->CompanyClient_id); // all company trans..

      }
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('modal/payment_trans_2', $this->data);
    }

    /**
     * delete message
    */
    function created_trans()
    {
      if ($this->input->post()) {

          $folio = $this->input->post('folio');
          $inv_id = $this->input->post('inv_id');
          $payment = $this->input->post('payment_method');
          $factura = $this->input->post('factura');
          $reference = "";
          foreach($factura as $fac){

            $reference .= $fac . " ";

          }

              $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
              $transaction = array(
                  "invoice_id" => 0,
                  "trans_id" => 0,
                  "client_id" => $this->input->post('client_id'),
                  "project_id"  => $this->input->post('project_id'),
                  "amount" => $this->input->post('amount'),
                  "folio" => $this->input->post('folio'),
                  "cheques" => $this->input->post('cheque'),
                  "payment_method" => $payment,
                  "ip" => $this->input->ip_address(),
                  "notes" => "invoice paid by" . $this->CompanyUser_FullName,
                  "creator_email" => $this->CompanyUser_email,
                  "created_by" => $user_id,
                  "reference" => $reference,
                  "created_at" => date('Y-m-d H:i:s'),
                  "updated_at" => date('Y-m-d H:i:s')
              );


              if ($this->billing_m->created_trans($transaction, $folio, $payment, $inv_id)) {

                  $this->session->set_flashdata('msg_status', 'success');
                  $this->session->set_flashdata('message', lang('messages_payment_add_success'));


              }
              if ($this->input->post('payment_send_mail') == 'on') {

                  $this->notifications->send_payment_notification($client_details->client_id, $invoice_id,
                      $this->input->post('payment_amount'));
              }

              redirect('billing/billing');


      }
    }

    function nota_credito()
       {

          $this->data['clients'] = $this->billing_m->get_proveedor_list();
          $this->data['facturas'] = $this->billing_m->get_facturas();
          $this->data['projects'] = $this->billing_m->get_projects();
          $this->data['invoice_number'] = $this->config->item('invoice')->nota_prefix . "-" . trans_reference(17,
                  $this->Company_id);
           $this->data['SCRIPT_PAGE'] = true;

           $this->load->view('modal/nota_credito', $this->data);

       }

    function add_nota_credito()
       {

           if ($this->input->post()) {

                       $monto = $this->input->post('amount');
                       $name = $this->input->post('folio');
                       $client = $this->input->post('client_id');
                       $invoice = $this->input->post('invoice');
                       $project = $this->input->post('project');
                       $tax = $this->input->post('tax');
                       $user_id = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;

                       $start_date = ($this->input->post('start_date')) ? $this->input->post('start_date') : date('Y-m-d');
                       $end_date = ($this->input->post('end_date')) ? $this->input->post('end_date') : date('Y-m-d');

                       if ($currency_id == 'default') {
                           $client_currency = get_client_details($client_id);
                           $currency = get_currency_name($client_currency->client_currency);
                       } else {

                           $currency = get_currency_name($currency_id);
                       }

                       $param = array(
                           "inv_no" => $this->input->post('folio'),
                           "client_id" => $this->input->post('client_id'),
                           "project_id" => $this->input->post('project'),
                           "status" => 2,
                           "approved" => 1,
                           "disc_desc" => $invoice,
                           "currency" => " ",
                           "solicitante" => " ",
                           "autorizado" => " ",
                           "notes" => " ",
                           "invoice_due" => date('Y-m-d', strtotime($end_date)),
                           "created_at" => date('Y-m-d', strtotime($start_date)),
                           "updated_at" => date('Y-m-d H:i:s'),
                           "inv_created_by" => $user_id,
                           "filter" => 0,
                           "tax" => $tax,
                           "type" => 0,
                           "type_pre" => 5

                       );

                   if ($this->billing_m->add_nota($param, $monto, $tax, $name, $client, $project, $invoice)) {

                       $this->session->set_flashdata('msg_status', 'success');
                       $this->session->set_flashdata('message', 'Nota de credito agregada con éxito !');

                   }
                  //  redirect('billing/billing');
           }
       }

       function add_gastos(){

         $this->data['clients'] = $this->billing_m->get_proveedor_list();
         $this->data['projects'] = $this->billing_m->get_projects();
         $this->data['milestones'] = $this->billing_m->get_milestones();
         $this->data['invoice_number'] = $this->config->item('invoice')->gasto_prefix . "-" . trans_reference(18,
                 $this->Company_id);
          $this->data['SCRIPT_PAGE'] = true;
          $this->data['show_expenses_overview'] = true;
          $this->load->view('modal/add_gastos', $this->data);
       }

       /**
        * download invoice in pdf
        * @param null $invoice_id
        */
       function concept_edit_modal($project_id = null, $client_id = null)
       {

           $client_id = intval($this->uri->segment(4));
           $project_id = intval($this->uri->segment(3));
           //por Proveedor
           $this->data['projects'] = $this->billing_m->get_projects();
           $this->data['clients'] = $this->billing_m->all_invoice_distinct($client_id);
           $this->data['pagos'] = $this->billing_m->get_pago_a();
           $this->data['all_transactions'] = $this->billing_m->transaction();
           $this->data['totals'] = $this->billing_m->distinct_totals();
           //por Obra
           $this->data['all_clients'] = $this->billing_m->all_client_invoice($client_id);
           $this->data['projectos'] = $this->billing_m->all_project_distinct_concept($project_id);
           $this->data['transactions'] = $this->billing_m->all_invoice();

           $this->data['client_id'] = $client_id;
           $this->data['project_id'] = $project_id;

           $this->data['SCRIPT_PAGE'] = true;
           $this->data['show_expenses_overview'] = true;

           $this->load->view('modal/concept_edit', $this->data);


       }

       /**
        * download invoice in pdf
        * @param null $invoice_id
        */
       function concept_edit_modal_tax($project_id = null, $client_id = null)
       {

           $client_id = intval($this->uri->segment(4));
           $project_id = intval($this->uri->segment(3));
           //por Proveedor
           $this->data['projects'] = $this->billing_m->get_projects();
           $this->data['clients'] = $this->billing_m->all_invoice_distinct($client_id);
           $this->data['pagos'] = $this->billing_m->get_pago_a();
           $this->data['all_transactions'] = $this->billing_m->transaction();
           $this->data['totals'] = $this->billing_m->distinct_totals();
           //por Obra
           $this->data['all_clients'] = $this->billing_m->all_client_invoice($client_id);
           $this->data['projectos'] = $this->billing_m->all_project_distinct_concept($project_id);
           $this->data['transactions'] = $this->billing_m->all_invoice();

           $this->data['client_id'] = $client_id;
           $this->data['project_id'] = $project_id;

           $this->data['SCRIPT_PAGE'] = true;
           $this->data['show_expenses_overview'] = true;

           $this->load->view('modal/concept_edit_tax', $this->data);


       }

       /**
        * update task
        */
       function update_concept()
       {

           if ($this->input->post()) {

               $client_id = intval($this->input->post('client_id'));
               $project_id = intval($this->input->post('project_id'));
               $param = array(
                   "concept" => $this->input->post('concepto')
               );

               }

               if ($this->billing_m->update_concept($param, $client_id, $project_id)) {

                   $this->session->set_flashdata('msg_status', 'success');
                   $this->session->set_flashdata('message', lang('messages_invoice_update_success'));
               }
               $this->session->set_flashdata('msg_status', 'success');
               $this->session->set_flashdata('message', lang('messages_invoice_update_success'));
               redirect('billing/program_todo/');

           }


    function excel(){
        require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
        require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("");
        $objPHPExcel->getProperties()->setLastModifiedBy("");
        $objPHPExcel->getProperties()->setTitle("");
        $objPHPExcel->getProperties()->setSubject("");
        $objPHPExcel->getProperties()->setDescription("..");

        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Task ID');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'User ID');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Task');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Description');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Date 1');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Date 2');

        $filname = "Task-Exported-on-".date('Y-m-d-H-i-s').'.xlsx';
        $objPHPExcel->getActiveSheet()->setTitle("Task-Overview");

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filname.'"');
        header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $writer->save('php://output');
        exit;

    }

}
