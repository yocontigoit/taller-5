<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/26
 * Time: 03:28 PM
 */
class Billing_M extends CI_Model
{

    /**
     * list all invoices
     * @return mixed
     */
    function  all_invoice()
    {

        $this->db->from('invoice');


        return $this->db->get()->result();
    }

    /**
     * list all invoices
     * @return mixed
     */
    function  all_invoice_2($project_id = null, $client_id = null)
    {

        $this->db->from('invoice');
        $this->db->where('project_id', $project_id);
        $this->db->where('client_id', $client_id);

        return $this->db->get()->result();
    }

    /**
     * list all invoices
     * @return mixed
     */
    function  all_invoice_3($invo_id = null)
    {

        $this->db->from('invoice');
        $this->db->where('id', $invo_id);

        return $this->db->get()->result();
    }

     function  get_facturas()
    {

        $this->db->from('transaction');

        return $this->db->get()->result();
    }



    function get_avances_raya($inv_id)
    {
        $this->db->from('raya_avance');
$this->db->where("ra_inv_id", $inv_id);
        return $this->db->get()->result();
    }

    /**
     * list all invoices_item
     * @return mixed
     */
    function  all_invoice_item()
    {

        $this->db->from('invoice_item');

        return $this->db->get()->result();
    }

    function  get_avances()
    {

        $this->db->from('import');

        return $this->db->get()->result();
    }

    /**
     * list all invoices_item
     * @return mixed
     */
    function  all_client()
    {

        $this->db->from('client');

        return $this->db->get()->result();
    }

    /**
     * list all invoices_item
     * @return mixed
     */
    function  all_client_invoice()
    {

        $this->db->from('client');
        $this->db->group_by('client.client_id');
        $this->db->join('invoice', "client.client_id = invoice.client_id");

        return $this->db->get()->result();
    }

    /**
     * list all invoices_item
     * @return mixed
     */
    function  get_invoice_item()
    {

        $this->db->from('invoice');
        $this->db->join('invoice_item', 'invoice.id = invoice_item.invoice_id' );
        return $this->db->get()->result();
    }

    /**
     * list all invoices_item
     * @return mixed
     */
    function  get_invoice_item_dis()
    {
        $this->db->from('invoice');
        $this->db->join('invoice_item', 'invoice.id = invoice_item.invoice_id' );
        return $this->db->get()->result();
    }



    /**
     * list all invoices
     * @return mixed
     */
    function  all_invoice_row($project_id)
    {

        $this->db->from('invoice');
        $this->db->where("project_id", $project_id);

        return $this->db->get()->row();

    }
     function  all_invoice_rows()
    {

        $this->db->from('invoice');

        return $this->db->get()->row();

    }
    /**
     * list all invoices
     * @return mixed
     */
    function  all_invoice_monto($inv_id)
    {

        $this->db->from('invoice');
        $this->db->where("id", $inv_id);

        return $this->db->get()->row();

    }
    function  all_invoiced_clients()
    {

       return $this->db->from('invoice')->distinct()->get()->result();

    }
    function  all_invoiced_clients_row()
    {

       return $this->db->from('invoice')->distinct()->get()->row();

    }

    function get_project_details()
    {

        $this->db->from("projects");
        return $this->db->get()->result();
    }

    /**
     * retrive all client invoices
     * @param $client_id
     * @return mixed
     */
    function client_all_invoice($client_id)
    {

        $this->db->from('invoice');
        $this->db->where("client_id", $client_id);

        return $this->db->get()->result();

    }

    /**
     * list all invoices
     * @return mixed
     */
    function  all_invoice_estimate($project_id)
    {

        $this->db->from('invoice');
        $this->db->where("project_id", $project_id);

        return $this->db->get()->result();
    }
    function  all_invoice_estimates()
    {

        $this->db->from('invoice');

        return $this->db->get()->result();
    }

    /**
     * list all invoices
     * @return mixed
     */
    function  all_estimacion()
    {

        $this->db->from('estimacion');
        return $this->db->get()->result();
    }

    /**
     * retrive all client invoices
     * @param $client_id
     * @return mixed
     */
    function client_all_invoice_estimate($project_id)
    {

        $this->db->from('invoice');
        $this->db->where("project_id", $project_id);

        return $this->db->get()->result();

    }

    function get_projects()
    {
        $this->db->from('projects');

    return $this->db->get()->result();
    }

    function get_projects_asc()
    {
        $this->db->from('projects');
        $this->db->order_by('project_id', 'asc');

    return $this->db->get()->result();
    }

    function get_projects_union()
    {
        $this->db->select('DISTINCT(projects.project_id), projects.project_title');
        $this->db->from('projects');
        $this->db->join('invoice', 'projects.project_id = invoice.project_id');
    return $this->db->get()->result();
    }

    function get_projects_estimate($project_id)
    {
        $this->db->from('projects');
        $this->db->where('project_id', $project_id);
    return $this->db->get()->result();
    }

    /**
     * Get clients by company
     * @param null $company_id
     * @return mixed
     */
    function get_company_clients($company_id = null)
    {

        $company_id = intval($company_id);
        $this->db->from('client');
        $this->db->where("company_id", $company_id);

        return $this->db->get()->result();
    }

    /**
     * get all company items
     * @param null $id
     * @return mixed
     */
    function get_all_items($id = null)
    {
        $company_id = intval($id);
        $this->db->from('items');

        return $this->db->get()->result();
    }

    /**
     * get client details
     * @param $client_id
     * @return mixed
     */
    function get_client_details($client_id)
    {

        return $this->db->where('client_id', $client_id)->get('client')->row();
    }

    /**
     * retrieve currencies
     * @return mixed
     */
    function get_currencies()
    {

        return $this->db->get('settings_currency')->result();
    }

    /**
     * Get all transactions...
     * @return mixed
     *
     */
    function all_transaction()
    {

        $this->db->select('*, transaction.id AS payment_id,invoice.payment_method AS pay_method, invoice');
        $this->db->from('transaction');
        $this->db->join('invoice', 'transaction.invoice_id = invoice.id');

        return $this->db->get()->result();
    }


    function transaction()
    {

       $this->db->from('transaction');

        return $this->db->get()->result();
    }

    function get_estimaciones()
    {

      $this->db->select('estimaciones');

      return $this->db->get->result();
    }

    function get_company_users($id = null)
    {

        $company_id = intval($id);
        $this->db->from('users');
        $this->db->join('employee', 'employee.emp_id  = users.user_id');
        $this->db->where("company_id", $company_id);

        return $this->db->get()->result();

    }



    /**
     * get all transaction by client
     * @param $client_id
     * @return mixed
     */
    function client_transaction($client_id)
    {
        $this->db->select('*, transaction.id AS payment_id,invoice.payment_method AS pay_method');
        $this->db->from('transaction');
        $this->db->join('invoice', 'transaction.invoice_id = invoice.id');
        $this->db->where('invoice.client_id', $client_id);

        return $this->db->get()->result();

    }

    /**
     * get all transaction by client
     * @param $client_id
     * @return mixed
     */
    function client_transaction2()
    {

        $this->db->from('transaction');
 $this->db->join('settings_payment_methods', 'settings_payment_methods.setting_id = transaction.payment_method');

        return $this->db->get()->result();

    }

    function get_invo_client(){
      $this->db->select('*');
      $this->db->from('invoice');
      $this->db->group_by("invoice.client_id");
      $this->db->join('client', 'invoice.client_id = client.client_id');
      // $this->db->group_by(array("invoice.client_id", "invoice.project_id"));

        return $this->db->get()->result();
    }

    /**
     * list all invoices
     * @return mixed
     */
    function  all_invoice_distinct()
    {


      $this->db->select('DISTINCT(invoice.client_id)');
      $this->db->select('invoice.client_id, invoice.project_id, invoice.program, invoice.type_pre, client.client_type');
      $this->db->from('invoice');
      $this->db->join('client', 'invoice.client_id = client.client_id');
      $this->db->where('invoice.type_pre != 5');

        return $this->db->get()->result();
    }


    /**
     * list all invoices
     * @return mixed
     */
    function  all_project_distinct()
    {

      $this->db->select('distinct(invoice.client_id)');
      $this->db->select('invoice.project_id, invoice.program, invoice.client_id, client.client_type, invoice.program');
      $this->db->from('invoice');
      $this->db->join('client', 'invoice.client_id = client.client_id');
      $this->db->where('invoice.type_pre != 5');

        return $this->db->get()->result();
    }

    /**
     * list all invoices
     * @return mixed
     */
    function  all_project_distinct2()
    {

      $this->db->select('distinct(invoice.client_id)');
      $this->db->select('invoice.project_id, invoice.program, invoice.client_id, client.client_type, invoice.program, invoice.tax');
      $this->db->from('invoice');
      $this->db->join('client', 'invoice.client_id = client.client_id');
      $this->db->where('invoice.type_pre != 5');

        return $this->db->get()->result();
    }

    /**
     * list all invoices
     * @return mixed
     */
    function  all_project_distinct_concept()
    {

      $this->db->select('distinct(invoice.client_id)');
      $this->db->select('invoice.project_id, invoice.program, invoice.client_id, client.client_type, invoice.program, invoice.concept');
      $this->db->from('invoice');
      $this->db->join('client', 'invoice.client_id = client.client_id');

        return $this->db->get()->result();
    }

    /**
     * list all invoices
     * @return mixed
     */
    function  distinct_totals()
    {

      $this->db->group_by('projects.project_id');
      $this->db->from('invoice');
      $this->db->join('projects', 'invoice.project_id = projects.project_id');

        return $this->db->get()->result();
    }


    /**
     * Get all invoiced transactions
     * @param $invoice_id
     * @return mixed
     */
    function get_invoice_transaction($invoice_id)
    {
        $this->db->from('transaction');
        $this->db->where('invoice_id', intval($invoice_id));

        return $this->db->get()->result();

    }
     function get_method_transactions($method_id)
    {
        $this->db->from('transaction');
        $this->db->where('payment_method', intval($method_id));

        return $this->db->get()->result();

    }


    /**
     * retrieve invoice information
     * @param $inv_id
     * @return mixed
     */
    function  client_invoice($inv_id)
    {

        $this->db->from('invoice');
        $this->db->where("id", $inv_id);

        return $this->db->get()->row();
    }

    /**
     * Get invoice items
     * @param null $id
     * @return mixed
     */
    function  get_invoice_items($id = null)
    {

        $this->db->from('invoice_item');
        $this->db->where("invoice_id", $id);

        return $this->db->get()->result();
    }

    /**
     * Get invoice items
     * @param null $id
     * @return mixed
     */
    function  get_invoice_items2($invoice_id = null, $id = null)
    {

        $this->db->from('invoice_item');
        $this->db->where("invoice_id", $invoice_id);
        $this->db->where("id", $id);
        return $this->db->get()->row();
    }

    function get_est_amount($project_id = null, $est_num = null){

        $this->db->from('est_amount');
        $this->db->where('project_id', $project_id);
        $this->db->where('id_amount', $est_num);


        return $this->db->get()->result();
    }


    /**
     * add payment to invoice
     * @param array $data
     * @return bool
     */
    function add_payment($data = array(), $id = null, $invoice = array())
    {

        $success = false;
        $this->db->trans_start();
        $success = $this->db->insert('transaction', $data);

        if ($success) {
            //factura
            $success = $this->db->update('invoice', array('factura' => 1), array('id' => $id));
        }

        if ($success) {
            //factura
            $success = $this->db->where('id', $id)->update('invoice', $invoice);
        }

        if ($success) {
            //check
            $invoice_id = intval($data['invoice_id']);
            $success = $this->set_invoice_status($invoice_id);

        }
        $this->db->trans_complete();

        return $success;

    }

    /**
     * add payment to invoice
     * @param array $data
     * @return bool
     */
    function change_payment($data = array(), $id = null, $invoice = array())
    {

        $success = false;
        $this->db->trans_start();
        $success = $this->db->where('invoice_id', $id)->update('transaction', $data);

        if ($success) {
            //factura
            $success = $this->db->update('invoice', array('factura' => 1), array('id' => $id));
        }

        if ($success) {
            //factura
            $success = $this->db->where('id', $id)->update('invoice', $invoice);
        }

        if ($success) {
            //check
            $invoice_id = intval($data['invoice_id']);
            $success = $this->set_invoice_status($invoice_id);

        }
        $this->db->trans_complete();

        return $success;

    }

      function add_estimacion($data = array())
    {

        $success = false;
        $this->db->trans_start();
        $success = $this->db->insert('estimaciones', $data);

        if ($success) {
            //check
            $invoice_id = intval($data['client_id']);
            $success = $this->set_invoice_status($invoice_id);

        }
        $this->db->trans_complete();

        return $success;

    }

    /**
     * update invoice status
     * @param null $invoice_id
     * @return bool
     */
    public function set_invoice_status($invoice_id = null)
    {
        $success = false;
        $invoice_amt = $this->get_invoice_total_amt($invoice_id);
        $paid_amt = $this->get_invoice_paid_amt($invoice_id);

        if ($paid_amt < 1) {
            $invoice_status = 2;
        } elseif (($invoice_amt - $paid_amt) <= 0) {
            $invoice_status = 1;
        } else {
            $invoice_status = 3;
        }

        $status = array('status' => $invoice_status, 'paid_at' => date('Y-m-d H:i:s'));
        $success = $this->db->update('invoice', $status, array('id' => $invoice_id));

        return $success;
    }

    /**
     * update invoice status
     * @param null $invoice_id
     * @return bool
     */
    public function update_orden($data = array(), $tax = array(), $id = null, $invoice_id = null)
    {
        $success = false;

        $success = $this->db->where('id', $id)->update('invoice_item', $data);

        if ($success) {
            $success = $this->db->where('invoice_id', $invoice_id)->update('invoice_item', $tax);
        }

        if ($success) {
            $success = $this->db->where('id', $invoice_id)->update('invoice', $tax);
        }

        return $success;
    }

    /**
     * update estimate
     * @param null $invoice_id
     * @return bool
     */
    function update_estimate($items = array())
    {
        $success = false;

        foreach ($items as $item) {

                $id = intval($item->item_id);

                $param = array(
                    'estimate' => $item->item_estimate,
                    'for_estimate' => $item->item_for_estimate,
                    'program' => $item->item_program,
                    'billing' => $item->item_billing,
                    'deliver' => $item->item_deliver
                );

                $success = $this->db->where('id', $id)->update('invoice',$param);
        }
        return $success;

    }

    /**
     * update concept invoice
     * @param null $client_id
     * @return bool
     */
    public function update_concept($data = array(), $client_id = null, $project_id = null)
    {
        $success = false;

        $success = $this->db->where('client_id', $client_id, 'project_id', $project_id)->update('invoice', $data);

        return $success;
    }



    /**
     * get invoice total
     * @param null $invoice_id
     * @return int
     */
    public function get_invoice_total_amt($invoice_id = null)
    {

        return invoice_sub_total_total($invoice_id);

    }

    /**
     * get invoiced paid amount
     * @param null $invoice_id
     * @return int
     */
    public function get_invoice_paid_amt($invoice_id = null)
    {
        return _get_invoice_payments($invoice_id);
    }

    function get_all_paid_invoices()
    {
        $this->db->from('invoice');
        //$this->db->join('transaction', 'transaction.invoice_id = invoice.id');
        $this->db->where("invoice.status = 1");

        return $this->db->get()->result();
    }

    function get_all_client_paid_invoices($client_id)
    {
        $this->db->from('invoice');
        // $this->db->join('transaction', 'transaction.invoice_id = invoice.id');
        $this->db->where("invoice.status = 1");
        $this->db->where('client_id', intval($client_id));

        return $this->db->get()->result();
    }

    /**
     * retrieve all unpaid invoices
     * @return mixed
     */
    function get_all_unpaid_invoices()
    {
        $this->db->from('invoice');
        $this->db->where("status = 2");
        $this->db->where('invoice_due >= NOW()');

        return $this->db->get()->result();
    }

    /**
     * retieve client unpaid invoices
     * @param $client_id
     * @return mixed
     */
    function get_all_client_unpaid_invoices($client_id)
    {
        $this->db->from('invoice');
        $this->db->where("status = 2");
        $this->db->where('invoice_due >= NOW()');
        $this->db->where('client_id', intval($client_id));

        return $this->db->get()->result();
    }

    /**
     * retrieve overdue invoices
     * @return mixed
     */
    function get_all_overdue_invoices()
    {
        $this->db->from('invoice');
        $this->db->where("status = 2");
        //$this->db->where("status <> 1");
        $this->db->where('invoice_due < NOW()');

        return $this->db->get()->result();
    }

    /**
     * get all client overdue invoices
     * @param $client_id
     * @return mixed
     */
    function get_all_client_overdue_invoices($client_id)
    {
        $this->db->from('invoice');
        $this->db->where("status = 2");
        //$this->db->where("status <> 1");
        $this->db->where('invoice_due < NOW()');
        $this->db->where('client_id', intval($client_id));

        return $this->db->get()->result();
    }


    /**
     * add invoice
     * @param array $data
     * @param array $items
     * @return null / invoice_id
     */
    function add_invoice($data = array(), $items = array(), $client_id = null, $project = null)
    {

        $success = false;
        $this->db->trans_start();

        if(!$success) {
            $success = $this->db->insert('invoice', $data);
            $invoice_id = $this->db->insert_id();

            if ($success) {
                //add items
                foreach ($items as $item) {
                    if ($item->item_hours_worked != '' && $item->hourly_rate != '' && $item->item_name != '') {
                        $item_name = get_item_name($item->item_name);
                        $param = array(
                            "invoice_id" => $invoice_id,
                            "client_id" => $client_id,
                            "project_id" => $project,
                            "item_name" => $item_name,
                            "item_desc" => $item->item_description,
                            "item_avance" => $item->item_avance,
                            "discount" => $item->item_discount,
                            "tax" => $item->item_tax,
                            "quantity" => $item->item_hours_worked,
                            "price" => $item->hourly_rate

                        );
                        $success = $this->db->insert('invoice_item', $param);

                    }
                }

                if ($success) {
                    $reference_no = explode("-", $data['inv_no']);
                    $success = $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
                        array('typeid' => 10, 'company_id' => $this->Company_id));
                }
            }
        }
        $items_count = $this->db->where(array('invoice_id' => $invoice_id))->get('invoice_item')->result();

        if(count($items_count)){
            $this->db->trans_complete();
        }else{
            $this->db->trans_rollback();
            $success = false;
        }

        return ($success) ? $invoice_id : null;
    }

    /**
     * add invoice
     * @param array $data
     * @param array $items
     * @return null / invoice_id
     */
    function add_presupuesto($data = array(), $items = array(), $project = null, $client_id = null )
    {

      $success = false;
      $this->db->trans_start();

      if(!$success) {
          $success = $this->db->insert('invoice', $data);
          $invoice_id = $this->db->insert_id();

          if ($success) {
              //add items
              foreach ($items as $item) {
                  if ($item->item_hours_worked != '' && $item->hourly_rate != '' && $item->item_name) {
                      $item_name = get_item_name($item->item_name);
                      $param = array(
                          "invoice_id" => $invoice_id,
                          "client_id" => $client_id,
                          "project_id" => $project,
                          "item_name" => $item->item_name,
                          "item_desc" => $item->item_description,
                          "discount" => $item->item_discount,
                          "tax" => $item->item_tax,
                          "quantity" => $item->item_hours_worked,
                          "price" => $item->hourly_rate

                      );
                      $success = $this->db->insert('invoice_item', $param);

                  }
              }

              // if ($success) {
              //     $reference_no = explode("-", $data['inv_no']);
              //     $success = $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
              //         array('typeid' => 10, 'company_id' => $this->Company_id));
              // }

          }

            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $transaction = array(
                "invoice_id" => $invoice_id,
                "trans_id" => 0,
                "client_id" => 0,
                "project_id"  => 0,
                "amount" => " ",
                "folio" => " ",
                "cheques" => " ",
                "payment_method" => " ",
                "ip" => $this->input->ip_address(),
                "notes" => "invoice paid by" . $this->CompanyUser_FullName,
                "creator_email" => $this->CompanyUser_email,
                "created_by" => $user_id,
                "reference" => " ",
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            );
            $success = $this->db->insert('transaction', $transaction);
      }

      $estimate = array(
          "est_inv"  => $invoice_id,
          "est_project" => $project,
          "est_client" => $client_id,
          "est_concept" => " ",
          "est_desc" => " ",
          "est_amount" => 0
      );
      $success = $this->db->insert('estimacion', $estimate);


      $items_count = $this->db->where(array('invoice_id' => $invoice_id))->get('invoice_item')->result();

      if(count($items_count)){
          $this->db->trans_complete();
      }else{
          $this->db->trans_rollback();
          $success = false;
      }

      return ($success) ? $invoice_id : null;
    }

    /**
     * add invoice
     * @param array $data
     * @param array $items
     * @return null / invoice_id
     */
    function add_raya($data = array(), $items = array(), $project = null, $client_id = null )
    {

        $success = false;
        $this->db->trans_start();

        if(!$success) {
            $success = $this->db->insert('invoice', $data);
            $invoice_id = $this->db->insert_id();

            if ($success) {
                //add items
                foreach ($items as $item) {
                    if ($item->item_hours_worked != '' && $item->hourly_rate != '' && $item->item_name != '') {
                        $item_name = get_client_name($item->item_name);
                        $param = array(
                            "invoice_id" => $invoice_id,
                            "client_id" => $client_id,
                            "project_id" => $project,
                            "item_name" => $item_name,
                            "item_desc" => $item->item_description,
                            "unit_raya" => $item->item_units_sd,
                            "discount" => $item->item_discount,
                            "tax" => $item->item_tax,
                            "quantity" => $item->item_hours_worked,
                            "price" => $item->hourly_rate

                        );
                        $success = $this->db->insert('invoice_item', $param);

                    }
                }

                if ($success) {
                    $reference_no = explode("-", $data['inv_no']);
                    $success = $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
                        array('typeid' => 15, 'company_id' => $this->Company_id));
                }

                $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $transaction = array(
                    "invoice_id" => $invoice_id,
                    "trans_id" => 0,
                    "client_id" => 0,
                    "project_id"  => 0,
                    "amount" => " ",
                    "folio" => " ",
                    "cheques" => " ",
                    "payment_method" => " ",
                    "ip" => $this->input->ip_address(),
                    "notes" => "invoice paid by" . $this->CompanyUser_FullName,
                    "creator_email" => $this->CompanyUser_email,
                    "created_by" => $user_id,
                    "reference" => " ",
                    "created_at" => date('Y-m-d H:i:s'),
                    "updated_at" => date('Y-m-d H:i:s')
                );
                $success = $this->db->insert('transaction', $transaction);

                $estimate = array(
                    "est_inv"  => $invoice_id,
                    "est_project" => $project,
                    "est_client" => $client_id,
                    "est_concept" => " ",
                    "est_desc" => " ",
                    "est_amount" => 0
                );
                $success = $this->db->insert('estimacion', $estimate);
            }
        }
        $items_count = $this->db->where(array('invoice_id' => $invoice_id))->get('invoice_item')->result();

        if(count($items_count)){
            $this->db->trans_complete();
        }else{
            $this->db->trans_rollback();
            $success = false;
        }

        return ($success) ? $invoice_id : null;
    }

    /**
     * add invoice
     * @param array $data
     * @param array $items
     * @return null / invoice_id
     */
    function add_oficina($data = array(), $items = array(), $project = null, $client_id = null )
    {

        $success = false;
        $this->db->trans_start();

        if(!$success) {
            $success = $this->db->insert('invoice', $data);
            $invoice_id = $this->db->insert_id();

            if ($success) {
                //add items
                foreach ($items as $item) {
                    if ($item->item_hours_worked != '' && $item->hourly_rate != '' && $item->item_name != '') {
                        $item_name = _company_user_name($item->item_name);
                        $param = array(
                            "invoice_id" => $invoice_id,
                            "client_id" => $client_id,
                            "project_id" => $project,
                            "item_name" => $item_name,
                            "item_desc" => $item->item_description,
                            "unit_raya" => $item->item_units_sd,
                            "discount" => $item->item_discount,
                            "tax" => $item->item_tax,
                            "quantity" => $item->item_hours_worked,
                            "price" => $item->hourly_rate

                        );
                        $success = $this->db->insert('invoice_item', $param);

                    }
                }

                if ($success) {
                    $reference_no = explode("-", $data['inv_no']);
                    $success = $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
                        array('typeid' => 19, 'company_id' => $this->Company_id));
                }

                $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $transaction = array(
                    "invoice_id" => $invoice_id,
                    "trans_id" => 0,
                    "client_id" => 0,
                    "project_id"  => 0,
                    "amount" => " ",
                    "folio" => " ",
                    "cheques" => " ",
                    "payment_method" => " ",
                    "ip" => $this->input->ip_address(),
                    "notes" => "invoice paid by" . $this->CompanyUser_FullName,
                    "creator_email" => $this->CompanyUser_email,
                    "created_by" => $user_id,
                    "reference" => " ",
                    "created_at" => date('Y-m-d H:i:s'),
                    "updated_at" => date('Y-m-d H:i:s')
                );
                $success = $this->db->insert('transaction', $transaction);

                $estimate = array(
                    "est_inv"  => $invoice_id,
                    "est_project" => $project,
                    "est_client" => $client_id,
                    "est_concept" => " ",
                    "est_desc" => " ",
                    "est_amount" => 0
                );
                $success = $this->db->insert('estimacion', $estimate);
            }
        }
        $items_count = $this->db->where(array('invoice_id' => $invoice_id))->get('invoice_item')->result();

        if(count($items_count)){
            $this->db->trans_complete();
        }else{
            $this->db->trans_rollback();
            $success = false;
        }

        return ($success) ? $invoice_id : null;
    }

    /**
     * add invoice
     * @param array $data
     * @param array $items
     * @return null / invoice_id
     */
    function add_nota($data = array(), $monto = null, $tax = null, $name = null, $client = null, $project = null, $invoice = null)
    {

        $success = false;
        $this->db->trans_start();

        if(!$success) {
            $success = $this->db->insert('invoice', $data);
            $invoice_id = $this->db->insert_id();

            if ($success) {
                //add items
                        $param = array(
                            "invoice_id" => $invoice_id,
                            "client_id" => $client,
                            "project_id" => $project,
                            "item_name" => $name,
                            "item_desc" => " ",
                            "invoice_f" => $invoice,
                            "nc" => 1,
                            "discount" => 0,
                            "nc_tax" => $tax,
                            "quantity" => -1,
                            "price" => $monto

                        );
                        $success = $this->db->insert('invoice_item', $param);


                }

                if ($success) {
                    //add items
                            $param = array(
                                "nc_amount" => $monto
                            );
                            $success = $this->db->update('invoice', $param, array('invoice_inv' => $invoice) );
                    }

                if ($success) {
                    $reference_no = explode("-", $data['inv_no']);
                    $success = $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
                        array('typeid' => 17, 'company_id' => $this->Company_id));
                }
            }

        $items_count = $this->db->where(array('invoice_id' => $invoice_id))->get('invoice_item')->result();

        if(count($items_count)){
            $this->db->trans_complete();
        }else{
            $this->db->trans_rollback();
            $success = false;
        }

        return ($success) ? $invoice_id : null;
    }

    /**
     * get payment methods
     * @param null $invoice_id
     * @return mixed
     */
    function get_payment_methods($invoice_id = null)
    {
        $this->db->from('settings_payment_methods');

        return $this->db->get()->result();

    }

    /**
     * remove invoice
     * @param null $inv_id
     * @return bool
     */
    function remove_invoice($inv_id = null)
    {
        $success = false;

        $this->db->trans_start();
        $success = $this->db->delete('invoice_item', array('invoice_id' => $inv_id));
        if ($success) {
            $success = $this->db->delete('transaction', array('invoice_id' => $inv_id));
            if ($success) {
                $success = $this->db->delete('invoice', array('id' => $inv_id));
            }
        }
        $this->db->trans_complete();

        return $success;
    }

    /**
     * remove payment
     * @param null $payment_id
     * @return bool
     */
    function remove_payment($payment_id = null)
    {

        $success = false;

        $this->db->trans_start();

        $inv_no = $this->db->where(array('id' => $payment_id))->get('transaction')->row();

        $this->set_invoice_status($inv_no->invoice_id);
        $success = $this->db->delete('transaction', array('id' => $payment_id));

        $this->db->trans_complete();

        return $success;

    }

    /**
     * remove payment
     * @param null $payment_id
     * @return bool
     */
    function remove_pago_a($payment_id = null)
    {

        $success = false;

        $this->db->trans_start();

        $success = $this->db->delete('pago_a', array('id' => $payment_id));

        $this->db->trans_complete();

        return $success;

    }

    /**
     * add note to invoice
     * @param array $note
     * @param null $inv_no
     * @return bool
     *
     */
    function add_invoice_note($note = array(), $inv_no = null)
    {
        $success = false;
        $success = $this->db->update('invoice', $note, array('id' => $inv_no));

        return $success;

    }

    /**
     * update recur invoices
     * @param array $data
     * @param null $inv_no
     * @return bool
     */
    function update_recur($data = array(), $inv_no = null)
    {
        $success = false;
        $success = $this->db->update('invoice', $data, array('id' => $inv_no));

        return $success;
    }

    /**
     * update activity log
     * @param array $data
     * @return bool
     *
     */
    function add_log($data = array())
    {

        $success = false;
        $success = $this->db->insert("activity_log", $data);

        return $success;
    }

    /**
     * add payment to invoice
     * @param array $data
     * @return bool
     */
    function add_orden($data = array())
    {

        $success = false;
        $this->db->trans_start();

        $success = $this->db->insert('invoice_item', $data);

        $this->db->trans_complete();

        return $success;

    }

     function add_discount($inv_no, $data = array())
    {

        $success = false;
        $this->db->trans_start();

        $this->db->where('id', $inv_no);
        $success = $this->db->update('invoice', $data);

        $this->db->trans_complete();

        return $success;

    }

    function transactions_caja($project_id)
    {
        $this->db->from('transaction_caja');
        $this->db->where('project_id', $project_id);

        return $this->db->get()->result();

    }

    /**
     * delete message
     * @param null $message_id
     */
    function status_program($message_id = null, $completo = null)
    {
        $success = false;

        $success = $this->db->update('invoice', array('program' => 1), array('id' => $message_id));

        if($success){
            $success = $this->db->update('invoice_item', array('program' => 1), array('invoice_id' => $message_id));
        }

        if($success){
            $success = $this->db->update('invoice', array('paid' => 0), array('id' => $message_id));
        }

        if($success){
            $success = $this->db->update('invoice', array('trans' => 1), array('id' => $message_id));
        }

        if($success){
            $success = $this->db->update('pago_a', array('trans' => 1), array('inv_id' => $message_id));
        }

        if($success){
            $success = $this->db->update('invoice', array('deliver' => 0), array('id' => $message_id));
        }

        if($success){
            $success = $this->db->update('invoice', array('completo' => $completo), array('id' => $message_id));
        }


    }

    /**
     * delete message
     * @param null $message_id
     */
    function status_desprogram($message_id = null, $completo = null)
    {
        $success = false;

        $success = $this->db->update('invoice', array('program' => 0), array('id' => $message_id));

        if($success){
            $success = $this->db->update('invoice_item', array('program' => 0), array('invoice_id' => $message_id));
        }

        if($success){
            $success = $this->db->update('invoice', array('paid' => 1), array('id' => $message_id));
        }

        if($success){
            $success = $this->db->update('invoice', array('trans' => 0), array('id' => $message_id));
        }

        if($success){
            $success = $this->db->update('pago_a', array('trans' => 0), array('inv_id' => $message_id));
        }

        if($success){
            $success = $this->db->update('invoice', array('deliver' => 0), array('id' => $message_id));
        }

        if($success){
            $success = $this->db->update('invoice', array('completo' => $completo), array('id' => $message_id));
        }


    }

    /**
     * delete message
     * @param null $message_id
     */
    function status_stimate($message_id = null)
    {

        $this->db->update('invoice', array('for_estimate' => 1), array('id' => $message_id));

    }

    /**
     * delete message
     * @param null $message_id
     */
    function status_paid1($message_id = null)
    {

        $this->db->update('invoice', array('paid' => 1), array('id' => $message_id));

    }
     function status_paid2($message_id = null)
    {

        $query = $this->db->update('invoice', array('paid' => 0), array('id' => $message_id));

        if ($query) {
            $query = $this->db->update('invoice', array('trans' => 1), array('id' => $message_id));
        }

          if ($query) {
            $query = $this->db->update('pago_a', array('trans' => 1), array('inv_id' => $message_id));
          }

          if ($query) {
            $query = $this->db->update('invoice', array('program' => 0), array('id' => $message_id));
          }

          if ($query) {
            $query = $this->db->update('invoice_item', array('program' => 0), array('invoice_id' => $message_id));
          }
    }

     function status_paid3($message_id = null)
    {

        $this->db->update('invoice', array('paid' => 3), array('id' => $message_id));

    }

    /**
     * delete message
     * @param null $message_id
     */
    function status_deliver($message_id = null)
    {

        $this->db->update('invoice', array('deliver' => 1), array('id' => $message_id));

    }

    /**
     * delete message
     * @param null $message_id
     */
    function update_find( $find = null, $id = 1)
    {

        $this->db->update('search', array('find' => $find), array('id_find' => $id));

    }

    function get_find(){

      $this->db->from('search');

      return $this->db->get()->row();

    }

    /**
     * update estimate
     * @param null $items
     * @return bool
     */
    function insert_estimacion($items = array(), $totals = array(), $est = array())
    {

        $success = false;
        $this->db->trans_start();

        foreach ($items as $item) {
          if ($item->item_invoice != '') {
            $message_id = $item->item_invo;
            $number = $item->item_number;

            $param = array(
                'est_project' => $item->item_project,
                'est_num' => $item->item_number,
                'est_inv' => $item->item_invo,
                'est_client' => $item->item_client,
                'est_invoice' => $item->item_invoice,
                'est_nameclient' => $item->item_nameclient,
                'est_pre' => $item->item_pre,
                'est_fac' => $item->item_fac,
                'est_typre' => $item->item_typre,
                'est_concept' => $item->item_concept,
                'est_desc' => $item->item_desc,
                'est_amount' => $item->item_amount
            );

            $success = $this->db->insert('estimacion', $param);

            if ($success) {
                $success = $this->db->update('invoice', array('for_estimate' => 2, 'est_num' => $number), array('id' => $message_id));
            }

            if ($success) {
                $success = $this->db->update('pago_a', array('old_est' => 1), array('inv_id' => $message_id));
            }
          }
        }

        if ($success) {
              $success = $this->db->insert('est_amount', $totals);
        }

        if ($success) {
            $success = $this->db->insert('estimaciones', $est);
        }

        $this->db->trans_complete();

        return $success;

    }


    /**
     * update estimate
     * @param null $items
     * @return bool
     */
    function insert_pago($items = array())
    {

        $success = false;
        $this->db->trans_start();

        foreach ($items as $item) {
            $param = array(
                'pago_inv' => $item->item_id,
                'pago_invno' => 2,
                'pago_client' => $item->item_client,
                'pago_project' => $item->item_project,
                'pago_amount' => $item->item_amount,
                'date_save' => date('Y-m-d')
             );

              $success = $this->db->insert('pago',$param);
          }


        $this->db->trans_complete();

        return $success;

    }

    /**
     * get payment methods
     * @param null $invoice_id
     * @return mixed
     */
    function get_pagos($inv_id = null)
    {
        $this->db->from('pago');

        return $this->db->get()->result();

    }


    /**
     * add pago autorizado
     * @param null $param
     * @return bool
     */
    function add_pago_a($param = array(), $invo_id = null)
    {
        $success = false;
            $success = $this->db->insert('pago_a',$param);

            if ($success) {

              $this->db->update('invoice', array('pago_a' => 1), array('id' => $invo_id));
            }
        return $success;
    }

    /**
     * get pago autorizado
     * @param null $param
     * @return bool
     */
    function get_pago_a($client_id = null )
    {
      $this->db->from('pago_a');
      $this->db->where('client_id', $client_id);

      return $this->db->get()->result();
    }

    /**
     * get pago autorizado
     * @param null $param
     * @return bool
     */
    function get_pago_a_invo($inv = null )
    {
      $this->db->from('pago_a');
      $this->db->where('inv_id', $inv);

      return $this->db->get()->result();
    }

    function get_pago_all()
    {
      $this->db->from('pago_a');

      return $this->db->get()->result();
    }

    function get_pago_all_2($project_id, $client_id)
    {
      $this->db->from('pago_a');
      $this->db->where('project_id', $project_id);
      $this->db->where('client_id', $client_id);
      return $this->db->get()->result();
    }

    /**
     * get pago autorizado
     * @param null $param
     * @return bool
     */
    function get_pago_a_billing()
    {
      $this->db->from('pago_a');

      return $this->db->get()->result();
    }

    /**
     * delete message
     * @param null $message_id
     */
    function created_trans($data = array(), $folio = null, $payment = null , $inv_id = array())
    {
      $success = false;
      $this->db->trans_start();
      $success = $this->db->insert('transaction', $data);

      if ($success) {
        $invo_id = " ";
        foreach($inv_id as $inv){

          $invo_id = $inv;

          $success = $this->db->update('invoice', array('program' => 0), array('id' => $invo_id));

        }
      }

      if ($success) {
        $invo_id = " ";
        foreach($inv_id as $inv){

          $invo_id = $inv;

          $success = $this->db->update('invoice', array('trans' => 0), array('id' => $invo_id));

        }
      }

      if ($success) {
        $invo_id = " ";
        foreach($inv_id as $inv){

          $invo_id = $inv;

          $success = $this->db->update('invoice', array('paid' => $payment), array('id' => $invo_id));
        }

      }

      if ($success) {
        $invo_id = " ";
        foreach($inv_id as $inv){

          $invo_id = $inv;

          $success = $this->db->update('invoice_item', array('program' => 0), array('invoice_id' => $invo_id));
        }

      }

        if ($success) {
          $invo_id = " ";
          foreach($inv_id as $inv){

            $invo_id = $inv;

            $success = $this->db->update('pago_a', array('trans' => 0), array('inv_id' => $invo_id));

          }
        }

          if ($success) {
            $invo_id = " ";
            foreach($inv_id as $inv){

              $invo_id = $inv;

              $success = $this->db->update('pago_a', array('old' => 1), array('inv_id' => $invo_id));

            }

          }

          if ($success) {
            $invo_id = " ";
            foreach($inv_id as $inv){

              $invo_id = $inv;
              $pago = pago_a_invo($invo_id);
              $total = invoice_total($invo_id);
              if($pago == $total){
                  $success = $this->db->update('invoice', array('program_fin' => 1), array('id' => $invo_id));
              }

            }

          }

      if ($success) {

        if ($folio != 0) {
          $reference_no = explode("-", $data['folio']);
          $success = $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
              array('typeid' => 16, 'company_id' => $this->Company_id));
        }
      }


      $this->db->trans_complete();

      return $success;


    }

    function get_proveedor_list()
    {

        $this->db->from("client");
        $this->db->where('client_type !=', 1);


        return $this->db->get()->result();

    }

    function get_milestone($project_id = null){

      $this->db->from("milestones");
      $this->db->where('project_id', $project_id);

      return $this->db->get()->result();

    }
    function get_factura($project_id = null,$client_id = null){

      $this->db->from("invoice");
      $this->db->where('project_id', $project_id);
      $this->db->where('client_id', $client_id);

      return $this->db->get()->result();

    }

    function get_diafano_corriente($invoice= null){

      $this->db->from("invoice");
      $this->db->where('invoice_inv', $invoice);

      return $this->db->get()->row();

    }

    function get_milestones(){

      $this->db->from('milestones');
      return $this->db->get()->result();

    }

    function get_items_client($client_id = null){

      $this->db->from('items');
      $this->db->where('item_client', $client_id);
      return $this->db->get()->result();

    }

    public function getPersonas(){
		$this->db->select('inv_no');
		$this->db->from('invoice');
		$query = $this->db->get();

		return $query->result();
	}

  function get_est_trans($project_id)
 {

     $this->db->from("est_trans");
     $this->db->where("project_id = " . $project_id);

     return $this->db->get()->result();
 }


}
