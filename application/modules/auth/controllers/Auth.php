<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    //redirect if needed, otherwise display the user list
    function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        } else {
            redirect('dashboard');
        }
    }


    //log the user in
    function login()
    {
        $this->data['message'] = "";
        $this->data['success_message'] = "";
        $this->data['title'] = 'Iniciar Sesión';

        //validate form input
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('txtpassword', 'Password', 'trim|required|xss_clean');

        if ($this->form_validation->run() == true) { //check to see if the user is logging in
            //check for "remember me"

            $remember = (bool)$this->input->post('remember');

            if ($this->ion_auth_model->login($this->input->post('email'), $this->input->post('txtpassword'), $remember)
            ) { //if the login is successful
                redirect('dashboard', 'refresh');
            } else { //if the login was un-successful

                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->ion_auth_model->errors();
                $this->session->set_flashdata('message', $this->data['message']);
                redirect('auth/login');
            }
        } else {  //the user is not logging in so display the login page
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            if ($this->input->post()) {
                $this->data['success_message'] = $this->session->flashdata('success_message');
            }

            $this->data['email'] = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'text',
                'value' => $this->form_validation->set_value('email'),
            );

            $this->data['password'] = array(
                'name' => 'txtpassword',
                'id' => 'txtpassword',
                'type' => 'password',
            );

            $this->data['content'] = 'auth/login';
            $this->load->view('_login_layout', $this->data);

        }
    }

    //create a new user
    function register_account()
    {
        $this->data['message'] = null;
        $this->data['success_message'] = null;
        if ($this->input->post()) {
            $this->form_validation->set_message('is_natural_no_zero', 'The %s field is required.');
            //validate form input
            $this->form_validation->set_rules('company_name', 'Company Name', 'required|xss_clean');
            $this->form_validation->set_rules('company_website', 'Company Website', 'required|xss_clean');
            $this->form_validation->set_rules('company_phone', 'Phone Number',
                'required|xss_clean|min_length[8]|max_length[16]');


            $this->form_validation->set_rules('username', 'User Name', 'required|xss_clean');
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
            $this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
            $this->form_validation->set_rules('password', 'Password',
                'required|min_length[4]|max_length[20]|matches[password_confirm]');
            $this->form_validation->set_rules('password_confirm', 'Confirm Password', 'required');

            if ($this->form_validation->run() == true) {

                $user_name = strtolower($this->input->post('username'));
                $first_name = strtolower($this->input->post('first_name'));
                $last_name = strtolower($this->input->post('last_name'));
                $email = $this->input->post('email');
                $password = $this->input->post('password');

                $company_data = array(
                    'client_name' => $this->input->post('company_name'),
                    'client_website' => $this->input->post('company_website'),
                    'client_email' => $this->input->post('email'),
                    'client_address_1' => $this->input->post('address1'),
                    'client_address_2' => $this->input->post('address2'),
                    'client_country' => $this->input->post('country'),
                    'client_city' => $this->input->post('city'),
                    'client_phone_no' => $this->input->post('company_phone'),
                    'client_status' => 0,
                    'company_id' => 1
                );


                $user_id = $this->ion_auth->register($first_name, $last_name, $user_name, $password, $email,
                    $company_data);

                if ($user_id) { //check to see if we are creating the user
                    //redirect them back to the admin page
                    $this->notifications->send_client_registration_notification($user_id, $password);
                    $this->session->set_flashdata('success_message', $this->lang->line("user_added"));
                    redirect('auth/login', 'refresh');
                }
            } else { //display the create user form
                //set the flash data error message if there is one
                $this->data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));


            }

            $this->data['success_message'] = $this->session->flashdata('success_message');

        }
        if ($this->config->item('system')->client_registration == 1) {
            redirect('auth/login', 'refresh');
        } else {
            $this->data['content'] = 'auth/register';
        }
        $this->load->view('_login_layout', $this->data);
    }

    //log the user out

    function forgot_password()
    {
        $this->form_validation->set_rules('email', 'email_address', 'required');
        if ($this->form_validation->run() == false) {
            //setup the input
            $this->data['email'] = array(
                'name' => 'email',
                'id' => 'email',
            );
            //set any errors and display the form
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->data['success_message'] = $this->session->flashdata('success_message');

            $this->data['content'] = 'auth/forgot_password';
            $this->load->view('_login_layout', $this->data);

        } else {
            //run the forgotten password method to email an activation code to the user
            $forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));

            if ($forgotten) { //if there were no errors
                redirect('auth/login', 'refresh');
            } else {

                $this->session->set_flashdata('message',
                    (validation_errors() ? validation_errors() : $this->session->flashdata('message')));
                redirect('auth/forgot_password', 'refresh');
            }
        }
    }


    //forgot password

    public function reset_password($code = null)
    {
        $code = $this->input->get('code');
        if (!$code) {
            show_404();
        }

        $user = $this->ion_auth->forgotten_password_check($code);

        if ($user) {  //if the code is valid then display the password reset form

            $this->form_validation->set_rules('new_password', 'New Password',
                'required|min_length[' . $this->config->item('min_password_length',
                    'ion_auth') . ']|max_length[' . $this->config->item('max_password_length',
                    'ion_auth') . ']|matches[new_confirm]');
            $this->form_validation->set_rules('new_confirm', 'Comfirm Password', 'required');

            if ($this->form_validation->run() == false) {//display the form
                //set the flash data error message if there is one
                $user_id = ($user->user_id) ? $user->user_id : $user->cl_user_id;
                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                $this->data['success_message'] = $this->session->flashdata('success_message');
                $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                $this->data['new_password'] = array(
                    'name' => 'new_password',
                    'id' => 'new_password',
                    'type' => 'password',
                    'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                );
                $this->data['new_password_confirm'] = array(
                    'name' => 'new_confirm',
                    'id' => 'new_confirm',
                    'type' => 'password',
                    'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                );
                $this->data['user_id'] = array(
                    'name' => 'user_id',
                    'id' => 'user_id',
                    'type' => 'hidden',
                    'value' => $user_id,
                );
                $this->data['csrf'] = null;//$this->_get_csrf_nonce();
                $this->data['code'] = $code;

                //render
                $this->data['content'] = 'auth/reset_password';

            } else {
                // do we have a valid request?
                if ($user->user_id) {
                    if ($user->user_id != $this->input->post('user_id')) {

                        //something fishy might be up
                        $this->ion_auth_model->clear_forgotten_password_code($code);
                        show_404();

                    } else {
                        // finally change the password
                        $identity = $user->{$this->config->item('user_identity', 'ion_auth')};
                        $change = $this->ion_auth_model->reset_password($identity, $this->input->post('new_password'),
                            1);

                        if ($change) { //if the password was successfully changed
                            $this->session->set_flashdata('success_message', "Password Changed successfully");
                            $this->logout();
                        } else {
                            $this->session->set_flashdata('message', "Password Change failed");
                            redirect('auth/reset_password/?code=' . $code, 'refresh');
                        }
                    }
                } else {//end_user

                    if ($user->cl_user_id != $this->input->post('user_id')) {

                        //something fishy might be up

                        $this->ion_auth_model->clear_forgotten_password_code($code);
                        show_404();

                    } else {
                        // finally change the password
                        $identity = $user->{$this->config->item('client_identity', 'ion_auth')};
                        $change = $this->ion_auth_model->reset_password($identity, $this->input->post('new_password'),
                            0);

                        if ($change) { //if the password was successfully changed
                            $this->session->set_flashdata('success_message', "Password Changed successfully");
                            $this->logout();
                        } else {
                            $this->session->set_flashdata('message', "Password Changed failed");
                            redirect('auth/reset_password&code=' . $code, 'refresh');
                        }
                    }
                }
            }
        } else { //if the code is invalid then send them back to the forgot password page
            $this->session->set_flashdata('message',
                (validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
            redirect("auth/forgot_password", 'refresh');
        }


        $this->load->view('_login_layout', $this->data);
    }


    //reset password - final step for forgotten password

    function logout()
    {
        $this->ion_auth->logout();
        redirect('auth', 'refresh');
    }

    function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    function _valid_csrf_nonce()
    {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== false &&
            $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue')
        ) {
            return true;
        } else {
            return false;
        }
    }


}
