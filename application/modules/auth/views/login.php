<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/10
 * Time: 11:23 AM
 */
?>
<style>
    #intro_image .img-responsive {
        display: inline-block;
    }

    .img-responsive,
    .thumbnail > img,
    .thumbnail a > img,
    .carousel-inner > .item > img,
    .carousel-inner > .item > a > img {
        display: block;
        width: 100% \9;
        max-width: 100%;
        height: auto;
    }
</style>

<header id="header">

    <div id="logo-group">
                    <span id="logo">
                            <img alt=""
                                 src="<?php echo base_url('files/company_logo/' . $this->config->item('company')->company_logo); ?>">
                    </span>
    </div>
        <span id="login-header-space">



        </span>
</header>
<div class="container">

    <div class="row login-container column-seperation">
        <div class="col-md-5 intro_image col-md-offset-1">

            <img class="img-responsive" src="<?php echo base_url('assets/images/intro_image.png'); ?>"
                 alt="intro_image">

        </div>
        <div class="col-md-5 ">
            <h2 style="color:#fff;">Iniciar sesión en  <?php echo ucwords(strtolower($this->config->item('company')->company_name)); ?></h2>
            <br>

            <?php echo form_open('auth/login', array('class' => 'login-form', 'id' => 'login-form')); ?>
            <div class="row">

                <div class="form-group col-md-10">
                    <?php if ($message) {
                        echo "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" . $message . "</div>";
                    } ?>
                    <?php if ($success_message) {
                        echo "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $success_message . "</div>";
                    } ?>
                    <label class="form-label" style="color:#fff;"><?php echo lang('label_username'); ?></label>

                    <div class="controls">
                        <div class="input-with-icon  right" style="color:#fff;">
                            <i class="" style="color:#fff;"></i>
                            <?php echo form_input($email, '',
                                'class="form-control" placeholder="' . lang("placeholder_username") . '" autofocus="autofocus"'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-10" style="color:#fff;">
                    <label class="form-label" style="color:#fff;"><?php echo lang('label_password'); ?></label>
                    <span class="help" style="color:#fff;"></span>

                    <div class="controls">
                        <div class="input-with-icon  right">
                            <i class="" style="color:#fff;"></i>

                            <div class="controls" style="color:#fff;"> <?php echo form_input($password, '',
                                    'class="form-control" placeholder="' . lang("placeholder_password") . '"'); ?> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="control-group  col-md-10">
                    <div class="checkbox checkbox check-success"style="color:#fff;">
                        <label for="checkbox1"><input type="checkbox" id="checkbox1" value="1">
<?php echo lang('label_remember_password'); ?> </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a
                                href="<?php echo base_url('auth/forgot_password'); ?>" style="color:#fff;">¿Contraseña olvidada?</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10" style="color:#fff;">
                    <?php echo form_submit('submit', lang('form_button_login'),'class="btn btn-primary btn-cons pull-right" style="background:#484848;border-color:#828282;"'); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
