<header id="header">
    <div id="logo-group">
                    <span id="logo">
                            <img alt="" src="<?php echo base_url('files/company_logo/company_logo.png'); ?>">
                    </span>
    </div>
        <span id="login-header-space">

                <span id="login-header-space"> <span
                        class="hidden-mobile"> <?php echo lang('messages_login_need_account'); ?></span>
                    <?php echo anchor('auth/register_account', lang('form_button_create_account'),
                        array('class' => 'btn btn-primary')); ?>
          </span>

        </span>
</header>
<div class="container">

    <div class="row login-container">
        <h2>Olvido Contraseña</h2>
        <?php $attib = array('class' => 'login-form');
        echo form_open("auth/forgot_password", $attib); ?>

        <div
            style="padding-left:10%;padding-right:10%; background:#FFF; border-left: 1px solid #D4D4D4; border-right: 1px solid #D4D4D4;">
            <?php if ($message) {
                echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $message . "</div>";
            } ?>
            <?php if ($success_message) {
                echo "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $success_message . "</div>";
            } ?>
            <div class="row">

                <div class="form-group col-md-10">
                    <label class="form-label"><?php echo lang('label_email'); ?></label>

                    <div class="controls">
                        <div class="input-with-icon  right">
                            <i class=""></i>
                            <?php echo form_input($email, '',
                                'class="form-control" placeholder="' . lang("placeholder_username") . '" autofocus="autofocus"'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <?php echo form_submit('submit', lang('form_button_login'),
                        'class="btn btn-primary btn-cons pull-right"'); ?>
                    <a href="<?php echo base_url(); ?>">Regresar</a></div>
                </div>
            </div>
            <br/><br/>
        </div>

        <?php echo form_close(); ?>

    </div>
</div>
