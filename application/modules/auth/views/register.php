<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/05/22
 * Time: 02:09 PM
 */
?>

<header id="header">

    <div id="logo-group">
        <span id="logo"> <img
                src="<?php echo base_url('files/company_logo/' . $this->config->item('company')->company_logo); ?>"
                alt="Zestpro"> </span>
    </div>
      <span id="login-header-space"> <span
              class="hidden-mobile"> <?php echo lang('messages_login_have_account'); ?></span>
          <?php echo anchor('auth/login', lang('form_button_back_login'),
              array('class' => 'btn btn-primary CreateButton')); ?>
    </span>
</header>

<div id="main" role="main">

    <!-- MAIN CONTENT -->
    <div id="content" class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"
                 style="float: none;margin-left: auto;margin-right: auto;">
                <div class="well no-padding">
                    <?php echo form_open('auth/register_account',
                        array('class' => 'smart-form client-form', 'id' => 'registration-form')); ?>
                    <header>
                        <?php echo ucwords(strtolower($this->config->item('company')->company_name)); ?> Registration
                    </header>
                    <?php if ($message) {
                        echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $message . "</div>";
                    } ?>
                    <fieldset>
                        <section>
                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                <input type="text" name="company_name" placeholder="Company Name">
                                <b class="tooltip tooltip-bottom-right">Needed to enter company</b> </label>
                        </section>

                        <section>
                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                <input type="text" name="company_phone" placeholder="Phone Number">
                                <b class="tooltip tooltip-bottom-right">Needed to enter phone number</b> </label>
                        </section>
                        <section>
                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                <input type="text" name="company_website" placeholder="Website">
                                <b class="tooltip tooltip-bottom-right">Needed to enter the website</b> </label>
                        </section>
                        <section>
                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                <input type="text" name="address1" placeholder="Address">
                                <b class="tooltip tooltip-bottom-right">Needed to enter the address</b> </label>
                        </section>
                        <section>
                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                <input type="text" name="address2" placeholder="Address">
                                <b class="tooltip tooltip-bottom-right">Needed to enter the address</b> </label>
                        </section>
                        <section>
                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                <input type="text" name="country" placeholder="Country">
                                <b class="tooltip tooltip-bottom-right">Needed to enter country</b> </label>
                        </section>
                        <section>
                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                <input type="text" name="city" placeholder="City">
                                <b class="tooltip tooltip-bottom-right">Needed to enter city</b> </label>
                        </section>
                        <section>
                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                <input type="text" name="username" placeholder="Username">
                                <b class="tooltip tooltip-bottom-right">Needed to enter username</b> </label>
                        </section>

                        <section>
                            <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                <input type="email" name="email" placeholder="Email address">
                                <b class="tooltip tooltip-bottom-right">Needed to verify your account</b> </label>
                        </section>

                    </fieldset>

                    <fieldset>
                        <div class="row">
                            <section class="col col-6">
                                <label class="input">
                                    <input type="text" name="first_name" placeholder="First name">
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="input">
                                    <input type="text" name="last_name" placeholder="Last name">
                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-6">
                                <label class="input"> <i class="icon-append fa fa-lock"></i>
                                    <input type="password" name="password" placeholder="Password">
                                    <b class="tooltip tooltip-bottom-right">Don't forget your password</b>
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="input"> <i class="icon-append fa fa-lock"></i>
                                    <input type="password" name="password_confirm" placeholder="Comfirm password">
                                    <b class="tooltip tooltip-bottom-right">Don't forget your password</b>
                                </label>
                            </section>
                        </div>

                    </fieldset>
                    <footer>
                        <?php echo form_submit('submit', lang('form_button_register'),
                            'class="btn btn-primary bzgreen"'); ?>

                    </footer>

                    <div class="message">
                        <i class="fa fa-check"></i>

                        <p>
                            Thank you for your registration!
                        </p>
                    </div>
                    </form>

                </div>

            </div>
        </div>
    </div>

</div>


<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)
<script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script>-->

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script> if (!window.jQuery) {
        document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');
    } </script>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script> if (!window.jQuery.ui) {
        document.write('<script src="js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    } </script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

<!-- BOOTSTRAP JS -->

<!--[if IE 7]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- MAIN APP JS FILE -->


<script type="text/javascript">


    // Model i agree button
    $("#i-agree").click(function () {
        $this = $("#terms");
        if ($this.checked) {
            $('#myModal').modal('toggle');
        } else {
            $this.prop('checked', true);
            $('#myModal').modal('toggle');
        }
    });

    // Validation
    $(function () {
        // Validation
        $("#bz-form-register").validate({

            // Rules for form validation
            rules: {
                username: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 3,
                    maxlength: 20
                },
                passwordConfirm: {
                    required: true,
                    minlength: 3,
                    maxlength: 20,
                    equalTo: '#password'
                },
                firstname: {
                    required: true
                },
                lastname: {
                    required: true
                },
                gender: {
                    required: true
                },
                terms: {
                    required: true
                }
            },

            // Messages for form validation
            messages: {
                login: {
                    required: 'Please enter your login'
                },
                email: {
                    required: 'Please enter your email address',
                    email: 'Please enter a VALID email address'
                },
                password: {
                    required: 'Please enter your password'
                },
                passwordConfirm: {
                    required: 'Please enter your password one more time',
                    equalTo: 'Please enter the same password as above'
                },
                firstname: {
                    required: 'Please select your first name'
                },
                lastname: {
                    required: 'Please select your last name'
                },
                gender: {
                    required: 'Please select your gender'
                },
                terms: {
                    required: 'You must agree with Terms and Conditions'
                }
            },

            // Ajax form submition
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function () {
                        $("#bz-form-register").addClass('submited');
                    }
                });
            },

            // Do not change code below
            errorPlacement: function (error, element) {
                error.insertAfter(element.parent());
            }
        });

    });
</script>