<header id="header">
    <div id="logo-group">
                    <span id="logo">
                            <img alt="" src="<?php echo base_url('files/company_logo/company_logo.png'); ?>">
                    </span>
    </div>
        <span id="login-header-space">

                <span id="login-header-space"> <span
                        class="hidden-mobile"> <?php echo lang('messages_login_need_account'); ?></span>
                    <?php echo anchor('auth/register_account', lang('form_button_create_account'),
                        array('class' => 'btn btn-primary')); ?>
          </span>

        </span>
</header>
<div class="container">

    <div class="row login-container">
        <h2>Reset Password</h2>
        <?php $attib = array('class' => 'login-form');
        echo form_open("auth/reset_password/?code=" . $code, $attib); ?>

        <div
            style="padding-left:10%;padding-right:10%;  border-left: 1px solid #D4D4D4; border-right: 1px solid #D4D4D4;">
            <?php if ($message) {
                echo "<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $message . "</div>";
            } ?>
            <?php if ($success_message) {
                echo "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $success_message . "</div>";
            } ?>
            <p>Please type new password. (at least <?php echo $min_password_length; ?> characters long)</p>

            <div class="row">
                <div class="form-group col-md-10">
                    <label class="form-label">New Password</label>
                    <span class="help"></span>

                    <div class="controls">
                        <div class="input-with-icon  right">
                            <i class=""></i>

                            <div class="controls"> <?php echo form_input($new_password, '',
                                    'class="form-control input-block-level" placeholder="New Password" autofocus="autofocus"'); ?> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-10">
                    <label class="form-label">New Password</label>
                    <span class="help"></span>

                    <div class="controls">
                        <div class="input-with-icon  right">
                            <i class=""></i>

                            <div class="controls"> <?php echo form_input($new_password_confirm, '',
                                    'class="form-control input-block-level" placeholder="Confirm New Password"'); ?> </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php echo form_input($user_id); ?>
            <?php echo form_hidden($csrf); ?>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="control-group" style="margin-bottom: 0;">
                    <div class="controls"> <?php echo form_submit('submit', 'Reset',
                            'class="btn btn-primary btn-cons pull-right"'); ?>
                        <a href="<?php echo base_url(); ?>">Back</a></div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>

    </div>
</div>
