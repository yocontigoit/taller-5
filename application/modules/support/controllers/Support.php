<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/04/02
 * Time: 08:44 AM
 */
class Support extends Admin_Controller
{


    function  __construct()
    {

        parent::__construct();
        $this->load->model("support_m");
    }

    /*
     * show list of all
     */
    function  index()
    {

        $this->data['content'] = 'support/support';
        $this->data['title'] = 'Support | Ticket';

        $support_page = $this->input->get('page', true) ? $this->input->get('page', true) : 'overview';
        $this->data['active'] = $support_page;

        switch ($support_page) {
            case 'open':
                if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) {
                    $this->data['tickets'] = $this->support_m->open_tickets();
                } else {
                    $this->data['tickets'] = $this->support_m->client_open_tickets($this->CompanyClient_id);
                }
                $this->data['_PAGE'] = $this->load->view('pages/' . $support_page, $this->data, true);
                break;
            case 'closed':
                if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) {
                    $this->data['tickets'] = $this->support_m->closed_tickets();
                } else {
                    $this->data['tickets'] = $this->support_m->client_closed_tickets($this->CompanyClient_id);
                }
                $this->data['_PAGE'] = $this->load->view('pages/' . $support_page, $this->data, true);
                break;
            case 'progress':
                if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) {
                    $this->data['tickets'] = $this->support_m->in_progress_tickets();
                } else {
                    $this->data['tickets'] = $this->support_m->client_in_progress_tickets($this->CompanyClient_id);
                }
                $this->data['_PAGE'] = $this->load->view('pages/' . $support_page, $this->data, true);

                break;
            case 'answered':
                if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) {
                    $this->data['tickets'] = $this->support_m->answered_tickets();
                } else {
                    $this->data['tickets'] = $this->support_m->client_answered_tickets($this->CompanyClient_id);
                }
                $this->data['_PAGE'] = $this->load->view('pages/' . $support_page, $this->data, true);
                break;
            default:
                if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) {
                    $this->data['open_tickets_count'] = $this->support_m->count_open_tickets();
                    $this->data['user_replies_count'] = $this->support_m->count_user_replies();
                    $this->data['client_replies_count'] = $this->support_m->count_client_replies();
                } else {
                    $this->data['open_tickets_count'] = $this->support_m->count_open_tickets();
                    $this->data['user_replies_count'] = $this->support_m->count_user_replies();
                    $this->data['client_replies_count'] = $this->support_m->count_client_replies();
                }
                $this->data['_PAGE'] = $this->load->view('pages/' . $support_page, $this->data, true);
                break;

        }
        $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * Show selected ticket plus replies..
     * @param null $ticket_id
     */
    function view($ticket_id = null)
    {
        //can view?
        $this->data['title'] = 'Support | Ticket';
        $this->data['content'] = 'pages/ticket_details';

        if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) {

            $this->data['ticket_details'] = $this->support_m->get_ticket_details($ticket_id);
            $this->data['ticket_replies'] = $this->support_m->get_ticket_messages($ticket_id);
        } else {

            $this->data['ticket_details'] = $this->support_m->get_ticket_details($ticket_id, $this->CompanyClient_id);
            $this->data['ticket_replies'] = $this->support_m->get_ticket_messages($this->data['ticket_details']->ticket_id);
        }
        $this->data['message'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * add new ticket
     */
    function  add()
    {

        if ($this->input->post()) {
			$success = false;
            $ticket_user_id = $this->session->userdata('username');
            $arr = $this->input->post('assign_to[]');
            $emp_name = "";
            foreach($arr as $emp)
            {
                $emp_name .= $emp . " ";
            }
            $ticket_data = array(
                "com_description" => $this->input->post('com_description'),
                "com_color" => $this->input->post('com_color'),
                "com_user" => $ticket_user_id,
                "com_colortext" => "fff",
                "com_obra" => $this->input->post('project'),
				"com_name" => $emp_name
            );


            $ticket_id = $this->support_m->add_new_ticket($ticket_data);
            redirect('/dashboard', 'refresh');

        } else {
            if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) {
                $this->data['content'] = 'support/admin_add';
                $this->data['clients'] = $this->support_m->get_company_clients($this->Company_id);
                $this->data['users'] = $this->support_m->get_company_users($this->Company_id);
                $this->data['status'] = $this->support_m->get_ticket_status();
            } else {
                $this->data['content'] = 'support/client_add';
            }
            $this->data['projects'] = $this->support_m->get_projects();

            $this->data['title'] = 'Nuevo Comunicado';
            $this->data['tickets'] = null;
            $this->data['message'] = true;


            $this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);
        }
    }

    /**
     * add new ticket
     */
    function  add_pending()
    {

        if ($this->input->post()) {
			$success = false;
            $ticket_user_id = $this->session->userdata('username');
            $arr = $this->input->post('assign_to[]');
            $emp_name = "";
            foreach($arr as $emp)
            {
                $emp_name .= $emp . " ";
            }
            $ticket_data = array(
                "com_date" => date("m.d.y"),   
                "com_description" => $this->input->post('com_description'),
                "com_color" => $this->input->post('com_color'),
                "com_user" => $ticket_user_id,
                "com_colortext" => "fff",
                "com_obra" => $this->input->post('project'),
				        "com_name" => $emp_name
            );


            $ticket_id = $this->support_m->add_new_pendient($ticket_data);
            redirect('/dashboard', 'refresh');

        } else {
            if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) {
                $this->data['content'] = 'support/add_pending';
                $this->data['clients'] = $this->support_m->get_company_clients($this->Company_id);
                $this->data['users'] = $this->support_m->get_company_users($this->Company_id);
                $this->data['status'] = $this->support_m->get_ticket_status();
            } else {
                $this->data['content'] = 'support/add_pending';
            }
            $this->data['projects'] = $this->support_m->get_projects();

            $this->data['title'] = 'Nuevo Pendiente';
            $this->data['tickets'] = null;
            $this->data['message'] = true;


            $this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);
        }
    }

    //add ticket reply

    function _upload_ticket_document($image_file = null)
    {

        $file_name = null;
        $success = false;
        if ($image_file) {
            $config['upload_path'] = './files/support/';
            $config['allowed_types'] = $this->config->item('system')->allowed_files;
            $config['remove_spaces'] = true;
            $config['overwrite'] = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('ticket_attachment')) {
                $data = $this->upload->data();
                $file_name = $data['file_name'];
                $success = true;
            } else {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', lang('messages_ticket_add_error'));
                redirect('support');
            }
        } else {
            $success = false;
        }

        return ($success) ? $file_name : '';
    }

    //update ticket status

    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->support_m->add_log($activity_data);

    }


    //assign ticket to a user

    function reply_ticket()
    {

        if ($this->input->post()) {
            $ticket_id = intval($this->input->post('ticket_id'));
            if ($this->input->post('reply_message')) {
                $reply_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;

                $reply_data = array(
                    "ticket_id" => $ticket_id,
                    "reply_date" => date('Y-m-d,H:i:s'),
                    "reply_message" => $this->input->post('reply_message'),
                    "reply_user_id" => $reply_user,
                    "reply_user_type" => $this->CompanyUser_type

                );

                if ($this->support_m->add_new_reply($reply_data, $ticket_id)) {
                    $log_params = array(
                        'activity_user' => $reply_user,
                        'activity_user_type' => $this->CompanyUser_type,
                        'activity_module' => 'tickets',
                        'activity_details' => "replied to ticket",
                        'activity_module_id' => $ticket_id,
                        'activity_status' => "success",
                        'activity_event' => "Replied to ticket" . $ticket_id,
                        'activity_icon' => 'fa fa-user'
                    );

                    $this->__activity_tracker($log_params);
                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_ticket_reply_success'));
                    redirect('support/view/' . $ticket_id);
                }
            } else {

                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', lang('messages_ticket_reply_error'));
                redirect('support/view/' . $ticket_id);
            }

        }

    }

    //attach document to a ticket

    function update_status($ticket_id = null, $status_id = null)
    {

        if ($this->support_m->update_ticket_status($ticket_id, $status_id)) {

            $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $log_params = array(
                'activity_user' => $activity_user,
                'activity_user_type' => $this->CompanyUser_type,
                'activity_module' => 'support',
                'activity_details' => "ticket update",
                'activity_module_id' => $ticket_id,
                'activity_status' => "success",
                'activity_date' => date('Y-m-d,H:i:s'),
                'activity_event' => "Ticket status updated successfully",
                'activity_icon' => 'fa fa-clipboard'
            );

            $this->__activity_tracker($log_params);
            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', lang('messages_ticket_save_success'));
            redirect('support/view/' . $ticket_id);

        }
    }

    //log activities

    function assign($ticket_id = null)
    {

        if ($this->input->post()) {

            $ticket_id = intval($this->input->post('ticket_id'));
            $assigned_to = $this->input->post('ticket_assign');
            $param = array(
                "ticket_assigned_to" => $assigned_to

            );
            if ($this->support_m->update_assignment($param, $ticket_id)) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'support',
                    'activity_details' => "ticket assigned",
                    'activity_module_id' => $ticket_id,
                    'activity_status' => "success",
                    'activity_event' => "Ticket assigned successfully",
                    'activity_icon' => 'fa fa-bug'
                );

                $this->__activity_tracker($log_params);
                $this->notifications->send_ticket_assigned_notification($ticket_id, $assigned_to,
                    $this->CompanyUser_FullName);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_ticket_save_success'));
                redirect('support/view/' . $ticket_id);
            }
        } else {


            $this->data['ticket_id'] = $ticket_id;
            $this->data['users'] = $this->support_m->get_company_users($this->Company_id);
            $this->data['ticket_details'] = $this->support_m->get_ticket_details($ticket_id);
            $this->load->view('modal/assign', $this->data);
        }

    }

}
