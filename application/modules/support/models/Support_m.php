<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/24
 * Time: 03:56 PM
 */
class Support_M extends CI_Model
{


    /**
     * add ticket
     * @param array $data
     * @return null
     */
    function add_new_ticket($data = array())
    {
        $success = false;
        $success = $this->db->insert('comunicados', $data);

    }

    /**
     * add ticket
     * @param array $data
     * @return null
     */
    function add_new_pendient($data = array())
    {
        $success = false;
        $success = $this->db->insert('pendientes', $data);

    }

    /**
     * add ticket reply
     * @param array $data
     * @return bool
     */
    function add_new_reply($data = array())
    {

        $user_type = 0;
        $success = false;
        $success = $this->db->insert('ticket_replies', $data);
        $ticket_id = intval($data['ticket_id']);
        $user_type = intval($data['reply_user_type']);

        if ($user_type == 1 && $success) {

            $success = $this->db->where('ticket_id', $ticket_id)->update('ticket', array('ticket_status' => 3));
        }

        return $success;
    }

    function get_projects()
    {
        $this->db->from('projects');
    return $this->db->get()->result();
    }
    /**
     * retrieve ticket information
     * @param null $ticket_id
     * @param null $client_id
     * @return mixed
     */
    function get_ticket_details($ticket_id = null, $client_id = null)
    {

        $id = intval($ticket_id);
        $this->db->from('ticket');
        if ($client_id) {
            $this->db->where("ticket_client_id", $client_id);
        }
        $this->db->where("ticket_id", $id);

        return $this->db->get()->row();

    }

    /**
     * count all open tickets
     * @return mixed
     */
    function count_open_tickets()
    {
        return $this->db->count_all('ticket', array('ticket_status' => 1));
    }

    function count_user_replies()
    {
        return $this->db->count_all('ticket_replies', array('reply_user_type' => 1));
    }

    function count_client_replies()
    {
        return $this->db->count_all('ticket_replies', array('reply_user_type' => 0));
    }


    function get_ticket_status()
    {

        return $this->db->get('ticket_status')->result();
    }

    function get_company_clients($id = null)
    {

        $company_id = intval($id);
        $this->db->from('client');
        $this->db->where("company_id", $company_id);

        return $this->db->get()->result();

    }

    function get_company_users($id = null)
    {

        $company_id = intval($id);
        $this->db->from('users');
        $this->db->join('employee', 'employee.emp_id  = users.user_id');
        $this->db->where("company_id", $company_id);

        return $this->db->get()->result();

    }

    function get_ticket_messages($ticket_id = null)
    {

        $id = intval($ticket_id);
        $this->db->from('ticket_replies');
        $this->db->where("ticket_id", $id);

        return $this->db->get()->result();
    }


    function update_ticket_status($ticket_id = null, $status_id = null)
    {
        $success = false;
        $success = $this->db->where('ticket_id', $ticket_id)->update('ticket', array('ticket_status' => $status_id));

        return $success;
    }

    function  open_tickets()
    {

        $this->db->from('ticket');
        $this->db->where("ticket_status = 1");

        return $this->db->get()->result();
    }

    function  closed_tickets()
    {

        $this->db->from('ticket');
        $this->db->where("ticket_status =4");

        return $this->db->get()->result();
    }

    function  answered_tickets()
    {

        $this->db->from('ticket');
        $this->db->where("ticket_status =3");

        return $this->db->get()->result();
    }

    function  in_progress_tickets()
    {
        $this->db->from('ticket');
        $this->db->where("ticket_status = 2");

        return $this->db->get()->result();
    }

    /****************************************************
     * 1open
     * 2in progress
     * 3answered
     * 4closed
     **/
    function  client_open_tickets($client_id = null)
    {

        $this->db->from('ticket');
        $this->db->where("ticket_status =  1");
        $this->db->where("ticket_client_id", $client_id);

        return $this->db->get()->result();
    }

    function  client_closed_tickets($client_id = null)
    {

        $this->db->from('ticket');
        $this->db->where("ticket_status");
        $this->db->where("ticket_client_id", $client_id);

        return $this->db->get()->result();
    }

    function  client_answered_tickets($client_id = null)
    {

        $this->db->from('ticket');
        $this->db->where("ticket_status");
        $this->db->where("ticket_client_id", $client_id);

        return $this->db->get()->result();
    }

    function  client_in_progress_tickets($client_id = null)
    {

        $this->db->from('ticket');
        $this->db->where("ticket_status =2");
        $this->db->where("ticket_client_id", $client_id);

        return $this->db->get()->result();
    }

    function add_log($data = array())
    {

        $success = false;
        $success = $this->db->insert("activity_log", $data);

        return $success;
    }


    function update_assignment($data = array(), $ticket_id = null)
    {

        $success = false;
        $success = $this->db->where('ticket_id', $ticket_id)->update('ticket', $data);

        return $success;
    }
}
