<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/24
 * Time: 10:46 AM
 */
?>
<div class="the-box">
    <div class="row">
        <div class="col-md-12">
            <fieldset>
                <legend><i class="fa fa-support"></i><?php echo lang('label_tickets'); ?>

                    <div class="pull-right">
                        <div class="btn-group btn-group-sm" role="group">
                            <a class="btn btn-danger" href="<?php echo base_url('support/add'); ?>">
                                <i class="fa fa-plus"></i>
                                <?php echo lang('form_button_add_ticket'); ?>
                            </a>
                        </div>
                    </div>
                </legend>
                <div class="col-md-3 pull-md-left sidebar">
                    <div class="panel panel-success panel-actions ">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="fa fa-filter"></i>&nbsp; View
                            </h3>
                        </div>
                        <div class="list-group">
                            <a href="<?php echo base_url('support/?page=open'); ?>"
                               class="list-group-item <?php echo ($active == 'open') ? 'active' : ''; ?>">
                                <i class="fa fa<?php echo ($active == 'open') ? '-dot' : ''; ?>-circle-o"></i>&nbsp;
                                <span><?php echo lang('label_open'); ?></span>
                                &nbsp; </a>
                            <a href="<?php echo base_url('support/?page=answered'); ?>"
                               class="list-group-item <?php echo ($active == 'answered') ? 'active' : ''; ?>">
                                <i class="fa fa<?php echo ($active == 'answered') ? '-dot' : ''; ?>-circle-o"></i>&nbsp;
                                <span><?php echo lang('label_answered'); ?></span>
                                &nbsp; </a>

                            <a href="<?php echo base_url('support/?page=progress'); ?>"
                               class="list-group-item <?php echo ($active == 'progress') ? 'active' : ''; ?>">
                                <i class="fa fa<?php echo ($active == 'progress') ? '-dot' : ''; ?>-circle-o"></i>&nbsp;
                                <span><?php echo lang('label_progress'); ?></span>
                                &nbsp; </a>
                            <a href="<?php echo base_url('support/?page=closed'); ?>"
                               class="list-group-item <?php echo ($active == 'closed') ? 'active' : ''; ?>">
                                <i class="fa fa<?php echo ($active == 'closed') ? '-dot' : ''; ?>-circle-o"></i>&nbsp;
                                <span><?php echo lang('label_closed'); ?></span>
                                &nbsp; </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <?php echo ($_PAGE) ? $_PAGE : null; ?>
                </div>
            </fieldset>
        </div>
    </div>
</div>
