<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2/16/2015
 * Time: 11:18 PM
 */
?>
<fieldset>
    <legend><i class="fa fa-support"></i><?php echo lang('label_open_tickets'); ?>
    </legend>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
               id="zest_table">
            <thead class="the-box dark full">

            <tr>
                <th>
                    <?php echo lang("label_support_ticket_department"); ?>
                </th>
                <th>
                    <?php echo lang("label_support_ticket_subject"); ?>
                </th>
                <th>
                    <?php echo lang("label_support_ticket_reporter"); ?>
                </th>
                <th>
                    <?php echo lang("label_support_ticket_priority"); ?>
                </th>
                <th>
                    <?php echo lang("label_support_ticket_assigned_to"); ?>
                </th>
                <th>
                    <?php echo lang("common_status"); ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php if (count($tickets)): foreach ($tickets as $ticket): ?>
                <tr class="tr-link" data-link="<?php echo base_url("support/view/" . $ticket->ticket_id); ?>">
                    <td>
                        <?php echo get_department($ticket->ticket_department); ?>
                    </td>
                    <td>
                        <?php echo $ticket->ticket_subject; ?>
                    </td>
                    <td>
                        <?php
                        $user_type = ($ticket->ticket_user_type) ? 1 : 0;
                        $user_details = users_details($ticket->ticket_by, $user_type);

                        ?>
                        <div class="media-left">
                            <a class="activity-avatar avatar-xs" data-toggle="tooltip"
                               title="<?php echo $user_details->full_name; ?>" data-placement="right">
                                <img src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>">
                            </a>
                        </div>

                    </td>
                    <td>

                        <?php $priority = get_priority($ticket->ticket_priority);

                        if (strtolower($priority) == 'low') {
                            $label = 'info';
                        } elseif (strtolower($priority) == 'medium') {
                            $label = 'warning';
                        } else {
                            $label = 'danger';
                        }

                        ?>
                        <span class="label label-<?php echo $label; ?>"><?php echo $priority; ?></span>
                    </td>
                    <td>
                        <?php echo users_details($ticket->ticket_assigned_to, 1)->full_name; ?>
                    </td>

                    <td>
                        <?php $status = get_ticket_status($ticket->ticket_status);

                        if ($status == 'open') {
                            $label = 'info';
                        } elseif ($status == 'closed') {
                            $label = 'success';
                        } elseif ($status == 'answered') {
                            $label = 'warning';
                        } else {
                            $label = 'danger';
                        }

                        ?>
                        <span class="label label-<?php echo $label; ?>"><?php echo $status; ?></span>

                    </td>
                </tr>
            <?php endforeach;endif ?>


            </tbody>
        </table>
    </div>
</fieldset>
