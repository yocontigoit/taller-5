<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/16
 * Time: 04:18 PM
 */
?>

<fieldset>
    <legend><i class="fa fa-support"></i>My tickets
    </legend>
    <div class="table-toolbar">
        <div class="btn-group nav-tabs hidden-xs">

            <a class="btn btn-primary backlink" id="back" href="http://demo.freelancecockpit.com/tickets">Back</a>
            <a class="btn btn-primary" id="note" data-toggle="mainmodal"
               href="http://demo.freelancecockpit.com/tickets/article/7/add">Add Note</a>
            <a class="btn btn-primary" id="queue" data-toggle="mainmodal"
               href="http://demo.freelancecockpit.com/tickets/queue/7">Queue</a>
            <a class="btn btn-primary" id="type" data-toggle="mainmodal"
               href="http://demo.freelancecockpit.com/tickets/type/7">Type</a>
            <a class="btn btn-primary" id="client" data-toggle="mainmodal"
               href="http://demo.freelancecockpit.com/tickets/client/7">Client</a>
            <a class="btn btn-primary" id="assign" data-toggle="mainmodal"
               href="http://demo.freelancecockpit.com/tickets/assign/7">Assign</a>
            <a class="btn btn-primary" id="status" data-toggle="mainmodal"
               href="http://demo.freelancecockpit.com/tickets/status/7">Status</a>
            <a class="btn btn-primary" id="close" data-toggle="mainmodal"
               href="http://demo.freelancecockpit.com/tickets/close/7">Close</a>

        </div>
        <div class="btn-group">

            <?php echo anchor('support/add', lang('form_button_add_ticket') . ' <i class="fa fa-plus"></i>',
                array('class' => 'btn btn-danger')); ?>

        </div>

    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
               id="sample_2">
            <thead class="the-box dark full">
            <!-- <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success" id="userstable">
                 <thead>-->
            <tr>
                <th>
                    <?php echo lang("label_support_ticket_no"); ?>
                </th>
                <th>
                    <?php echo lang("label_support_ticket_priority"); ?>
                </th>
                <th class="hidden-xs">
                    <?php echo lang("label_support_ticket_department"); ?>
                </th>
                <th>
                    <?php echo lang("label_support_ticket_assigned_to"); ?>
                </th>
                <th>
                    <?php echo lang("common_status"); ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php if (count($tickets)): foreach ($tickets as $ticket): ?>
                <tr class="tr-link" data-link="<?php echo base_url("user/edit/" . ($user->user_id * 99)); ?>">
                    <td>
                        <?php echo $ticket->user_id; ?>
                    </td>
                    <td>
                        <?php echo $ticket->emp_name; ?>
                    </td>
                    <td class="hidden-xs">
                        <?php echo $ticket->emp_surname; ?>
                    </td>
                    <td>
                        <?php echo $ticket->email; ?>
                    </td>
                    <td>
                        <?php echo $ticket->active; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="6"><?php echo lang("messages_no_ticket_found"); ?></td>
                </tr>
            <?php endif; ?>

            </tbody>
        </table>
    </div>
</fieldset>