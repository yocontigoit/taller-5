<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/16
 * Time: 04:18 PM
 */
?>

<div class="row">
    <div class="col-lg-4">

        <!-- BEGIN OPEN TICKETS -->
        <div class="panel panel-danger panel-square panel-no-border text-center">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo lang('label_open_tickets'); ?></h3>
            </div>
            <div class="panel-body">
                <h1 class="bolded tiles-number text-danger"><?php echo number_format($open_tickets_count); ?></h1>

            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel panel-success panel-block-color -->
        <!-- END OPEN TICKETS -->

    </div>
    <!-- /.col-sm-6 -->
    <div class="col-lg-4">

        <!-- BEGIN CLIENT REPLIES -->
        <div class="panel panel-warning panel-square panel-no-border text-center">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo lang('label_client_replies'); ?></h3>
            </div>
            <div class="panel-body">
                <h1 class="bolded tiles-number text-warning"><?php echo number_format($client_replies_count); ?></h1>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel panel-success panel-block-color -->
        <!-- END CLIENT REPLIES -->

    </div>
    <!-- /.col-sm-6 -->
    <div class="col-lg-4">

        <!-- BEGIN USER REPLIES -->
        <div class="panel panel-info panel-square panel-no-border text-center">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo lang('label_user_replies'); ?></h3>
            </div>
            <div class="panel-body">
                <h1 class="bolded tiles-number text-warning"><?php echo number_format($user_replies_count); ?></h1>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel panel-success panel-block-color -->
        <!-- END USER REPLIES -->

    </div>
    <!-- /.col-sm-6 -->

</div>