<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/17
 * Time: 08:44 AM
 */
?>
<div class="the-box">
    <div class="row">
        <div class="col-lg-12">
            <fieldset>
                <legend><i class="fa fa-support"></i>Details
                    <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) { ?>


                        <span class="btn-group pull-right">

                          <button type="button" class="btn btn-danger btn-sm dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-tag"></i>
                              Change Status
                              <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu">

                              <?php
                              $ticket_status = ticket_status();
                              if (count($ticket_status)):foreach ($ticket_status as $status): ?>

                                  <li>
                                      <a href="<?php echo base_url('support/update_status/' . $ticket_details->ticket_id . '/' . $status->status_id); ?>"><?php echo ucfirst(strtolower($status->status_name)); ?></a>
                                  </li>
                              <?php endforeach; endif ?>
                          </ul>


                    </span>
                        <div class="btn-group  nav-tabs hidden-xs pull-right">

                            <a class="btn btn-info btn-sm" data-hover="tooltip" title="Assign ticket"
                               data-placement="right" data-toggle="zestModal"
                               href="<?php echo base_url('support/assign/' . $ticket_details->ticket_id); ?>">Assign</a>

                        </div>
                    <?php } ?>
                </legend>
                <div class="col-lg-3">
                    <!--Ticket details -->
                    <div class="box">
                        <!-- BEGIN PANEL-->
                        <div class="panel panel-danger panel-square panel-no-border text-center">
                            <div class="panel-heading">
                                <h3 class="panel-title">Ticket Details</h3>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered table-hover table-extra-padd">
                                    <tbody>
                                    <tr>
                                        <td class="col-sm-4"><?php echo lang('label_support_ticket_no'); ?></td>
                                        <td class="col-sm-8"><?php echo $ticket_details->ticket_id; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="col-sm-4"><?php echo lang('label_date'); ?>:</td>
                                        <td class="col-sm-8"><?php echo $ticket_details->ticket_date; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="col-sm-4"><?php echo lang('label_departments'); ?>:</td>
                                        <td class="col-sm-8"><?php echo get_department($ticket_details->ticket_department); ?></td>
                                    </tr>
                                    <tr>
                                        <td class="col-sm-4"><?php echo lang('label_status'); ?>:</td>

                                        <?php $status = get_ticket_status($ticket_details->ticket_status);

                                        if ($status == 'open') {
                                            $label = 'info';
                                        } elseif ($status == 'closed') {
                                            $label = 'success';
                                        } elseif ($status == 'answered') {
                                            $label = 'warning';
                                        } else {
                                            $label = 'danger';
                                        }

                                        ?>
                                        <td class="col-sm-8"><span
                                                class="label label-<?php echo $label; ?>"><?php echo $status; ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-sm-4"><?php echo lang('label_support_ticket_assigned_to'); ?>:
                                        </td>
                                        <td class="col-sm-8"><span
                                                class="label label-info"><?php echo users_details($ticket_details->ticket_assigned_to,
                                                    1)->full_name; ?></span></td>
                                    </tr>
                                    <tr>
                                        <td class="col-sm-4"><?php echo lang('label_clients_company_name'); ?>:</td>
                                        <td class="col-sm-8"><span
                                                class="label label-info"><?php echo client_company($ticket_details->ticket_client_id); ?></span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel panel-success panel-block-color -->
                        <!-- END PANEL -->
                        <?php if ($ticket_details->ticket_attachment != ''): ?>
                            <p><strong><?php echo lang('label_attachment'); ?> :</strong></p>
                            <ul class="attachment-list">
                                <li><?php echo $ticket_details->ticket_attachment; ?><a
                                        href="<?php echo base_url('support/download/' . $ticket_details->ticket_attachment); ?>"><i
                                            class="fa fa-cloud-download"></i></a></li>
                            </ul>
                        <?php endif; ?>

                    </div>
                    <!--end details-->
                </div>

                <div class="col-lg-9">

                    <ul class="support-timeline">
                        <li class="centering-line"></li>
                        <li class="item-timeline highlight post-form-timeline">
                            <!-- text-center -->
                            <div class="buletan"></div>
                            <?php $poster_details = users_details($ticket_details->ticket_by,
                                $ticket_details->ticket_user_type); ?>
                            <div class="inner-content">
                                <div class="heading-timeline">
                                    <img style="width: 80px; height: 80px;" ; alt="Avatar" class="avatar"
                                         src="<?php echo base_url('files/profile_images/' . $poster_details->avatar); ?>">

                                    <div class="user-timeline-info">
                                        <p style="margin-left:60px; margin-bottom: 5px;">
                                            <?php echo $poster_details->full_name; ?>
                                            <small><?php echo $ticket_details->ticket_date; ?></small>
                                            <br/>

                                        </p>

                                    </div>
                                    <!-- /.user-timeline-info -->
                                </div>
                                <!-- /.heading-timeline -->
                                <br/><br/>

                                <?php echo nl2br_except_pre($ticket_details->ticket_message); ?>


                                <br/>
                                <?php if ($ticket_details->ticket_attachment != ''): ?>
                                    <p><strong><?php echo lang('label_attachment'); ?> :</strong></p>
                                    <ul class="attachment-list">
                                        <li><a href="#fakelink"><?php echo $ticket_details->ticket_attachment; ?></li>
                                    </ul>
                                    <?php echo form_open('message/download/', array('role' => 'form')); ?>
                                    <input class="form-control" type="hidden" readonly name="ticket_id"
                                           value="<?php echo $ticket_details->ticket_id; ?>"/>
                                    <input class="form-control" type="hidden" readonly name="attachment"
                                           value="<?php echo $ticket_details->ticket_attachment; ?>"/>
                                    <button class="btn btn-info"><i class="fa fa-cloud-download"></i> Download</button>
                                    <?php echo form_close(); ?>
                                <?php endif; ?>
                            </div>
                            <!-- /.inner-content -->

                        </li>

                        <li class="item-timeline highlight">
                            <div class="buletan"></div>
                            <div class="inner-content">
                                <!--<form style="margin-top: -1px" role="form">-->
                                <?php echo form_open('support/reply_ticket', array('id' => 'create_reply')); ?>
                                <input class="form-control" type="hidden" readonly name="ticket_id"
                                       value="<?php echo $ticket_details->ticket_id; ?>"/>

                                <p>
                                    <textarea class="summernote-sm" name="reply_message"></textarea>
                                </p>

                                <p class="text-right">
                                    <button class="btn btn-danger btn-sm" type="submit">reply</button>
                                </p>
                                </form>
                            </div>
                            <!-- /.inner-content -->

                        </li>

                        <?php if (count($ticket_replies)): foreach ($ticket_replies as $ticket_reply): ?>
                            <li class="item-timeline post-form-timeline">
                                <div class="buletan"></div>
                                <div class="inner-content">
                                    <!-- BEGIN HEADINg TIMELINE -->
                                    <?php $user_details = users_details($ticket_reply->reply_user_id,
                                        $ticket_reply->reply_user_type); ?>
                                    <div class="heading-timeline">
                                        <img alt="Avatar" class="avatar"
                                             src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>">

                                        <div class="user-timeline-info">
                                            <p>
                                                <?php echo $user_details->full_name; ?>
                                                <small><?php echo $ticket_reply->reply_date; ?></small>
                                            </p>
                                        </div>
                                        <!-- /.user-timeline-info -->
                                    </div>
                                    <!-- /.heading-timeline -->
                                    <!-- END HEADINg TIMELINE -->

                                    <?php echo $ticket_reply->reply_message; ?>


                                    <!-- BEGIN FOOTER TIMELINE -->
                                    <div class="footer-timeline">
                                        <ul class="timeline-option">
                                            <li class="option-row">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <ol>
                                                            <li></li>
                                                            <li></li>
                                                        </ol>
                                                    </div>
                                                    <!-- /.col-xs-6 -->
                                                    <div class="col-xs-6 text-right">
                                                        <ol>
                                                            <li><a href="#fakelink"><i
                                                                        class="fa fa-clock-o"></i><?php echo ago(strtotime($ticket_reply->reply_date)); ?>
                                                                </a></li>
                                                            <li><a href="#fakelink"><?php echo lang('ago'); ?></a></li>

                                                        </ol>
                                                    </div>
                                                    <!-- /.col-xs-6 -->
                                                </div>
                                                <!-- /.row -->
                                            </li>

                                        </ul>
                                    </div>
                                    <!-- /.footer-timeline -->
                                    <!-- END FOOTER TIMELINE -->
                                </div>
                                <!-- /.inner-content -->
                            </li>
                        <?php endforeach;endif; ?>
                    </ul>

                </div>
            </fieldset>
        </div>
    </div>
</div>