<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/24
 * Time: 10:19 AM
 */
?>
<div class="the-box">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <fieldset>
                <legend><i class="fa fa-plus"></i><?php echo lang('label_add_ticket'); ?>
                </legend>

                <?php echo form_open_multipart('support/add_pending', array('id' => 'create_ticket')); ?>
                <div class="form-group">
                    <label>Texto del Pendiente</label>
                    <input class="form-control" name="com_description" />

                </div>
                <div class="form-group">
                    <label>Pendiente de:</label>


                        <select data-placeholder="Select client..." class="form-control  chosen-select" tabindex="4"
                                name="project">
                            <optgroup label="">
                                <option value="Oficina">Oficina</option>
                                <?php if (count($projects)):foreach ($projects as $client): ?>
                                    <option
                                        value="<?php echo $client->project_title; ?>"><?php echo ucfirst($client->project_title) ?></option>
                                <?php endforeach; endif; ?>
                            </optgroup>


                        </select>


                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Nivel de urgencia</label>
                            <select class="form-control" name="com_color">
                                <option value="danger">Urgente</option>
								<option value="warning">Intermedio</option>
                                <option value="info">Normal</option>
                                <option value="success">Tarea terminada</option>

                            </select>
                        </div>
                    </div>
					<div class="col-md-8">
						<div class="form-group">
							<label>Seleccionar usuario</label>
							<select data-placeholder="Select users..."
							class="form-control chosen-select" multiple tabindex="4"
							name="assign_to[]">
								<optgroup label="<?php echo lang("label_users") ?>">
									<?php if (count($users)) : foreach ($users as $user): ?>
										<option value="<?php echo $user->emp_name . " " .$user->emp_surname ?>"><?php echo ucfirst($user->emp_name) . " " . ucfirst($user->emp_surname); ?></option>
									<?php endforeach; endif; ?>
								</optgroup>
							</select>
						</div>
					</div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-success btn-block btn-lg"><i
                                class="fa fa-sign-in"></i> <?php echo lang('form_button_save'); ?></button>
                    </div>
                </div>

                </form>
            </fieldset>
        </div>

    </div>
</div>
