<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/24
 * Time: 10:19 AM
 */
?>
<div class="the-box">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <fieldset>
                <legend><i class="fa fa-plus"></i><?php echo lang('label_add_ticket'); ?>
                </legend>
                <?php echo form_open_multipart('support/add', array('id' => 'create_ticket')); ?>
                <input class="form-control" type="hidden" readonly name="client_name"
                       value="<?php echo $this->CompanyClient_id; ?>"/>

                <div class="form-group">
                    <label> Client</label>
                    <input class="form-control" type="text" readonly
                           value="<?php echo client_company($this->CompanyClient_id); ?>"/>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label><?php echo lang('label_departments'); ?></label>
                            <select data-placeholder="Select people..." class="form-control" name="department_id">
                                <option value="Empty">&nbsp;</option>
                                <optgroup label="<?php echo lang("label_departments") ?>">

                                    <?php foreach ($this->config->item('support') as $department): ?>
                                        <option
                                            value="<?php echo $department->department_id; ?>"><?php echo ucfirst($department->department_desc); ?></option>
                                    <?php endforeach; ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label><?php echo lang("label_priorities") ?></label>
                            <select data-placeholder="Select people..." class="form-control" name="priority">
                                <option value="Empty">&nbsp;</option>
                                <optgroup label="<?php echo lang("label_priorities") ?>">
                                    <?php $priorities = task_priorities(); ?>
                                    <?php foreach ($priorities as $priority): ?>
                                        <option
                                            value="<?php echo $priority->priority_id; ?>"><?php echo ucfirst($priority->priority_name); ?></option>
                                    <?php endforeach; ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <label><?php echo lang('label_support_ticket_subject'); ?></label>
                    <input type="text" class="form-control input-lg" placeholder="Talk about what..."
                           name="ticket_subject" required>
                </div>
                <!-- /.input-group -->
                <div class="form-group">
                    <textarea class="summernote-lg" name="ticket_message"></textarea>
                </div>
                <!-- /.input-group -->
                <div class="form-group">
                    <label><?php echo lang('label_attachment'); ?></label>

                    <div class="input-group">
                        <input type="text" class="form-control" readonly>
												<span class="input-group-btn">
													<span class="btn btn-primary btn-file">
														Browse files<input type="file" multiple
                                                                           name="ticket_attachment">
													</span>
												</span>
                    </div>
                    <!-- /.input-group -->
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-success btn-block btn-lg"><i
                                class="fa fa-sign-in"></i> <?php echo lang('form_button_save'); ?></button>
                    </div>
                </div>

                </form>
            </fieldset>
        </div>
    </div>
</div>