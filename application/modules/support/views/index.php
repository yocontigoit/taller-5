<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/24
 * Time: 10:46 AM
 */
?>
<div class="the-box">
    <div class="row">
        <div class="col-md-12">
            <fieldset>
                <legend><i class="fa fa-support"></i>
                </legend>
                <div class="table-toolbar">
                    <div class="btn-group">

                        <?php echo anchor('support/add', lang('form_button_add_ticket') . ' <i class="fa fa-plus"></i>',
                            array('class' => 'btn btn-danger')); ?>

                    </div>

                </div>
                <div class="table-responsive">
                    <table
                        class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
                        id="sample_2">
                        <thead class="the-box dark full">
                        <!-- <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success" id="userstable">
                             <thead>-->
                        <tr>
                            <th>
                                <?php echo lang("label_support_ticket_no"); ?>
                            </th>
                            <th>
                                <?php echo lang("label_support_ticket_priority"); ?>
                            </th>
                            <th class="hidden-xs">
                                <?php echo lang("label_support_ticket_department"); ?>
                            </th>
                            <th>
                                <?php echo lang("label_support_ticket_assigned_to"); ?>
                            </th>
                            <th>
                                <?php echo lang("common_status"); ?>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (count($tickets)): foreach ($tickets as $ticket): ?>
                            <tr class="tr-link"
                                data-link="<?php echo base_url("user/edit/" . ($user->user_id * 99)); ?>">
                                <td>
                                    <?php echo $ticket->user_id; ?>
                                </td>
                                <td>
                                    <?php echo $ticket->emp_name; ?>
                                </td>
                                <td class="hidden-xs">
                                    <?php echo $ticket->emp_surname; ?>
                                </td>
                                <td>
                                    <?php echo $ticket->email; ?>
                                </td>
                                <td>
                                    <?php echo $ticket->active; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="6"><?php echo lang("messages_no_ticket_found"); ?></td>
                            </tr>
                        <?php endif; ?>

                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
    </div>
</div>
