<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/06/03
 * Time: 03:11 PM
 */
class Bitcoin extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("bitcoin_m");
    }

    function add_payment($invoice_id = null)
    {

        $blockchain_root = $this->config->item('bitcoin')->bitcoin_root_url;
        $blockchain_receive_root = $this->config->item('bitcoin')->bitcoin_root_receive;
        $secret = $this->config->item('bitcoin')->bitcoin_secret_key;
        $my_xpub = $this->config->item('bitcoin')->bitcoin_address;
        $my_api_key = $this->config->item('bitcoin')->bitcoin_api_key;


        $reference_no = get_invoice_reference_no($invoice_id);
        $invoice_currency = get_invoice_currency($invoice_id);
        $invoice_amount = invoice_balance($invoice_id);

        if ($invoice_amount <= 0) {
            $invoice_amount = 0.00;
        }
        $invoice_info = new stdClass();
        $invoice_info->item_name = $reference_no;
        $invoice_info->item_number = $invoice_id;
        $invoice_info->amount = $invoice_amount;
        $invoice_info->currency = $invoice_currency;


        $this->data['invoice_info'] = $invoice_info;

        $this->data['bitcoin'] = true;

        $urls = "https://blockchain.info/tobtc?currency=" . $invoice_currency . "&value=" . $invoice_amount;

        $btc_amount = $this->curl_get_contents($urls);

        $this->data['btc_amount'] = $this->round_up($btc_amount, 3);

        $blockchain_api = "https://blockchain.info/api/receive?api_code=7a4d9254-81ce-496d-98ce-8408082915c9&method=create&cors=true&format=plain&address=" . $my_xpub . "&shared=false&callback=" . base_url() . "bitcoin%2Fsuccess%3Fusdamount%3D" . $invoice_amount . "%26invoicename%3D" . $reference_no . "%26btcamount%3D" . $btc_amount . "%26invoice%3D" . $invoice_id . "%26client%3D" . $this->CompanyClient_id;

        $recieve_api = $this->curl_get_contents($blockchain_api);

        $decoded = json_decode($recieve_api);


        $this->data['btc_address'] = $decoded->{'input_address'};

        $this->data['title'] = 'Agregar Pago';

        $this->load->view('modal/add_payment', $this->data);
    }

    function curl_get_contents($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    function round_up($value, $precision)
    {
        $pow = pow(10, $precision);

        return (ceil($pow * $value) + ceil($pow * $value - ceil($pow * $value))) / $pow;
    }

    function cancel()
    {
        $this->session->set_flashdata('msg_status', 'error');
        $this->session->set_flashdata('message', 'Bitcoin payment canceled.');
        redirect('clients');
    }

    function success()
    {

        echo "*ok*";
        function round_up($value, $precision)
        {
            $pow = pow(10, $precision);

            return (ceil($pow * $value) + ceil($pow * $value - ceil($pow * $value))) / $pow;
        }

        $transactionid = $_GET['transaction_hash'];
        $invoice_id = $_GET['invoice'];
        $usdamount = $_GET['usdamount'];
        $btcamount = $_GET['btcamount'];
        $client_id = $_GET['client'];
        $amountsentsatoshi = $_GET['value'];
        $amountsent = $amountsentsatoshi / 100000000;
        $ratio = $amountsent / $btcamount;
        $paid = $usdamount * $ratio;
        $paid_amount = round_up($paid, 2);

        $client_details = get_client_details($client_id);
        $invoice_currency = get_invoice_currency($invoice_id);
        $transaction = array(
            'invoice_id' => $invoice_id,
            'trans_id' => $transactionid,
            'amount' => $paid_amount,
            'payment_method' => 4,
            'ip' => $this->input->ip_address(),
            'notes' => 'Paid by ' . $this->CompanyUser_FullName . ' via Bitcoin | Invoice Currency:' . $invoice_currency . '- Amount in BTC: ' . $amountsent,
            'creator_email' => $this->CompanyClient_email,
            'created_by' => $client_details->client_name,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );
        if ($this->bitcoin_m->add_payment($transaction)) {

            $this->notifications->send_payment_notification($this->CompanyClient_id, $invoice_id, $paid_amount);

        }

    }

}