<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/06/03
 * Time: 03:12 PM
 */
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Paying
                <strong>
                    <?php echo $invoice_info->currency; ?> <?php echo number_format($invoice_info->amount, 2) ?>
                </strong> for Invoice #<?php echo $invoice_info->item_name; ?> via Bitcoin</h4>
        </div>
        <div class="modal-body">
            <?php
            $attributes = array('id' => 'payment-form', 'class' => 'bs-example form-horizontal');
            if (isset($errors) && !empty($errors) && is_array($errors)) {
                echo '<div class="alert alert-error"><h4>Error!</h4>The following error(s) occurred:<ul>';
                foreach ($errors as $e) {
                    echo "<li>$e</li>";
                }
                echo '</ul></div>';
            } ?>

            <div id="payment-errors"></div>
            <input type="hidden" name="invoice_id" value="<?php echo $invoice_info->item_number; ?>">
            <input type="hidden" name="amount" value="<?php echo number_format($invoice_info->amount, 2) ?>">
            <input type="hidden" name="btc_amount" value="<?php echo $btc_amount ?>">


            <h4>Send <?php echo $btc_amount; ?> BTC to <a
                    href="bitcoin:<?php echo $btc_address; ?>?amount=<?php echo $btc_amount; ?>"><?php echo $btc_address; ?></a>
            </h4>
            <br>

            <div class="alert alert-info" style="align:center">Your invoice will be marked as paid automatically.</div>
            <div class="modal-footer"><a href="#" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></a>
            </div>


        </div>

        </form>

    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->