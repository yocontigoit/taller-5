<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/10/29
 * Time: 03:33 PM
 */

?>

<div id="contentarea" class="contentarea">
    <div class="row">
        <div class="col-md-12">

            <div class="col-md-4">
                <div class="the-box">
                    <div class="progress" style="height: 20px;">

                        <?php

                        $project_info = get_project_details($project_id);
                        $pro_percentage = 0;
                        if ($project_info->project_use_worklog == 0) {
                            $pro_percentage = $project_info->project_progress;
                        } else {
                            $pro_percentage = calculate_project_percentage($project_id);

                        }

                        ?>
                        <div class="progress-bar progress-bar-info" role="progressbar"
                             aria-valuenow="<?php echo $pro_percentage; ?>" aria-valuemin="0" aria-valuemax="100"
                             style="width:<?php echo $pro_percentage; ?>%;">
                            <?php echo $pro_percentage; ?>% Terminado
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
            <center><h2>  <?php $project_info = get_project_details($project_id);
               echo $project_info->project_title;?></h2></center>
            </div>
            <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) { ?>
                <div class="col-md-4">
                    <div class="the-box toolbar">
                        <div class="btn-toolbar" role="toolbar">

                            <div class="btn-group pull-right pro-start-stop-div">

                                <a class="btn pro-countdown-timer btn-danger" style="background:#828282;border-width:0px;"  href="#" id="pro-start-timer"
                                   data-toggle="tooltip" title="Start / Stop recording"><i class="fa fa-play"></i></a>

                            </div>

                            <div class="btn-group pull-left">
                                <a class="btn text-green" data-toggle="tooltip" title="Edit Project"
                                   href="<?php echo base_url('project/edit_project/' . $project_id); ?>"><i
                                        class="fa fa-pencil-square-o  fa-2x"></i></a>
                            </div>
                            <div class="btn-group pull-left">
                                <a class="btn text-danger" style="color:#828282;" data-toggle="tooltip" title="Clone Project"
                                   href="<?php echo base_url('project/clone_project/' . $project_id); ?>"><i
                                        class="fa fa-copy  fa-2x"></i></a>
                            </div>
                            <div class="btn-group pull-left">
                                <a class="btn text-light-blue" data-toggle="tooltip" title="Invoice Project"
                                   href="<?php echo base_url('project/invoice_project/' . $project_id); ?>"><i
                                        class="fa fa-credit-card  fa-2x"></i></a>
                            </div>


                        </div>
                        <!-- /.btn-toolbar -->


                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">

            <div class="col-md-3">

                <ul class="ver-inline-menu tabbable margin-bottom-10">
                    <li <?php echo ($this->uri->segment(2) == 'overview' || $this->uri->segment(3) == '') ? 'class="active"' : '' ?>>
                        <?php echo anchor('project/overview/preview/' . $project_id,
                            '<i class="fa fa-folder-open"></i>' . lang('label_project_overview')); ?>
                        <span class="after"></span>
                    </li>

                    <?php if ((project_setting('view_gantt',
                                $project_id) && $this->CompanyUserRolePermission_id == 2) || $this->CompanyUserRolePermission_id == 1
                    ) : ?>

                    <?php endif; ?>
                    <?php if ((project_setting('view_milestones',
                                $project_id) && $this->CompanyUserRolePermission_id == 2) || $this->CompanyUserRolePermission_id == 1
                    ) : ?>
                        <li <?php echo ($this->uri->segment(2) == 'milestone') ? 'class="active"' : '' ?>>
                            <?php echo anchor('project/milestone/preview/' . $project_id,
                                '<i class="fa fa-map-signs"></i>' . lang('label_project_milestone')); ?>
                        </li>
                    <?php endif; ?>
                    <?php if ((project_setting('view_issues',
                                $project_id) && $this->CompanyUserRolePermission_id == 2) || $this->CompanyUserRolePermission_id == 1
                    ) : ?>
                        <li <?php echo ($this->uri->segment(2) == 'issues') ? 'class="active"' : '' ?>>
                            <?php echo anchor('project/issues/preview/' . $project_id . '/issues',
                                '<i class="fa fa-bug"></i>Contingencias' . lang('')); ?>
                        </li>
                    <?php endif; ?>
                    <?php if ((project_setting('view_team_members',
                                $project_id) && $this->CompanyUserRolePermission_id == 2) || $this->CompanyUserRolePermission_id == 1
                    ) : ?>
                        <li <?php echo ($this->uri->segment(2) == 'team') ? 'class="active"' : '' ?>>
                            <?php echo anchor('project/team/preview/' . $project_id . '/team',
                                '<i class="fa fa-users"></i>Equipo de Trabajo' . lang('')); ?>
                        </li>
                    <?php endif; ?>
                    <?php if ((project_setting('view_project_files',
                                $project_id) && $this->CompanyUserRolePermission_id == 2) || $this->CompanyUserRolePermission_id == 1
                    ) : ?>
                        <li <?php echo ($this->uri->segment(2) == 'documents') ? 'class="active"' : '' ?>>
                            <?php echo anchor('project/documents/preview/' . $project_id . '/documents',
                                '<i class="fa fa-file-pdf-o"></i>' . lang('label_project_documents')); ?>
                        </li>
                    <?php endif; ?>
                    <?php if ((project_setting('view_project_comments',
                                $project_id) && $this->CompanyUserRolePermission_id == 2) || $this->CompanyUserRolePermission_id == 1
                    ) : ?>
                        <li <?php echo ($this->uri->segment(2) == 'comments') ? 'class="active"' : '' ?>>
                            <?php echo anchor('project/comments/preview/' . $project_id . '',
                                '<i class="fa fa-comments"></i> Reportes'); ?>
                        </li>
                        <li <?php echo ($this->uri->segment(2) == 'gantt') ? 'class="active"' : '' ?>>
                            <?php echo anchor('project/gantt/preview/' . $project_id . '',
                                '<i class="fa fa-money"></i> Ingresos Diversos'); ?>
                        </li>
                    <?php endif; ?>
                    <?php if ((project_setting('view_expenses',
                                $project_id) && $this->CompanyUserRolePermission_id == 2) || $this->CompanyUserRolePermission_id == 1
                    ) : ?>
                        <li <?php echo ($this->uri->segment(2) == 'expenses') ? 'class="active"' : '' ?>>
                            <?php echo anchor('project/expenses/preview/' . $project_id . '',
                                '<i class="fa fa-money"></i> Estado de cuenta' ); ?>
                        </li>
                    <?php endif; ?>
                    <?php if ((project_setting('view_costs',
                                $project_id) && $this->CompanyUserRolePermission_id == 2) || $this->CompanyUserRolePermission_id == 1
                    ) : ?>
                        <li <?php echo ($this->uri->segment(2) == 'costs') ? 'class="active"' : '' ?>>
                            <?php echo anchor('project/costs/preview/' . $project_id . '',
                                '<i class="fa fa-money"></i>Gastos de proyecto'); ?>
                        </li>
                    <?php endif; ?>
                    <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                        <li <?php echo ($this->uri->segment(2) == 'notes') ? 'class="active"' : '' ?>>
                            <?php echo anchor('project/notes/preview/' . $project_id . '/notes',
                                '<i class="fa fa-book"></i> Minutas'); ?>
                        </li>
                    <?php } ?>
                    <?php if ((project_setting('view_timesheets',
                                $project_id) && $this->CompanyUserRolePermission_id == 2) || $this->CompanyUserRolePermission_id == 1
                    ) : ?>
                        <!-- <li <?php echo ($this->uri->segment(2) == 'timesheet') ? 'class="active"' : '' ?>>
                            <?php echo anchor('project/timesheet/preview/' . $project_id . '/timesheet',
                                '<i class="fa fa-clock-o"></i>' . lang('label_project_time_sheet')); ?>
                        </li> -->
                    <?php endif; ?>
                    <?php if ((project_setting('view_tasks',
                                $project_id) && $this->CompanyUserRolePermission_id == 2) || $this->CompanyUserRolePermission_id == 1
                    ) : ?>
                        <li <?php echo ($this->uri->segment(2) == 'tasks') ? 'class="active"' : '' ?>>
                            <?php echo anchor('project/tasks/preview/' . $project_id . '/tasks',
                                '<i class="fa fa-tasks"></i>' . lang('label_project_tasks')); ?>

                        </li>
                    <?php endif; ?>
                </ul>

            </div>
            <!-- /.col-sm-3 -->
            <div class="col-md-9">
                <div class="the-box">
                    <?php echo (isset($_PAGE)) ? $_PAGE : 'Nothing to display here..'; ?>
                </div>
                <!-- /.the-box -->
            </div>
            <!-- /.col-sm-9 -->

        </div>

    </div>
    <!-- /.row -->
</div>
