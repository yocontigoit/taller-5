<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/09/17
 * Time: 9:06 AM
 */


?>
<div class="the-box">
    <div class="table-toolbar">
        <div class="btn-group">

            <?php echo anchor('project/add', '<h4>Proyecto Nuevo <i class="fa fa-plus"></i></h4>',
                array('class' => 'btn btn-info')); ?>

        </div>
        <div class="btn-group pull-right">
            <button class="btn dropdown-toggle" data-toggle="dropdown">Herramientas<i class="fa fa-angle-down"></i>
            </button>
            <ul class="dropdown-menu pull-right">
                <li>
                    <a href="#">
                        Imprimir
                    </a>
                </li>
                <li>
                    <a href="#">
                        Guardar como PDF
                    </a>
                </li>

                <li>
                    <a href="#">
                        Agregar proyecto como csv
                    </a>
                </li>
                <li>
                    <a href="#">
                        Exportar en Excel
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="the-box">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
                   id="zest_table">
                <thead class="the-box dark full">
                <tr>
                    <th><?php echo lang('label_project_code'); ?></th>
                    <th class="hidden-xs"><?php echo lang('label_project_name'); ?></th>
                    <th class="hidden-xs"><?php echo lang('label_project_start_date'); ?></th>
                    <th><?php echo lang('label_project_end_date'); ?></th>

                    <th><?php echo lang('label_project_status'); ?></th>
                    <th><?php echo lang('label_options'); ?></th>

                </tr>
                </thead>
                <tbody>

                <?php if (count($projects)): foreach ($projects as $project): ?>
                    <tr>
                        <td>
                            <?php echo $project->project_code; ?>
                            <?php
                            $project_status = get_task_status_name($project->project_status);
                            $project_info = get_project_details($project->project_id);
                            $pro_percentage = 0;
                            if ($project_info->project_use_worklog == 0) {
                                $pro_percentage = $project_info->project_progress;
                            } else {
                                $pro_percentage = calculate_project_percentage($project->project_id);

                            }

                            if ($pro_percentage >= 100) {
                                $project_status = 'Terminado';
                                $bg = 'success';
                            } else {
                                $project_status = 'Activo';
                                $bg = 'info';
                            }
                            ?>

                            <div class="progress progress-xs progress-striped active">
                                <div
                                    class="progress-bar progress-bar-<?php echo ($pro_percentage >= 100) ? 'success' : 'primary'; ?>"
                                    data-toggle="tooltip" data-original-title="<?php echo $pro_percentage ?>%"
                                    style="width: <?php echo $pro_percentage; ?>%"></div>
                            </div>
                        </td>
                        <td class="hidden-xs" style="text-align:center;">
                            <a class=""
                                       href="<?php echo site_url('project/overview/preview/' . $project->project_id); ?>"
                                       title="">
                            <?php echo $project->project_title; ?></a>
                        </td>
                        <td class="hidden-xs">
                            <?php echo date('Y-m-d', strtotime($project->project_start_date)); ?>

                        </td>
                        <td>
                            <?php echo date('Y-m-d', strtotime($project->project_due_date)); ?>

                        </td>
                        <td>
                            <span class="label label-<?php echo $bg; ?>"><?php echo $project_status; ?></span>
                        </td>
                        <td>

                                                <a href="<?php echo base_url('project/edit_project/' . $project->project_id); ?>"
                                                   title=""><i
                                                        class="fa fa-pencil-square-o" style="font-size:25px;color:#428bca"></i>
                                                </a> &nbsp&nbsp&nbsp&nbsp
                                           <a data-toggle="zestModal"
                                                   href="<?php echo base_url('project/delete/' . $project->project_id); ?>"
                                                   title=""><i
                                                        class="fa fa-trash" style="font-size:25px;color:#428bca""></i></a>

                                        </ul>

                                </div>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php endif; ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
