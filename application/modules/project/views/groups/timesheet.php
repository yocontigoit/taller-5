<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 4/6/2015
 * Time: 2:06 PM
 */
?>

<div class="row">
    <div class="col-lg-12">
        <fieldset>
            <legend><i class="fa fa-clock-o"></i>Log Sheet</legend>
            <div class="col-lg-8">
                <div class="content-col" id="page">
                    <div class="inner-content">
                        <div id="calendar" class="mt-20"></div>
                        <p class="text-muted mt-10"><?php ?></p>
                    </div>
                </div>
                <div style="clear:both"></div>

            </div>

            <div class="col-lg-4">

                <form method="post" action="<?php echo base_url('project/timesheet/log_time/' . $project_id); ?>">
                    <div id="" class="panel">

                        <div class="form-wrapper">

                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Tiempo de trabajar:</label>
                                        <select class="form-control" name="tasks_for" required="">
                                            <?php if (count($assigned_team)): foreach ($assigned_team as $team):
                                                $user_details = users_details($team->user_id, 1);
                                                ?>
                                                <option
                                                    value="<?php echo $team->user_id; ?>"><?php echo $user_details->full_name; ?></option>

                                            <?php endforeach; endif ?>
                                        </select>
                                    </div>

                                </div>

                            </div>

                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-12">
                                        <label> Tarea </label>
                                        <input class="form-control " type="hidden"
                                               value="<?php echo get_project_details($project_id)->project_code; ?>"
                                               name="client_project" size="6">

                                        <select class="form-control" name="client_tasks">

                                            <optgroup label="Task">
                                                <option value="none">ninguna</option>
                                                <?php

                                                if (!empty($project_tasks)) {
                                                    foreach ($project_tasks as $project_task) { ?>
                                                        <option
                                                            value="<?php echo $project_task->task_id; ?>"><?php echo strtoupper($project_task->task_name); ?></option>
                                                    <?php }
                                                } ?>
                                            </optgroup>

                                        </select>
                                    </div>

                                </div>

                            </div>
                            <div class="row">


                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Fecha inicio</label>
                                        <input type="text" class="form-control datepicker" name="start_time"
                                               data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                               value="<?php echo date('d-m-Y') ?>">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                              </div>
                              <!-- /.row -->

                              <div class="row">
                                <!-- /.col-sm-6 -->
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Hora inicio</label>

                                        <div class="input-group input-append bootstrap-timepicker">
                                            <input type="text" class="form-control timepicker timepicker-24"
                                                   name="start_timer">
                                            <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col-sm-6 -->

                            </div>
                            <!-- /.row -->

                            <div class="row">


                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Fecha Termino</label>
                                        <input type="text" class="form-control datepicker" name="end_time"
                                               data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                               value="<?php echo date('d-m-Y') ?>">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                              </div>
                              <!-- /.row -->

                              <div class="row">
                                <!-- /.col-sm-6 -->
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Hora Termino</label>

                                        <div class="input-group input-append bootstrap-timepicker">
                                            <input type="text" class="form-control timepicker timepicker-24"
                                                   name="end_timer">
                                            <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col-sm-6 -->

                            </div>
                            <!-- /.row -->


                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-12">
                                        <label> Hours </label>
                                        <input class="form-control " type="text" value="" name="client_hours" size="6">
                                    </div>

                                </div>

                            </div>

                            <div class="form-group">

                                <div class="row">
                                    <div class="col-md-12">
                                        <label><?php echo lang('label_notes'); ?></label>
                                        <textarea id="body" style="float: left;padding: 0 0 15px;width: 100%;" rows=""
                                                  cols="" name="body"></textarea>
                                    </div>

                                </div>

                            </div>


                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-block btn-lg" style="color:#828282;border-width:0px;color:#fff;"><i
                                    class="fa fa-sign-in"></i> <?php echo lang('form_button_log_hours'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </fieldset>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-clock-o"></i>
                <span class="badge pull-right text-muted"><?php echo count($manual_timelogs); ?></span>
            </div>
            <table class="table  table-hover table-full-width table-th-block">
                <tbody>
                <tr>
                    <td><?php echo lang('label_project_name'); ?></td>
                    <td>Task</td>
                    <td>Total Hours</td>
                    <td>Time Logged For</td>
                    <td>Time Logged By</td>
                    <td>Action</td>
                </tr>
                <?php if (count($manual_timelogs)):foreach ($manual_timelogs as $entry): ?>
                    <tr>
                        <td><?php echo get_project_details($entry->project_id)->project_code; ?></td>
                        <td><?php echo get_task_details($entry->task_id)->task_name; ?> </td>
                        <td><?php echo $entry->hours; ?> </td>
                        <td>
                            <?php
                            $user_details = users_details($entry->user_id, 1);
                            echo $user_details->full_name;
                            ?>

                        </td>
                        <td>
                            <?php

                            $user_detail = users_details($entry->entry_by, 1);
                            echo $user_detail->full_name;
                            ?>

                        </td>
                        <td>

                        </td>
                    </tr>
                <?php endforeach; endif; ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
