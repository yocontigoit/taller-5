<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/17
 * Time: 08:44 AM
 */
?>
<div class="row">
    <div class="col-lg-12">
        <fieldset>
            <legend><i class="fa fa-support"></i>
                <?php echo $expense_details->description; ?>
            </legend>
            <div class="col-lg-6">
                <!--Ticket details -->
                <div class="box">
                    <!-- BEGIN PANEL-->
                    <div class="panel panel-info panel-square panel-no-border text-center">
                        <div class="panel-heading">
                            <h3 class="panel-title"> Details</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered table-hover table-extra-padd">
                                <tbody>
                                <tr>
                                    <td class="col-sm-4"><?php echo 'Expense Date'; ?></td>
                                    <td class="col-sm-8"><?php echo $expense_details->date_entry; ?></td>
                                </tr>
                                <tr>
                                    <td class="col-sm-4"><?php echo 'Date Created'; ?></td>
                                    <td class="col-sm-8"><?php echo $expense_details->date_entry; ?></td>
                                </tr>
                                <tr>
                                    <td class="col-sm-4"><?php echo 'Category'; ?>:</td>
                                    <td class="col-sm-8"><?php echo get_category_name($expense_details->classification); ?></td>
                                </tr>
                                <tr>
                                    <td class="col-sm-4"><?php echo 'Estimated Cost'; ?>:</td>
                                    <td class="col-sm-8"><?php echo $expense_details->estimate_cost; ?></td>
                                </tr>
                                <tr>
                                    <td class="col-sm-4"><?php echo 'Expense Cost'; ?>:</td>
                                    <td class="col-sm-8"><?php echo $expense_details->real_cost; ?></td>
                                </tr>

                                <tr>
                                    <td class="col-sm-4"><?php echo 'Added BY'; ?>:</td>
                                    <td class="col-sm-8"><span class="label label-info"><?php echo 'unknown'; ?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-sm-4"><?php echo lang('label_clients_company_name'); ?>:</td>
                                    <td class="col-sm-8"><span
                                            class="label label-info"><?php //echo client_company($ticket_details->ticket_client_id); ?></span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END PANEL -->

                </div>
                <!--end details-->
            </div>

            <div class="col-lg-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div
                            style="margin: auto; position: absolute; top: 0%; left: 40%; bottom: 0px; right: 0; transform: translate(-50% -50%);">
                            <?php
                            $total_expense = tot_expense_amount();
                            if ($expense_details->real_cost > 0 && $total_expense > 0) {
                                $percentage = round(($expense_details->real_cost / $total_expense) * 100);
                            } else {

                                $percentage = 0;
                            }
                            ?>
                            <div class="chart easy-pie-expense text-danger" style="color:#828282;" data-percent="<?php echo $percentage; ?>"
                                 data-line-width="10" data-rotate="270" data-scale-Color="false" data-size="120"
                                 data-animate="2000">
                                <span class="h2 step font-bold"><?php echo $percentage; ?></span>%

                                <div
                                    class="easypie-text text-muted"><?php echo $expense_details->real_cost . "/" . $total_expense; ?></div>
                            </div>
                            <div class="font-bold m-t">Expense/Total Expenses</div>
                        </div>
                        <div style="clear: both"></div>
                    </div>
                </div>

                <div class="row" style="margin-top:50%;">
                    <div class="col-lg-12">
                        <div class="panel panel-danger panel-square panel-no-border text-center">
                            <div class="panel-heading" style="background:#828282;border-width:0px;">
                                <h3 class="panel-title">Expense Amount</h3>
                            </div>
                            <div class="panel-body">
                                <h1 class="bolded tiles-number text-danger" style="color:#828282;"><?php echo format_currency($expense_details->real_cost); ?></h1>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </fieldset>
    </div>
</div>
