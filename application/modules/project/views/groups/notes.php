<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/04
 * Time: 08:41 AM
 */
?>

<div class="row">

    <div class="col-lg-12">
        <fieldset>
            <legend><i class="fa fa-book"></i> Minutas
                <span class="col-md-offset-8"><a class="btn btn-info btn-sm"
                                            href="<?php echo base_url('project/notes/save_note/' . $project_id); ?>">Agregar Minuta<i
                            class="fa fa-plus"></i></a></span>
            </legend>
<br>
<div class="col-lg-12">

    <div class="events_list">

        <?php if (count($project_notes)): foreach ($project_notes as $note): ?>
            <div class="event_item">
                <div style="width: 180px;" class="event_hour">Minuta: <?php echo readable_date_format($note->note_date); ?></div>
                <ul class="event_content">
                    <li>
                        <a data-toggle="zestModal" href="<?php echo base_url('project/notes/notes/' . $note->project_id . '/' . $note->note_id); ?>">
                            <p>"                " ."      "."      ". <?php echo $note->cost_description; ?>.    <?php echo $note->cost_classification; ?></p>
                        </a>

                    </li>
                </ul>

            </div>
        <?php endforeach; endif; ?>
    </div>
</div>
</div>
<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/chosen/chosen.jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/libs/jquery-2.0.2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/libs/jquery-ui-1.10.3.min.js') ?>"></script>
<script type="text/javascript">
  $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

</script>
