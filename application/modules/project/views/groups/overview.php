<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/10/30
 * Time: 12:10 PM
 */
 $avancetotal =0;
 $totalsem =0;
?>
<style>
    .project-info-bg {
        background: #FBFBFB !important;
        color: #333 !important;
        border-top: 1px solid #E4E5E7;
        border-left: 1px solid #E4E5E7;
        border-right: 1px solid #E4E5E7;
        font-weight: 600;
    }

</style>
<div class="panel-body">
    <div class="row">

        <?php
        $pro_duration = duration_diff($project_details->project_start_date, $project_details->project_due_date);
        $project_status = get_task_status_name($project_details->project_status);
        $project_info = get_project_details($project_details->project_id);
        $pro_percentage = 0;
		$total = 0;
        if ($project_info->project_use_worklog == 0) {
            $pro_percentage = $project_info->project_progress;
        } else {
            $pro_percentage = calculate_project_percentage($project_info->project_id);

        }
        if ($pro_percentage >= 100) {
            $project_status = 'Completo';
            $bg = '#03a9f4';
        } else {
$project_status = 'ACTIVO';
            $bg = '#03a9f4';
        }
		if(isset($import) && is_array($import) && count($import)): $i=1;
				foreach ($import as $data) {
					if($data->codigo_proyecto == "PRO-00005") {
						$total += $data->importe;
					}
				}endif;



			$totpp=0; $totmi = 0; if (count($milestones)): foreach ($milestones as $milestone): if ($milestone->newer != 1):
				    $totmi += $milestone->importe;
				endif; endforeach; endif;
        $totpp = $totmi;
				$totpi=0; if (count($import)): foreach ($import as $data):
				   $totpi = $totpi + $data->importe;
				 endforeach; endif;
        ?>

         <div class="col-md-6">
          <img src="https://proyectosinternos.com/Taller5/files/profile_images/<?php echo $project_details->avatar; ?>" width="100%" height="auto">
          <center>
              <?php echo form_open_multipart('project/change_image',
                  array('class' => 'form-horizontal', 'role' => 'form')); ?>

              <input type="hidden" name="project_id" value="<?php echo($project_details->project_id); ?>">

              <div class="form-group">
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                      <div class="fileinput-preview fileinput-exists thumbnail"
                           style="max-width: 200px; max-height: 150px;">
                      </div>

                      <div>
                          <span class="btn btn-file">
                            <span class="fileinput-new">
                               Seleccionar Imagen
                            </span>
                            <span class="fileinput-exists">
                               Otra
                            </span>
                            <input type="file" name="user_picture">
                          </span>
                          <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                              Eliminar
                          </a>
                          <button type="submit"
                                  class="btn btn-sm btn-success fileinput-exists" align="left">Actualizar Imagen</button>
                      </div>
                  </div>
              </div></center>
          <?php echo form_close(); ?>
      </div>
  <div class="col-md-4"><h3 style="background:<?php echo $bg; ?>;color:#fff;text-align:center;padding:20px;margin-top:0px;margin-left:15%;margin-right:15%;"><?php echo $project_status; ?></h3></div>

</br>


        <div class="col-md-6">

            <div class="block">
                <ul class="headline-info">
                    <li><?php echo lang('label_start_date'); ?>: <strong class="text-info"><?php echo date('Y-m-d',
                                strtotime($project_details->project_start_date)); ?></strong></li>
                    <li><?php echo lang('label_due_date'); ?>: <strong class="text-info"><?php echo date('Y-m-d',
                                strtotime($project_details->project_due_date)); ?></strong></li>
                                <li>Total de Semanas: <?php
        $diff = strtotime($project_details->project_due_date, 0) - strtotime($project_details->project_start_date, 0);
echo $totalsem =  floor($diff / 604800); ?></li>
                </ul>
                <hr/>

				<div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="summary">
                              <div class="content">
                                <div class="row">
                                  <p class="billable">Presupuesto Obra: </p><br/>
                                  <span
                                      class="" style="font-size:19px;">$<?php echo format_currency($totpi); ?></span>
                              </div>
                                <div class="row">
                                <p class="billable"><?php echo lang('label_project_expenses'); ?></p><br/>
                                <span
                                    class="" style="font-size:19px;">$0.00
                                    <!-- <?php echo round($total/6,2); ?> -->
                                  </span>
                                  </div>
                                  <div class="row">
                                    <p class="billable">Estimaciones: </p><br/>
                                    <span
                                        class="" style="font-size:19px;">$0.00</span>
                                </div>
                                  <div class="row">
                                    <p class="billable">Pagos: </p><br/>
                                    <span
                                        class="" style="font-size:19px;">$0.00</span>
                                </div>

                                <div class="row">
                                  <p class="billable">Saldo: </p><br/>
                                  <span
                                      class="" style="font-size:19px;">$<?php echo format_currency($totpi); ?></span>
                              </div>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="summary">
                              <div class="content">
                                <div class="row">
                                  <p class="billable">Presupuesto Proyecto: </p><br/>
                                  <span
                                      class="" style="font-size:19px;">$<?php echo format_currency($totmi); ?></span>
                              </div>
                                <div class="row">
                                <p class="billable"><?php echo lang('label_project_expenses'); ?></p><br/>
                                <span
                                    class="" style="font-size:19px;">$0.00
                                    <!-- <?php echo round($total/6,2); ?> -->
                                  </span>
                                  </div>

                                  <div class="row">
                                    <p class="billable">Pagos: </p><br/>
                                    <span
                                        class="" style="font-size:19px;">$<?php $totalpaid=0; if(count($pagos)): foreach($pagos as $pago):
                                            $totalpaid = $totalpaid + $pago->amount;
                                            endforeach;endif;
                                            echo format_currency($totalpaid)?></span>
                                </div>

                                <div class="row">
                                  <p class="billable">Saldo: </p><br/>
                                  <span
                                      class="" style="font-size:19px;">$<?php echo format_currency($totpp-$totalpaid); ?></span>
                              </div>
                                <div class="row"></span>
                              </div>
                              </div>
                            </div>
                        </div>


                    </div>
                </div>
			</div>
		</div>

            </div>

        </div>
    </div>


    <div class="row overview-header">
        <center><h3 class="panel-heading project-info-bg no-radius">Presupuesto y Descripción del proyecto</h3></Center>

        <div class="col-sm-12">

            <?php echo $project_details->project_desc; ?>
			<div style="text-align:center;">
				<form action="<?php echo base_url(); ?>project/overview/import"
					method="post" name="upload_excel" enctype="multipart/form-data">
          <input type="hidden" class="form-control" name="project_id" value="<?php echo $project_id; ?>">
					<input class="button btn-info" type="file" name="file" id="file" required="">
					<br>
					<button class="button btn-info" type="submit" id="submit" name="import">Importar</button>
				</form>
			<br>
			<br>

			<div style="width:80%; margin:0 auto;" style="text-align:center;">
        <h3 style="text-align:center;">Presupuesto Proyecto</h3>
        <table class="table table-responsive table-border table-striped" id="t01">
          <tr class="info">
            <th style="text-align:center;">Número</th>
            <th style="text-align:center;">Concepto</th>
            <th style="text-align:center;">Importe</th>
            <th style="text-align:center;">Ejercido</th>
            <th style="text-align:center;">Saldo</th>
          </tr>
          <?php $totpp=0; if (count($milestones)): foreach ($milestones as $milestone): if ($milestone->newer != 1):?>
          <tr>
            <td style="text-align:center;"><?php echo $milestone->numero; ?></td>
            <td style="text-align:center;">
              <a data-toggle="zestModal"
                 href="<?php echo base_url('project/overview/view_modal_milestone/' . $project_id . '/' . $milestone->id); ?>"
                 title="">
                <?php echo $milestone->milestone_name; ?></a></td>
            <td style="text-align:center;">$<?php $totpp = $totpp + $milestone->importe; echo format_currency($milestone->importe); ?></td>
            <td style="text-align:center;">$<?php $importeavance=0;
            if(count($est_trans)): foreach($est_trans as $est_tran): if($est_tran->avance == $milestone->milestone_name):

                $importeavance = $importeavance + ($est_tran->amount);

            endif; endforeach; endif;

            echo format_currency($importeavance);
            $avancetotal = $avancetotal + $importeavance; ?></td>
            <td style="text-align:center;">$<?php $totpp = $totpp + $milestone->importe; echo format_currency($milestone->importe - $importeavance); ?></td>
          </tr>
        <?php endif; endforeach; endif; ?>
        </table>
        <div class="row">


                        <div class="row billing_top_row">
                            <div class="billing_settings">
                                <ul class="left_box">
    								<li class="revenue_month">
                                        <div style="padding-right: 5px;">
                                            <em>Presupuesto Inicial</em>
                                            <span
                                            class="text-danger">$<?php echo format_currency($totmi); ?></td></span>
                                        </div>
                                    </li>
                                    <li class="revenue_month">
                                                        <div style="padding-right: 5px;">
                                                            <em>Presupuesto extra</em>
                                                            <span
                                                            class="text-danger">$0.00</span>
                                                        </div>
                                                    </li>
                                                    <li class="revenue_month">
                                                                        <div style="padding-right: 5px;">
                                                                            <em>Presupuesto total</em>
                                                                            <span
                                                                            class="text-danger">$<?php echo format_currency($totmi); ?></span>
                                                                        </div>
                                                                    </li>
                                    <li class="revenue_today">
                                      <div style="padding-right: 5px;">
                                            <em>Gastos</em>
                                            <span
    											class="text-danger">$0.00
    										</span>
                                        </div>
                                    </li>
                                    <li class="revenue_today">
                                      <div style="padding-right: 5px;">
                                            <em>Saldo pendiente</em>
                                            <span
    											class="text-danger">$0.00
    										</span>
                                        </div>
                                    </li>
                                    <li class="revenue_today">
                                    <br>
                                    </li>


                                    <li class="revenue_year">
                                    <div style="padding-right: 5px;">
                                            <em>Pagos</em>
                                            <span
                                          class="text-danger"><?php echo format_currency($totalpaid);?></span>
                                        </div>
                                    </li>
                                    <li class="revenue_year">
                                    <div style="padding-right: 5px;">
                                            <em>Pendiente por pagar</em>
                                            <span
                                            class="text-danger"><?php echo format_currency($totmi-$totalpaid);?></span>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-sm-4 text-center">

    										<span class="chart chart-widget-pie widget-easy-pie-1" data-percent="<?php echo $total; ?>">
    											<span class="money_format"><?php echo $total; ?></span>
    										</span>
                            </div>
                        </div> -->


                    </div><a class="btn btn-info align-left" href="https://proyectosinternos.com/Taller5/project/overview/clean_pi/<?php echo $project_id ?>">Limpiar Presupuesto</a>
                    <h3 style="text-align:center;">Presupuesto Obra</h3>
			<table class="table table-responsive table-border table-striped" id="t01">
				<tr class="info">
					<th style="text-align:center;">Número</th>
					<th style="text-align:center;">Concepto</th>
					<th style="text-align:center;">Importe</th>
          <th style="text-align:center;">Ejercido</th>
          <th style="text-align:center;">Saldo</th>
				</tr>
			<?php $totales=0;
				if(isset($import)):
				foreach ($import as $data) {
					if($data->project_id == $project_id) {
			?>
			<tr class="tr-link" data-toggle="zestModal" data-link="<?php echo base_url('project/milestone/view/' . $data->project_id . '/' . $data->id); ?>">
				<td width="4%"style="text-align:center;"><?php echo round($data->numero, 2); ?>%</td>
				<td width="56%"style="text-align:center;"><?php echo $data->milestone_name; ?></td>
				<td width="20%"style="text-align:center;">$<?php $totales = $totales + $data->importe; echo format_currency($data->importe); ?></td>
<td width="20%"><?php $importeavance=0; if(count($avances)): foreach($avances as $avance): if($avance->item_avance == $data->milestone_name):  $importeavance = $importeavance + ($avance->price * $avance->quantity);
            endif; endforeach; endif;
            echo format_currency($importeavance);
            $avancetotal = $avancetotal + $importeavance; ?></td>
<td width="20%"style="text-align:center;">$<?php $totales = $totales + $data->importe; echo format_currency($data->importe - $importeavance); ?></td>
			</tr>
					<?php } } endif; ?>
			<tr class="success">
				<td></td>
				<td style="text-align:center;">Presupuesto inicial: </td>
        <td style="text-align:center;">$<?php echo format_currency($totpi); ?></td>
				<td>$<?php echo format_currency($avancetotal); ?></td>
				<td style="text-align:center;">$<?php echo format_currency($totpi - $avancetotal); ?></td>

			</tr>
</table>
	<h3 style="text-align:center;"> Extras </h3>
  <table class="table table-responsive table-border table-striped">
		<tr class="info">
          <th style="text-align:center;">Números</th>
          <th style="text-align:center;">Concepto</th>
          <th style="text-align:center;">Importe</th>
          <th style="text-align:center;">Ejercido</th>
          <th style="text-align:center;">Saldo</th>
        </tr>
        <?php $val=0; $paid=0; if (count($milestones)): foreach ($milestones as $milestone): if ($milestone->newer == 1):?>
        <tr>
          <td style="text-align:center;"><?php echo $milestone->numero; ?></td>
          <td style="text-align:center;"><?php echo $milestone->milestone_name; ?></td>
          <td style="text-align:center;">$<?php $val = $val + $milestone->importe;  echo format_currency($totpi); ?></td>
          <td style="text-align:center;"><?php $importeavance=0; if(count($avances)): foreach($avances as $avance): if($avance->item_avance == $milestone->milestone_name):  $importeavance = $importeavance + ($avance->price * $avance->quantity);
            endif; endforeach; endif;
            echo format_currency($importeavance);
            $avancetotal = $avancetotal + $importeavance; ?></td>
          <td style="text-align:center;">$<?php $val = $val + $milestone->importe;  echo format_currency($milestone->importe - $importeavance); ?></td>
        </tr>
      <?php endif; endforeach; endif; ?>


      <tr class="success">
        <td></td>
        <td style="text-align:center;">Presupuesto extra: </td>
        <td style="text-align:center;">$<?php echo format_currency($val); ?></td>
        <td style="text-align:center;">$<?php echo format_currency($paid); ?></td>
        <td style="text-align:center;">$<?php echo format_currency($val); ?></td>
      </tr>
      <?php $gtot =0; $gtot = $totpi +$val;  format_currency($totales +$val);
      $tot = $gtot * ($project_details->project_traslado/100);
      if ($project_details->project_honorario == 0){

          $tot2 = $project_details->project_cantidad * $totalsem;

      } else {
          $tot2 = $gtot * ($project_details->project_honorario/100);
      }



      $grandtot = $gtot + $tot + $tot2;   format_currency($grandtot);
     $iva = ($grandtot *.16);
     $coniva = ($grandtot * 1.16); ?>

      </table>
			</div>
		</div>
 </div>
</br></br></br></br></br></br>
		<div class="row" style="padding-top:30px;">


                    <div class="row billing_top_row" style="padding-top:30px;">
                        <div class="billing_settings">
                            <ul class="left_box">
								<li class="revenue_month">
                                    <div style="padding-right: 5px;">
                                        <em>Presupuesto Inicial</em>
                                        <span
                                        class="text-danger">$<?php echo format_currency($totpi); ?></span>
                                    </div>
                                </li>
                                <li class="revenue_month">
                                                    <div style="padding-right: 5px;">
                                                        <em>Presupuesto extra</em>
                                                        <span
                                                        class="text-danger">$<?php echo format_currency($val); ?></span>

                                                    </div>
                                                </li>
                                                <li class="revenue_month">
                                                    <div style="padding-right: 5px;">
                                                        <em>Traslados...</em>
                                                        <span
                                                        class="text-danger">$<?php echo format_currency($tot); ?></span>
                                                    </div>
                                                </li>
                                                <li class="revenue_month">
                                                    <div style="padding-right: 5px;">
                                                        <em>Honorarios...</em>
                                                        <span
                                                        class="text-danger">$<?php echo format_currency($tot2); ?></span>
                                                    </div>
                                                </li>
                                                <li class="revenue_month">
                                                                    <div style="padding-right: 5px;">
                                                                        <em>Presupuesto total</em>
                                                                        <span
                                                                        class="text-danger">$<?php echo format_currency($grandtot); ?></span>
                                                                    </div>
                                                                </li>
                                                                <li class="revenue_month">
                                                                    <div style="padding-right: 5px;">
                                                                        <em>I.V.A.</em>
                                                                        <span
                                                                        class="text-danger">$<?php echo format_currency($iva); ?></span>
                                                                    </div>
                                                                </li>
                                                                 <li class="revenue_month">
                                                                    <div style="padding-right: 5px;">
                                                                        <em>Total con I.V.A.</em>
                                                                        <span
                                                                        class="text-danger">$<?php echo format_currency($coniva); ?></span>
                                                                    </div>
                                                                </li>
                                <li class="revenue_today">
                                  <div style="padding-right: 5px;">
                                        <em>Gastos</em>
                                        <span
											class="text-danger">$0.00
										</span>
                                    </div>
                                </li>
                                <li class="revenue_today">
                                  <div style="padding-right: 5px;">
                                        <em>Saldo por ejercer</em>
                                        <span
											class="text-danger">$<?php echo format_currency($grandtot); ?>
										</span>
                                    </div>
                                </li>
                                 <li class="revenue_today">
                                  <div style="padding-right: 5px;">
                                        <em>Saldo por ejercer con I.V.A.</em>
                                        <span
											class="text-danger">$<?php echo format_currency($coniva); ?>
										</span>
                                    </div>
                                </li>
                                <li class="revenue_today">
                                <br>
                                </li>
                                <li class="revenue_year">
                                <div style="padding-right: 5px;">
                                        <em>Estimaciones</em>
                                        <span
                                      class="text-danger">$0.00</span>
                                    </div>
                                </li>
                                <li class="revenue_year">
                                <div style="padding-right: 5px;">
                                        <em>Pagos</em>
                                        <span
                                      class="text-danger">$0.00</span>
                                    </div>
                                </li>
                                <li class="revenue_year">
                                <div style="padding-right: 5px;">
                                        <em>Saldo pendiente</em>
                                        <span
                                      class="text-danger">$0.00</span>
                                    </div>
                                </li>
                                <li class="revenue_year">
                                <div style="padding-right: 5px;">
                                        <em>Pagos directos cliente</em>
                                        <?php
                                        $total_client = 0;
                                          if(count($invoices)): foreach($invoices as $invo): if($invo->paid == 4):
                                            $total_client += invoice_total($invo->id);
                                          endif; endforeach; endif;
                                        ?>

                                        <span class="text-danger">$<?php echo format_currency($total_client); ?></span>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-sm-4 text-center">

										<span class="chart chart-widget-pie widget-easy-pie-1" data-percent="<?php echo $total; ?>">
											<span class="money_format"><?php echo $total; ?></span>
										</span>
                        </div>
                    </div> -->


                </div>
				<!-- <div class="row">
					<div id="project_statistics">
					</div>
				</div> -->


    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <?php if (count($assigned_team)): foreach ($assigned_team as $team):
                    $user_details = users_details($team->user_id, 1);
                    ?>
                    <div class="col-sm-4">
                        <!-- BEGIN USER CARD LONG -->
                        <div class="the-box no-border contentarea">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="media user-card-sm">
                                        <div class="media-left">
                                            <a class="pull-left" href="#fakelink">
                                                <img class="media-object img-circle"
                                                     src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                                                     alt="Avatar">
                                            </a>
                                        </div>
                                        <?php
                                        $team_role = project_role_details($team->role);
                                        ?>
                                        <div class="media-body">
                                            <h4 class="media-heading"><?php echo $user_details->full_name; ?></h4>

                                            <p>
                                                <span
                                                    class="label label-info"><strong><?php echo $team_role->role_name; ?></strong></span>
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; endif ?>
            </div>

        </div>
    </div>
<div class="row">
  <table class="table table-responsive table-border table-striped">
    <tr class="info">
<th>Concepto</th>
<th>Semana 1</th>
<th>Semana 2</th>
<th>Semana 3</th>
<th>Semana 4</th>
    </tr>
    <tr><td>- DEMOLICIONES Y DESMONTAJES-</td><td></td><td><i class="fa fa-money"></i></td><td></td><td><i class="fa fa-check"></i></td></tr>
    <tr><td>- FIRMES Y PISOS -</td><td><i class="fa fa-check"></i></td><td><i class="fa fa-money"></i></td><td></td><td></td></tr>
    <tr><td>- AZOTEAS -</td><td></td><td></td><td></td><td></td></tr>
    <tr><td>- RECUBRIMIENTOS EN MUROS -</td><td></td><td><i class="fa fa-check"></i></td><td></td><td></td></tr>
    <tr><td>- ELEMENTOS DE OBRA CIVIL -</td><td></td><td></td><td><i class="fa fa-check"></i></td><td></td></tr>
    <tr><td>- INSTALACION HIDRAULICA -</td><td><i class="fa fa-check"></i></td><td></td><td></td><td></td></tr>
    <tr><td>- INSTALACION SANITARIA -</td><td></td><td><i class="fa fa-money"></i></td><td><i class="fa fa-check"></i></td><td><i class="fa fa-check"></i></td></tr>
    <tr><td>- INSTALACION DE GAS -</td><td></td><td></td><td></td><td></td></tr>
    <tr><td>- GRIFERIA Y ACCESORIOS -</td><td></td><td><i class="fa fa-check"></i></td><td></td><td></td></tr>
    <tr><td>- MUEBLES HIDROSANITARIOS -</td><td></td><td></td><td></td><td></td></tr>
    <tr><td>- INSTALACION ELECTRICA BAJA TENSION -</td><td></td><td></td><td></td><td></td></tr>
    <tr><td>- YESERIA -</td><td></td><td></td><td></td><td></td></tr>
    <tr><td>- ALUMINIO -</td><td></td><td></td><td></td><td></td></tr>
    <tr><td>- CARPINTERIA -</td><td></td><td><i class="fa fa-money"></i></td><td></td><td></td></tr>
    <tr><td>- COCINA -</td><td><i class="fa fa-check"></i></td><td><i class="fa fa-money"></i></td><td><i class="fa fa-money"></i></td><td></td></tr>
    <tr><td>- PINTURA -</td><td><i class="fa fa-money"></i></td><td><i class="fa fa-check"></i></td><td></td><td></td></tr>
    <tr><td>- OBRA FALSA -</td><td></td><td><i class="fa fa-money"></i></td><td><i class="fa fa-money"></i></td><td><i class="fa fa-check"></i></td></tr>
    <tr><td>- LIMPIEZA -</td><td><i class="fa fa-money"></i></td><td><i class="fa fa-check"></i></td><td><i class="fa fa-check"></i></td><td><i class="fa fa-check"></i></td></tr>




</div>



<div class="row">
    <div class="col-sm-12 contentarea">
        <div class="col-sm-12">
            <div class="the-box">

                <div class="panel panel-white no-radius">
                    <div class="panel-heading border-bottom">
                        <h3 class="box-title"><?php echo lang('label_project_activities'); ?></h3>
                    </div>
                    <div class="panel-body timeline-scroll">
                        <?php if (count($activities)) : ?>
                            <ul class="timeline-xs margin-top-15 margin-bottom-15">

                                <?php foreach ($activities as $activity): ?>
                                    <li class="timeline-item <?php echo $activity->activity_status; ?>">
                                        <div class="margin-left-15">
                                            <div
                                                class="text-muted text-small"> <?php echo ago(strtotime($activity->activity_date)) . " " . lang('ago'); ?> </div>
                                            <p>

                                            <div class="media-left">
                                                        <span class="activity-avatar avatar-xs">
                                                           <?php $user_details = users_details($activity->activity_user,
                                                               $activity->activity_user_type); ?>
                                                            <img
                                                                src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                                                                alt="...">
                                                        </span>
                                            </div>
                                            <div class="media-body">
                                                <div class="media-heading">
                                                    <a href=""
                                                       class="text-info strong"><?php echo activity_user($activity->activity_user_type,
                                                            $activity->activity_user); ?></a>
                                                    <small><span
                                                            class="text-off"><?php echo $activity->activity_date; ?></span>
                                                    </small>
                                                </div>
                                                <p>
                                                    <span
                                                        class="label label-<?php echo $activity->activity_status; ?>"></span>
                                                </p>

                                                <p>  <?php echo $activity->activity_event; ?></p>
                                            </div>


                                            </p>
                                        </div>
                                    </li>
                                <?php endforeach; ?>

                            </ul>
                        <?php else: ?>
                            <p>No project activities</p>
                        <?php endif; ?>
                    </div>

                </div>

            </div>
        </div>
        <!-- /.col-sm-6 -->


        <!-- /.col-sm-6 -->

    </div>
</div>
