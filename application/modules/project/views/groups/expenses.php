<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/05/20
 * Time: 08:59 AM
 */
?>
<div class="row">
    <div class="col-lg-12">
        <fieldset>
            <legend><i class="fa fa-money"></i><?php echo lang('label_expenses'); ?>
                <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2): ?>
                    <!-- <span class="pull-right"><a data-toggle="zestModal" class="btn btn-info btn-sm"
                                                href="<?php echo base_url('project/costs/expense_cost/' . $project_id); ?>">Agregar Gastos<i
                                class="fa fa-plus"></i></a></span> -->
                <?php endif; ?>
            </legend>

            <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table_cuenta">
                <thead>
                <tr>
                    <th style='width:15%' class="text-center">Fecha</th>
                    <th style='width:15%' class="text-center">Metodo de pago</th>
                    <th style='width:25%' class="text-center">Concepto/Proveedor</th>
                    <th>Referencia</th>
                    <th style='width:15%' class="text-center">Ingreso</th>
                    <th style='width:15%' class="text-center">Egreso</th>

                </tr>
                </thead>
                <tbody>
                <?php if (count($transactions)): foreach ($transactions as $transaction): ?>
                    <tr>
                        <td class="text-center">
                            <?php echo $transaction->created_at; ?>
                        </td>
                        <td class="text-center">
                            <?php if($transaction->paid != 0){ ?>
                            <span style="margin:0px;" class="label label-success"><?php echo payment_method($transaction->paid); ?></span>
                          <?php }else{ ?>
                            <span style="margin:0px;" class="label label-info">Sin metodo de pago</span>
                          <?php } ?>
                        </td>
                        <td class="text-center">
                              <a href="<?php echo base_url('client/view/' . $transaction->client_id); ?>"><?php echo client_company($transaction->client_id); ?></a>
                        </td>
                        <td> <a href="<?php echo base_url('billing/invoice/' . $transaction->id); ?>"><?php if($transaction->invoice_inv !=""){echo $transaction->invoice_inv;}
                        if($transaction->invoice_inv ==""){echo $transaction->inv_no;}

                        ?></a></td>
                        <td class="text-right">

                        </td>
                        <td class="text-right">

                            <?php echo format_currency(invoice_total($transaction->id)); ?>
                        </td>

                    </tr>
                <?php endforeach; endif; ?>
                <?php if (count($est_trans)) : foreach ($est_trans as $trans) : ?>
                <tr>
                    <td class="text-center">
                        <?php echo $trans->date; ?>
                    </td>
                    <td class="text-center">
                      <?php if($trans->account != null){ ?>
                        <span style="margin:0px;" class="label label-success"><?php echo payment_method($trans->account); ?></span>
                      <?php }else{ ?>
                        <span style="margin:0px;" class="label label-info">Sin metodo de pago</span>
                      <?php } ?>
                    </td>
                    <td class="text-center">
                        <?php echo client_company($trans->client_id); ?>
                    </td><td></td>
                    <td class="text-right">

                      <?php echo format_currency($trans->amount); ?>
                    </td>

                    <td class="text-right">
                        <!-- <div class="btn-group-action">
                            <div class="btn-group">
                                <a class=" btn btn-info"
                                   href="<?php echo base_url('project/expenses/view/' . $expense->cost_id . '/' . $project_id); ?>"
                                   title=""><i class="fa fa-eye"></i><?php echo lang('label_view'); ?></a>
                                <button type="button" class="btn btn-default dropdown-toggle"
                                        data-toggle="dropdown">
                                    <span class="caret">&nbsp;</span>
                                </button>
                                <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a data-toggle="zestModal"
                                               href="<?php echo base_url('project/expenses/edit_expense/' . $expense->cost_id . '/' . $project_id); ?>"
                                               title=""><i
                                                    class="fa fa-pencil-square-o"></i><?php echo lang('label_edit'); ?>
                                            </a></li>
                                        <li><a data-toggle="zestModal"
                                               href="<?php echo base_url('project/expenses/delete/' . $expense->cost_id . '/' . $project_id); ?>"
                                               title=""><i
                                                    class="fa fa-trash"></i><?php echo lang('label_delete'); ?></a>
                                        </li>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div> -->
                    </td>
                </tr>
              <?php  endforeach; endif;  ?>
                </tbody>

            </table>
            <div class="col-md-6">
            <table class="table  table-hover table-full-width table-th-block table-info">
              <thead>
                <tr>
                  <th colspan="2" class="text-center">Cuenta Corriente</th>

                </tr>
              </thead>
              <tbody>
                  <tr><td>Ingresos</td>
                    <td class="text-right">
                    <?php $totalcorriente2 = 0; if(count($est_trans)): foreach($est_trans as $corrient3): if($corrient3->method == 1): ?>
                      <?php $totalcorriente2 += $corrient3->amount;  ?>
                    <?php endif; endforeach; endif; ?>
                    <?php $grantotalc = $totalcorriente2;  ?>
                    <strong><h4>$<?php echo format_currency($grantotalc); ?></h4></strong>
                  </td>

                </tr>
                <tr>
                    <td>Egresos</td>
                  <td class="text-right">
                    <?php $grantotalc = 0; $totalcorriente = 0; if (count($transactions)): foreach ($transactions as $corriente): if($corriente->tax == 0):?>
                      <?php $totalcorriente += invoice_total($corriente->id) ?>
                    <?php endif; endforeach; endif; ?>
                    <?php $grantotalc = $totalcorriente;  ?>
                    <strong><h4>$<?php echo format_currency($grantotalc); ?></h4></strong>
                  </td>
                  </tr><tr>
                    <td>SALDO: </td>
                    <td class="text-right"><strong><h4><?php echo format_currency($totalcorriente2 - $totalcorriente); ?></h4></strong></td>

                </tr>

              </tbody>
            </table>
            </div>
            <div class="col-md-6">
            <table class="table  table-hover table-full-width table-th-block table-info">
              <thead>
                <tr>
                  <th colspan="2" class="text-center"> Espacio Diafano SA de CV </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                <td>Ingresos</td><td class="text-right"><strong><h4>$0.00</h4></strong></td></tr>
                  <td>Egresos</td>
                         <td class="text-right">
                    <?php $totaldiafano = 0; $grantotald = 0; if (count($transactions)): foreach ($transactions as $diafano): if($diafano->tax == 16):?>
                      <?php $totaldiafano += invoice_total($diafano->id); ?>
                    <?php endif; endforeach; endif; ?>
                    <?php $grantotald = $totaldiafano; ?>
                    <strong><h4>$<?php echo format_currency($grantotald); ?></h4></strong>
                  </td>



                </tr>
                <tr>
                    <td>SALDO: </td>
                    <td class="text-right"><strong><h4><?php echo format_currency(0 - $totaldiafano); ?></h4></strong></td>

                </tr>
              </tbody>
            </table>
            </div>
            <center><h3>Saldos</h3></center>
            <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table_un">
                <thead>
                <tr>

                    <th style='width:25%' class="text-center">Concepto/Proveedor</th>
                    <th style='width:25%' class="text-center">Total Gasto</th>


                </tr>
                </thead>
                <tbody>
                <?php $saldo=0; if (count($client)): foreach ($client as $cli): if($cli->client_type == 2 or $cli->client_type == 3): ?>
                    <tr>
                        <td class="text-center">
                            <?php echo $cli->client_name; ?>
                        </td>
                        <td class="text-rigth">
                          <?php if (count($transactions)): foreach ($transactions as $transaction): if($transaction->client_id == $cli->client_id):
                             $saldo = $saldo + invoice_total($transaction->id); ?>
                              <?php endif; endforeach; endif;
                      echo format_currency($saldo); $saldo=0;?> </td>
                    </tr>
                <?php  endif; endforeach; endif; ?>

                </tbody>

            </table>
            </div>
        </fieldset>
    </div>
</div>
