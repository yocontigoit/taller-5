<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 4/6/2015
 * Time: 2:06 PM
 */
?>

<!-- Task detailed -->
<div class="row">
    <div class="col-lg-8">
        <?php $user_details = get_user_details($task_details->user_id); ?>

        <div class="block">
            <h3><strong class="text-danger" style="color:#828282;" ><?php echo $task_details->task_name; ?></strong></h3>
            <h5><strong class="text-danger" style="color:#828282;" >Avance: - Carpinteria -</strong></h5>
            <ul class="headline-info">
                <li><?php echo lang('label_client_visible'); ?>: <a
                        href="#"><?php echo ($task_details->task_visible == 1) ? "Visible" : "Oculta"; ?></a></li>
                <li><?php echo lang('label_status'); ?>: <span
                        class="text-semibold text-success"><?php echo get_task_status_name($task_details->status_task);//On hold ?></span>
                </li>
                <li><?php echo lang('label_added_by'); ?>: <a
                        href="#"><?php echo $user_details->emp_name . " " . $user_details->emp_surname;//Monica ?></a>
                </li>


                <?php if ($this->CompanyUserRolePermission_id == 1): ?>
                    <li class="pull-right">

                        <div class="btn-group pull-left">
                            <a class="btn text-danger" style="color:#828282;" data-toggle="zestModal"
                               href="<?php echo base_url('project/tasks/delete_task/' . $project_id . '/' . $task_details->task_id); ?>"><i
                                    class="fa fa-trash-o fa-lg"></i></a>
                        </div>
                        <div class="btn-group pull-left">
                            <a class="btn text-danger" style="color:#828282;" data-toggle="zestModal"
                               href="<?php echo base_url('project/tasks/edit_task/' . $project_id . '/' . $task_details->task_id); ?>"><i
                                    class="fa fa-pencil-square-o  fa-lg"></i></a>
                        </div>
                    </li>
                <?php endif; ?>


            </ul>
            <div class="row">
                <div class="col-md-12">

                        <?php $task_logged_hours = task_logged_hours($task_details->task_id);

                        if ($task_logged_hours > 0 && $task_details->task_est_hours > 0) {
                            $percentage = round(($task_logged_hours / $task_details->task_est_hours) * 100);
                        } else {
                            $percentage = 0;
                        }
                        ?>



                    <!-- /.col-sm-4 -->

                    <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="summary">
                                        <p class="billable">Horas</p><br/>
                                        <span class="text-danger" style="color:#828282;"><?php echo $task_details->task_est_hours; ?></span>
                                        <span class="unit"><?php echo lang('hours'); ?></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="summary">
                                        <p>Días</p><br/>
                                        <span class="text-danger" style="color:#828282;"><?php echo $task_details->task_duration; ?></span>
                                        <span class="unit"><?php echo lang('days'); ?></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="summary">
                                        <p class="billable"><?php echo lang('label_logged_hours'); ?></p><br/>
                                        <span class="text-danger" style="color:#828282;"><?php echo round($task_logged_hours); ?></span> <span
                                            class="unit"><?php echo lang('hours'); ?></php></span>
                                    </div>

                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <?php if ($task_details->task_use_checklist && $this->CompanyUserRolePermission_id == 1) { ?>
                <div class="panel panel-info">
                      <ul class="ver-inline-menu tabbable">
                          <li>
                              <a href="#" class="add_new_checklist"><i class="fa fa-plus-square-o"></i>
                                  <?php echo lang('label_add_check_list'); ?></a>
                          </li>
                      </ul>

                  </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="checklist-items">

                        </div>
                    </div>
                </div>
            <?php } ?>
            <hr/>

            <h6><?php echo lang('label_description'); ?>:</h6>

            <p>
                <?php echo e($task_details->task_description); ?>
            </p>

            <hr/>
<div class="row block-inner">
                <div class="col-sm-12">
                    <div class="well">
                        <dl>
                            <dt class="text-info"><?php echo lang('label_requirements'); ?>:</dt>
                            <p>
                                <?php echo e($task_details->task_requirements); ?>
                            </p>
                        </dl>
                    </div>
                </div>
            </div>

            <div class="panel-footer">
                <div class="pull-left">

                    <ul class="footer-links-group">
                        <li><i class="fa fa-plus-circle muted"></i><?php echo lang('label_added_on'); ?>: <a href="#"
                                                                                                             class="text-semibold"><?php echo readable_date_format($task_details->date_added);//Sep 13, 2013< ?></a>
                        </li>
                        <li><i class="fa fa-check-circle muted"></i><?php echo lang('label_due_by'); ?>: <a href="#"
                                                                                                            class="text-semibold"><?php echo readable_date_format(date_from_timestamp($task_details->task_end / 1000));//Sep 13, 2013< ?></a>
                        </li>
                        <li class="has-label"><span
                                class="label label-success"><?php echo $task_details->task_progress; ?>
                                % <?php echo lang('label_complete'); ?></span></li>

                    </ul>
                </div>


            </div>

        </div>
        <!-- /task description -->


        <h6 class="heading-hr"><i class="fa fa-comments"></i><?php echo lang('label_comments'); ?>
            <span class="pull-right">
                <a id="show_main-reply" title="Reply comment"
                   class="btn btn-xs btn-danger btn-square" style="background:#828282;border-width:0px;color:#fff;"><?php echo lang('label_comment'); ?><i
                        class="fa fa-reply"></i></a>
            </span>
        </h6>

        <!-- Add comment form -->
        <div id="main-reply" class="block" style="display:none;">
            <h5><i class="fa fa-comments"></i><?php echo lang('label_add_comment'); ?></h5>

            <div class="well">
                <?php echo form_open('project/tasks/add_task_comment/' . $task_details->task_id,
                    array('role' => 'form')); ?>

                <input type="hidden" name="project_id" class="form-control"
                       value="<?php echo $task_details->project_id; ?> ">

                <div class="form-group">
                    <label><?php echo lang('label_comment'); ?>:</label>
                    <textarea class="summernote-sm" name="task_message"></textarea>
                </div>

                <div class="form-actions text-right">
                    <button type="submit" class="btn btn-success"><i class="fa fa-rocket"></i> Send</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- /add comment form -->
        <ul class="media-list media-sm media-dotted">

            <?php if (count($task_comments)): foreach ($task_comments as $comment) : ?>
                <?php $user_details = users_details($comment->message_by_id, 1); ?>
                <li class="media">
                    <a class="pull-left" href="#fakelink">
                        <img class="media-object img-circle"
                             src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                             alt="Avatar">
                    </a>

                    <div class="media-body" style="width: 10000px;">
                        <h4 class="media-heading"><a href="#fakelink"><?php echo $user_details->full_name; ?></a></h4>

                        <p class="date">
                            <small><?php echo ago(strtotime($comment->message_date)); ?></small>
                        </p>
                        <?php echo $comment->message_text; ?>
                        <p class="comment-action">

                            <a href="#<?php echo $comment->message_id; ?>_conversation-reply" data-toggle="collapse"
                               title="Reply comment"
                               class="btn btn-xs btn-info btn-square"><?php echo lang('label_reply'); ?><i
                                    class="fa fa-reply"></i></a>
                        </p>

                        <div id="<?php echo $comment->message_id; ?>_conversation-reply"
                             class="comment-action collapse">
                            <?php echo form_open('project/tasks/add_task_reply/' . $comment->message_id,
                                array('role' => 'form')); ?>
                            <input type="hidden" name="project_id" class="form-control"
                                   value="<?php echo $task_details->project_id; ?> ">
                            <input type="hidden" name="task_id" class="form-control"
                                   value="<?php echo $task_details->task_id; ?> ">

                            <div class="form-group">
                                <textarea name="message_reply" placeholder="Your message..."
                                          class="summernote-sm form-control"></textarea>
                            </div>
                            <div class="form-actions text-right">
                                <input type="submit" value="Add comment" class="btn btn-primary">
                            </div>
                            <?php echo form_close(); ?>
                        </div>


                        <?php
                        $comment_conversations = get_task_conversation($comment->message_id);

                        ?>


                        <?php if (!empty($comment_conversations)) : ?>

                            <ul class="media-list">
                                <?php foreach ($comment_conversations as $conversations) : ?>
                                    <?php $user_details = users_details($conversations->replied_by, 1); ?>
                                    <li class="media">
                                        <a class="pull-left" href="#fakelink">
                                            <img class="media-object img-circle"
                                                 src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                                                 alt="Avatar">
                                        </a>

                                        <div class="media-body">
                                            <h4 class="media-heading"><a
                                                    href="#fakelink"><?php echo $user_details->full_name; ?></a></h4>

                                            <p class="date">
                                                <small><?php echo ago(strtotime($conversations->reply_date)); ?></small>
                                            </p>
                                            <?php echo $conversations->reply_message; ?>

                                        </div>
                                    </li>

                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </li>

            <?php endforeach; endif; ?>


        </ul>


    </div>

    <div class="col-lg-4">
        <!-- Attached files -->


        <!-- Timer -->
        <?php if ($task_details->task_timer == 1) { ?>


        <div class="panel panel-success">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="fa fa-clock-o"></i><?php echo lang('label_task_timer'); ?></h6>
            </div>

            <div class="panel-body">
                <ul class="timer-weekdays">
                    <li><a href="#" class="label label-<?php echo (date('D',
                                time()) === 'Mon') ? 'success' : 'default' ?>">Lun</a></li>
                    <li><a href="#" class="label label-<?php echo (date('D',
                                time()) === 'Tue') ? 'success' : 'default' ?>">Mar</a></li>
                    <li><a href="#" class="label label-<?php echo (date('D',
                                time()) === 'Wed') ? 'success' : 'default' ?>">Mier</a></li>
                    <li><a href="#" class="label label-<?php echo (date('D',
                                time()) === 'Thu') ? 'success' : 'default' ?>">Jue</a></li>
                    <li><a href="#" class="label label-<?php echo (date('D',
                                time()) === 'Fri') ? 'success' : 'default' ?>">Vier</a></li>
                    <li><a href="#" class="label label-<?php echo (date('D',
                                time()) === 'Sat') ? 'success' : 'default' ?>">Sab</a></li>
                    <li><a href="#" class="label label-<?php echo (date('D',
                                time()) === 'Sun') ? 'success' : 'default' ?>">Dom</a></li>
                </ul>

                <ul class="timer dashboard-timer">

                    <li class="timer-clock" style="font-size:40px;">
                        <em> 00:00:00</em>
                    </li>
                </ul>
            </div>

            <div class="panel-footer">
                <div class="pull-left">
                    <span><i
                            class="fa fa-clock-o"></i><?php echo round($task_logged_hours); ?><?php echo lang('hours'); ?></span>
                </div>
                <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) { ?>
                    <div class="pull-right">
                        <ul class="footer-icons-group start-stop-div">

                            <li><a class="countdown-timer" href="#" id="start-timer"><i class="fa fa-play"></i></a></li>

                        </ul>
                    </div>
                <?php } ?>
            </div>
        </div>
        <!-- /timer -->
<?php } ?>

        <!-- Attached files -->
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="fa fa-link"></i><?php echo lang('label_attached_files'); ?>
                    <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) { ?>
                        <span class="pull-right">
                        <a class="text-danger" style="color:#828282;" data-toggle="zestModal"
                           href="<?php echo base_url('project/documents/upload/' . $project_id . '/' . $task_details->task_id); ?>">
                            <i class="fa fa-paperclip fa-lg"></i>
                        </a>
                         </span>
                    <?php } ?>
                </h6>

            </div>
            <ul class="list-group">
                <?php if (count($task_attachments)): foreach ($task_attachments as $attachment): ?>
                    <li class="list-group-item has-button"><i
                            class="fa fa-file-<?php echo $attachment->file_type; ?>-o"></i> <a
                            href="<?php echo base_url('project/documents/preview_document/' . $attachment->attachment_id); ?>"><?php echo $attachment->file_name; ?></a> <a
                            href="<?php echo base_url('project/documents/download/' . $attachment->attachment_id); ?>"
                            class="btn btn-link btn-icon"><i class="fa fa-download"></i></a></li>
                <?php endforeach; endif; ?>
            </ul>
        </div>
        <!-- /attached files -->


        <!-- Assigned users -->
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="fa fa-users"></i><?php echo lang('label_assigned_users'); ?></h6>
            </div>
            <ul class="message-list media-list">
                <?php if (count($task_assigments)): foreach ($task_assigments as $assignment):
                    $user_details = users_details($assignment->user_id, 1);
                    ?>
                    <a class="pull-left" href="<?php echo base_url('user/edit/' . ($assignment->user_id * 99)); ?>">

                    <li class="media user-card-sm">
                        <div class="clearfix">
                            <img class="media-object img-circle"
                                     src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                                     alt="Avatar">


                            <div class="media-body">
                                <h4 class="media-heading"><?php echo $user_details->full_name; ?></h4>
                            </div>
                            <div class="right-button pull-right">
                                <a data-toggle="zestModal" class="btn btn-link btn-icon btn-sm"
                                   href="<?php echo base_url('user/send_message/' . $assignment->user_id); ?>"><i
                                        class="fa fa-comments"></i> </a>

                            </div>
                        </div>
                    </li>
                      </a>
                <?php endforeach; endif; ?>

            </ul>
        </div>
        <!-- /assigned users -->

    </div>
</div>
<!-- /task detailed -->
<script>
    task_id = <?php echo $task_details->task_id ?>;
    use_check_list = '<?php echo ($task_details->task_use_checklist == 1) ? "true" : "false" ?>';
</script>
