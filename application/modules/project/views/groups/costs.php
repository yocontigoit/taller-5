<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/03/10
 * Time: 10:31 AM
 */
?>
<fieldset>

    <legend><i class="fa fa-dollar"></i>Costos Hora Hombre
        <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2): ?>
            <span class="pull-right">

                                    <button data-toggle="zestModal" class="btn btn-info btn-sm"
                                       href="<?php echo base_url('project/costs/additional_cost/' . $project_id); ?>">Agregar Costo
                                        <i class="fa fa-plus"></i>
                                    </button>

             </span>
        <?php endif; ?>
    </legend>
    <div class="row overview-header">

    </div>
    <div class="row">
        <div id="" class="col-sm-12">

            <div class="table-responsive">


                <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table">
                    <thead class="the-box dark full">
                    <tr>
                        <h2>Tiempo/costo por participante</h2>
                    </tr>
                    <tr>
                        <th align="center"><?php echo lang('label_assigned_users'); ?></th>
                        <th align="center"><?php echo lang('label_estimate_hours'); ?></th>
                        <th align="center"><?php echo lang('label_done'); ?></th>
                        <th align="center"><?php echo lang('label_hourly_cost'); ?></th>
                        <th align="center"><?php echo lang('label_total_estimate'); ?></th>
                        <th align="center"><?php echo lang('label_total_done'); ?></th>
                        <th align="center">
                            <?php echo lang('label_cost_delta'); ?>

                        </th>

                    </tr>

                    </thead>
                    <tbody>
                    <?php if (count($assigned_team)): foreach ($assigned_team as $team):
                        $user_details = users_details($team->user_id, 1);
                        ?>
                        <tr>
                            <td align="center">
                                <a style="color:#656d78;" href="https://proyectosinternos.com/Taller5/user/edit/<?php echo $team->user_id * 99; ?>"><?php echo $user_details->full_name; ?></a>
                            </td>
                            <td align="center">
                                <?php echo resource_planned_effort($project_id, $team->user_id); ?>
                            </td>
                            <td align="center">
                                <?php echo resource_completed_effort($project_id, $team->user_id); ?>
                            </td>
                            <td align="center" ><?php echo $team->hourly_cost; ?></td>
                            <td align="center" ><?php echo resource_planned_effort($project_id,
                                        $team->user_id) * resource_completed_effort($project_id,
                                        $team->user_id); ?></td>
                            <td align="center"
                                style="color: rgb(182, 30, 45);"><?php echo($team->hourly_cost * resource_completed_effort($project_id,
                                        $team->user_id)); ?></td>

                            <td align="center" style="color: rgb(182, 30, 45);"><?php echo resource_planned_effort($project_id,
                                        $team->user_id) - resource_completed_effort($project_id,
                                        $team->user_id); ?></td>
                        </tr>

                    <?php endforeach; endif ?>
                </table>

                <br>
                <br>
                <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table">
                    <tr>
                        <h2 class="sectionTitle">Costos de Proyecto</h2>
                    </tr>
                    <thead class="the-box dark full">
                    <tr>
                        <th align="center"><?php echo lang('label_description'); ?></th>
                        <th align="center"><?php echo lang('label_classification'); ?></th>
                        <th align="center"><?php echo lang('label_date'); ?></th>
                        <th align="center"><?php echo lang('label_estimate_cost'); ?></th>
                        <th align="center"><?php echo lang('label_real_cost'); ?></th>

                        <th align="center">
                        </th>

                    </tr>

                    </thead>
                    <tbody>
                    <?php
                    $estimate_costs_total = 0;
                    $actual_costs_total = 0;

                    ?>
                    <?php if (count($additional_costs)): foreach ($additional_costs as $costs): ?>
                        <tr>
                            <td align="center"><?php echo $costs->description; ?></td>
                            <td align="center"><?php echo get_category_name($costs->classification); ?></td>
                            <td align="center"><?php echo $costs->date_entry; ?>  </td>
                            <td align="center">$<?php echo number_format($costs->estimate_cost,2); ?></td>
                            <td align="center">$<?php echo number_format($costs->real_cost,2); ?></td>

                            <td width="20" align="center">
                                <button class="btn btn-danger remove_cost" style="background:#828282;border-width:0px;" data-id="<?php echo $costs->cost_id; ?>"><i
                                        class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                        <?php
                        $estimate_costs_total += $costs->estimate_cost;
                        $actual_costs_total += $costs->real_cost;
                        ?>
                    <?php endforeach; endif; ?>
                    <?php if (count($expense_costs2)): foreach ($expense_costs2 as $costs): ?>
                        <tr>
                            <td align="center"><?php echo $costs->description; ?></td>
                            <td align="center"><?php echo get_category_name($costs->classification); ?></td>
                            <td align="center"><?php echo $costs->date_entry; ?>  </td>
                            <td align="center">$<?php echo number_format($costs->estimate_cost,2); ?></td>
                            <td align="center">$<?php echo number_format($costs->real_cost,2); ?></td>

                            <td width="20" align="center">
                                <button class="btn btn-danger remove_cost" style="background:#828282;border-width:0px;" data-id="<?php echo $costs->cost_id; ?>"><i
                                        class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                        <?php
                        $estimate_costs_total += $costs->estimate_cost;
                        $actual_costs_total += $costs->real_cost;
                        ?>
                    <?php endforeach; endif; ?>
                    <tr>
                        <td colspan="3">Total</td>
                        <td align="center">$<?php echo number_format($estimate_costs_total,2); ?></td>
                        <td align="center">$<?php echo number_format($actual_costs_total,2); ?></td>
                        <td align="center"></td>
                    </tr>

                </table>

                <br>
                <br>
                <table class="table" style="background-color: #fff;">
                    <tbody>
                    <tr class="data">
                        <td style="padding: 10px;">

                        </td>
                        <td align="right">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td align="center"></td>
                                    <td align="right">presupuesto</td>
                                    <td align="center"></td>
                                    <td align="right">Costos</td>
                                    <td align="center"></td>
                                    <td align="right">Restante</td>
                                </tr>

                                <tr style="font-size: 18px; ">
                                    <td align="center">Total: </td>
                                    <td align="right"><?php echo number_format($project_details->project_budget,
                                            2); ?></td>
                                    <td align="center">-</td>
                                    <td align="right"><?php echo number_format(project_total_done_costs($project_id),
                                            2); ?></td>
                                    <td align="center">=</td>
                                    <td align="right"><?php echo number_format(($project_details->project_budget - project_total_done_costs($project_id)),
                                            2); ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>

        </div>
    </div>
</fieldset>
