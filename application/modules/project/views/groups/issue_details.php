<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/17
 * Time: 08:44 AM
 */
?>
<div class="row">
    <div class="col-lg-12">
        <fieldset>
            <legend><i class="fa fa-bug"></i>Details
                <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) { ?>
                    <span class="btn-group pull-right">

                          <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-tag"></i>
                              Cambiar estatus
                              <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu">

                              <?php
                              $ticket_status = ticket_status();
                              if (count($ticket_status)):foreach ($ticket_status as $status): ?>

                                  <li>
                                      <a href="<?php echo base_url('project/issues/update_status/' . $issue_details->issue_id . '/' . $status->status_id); ?>"><?php echo ucfirst(strtolower($status->status_name)); ?></a>
                                  </li>
                              <?php endforeach; endif ?>
                          </ul>


                    </span>
                <?php } ?>
            </legend>
            <div class="col-lg-3">
                <!--Ticket details -->
                <div class="box">
                    <!-- BEGIN PANEL-->
                    <div class="panel panel-inf panel-square panel-no-border text-center">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?php echo lang('label_issue_details'); ?></h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered table-hover table-extra-padd">
                                <tbody>
                                <tr>
                                    <td class="col-sm-4"><?php echo lang('label_issue_no'); ?></td>
                                    <td class="col-sm-8"><?php echo $issue_details->issue_id; ?></td>
                                </tr>
                                <tr>
                                    <td class="col-sm-4"><?php echo lang('label_date'); ?>:</td>
                                    <td class="col-sm-8"><?php echo $issue_details->issue_date; ?></td>
                                </tr>

                                <tr>
                                    <td class="col-sm-4"><?php echo lang('label_status'); ?>:</td>

                                    <?php $status = get_ticket_status($issue_details->issue_status);

                                    if ($status == 'open') {
                                        $label = 'info';
                                    } elseif ($status == 'closed') {
                                        $label = 'success';
                                    } elseif ($status == 'answered') {
                                        $label = 'warning';
                                    } else {
                                        $label = 'info';
                                    }

                                    ?>
                                    <td class="col-sm-8"><span
                                            class="label label-<?php echo $label; ?>"><?php echo $status; ?></span></td>
                                </tr>
                                <tr>
                                    <td class="col-sm-4"><?php echo lang('label_issue_assigned_to'); ?>:</td>
                                    <td class="col-sm-8"><span
                                            class="label label-info"><?php echo users_details($issue_details->user_id,
                                                1)->full_name;; ?></span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel panel-success panel-block-color -->
                    <!-- END PANEL -->

                </div>
                <!--end details-->
            </div>

            <div class="col-lg-9">

                <ul class="support-timeline">
                    <li class="centering-line"></li>
                    <li class="item-timeline highlight post-form-timeline">
                        <!-- text-center -->
                        <div class="buletan"></div>
                        <?php $poster_details = users_details($issue_details->issue_by, 1); ?>
                        <div class="inner-content">
                            <div class="heading-timeline">
                                <img style="width: 80px; height: 80px;" ; alt="Avatar" class="avatar"
                                     src="<?php echo base_url('files/profile_images/' . $poster_details->avatar); ?>">

                                <div class="user-timeline-info">
                                    <p style="margin-left:60px; margin-bottom: 5px;">
                                        <?php echo $poster_details->full_name; ?>
                                        <small><?php echo $issue_details->entry_date; ?></small>
                                        <br/>
                                    </p>

                                </div>
                                <!-- /.user-timeline-info -->
                            </div>
                            <!-- /.heading-timeline -->
                            <br/><br/>

                            <?php echo $issue_details->issue_description; ?>


                        </div>
                        <!-- /.inner-content -->

                    </li>

                    <li class="item-timeline highlight">
                        <div class="buletan"></div>
                        <div class="inner-content">
                            <!--<form style="margin-top: -1px" role="form">-->
                            <?php echo form_open('project/issues/reply_issue', array('id' => 'create_reply')); ?>
                            <input type="hidden" name="project_id" class="form-control"
                                   value="<?php echo $issue_details->project_id; ?> ">
                            <input class="form-control" type="hidden" readonly name="issue_id"
                                   value="<?php echo $issue_details->issue_id; ?>"/>

                            <p>
                                <textarea class="summernote-sm" name="reply_message"></textarea>
                            </p>

                            <p class="text-right">
                                <button class="btn btn-info btn-sm" type="submit">reply</button>
                            </p>
                            </form>
                        </div>
                        <!-- /.inner-content -->

                    </li>

                    <?php if (count($issue_replies)): foreach ($issue_replies as $issue_reply): ?>
                        <li class="item-timeline post-form-timeline">
                            <div class="buletan"></div>
                            <div class="inner-content">
                                <!-- BEGIN HEADINg TIMELINE -->
                                <?php $user_details = users_details($issue_reply->reply_user_id,
                                    $issue_reply->reply_user_type); ?>
                                <div class="heading-timeline">
                                    <img alt="Avatar" class="avatar"
                                         src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>">

                                    <div class="user-timeline-info">
                                        <p>
                                            <?php echo $user_details->full_name; ?>
                                            <small><?php echo $issue_reply->reply_date; ?></small>
                                        </p>

                                    </div>
                                    <!-- /.user-timeline-info -->
                                </div>
                                <!-- /.heading-timeline -->
                                <!-- END HEADINg TIMELINE -->

                                <?php echo $issue_reply->reply_message; ?>


                                <!-- BEGIN FOOTER TIMELINE -->
                                <div class="footer-timeline">
                                    <ul class="timeline-option">
                                        <li class="option-row">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <ol>
                                                        <li></li>
                                                        <li></li>
                                                    </ol>
                                                </div>
                                                <!-- /.col-xs-6 -->
                                                <div class="col-xs-6 text-right">
                                                    <ol>
                                                        <li><a href="#fakelink"><i
                                                                    class="fa fa-clock-o"></i><?php echo ago(strtotime($issue_reply->reply_date)); ?>
                                                            </a></li>
                                                        <li><a href="#fakelink"><?php echo lang('ago'); ?></a></li>

                                                    </ol>
                                                </div>
                                                <!-- /.col-xs-6 -->
                                            </div>
                                            <!-- /.row -->
                                        </li>

                                    </ul>
                                </div>
                                <!-- /.footer-timeline -->
                                <!-- END FOOTER TIMELINE -->
                            </div>
                            <!-- /.inner-content -->
                        </li>
                    <?php endforeach;endif; ?>
                </ul>

            </div>
        </fieldset>
    </div>
</div>
