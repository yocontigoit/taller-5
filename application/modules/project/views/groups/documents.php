<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/10/30
 * Time: 11:18 AM
 */
?>
<fieldset>
    <legend><i class="fa fa-paperclip"></i><?php echo lang('label_documents'); ?>
        <span class="pull-right"><button data-toggle="zestModal" class="btn btn-info btn-sm" style="color:#ffffff;"
                                    href="<?php echo base_url('project/documents/upload/' . $project_id); ?>">Agregar Documento  <i
                    class="fa fa-paperclip"></i></button></span>

    </legend>
    <div class="row mix-grid">

        <?php if (count($documents_details)): foreach ($documents_details as $details) : ?>
            <div class="col-sm-4 col-md-3">
                <!-- Begin assets item -->
                <div class="thumbnail media-lib-item">
                    <div class="checklist-left">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="" class="i-grey-flat">
                            </label>
                        </div>
                    </div>
                    <div class="more-dropdown">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-cog"></i>
                            </button>
                            <ul class="dropdown-menu pull-right square" role="menu">

                                <li>
                                    <a href="<?php echo base_url('project/documents/download/' . $details->attachment_id); ?>"><i
                                            class="fa fa-download"></i><?php echo lang('label_download'); ?></a></li>
                                <li><a data-toggle="zestModal"
                                       href="<?php echo base_url('project/documents/file_edit/' . $details->attachment_id); ?>"><i
                                            class="fa fa-pencil-square-o"></i><?php echo lang('label_edit'); ?></a></li>
                                <li>
                                    <a href="<?php echo base_url('project/documents/delete/' . $details->attachment_id); ?>"><i
                                            class="fa fa-trash-o"></i><?php echo lang('label_delete'); ?></a></li>
                            </ul>
                        </div>
                    </div>

                    <?php if ($details->file_type == ".pdf" || $details->file_type == ".doc" || $details->file_type == ".docx"
                    || $details->file_type == ".txt" || $details->file_type == ".xls"){ ?>

                    <a data-toggle="zestModal"
                       href="<?php echo base_url('project/documents/preview_document/' . $details->attachment_id); ?>">
                        <img src="<?php echo base_url('assets/images/img-file/document-file.jpg'); ?>"
                             style="height:150px;" alt="...">

                        <?php }elseif ($details->file_type == ".zip" || $details->file_type == ".rar"){ ?>

                        <a href="<?php echo base_url('files/project_files/' . $details->path . $details->file_name); ?>"
                           class="magnific-img">
                            <img src="<?php echo base_url('assets/images/img-file/document-file.jpg'); ?>"
                                 style="height:150px;" alt="...">

                            <?php } else { ?>
                                <a href="<?php echo base_url('files/project_files/' . $details->path . $details->file_name); ?>"
                                   class="magnific-img">
                                    <img
                                        src="<?php echo base_url('files/project_files/' . $details->path . $details->file_name); ?>"
                                        style="height:150px;" alt="...">
                                </a>
                            <?php } ?>
                            <div class="caption text-center">
                                <p class="small"><a href="#fakelink"><?php echo $details->file_name; ?></a><br/>
                                    <span
                                        class="small text-muted"><?php echo readable_date_format($details->upload_date); ?></span>
                                </p>
                            </div>
                            <!-- /.caption text-center -->
                </div>
                <!-- /.thumbnail media-lib-item -->
                <!-- End assets item -->
            </div><!-- /.col-sm-4 col-md-3 -->
        <?php endforeach; endif ?>

    </div>
    <!-- /.row -->

</fieldset>
