<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/10/24
 * Time: 11:59 AM
 */
?>

<div class="row">
    <div class="col-lg-12">
        <fieldset>
            <legend><i class="fa fa-calendar-o"></i>Tareas
                <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2): ?>
                    <div class="btn-group pull-right">
                        <a class="btn btn-info btn-sm"   data-toggle="zestModal"
                           href="<?php echo base_url('project/tasks/add_task/' . $project_id); ?>">
                            <i class="fa fa-plus"></i>
                            Agregar tarea
                        </a>
                    </div>
                <?php endif; ?>
            </legend>

            <div class="panel">
                <!--<div class="panel-heading">-->
                <div class="tabbable page-tabs panel-success">
                    <ul class="nav nav-tabs nav-settings">
                        <li class="<?php echo ($active_page == 'all') ? "active" : ""; ?>"><a
                                href="<?php echo base_url('project/tasks/preview/' . $project_id . '/?status=all'); ?>"><i
                                    class="fa fa-sign-in"></i> Todas</a></li>
                        <li class="<?php echo ($active_page == 'active') ? "active" : ""; ?>"><a
                                href="<?php echo base_url('project/tasks/preview/' . $project_id . '/?status=active'); ?>"><i
                                    class="fa fa-sign-in"></i> Activas</a></li>
                        <li class="<?php echo ($active_page == 'closed') ? "active" : ""; ?>"><a
                                href="<?php echo base_url('project/tasks/preview/' . $project_id . '/?status=closed'); ?>"><i
                                    class="fa fa-sign-in"></i> Terminadas</a></li>
                    </ul>
                    <!-- </div>-->

                    <div>

                        <?php echo ($_TASK_PAGE) ? $_TASK_PAGE : null; ?>
                    </div>
                </div>
                <!-- /page tabs -->
            </div>
        </fieldset>
    </div>
</div>
