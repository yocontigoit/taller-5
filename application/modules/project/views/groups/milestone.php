<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 4/23/2015
 * Time: 11:13 PM
 */
?>
<fieldset>
    <legend><i class="fa fa-map-signs"></i><?php echo lang('label_milestone'); ?>
        <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2): ?>
            <span class="pull-right"><button data-toggle="zestModal" class="btn btn-info btn-sm" style="color:#ffffff;"
                                        href="<?php echo base_url('project/milestone/add_concepto/' . $project_id); ?>"><i
                         class="fa fa-bars"></i>Agragar Etapa  </button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <button data-toggle="zestModal" class="btn btn-info btn-sm" style="font-size:12px;color:#ffffff;"
                                        href="<?php echo base_url('project/milestone/add_project/' . $project_id); ?>"><i
                        class="fa fa-plus"></i> Agregar Avance </button>    </span>
        <?php endif; ?>
    </legend>


    <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table">
        <thead>
        <tr>
            <th style="text-align:center"><?php echo lang('label_progress'); ?></th>
            <th style="text-align:center"><?php echo lang('label_milestone_name'); ?></th>
            <th style="text-align:center"><?php echo lang('label_start_date'); ?></th>
            <th style="text-align:center"><?php echo lang('label_options'); ?></th>
        </tr>
        </thead>
        <tbody>
          <!-- <?php if (count($concepts)): foreach ($concepts as $milestone): ?>
              <tr>
                  <td style="text-align:center">

                      <?php $percentage = 0; ?>
                      <div class="chart milestone-progress" style="margin: 0"
                           data-percent="<?php echo $percentage; ?>" data-line-width="5" data-rotate="270"
                           data-scale-Color="false" data-size="50" data-animate="2000">
                          <span class="step font-bold"><?php echo $percentage; ?></span>%
                      </div>


                  </td>
                  <td style="text-align:center">
                      <?php echo $milestone->concepto; ?>
                  </td>
                  <td style="text-align:center">
                    <?php $project_details = $this->project_m->project_details($project_id);
                    $date = $project_details->project_start_date;

                    echo date($date);?>
                  </td>
                  <td style="text-align:center">

                  </td>

                  <td style="text-align:center">
                      <div class="btn-group-action">
                          <div class="btn-group">
                              <a class="btn btn-info" data-toggle="zestModal"
                                 href="<?php echo base_url('project/milestone/view/' . $project_id . '/' . $milestone->id); ?>"
                                 title=""><i class="fa fa-eye"></i><?php echo lang('label_view'); ?></a>
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                  <span class="caret">&nbsp;</span>
                              </button>
                              <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                                  <ul class="dropdown-menu pull-right">
                                      <li><a data-toggle="zestModal"
                                             href="<?php echo base_url('project/milestone/delete/' . $project_id . '/' . $milestone->id); ?>"
                                             title=""><i class="fa fa-trash"></i> <?php echo lang('label_delete'); ?></a>
                                      </li>
                                  </ul>
                              <?php } ?>
                          </div>
                      </div>
                  </td>
              </tr>
          <?php endforeach; endif; ?> -->
        <?php if (count($milestones)): foreach ($milestones as $milestone): ?>
            <tr>
                <td style="text-align:center">
                    <?php $percentage = $milestone->progress; ?>
                    <div class="chart milestone-progress" style="margin: 0"
                         data-percent="<?php echo $percentage; ?>" data-line-width="5" data-rotate="270"
                         data-scale-Color="false" data-size="50" data-animate="2000">
                        <span class="step font-bold"><?php echo $percentage; ?></span>%
                    </div>


                </td>
                <td style="text-align:center">
                    <?php echo $milestone->milestone_name; ?>
                </td>
                <td style="text-align:center">
                    <?php echo $milestone->start_date; ?>
                </td>
              
                <td style="text-align:center">
                    <div class="btn-group-action">
                        <div class="btn-group">
                            <a class="btn btn-info" data-toggle="zestModal"
                               href="<?php echo base_url('project/milestone/view/' . $project_id . '/' . $milestone->id); ?>"
                               title=""><i class="fa fa-eye"></i><?php echo lang('label_view'); ?></a>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret">&nbsp;</span>
                            </button>
                            <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                                <ul class="dropdown-menu pull-right">
                                    <li><a data-toggle="zestModal"
                                           href="<?php echo base_url('project/milestone/delete/' . $project_id . '/' . $milestone->id); ?>"
                                           title=""><i class="fa fa-trash"></i> <?php echo lang('label_delete'); ?></a>
                                    </li>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                </td>
            </tr>
        <?php endforeach; endif; ?>
        </tbody>

    </table>


</fieldset>
