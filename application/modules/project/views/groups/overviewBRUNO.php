<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/10/30
 * Time: 12:10 PM
 */
?>
<style>
    .project-info-bg {
        background: #FBFBFB !important;
        color: #333 !important;
        border-top: 1px solid #E4E5E7;
        border-left: 1px solid #E4E5E7;
        border-right: 1px solid #E4E5E7;
        font-weight: 600;
    }

</style>
<div class="panel-body">
    <div class="row">
        <?php
        $pro_duration = duration_diff($project_details->project_start_date, $project_details->project_due_date);
        $project_status = get_task_status_name($project_details->project_status);
        $project_info = get_project_details($project_details->project_id);
        $pro_percentage = 0;
        if ($project_info->project_use_worklog == 0) {
            $pro_percentage = $project_info->project_progress;
        } else {
            $pro_percentage = calculate_project_percentage($project_info->project_id);

        }
        if ($pro_percentage >= 100) {
            $project_status = 'Complete';
            $bg = 'success';
        } else {

            $bg = 'info';
        }
        ?>
        <div class="ribbon <?php echo $bg; ?>"><span><?php echo $project_status; ?></span></div>
<div class="col-md-12"><center><h1 class="bolded tiles-number text-warning" style="color:#ff0000"><?php echo $project_details->project_title; ?></h1>

</center>
  </div>
</br>
        <div class="col-md-6">
          <center><h3><?php echo $project_details->project_type; ?></h3></center>
          <img src="http://www.architecturelist.com/wp-content/uploads/2011/12/Casa-V-SMA-A.jpg" width="100%" height="auto">
<label for="a" class="btn">Cambiar Foto</label>
            <input id="a" name="a" type="file" style="visibility:hidden;">
          <?php
            $customer_details = get_client_details($project_details->client_id);
            ?>

      </div>

        <div class="col-md-6">

            <div class="block">
                <ul class="headline-info">
                    <li><?php echo lang('label_start_date'); ?>: <strong class="text-info"><?php echo date('Y-m-d',
                                strtotime($project_details->project_start_date)); ?></strong></li>
                    <li><?php echo lang('label_due_date'); ?>: <strong class="text-info"><?php echo date('Y-m-d',
                                strtotime($project_details->project_due_date)); ?></strong></li>
                </ul>
                <hr/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="summary">
                              <div class="content">
                                <div class="row">
                                <p class="billable"><?php echo lang('label_project_expenses'); ?></p><br/>
                                <span
                                    class="text-danger"><?php echo format_currency(project_expense($project_info->project_id)); ?></span>
                                  </div>
                                  <div class="row">
                                    <p class="billable">Presupuesto: </p><br/>
                                    <span
                                        class="text-danger"><?php echo format_currency($project_details->project_budget); ?></span>
                                </div>
                                <div class="row">
                                  <p class="billable">Pendiente: </p><br/>
                                  <span
                                      class="text-danger"><?php


                                       echo format_currency($project_details->project_budget); ?></span>
                              </div>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="summary">
                                <p>Duración</p><br/>
                                <span class="text-danger"><?php

                                $today_start_diff = time_elapse($project_details->project_start_date);

                                $elapse_days = explode(" ", $today_start_diff);
                                if ($elapse_days[0] > 0 && $pro_duration > 0) {
                                    $elapse_percentage = round(($elapse_days[0] / $pro_duration) * 100);
                                } else {
                                    $elapse_percentage = 0;
                                }


                                echo $elapse_days[0] . "/" . $pro_duration;   ?><span
                                    class="unit"><?php echo lang('days'); ?></span></span>
                            </div>
                        </div>

                    </div>
                </div>


            </div>

        </div>
    </div>


    <div class="row overview-header">
        <div class="panel-heading project-info-bg no-radius"><?php echo lang('label_project_description'); ?></div>

        <div class="col-sm-12">

            <?php echo $project_details->project_desc; ?>

 </div>


    </div>
    <div class="row overview-header">
        <div class="panel-heading project-info-bg no-radius">Presupuesto del proyecto</div>

        <div class="col-sm-12">

    <div align="center">
      <form action="<?php echo base_url(); ?>project/overview/import"
        method="post" name="upload_excel">
        <input class="button btn-info"  type="file" name="file" id="file">
        <button class="button btn-info" type="submit" id="submit" name="import">Importar</button>
      </form>
    <br>
    <br><br>

    <div style="width:80%; margin:0 auto;" align="center">
    <table class= id="t01">
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Date</th>
      </tr>
    <?php
     foreach ($view_data as  $data) {
    ?>
    <tr>
      <td><?php echo $data->name; ?></td>
      <td><?php echo $data->email; ?></td>
      <td><?php echo $data->created_date; ?></td>
    </tr>
    <?php } ?>
    </table>
    </div>
  </div>


   </div>


      </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <?php if (count($assigned_team)): foreach ($assigned_team as $team):
                    $user_details = users_details($team->user_id, 1);
                    ?>
                    <div class="col-sm-4">
                        <!-- BEGIN USER CARD LONG -->
                        <div class="the-box no-border contentarea">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="media user-card-sm">
                                        <div class="media-left">
                                            <a class="pull-left" href="#fakelink">
                                                <img class="media-object img-circle"
                                                     src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                                                     alt="Avatar">
                                            </a>
                                        </div>
                                        <?php
                                        $team_role = project_role_details($team->role);
                                        ?>
                                        <div class="media-body">
                                            <h4 class="media-heading"><?php echo $user_details->full_name; ?></h4>

                                            <p>
                                                <span
                                                    class="label label-info"><strong><?php echo $team_role->role_name; ?></strong></span>
                                            </p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; endif ?>
            </div>

        </div>
    </div>

</div>
<div class="row">

    <div class="col-sm-12 contentarea">
        <div class="col-sm-3">
            <div class="inline">
                <?php $per_details = calculate_expense_budget_percentage($project_id); ?>
                <div class="chart easy-pie-chart-4 text-danger" data-percent="<?php echo $per_details->percentage; ?>"
                     data-line-width="5" data-rotate="270" data-scale-Color="false" data-size="120" data-animate="2000">
                    <span class="h2 step font-bold"><?php echo $per_details->percentage; ?></span>%

                    <div
                        class="easypie-text text-muted"><?php echo $per_details->exp_amount . "/" . $per_details->budget; ?></div>
                </div>
                <div class="font-bold m-t">Expense/Budget</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="inline">
                <div class="chart easy-pie-tasks text-danger"
                     data-percent="<?php echo task_complete_perc($project_id); ?>" data-line-width="5" data-rotate="270"
                     data-scale-Color="false" data-size="120" data-animate="2000">
                    <span class="h2 step font-bold"><?php echo task_complete_perc($project_id); ?></span>%

                    <div
                        class="easypie-text text-muted"><?php echo complete_tasks($project_id) . "/" . project_tasks($project_id); ?></div>
                </div>
                <div class="font-bold m-t">Closed Tasks/All Tasks</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="inline">
                <?php

                $total_done = project_total_done_costs($project_id);
                $total_planned = $project_details->project_budget;

                if ($total_done > 0 && $total_planned > 0) {
                    $percentage = round(($total_done / $total_planned) * 100);
                } else {
                    $percentage = 0;
                }
                ?>
                <div class="chart easy-pie-budget text-danger" data-percent="<?php echo $percentage; ?>"
                     data-line-width="5" data-rotate="270" data-scale-Color="false" data-size="120" data-animate="2000">
                    <span class="h2 step font-bold"><?php echo $percentage; ?></span>%

                    <div
                        class="easypie-text text-muted"><?php echo format_amount($total_done) . "/" . format_amount($total_planned); ?></div>
                </div>
                <div class="font-bold m-t">Cost incurred/ Budget</div>
            </div>
        </div>

    </div>
</div>
<div class="row">

    <div class="col-sm-12 contentarea">

        <div class="col-sm-3">
            <div class="inline">
                <?php

                $total_done = project_total_done_costs($project_id);
                $total_planned = project_planned_costs($project_id);

                if ($total_done > 0 && $total_planned > 0) {
                    $curr_percentage = round(($total_done / $total_planned) * 100);
                } else {
                    $curr_percentage = 0;
                }
                ?>
                <div class="chart easy-pie-chart-1 text-success" data-percent="<?php echo $curr_percentage; ?>"
                     data-line-width="5" data-track-Color="#f0f0f0" data-bar-color="#8ec165" data-rotate="270"
                     data-scale-Color="false" data-size="120" data-animate="2000">
                    <span class="h2 step font-bold"><?php echo $curr_percentage; ?></span>%
                    <div
                        class="easypie-text text-muted"><?php echo format_amount($total_done) . "/" . format_amount($total_planned); ?></div>
                </div>
                <div class="font-bold m-t">Cost incurred/ estimated</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="inline">
                <?php $total_users = company_users_count();
                $assigned_team = assigned_users_count($project_id);
                if ($total_users > 0 && $assigned_team > 0) {
                    $assigned_percentage = ($assigned_team / $total_users) * 100;
                } else {
                    $assigned_percentage = 0;
                }

                ?>

                <div class="chart easy-pie-chart-4 text-danger" data-percent="<?php echo $assigned_percentage; ?>"
                     data-line-width="5" data-rotate="270" data-scale-Color="false" data-size="120" data-animate="2000">
                    <span class="h2 step font-bold"><?php echo $assigned_percentage; ?></span>%

                    <div class="easypie-text text-muted"><?php echo $assigned_team . "/" . $total_users; ?>Operators
                    </div>
                </div>
                <div class="font-bold m-t">Team/All Users</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="inline">
                <?php $percentage = calculate_project_percentage($project_id); ?>
                <div class="chart easy-pie-chart-4 text-danger" data-percent="<?php echo $percentage; ?>"
                     data-line-width="5" data-rotate="270" data-scale-Color="false" data-size="120" data-animate="2000">
                    <span class="h2 step font-bold"><?php echo $percentage; ?></span>%

                    <div
                        class="easypie-text text-muted"><?php echo round(get_work_done($project_id)) . "/" . round(get_work_estimate($project_id)); ?></div>
                </div>
                <div class="font-bold m-t">Work done/estimated</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="inline">
                <?php
                $today_start_diff = time_elapse($project_details->project_start_date);

                $elapse_days = explode(" ", $today_start_diff);
                if ($elapse_days[0] > 0 && $pro_duration > 0) {
                    $elapse_percentage = round(($elapse_days[0] / $pro_duration) * 100);
                } else {
                    $elapse_percentage = 0;
                }
                ?>
                <div class="chart easy-pie-chart-4 text-danger" data-percent="<?php echo $elapse_percentage; ?>"
                     data-line-width="5" data-rotate="270" data-scale-Color="false" data-size="120" data-animate="2000">
                    <span class="h2 step font-bold"><?php echo $elapse_percentage; ?></span>%

                    <div class="easypie-text text-muted"><?php echo $elapse_days[0] . "/" . $pro_duration; ?> days</div>
                </div>
                <div class="font-bold m-t">Tiempo Transcurrido</div>
            </div>
        </div>

    </div>
    <!-- /.col-sm-12-->

</div>

<div class="row">
    <div class="col-sm-12 contentarea">
        <div class="col-sm-6">
            <div class="the-box">

                <div class="panel panel-white no-radius">
                    <div class="panel-heading border-bottom">
                        <h3 class="box-title"><?php echo lang('label_project_activities'); ?></h3>
                    </div>
                    <div class="panel-body timeline-scroll">
                        <?php if (count($activities)) : ?>
                            <ul class="timeline-xs margin-top-15 margin-bottom-15">

                                <?php foreach ($activities as $activity): ?>
                                    <li class="timeline-item <?php echo $activity->activity_status; ?>">
                                        <div class="margin-left-15">
                                            <div
                                                class="text-muted text-small"> <?php echo ago(strtotime($activity->activity_date)) . " " . lang('ago'); ?> </div>
                                            <p>

                                            <div class="media-left">
                                                        <span class="activity-avatar avatar-xs">
                                                           <?php $user_details = users_details($activity->activity_user,
                                                               $activity->activity_user_type); ?>
                                                            <img
                                                                src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                                                                alt="...">
                                                        </span>
                                            </div>
                                            <div class="media-body">
                                                <div class="media-heading">
                                                    <a href=""
                                                       class="text-info strong"><?php echo activity_user($activity->activity_user_type,
                                                            $activity->activity_user); ?></a>
                                                    <small><span
                                                            class="text-off"><?php echo $activity->activity_date; ?></span>
                                                    </small>
                                                </div>
                                                <p>
                                                    <span
                                                        class="label label-<?php echo $activity->activity_status; ?>"></span>
                                                </p>

                                                <p>  <?php echo $activity->activity_event; ?></p>
                                            </div>


                                            </p>
                                        </div>
                                    </li>
                                <?php endforeach; ?>

                            </ul>
                        <?php else: ?>
                            <p>No project activities</p>
                        <?php endif; ?>
                    </div>

                </div>

            </div>
        </div>
        <!-- /.col-sm-6 -->

        <div class="col-sm-6">

            <div class="the-box">

                <h4 class="small-title"><?php echo lang('label_risk_analysis'); ?>
                    <span class="pull-right">


                                            <a data-toggle="zestModal" class="text-danger"
                                               href="<?php echo base_url('project/risk_matrix/' . $project_details->project_id); ?>">
                                                <i class="fa fa-plus"></i>
                                            </a>




                </span></h4>

                <div class="table-responsive">
                    <table class="table" cellpadding="0" cellspacing="0" border="0" width="100%">

                        <tbody>
                        <tr>
                            <td style="
                        padding-top: 0px;
                        padding-right: 0px;
                        padding-bottom: 0px;
                        padding-left: 0px;
                        ">
                                <div
                                    style="padding-bottom: 0px;padding-left: 0px;padding-right: 0px;padding-top: 0px;margin-top: 90px; -moz-transform: rotate(270deg); -o-transform: rotate(270deg); -webkit-transform: rotate(270deg); filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);">
                                    Impact
                                </div>
                            </td>
                            <td>
                                <table id="risk_chart" border="1" class="table">
                                    <tbody>

                                    <tr>
                                        <td align="right" width="15">100</td>
                                        <td align="center" bgcolor="#FFFFFF"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                100, 0, 5);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FFFFFF"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                100, 6, 10);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FFC0C0"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                100, 11, 20);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FF8080"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                100, 21, 30);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></a></td>
                                        <td align="center" bgcolor="#FF0000"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                100, 31, 100);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></a></td>
                                    </tr>
                                    <tr>
                                        <td align="right" width="15">30</td>
                                        <td align="center" bgcolor="#FFFFFF"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                30, 0, 5);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FFFFFF"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                30, 6, 10);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FFFFFF"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                30, 11, 20);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FFC0C0"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                30, 21, 30);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FF8080"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                30, 31, 100);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                    </tr>
                                    <tr>
                                        <td align="right" width="15">20</td>
                                        <td align="center" bgcolor="#C0FFC0"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                20, 0, 5);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FFFFFF"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                20, 6, 10);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FFFFFF"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                20, 11, 20);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FFFFFF"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                20, 21, 30);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FFC0C0"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                20, 31, 100);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                    </tr>
                                    <tr>
                                        <td align="right" width="15">10</td>
                                        <td align="center" bgcolor="#80FF80"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                10, 0, 5);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#C0FFC0"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                10, 6, 10);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FFFFFF"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                10, 11, 20);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FFFFFF"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                10, 21, 30);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FFFFFF"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                10, 31, 100);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                    </tr>
                                    <tr>
                                        <td align="right" width="15">5</td>
                                        <td align="center" bgcolor="#00FF00"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                5, 0, 5);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#80FF80"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                5, 6, 10);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#C0FFC0"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                5, 11, 20);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FFFFFF"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                5, 21, 30);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                        <td align="center" bgcolor="#FFFFFF"
                                            width="15"><?php $risk_result = get_risk_manager($project_details->project_id,
                                                5, 31, 100);
                                            if (count($risk_result)) {
                                                foreach ($risk_result as $result) {
                                                    echo $result->risk_id . ", ";
                                                }
                                            } ?></td>
                                    </tr>
                                    <tr>
                                        <td width="20"></td>
                                        <td width="20" align="center">5</td>
                                        <td width="20" align="center">10</td>
                                        <td width="20" align="center">20</td>
                                        <td width="20" align="center">30</td>
                                        <td width="20" align="center">100</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td align="center">Probability</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- .the-box  -->
        </div>
        <!-- /.col-sm-6 -->

    </div>
</div>
