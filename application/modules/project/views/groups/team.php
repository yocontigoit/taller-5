<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/09/22
 * Time: 04:09 PM
 */
?>
<div class="row">
    <div class="col-md-12">
        <fieldset>
            <legend><i class="fa fa-users"></i>Team
                <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2): ?>
                    <span class="pull-right"><button data-toggle="zestModal" class=" btn btn-info btn-sm" style="color:#ffffff;"
                                                href="<?php echo base_url('project/team/edit_resource/' . $project_id); ?>">Editar Team <i
                                class="fa fa-pencil-square-o"></i></button></span>
                <?php endif; ?>
            </legend>
            <div class="row">
                <?php if (count($assigned_team)): foreach ($assigned_team as $team):
                    $user_details = users_details($team->user_id, 1);
                    ?>
                    <div class="col-sm-6">
                        <!-- BEGIN USER CARD LONG -->
                        <div class="the-box no-border contentarea">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="media user-card-sm">
                                            <div class="media-left">
                                                <a class="pull-left" href="https://proyectosinternos.com/Taller5/user/edit/<?php echo $team->user_id * 99; ?>">
                                                    <img class="media-object img-circle"
                                                         src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                                                         alt="Avatar">
                                                </a>
                                            </div>
                                            <?php
                                            $estimated_hours = resource_planned_effort($project_id, $team->user_id);
                                            $completed_hours = resource_completed_effort($project_id, $team->user_id);
                                            $team_role = project_role_details($team->role);
                                            ?>
                                            <div class="media-body">
                                                <a style="color:#656d78;" href="https://proyectosinternos.com/Taller5/user/edit/<?php echo $team->user_id * 99; ?>"><h4 class="media-heading"><?php echo $user_details->full_name; ?></h4></a>

                                                <p>
                                                    <span
                                                        class="label label-success"><strong><?php echo $team_role->role_name; ?></strong></span>
                                                </p>

                                                <p>
                                                    <span
                                                        class="label label-danger" style="background:#828282;"><strong><?php echo round($completed_hours,
                                                                3); ?> hrs</strong></span>
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                        <?php

                                        if ($estimated_hours > 0 && $completed_hours > 0) {

                                            $work_percentage = round(($completed_hours / $estimated_hours) * 100);
                                        } else {
                                            $work_percentage = 0;
                                        }
                                        ?>
                                        <div class="media-object inline">
                                            <div class="chart easy-pie-chart-1 text-success" style="margin-top: 0px;margin-left:25px;"
                                                 data-percent="<?php echo $work_percentage; ?>" data-line-width="5"
                                                 data-track-Color="#f0f0f0" data-bar-color="#8ec165" data-rotate="270"
                                                 data-scale-Color="false" data-size="50" data-animate="2000">
                                                <span class="h4 step font-bold"><?php echo $work_percentage; ?></span>%
                                                <div
                                                    class="easypie-text text-muted"><?php echo round($completed_hours) . "/" . round($estimated_hours);?></div>
                                            </div>
                                            <p>
                                                <span class="label label-info"><strong>Trabajo
                                                        hecho/estimado</strong></span>
                                            </p>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.the-box .no-border -->
                        <!-- BEGIN USER CARD LONG -->
                    </div><!-- /.col-sm-6 -->
                <?php endforeach; endif ?>

            </div>
            <!-- /.row -->
        </fieldset>
    </div>
</div>
