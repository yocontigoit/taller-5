<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/03/06
 * Time: 11:14 AM
 */

?>
<div class="row">

    <div class="table-responsive" width="100%">
        <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table" width="100%">
            <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Proveedor</th>
                    <th>Concepto</th>
                    <th>Monto</th>
                </tr>
                </thead>
                <?php $totdisc=0; if(count($discounts)): foreach($discounts as $discount):
                
                ?>
                <tbody>
                <tr>
                    <td>
                        <?php echo substr($discount->created_at,0 ,-8) ?>
                    </td>
                    <td>
                         <a href="<?php echo base_url('client/view/' . $discount->client_id); ?>"><?php echo client_company($discount->client_id); ?></a>
                    </td>
                    <td>
                        Descuento
                    </td>
                    <td>
                        $<?php echo format_currency($discount->disc_amount);
                        
                        $totdisc = $totdisc + $discount->disc_amount; ?>
                    </td>
                </tr>
                
                <?php
                endforeach; endif; ?>
            </tbody>
        </table>
        <table class="table table-hover table-full-width table-th-block table-success">
            <tr>
                <td style="text-align:right;" width="80%">Descuentos:</td>
                <td width="20%">$<?php echo format_currency($totdisc); ?></td>
            </tr>
            <tr>
                <td style="text-align:right;" width="80%">Acumulado 3%:</td>
                <td width="20%">$<?php $totperc=0; if(count($threepercent)): foreach($threepercent as $percent): if($percent->client_id == $client_id):
                    
                   $totperc = $totperc + ($percent->amount *.03);
                    
                
                endif;endforeach;endif;
                
                echo format_currency($totperc);?></td>
            </tr>
            <tr>
                <td style="text-align:right;" width="80%">Total:</td>
                <td width="20%">$<?php echo format_currency($totdisc + $totperc); ?></td>
            </tr>
        </table>
    </div>

</div>
