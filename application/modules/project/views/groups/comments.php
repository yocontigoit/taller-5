<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 4/23/2015
 * Time: 11:13 PM
 */
?>
<fieldset>
    <legend><i class="fa fa-comments"></i> Reportes

        <span class="pull-right">
                <a id="show_main-reply" title="Reply comment"
                   class="btn btn-sm btn-info"> Agregar Reporte<i
                        class="fa fa-calendar"></i></a>
            </span>

    </legend>

        <div class="col-md-12">
            <!-- Add comment form -->
            <div id="main-reply" class="block" style="display:none;">
                <div class="well">
                    <?php echo form_open('project/comments/add_project_comment', array('role' => 'form')); ?>

                    <input type="hidden" name="project_id" class="form-control" value="<?php echo $project_id; ?> ">
<div class="row">
  <div class="col-md-4">
                    <div class="form-group">
                        <label for="taskPercent">Titulo</label>
                        <input type="text" class="form-control" name="title"></textarea>
                    </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="taskPercent">Fecha de inicio</label>
                            <input type="text" class="form-control datepicker" name="period"
                                   data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                   value="<?php echo date('d-m-Y') ?>">

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="taskPercent">Fecha de termino</label>
                            <input type="text" class="form-control datepicker" name="cost_date"
                                   data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                   value="<?php echo date('d-m-Y') ?>">

                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Multiple entrada de archivos</label>

                            <div class="input-group">
                                <input type="text" class="form-control" readonly>
    										<span class="input-group-btn">
    											<span class="btn btn-primary btn-file">
    												<i class="fa fa-plus"></i><input type="file" multiple name="project_upload[]">
    											</span>
    										</span>
                            </div>
                            <!-- /.input-group -->
                        </div>
                        <!-- /.form-group -->

                    </div>
                    <div class="col-md-12">
                      <div class="table-responsive">
                        <table id="item_table" class="table table-striped table-bordered">
                          <thead>
                            <tr class="blueheader">
                              <th>Fecha</th>
                              <th>Partida</th>
                              <th>Concepto</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody class="solsoParent">
                            <tr style="display: none !important; " class="updatesolsoChild">
                              <td>
                                <input type="text" class="form-control datepicker" name="item_date"
                                       data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                       value="<?php echo date('d-m-Y') ?>">
                              </td>
                              <td>
                                <input type="text" name="item_partida" class="form-control required solsoEvent"
                                autocomplete="off">
                              </td>

                              <td>
                                <input type="text" name="item_concept" class="form-control required solsoEvent"
                                autocomplete="off">
                              </td>
                              <td>
                                  <button type="button" class="btn btn-info removeClone"><i
                                          class="fa fa-trash"></i></button>
                              </td>
                              </tr>


                            </table>
                          </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <div class="form-actions text-right">
                      <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                                  <a href="javascript:void(0)" id="btn_update_add_item"
                                     class="btn btn-small btn-info"><i class="fa fa-plus"></i>Agregar Concepto</a>
                      <?php } ?>
                        <button type="button" class="btn btn-success" id="save_comments_items"><i class="fa fa-save"></i> Guardar</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            </div>
<div class="col-lg-12">
            <ul class="media-list media-sm media-dotted">

                <?php if (count($comments)): foreach ($comments as $comment) : ?>
                    <?php $user_details = users_details($comment->message_by_id, $comment->user_type); ?>
                    <button class="accordion"><h4><strong>Reporte:</strong> <?php echo $comment->Title; ?>  (<?php echo $comment->Periodo; ?>)</h4></button>
                    <div class="panel">
                      <li class="media">
                      <div class="row" style="background: #6e7376;color: white;margin:0px;">

                        <div class="col-sm-6" style="background: #6e7376;color: white;padding:2px;">

                        <h5 class="media-heading" style="padding-top: 5px;"> ARQ. SUP. DE OBRA: <a href="#fakelink"><?php echo $user_details->full_name; ?></a>
                        </h5>
                      </div>
                      <div class="col-sm-6" align="rigth" style="color: white;padding:5px;">
                        <p style="color: white;">
                            <small style="color: white;text-align:right;">Realizado el:   <?php echo $comment->message_date; ?></small>
                        </p>

                        <a class="btn btn-info"
                           href="<?php echo base_url('project/comments/print_report/' . $comment->message_id . '/'. $comment->project_id); ?>">
                            <i class="fa fa-print"></i>
                            PDF
                        </a>
                         <button class="btn btn-info" data-toggle="zestModal"
                           href="<?php echo base_url('project/comments/edit_report/' . $comment->message_id); ?>">
                            <i class="fa fa-pencil-square-o"></i>
                            editar</button>


</div>

</div>
                        <div class="media-body" style="width: 10000px;">

                            <?php echo $comment->message_text; ?>
                            <p align="right" style="margin-top: 0px;" class="comment-action">
                                <a title="Reply comment" data-toggle="collapse"
                                   href="#<?php echo $comment->message_id; ?>_read-reply"
                                   class="btn btn-xs btn-danger btn-square" style="background:#828282;border-width:0px;"> Comentar<i
                                        class="fa fa-reply"></i></a>
                            </p>

                            <div id="<?php echo $comment->message_id; ?>_read-reply" class="comment-action collapse">
                                <?php echo form_open('project/comments/add_comment_reply', array('role' => 'form')); ?>
                                <input type="hidden" name="project_id" class="form-control"
                                       value="<?php echo $comment->project_id; ?> ">
                                <input type="hidden" name="message_id" class="form-control"
                                       value="<?php echo $comment->message_id; ?> ">

                                <div class="form-group">
                                    <textarea name="message_reply" placeholder="Mensaje..."
                                              class="form-control"></textarea>
                                </div>
                                <div class="form-actions text-right">
                                    <input type="submit" value="Enviar" class="btn btn-primary">
                                </div>
                                <?php echo form_close(); ?>
                            </div>

                            <?php $user_id = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id; ?>
                            <?php $comment_conversations = project_comments_replies($comment->message_id); ?>
                            <?php if (!empty($comment_conversations)) : ?>

                                <ul class="media-list">
                                    <?php foreach ($comment_conversations as $conversations) : ?>
                                        <?php $user_details = users_details($conversations->replied_by,
                                            $conversations->user_type); ?>
                                        <li class="media">
                                            <a class="pull-left" href="#fakelink">
                                                <img class="media-object img-circle"
                                                     src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                                                     alt="Avatar">
                                            </a>

                                            <div class="media-body">
                                                <h4 class="media-heading"><a
                                                        href="#fakelink"><?php echo $user_details->full_name; ?></a>

                                                </h4>

                                                <p class="date">
                                                    <small><?php echo ago(strtotime($conversations->reply_date)); ?></small>

                                                </p>

                                                <?php echo $conversations->reply_message; ?>

                                            </div>
                                            <?php if (($user_id == $conversations->replied_by && $this->CompanyUser_type == $conversations->user_type) || $this->CompanyUserRolePermission_id == 1): ?>
                                                <span class="pull-right">
                                                 <a href="<?php echo base_url('project/comments/delete_comment/' . $conversations->reply_id . '/' . $project_id); ?>"
                                                    data-toggle="zestModal"><i style="color:#828282;" class="fa fa-trash-o text-danger"></i>
                                                 </a>

                                             </span>
                                             <span class="pull-right">
                                               <a href=""
                                                  data-toggle="zestModal"><i style="color:#828282;" class="fa fa-pencil-square-o text-danger"></i>
                                               </a>

                                          </span>

                                            <?php endif; ?>
                                        </li>

                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </li>
                    </div>

                <?php endforeach; endif; ?>


            </ul>
          </div>


        </div>
    </div>

</fieldset>

<style>
button.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 8px;
    width: 100%;
    text-align: left;
    border: none;
    outline: none;
    display: block;
    transition: .3s;
}

/* Add a background color to the button if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
button.accordion.active, button.accordion:hover {
    background-color: #6e7376;
    color: #fff;
}

/* Style the accordion panel. Note: hidden by default */
div.panel {
    padding: 0px;
    background-color: white;
    display: none;
}
</style>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
            button.style.display = "block";
        } else {
            panel.style.display = "block";
            button.style.display = "none";
        }
    }
}
</script>
<script src="https://proyectosinternos.com/Taller5/assets/libs/jquery-2.0.2.min.js" ></script>
<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>
<script type="text/javascript">
  $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    $('#btn_update_add_item').on('click', function(e) {
        // $( '.solsoSelect2.solsoCloneSelect2').select2('destroy');

        $('.solsoParent').append('<tr>' + $('tr.updatesolsoChild').html() + '</tr>');



        $( '.crt' ).each(function( index ) {
            $( this ).text(index+1);

            if (index > 0) {
                $( this ).parent().find( '.removeClone' ).removeClass('disabled');
            }
        });

        // $( ".sub_total" ).last().val('0.00');
        //
        // $( '.solsoCloneSelect2' ).select2();

        return false;
    });

    $( document ).on('click', '.removeClone', function() {

        $(this).parents().eq(1).remove();

        $( '.crt' ).each(function( index ) {
            $( this ).text(index+1);
        });

    });

    $('#save_comments_items').click(function() {

        var items = [];
        $('#item_table > tbody >tr').each(function() {
            var row = {};
            $(this).find('input,select,textarea').each(function() {

                row[$(this).attr('name')] = $(this).val();
            });

            items.push(row);
        });

        var project_id = $('input[name="project_id"]').val();
        var title  = $('input[name="title"]').val();
        var period  = $('input[name="period"]').val();
        var cost_date  = $('input[name="cost_date"]').val();

        console.log(items);


        // if(client_id == 'Empty'){
        //     return;
        // }

        $.post("<?php echo site_url('project/comments/add_project_comment'); ?>", {
                project_id:project_id,
                title: title,
                period: period,
                cost_date: cost_date,
                items: JSON.stringify(items)
            },
            function(data) {

                window.location.href = "https://proyectosinternos.com/Taller5/project/comments/preview/"+project_id;
            });

    });

</script>
