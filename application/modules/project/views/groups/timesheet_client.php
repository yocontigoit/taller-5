<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 4/6/2015
 * Time: 2:06 PM
 */
?>
<div class="row">
    <div class="col-lg-12">
        <fieldset>
            <legend><i class="fa fa-clock-o"></i>Log Sheet</legend>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-clock-o"></i>
                            <span class="badge pull-right text-muted"><?php echo count($manual_timelogs); ?></span>
                        </div>
                        <table class="table  table-hover table-full-width table-th-block">
                            <tbody>
                            <tr>
                                <td>Project Name</td>
                                <td>Task</td>
                                <td>Total Hours</td>
                                <td>Time Logged For</td>
                                <td>Time Logged By</td>
                                <td>Action</td>
                            </tr>
                            <?php if (count($manual_timelogs)):foreach ($manual_timelogs as $entry): ?>
                                <tr>
                                    <td><?php echo get_project_details($entry->project_id)->project_code; ?></td>
                                    <td><?php echo get_task_details($entry->task_id)->task_name; ?> </td>
                                    <td><?php echo $entry->hours; ?> </td>
                                    <td>
                                        <?php
                                        $user_details = users_details($entry->user_id, 1);
                                        echo $user_details->full_name;
                                        ?>

                                    </td>
                                    <td>
                                        <?php

                                        $user_detail = users_details($entry->entry_by, 1);
                                        echo $user_detail->full_name;
                                        ?>

                                    </td>
                                    <td>
                                        <a class="btn btn-danger disabled"
                                           href="<?php echo base_url('project/timesheet/delete_log/' . $entry->timer_id); ?>"><i
                                                class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; endif; ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>


        </fieldset>
    </div>

</div>