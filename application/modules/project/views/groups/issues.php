<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 4/23/2015
 * Time: 11:13 PM
 */
?>
<fieldset>
    <legend><i class="fa fa-bug"></i> Contingencias
        <span class="pull-right"><a data-toggle="zestModal" class="btn btn-sm btn-info"
                                    href="<?php echo base_url('project/issues/add/' . $project_id); ?>">Agregar Contingencia<i
                    class="fa fa-plus" ></i></a></span>

    </legend>


    <table class="table  table-hover table-full-width table-th-block table-success" id="zest_table">
        <thead>
        <tr>
            <th><?php echo lang('label_priorities'); ?></th>
            <th><?php echo lang('label_status'); ?></th>
            <th><?php echo lang('label_description'); ?></th>
            <th><?php echo lang('label_task'); ?></th>
            <th><?php echo lang('label_assign_to'); ?></th>
            <th><?php echo lang('label_do_on'); ?></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($project_issues)): foreach ($project_issues as $issue): ?>
            <tr>
                <td>

                    <?php $priority = get_priority($issue->issue_priority);

                    if (strtolower($priority) == 'low') {
                        $label = 'success';
                    } elseif (strtolower($priority) == 'medium') {
                        $label = 'info';
                    } else {
                        $label = 'warning';
                    }

                    ?>
                    <span class="label label-<?php echo $label; ?>"><?php echo $priority; ?></span>
                </td>
                <td>
                    <?php $status = get_ticket_status($issue->issue_status);

                    if ($status == 'open') {
                        $label = 'info';
                    } elseif ($status == 'closed') {
                        $label = 'success';
                    } elseif ($status == 'answered') {
                        $label = 'warning';
                    } else {
                        $label = 'info';
                    }

                    ?>

                    <span class="label label-<?php echo $label; ?>"><?php echo $status; ?>
                </td>
                <td>
                    <?php echo $issue->issue_description; ?>
                </td>
                <td>
                    <?php echo $issue->task_id; ?>
                </td>
                <td>
                    <a style="color:#656d78;" href="https://proyectosinternos.com/Taller5/user/edit/<?php echo $issue->user_id * 99; ?>"><?php echo ($issue->user_id) ? users_details($issue->user_id, 1)->full_name : "--------"; ?></a>
                </td>
                <td>
                    <?php echo $issue->issue_date; ?>
                </td>
                <td>
                    <div class="btn-group-action">
                        <div class="btn-group">
                            <a class=" btn btn-info"
                               href="<?php echo base_url('project/issues/view/' . $issue->issue_id); ?>" title=""><i
                                    class="fa fa-eye"></i>view</a>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret">&nbsp;</span>
                            </button>
                            <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                                <ul class="dropdown-menu pull-right">
                                    <li><a class="text-danger" style="color:#828282;" data-toggle="zestModal"
                                           href="<?php echo base_url('project/issues/assign/' . $issue->issue_id); ?>"><i
                                                class="fa fa-share-square-o"></i> Reassign</a></li>
                                    <li><a href="<?php echo base_url(); ?>" title=""><i class="fa fa-trash"></i> Delete</a>
                                    </li>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                </td>
            </tr>
        <?php endforeach; endif; ?>
        </tbody>

    </table>


</fieldset>
