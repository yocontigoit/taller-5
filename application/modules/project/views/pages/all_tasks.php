<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/24
 * Time: 01:07 PM
 */
?>
<div class="datatable-tasks">
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
               id="zest_table">
            <!-- <thead>-->
            <thead class="the-box dark full">
            <tr>
                <th>Status</th>
                <th><?php echo lang('label_task_description'); ?></th>
                <th class="task-priority"><?php echo lang('label_task_priority'); ?></th>
                <th class="task-date-added"><?php echo lang('label_task_date'); ?></th>
                <th><?php echo lang('label_assigned_users'); ?></th>
                <!-- <th class="task-progress"><?php echo lang('label_task_progress'); ?></th> -->
                <th class="task-deadline"><?php echo lang('label_task_due'); ?></th>
                <th class="task-tools text-center"><?php echo lang('label_task_options'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if (count($all_tasks)) : foreach ($all_tasks as $task): ?>
                <tr>
                    <td class="tex-center">
                        <form method="post" action="<?php echo base_url('project/tasks/update_task_status'); ?>">
                              <input type="hidden" name="task_id" value="<?php echo $task->task_id ?>">
                              <input type="hidden" name="project_id" value="<?php echo $task->project_id ?>">
                              <input type="hidden" name="avance" value="<?php echo $task->task_milestone_id ?>">
                              <input type="checkbox" name="status"
                                     class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($task->status_task == 1) ? 'checked' : ''; ?>/>
                              <div class="row">
                                <div class="col-xs-offset-3">
                                  <button type="submit" class="btn btn-default" name="saver"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span></button>
                                </div>
                              </div>
                        </form>
                    </td>
                    <td class="task-desc">
                        <?php echo $task->task_name; ?>
                    </td>
                    <td class="text-center">


                        <?php $priority = get_priority($task->task_priority);

                        if (strtolower($priority) == 'Baja') {
                            $label = 'info';
                        } elseif (strtolower($priority) == 'Media') {
                            $label = 'warning';
                        } else {
                            $label = 'danger';
                        }

                        ?>

                        <span class="label label-<?php echo $label; ?>"><?php echo $priority; ?></span>


                    </td>
                    <td class="text-center" style="width: 100px;">
                       <?php echo date_from_timestamp($task->task_start / 1000); ?></td>
                    <td>


                        <?php
                        $task_assigned_team = task_assigned($task->task_id);

                        if (count($task_assigned_team)): foreach ($task_assigned_team as $team): ?>
                            <?php

                            $user_details = users_details($team->user_id, 1);

                            ?>

                            <a style="color:#656d78;" href="https://proyectosinternos.com/Taller5/user/edit/<?php echo $team->user_id * 99; ?>">
                                <img width="80%" data-toggle="tooltip" data-title="<?php echo $user_details->full_name; ?>"
                                     src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                                     class="assigned-image-small" alt="<?php echo $user_details->full_name; ?>"
                                     data-original-title="" title="">
                            </a>
                        <?php endforeach;endif; ?>


                    </td>
                    <!-- <td>
                      <?php
                      $tasks_status = task_status();
                      foreach ($tasks_status as $status) { ?>
                        <?php if ($task->task_status == "STATUS_" . $status->status_name) { ?>
                      <h4 class=""><?php echo ucfirst(strtolower($status->status_name));?></h4>
                      <?php }
                      } ?>
                    </td> -->
                    <td class="text-center" style="width: 100px;">
                        <?php echo date_from_timestamp($task->task_end / 1000); ?>
                  </td>
                    <td class="text-center">

                        <div class="btn-group-action">
                            <div class="btn-group">
                                <a class=" btn btn-info"
                                   href="<?php echo site_url('project/tasks/details/' . $project_id . '/' . $task->task_id . '/tasks'); ?>"
                                   title=""><i class="fa fa-eye"></i><?php echo lang('label_view'); ?></a>
                                <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2): ?>
                                    <button type="button" class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown">
                                        <span class="caret">&nbsp;</span>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a class="text-danger" data-toggle="zestModal"
                                               href="<?php echo base_url('project/tasks/edit_task/' . $project_id . '/' . $task->task_id); ?>"
                                               title=""><i class="fa fa-pencil-square-o"></i> Edit</a></li>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>

                    </td>
                </tr>
            <?php endforeach; endif; ?>

            </tbody>
        </table>
    </div>
</div>

<script src="https://proyectosinternos.com/Taller5/assets/libs/jquery-2.0.2.min.js" ></script>
<script type="text/javascript">
$(document).ready(function(){

  //****************************start ********************************************/

  // $('.ios-switch').change(function() {
  //     //$('.todo .checkbox >  input:checked').click(function() {
  //     var $this = $(this).parent().parent().parent();
  //
  //     if ($(this).prop('checked')) {
  //         $this.addClass("complete");
  //         var selected = "";
  //         //oSortable = $('.dd-list').nestedSortable('toArray');
  //         var selected = get_selected_values();
  //         saveChanges(selected);
  //
  //     } else {
  //         // insert undo code here...
  //     }
  //
  // });
  //
  //
  // function get_selected_values()
  // {
  //     var selected_values = [];
  //     $('.ios-switch:checked').each(function()
  //     {
  //         selected_values.push($(this).val());
  //     });
  //     alert("values"+selected_values)
  //     return selected_values;
  // }
  //
  // function saveChanges(selected) {
  //     var that = this;
  //     $.ajax({
  //         type: "POST",
  //         url: "<?php echo base_url('project/tasks/update_task_status'); ?>",
  //         dataType: "json",
  //         data: {selectedtask: selected.join('~')},
  //         cache: true, // (warning: this will cause a timestamp and will call the request twice)
  //         success: function(data) {
  //             that.reset();
  //         }
  //
  //     });
  // }



  //***************************END***********************************************/
});
</script>


<!-- /tasks table -->
