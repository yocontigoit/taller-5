<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/24
 * Time: 01:08 PM
 */
?>
<!-- Tasks table -->

<div class="datatable-tasks">
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
               id="zest_table">
            <thead>
            <tr>
                <th><?php echo lang("label_task_description"); ?></th>
                <th class="task-priority"><?php echo lang("label_task_priority"); ?></th>
                <th class="task-date-added"><?php echo lang("label_task_date"); ?></th>
                <th><?php echo lang('label_assigned_users'); ?></th>
                <th class="task-progress"><?php echo lang("label_task_progress"); ?></th>
                <th class="task-deadline"><?php echo lang("label_task_due"); ?></th>
                <th class="task-tools text-center"><?php echo lang("label_task_options"); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if (count($active_tasks)) : foreach ($active_tasks as $task): ?>
                <tr>
                    <td class="task-desc">
                        <?php echo $task->task_name; ?>
                    </td>
                    <td class="text-center">

                        <?php $priority = get_priority($task->task_priority);

                        if (strtolower($priority) == 'low') {
                            $label = 'info';
                        } elseif (strtolower($priority) == 'medium') {
                            $label = 'warning';
                        } else {
                            $label = 'danger';
                        }

                        ?>

                        <span class="label label-<?php echo $label; ?>"><?php echo $priority; ?></span>


                    </td>
                    <td> <?php echo $task->date_added; ?></td>
                    <td>


                        <?php
                        $task_assigned_team = task_assigned($task->task_id);

                        if (count($task_assigned_team)): foreach ($task_assigned_team as $team): ?>
                            <?php

                            $user_details = users_details($team->user_id, 1);

                            ?>

                            <a style="color:#656d78;" href="https://proyectosinternos.com/Taller5/user/edit/<?php echo $team->user_id * 99; ?>">
                                <img  data-toggle="tooltip" data-title="<?php echo $user_details->full_name; ?>"
                                     src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                                     class="assigned-image-small" alt="<?php echo $user_details->full_name; ?>"
                                     data-original-title="" title="">
                            </a>
                        <?php endforeach;endif; ?>


                    </td>
                    <td>
                        <div class="progress progress-micro">
                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="90"
                                 aria-valuemin="0" aria-valuemax="100"
                                 style="width: <?php echo $task->task_progress; ?>%;">
                                <span class="sr-only"><?php echo $task->task_progress; ?>
                                    % <?php lang('label_complete') ?></span>
                            </div>
                        </div>
                    </td>

                    <td>
                        <?php
                        $task_due = duration_diff(date('Y-m-d'), date_from_timestamp($task->task_end / 1000));

                        ?>
                        <i class="fa fa-clock-o"></i> <strong
                            class="text-danger"><?php echo $task_due . " "; ?></strong> days left

                    </td>
                    <td class="text-center">
                        <div class="btn-group-action">
                            <div class="btn-group">
                                <a class=" btn btn-info"
                                   href="<?php echo site_url('project/tasks/details/' . $project_id . '/' . $task->task_id . '/tasks'); ?>"
                                   title=""><i class="fa fa-eye"></i><?php echo lang('label_view'); ?></a>
                                <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2): ?>
                                    <button type="button" class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown">
                                        <span class="caret">&nbsp;</span>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a class="text-danger" data-toggle="zestModal"
                                               href="<?php echo base_url('project/tasks/edit_task/' . $project_id . '/' . $task->task_id); ?>"
                                               title=""><i class="fa fa-pencil-square-o"></i> Edit</a></li>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; endif; ?>

            </tbody>
        </table>
    </div>
</div>

<!-- /tasks table -->
