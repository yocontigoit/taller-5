<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/04/12
 * Time: 11:08 AM
 */
?>
<div class="modal-dialog" style="width: 100%;height: 100%;">
    <div class="modal-content modal-lg" style="margin:1% 1% 2%;width: 98%;">
        <div class="modal-header bg-info">
            <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                    <i class="fa fa-times"></i>
                    </span>
                <span class="sr-only">Cerrar</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_preview'); ?></h4>
        </div>
        <div class="row">
            <div class="col-md-9">
                <iframe
                    src="https://docs.google.com/gview?url=<?php echo base_url('files/project_files/' . $file_path); ?>&embedded=true"
                    height="700" width="100%" frameborder="0" style="border: 0px solid #ddd;"
                    class="background-loader"></iframe>
            </div>
            <div class="col-md-3">
                <div class="margin-left-15">
                    <p>

                    <div class="media-left">
                      <span class="activity-avatar avatar-md">
                         <?php $user_details = users_details($details->uploaded_by, $details->uploader_type); ?>
                          <img src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>" alt="...">
                     </span>
                    </div>
                    <div class="media-body">
                        <div class="media-heading">
                            <a href="" class="text-info strong"><?php echo activity_user($details->uploader_type,
                                    $details->uploaded_by); ?></a>

                        </div>
                    </div>
                    </p>
                </div>
                <div class="section-wrapper">
                    <table class="table table-bordered table-hover table-extra-padd">
                        <tbody>
                        <tr>
                            <td class="col-sm-4">
                                <?php echo lang('label_date'); ?>:
                            </td>
                            <td class="col-sm-4">
                                <?php echo readable_date_format($details->upload_date); ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-sm-4">
                                <?php echo lang('label_filesize'); ?>:
                            </td>
                            <td class="col-sm-4">
                                <?php echo $details->file_size; ?>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <a class="btn btn-info"
                   href="<?php echo base_url('project/documents/download/' . $details->attachment_id); ?>"><i
                        class="fa fa-cloud-download"></i><?php echo lang('label_download'); ?></a>

            </div>
        </div>

    </div>
</div>