<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Cerrar</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_update_resource'); ?></h4>
        </div>
        <form method="post" action="<?php echo base_url('project/team/update_resources'); ?>">
            <input type="hidden" name="project_id" class="input-sm form-control" value="<?php echo $project_id; ?>">

            <div class="modal-body">
                <!-- table -->
                <table
                    class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
                    id="zest_table">
                    <thead class="the-box dark full">
                    <tr>
                        <th>
                            <?php echo lang('label_name'); ?>
                        </th>
                        <th>
                            <?php echo lang('label_role'); ?>

                        </th>

                        <th>
                            <?php echo lang('label_est_worklog'); ?>

                        </th>
                        <th>
                            <?php echo lang('label_work_done'); ?>

                        </th>

                        <th>
                            <?php echo lang('label_hourly_cost'); ?>

                        </th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php if (count($assigned_team)) : foreach ($assigned_team as $assigned) :
                        $user_details = users_details($assigned->user_id, 1); ?>
                        <tr>
                            <td>
                                <?php echo $user_details->full_name; ?>
                            </td>
                            <td>
                                <select class="form-control" name="project_role[<?php echo $assigned->user_id; ?>]">
                                    <option value="...">Select...</option>
                                    <?php if (count($project_roles)): foreach ($project_roles as $role): ?>
                                        <?php if ($assigned->role == $role->role_id): ?>
                                            <option selected
                                                    value="<?php echo $role->role_id; ?>"><?php echo $role->role_name; ?></option>
                                        <?php else: ?>
                                            <option
                                                value="<?php echo $role->role_id; ?>"><?php echo $role->role_name; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; endif; ?>
                                </select>
                            </td>

                            <td>
                                <?php echo resource_planned_effort($project_id, $assigned->user_id); ?>
                            </td>
                            <td>
                                <?php echo resource_completed_effort($project_id, $assigned->user_id); ?>
                            </td>
                            <td>
                                <input type="text" name="hourly_cost[<?php echo $assigned->user_id; ?>]"
                                       class="input-sm form-control amount"
                                       value="<?php echo $assigned->hourly_cost; ?>">
                            </td>

                        </tr>
                    <?php endforeach; endif; ?>
                    </tbody>
                </table>

                <!-- emd table -->
            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" value="addNewTask" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    Guardar
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    Cancelar
                </button>
            </div>
        </form>
    </div>
</div>
</div>

