<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
// $report_id = $report->message_id;
//  $report = get_report($report_id);
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                        <i class="fa fa-times"></i>
                        </span>
                <span class="sr-only">Cerrar</span>
            </button>
            <h4 class="modal-title">Reporte</h4>
        </div>
        <?php echo form_open_multipart('project/comments/edit_comments', array()); ?>

        <div class="modal-body">

            <input type="hidden" class="form-control" name="message_id" value="<?php echo $report->message_id; ?>">
            <input type="hidden" name="project_id" value="<?php echo $report->project_id; ?>">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="taskPercent">Nombre</label>
                        <input type="text" class="form-control" value="<?php echo $report->Title; ?>"
                               name="Title">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="taskTitle">Fecha de inicio</label>
                        <input type="text" class="form-control datepicker" name="message_date"
                               data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                               value="<?php echo date('d-m-Y', strtotime($report->message_date)); ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="taskTitle">Fecha de vencimiento</label>
                        <input type="text" class="form-control datepicker" name="Periodo" data-date-format="dd-mm-yyyy"
                               placeholder="dd-mm-yyyy"
                               value="<?php echo date('d-m-Y', strtotime($report->Periodo)); ?>">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Multiple entrada de archivos</label>

                    <div class="input-group">
                        <input type="text" class="form-control" readonly>
                <span class="input-group-btn">
                  <span class="btn btn-primary btn-file">
                    <i class="fa fa-plus"></i><input type="file" multiple name="project_upload[]">
                  </span>
                </span>
                    </div>
                    <!-- /.input-group -->
                </div>
                <!-- /.form-group -->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- <div class="form-group">
                        <label for="taskPercent">Detalles</label>
                        <textarea class="summernote" name="message_text"><?php echo $report->message_text; ?></textarea>
                    </div> -->
                    <div class="table-responsive">
                      <table id="item_table" class="table table-striped table-bordered">
                        <thead>
                          <tr class="blueheader">
                            <th>Fecha</th>
                            <th>Partida</th>
                            <th>Concepto</th>
                            <th></th>
                          </tr>
                        </thead>
                          <?php if(count($report_items)) : foreach($report_items as $item): ?>
                          <tbody class="solsoParent">
                          <tr class="updatesolsoChild">
                            <td>
                              <input type="text" class="form-control datepicker" name="item_date"
                                     data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                     value="<?php echo $item->date;  ?>">
                            </td>
                            <td>
                              <input type="text" name="item_partida" class="form-control required solsoEvent"
                              autocomplete="off" value="<?php echo $item->item;  ?>">
                            </td>

                            <td>
                              <input type="text" name="item_concept" class="form-control required solsoEvent"
                              autocomplete="off" value="<?php echo $item->concept;  ?>">
                            </td>
                            <td>
                                <button type="button" class="btn btn-info removeClone"><i
                                        class="fa fa-trash"></i></button>
                            </td>
                            </tr>
                            </tbody>
                          <?php endforeach; endif; ?>
                          </table>
                        </div>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <button class="btn btn-info btn-icon" name="submit" type="input">
                <i class="fa fa-check-square-o"></i>
                Guardar
            </button>
            <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                <i class="fa fa-times-circle-o"></i>
                Cancelar
            </button>
            <div class="pull-left">
              <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                <a href="javascript:void(0)" id="btn_add_item"
                class="btn btn-small btn-info"><i class="fa fa-plus"></i>Agregar Concepto</a>
              <?php } ?>
            </div>
        </div>
        </form>
    </div>
</div>
<link href="https://proyectosinternos.com/Taller5/assets/plugins/summernote/summernote.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="https://proyectosinternos.com/Taller5/assets/plugins/summernote/summernote.min.js"></script>
<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script type="text/javascript">

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    $('.summernote').summernote({
      height: 300,                 // set editor height
      width: "90%",                 // set editor height
      minHeight: null,             // set minimum height of editor
      maxHeight: null,             // set maximum height of editor
      dialogsInBody: true
    });

    $('#btn_add_item').on('click', function(e) {
        // $( '.solsoSelect2.solsoCloneSelect2').select2('destroy');

        $('.solsoParent').append('<tr>' + $('tr.updatesolsoChild').html() + '</tr>');



        $( '.crt' ).each(function( index ) {
            $( this ).text(index+1);

            if (index > 0) {
                $( this ).parent().find( '.removeClone' ).removeClass('disabled');
            }
        });

        // $( ".sub_total" ).last().val('0.00');
        //
        // $( '.solsoCloneSelect2' ).select2();

        return false;
    });

    $( document ).on('click', '.removeClone', function() {

        $(this).parents().eq(1).remove();

        $( '.crt' ).each(function( index ) {
            $( this ).text(index+1);
        });

    });
</script>
