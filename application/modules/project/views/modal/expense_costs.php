<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Cerrar</span>
            </button>
            <h4 class="modal-title">Agregar Gasto</h4>
        </div>
        <?php echo form_open_multipart('project/costs/add_expense_costs'); ?>
        <div class="modal-body">
            <input type="hidden" class="form-control" name="project_id" value="<?php echo $project_id; ?>">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="taskTitle">Nombre</label>
                        <input class="form-control" type="text" name="cost_description" required>

                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent"><?php echo lang('label_date'); ?></label>
                        <input type="text" class="form-control datepicker" name="cost_date"
                               data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                               value="<?php echo date('d-m-Y') ?>">

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent">Categoria</label>
                        <select class="form-control" name="cost_classification">

                            <optgroup label="<?php echo lang('label_category'); ?>">
                                <?php foreach ($categories as $category): ?>
                                    <option
                                        value="<?php echo $category->class_id; ?>"><?php echo $category->class_name; ?></option>
                                <?php endforeach; ?>
                            </optgroup>
                        </select>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent"><?php echo lang('label_estimate_cost'); ?></label>
                        <input type="text" class="form-control" name="cost_estimate">
                        <input type="hidden" value="" class="form-control" name="cost_actual">

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                      <label for="taskPriority"><?php echo lang('label_payment_method'); ?> </label>
                      <select class="form-control" name="payment_method" onChange="pagoOnChange(this)" required title="Seleccione un metodo de pago !">
                          <option value="">Ninguno</option>

                          <?php foreach ($this->config->item('payment') as $payment): if($payment->oculta == 0): ?>
                              <option
                                  value="<?php echo $payment->payment_code; ?>"><?php echo $payment->payment_name. " ".$payment->payment_metod; ?></option>
                          <?php endif; endforeach; ?>

                      </select>
                      <span class="help-block">Efectivo, tarjeta de crédito..</span>
                        <!-- <label for="taskPercent">Estimación</label>
                        <select class="form-control" name="estimate">
                            <optgroup label="Estimaciones">
                                <?php $count = 1; foreach ($estimaciones as $estimate): if ($estimate->project_id == $project_id): ?>
                                    <option
                                        value="<?php echo $estimate->id; ?>">Estimación: <?php echo $count++ ?></option>
                                <?php endif; endforeach; ?>
                            </optgroup>
                        </select> -->

                    </div>
                </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                 <div class="form-group">
                      <input type="text" class="form-control" readonly>
                                                          <span class="input-group-btn">
                                                              <span class="btn btn-primary btn-file">
                                                                  Buscar Archivo<input type="file" multiple
                                                                                     name="expense_receipt">
                                                              </span>
                                                          </span>
                  </div>
                  <!-- /.input-group -->


              </div>

          </div>
            </div>
        <div class="modal-footer">
            <button class="btn btn-info btn-icon" name="submit" type="input">
                <i class="fa fa-check-square-o"></i>
                <?php echo lang('form_button_save'); ?>
            </button>
            <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                <i class="fa fa-times-circle-o"></i>
                <?php echo lang('form_button_cancel'); ?>
            </button>
        </div>
        </form>
    </div>
</div>
<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>
<script type="text/javascript">


    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

</script>
