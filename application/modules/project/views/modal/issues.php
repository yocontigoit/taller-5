<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Cerrar</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_issues'); ?></h4>
        </div>
        <?php echo form_open_multipart('project/issues/add_issue', array()); ?>

        <div class="modal-body">
            <input type="hidden" class="form-control" name="project_id" value="<?php echo $project_id; ?>">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskTitle">Prioridad</label>

                        <select class="form-control" name="issue_priority">
                            <option value="...">Seleccionar...</option>
                            <?php
                            $tasks_priorities = task_priorities();
                            foreach ($tasks_priorities as $priority) { ?>
                                <option
                                    value="<?php echo $priority->priority_id; ?>"><?php echo ucfirst(strtolower($priority->priority_name)); ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskTitle">Estatus</label>
                        <select class="form-control" name="issue_status">
                            <option value="...">Seleccionar...</option>
                            <?php
                            $tasks_status = ticket_status();
                            foreach ($tasks_status as $status) { ?>

                                <option
                                    value="<?php echo $status->status_id; ?>"><?php echo ucfirst(strtolower($status->status_name)); ?></option>

                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="taskPercent">Descripción</label>
                        <textarea class="form-control" rows="5" name="issue_description" required=""></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskTitle">Tareas</label>
                        <select class="form-control" name="tasks">

                            <optgroup label="Task">
                                <option value="none">Ninguna</option>
                                <?php

                                if (!empty($project_tasks)) {
                                    foreach ($project_tasks as $project_task) { ?>
                                        <option
                                            value="<?php echo $project_task->task_id; ?>"><?php echo strtoupper($project_task->task_name); ?></option>
                                    <?php }
                                } ?>
                            </optgroup>

                        </select>
                    </div>
                </div>
                <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="taskTitle">Cesionario</label>
                            <select data-placeholder="Asignar" class="form-control chosen-select" tabindex="4"
                                    name="assign_to">
                                <optgroup label="<?php echo lang("label_users") ?>">

                                    <?php if (count($users)) : foreach ($users as $user): ?>

                                        <option value="<?php echo $user->user_id; ?>">
                                            <?php echo ucfirst($user->emp_name) . " " . ucfirst($user->emp_surname); ?>
                                        </option>

                                    <?php endforeach; endif; ?>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskTitle">Fecha</label>
                        <input type="text" class="form-control datepicker" name="issue_date"
                               data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                               value="<?php echo date('d-m-Y'); ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskTitle">Archivo</label>

                        <div class="input-group">
                            <input type="text" class="form-control" readonly>
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
												<i class="fa fa-plus"></i><input type="file" name="issue_upload">
											</span>
										</span>
                        </div>
                        <!-- /.input-group -->
                    </div>
                </div>
            </div>


        </div>
        <div class="modal-footer">
            <button class="btn btn-info btn-icon" name="submit" type="input">
                <i class="fa fa-check-square-o"></i>
                Guardar
            </button>
            <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                <i class="fa fa-times-circle-o"></i>
                Cancelar
            </button>
        </div>
        </form>
    </div>
</div>
<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>

<script type="text/javascript">

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });


    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }
</script>
