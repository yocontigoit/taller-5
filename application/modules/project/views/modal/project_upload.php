<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 4/10/2016
 * Time: 2:10 PM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Cerrar</span>
            </button>
            <h4 class="modal-title">Subir Archivo</h4>
        </div>

        <?php echo form_open_multipart(base_url('project/documents/upload/' . $project_id . '/' . $task_id), ''); ?>
        <div class="modal-body">
            <input type="hidden" class="form-control" name="project_id" value="">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="fileTitle">Titulo de archivo</label>
                        <input class="form-control" type="text" name="file_title" required>

                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="fileDescription">Descripción de archivo</label>
                        <textarea class="form-control rounded" name="file_description" required></textarea>

                    </div>
                </div>
            </div>
<div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="fileDescription">Visible al cliente</label>
                        <select class="form-control"><option>si</option><option>no</option></select>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Multiple entrada de archivos</label>

                        <div class="input-group">
                            <input type="text" class="form-control" readonly>
										<span class="input-group-btn">
											<span class="btn btn-primary btn-file">
												<i class="fa fa-plus"></i><input type="file" multiple
                                                                                 name="project_upload[]">
											</span>
										</span>
                        </div>
                        <!-- /.input-group -->
                    </div>
                    <!-- /.form-group -->

                </div>

            </div>


        </div>
        <div class="modal-footer">
            <button class="btn btn-success btn-icon" value="addNewTask" name="submit" type="input">
                <i class="fa fa-check-square-o"></i>
                Guardar
            </button>
            <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                <i class="fa fa-times-circle-o"></i>
                Cancelar
            </button>
        </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        /** BEGIN INPUT FILE **/
        if ($('.btn-file').length > 0) {
            $(document)
                .on('change', '.btn-file :file', function () {
                    "use strict";
                    var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [numFiles, label]);
                });
            $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

                if (input.length) {
                    input.val(log);
                } else {
                    if (log) alert(log);
                }
            });
        }
    });

</script>
