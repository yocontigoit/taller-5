<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
                <span aria-hidden="true">
                    <i class="fa fa-times"></i>
                </span>
                <span class="sr-only">Cerrar</span>
            </button>
            <h4 class="modal-title"><i class="fa fa-book"></i> Minutas</h4>

        </div>
        <div class="modal-body">
            <?php echo form_open_multipart('project/notes/save_note/' . $project_id . '/' . $note_id, array('role' => 'form')); ?>
            <input type="hidden" name="project_id" value="<?php echo $project_id ?>">
            <input type="hidden" name="task_depend" value="0">
            <input class="form-control" type="hidden" value="1" name="task_code" required="">

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="taskTitle">Tema</label>
                            <input class="form-control" type="text" name="cost_description" value="<?php echo $notes->cost_description; ?>" required>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="taskPercent"><?php echo lang('label_date'); ?></label>
                            <input type="text" class="form-control datepicker" name="cost_date"
                             data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                             value="<?php echo date('d-m-Y') ?>">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="taskPercent">Clasificación</label>
                                <select class="form-control" name="cost_classification" value="<?php echo $notes->cost_classification; ?>">
                                    <option value="Sin Tareas">Sin tareas</option>
                                    <option value="Con Pendientes">Con Pendientes</option>
                                </select>

                        </div>
                    </div>

                </div>
                <?php $note = intval($notes->assistant); ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="taskPercent">Asistentes</label>
                                <select data-placeholder="Selecciona..." class="form-control chosen-select" multiple tabindex="4" name="assign_to0[]">
                                  <option value="Empty">&nbsp;</option>
                                  <optgroup label="<?php echo lang("label_clients") ?>">
                                      <?php if (count($clients)):foreach ($clients as $client): ?>
                                          <?php if ($note == $client->client_id) { ?>
                                              <option selected="selected"
                                                      value="<?php echo $notes->assistant; ?>"><?php echo ucfirst($client->client_name) ?></option>
                                          <?php } else { ?>
                                              <option
                                                  value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name) ?></option>
                                          <?php } ?>
                                      <?php endforeach; endif; ?>
                                  </optgroup>
                                </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div id="main-reply" class="block">
                            <div class="well">
                                <div class="form-group">
                                    <div class="row">
                                      <div class="col-sm-offset-6">
                                          <label for="taskPercent">Detalles</label>
                                      </div>
                                    </div>
                                    <div class="col-sm-offset-2">
                                        <textarea name="message" ><?php echo $notes->message; ?></textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-sm-1"><h4>1.</h4></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="taskPercent">Tarea </label>
                                <input type="text" class="form-control" name="taskname1" placeholder="nombre" value="<?php echo $notes->taskname1; ?>"></input>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                              <label for="taskStart"><?php echo lang('label_task_assign_to'); ?></label>
                              <select data-placeholder="Asignar" class="form-control chosen-select" multiple tabindex="4"
                                      name="task_assign1[]">
                                  <optgroup label="<?php echo lang("label_users") ?>">
                                      <?php if (count($users)) : foreach ($users as $user): ?>
                                          <option
                                              value="<?php echo $user->user_id; ?>" <?php foreach ($task_members as $assigned) {
                                              if ($user->user_id == $assigned->user_id) { ?> selected="selected" <?php } else {

                                              }

                                          } ?>>
                                          <?php echo ucfirst(users_details($user->user_id, 1)->full_name); ?>
                                          </option>

                                      <?php endforeach; endif; ?>
                                  </optgroup>
                              </select>
                              <span class="help-block">Personas a las que le corresponde la tarea.</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-sm-1"><h4>2.</h4></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="taskPercent">Tarea </label>
                                <input type="text" class="form-control" name="taskname2" placeholder="nombre" value="<?php echo $notes->taskname2; ?>"></input>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                              <label for="taskStart"><?php echo lang('label_task_assign_to'); ?></label>
                              <select data-placeholder="Asignar" class="form-control chosen-select" multiple tabindex="4"
                                      name="task_assign2[]">
                                  <optgroup label="<?php echo lang("label_users") ?>">
                                      <?php if (count($project_members)) : foreach ($project_members as $user):
                                          $user = get_user_details($user->user_id);
                                          ?>
                                          <option
                                              value="<?php echo $user->emp_id; ?>"><?php echo ucfirst($user->emp_name) . " " . ucfirst($user->emp_surname); ?></option>
                                      <?php endforeach; endif; ?>
                                  </optgroup>
                              </select>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-sm-1"><h4>3.</h4></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="taskPercent">Tarea </label>
                                <input type="text" class="form-control" name="taskname3" placeholder="nombre" value="<?php echo $notes->taskname3; ?>"></input>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                              <label for="taskStart"><?php echo lang('label_task_assign_to'); ?></label>
                              <select data-placeholder="Asignar" class="form-control chosen-select" multiple tabindex="4"
                                      name="task_assign3[]">
                                  <optgroup label="<?php echo lang("label_users") ?>">
                                      <?php if (count($project_members)) : foreach ($project_members as $user):
                                          $user = get_user_details($user->user_id);
                                          ?>
                                          <option
                                              value="<?php echo $user->emp_id; ?>"><?php echo ucfirst($user->emp_name) . " " . ucfirst($user->emp_surname); ?></option>
                                      <?php endforeach; endif; ?>
                                  </optgroup>
                              </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-sm-1"><h4>4.</h4></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="taskPercent">Tarea </label>
                                <input type="text" class="form-control" name="taskname4" placeholder="nombre" value="<?php echo $notes->taskname4; ?>"></input>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                              <label for="taskStart"><?php echo lang('label_task_assign_to'); ?></label>
                              <select data-placeholder="Asignar" class="form-control chosen-select" multiple tabindex="4"
                                      name="task_assign4[]">
                                  <optgroup label="<?php echo lang("label_users") ?>">
                                      <?php if (count($project_members)) : foreach ($project_members as $user):
                                          $user = get_user_details($user->user_id);
                                          ?>
                                          <option
                                              value="<?php echo $user->emp_id; ?>"><?php echo ucfirst($user->emp_name) . " " . ucfirst($user->emp_surname); ?></option>
                                      <?php endforeach; endif; ?>
                                  </optgroup>
                              </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-sm-1"><h4>5.</h4></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="taskPercent">Tarea </label>
                                <input type="text" class="form-control" name="taskname5" placeholder="nombre" value="<?php echo $notes->taskname5; ?>"></input>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                              <label for="taskStart"><?php echo lang('label_task_assign_to'); ?></label>
                              <select data-placeholder="Asignar" class="form-control chosen-select" multiple tabindex="4"
                                      name="task_assign5[]">
                                  <optgroup label="<?php echo lang("label_users") ?>">
                                      <?php if (count($project_members)) : foreach ($project_members as $user):
                                          $user = get_user_details($user->user_id);
                                          ?>
                                          <option
                                              value="<?php echo $user->emp_id; ?>"><?php echo ucfirst($user->emp_name) . " " . ucfirst($user->emp_surname); ?></option>
                                      <?php endforeach; endif; ?>
                                  </optgroup>
                              </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-sm-1"><h4>6.</h4></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="taskPercent">Tarea </label>
                                <input type="text" class="form-control" name="taskname6" placeholder="nombre" value="<?php echo $notes->taskname6; ?>"></input>
                            </div>
                         </div>
                         <div class="col-md-5">
                              <div class="form-group">
                                <label for="taskStart"><?php echo lang('label_task_assign_to'); ?></label>
                                <select data-placeholder="Asignar" class="form-control chosen-select" multiple tabindex="4"
                                        name="task_assign6[]">
                                    <optgroup label="<?php echo lang("label_users") ?>">
                                        <?php if (count($project_members)) : foreach ($project_members as $user):
                                            $user = get_user_details($user->user_id);
                                            ?>
                                            <option
                                                value="<?php echo $user->emp_id; ?>"><?php echo ucfirst($user->emp_name) . " " . ucfirst($user->emp_surname); ?></option>
                                        <?php endforeach; endif; ?>
                                    </optgroup>
                                </select>
                              </div>
                          </div>
                    </div>
          </div>
        </br>
          <div class="row">


      </fieldset>


  </div>

</div>
  <div class="row">
      <div class="col-lg-12">
          <button type="submit" class="btn btn-block btn-lg" style="background:#828282;border-width:0px;color:#fff;"><i
                  class="fa fa-sign-in"></i> <?php echo lang('form_button_save'); ?></button>
      </div>
  </div>
        </div>
    </div>
</div>
<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url('assets/plugins/summernote/summernote.min.js');?>"></script>
<link href="<?php echo base_url('assets/plugins/summernote/summernote.min.css'); ?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>


<script type="text/javascript">

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });


    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }
</script>
<style media="screen">
textarea {
    margin-top: 10px;
    margin-left: 50px;
    width: 500px;
    height: 100px;
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0.07);
    border-color: -moz-use-text-color #FFFFFF #FFFFFF -moz-use-text-color;
    border-image: none;
    border-radius: 6px 6px 6px 6px;
    border-style: none solid solid none;
    border-width: medium 1px 1px medium;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.12) inset;
    color: #555555;
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 1em;
    line-height: 1.4em;
    padding: 5px 8px;
    transition: background-color 0.2s ease 0s;
}


textarea:focus {
    background: none repeat scroll 0 0 #FFFFFF;
    outline-width: 0;
}
</style>
