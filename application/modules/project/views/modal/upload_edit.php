<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 4/10/2016
 * Time: 2:10 PM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Cerrar</span>
            </button>
            <h4 class="modal-title">Editar Archivo</h4>
        </div>

        <?php echo form_open_multipart(base_url('project/documents/file_edit/' . $file_details->attachment_id), ''); ?>
        <div class="modal-body">
            <input type="hidden" class="form-control" name="project_id"
                   value="<?php echo $file_details->project_id; ?>">
            <input type="hidden" class="form-control" name="task_id" value="<?php echo $file_details->task_id; ?>">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="fileTitle">Titulo de archivo</label>
                        <input class="form-control" type="text" name="file_title"
                               value="<?php echo $file_details->title; ?>" required>

                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="fileDescription">Visible al cliente</label>
                        <select class="form-control"><option>si</option><option>no</option></select>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="fileDescription">Descripcion de archivo</label>
                        <textarea class="form-control rounded" name="file_description"
                                  required><?php echo $file_details->description; ?></textarea>

                    </div>
                </div>
            </div>


        </div>
        <div class="modal-footer">
            <button class="btn btn-success btn-icon" value="addNewTask" name="submit" type="input">
                <i class="fa fa-check-square-o"></i>
                Guardar
            </button>
            <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                <i class="fa fa-times-circle-o"></i>
                Cancelar
            </button>
        </div>
        </form>
    </div>
</div>

