<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Cerrar</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_expense_costs'); ?></h4>
        </div>
        <?php echo form_open_multipart('projects/costs/edit_expense_costs'); ?>
        <div class="modal-body">
            <input class="form-control" type="hidden" name="cost_id" value="<?php echo $expense_details->cost_id; ?>">
            <input class="form-control" type="hidden" name="project_id" value="<?php echo $project_id; ?>">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="taskTitle"><?php echo lang('label_description'); ?></label>
                        <input class="form-control" type="text" name="cost_description"
                               value="<?php echo $expense_details->description; ?>">

                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent"><?php echo lang('label_date'); ?></label>
                        <input type="text" class="form-control datepicker" name="cost_date"
                               data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                               value="<?php echo date('d-m-Y', strtotime($expense_details->date_entry)); ?>">

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent"><?php echo lang('label_classification'); ?></label>

                        <select class="form-control" name="cost_classification">

                            <optgroup label="<?php echo lang('label_category'); ?>">
                                <?php foreach ($categories as $category): ?>
                                    <?php if ($expense_details->classification == $category->class_id) { ?>
                                        <option selected
                                                value="<?php echo $category->class_id; ?>"><?php echo $category->class_name; ?></option>
                                    <?php } else { ?>
                                        <option
                                            value="<?php echo $category->class_id; ?>"><?php echo $category->class_name; ?></option>
                                    <?php } ?>
                                <?php endforeach; ?>
                            </optgroup>
                        </select>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent"><?php echo lang('label_estimate_cost'); ?></label>
                        <input type="text" class="form-control" name="cost_estimate"
                               value="<?php echo $expense_details->estimate_cost; ?>">

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent"><?php echo lang('label_actual_cost'); ?></label>
                        <input type="text" class="form-control" name="cost_actual"
                               value="<?php echo $expense_details->real_cost; ?>">

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <div class="input-group">
                        <input type="text" class="form-control" readonly>
                                                            <span class="input-group-btn">
                                                                <span class="btn btn-primary btn-file">
                                                                    Acceder a archivos<input type="file" multiple
                                                                                       name="expense_receipt">
                                                                </span>
                                                            </span>
                    </div>
                    <!-- /.input-group -->


                </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-info btn-icon" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    <?php echo lang('form_button_save'); ?>
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>
<script type="text/javascript">


    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });
    /** BEGIN INPUT FILE **/
    if ($('.btn-file').length > 0) {
        $(document)
            .on('change', '.btn-file :file', function () {
                "use strict";
                var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });
        $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if (input.length) {
                input.val(log);
            } else {
                // if( log ) alert(log);
            }
        });
    }
</script>
