<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                        <i class="fa fa-times"></i>
                        </span>
                <span class="sr-only">Cerrar</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_milestone'); ?></h4>
        </div>
        <?php echo form_open_multipart('project/milestone/add_milestone_project', array()); ?>

        <div class="modal-body">
            <input type="hidden" class="form-control" name="project_id" value="<?php echo $project_id; ?>">
            <div class="col-md-12">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="taskPercent">Nombre</label>
                        <input type="text" class="form-control" name="milestone_name">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="taskPercent">Importe</label>
                        <input type="text" class="form-control" name="importe">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="taskPercent">Descripción</label>
                        <textarea class="form-control" rows="5" name="milestone_description" required=""></textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskTitle">Fecha de inicio</label>
                        <input type="text" class="form-control datepicker" name="start_date"
                               data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                               value="<?php echo date('d-m-Y'); ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskTitle">Fecha de vencimiento</label>
                        <input type="text" class="form-control datepicker" name="due_date" data-date-format="dd-mm-yyyy"
                               placeholder="dd-mm-yyyy" value="<?php echo date('d-m-Y'); ?>">
                    </div>
                </div>
            </div>

</div>

        </div>
        <div class="modal-footer">
            <button class="btn btn-info btn-icon" name="submit" type="input">
                <i class="fa fa-check-square-o"></i>
                Guardar
            </button>
            <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                <i class="fa fa-times-circle-o"></i>
                Cancelar
            </button>
        </div>
        </form>
    </div>
</div>
<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script type="text/javascript">

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

</script>
