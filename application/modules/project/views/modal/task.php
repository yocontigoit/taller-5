<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_add_task'); ?></h4>
        </div>
        <form method="post" action="<?php echo base_url('project/tasks/add_new/' . $project_id); ?>">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label
                                for="taskTitle"><?php echo lang('label_task_title'); ?> <?php echo $project_id; ?></label>
                            <input class="form-control" type="text" value="" name="task_name" required="">
                <input class="form-control" type="hidden" value="1" name="task_code" required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="catId"><?php echo lang('label_task_dependant'); ?></label>
                            <select class="form-control" name="task_depend">

                                <optgroup label="Task">
                                    <option value="none">ninguna</option>
                                    <?php

                                    if (!empty($project_tasks)) {
                                        foreach ($project_tasks as $project_task) { ?>
                                            <option
                                                value="<?php echo $project_task->task_level . "_" . $project_task->task_id; ?>"><?php echo strtoupper($project_task->task_name); ?></option>
                                        <?php }
                                    } ?>
                                </optgroup>

                            </select>
                            <span class="help-block">Selecciona una tarea anterior.</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="taskPriority"><?php echo lang('label_task_priority'); ?></label>

                            <select class="form-control" name="task_priority">
                                <option value="...">Seleccionar...</option>
                                <?php
                                $tasks_priorities = task_priorities();
                                foreach ($tasks_priorities as $priority) { ?>
                                    ?>
                                    <option
                                        value="<?php echo $priority->priority_id; ?>"><?php echo ucfirst(strtolower($priority->priority_name)); ?></option>

                                <?php } ?>
                            </select>
                            <span class="help-block">Bajo, medio, alto....</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="taskStatus"><?php echo lang('label_task_status'); ?></label>
                            <select class="form-control" name="task_status">
                                <option value="...">Seleccionar...</option>
                                <?php
                                $tasks_status = task_status();
                                foreach ($tasks_status as $status) { ?>
                                    ?>
                                    <option
                                        value="<?php echo $status->status_id; ?>"><?php echo ucfirst(strtolower($status->status_name)); ?></option>

                                <?php } ?>
                            </select>
                            <span class="help-block">Activa, Terminada, etc.</span>
                        </div>
                    </div>

                          <input type="hidden"  name="task_percentage" value="0">



                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="taskStart"><?php echo lang('label_task_assign_to'); ?></label>
                            <select data-placeholder="Asignar" class="form-control chosen-select" multiple tabindex="4"
                                    name="task_assign[]">
                                <optgroup label="<?php echo lang("label_users") ?>">
                                    <?php if (count($project_members)) : foreach ($project_members as $user):
                                        $user = get_user_details($user->user_id);
                                        ?>
                                        <option
                                            value="<?php echo $user->emp_id; ?>"><?php echo ucfirst($user->emp_name) . " " . ucfirst($user->emp_surname); ?></option>
                                    <?php endforeach; endif; ?>
                                </optgroup>
                            </select>
                            <span class="help-block">Personas a las que le corresponde la tarea.</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="taskStatus">Lleva temporizador?</label>
                            <select class="form-control" name="task_timer">
                                <option value="0">No</option>
                                <option value="1">Si</option>

                            </select>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="taskStart"><?php echo lang('label_task_start_date'); ?></label>
                            <input type="text" class="form-control datepicker" name="task_start"
                                   data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                   value="<?php echo date('d-m-Y') ?>">
                            <!--<input id="newtaskStart" class="form-control" type="text" value="" name="task_start" required="">-->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="taskDue"><?php echo lang('label_task_due_date'); ?></label>
                            <input type="text" class="form-control datepicker" name="task_due"
                                   data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                   value="<?php echo date('d-m-Y') ?>">
                            <!--<input id="newtaskDue" class="form-control" type="text" value="" name="task_due" required="">-->
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="taskDesc"><?php echo lang('label_task_description'); ?></label>
                    <textarea class="form-control" rows="5" name="task_desc" required=""></textarea>
                </div>

                <div class="form-group">
                    <label for="taskDesc"><?php echo lang('label_task_requirements'); ?></label>
                    <textarea class="form-control" rows="5" name="task_req" required=""></textarea>
                </div>
                <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="taskStart"><?php echo lang('label_milestone'); ?></label>
                          <select data-placeholder="milestone" class="form-control chosen-select" tabindex="4"
                                  name="task_milestone">
                              <optgroup label="<?php echo lang("label_milestone") ?>">
                                  <option value="0"><?php echo lang('label_none'); ?></option>
                                  <?php if ($this->CompanyUserRolePermission_id != 3) { ?>
                                      <?php if (count($milestones)) : foreach ($milestones as $milestone): ?>
                                          <option
                                              value="<?php echo $milestone->id; ?>"> <?php echo $milestone->milestone_name; ?></option>
                                      <?php endforeach; endif; ?>
                                  <?php } ?>
                              </optgroup>
                          </select>
                          <span class="help-block">Asignar tarea a un avance.</span>
                      </div>
                  </div>
                    <!-- <div class="col-md-3">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label><?php echo lang('label_is_milestone'); ?></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="checkbox" name="task_milestone"
                                           class="ios-switch ios-switch-success ios-switch-lg" <?php //echo ($user_info->active == 1) ? 'checked' :''; ?> />
                                </div>
                            </div>
                            <span class="help-block">va a ser un grupo de tareas (avance)?...</span>
                        </div>
                    </div> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label><?php echo lang('label_is_visible'); ?></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="checkbox" name="task_visible"
                                           class="ios-switch ios-switch-success ios-switch-lg" <?php //echo ($user_info->active == 1) ? 'checked' :''; ?> />
                                </div>
                            </div>
                            <span class="help-block">Hide task from client...</span>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" value="addNewTask" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    <?php echo lang('form_button_save'); ?>
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </button>
            </div>
        </form>
    </div>
</div>
</div>

<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>

<script type="text/javascript">
    $('#tasks-percent').slider({
        formater: function (value) {
            return value + '% Complete..';
        }
    });

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }
</script>
