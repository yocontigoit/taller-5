<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Matriz de riesgo</h4>
        </div>
        <form method="post" action="<?php echo base_url('project/add_risk'); ?>">
            <div class="modal-body">
                <input type="hidden" class="form-control" name="project_id" value="<?php echo $project_id; ?>">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="taskTitle">Nombre de matriz</label>
                            <input class="form-control" type="text" name="risk_name" required="">
                            <span class="help-block">Dale nombre al riesgo</span>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="taskPercent">Probabilidad</label>
                            <input type="text" class="form-control" name="risk_probability">
                            <span class="help-block">probabilidad</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="taskPercent">Porcentaje</label>
                            <input type="text" class="form-control" name="risk_percentage">
                            <span class="help-block">porcentaje.</span>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" value="addNewTask" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    Guardar
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    Cancelar
                </button>
            </div>
        </form>
    </div>
</div>

