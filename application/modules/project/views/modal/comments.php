<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 4/23/2015
 * Time: 11:13 PM
 */
?>
<fieldset>
    <legend><i class="fa fa-comments"></i> Reportes

        <span class="pull-right">
                <a id="show_main-reply" title="Reply comment"
                   class="btn btn-xs btn-danger btn-square"> Agregar Reporte<i
                        class="fa fa-calendar"></i></a>
            </span>

    </legend>
    <div class="row">
      <div class="col-lg-2">

                      <?php if (count($comments)): foreach ($comments as $comment) : ?>
                          <?php $user_details = users_details($comment->message_by_id, $comment->user_type); ?>

                            <div class="row" style="background: #6e7376;color: white;">
                              <div class="col-sm-12" style="background: #6e7376;color: white;">
                                <p><strong>Reporte:</strong> <?php echo $comment->Title; ?>  (<?php echo $comment->Periodo; ?>)</p>
                              </div>
                            <div class="col-sm-12" style="background: #6e7376;color: white;">
                              <p class="date" style="background: #6e7376;color: white;">
                                  <small style="background: #6e7376;color: white;">Fecha: <?php echo $comment->message_date; ?></small>
                              </p>

      </div>

      </div>



                      <?php endforeach; endif; ?>


                  </ul>
                </div>
        <div class="col-md-10">
            <!-- Add comment form -->
            <div id="main-reply" class="block" style="display:none;">
                <div class="well">
                    <?php echo form_open('project/comments/add_project_comment', array('role' => 'form')); ?>

                    <input type="hidden" name="project_id" class="form-control" value="<?php echo $project_id; ?> ">
<div class="row">
  <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent">Titulo</label>
                        <input type="text" class="form-control" name="title"></textarea>
                    </div>
                    </div>
                      <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent">Periodo</label>
                        <input type="text" class="form-control" name="period"></textarea>
                    </div>
                  </div>
                    <div class="form-group">
                        <textarea class="summernote-lg" name="message"></textarea>
                    </div>

                    <div class="form-actions text-right">
                        <button type="submit" class="btn btn-info"><i class="fa fa-rocket"></i> Send</button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>

<div class="col-lg-12">
            <ul class="media-list media-sm media-dotted">

                <?php if (count($comments)): foreach ($comments as $comment) : ?>
                    <?php $user_details = users_details($comment->message_by_id, $comment->user_type); ?>
                    <li class="media">
                      <div class="row" style="background: #6e7376;color: white;">
                        <div class="col-sm-6" style="background: #6e7376;color: white;">
                          <h4><strong>Reporte:</strong> <?php echo $comment->Title; ?>  (<?php echo $comment->Periodo; ?>)</h4>
                        </div>
                        <div class="col-sm-6" style="background: #6e7376;color: white;">

                        <h5 class="media-heading" style="padding-top: 5px;"> ARQ. SUP. DE OBRA: <a href="#fakelink"><?php echo $user_details->full_name; ?></a>
                        </h5>
                      </div>
                      <div class="col-sm-6" style="background: #6e7376;color: white;">
                        <p class="date" style="background: #6e7376;color: white;">
                            <small style="background: #6e7376;color: white;">Fecha: <?php echo $comment->message_date; ?></small>
                        </p>

</div>

</div>
                        <div class="media-body" style="width: 10000px;">

                            <?php echo $comment->message_text; ?>
                            <p align="right" style="margin-top: 0px;" class="comment-action">
                                <a title="Reply comment" data-toggle="collapse"
                                   href="#<?php echo $comment->message_id; ?>_read-reply"
                                   class="btn btn-xs btn-danger btn-square"> Comentar<i
                                        class="fa fa-reply"></i></a>
                            </p>

                            <div id="<?php echo $comment->message_id; ?>_read-reply" class="comment-action collapse">
                                <?php echo form_open('project/comments/add_comment_reply', array('role' => 'form')); ?>
                                <input type="hidden" name="project_id" class="form-control"
                                       value="<?php echo $comment->project_id; ?> ">
                                <input type="hidden" name="message_id" class="form-control"
                                       value="<?php echo $comment->message_id; ?> ">

                                <div class="form-group">
                                    <textarea name="message_reply" placeholder="Mensaje..."
                                              class="form-control"></textarea>
                                </div>
                                <div class="form-actions text-right">
                                    <input type="submit" value="Enviar" class="btn btn-primary">
                                </div>
                                <?php echo form_close(); ?>
                            </div>

                            <?php $user_id = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id; ?>
                            <?php $comment_conversations = project_comments_replies($comment->message_id); ?>
                            <?php if (!empty($comment_conversations)) : ?>

                                <ul class="media-list">
                                    <?php foreach ($comment_conversations as $conversations) : ?>
                                        <?php $user_details = users_details($conversations->replied_by,
                                            $conversations->user_type); ?>
                                        <li class="media">
                                            <a class="pull-left" href="#fakelink">
                                                <img class="media-object img-circle"
                                                     src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                                                     alt="Avatar">
                                            </a>

                                            <div class="media-body">
                                                <h4 class="media-heading"><a
                                                        href="#fakelink"><?php echo $user_details->full_name; ?></a>

                                                </h4>

                                                <p class="date">
                                                    <small><?php echo ago(strtotime($conversations->reply_date)); ?></small>

                                                </p>

                                                <?php echo $conversations->reply_message; ?>

                                            </div>
                                            <?php if (($user_id == $conversations->replied_by && $this->CompanyUser_type == $conversations->user_type) || $this->CompanyUserRolePermission_id == 1): ?>
                                                <span class="pull-right">
                                                 <a href="<?php echo base_url('project/comments/delete_comment/' . $conversations->reply_id . '/' . $project_id); ?>"
                                                    data-toggle="zestModal"><i class="fa fa-trash-o text-danger"></i>
                                                 </a>
                                             </span>
                                            <?php endif; ?>
                                        </li>

                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </li>

                <?php endforeach; endif; ?>


            </ul>
          </div>


        </div>
    </div>

</fieldset>
