<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Cerrar</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_additional_costs'); ?></h4>
        </div>

        <?php echo form_open_multipart('project/costs/add_additional_costs'); ?>
        <div class="modal-body">
            <input type="hidden" class="form-control" name="project_id" value="<?php echo $project_id; ?>">

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="taskTitle"><?php echo lang('label_description'); ?></label>
                        <input class="form-control" type="text" name="cost_description" required>

                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent"><?php echo lang('label_date'); ?></label>
                        <input type="text" class="form-control datepicker" name="cost_date"
                               data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                               value="<?php echo date('d-m-Y') ?>">

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent"><?php echo lang('label_classification'); ?></label>
                        <select class="form-control" name="cost_classification">

                            <optgroup label="<?php echo lang('label_category'); ?>">
                                <?php foreach ($categories as $category): ?>
                                    <option
                                        value="<?php echo $category->class_id; ?>"><?php echo $category->class_name; ?></option>
                                <?php endforeach; ?>
                            </optgroup>
                        </select>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent"><?php echo lang('label_estimate_cost'); ?></label>
                        <input type="text" class="form-control" name="cost_estimate">

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskPercent"><?php echo lang('label_actual_cost'); ?></label>
                        <input type="text" class="form-control" name="cost_actual">

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group">
                        <input type="text" class="form-control" readonly>
                                                            <span class="input-group-btn">
                                                                <span class="btn btn-primary btn-file">
                                                                    Acceder a archivos <input type="file" multiple
                                                                                       name="expense_receipt">
                                                                </span>
                                                            </span>
                    </div>
                    <!-- /.input-group -->


                </div>

            </div>

        </div>
        <div class="modal-footer">
            <button class="btn btn-info btn-icon" name="submit" type="input">
                <i class="fa fa-check-square-o"></i>
                <?php echo lang('form_button_save'); ?>
            </button>
            <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                <i class="fa fa-times-circle-o"></i>
                <?php echo lang('form_button_cancel'); ?>
            </button>
        </div>
        </form>
    </div>
</div>
<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>
<script type="text/javascript">


    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

</script>
