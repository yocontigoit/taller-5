<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/04/12
 * Time: 11:08 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header bg-info">
            <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                    <i class="fa fa-times"></i>
                    </span>
                <span class="sr-only">Cerrar</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_delete'); ?></h4>
        </div>
        <form method="post" action="<?php echo base_url('project/delete'); ?>">
            <div class="modal-body">
                <p><?php echo lang('messages_delete_project_warning') ?></p>

                <input type="hidden" name="project_id" value="<?php echo $project_id; ?>">

                <div class="modal-footer">
                    <button class="btn btn-info btn-icon" value="addNewTask" name="submit" type="input">
                        <i class="fa fa-check-square-o"></i>
                        <?php echo lang('form_button_continue'); ?>
                    </button>
                    <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                        <i class="fa fa-times-circle-o"></i>
                        <?php echo lang('form_button_cancel'); ?>
                    </button>
                </div>


            </div>
        </form>
    </div>


</div>
