<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
                    <link rel="icon" type="image/png" href="https://proyectosinternos.com/Taller5/assets/images/site_favicon.png">
            <title></title>

        <link href="https://proyectosinternos.com/Taller5/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="https://proyectosinternos.com/Taller5/assets/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="https://proyectosinternos.com/Taller5/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="https://proyectosinternos.com/Taller5/assets/css/chat.css" rel="stylesheet" type="text/css" />
        <link href="https://proyectosinternos.com/Taller5/assets/plugins/morris-chart/morris.min.css" rel="stylesheet" type="text/css" />
        <link href="https://proyectosinternos.com/Taller5/assets/plugins/nifty-modal/css/component.css" rel="stylesheet" type="text/css" />
        <link href="https://proyectosinternos.com/Taller5/assets/plugins/ios7-switch/ios7-switch.css" rel="stylesheet" type="text/css" />
        <link href="https://proyectosinternos.com/Taller5/assets/plugins/chosen/chosen.min.css" rel="stylesheet" type="text/css" />
        <link href="https://proyectosinternos.com/Taller5/assets/plugins/slider/slider.min.css" rel="stylesheet" type="text/css" />
        <link href="https://proyectosinternos.com/Taller5/assets/plugins/toastr/toastr.css" rel="stylesheet" type="text/css" />
        <link href="https://proyectosinternos.com/Taller5/assets/css/dashboard.css" rel="stylesheet" type="text/css" />
        <link href="https://proyectosinternos.com/Taller5/assets/css/sb-admin.css" rel="stylesheet" type="text/css" />
        <link href="https://proyectosinternos.com/Taller5/assets/css/default.css" rel="stylesheet" type="text/css" />
        <link href="https://proyectosinternos.com/Taller5/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript">
        var base_url = "https://proyectosinternos.com/Taller5/";
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesnt work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="js/ie/html5shiv.js" cache="false">
    </script>
    <script src="js/ie/respond.min.js" cache="false">
    </script>
    <script src="js/ie/excanvas.js" cache="false">
    </script> <![endif]-->
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                        <i class="fa fa-times"></i>
                        </span>
                <span class="sr-only">Cerrar</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_milestone'); ?></h4>
        </div>
        <?php echo form_open_multipart('project/milestone/edit_milestone', array()); ?>


            <input type="hidden" class="form-control" name="project_id" value="<?php echo $project_id; ?>">
            <input type="hidden" class="form-control" name="milestone_id" value="<?php echo $milestone->id; ?>">
<div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="taskPercent">Nombre</label>
                        <input type="text" class="form-control" value="<?php echo $milestone->milestone_name; ?>"
                               name="milestone_name">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="taskPercent">Importe</label>
                        <input type="text" class="form-control" value="<?php echo $milestone->importe; ?>"
                               name="importe">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="taskPercent">Descripcion</label>
                        <textarea class="form-control" rows="5" name="milestone_description"
                                  required=""><?php echo $milestone->description; ?></textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskTitle">Fecha de inicio</label>
                        <input type="text" class="form-control datepicker" name="start_date"
                               data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                               value="<?php echo date('d-m-Y', strtotime($milestone->start_date)); ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="taskTitle">Fecha de vencimiento</label>
                        <input type="text" class="form-control datepicker" name="due_date" data-date-format="dd-mm-yyyy"
                               placeholder="dd-mm-yyyy"
                               value="<?php echo date('d-m-Y', strtotime($milestone->due_date)); ?>">
                    </div>
                </div>
            </div>
  </div>
            <div class="col-md-6">
                <h4>Tareas</h4>
              <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-info"><thead><th>Tarea</th><th>Progreso</th></thead><tbody><tr><td></td><td></td></tr><tr><td></td><td></td></tr></tbody></table>
              <h4>Ejercido</h4>
              <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-info"><thead><th>Orden</th><th>Monto</th></thead><tbody><?php $importeavance =0; if(count($avances)): foreach($avances as $avance): if($avance->item_avance == $milestone->milestone_name):  $importeavance = $importeavance + ($avance->price * $avance->quantity);
            ?>
              <tr>
                  <td><?php echo $avance->inv_no ?></td>
                  <td><?php echo $avance->price *$avance->quantity ?></td>
              </tr>
                  
                  
                  <?php  endif;endforeach;endif;?></tbody></table>
              </div>

        <div class="modal-footer">
            <button class="btn btn-info btn-icon" name="submit" type="input">
                <i class="fa fa-check-square-o"></i>
                Guardar
            </button>
            <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                <i class="fa fa-times-circle-o"></i>
                Cancelar
            </button>
        </div>
        </form>
    </div>
</div>
<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script type="text/javascript">

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

</script>
<div class="md-overlay"></div>




    <script src="https://proyectosinternos.com/Taller5/assets/libs/jquery-2.0.2.min.js" ></script>
    <script src="https://proyectosinternos.com/Taller5/assets/libs/jquery-ui-1.10.3.min.js" ></script>
    <script src="https://proyectosinternos.com/Taller5/assets/js/jquery-migrate.js" ></script>
    <script src="https://proyectosinternos.com/Taller5/assets/js/bootstrap.min.js" ></script>
    <script src="https://proyectosinternos.com/Taller5/assets/plugins/nicescroll/jquery.nicescroll.js" ></script>
    <script src="https://proyectosinternos.com/Taller5/assets/plugins/chosen/chosen.jquery.min.js" ></script>
    <script src="https://proyectosinternos.com/Taller5/assets/plugins/ios7-switch/ios7.switch.js" ></script>
    <script src="https://proyectosinternos.com/Taller5/assets/plugins/nifty-modal/js/classie.js" ></script>
    <script src="https://proyectosinternos.com/Taller5/assets/plugins/nifty-modal/js/modalEffects.js" ></script>
    <script src="https://proyectosinternos.com/Taller5/assets/plugins/slider/bootstrap-slider.js" ></script>
    <script src="https://proyectosinternos.com/Taller5/assets/plugins/toastr/toastr.js" ></script>
    <script src="https://proyectosinternos.com/Taller5/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js" ></script>
    <script src="https://proyectosinternos.com/Taller5/assets/js/app.js" ></script>
    <script src="https://proyectosinternos.com/Taller5/assets/js/custom.js" ></script>
    <script src="https://proyectosinternos.com/Taller5/assets/js/zest_chat.js" ></script>












    <script src="https://proyectosinternos.com/Taller5/assets/plugins/easypiechart/jquery.easy-pie-chart.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('.milestone-progress').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#A3E135',
                lineWidth: 10,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });
        });
    </script>















    <link href="https://proyectosinternos.com/Taller5/assets/plugins/datatable/css/bootstrap.datatable.min.css" rel="stylesheet" type="text/css" />
    <script src="https://proyectosinternos.com/Taller5/assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
    <script src="https://proyectosinternos.com/Taller5/assets/plugins/datatable/js/bootstrap.datatable.js"></script>

    
        <script src="https://proyectosinternos.com/Taller5/assets/js/timer.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){

                var timer = '';

                $.post(base_url + 'project/get_active_timer', {project_id:30}, function(timer_response){
                    var response = JSON.parse(timer_response);
                    if (response.has_timer)
                    {
                        $('#navbar_timer').removeClass('hide');
                        button = '';

                        timer_offset = response.timer_offset;
                        timer = $('#project-timer').timer({startVal : timer_offset});
                        timer.startTimer();

                        button = '<a class="btn pro-countdown-timer btn-info" href="#" id="pro-start-stop" data-toggle="tooltip" title="Start / Stop recording"><i class="fa fa-stop"></i></a>';
                        $('.pro-start-stop-div').html(button);

                    }
                    else
                    {

                        timer = $('#project-timer').timer();

                    }
                });


                $(document).on('click', '.pro-countdown-timer', function(e) {
                    id = $(this).attr('id');
                    e.preventDefault();
                    $('.error').remove();
                    toggle_timer(timer, id);
                });


                function toggle_timer(timer, id)
                {

                    if(id == 'pro-start-timer')
                    {

                        $.post(base_url + 'project/project_timer', {start_timer : 'true',project_id:30}, function(timer_response){
                            var response = JSON.parse(timer_response);
                            if (response.success)
                            {
                                $('#navbar_timer').removeClass('hide');
                                timer.startTimer();
                                button = '';

                                button = '<a class="btn pro-countdown-timer btn-info" href="#" id="pro-stop-timer" data-toggle="tooltip" title="Stop recording"><i class="fa fa-stop"></i></a>';

                                $('.pro-start-stop-div').html(button);
                            }
                            else {
                                alert(response.error);
                            }
                        });
                    }
                    else
                    {
                        $.post(base_url + 'project/project_timer', {project_id :30, start_timer : 'false'}, function(timer_response){
                            var response = JSON.parse(timer_response);
                            if (response.success)
                            {
                                $('#navbar_timer').addClass('hide');
                                button = '';
                                button = '<a class="btn pro-countdown-timer btn-danger" href="#" id="pro-start-timer" data-toggle="tooltip" title="Start recording"><i class="fa fa-play"></i></a>';

                                $('.pro-start-stop-div').html(button);
                                timer.resetTimer();
                                // window.location.reload(true);
                            }
                            else {
                                alert(response.error);
                            }
                        });
                    }

                }
            });

        </script>




<!-- START initialization -->
<script type="text/javascript">
    $(document).ready(function(){
        Zest.init(); // initlayout and core plugins
        zestCHAT.initializeBase(); // initialise chat
        $("body").tooltip({ selector: '[data-toggle=tooltip]' });
        $(".ios-switch").each(function(){
            mySwitch = new Switch(this);
        });

        $('.table-responsive').on('shown.bs.dropdown', function (e) {
            var $table = $(this),
                $menu = $(e.target).find('.dropdown-menu'),
                tableOffsetHeight = $table.offset().top + $table.height(),
                menuOffsetHeight = $menu.offset().top + $menu.outerHeight(true);

            if (menuOffsetHeight > tableOffsetHeight)
                $table.css("padding-bottom", menuOffsetHeight + 20 - tableOffsetHeight);
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('#salestimate_stats').highcharts().setSize($('#salestimate_stats').width(),$('#salestimate_stats').height());
        });


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('#category-stats').highcharts().setSize($('#category-stats').width(),$('#category-stats').height());
        });
        $('.table-responsive').on('hide.bs.dropdown', function () {
            $(this).css("padding-bottom", 0);
        });


        /** BEGIN INPUT FILE **/
        if ($('.btn-file').length > 0){
            $(document)
                .on('change', '.btn-file :file', function() {
                    "use strict";
                    var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [numFiles, label]);
                });
            $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

                if( input.length ) {
                    input.val(log);
                } else {
                    // if( log ) alert(log);
                }
            });
        }
    });



</script>
