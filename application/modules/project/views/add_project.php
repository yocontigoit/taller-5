
















<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/01/26
 * Time: 11:16 AM
 */
?>

<div class="row">
    <div class="col-sm-12">
        <div class="the-box">
            <fieldset>
                <legend><i class="fa fa-bar-chart-o"></i><?php echo strtoupper(lang('label_add_project')); ?></legend>
                <div class="row">
                    <?php echo validation_errors(); ?>
                    <?php echo form_open('project/add',
                        array('class' => 'panel form-horizontal form-bordered', 'id' => 'add-project')); ?>
                    <div class="col-md-7">
                        <div class="tab-content">
                            <!--Start adding new estimate -->
                            <div class="tab-pane fade in active" id="estimate">


                                <div class="panel-body">
                                    <div class="form-group header bgcolor-default">
                                        <div class="col-md-12">
                                            <h4><?php echo lang('label_project_detail_setting'); ?></h4>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label"><?php echo lang('label_project_title'); ?></label>

                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" name="project_title"
                                                   value="<?php echo set_value('project_title'); ?>"/>
                                                   <input class="form-control" type="hidden" name="project_code"
                                                          value="<?php echo $project_code; ?>"/>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label"><?php echo lang('label_project_client'); ?></label>

                                        <div class="col-sm-8">

                                            <select data-placeholder="Select client..."
                                                    class="form-control  chosen-select" tabindex="4" name="client">
                                                <option value="Empty">&nbsp;</option>

                                                <optgroup label="<?php echo lang("label_clients") ?>">
                                                    <?php if (count($clients)):foreach ($clients as $client): if($client->client_type == 1): ?>
                                                        <option
                                                            value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name) ?></option>
                                                    <?php endif; endforeach; endif; ?>
                                                </optgroup>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label
                                          class="col-sm-3 control-label">Tipo de Proyecto</label>

                                          <div class="col-sm-8">

                                              <select data-placeholder="Select client..."
                                                      class="form-control  chosen-select" tabindex="4" name="type">
                                                  <option value="Empty">Elige..</option>
                                                  <option value="Proyecto">Proyecto</option>
                                                  <option value="Coordinación de Oba">Coordinación de Obra</option>
                                                  <option value="Supervisión Arquitectonica">Supervisión Arquitectonica</option>
                                                  <option value="Proyecto y Supervisión">Proyecto y Supervisión</option>

                                              </select>

                                          </div>
                                  </div>


                                    <div class="form-group header bgcolor-default mt-20">
                                        <div class="col-md-12">
                                            <h4><?php echo lang('label_project_setting'); ?></h4>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label"><?php echo lang('label_project_assign_to'); ?></label>

                                        <div class="col-sm-8">
                                            <select data-placeholder="Select users..."
                                                    class="form-control chosen-select" multiple tabindex="4"
                                                    name="assign_to[]">
                                                <optgroup label="<?php echo lang("label_users") ?>">
                                                    <?php if (count($users)) : foreach ($users as $user): ?>
                                                        <option
                                                            value="<?php echo $user->user_id; ?>"><?php echo ucfirst($user->emp_name) . " " . ucfirst($user->emp_surname); ?></option>
                                                    <?php endforeach; endif; ?>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label"><?php echo lang('label_project_budget'); ?></label>

                                        <div class="col-sm-4">
                                            <input class="form-control" type="text" placeholder="0.00" name="budget"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label">Fondo de Avance Obra</label>

                                        <div class="col-sm-4">
                                            <input class="form-control" type="text" placeholder="0.00" name="fondo"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label">TRASLADOS, ACARREOS HERRAM Y EQUIPO MENOR, ETC.</label>

                                        <div class="col-sm-4">
                                            <input class="form-control" type="text" placeholder="0.00" name="traslados"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label">HONORARIOS SUPERVISIÓN, ADMINISTRACIÓN Y DIRECCIÓN.</label>

                                        <div class="col-sm-4">
                                            <input class="form-control" type="text" placeholder="0.00" name="honorarios"/>
                                        </div>
                                    </div>
                                       <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label">HONORARIOS POR CANTIDAD.</label>
                                        <div class="col-sm-4">
                                            <input class="form-control" type="text" placeholder="0.00" name="honorarios_cantidad"/>
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label">DIRECCIÓN DEL PROYECTO.</label>
                                        <div class="col-sm-4">
                                            <input class="form-control" type="text" placeholder="Calle,No." name="direccion" />
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <div class="col-sm-3 control-label">
                                            <label><?php echo lang('label_use_worklog'); ?></label>
                                        </div>


                                        <div class="col-sm-8">
                                            <input type="checkbox" name="project_worklog" id="project_worklog"
                                                   class="ios-switch ios-switch-success ios-switch-lg"/>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-lg-3 control-label"><?php echo lang('label_project_progress'); ?></label>

                                        <div class="col-lg-8 progress-box">
                                            <div id="progress-slider"></div>
                                            <input id="progress" type="hidden" value="0" name="progress"/>
                                        </div>
                                    </div>


                                    <div class="form-group header bgcolor-default mt-10">
                                        <div class="col-md-12">
                                            <h4><?php echo lang('label_project_date_setting'); ?></h4>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label"><?php echo lang('label_project_start_date'); ?></label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control datepicker" id="start_date"
                                                   name="start_date" data-rule-required="true"
                                                   data-msg-required="required" data-date-format="dd-mm-yyyy"
                                                   placeholder="dd-mm-yyyy" value="<?php echo date('d-m-Y') ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label"><?php echo lang('label_project_end_date'); ?></label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control datepicker" name="end_date"
                                                   data-rule-greaterThanOrEqual="#start_date"
                                                   data-msg-greaterThanOrEqual="End data must be greater or equal to start date"
                                                   data-rule-required="true" data-msg-required="required"
                                                   data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                                   value="<?php echo date('d-m-Y') ?>">

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label"><?php echo lang('label_project_description'); ?></label>

                                        <div class="col-sm-8">
                                            <textarea class="form-control rounded summernote-sm" name="project_desc"
                                                      style="height:100%"></textarea>
                                        </div>
                                    </div>

                                </div>
                                <hr/>
                                <button type="input" name="submit" value="editTask"
                                        class="btn btn-success btn-lg btn-icon mt-10"><i
                                        class="fa fa-check-square-o"></i> <?php echo lang('form_button_add_project'); ?>
                                </button>

                            </div>

                        </div>

                    </div>
                    <div class="col-md-5" id="form">
                        <div class="tab-content">
                            <!--Start adding new estimate -->
                            <div class="tab-pane fade in active" id="estimate">

                                <div class="panel-body">
                                    <div class="form-group header bgcolor-default">
                                        <div class="col-md-12">
                                            <h4><?php echo lang('label_project_permission_setting'); ?></h4>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    </div>

                                    <?php
                                    $current_permissions = json_decode($current_permissions);

                                    foreach ($project_permissions as $key => $p) { ?>
                                        <div class="checkbox checkbox-primary pad-left">
                                            <input name="settings[<?php echo $p->setting ?>]" <?php
                                            if (array_key_exists($p->setting, $current_permissions)) {
                                                echo "";
                                            } elseif ($current_permissions->settings == 'on') {
                                                echo "";
                                            }

                                            ?>  type="checkbox" >
                                            <label>
                                                <?php echo lang('label_' . $p->setting) ?>
                                            </label>
                                        </div>
                                        <hr class="no-margin"/>
                                    <?php } ?>

                                </div>
                                <!-- end panel -->
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </fieldset>
        </div>
    </div>
</div>
