<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/05/12
 * Time: 04:43 PM
 */
?>
<?php

$percent_finished = check_list_complete_percentage($task_id);
?>
<div class="progress no-margin" style="height:30px;">
    <div class="progress-bar progress-bar-default task-progress-bar" role="progressbar" aria-valuenow="40"
         aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $percent_finished; ?>%;height: 20px;">
        <?php echo $percent_finished; ?>%
    </div>
</div>
<hr/>
<div class="clearfix"></div>
<?php if (count($check_lists) > 0) { ?>
    <h4 class="bold chk-heading"><?php echo lang('label_checklist_items'); ?></h4>
<?php } ?>
<?php if (count($check_lists)) {
    foreach ($check_lists as $list) { ?>
        <div class="checklist" data-checklist-id="<?php echo $list->id; ?>">
            <div class="checkbox checkbox-success checklist-checkbox" data-toggle="tooltip" title="">
                <input type="checkbox" name="checklist-box" <?php if ($list->finished == 1) {
                    echo 'checked';
                }; ?>>
                <label for=""><span class="hide"><?php echo $list->description; ?></span></label>
                <textarea width="95%" style="width:90%;" name="checklist-description" rows="1"><?php echo $list->description; ?></textarea>
                <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                    <a href="#" class="pull-right text-muted remove-checklist" data-id="<?php echo $list->id; ?>"><i
                            class="fa fa-remove"></i></a>
                <?php } ?>
            </div>
        </div>
    <?php }
} ?>
