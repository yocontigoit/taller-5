<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 4/10/2016
 * Time: 9:52 AM
 */
?>

<div class="row">
    <div class="col-sm-12">
        <div class="the-box">
            <fieldset>
                <legend><i class="fa fa-bar-chart-o"></i><?php echo strtoupper(lang('label_edit_project')); ?></legend>

                <div class="row">
                    <?php echo validation_errors(); ?>
                    <?php echo form_open('project/edit_project',
                        array('class' => 'panel form-horizontal form-bordered', 'id' => 'add-project')); ?>
                    <div class="col-md-7">

                        <div class="tab-content">
                            <!--Start adding new estimate -->
                            <div class="tab-pane fade in active" id="estimate">

                                <div class="panel-body">
                                    <div class="form-group header bgcolor-default">
                                        <div class="col-md-12">
                                            <h4><?php echo lang('label_project_detail_setting'); ?></h4>
                                        </div>
                                    </div>
                                    <input class="form-control" type="hidden" name="project_id"
                                           value="<?php echo $project_id; ?>"/>

                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label"><?php echo lang('label_project_code'); ?></label>

                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" readonly name="project_code"
                                                   value="<?php echo $project_details->project_code; ?>"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label"><?php echo lang('label_project_title'); ?></label>

                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" name="project_title"
                                                   value="<?php echo $project_details->project_title; ?>"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label"><?php echo lang('label_project_client'); ?></label>

                                        <div class="col-sm-8">

                                            <select data-placeholder="Select client..." class="form-control"
                                                    tabindex="4" name="client">
                                                <option value="Empty">&nbsp;</option>

                                                <optgroup label="<?php echo lang("label_clients") ?>">
                                                    <?php if (count($clients)):foreach ($clients as $client): ?>
                                                        <?php if ($project_details->client_id == $client->client_id) { ?>
                                                            <option selected="selected"
                                                                    value="<?php echo $project_details->client_id; ?>"><?php echo ucfirst($client->client_name) ?></option>
                                                        <?php } else { ?>
                                                            <option
                                                                value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name) ?></option>
                                                        <?php } ?>
                                                    <?php endforeach; endif; ?>
                                                </optgroup>
                                            </select>

                                        </div>
                                    </div>


                                    <div class="form-group header bgcolor-default mt-20">
                                        <div class="col-md-12">
                                            <h4><?php echo lang('label_project_setting'); ?></h4>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label"><?php echo lang('label_project_assign_to'); ?></label>

                                        <div class="col-sm-8">
                                            <select data-placeholder="Select users..."
                                                    class="form-control chosen-select" multiple tabindex="4"
                                                    name="assign_to[]">
                                                <optgroup label="<?php echo lang("label_users") ?>">
                                                    <?php if (count($users)) : foreach ($users as $user): ?>

                                                        <option
                                                            value="<?php echo $user->user_id; ?>" <?php foreach ($assigned_team as $assigned) {
                                                            if ($user->user_id == $assigned->user_id) { ?> selected="selected" <?php } else {
                                                            }
                                                        } ?>>
                                                            <?php echo ucfirst($user->emp_name) . " " . ucfirst($user->emp_surname); ?>
                                                        </option>

                                                    <?php endforeach; endif; ?>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label">Presupuesto de proyecto</label>

                                        <div class="col-sm-4">
                                            <input class="form-control" type="text" placeholder="0.00" name="budget"
                                                   value="<?php echo $project_details->project_budget; ?> "/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label">Fondo Avance de Obra</label>

                                        <div class="col-sm-4">
                                            <input class="form-control" type="text" placeholder="0.00" name="fondo"
                                                   value="<?php echo $project_details->fondo; ?> "/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label">TRASLADOS, ACARREOS HERRAM Y EQUIPO MENOR, ETC.</label>

                                        <div class="col-sm-4">
                                            <input class="form-control" type="text" placeholder="0.00" name="traslados" value="<?php echo $project_details->project_traslado ?> "/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label">HONORARIOS SUPERVISIÓN, ADMINISTRACIÓN Y DIRECCIÓN.</label>
                                        <div class="col-sm-4">
                                            <input class="form-control" type="text" placeholder="0.00" name="honorarios" value="<?php echo $project_details->project_honorario; ?> "/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label">HONORARIOS POR CANTIDAD.</label>
                                        <div class="col-sm-4">
                                            <input class="form-control" type="text" placeholder="0.00" name="honorarios_cantidad" value="<?php echo $project_details->project_cantidad; ?> "/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label">DIRECCIÓN DEL PROYECTO.</label>
                                        <div class="col-sm-4">
                                            <input class="form-control" type="text" placeholder="Calle,No." name="direccion" value="<?php echo $project_details->project_direccion; ?> "/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label">HORARIO DE ENTREGA.</label>
                                        <div class="col-sm-4">
                                          <div class='input-group date' id='datetimepicker3'>
                                            <input class="form-control" type="text" placeholder="" name="horario" value="<?php echo $project_details->project_horario; ?> "/>
                                            <span class="input-group-addon">
                                              <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                          </div>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-sm-3 control-label">
                                            <label><?php echo lang('label_use_worklog'); ?></label>
                                        </div>


                                        <div class="col-sm-8">
                                            <input type="checkbox" name="project_worklog" id="project_worklog"
                                                   class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($project_details->project_use_worklog == 1) ? 'checked' : ''; ?> />
                                        </div>

                                    </div>

                                    <?php

                                    $pro_percentage = 0;
                                    if ($project_details->project_use_worklog == 0) {
                                        $pro_percentage = $project_details->project_progress;
                                    } else {
                                        $pro_percentage = calculate_project_percentage($project_details->project_id);

                                    }

                                    ?>
                                    <div class="form-group">
                                        <label
                                            class="col-lg-3 control-label"><?php echo lang('label_project_progress'); ?></label>

                                        <div class="col-lg-8 progress-box">
                                            <div id="progress-slider"></div>
                                            <input id="progress" type="hidden" value="<?php echo $pro_percentage; ?>"
                                                   name="progress"/>
                                        </div>
                                    </div>


                                    <div class="form-group header bgcolor-default mt-10">
                                        <div class="col-md-12">
                                            <h4><?php echo lang('label_project_date_setting'); ?></h4>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label"><?php echo lang('label_project_start_date'); ?></label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control datepicker" name="start_date"
                                                   data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                                   value="<?php echo date('d-m-Y',
                                                       strtotime($project_details->project_start_date)); ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label"><?php echo lang('label_project_end_date'); ?></label>

                                        <div class="col-sm-8">
                                            <input type="text" class="form-control datepicker" name="end_date"
                                                   data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                                                   value="<?php echo date('d-m-Y',
                                                       strtotime($project_details->project_due_date)); ?>">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label
                                            class="col-sm-3 control-label"><?php echo lang('label_project_description'); ?></label>

                                        <div class="col-sm-8">
                                            <textarea class="form-control rounded summernote-sm" name="project_desc"
                                                      style="height:100%"><?php echo $project_details->project_desc; ?></textarea>
                                        </div>
                                    </div>

                                </div>
                                <hr/>
                                <button type="input" name="submit" value="editTask"
                                        class="btn btn-success btn-lg btn-icon mt-10"><i
                                        class="fa fa-check-square-o"></i> <?php echo lang('form_button_edit_project'); ?>
                                </button>
                            </div>

                        </div>
                    </div>
                    <!-- end col-7 -->
                    <div class="col-md-5">
                        <div class="tab-content">
                            <!--Start adding new estimate -->
                            <div class="tab-pane fade in active" id="estimate">

                                <div class="panel-body">
                                    <div class="form-group header bgcolor-default">
                                        <div class="col-md-12">
                                            <h4><?php echo lang('label_project_permission_setting'); ?></h4>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    </div>

                                    <?php

                                    if ($current_permissions == null) {
                                        $current_permissions = '{"settings":"on"}';
                                    }
                                    $current_permissions = json_decode($current_permissions);

                                    foreach ($project_permissions as $key => $p) { ?>
                                        <div class="checkbox checkbox-primary pad-left">
                                            <input name="settings[<?php echo $p->setting ?>]" <?php
                                            if (array_key_exists($p->setting, $current_permissions)) {
                                                echo "checked=\"checked\"";
                                            } elseif (isset($current_permissions->settings) && $current_permissions->settings == 'on') {
                                                echo "checked=\"checked\"";
                                            }

                                            ?>  type="checkbox">
                                            <label>
                                                <?php echo lang('label_' . $p->setting) ?>
                                            </label>
                                        </div>
                                        <hr class="no-margin"/>
                                    <?php } ?>

                                </div>
                                <!-- end panel -->


                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>

                </div>

            </fieldset>
        </div>
    </div>
</div>
