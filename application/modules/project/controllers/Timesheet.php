<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/03/10
 * Time: 10:33 AM
 */
class Timesheet extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();

        $this->load->model('project_m');
    }


    /**
     * list all time sheet/logged times
     */
    function preview()
    {

        $this->data['option'] = $this->uri->segment(2);
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Time Sheet';


        $this->data['project_id'] = $this->uri->segment(4);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }


        if ($this->CompanyUserRolePermission_id == 3) {

            $this->data['manual_timelogs'] = $this->project_m->get_manual_logged_time(intval($this->data['project_id']));
            $this->data['_PAGE'] = $this->load->view('groups/timesheet_client', $this->data, true);

        } else {
            $this->data['project_tasks'] = $this->project_m->get_project_tasks(intval($this->data['project_id']));
            $this->data['assigned_team'] = $this->project_m->get_assigned_team(intval($this->data['project_id']));
            $this->data['manual_timelogs'] = $this->project_m->get_manual_logged_time(intval($this->data['project_id']));
            $this->data['show_datepicker'] = true;
            $this->data['show_log_calendar'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);
        }


        $this->load->view('_main_layout', $this->data);
    }


    /**
     * add/log user time entries
     * @param null $project_id
     */
    function log_time($project_id = null)
    {

        if ($this->input->post()) {

            $start_date = $this->input->post('start_time');
            $start_time = $this->input->post('start_timer');
            $start_time = strtotime($start_date . ' ' . $start_time);


            $end_date = $this->input->post('end_time');
            $end_time = $this->input->post('end_timer');
            $end_time = strtotime($end_date . ' ' . $end_time);
            $timer_entry = array(
                'task_id' => $this->input->post('client_tasks'),
                'project_id' => $project_id,
                'entry_by' => $this->CompanyUser_id,
                'user_id' => $this->input->post('tasks_for'),
                'start_timer' => $start_time,
                'stop_timer' => $end_time,
                'entry_date' => date('Y-m-d,H:i:s'),
                'manual_entry' => 1

            );

            if ($this->project_m->log_task_timesheet($timer_entry)) {

                redirect('project/timesheet/preview/' . $project_id . '/timesheet');
            }

        }
    }

    /**
     * delete log
     * @param null $log_id
     */
    function delete_log($log_id = null)
    {
        $log_id = intval($log_id);

        if ($this->project_m->remove_task_log($log_id)) {
            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', 'Log removed');
            redirect($_SERVER['HTTP_REFERER']);
        }

    }

}