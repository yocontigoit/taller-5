<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 4/23/2015
 * Time: 11:11 PM
 */
class Issues extends Admin_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('project_m');
    }


    /**
     * list all issues
     */
    function preview()
    {

        $this->data['option'] = $this->uri->segment(2);
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Contingencias | Proyecto';

        $this->data['project_id'] = $this->uri->segment(4);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }
        $this->data['project_issues'] = $this->project_m->get_project_issues(intval($this->data['project_id']));
        $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);
        $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * view selected issue info
     * @param null $issue_id
     */
    function view($issue_id = null)
    {

        $issue_id = intval($issue_id);
        $this->data['title'] = 'Project | Issues';
        $this->data['content'] = 'project/project_details';


        $this->data['issue_details'] = $this->project_m->get_issue_details($issue_id);
        $this->data['issue_replies'] = $this->project_m->get_issue_messages($issue_id);
        $this->data['project_id'] = $this->data['issue_details']->project_id;
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        //check if issue belong to logged in client
        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }
        $this->data['_PAGE'] = $this->load->view('groups/issue_details', $this->data, true);
        $this->data['message'] = true;
        $this->data['SCRIPT_PAGE'] = true;

        $this->load->view('_main_layout', $this->data);
    }

    /**
     * reply to issue
     */
    function reply_issue()
    {

        if ($this->input->post()) {

            $reply_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $issue_id = intval($this->input->post('issue_id'));
            $reply_data = array(
                "issue_id" => $issue_id,
                "reply_date" => date('Y-m-d,H:i:s'),
                "reply_message" => $this->input->post('reply_message'),
                "reply_user_id" => $reply_user,
                "reply_user_type" => $this->CompanyUser_type
            );

            if ($this->project_m->add_issue_reply($reply_data)) {
                $log_params = array(
                    'activity_user' => $reply_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => "replied to an issue",
                    'activity_module_id' => $issue_id,
                    'activity_status' => "success",
                    'activity_event' => "Respondido al caso" . $issue_id,
                    'activity_icon' => 'fa fa-user'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_issue_reply_success'));
                redirect('project/issues/view/' . $issue_id);
            }

        }

    }

    /**
     * save issue activities
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->project_m->add_log($activity_data);

    }

    /**
     * add issue
     * @param null $project_id
     */
    function add($project_id = null)
    {
        $project_id = intval($project_id);
        $this->data['project_id'] = $project_id;
        $this->data['project_tasks'] = $this->project_m->get_project_tasks($project_id);
        $this->data['users'] = $this->project_m->get_company_users($this->Company_id);
        $this->load->view('modal/issues', $this->data);
    }

    /**
     * save issue
     */
    function add_issue()
    {

        if ($this->input->post()) {
            $project_id = $this->input->post('project_id');

            $file_name = '';

            if (file_exists($_FILES['issue_upload']['tmp_name']) || is_uploaded_file($_FILES['issue_upload']['tmp_name'])) {
                $file_name = $this->_upload_issue_file($_POST);

            }
            $param = array(
                "project_id" => $project_id,
                "issue_priority" => $this->input->post('issue_priority'),
                "issue_status" => $this->input->post('issue_status'),
                "issue_description" => $this->input->post('issue_description'),
                "task_id" => $this->input->post('tasks'),
                "user_id" => $this->input->post('assign_to'),
                "issue_date" => date('Y-m-d', strtotime($this->input->post('issue_date'))),
                "entry_date" => date('Y-m-d H:i:s'),
                "issue_files" => $file_name,
                "issue_by" => $this->CompanyUser_id
            );

            $issue_id = $this->project_m->add_issue($param);
            if ($issue_id) {
                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => "New issue",
                    'activity_module_id' => $issue_id,
                    'activity_status' => "success",
                    'activity_event' => "Nuevo caso agregado",
                    'activity_icon' => 'fa fa-bug'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_issue_add_success'));
                redirect('project/issues/view/' . $issue_id);

            }
        }
    }

    /**
     * save issue attachment to directory
     * @param null $image_file
     * @return null|string
     */
    function _upload_issue_file($image_file = null)
    {

        $file_name = null;
        $success = false;
        if ($image_file) {
            $config['upload_path'] = './files/issue_attachments/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['remove_spaces'] = true;

            $config['overwrite'] = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('issue_upload')) {
                $data = $this->upload->data();
                $file_name = $data['file_name'];
                $success = true;
            } else {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', lang('messages_file_add_error'));

            }
        } else {
            $success = false;
        }

        return ($success) ? $file_name : '';
    }

    /**
     * assign issue to user
     * @param null $issue_id
     */
    function assign($issue_id = null)
    {

        if ($this->input->post()) {

            $issue_id = intval($this->input->post('issue_id'));
            $assigned_to = $this->input->post('issue_assign');
            $param = array(
                "user_id" => $assigned_to

            );
            if ($this->project_m->update_issue_assignment($param, $issue_id)) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => "Issue reassigned",
                    'activity_module_id' => $issue_id,
                    'activity_status' => "success",
                    'activity_event' => "Caso reasignado",
                    'activity_icon' => 'fa fa-bug'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_issue_save_success'));
                redirect('project/issues/view/' . $issue_id);
            }
        } else {


            $this->data['issue_id'] = $issue_id;
            $this->data['users'] = $this->project_m->get_company_users($this->Company_id);
            $this->data['issue_details'] = $this->project_m->get_issue_details($issue_id);
            $this->load->view('modal/issue_assign', $this->data);
        }

    }

    /**
     * update issue status
     * @param null $issue_id
     * @param null $status_id
     */
    function update_status($issue_id = null, $status_id = null)
    {

        if ($this->project_m->update_issue_status($issue_id, $status_id)) {

            $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $log_params = array(
                'activity_user' => $activity_user,
                'activity_user_type' => $this->CompanyUser_type,
                'activity_module' => 'project',
                'activity_details' => "issue  update",
                'activity_module_id' => $issue_id,
                'activity_status' => "success",
                'activity_date' => date('Y-m-d,H:i:s'),
                'activity_event' => "Estatus de caso actualizado exitosamente",
                'activity_icon' => 'fa fa-bug'
            );

            $this->__activity_tracker($log_params);
            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', lang('messages_issue_save_success'));
            redirect('project/issues/view/' . $issue_id);

        }
    }
}

