<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/24
 * Time: 11:12 AM
 */
class Notes extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();

        $this->load->model('project_m');
    }

    /**
     * list all notes
     */
    function preview()
    {

        $this->data['option'] = $this->uri->segment(2);
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Notes';
        $this->data['show_datepicker'] = true;

        $this->data['project_id'] = $this->uri->segment(4);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }

        $this->data['note_id'] = ($this->uri->segment(6)) ? intval($this->uri->segment(6)) : null;
        $this->data['project_notes'] = $this->project_m->get_notes($this->data['project_id']);

        if ($this->data['note_id']) {
            $this->data['notes_details'] = $this->project_m->note_details($this->data['note_id']);
        }
        $this->data['users'] = $this->project_m->get_company_users($this->Company_id);
        $this->data['clients'] = $this->project_m->get_company_clientsn($this->Company_id);

        $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);
        $this->data['show_datepicker'] = true;
        $this->data['message'] = true;
$this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * save note
     * @param null $project_id
     * @param null $notes_id
     */
    public function save_note($project_id = null, $notes_id = null)
    {

        $project_id = $this->input->post('project_id');
        $assign_data = $this->input->post('task_assign1');
        $assign_data1 = $this->input->post('task_assign2');
        $assign_data2 = $this->input->post('task_assign3');
        $assign_data3 = $this->input->post('task_assign4');
        $assign_data4 = $this->input->post('task_assign5');
        $assign_data5 = $this->input->post('task_assign6');

        if ($notes_id) {//update...
            $arr0 = $this->input->post('assign_to0[]');
            $client_name = "";
            foreach($arr0 as $emp0)
            {
                $client_name .= $emp0 . " ";
            }
            if (isset($client_name)) {
              $param = array(
                  "note_message" => $this->input->post("cost_description"),
                  "cost_description" => $this->input->post("cost_description"),
                  "cost_date" => $this->input->post("cost_date"),
                  "cost_classification" => $this->input->post("cost_classification"),
                  "assistant" => $client_name,
                  "message" => $this->input->post("message"),
                  "taskname1" => $this->input->post("taskname1"),
                  "encargados1" => serialize($this->input->post('task_assign1')),//[] serialized data..,,
                  "taskname2" => $this->input->post("taskname2"),
                  "encargados2" => serialize($this->input->post('task_assign2')),
                  "taskname3" => $this->input->post("taskname3"),
                  "encargados3" => serialize($this->input->post('task_assign3')),
                  "taskname4" => $this->input->post("taskname4"),
                  "encargados4" => serialize($this->input->post('task_assign4')),
                  "taskname5" => $this->input->post("taskname5"),
                  "encargados5" => serialize($this->input->post('task_assign5')),
                  "taskname6" => $this->input->post("taskname6"),
                  "encargados6" => serialize($this->input->post('task_assign6')),
              );
            }


            if (isset($assign_data)) {
              $param2 = array(
                  "task_name" => $this->input->post("taskname1"),//"Documention",
                  "task_code" => $this->input->post("task_code"),//"",
                  "task_level" => 1,//1,
                  "task_milestone_id" => 0,
                  "project_id" => $this->input->post("project_id"),
                  "task_visible" => null,
                  "task_use_checklist" => 1,
                  "task_description" => null,
                  "task_requirements" => null,
                  "task_progress" => 0,//"percentage",
                  "task_timer" => 0,//"percentage",

                  "task_status" => null,//"STATUS_ACTIVE",
                  "task_start" => null,//1363320000000,
                  "task_priority" => 1,//1363320000000,
                  "task_duration" => 0,//2,
                  "task_est_hours" => 0,//2,
                  "task_end" => null,//1363665599999,
                  "task_startIsMilestone" => 0,//false,
                  "task_endIsMilestone" => 0,//false,
                  "task_collapsed" => 0,//false,
                  "task_assigs" => serialize($this->input->post('task_assign1')),//[] serialized data..,
                  //"task_depends" =>$this->input->post('task_depend'),
                  "task_earlyStart" => 0,//2,
                  "task_earlyFinish" => 0,//2,
                  "task_latestStart" => 0,//2,
                  "task_latestFinish" => 0,//2,
                  "task_criticalCost" => 0,//2,
                  "task_isCritical" => 0,//2,
                  "task_hasChild" => 0,//2,
                  "date_added" => $this->input->post("cost_date"),//"",
                  "user_id" => intval($this->CompanyUser_id)
              );
            }

            if (isset($assign_data1)) {
              $param3 = array(
                  "task_name" => $this->input->post("taskname2"),//"Documention",
                  "task_code" => $this->input->post("task_code"),//"",
                  "task_level" => 1,//1,
                  "task_milestone_id" => 0,
                  "project_id" => $this->input->post("project_id"),
                  "task_visible" => null,
                  "task_use_checklist" => 1,
                  "task_description" => null,
                  "task_requirements" => null,
                  "task_progress" => 0,//"percentage",
                  "task_timer" => 0,//"percentage",

                  "task_status" => null,//"STATUS_ACTIVE",
                  "task_start" => null,//1363320000000,
                  "task_priority" => 1,//1363320000000,
                  "task_duration" => 0,//2,
                  "task_est_hours" => 0,//2,
                  "task_end" => null,//1363665599999,
                  "task_startIsMilestone" => 0,//false,
                  "task_endIsMilestone" => 0,//false,
                  "task_collapsed" => 0,//false,
                  "task_assigs" => serialize($this->input->post('assign_to2')),//[] serialized data..,
                  //"task_depends" =>$this->input->post('task_depend'),
                  "task_earlyStart" => 0,//2,
                  "task_earlyFinish" => 0,//2,
                  "task_latestStart" => 0,//2,
                  "task_latestFinish" => 0,//2,
                  "task_criticalCost" => 0,//2,
                  "task_isCritical" => 0,//2,
                  "task_hasChild" => 0,//2,
                  "date_added" => $this->input->post("cost_date"),//"",
                  "user_id" => intval($this->CompanyUser_id)
              );
            }
            if (isset($assign_data2)) {
              $param4 = array(
                  "task_name" => $this->input->post("taskname3"),//"Documention",
                  "task_code" => $this->input->post("task_code"),//"",
                  "task_level" => 1,//1,
                  "task_milestone_id" => 0,
                  "project_id" => $this->input->post("project_id"),
                  "task_visible" => null,
                  "task_use_checklist" => 1,
                  "task_description" => null,
                  "task_requirements" => null,
                  "task_progress" => 0,//"percentage",
                  "task_timer" => 0,//"percentage",

                  "task_status" => null,//"STATUS_ACTIVE",
                  "task_start" => null,//1363320000000,
                  "task_priority" => 1,//1363320000000,
                  "task_duration" => 0,//2,
                  "task_est_hours" => 0,//2,
                  "task_end" => null,//1363665599999,
                  "task_startIsMilestone" => 0,//false,
                  "task_endIsMilestone" => 0,//false,
                  "task_collapsed" => 0,//false,
                  "task_assigs" => serialize($this->input->post('assign_to3')),//[] serialized data..,
                  //"task_depends" =>$this->input->post('task_depend'),
                  "task_earlyStart" => 0,//2,
                  "task_earlyFinish" => 0,//2,
                  "task_latestStart" => 0,//2,
                  "task_latestFinish" => 0,//2,
                  "task_criticalCost" => 0,//2,
                  "task_isCritical" => 0,//2,
                  "task_hasChild" => 0,//2,
                  "date_added" => $this->input->post("cost_date"),//"",
                  "user_id" => intval($this->CompanyUser_id)
              );
            }
            if (isset($assign_data3)) {
              $param5 = array(
                  "task_name" => $this->input->post("taskname4"),//"Documention",
                  "task_code" => $this->input->post("task_code"),//"",
                  "task_level" => 1,//1,
                  "task_milestone_id" => 0,
                  "project_id" => $this->input->post("project_id"),
                  "task_visible" => null,
                  "task_use_checklist" => 1,
                  "task_description" => null,
                  "task_requirements" => null,
                  "task_progress" => 0,//"percentage",
                  "task_timer" => 0,//"percentage",

                  "task_status" => null,//"STATUS_ACTIVE",
                  "task_start" => null,//1363320000000,
                  "task_priority" => 1,//1363320000000,
                  "task_duration" => 0,//2,
                  "task_est_hours" => 0,//2,
                  "task_end" => null,//1363665599999,
                  "task_startIsMilestone" => 0,//false,
                  "task_endIsMilestone" => 0,//false,
                  "task_collapsed" => 0,//false,
                  "task_assigs" => serialize($this->input->post('assign_to4')),//[] serialized data..,
                  //"task_depends" =>$this->input->post('task_depend'),
                  "task_earlyStart" => 0,//2,
                  "task_earlyFinish" => 0,//2,
                  "task_latestStart" => 0,//2,
                  "task_latestFinish" => 0,//2,
                  "task_criticalCost" => 0,//2,
                  "task_isCritical" => 0,//2,
                  "task_hasChild" => 0,//2,
                  "date_added" => date('Y-m-d H:i:s'),//"",
                  "user_id" => intval($this->CompanyUser_id)
              );
            }
            if (isset($assign_data4)) {
              $param6 = array(
                  "task_name" => $this->input->post("taskname5"),//"Documention",
                  "task_code" => $this->input->post("task_code"),//"",
                  "task_level" => 1,//1,
                  "task_milestone_id" => 0,
                  "project_id" => $this->input->post("project_id"),
                  "task_visible" => null,
                  "task_use_checklist" => 1,
                  "task_description" => null,
                  "task_requirements" => null,
                  "task_progress" => 0,//"percentage",
                  "task_timer" => 0,//"percentage",

                  "task_status" => null,//"STATUS_ACTIVE",
                  "task_start" => null,//1363320000000,
                  "task_priority" => 1,//1363320000000,
                  "task_duration" => 0,//2,
                  "task_est_hours" => 0,//2,
                  "task_end" => null,//1363665599999,
                  "task_startIsMilestone" => 0,//false,
                  "task_endIsMilestone" => 0,//false,
                  "task_collapsed" => 0,//false,
                  "task_assigs" => serialize($this->input->post('assign_to5')),//[] serialized data..,
                  //"task_depends" =>$this->input->post('task_depend'),
                  "task_earlyStart" => 0,//2,
                  "task_earlyFinish" => 0,//2,
                  "task_latestStart" => 0,//2,
                  "task_latestFinish" => 0,//2,
                  "task_criticalCost" => 0,//2,
                  "task_isCritical" => 0,//2,
                  "task_hasChild" => 0,//2,
                  "date_added" => $this->input->post("cost_date"),//"",
                  "user_id" => intval($this->CompanyUser_id)
              );
            }
            if (isset($assign_data5)) {
              $param7 = array(
                  "task_name" => $this->input->post("taskname6"),//"Documention",
                  "task_code" => $this->input->post("task_code"),//"",
                  "task_level" => 1,//1,
                  "task_milestone_id" => 0,
                  "project_id" => $this->input->post("project_id"),
                  "task_visible" => null,
                  "task_use_checklist" => 1,
                  "task_description" => null,
                  "task_requirements" => null,
                  "task_progress" => 0,//"percentage",
                  "task_timer" => 0,//"percentage",

                  "task_status" => null,//"STATUS_ACTIVE",
                  "task_start" => null,//1363320000000,
                  "task_priority" => 1,//1363320000000,
                  "task_duration" => 0,//2,
                  "task_est_hours" => 0,//2,
                  "task_end" => null,//1363665599999,
                  "task_startIsMilestone" => 0,//false,
                  "task_endIsMilestone" => 0,//false,
                  "task_collapsed" => 0,//false,
                  "task_assigs" => serialize($this->input->post('assign_to6')),//[] serialized data..,
                  //"task_depends" =>$this->input->post('task_depend'),
                  "task_earlyStart" => 0,//2,
                  "task_earlyFinish" => 0,//2,
                  "task_latestStart" => 0,//2,
                  "task_latestFinish" => 0,//2,
                  "task_criticalCost" => 0,//2,
                  "task_isCritical" => 0,//2,
                  "task_hasChild" => 0,//2,
                  "date_added" => $this->input->post("cost_date"),//"",
                  "user_id" => intval($this->CompanyUser_id)
              );
            }
            if (isset($param2)) {

              if ($this->project_m->update_note($param, $param2, $param3, $param4, $param5, $param6, $param7, $assign_data, $assign_data1, $assign_data2, $assign_data3, $assign_data4, $assign_data5, $project_id, $notes_id)) {

                  $this->session->set_flashdata('msg_status', 'success');
                  $this->session->set_flashdata('message', lang('messages_note_save_success'));
                  redirect('project/notes/preview/' . $project_id . '/notes/' . $notes_id);
              }
            }

        } else {//insert..
            $project_id = $this->uri->segment(4);
            $param = array(
                "note_message" => $this->input->post("cost_description"),
                "cost_description" => $this->input->post("cost_description"),
                "cost_date" => $this->input->post("cost_date"),
                "cost_classification" => $this->input->post("cost_classification"),
                "assistant" => null,
                "message" => $this->input->post("message"),
                "taskname1" => $this->input->post("taskname1"),
                "encargados1" => serialize($this->input->post('task_assign1')),//[] serialized data..,,
                "taskname2" => $this->input->post("taskname2"),
                "encargados2" => serialize($this->input->post('task_assign2')),
                "taskname3" => $this->input->post("taskname3"),
                "encargados3" => serialize($this->input->post('task_assign3')),
                "taskname4" => $this->input->post("taskname4"),
                "encargados4" => serialize($this->input->post('task_assign4')),
                "taskname5" => $this->input->post("taskname5"),
                "encargados5" => serialize($this->input->post('task_assign5')),
                "taskname6" => $this->input->post("taskname6"),
                "encargados6" => serialize($this->input->post('task_assign6')),
                "project_id" => $project_id,
                "note_date" => date('Y-m-d,H:i:s'),
                "note_user_id" => $this->CompanyUser_id
            );

            $notes_id = $this->project_m->add_note($param);
            if ($notes_id) {


                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_note_add_success'));
                redirect('project/notes/preview/' . $project_id . '/notes/' . $notes_id);
            }
        }

    }

    /**
     * save notes activities
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->project_m->add_log($activity_data);

    }

    function notes($project_id = null, $note_id = null, $task_id = null)
    {

        $project_id = intval($project_id);
        $this->data['project_id'] = $project_id;
        $note_id = $this->uri->segment(5);
        $this->data['note_id'] = $note_id;
        $this->data['notes'] = $this->project_m->note_details($this->data['note_id']);
        $this->data['users'] = $this->project_m->get_assigned_team($project_id);
        $this->data['clients'] = $this->project_m->get_company_clientsn($this->Company_id);
        $this->data['clients_user'] = $this->project_m->get_company_clientsn($this->Company_id);
        $this->data['userss'] = $this->project_m->get_company_users($this->Company_id);
        $this->data['project_members'] = $this->project_m->get_assigned_team($project_id);
        $this->data['task_details'] = $this->project_m->get_task_details($project_id, intval($task_id));
        $this->data['task_members'] = $this->project_m->get_task_assignment($project_id, $task_id);

        $this->load->view('modal/notesm', $this->data);
    }
}
