<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/24
 * Time: 11:46 AM
 */
class Overview extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();

        $this->load->model('overview_m');
        $this->load->model('project_m');

    }


    /**
     * Get project details/dashboards statistics..
     */
    function preview()
    {


        $this->data['option'] = $this->uri->segment(2);//tasks/files/gantt
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Overview';

        $this->data['project_id'] = $this->uri->segment(4);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');

        }

        $this->data['activities'] = $this->overview_m->get_project_activities(intval($this->data['project_id']),
            'project');
        $this->data['invoices'] = $this->project_m->get_invoices($this->data['project_id']);
        $this->data['project_details'] = $this->overview_m->project_details($this->data['project_id']);
        $this->data['assigned_team'] = $this->overview_m->get_project_manager($this->data['project_id']);
        $this->data['milestones'] = $this->overview_m->get_project_milestone($this->data['project_id']);
        $this->data['import'] = $this->overview_m->get_import_milestone($this->data['project_id']);

		$this->data['view_data']= $this->overview_m->view_data($this->data['project_id']);
		$this->data['avances']= $this->project_m->get_avances($this->data['project_id']);
		$client_id = $this->data['project_details']->client_id;
		$this->data['est_trans'] = $this->overview_m->get_est_trans($client_id);
		$this->data['pagos'] = $this->project_m->get_est_trans($client_id);

      $this->data['show_project_timer'] = true;
      $this->data['show_project_stats'] = true;
      $this->data['show_project_overview'] = true;

      $this->data['datatable'] = true;
      $this->data['SCRIPT_PAGE'] = true;

        $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);

        $this->load->view('_main_layout', $this->data);
    }

	function index()
	{
	    $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;
		$this->data['view_data']= $this->overview_m->view_data($project_id);
		$this->load->view('overview', $this->data, FALSE);
	}

	function importbulkemail(){
		$this->load->view('overview');
	}

	function clean_pi($project_id){
		$this->overview_m->clean_pi($project_id);
		redirect('project/overview/overview/' .$project_id);
	}

	function import(){
    if ($this->input->post()) {
        $project_id = $this->input->post('project_id');
        if(isset($_POST["import"]))
    		{
    			$filename=$_FILES["file"]["tmp_name"];
    			if($_FILES["file"]["size"] > 0)
    			{
    				$row = 1;
                        if (($handle = fopen($filename, "r")) !== FALSE) {
                        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                             $num = count($data);
                             echo "<p> $num fields in line $row: <br /></p>\n";
                             $row++;
                             for ($c=0; $c < $num; $c++) {
                                 $col[$c] = $data[$c];
                             }
                             $data_csv = array (
                                'numero' => $col[0],
                                'concepto' => $col[1],
                                'importe' => $col[2],
                                'codigo_proyecto' => $col[3],
                                "project_id" => $project_id
                             );

                             $param = array(
                                 "project_id" => $project_id,
                                 "numero" => $col[0],
                                 "milestone_name" => $col[1],
                                 "importe" => $col[2],
                                 "description" => null,
                                 "start_date" => date('Y-m-d'),
                                 "due_date" => date('Y-m-d'),
                                 "created" => date('Y-m-d H:i:s'),
                                 "tipo" => 2
                             );

                             $insert = $this->overview_m->insertCSV($data_csv, $param);
                        }

                fclose($handle);
                }
    			$this->session->set_flashdata('message', 'Data imported successfully..');
    			redirect('https://proyectosinternos.com/Taller5/project/overview/preview/'. $project_id);
    			}else{
    				$this->session->set_flashdata('message', 'Something went wrong..');
    			}
    		}
    }

	}

  /**
   * edit milestone
   * @param null $project_id
   * @param null $milestone_id
   */
  function view_modal_milestone($project_id = null, $milestone_id = null)
  {
      $project_id = intval($project_id);
      $this->data['project_id'] = $project_id;
      	$this->data['avances']= $this->project_m->get_avances($this->data['project_id']);
      $this->data['milestone'] = $this->project_m->selected_milestone($project_id, $milestone_id);
      $this->load->view('modal/edit_milestone', $this->data);
  }
}
