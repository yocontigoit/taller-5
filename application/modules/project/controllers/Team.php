<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/03/10
 * Time: 10:33 AM
 */
class Team extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();

        $this->load->model('project_m');
    }


    /**
     * list all assgined users
     */
    function preview()
    {

        $this->data['option'] = $this->uri->segment(2);
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Assignment';


        $this->data['project_id'] = $this->uri->segment(4);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }
        $this->data['assigned_team'] = $this->project_m->get_assigned_team($this->data['project_id']);
        $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);
        $this->data['show_team_overview'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }


    /**
     * edit project assigned user.
     * @param null $project_id
     */
    function edit_resource($project_id = null)
    {

        $this->data['project_id'] = $project_id;
        $this->data['assigned_team'] = $this->project_m->get_assigned_team($project_id);
        $this->data['project_roles'] = $this->project_m->get_all_project_roles();

        $this->load->view('modal/resources', $this->data);
    }


    /**
     * save/update assigned user data
     */
    function update_resources()
    {

        if ($this->input->post()) {
            $hourly_cost = $this->input->post('hourly_cost');
            $project_role = $this->input->post('project_role');
            $project_id = $this->input->post('project_id');

            foreach ($hourly_cost as $key => $value) {

                $data = array(
                    "hourly_cost" => $value,
                    "role" => $project_role[$key]
                );
                $this->project_m->update_assigned_team($data, $project_id, $key);
            }
            redirect('project/team/preview/' . $project_id . '/team');
        }

    }

}