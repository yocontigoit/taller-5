<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/09/11
 * Time: 8:54 AM
 */
class Project extends Admin_Controller
{


    function __construct()
    {

        parent::__construct('project');
        $this->load->model('project_m');
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
    }

    /**
     * list all projects
     */
    function index()
    {

        $this->data['content'] = 'project/index';
        $this->data['title'] = 'Projectos';

        if ($this->CompanyClient_id > 0) {
            $this->data['projects'] = $this->project_m->get_client_projects($this->CompanyClient_id);
        } else {
            if ($this->CompanyUserRolePermission_id == 1) {
                $this->data['projects'] = $this->project_m->get_all_projects();
            } else {
                $this->data['projects'] = $this->project_m->get_all_assigned_projects($this->CompanyUser_id);
            }
        }
        $this->data['datatable'] = true;
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);

    }

    /*
     * Load other pages..
     */
    function view($project_id = null)
    {

        $this->data['option'] = $this->uri->segment(4) ? $this->uri->segment(4) : 'overview';
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Project';

        $this->data['project_id'] = ($project_id) ? intval($project_id) : $this->uri->segment(3);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }

        if ($this->data['option']) {
            $this->data['SCRIPT_PAGE'] = true;
            $this->data['PAGE'] = $this->load->view('project/groups/' . $this->data['option'], $this->data, true);
        }
        $this->load->view('_main_layout', $this->data);

    }

    /**
     * add new project..
     */
    function add()
    {
        //check add project permission
        $this->check_action_permission('add_update');

        $rules = $this->form_validator->project_add_rules;
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() && $this->input->post()) {


            $use_worklog = ($this->input->post('project_worklog') == 'on') ? 1 : 0;
            $settings = json_encode($this->input->post('settings'));
            $param = array(
                "client_id" => intval($this->input->post('client')),
                "project_type" => $this->input->post('type'),
                "project_code" => $this->input->post('project_code'),
                "project_title" => $this->input->post('project_title'),
                "project_start_date" => date('Y-m-d', strtotime($this->input->post('start_date'))),
                "project_due_date" => date('Y-m-d', strtotime($this->input->post('end_date'))),
                "project_status" => 1,
                "project_settings" => $settings,
                "project_use_worklog" => $use_worklog,
                "project_budget" => $this->input->post('budget'),
                "fondo" => $this->input->post('fondo'),
                "project_traslado" => $this->input->post('traslados'),
                "project_honorario" => $this->input->post('honorarios'),
                "project_progress" => $this->input->post('progress'),
                "project_desc" => $this->input->post('project_desc'),
                "date_created" => date('Y-m-d,H:i:s'),
                "user_id" => intval($this->CompanyUser_id)
            );

            $project_id = $this->project_m->add_project($param, $this->CompanyUser_id, $this->input->post('assign_to'));

            if ($project_id) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => $this->input->post('project_title'),
                    'activity_module_id' => $project_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Agregar un nuevo proyecto ",
                    'activity_icon' => 'fa fa-user'
                );

                $this->__activity_tracker($log_params);
                if ($this->CompanyUser_id > 0) {
                    $this->notifications->send_new_project_notification($project_id);
                    $this->notifications->send_project_assigned_notification($project_id,
                        $this->input->post('assign_to'), $this->CompanyUser_FullName);
                }

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_project_add_success'));
                redirect('project/overview/preview/' . $project_id);

            } else {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', lang('messages_project_add_error'));
                redirect(current_url());

            }
        } else {

            $this->data['content'] = 'project/add_project';
            $this->data['title'] = 'Add New Project';
            $this->data['project_code'] = $this->config->item('invoice')->project_prefix . "-" . trans_reference(13,
                    $this->Company_id);
            $this->data['users'] = $this->project_m->get_company_users($this->Company_id);
            $this->data['clients'] = $this->project_m->get_company_clients($this->Company_id);
            $this->data['project_permissions'] = $this->project_m->get_settings();
            $this->data['current_permissions'] = '{"settings":"on"}';
            $this->data['message'] = true;
            $this->data['show_nouislider'] = true;
            $this->data['show_datepicker'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);
        }

    }

    /**
     * save project activities to database
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->project_m->add_log($activity_data);

    }

    /**
     * delete project
     */
    function delete()
    {
        $this->check_action_permission('delete');
        if ($this->input->post()) {
            $project_id = $this->input->post('project_id');

            if ($this->project_m->delete_project($project_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_project_delete_success'));
                redirect('project');
            } else {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'something went wrong');
                redirect('project/overview/preview/' . $project_id);

            }
        } else {
            $this->data['project_id'] = $this->uri->segment(3);
            $this->load->view('modal/delete_project', $this->data);
        }
    }

    /**
     * add rist
     * @param null $project_id
     */
    function risk_matrix($project_id = null)
    {
        //$this->check_action_permission('add_update');
        $this->data['title'] = 'Add risk';
        $this->data['project_id'] = intval($project_id);
        $this->load->view('modal/risk_matrix', $this->data);
    }

    /**
     * edit project
     * @param null $project_id
     */
    function  edit_project($project_id = null)
    {
        $this->check_action_permission('add_update');

        if ($this->input->post()) {

            $project_id = intval($this->input->post('project_id'));
            $use_worklog = ($this->input->post('project_worklog') == 'on') ? 1 : 0;
            $settings = json_encode($this->input->post('settings'));
            $param = array(
                "client_id" => intval($this->input->post('client')),
                "project_code" => $this->input->post('project_code'),
                "project_title" => $this->input->post('project_title'),
                "project_start_date" => date('Y-m-d', strtotime($this->input->post('start_date'))),
                "project_due_date" => date('Y-m-d', strtotime($this->input->post('end_date'))),
                "project_status" => 1,
                "project_settings" => $settings,
                "project_use_worklog" => $use_worklog,
                "project_budget" => $this->input->post('budget'),
                "fondo" => $this->input->post('fondo'),
                "project_traslado" => $this->input->post('traslados'),
                "project_honorario" => $this->input->post('honorarios'),
                "project_cantidad" => $this->input->post('honorarios_cantidad'),
                "project_desc" => $this->input->post('project_desc'),
                "project_progress" => $this->input->post('progress'),
                "project_direccion" => $this->input->post('direccion'),
                "project_horario" => $this->input->post('horario')


            );

            if ($this->project_m->edit_project($param, $this->input->post('assign_to'), $project_id)) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => $this->input->post('project_title'),
                    'activity_module_id' => $project_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Proyecto actualizado :<span class=\"label label-success\">" . $this->input->post('project_code') . "</span>",
                    'activity_icon' => 'fa fa-user'
                );

                $this->__activity_tracker($log_params);
                if ($this->CompanyUser_id > 0) {
                    $this->notifications->send_project_assigned_notification($project_id,
                        $this->input->post('assign_to'), $this->CompanyUser_FullName);
                }
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_project_save_success'));
                redirect('project/overview/preview/' . $project_id);

            } else {

                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', lang('messages_project_save_error'));
                redirect(current_url());

            }


        } else {

            $this->data['content'] = 'project/edit_project';
            $this->data['title'] = 'Edit Project';
            $this->data['project_id'] = intval($project_id);
            $this->data['project_details'] = $this->project_m->project_details($project_id);
            $this->data['assigned_team'] = $this->project_m->get_assigned_team($project_id);
            $this->data['users'] = $this->project_m->get_company_users($this->Company_id);
            $this->data['clients'] = $this->project_m->get_company_clients($this->Company_id);
            $this->data['project_permissions'] = $this->project_m->get_settings();
            $this->data['current_permissions'] = $this->data['project_details']->project_settings;
            $this->data['message'] = true;
            $this->data['show_nouislider'] = true;
            $this->data['show_datepicker'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);
        }

    }

    /**
     * add risk
     */
    function add_risk()
    {
        $this->check_action_permission('add_update');
        $project_id = $this->input->post('project_id');
        $param = array(
            "risk_name" => $this->input->post('risk_name'),
            "probability" => $this->input->post('risk_probability'),
            "impact_percentage" => $this->input->post('risk_percentage'),
            "project_id" => $project_id
        );
        if ($this->project_m->add_project_risk($param)) {
            redirect('project/overview/preview/' . $project_id);
        }
    }

    /**
     * invoice project
     * @param null $project_id
     */
    function invoice_project($project_id = null)
    {
        $this->check_action_permission('add_update');
        $project_id = intval($project_id);

        $project_details = $this->project_m->project_details($project_id);
        $invoice_number = $this->config->item('invoice')->invoice_prefix . "-" . trans_reference(10, $this->Company_id);
        $currency_iso = get_currency_iso($this->config->item('local')->company_currency);
        $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;

        $param = array(
            "inv_no" => $invoice_number,
            "client_id" => $project_details->client_id,
            "status" => 2,
            "approved" => 1,
            "currency" => $currency_iso,
            "invoice_due" => $project_details->project_due_date,
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s'),
            "inv_created_by" => $user_id
        );

        $invoice_id = $this->project_m->invoice_project($param, $project_id);
        if ($invoice_id) {

            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', 'project invoiced successfully');

            $log_params = array(
                'activity_user' => $user_id,
                'activity_user_type' => $this->CompanyUser_type,
                'activity_module' => 'project',
                'activity_details' => 'Converted Project  to Invoice',
                'activity_module_id' => intval($invoice_id),
                'activity_status' => "info",
                'activity_date' => date('Y-m-d,H:i:s'),
                'activity_event' => "Proyecto convertido a factura",
                'activity_icon' => 'fa fa-money'
            );

            $this->__activity_tracker($log_params);
            redirect('billing/invoice/' . $invoice_id);
        }


    }

    /**
     * copy/clone project
     * @param null $project_id
     */
    function clone_project($project_id = null)
    {
        $project_id = intval($project_id);
        if ($project_id) {

            $project_code = $this->config->item('invoice')->project_prefix . "-" . trans_reference(13,
                    $this->Company_id);
            $project_details = $this->project_m->project_details($project_id);

            $param = array(
                "client_id" => $project_details->client_id,
                "project_code" => $project_code,
                "project_title" => $project_details->project_title,
                "project_start_date" => $project_details->project_start_date,
                "project_due_date" => $project_details->project_due_date,
                "project_status" => $project_details->project_status,
                "project_budget" => $project_details->project_budget,
                "project_desc" => $project_details->project_desc,
                "project_progress" => $project_details->project_progress,
                "date_created" => $project_details->date_created,
                "user_id" => intval($this->CompanyUser_id)
            );

            $project_id = $this->project_m->add_project($param, $this->CompanyUser_id, null);

            if ($project_id) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => $project_details->project_title,
                    'activity_module_id' => $project_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Proyecto copiado",
                    'activity_icon' => 'fa fa-user'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_project_copy_success'));
                redirect('project/view/' . $project_id . '/overview');
            }
        }

    }

    /**
     * get project active timers
     */
     function change_image()
    {
        if ($this->input->post()) {
            if ($this->CompanyUser_id) {


                $project_id = intval($this->input->post('project_id'));
                $config['upload_path'] = './files/profile_images/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '1000000';
                $config['max_width'] = '100000';
                $config['max_height'] = '100000';
                $config['file_name'] = 'project-' . $project_id;
                $config['remove_spaces'] = true;
                $config['overwrite'] = true;

                $this->load->library('upload', $config);


                if (!$this->upload->do_upload('user_picture')) {
                    $this->session->set_flashdata('msg_status', 'error');
                    $this->session->set_flashdata('message', lang('messages_avatar_add_error'));
                    redirect('project/overview/preview/' . $project_id);
                } else {
                    $data = $this->upload->data();
                    if ($this->project_m->update_profile_image($data['file_name'], $project_id)) {
                        //Set user avatar


                        $this->session->set_flashdata('msg_status', 'success');
                        $this->session->set_flashdata('message', lang('messages_avatar_add_success'));
                        redirect('project/overview/preview/' . $project_id);

                    }


                }

            }
        } else {

            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', lang('messages_avatar_add_success'));
            redirect('project/overview/preview/' . $project_id);
}
    }


    function get_active_timer()
    {
        $project_id = intval($this->input->post('project_id'));


        $user_id = ($this->CompanyUserRolePermission_id == 1) ? null : $this->CompanyUser_id;

        $time_entry = $this->project_m->get_project_timer($project_id, $user_id);

        if (!empty($time_entry)) {
            $start_time = $time_entry->start_timer;
            $end_time = time();

            $time_clocked = $end_time - $start_time;

            $response = array('has_timer' => true, 'timer_offset' => $time_clocked);
        } else {
            $response = array('has_timer' => false);
        }
        echo json_encode($response);

    }

    /**
     * Update and log project timer
     */
    function project_timer()
    {

        $project_id = $this->input->post('project_id');
        $start_timer = $this->input->post('start_timer');

        if ($start_timer == 'true') {

            $start_time = timer_already_logged($project_id, $this->CompanyUser_id);
            if ($start_time) {

                $timer_entry = array(
                    'project_id' => $project_id,
                    'start_timer' => $start_time,
                    'timer_active' => 1
                );
                $this->project_m->update_project_timesheet($timer_entry, $project_id, $this->CompanyUser_id);
            } else {

                $timer_entry = array(
                    'project_id' => $project_id,
                    'timer_by' => $this->CompanyUser_id,
                    'start_timer' => time(),
                    'timer_active' => 1

                );
                $this->project_m->log_project_timesheet($timer_entry);

            }

            $response = array('success' => true);
        } else {

            $timer_entry = array(
                'stop_timer' => time(),
                'timer_active' => 0
            );
            if ($this->project_m->stop_project_timer($timer_entry, $project_id, $this->CompanyUser_id)) {
                $response = array('success' => true);
            }

            $response = array('success' => true);
        }
        echo json_encode($response);


    }

    /**
     * edit milestone
     * @param null $project_id
     * @param null $milestone_id
     */
    function view_modal_milestone($project_id = null, $milestone_id = null)
    {
        $project_id = intval($project_id);
        $this->data['project_id'] = $project_id;
        $this->data['milestone'] = $this->project_m->selected_milestone($project_id, $milestone_id);
        $this->load->view('modal/edit_milestone', $this->data);
    }

}
