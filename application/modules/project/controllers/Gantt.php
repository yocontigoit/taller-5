<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/24
 * Time: 11:11 AM
 */
class Gantt extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();
        $this->load->model('gantt_m');
        $this->load->model('project_m');
    }

    /**
     * display gantt chart information
     */
    function preview()
    {

        $this->data['option'] = $this->uri->segment(2);//tasks/files/gantt
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Gantt Chat';
          $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;


        $this->data['project_id'] = intval($this->uri->segment(4));
        $this->data['discounts'] = $this->project_m->get_discounts($this->data['project_id']);

        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;
        $this->data['client_id'] = 28;
$this->data['threepercent'] = $this->project_m->get_3percent();
        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }
        $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);
        $this->data['gantt_show'] = true;
        $this->data['SCRIPT_PAGE'] = true;

        $this->load->view('_main_layout', $this->data);
    }

    /**
     * load gantt chart data
     * @param null $project_id
     */
    public function show($project_id = null)
    {
        //check if allowed edit..
        $this->data['project_data'] = $this->gantt_m->get_gantt_data($project_id);
        echo '{"ok":true,"mode":"edit","data":' . json_encode($this->data['project_data'], JSON_NUMERIC_CHECK) . '}';
        exit();
    }


}