<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/07/06
 * Time: 11:19 AM
 */
class Documents extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();
        $this->load->model('project_m');
    }

    /**
     * Get project details..
     */
    function preview()
    {

        $this->data['option'] = $this->uri->segment(2);
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Media';


        $this->data['project_id'] = $this->uri->segment(4);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }
        $this->data['documents_details'] = $this->project_m->project_attachments($this->data['project_id']);

        $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);

        $this->data['show_document_preview'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }


    /**
     * save attachments
     * @param null $project_id
     * @param null $task_id
     */
    function upload($project_id = null, $task_id = null)
    {

        if ($this->input->post()) {
            $user_id = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;


            $project_details = $this->project_m->project_details($project_id);
            $path = date("Y-m-d",
                    strtotime($project_details->project_start_date)) . "_" . $project_id . "_" . $project_details->project_code . '/';
            $fullpath = './files/project_files/' . $path;

            if (!is_dir($fullpath)) {
                mkdir($fullpath, 0777, true);
            }
            $config['upload_path'] = $fullpath;
            $config['allowed_types'] = $this->config->item('system')->allowed_files; // 'gif|jpg|png|jpeg|pdf';
            $config['max_size'] = $this->config->item('system')->max_file_size; //'1000'
            $config['overwrite'] = false;

            $this->load->library('upload');

            $this->upload->initialize($config);

            if (!$this->upload->do_multi_upload("project_upload")) {

                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', lang('messages_file_add_error'));
                redirect('project/documents/preview/' . $project_id . '/documents');
            } else {

                $fileinfs = $this->upload->get_multi_upload_data();
                foreach ($fileinfs as $findex => $fileinf) {
                    $data = array(
                        'project_id' => $project_id,
                        'task_id' => $task_id,
                        'path' => $path,
                        'upload_date' => date('Y-m-d,H:i:s'),
                        'uploader_type' => $this->CompanyUser_type,
                        'file_name' => $fileinf['file_name'],
                        'title' => $this->input->post('file_title'),
                        'description' => $this->input->post('file_description'),
                        'file_type' => $fileinf['file_ext'],
                        'file_size' => $fileinf['file_size'],
                        'is_image' => $fileinf['is_image'],
                        'image_width' => $fileinf['image_width'],
                        'image_height' => $fileinf['image_height'],
                        'uploaded_by' => $user_id
                    );
                    $file_id = $this->project_m->save_attachment($data);


                }

                if ($task_id && $project_id) {
                    redirect('project/tasks/details/' . $project_id . '/' . $task_id . '/tasks');
                }
                redirect('project/documents/preview/' . $project_id . '/documents');

            }
        } else {


            $this->data['project_id'] = $project_id;
            $this->data['task_id'] = $task_id;
            $this->load->view('modal/project_upload', $this->data);
        }
    }

    /**
     * edit attachment information
     * @param null $attachment_id
     */
    function file_edit($attachment_id = null)
    {

        if ($this->input->post()) {

            $project_id = $this->input->post('project_id', true);
            $task_id = $this->input->post('task_id');

            $data = array(
                'title' => $this->input->post('file_title'),
                'description' => $this->input->post('file_description')
            );


            if ($this->project_m->update_attachment($data, $attachment_id, $project_id)) {

                if ($task_id && $project_id) {
                    redirect('project/tasks/details/' . $project_id . '/' . $task_id . '/tasks');
                }
                redirect('project/documents/preview/' . $project_id . '/documents');
            }

        } else {

            $this->data['file_details'] = $this->project_m->attachment_details($attachment_id);
            $this->data['project_id'] = $this->data['file_details']->project_id;
            $this->load->view('modal/upload_edit', $this->data);
        }
    }

    /**
     * download attachment
     */
    function download()
    {
        $this->load->helper('download');
        $attachment_id = $this->uri->segment(4);
        $file_details = $this->project_m->project_file_details($attachment_id);

        $fullpath = './files/project_files/' . $file_details->path . $file_details->file_name;
        if ($file_details->path == null) {
            $fullpath = './files/project_files/' . $file_details->file_name;
        }

        if ($fullpath) {
            $data = file_get_contents($fullpath);
            force_download($file_details->file_name, $data);
        } else {
            redirect(current_url());
        }
    }

    /**
     * view attachment details
     */
    function preview_document()
    {

        $attachment_id = $this->uri->segment(4);
        $file_details = $this->project_m->project_file_details($attachment_id);
        $this->data['file_path'] = $file_details->path . $file_details->file_name;
        $this->data['details'] = $file_details;
        $this->load->view('modal/document_preview', $this->data);

    }

    /**
     * delete attachment
     * @param null $attachment_id
     */
    function delete($attachment_id = null)
    {
        //@TODO: Remove attachment_details()/project_file_details.. Duplicated
        $file_details = $this->project_m->attachment_details($attachment_id);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        if (!pro_belongs_to_client($file_details->project_id, $client_id)) {
            redirect('no_access');
        }

        $file_name = $file_details->file_name;

        $path = $file_details->path;
        $fullpath = './files/project_files/' . $path . $file_name;
        if ($path == null) {
            $fullpath = './files/project_files/' . $file_name;
        }

        unlink($fullpath);

        if ($this->project_m->delete_attachment($attachment_id)) {
            redirect('/project/documents/preview/' . $file_details->project_id . '/documents');
        }

    }
}