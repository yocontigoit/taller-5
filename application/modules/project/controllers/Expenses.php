<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/05/20
 * Time: 08:57 AM
 */
class Expenses extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();

        $this->load->model('project_m');
    }


    /**
     * Preview project expenses..
     */
    function preview()
    {

        $this->data['option'] = $this->uri->segment(2);
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Overview';
 $this->data['client'] = $this->project_m->client();

        $this->data['project_id'] = $this->uri->segment(4);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }
        $this->data['expense_costs'] = $this->project_m->get_expenses(intval($this->data['project_id']));
        $this->data['transactions'] = $this->project_m->get_invoices(intval($this->data['project_id']));
        $this->data['est_trans'] = $this->project_m->get_est_trans_2(intval($this->data['project_id']));
        $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);
        $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * view expense details
     * @param null $expense_id
     * @param $project_id
     */
    function view($expense_id = null, $project_id)
    {

        $this->data['project_id'] = $project_id;
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Expenses';

        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        //check if attachment belongs to logged in client
        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }
        $this->data['client'] = $this->project_m->client();
        $this->data['expense_details'] = $this->project_m->expense_details($expense_id);
        $this->data['show_expenses_details'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['_PAGE'] = $this->load->view('groups/expense_details', $this->data, true);
        $this->load->view('_main_layout', $this->data);

    }

    /**
     * edit expense..
     */
    function edit_expense()
    {

        $expense_id = intval($this->uri->segment(3));
        $project_id = intval($this->uri->segment(4));
        $this->data['title'] = 'Edit Expense Costs';
        $this->data['project_id'] = intval($project_id);

        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['expense_details'] = $this->project_m->expense_details($expense_id);
        $this->data['categories'] = $this->project_m->categories();
        $this->load->view('modal/edit_expenses', $this->data);
    }

    /**
     * edit expense data
     */
    function edit_expense_costs()
    {

        if ($this->input->post()) {
            $cost_id = $this->input->post('cost_id');
            $project_id = $this->input->post('project_id');
            $file_name = '';

            if (file_exists($_FILES['expense_receipt']['tmp_name']) || is_uploaded_file($_FILES['expense_receipt']['tmp_name'])) {
                $file_name = $this->_upload_expense_receipt($_POST);

            }

            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $data = array(
                "description" => $this->input->post('cost_description'),
                "date_entry" => date('Y-m-d', strtotime($this->input->post('cost_date'))),
                "classification" => $this->input->post('cost_classification'),
                "estimate_cost" => $this->input->post('cost_estimate'),
                "real_cost" => $this->input->post('cost_actual')
            );

            if ($file_name != '') {
                $attach_data = array("receipt" => $file_name);
                $data = array_merge($data, $attach_data);
            }
            if ($this->project_m->update_cost_receipt($data, $cost_id)) {
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_cost_save_success'));

                $log_params = array(
                    'activity_user' => $user_id,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'expenses',
                    'activity_details' => 'Updated expense costs to project',
                    'activity_module_id' => $cost_id,
                    'activity_status' => "info",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => 'Actualizar costos',
                    'activity_icon' => 'fa fa-money'
                );

                $this->__activity_tracker($log_params);
                redirect('project/expenses/preview/' . $project_id);
            }
        }
    }

    /**
     * save expense attachment to directory
     * @param null $image_file
     * @return null|string
     */
    function _upload_expense_receipt($image_file = null)
    {

        $file_name = null;
        $success = false;
        if ($image_file) {
            $config['upload_path'] = './files/receipts/';
            $config['allowed_types'] = $this->config->item('system')->allowed_files;
            $config['remove_spaces'] = true;
            $config['overwrite'] = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('expense_receipt')) {
                $data = $this->upload->data();
                $file_name = $data['file_name'];
                $success = true;
            } else {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', lang('messages_receipt_add_error'));
                redirect('expenses');
            }
        } else {
            $success = false;
        }

        return ($success) ? $file_name : '';
    }

    /**
     * ave expense log
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->project_m->add_log($activity_data);

    }

    /**
     * delete expense
     */
    function delete()
    {
        if ($this->input->post()) {
            $project_id = $this->input->post('project_id');
            $cost_id = $this->input->post('cost_id');

            if ($this->project_m->delete_expense($cost_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_expense_delete_success'));
                redirect('project/expenses/preview/' . $project_id);
            }
        } else {

            $this->data['cost_id'] = $this->uri->segment(4);
            $this->data['project_id'] = $this->uri->segment(5);
            $this->load->view('modal/delete_expense', $this->data);
        }

    }
}
