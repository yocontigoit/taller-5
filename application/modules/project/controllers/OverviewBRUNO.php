<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/24
 * Time: 11:46 AM
 */
class Overview extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();

        $this->load->model('overview_m');
    }


    /**
     * Get project details/dashboards statistics..
     */
    function preview()
    {

        $this->data['option'] = $this->uri->segment(2);//tasks/files/gantt
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Overview';


        $this->data['project_id'] = $this->uri->segment(4);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }

        $this->data['activities'] = $this->overview_m->get_project_activities(intval($this->data['project_id']),
            'project');

        $this->data['project_details'] = $this->overview_m->project_details($this->data['project_id']);
        $this->data['assigned_team'] = $this->overview_m->get_project_manager($this->data['project_id']);
        $this->data['view_data']= $this->overview_m->view_data();

        $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);
        $this->data['show_project_timer'] = true;
        $this->data['show_project_overview'] = true;
  $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

	function index()
	{
		$this->data['view_data']= $this->overview_m->view_data();
		$this->load->view('overview', $this->data, FALSE);
	}

	function importbulkemail(){
		$this->load->view('overview');
	}

	function import(){
		if(isset($_POST["import"]))
		{
			$filename=$_FILES["file"]["tmp_name"];
			if($_FILES["file"]["size"] > 0)
			{
				$file = fopen($filename, "r");
				while (($importdata = fgetcsv($file, 10000, ",")) !== FALSE)
				{
                  $data = array(
                      'name' => $importdata[0],
                      'email' =>$importdata[1],
                      'created_date' => date('Y-m-d'),
                      );
					$insert = $this->overview_m->insertCSV($data);
				}
			fclose($file);
			$this->session->set_flashdata('message', 'Data imported successfully..');
			redirect('https://proyectosinternos.com/Taller5/project');
			}else{
				$this->session->set_flashdata('message', 'Something went wrong..');
			}
		}
	}
}
