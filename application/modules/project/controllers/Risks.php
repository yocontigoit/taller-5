<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/24
 * Time: 11:12 AM
 */
class Risks extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();

        $this->load->model('project_m');
    }


    function preview()
    {

        $this->data['option'] = $this->uri->segment(2);//tasks/files/gantt
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Overview';


        $this->data['project_id'] = intval($this->uri->segment(4));
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }
        $this->data['projectdata'] = json_encode($this->project_m->get_data($this->data['project_id']));

        $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);

        $this->load->view('_main_layout', $this->data);
    }

    public function add($project_id = null)
    {
        $this->data['content'] = 'project/gantt';
        $this->data['id'] = $project_id;
        $this->load->view('main_layout', $this->data);
    }

    public function show($project_id = null)
    {

        $log_params = array(
            'activity_user' => $this->CompanyUser_id,
            'activity_user_type' => $this->CompanyUser_type,
            'activity_module' => 'project',
            'activity_details' => $this->input->post('project_title'),
            'activity_module_id' => $project_id,
            //'activity_module_id'	    =>  "success",
            'activity_event' => "Agrega un nuevo proyecto ",
            'activity_icon' => 'fa fa-user'
        );

        $this->__activity_tracker($log_params);
        redirect('project/view/' . $project_id . '/overview');
    }
}