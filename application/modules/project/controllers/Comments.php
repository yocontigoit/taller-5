<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/07/01
 * Time: 08:35 AM
 */
class Comments extends Admin_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('project_m');
    }

    /**
     * Preview project comments..
     */
    function preview()
    {

        $this->data['option'] = $this->uri->segment(2);//tasks/files/gantt
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Comments';


        $this->data['project_id'] = $this->uri->segment(4);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        //check if comment belong to logged in client
        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }
        $this->data['comments'] = $this->project_m->project_comments(intval($this->data['project_id']));

  $this->data['show_datepicker'] = true;
        $this->data['show_project_comments'] = true;
        $this->data['message'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);


        $this->load->view('_main_layout', $this->data);
    }

    /**
     * add project comments
     */
    function add_project_comment()
    {

        if ($this->input->post()) {
            $project_id = $this->input->post('project_id');
            $title = $this->input->post('title');
            $period = $this->input->post('period');
            $cost_date = $this->input->post('cost_date');
            $user_id = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;

            $items = json_decode($this->input->post('items'));

            $param = array(
                "message_date" => date('Y-m-d H:i:s'),
                "Title" => $title,
                "Periodo" => $period,
                "message_text" => $this->input->post('message'),
                "project_id" => intval($project_id),
                "user_type" => $this->CompanyUser_type,
                "message_by_id" => intval($user_id)
            );

            $message_id = $this->project_m->add_project_comment($param, $items);
            if ($message_id) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => 'added a new comment',
                    'activity_module_id' => $message_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "New comment added successfully",
                    'activity_icon' => 'fa fa-plane'
                );
                //@TODO: notify assigned users... Maybe the customer as well
                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_project_comment_add_success'));
                // redirect('project/comments/preview/' . $project_id);

            }

        }//end if post
    }

    /**
     * save comment log
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->project_m->add_log($activity_data);

    }

    /**
     * add comment reply
     */
    function add_comment_reply()
    {

        if ($this->input->post()) {

            $project_id = $this->input->post('project_id');
            $message_id = $this->input->post('message_id');
            $user_id = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $param = array(
                "reply_date" => date('Y-m-d H:i:s'),
                "reply_message" => $this->input->post('message_reply'),
                "message_id" => intval($message_id),
                "user_type" => $this->CompanyUser_type,
                "replied_by" => intval($user_id)
            );

            $reply_id = $this->project_m->add_project_comment_replies($param);
            if ($reply_id) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => 'replied to  a comment',
                    'activity_module_id' => $reply_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Replied to comment no" . $message_id,
                    'activity_icon' => 'fa fa-plane'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_project_comment_reply_success'));
                redirect('project/comments/preview/' . $project_id);

            }

        }//end if post
    }

    public function print_report($report_id = null, $project_id = null)
    {
        $this->load->helper('pdf');

        $this->data['project_details'] = $this->project_m->project_details($project_id);
        $this->data['reporte'] = $this->project_m->get_report_data($report_id);

        generate_report_pdf($report_id, $this->data, 'default', $this->data['reporte']->message_id);
    }

    public function edit_report($report_id = null)
    {

      $this->data['report'] = $this->project_m->get_report_data($report_id);
      $this->data['report_items'] = $this->project_m->get_report_items($report_id);

      $this->load->view('modal/edit_report', $this->data);
    }
    /**
     * delete comment
     */
    function delete_comment()
    {

        $this->data['content'] = 'project/project_details';
        if ($this->input->post()) {

            $reply_id = intval($this->input->post('reply_id'));
            $project_id = intval($this->input->post('project_id'));
            if ($this->project_m->remove_project_comment($reply_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_comment_delete_success'));

            } else {

                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'Something went wrong deleting..');
            }
            redirect('project/comments/preview/' . $project_id);
        } else {

            $this->data['title'] = 'Delete Comment';
            $this->data['message_id'] = intval($this->uri->segment(4));
            $this->data['project_id'] = intval($this->uri->segment(5));
            $this->load->view('modal/delete_pro_comment', $this->data);
        }
    }

    /**
     * save milestone
     */
    function edit_comments()
    {

        if ($this->input->post()) {

            $message_id = intval($this->input->post('message_id'));
            $project_id = intval($this->input->post('project_id'));

            $param = array(
                "Title" => $this->input->post('Title'),
                "message_date" => $this->input->post('message_date'),
                "Periodo" => $this->input->post('Periodo'),
                "message_text" => $this->input->post('message_text'),
            );
            if ($this->project_m->update_comments($param, $message_id)) {
                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => "Milestone update",
                    'activity_module_id' => $milestone_id,
                    'activity_status' => "success",
                    'activity_event' => "Avance actualizado exitosamente",
                    'activity_icon' => 'fa fa-map-signs'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_milestone_save_success'));
                redirect('project/comments/preview/' . $project_id);
            }
        }
    }
}
