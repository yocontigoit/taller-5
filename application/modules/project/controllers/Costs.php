<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/03/10
 * Time: 10:33 AM
 */
class Costs extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();

        $this->load->model('project_m');
    }


    /**
     * Preview project costs..
     */
    function preview()
    {

        $this->data['option'] = $this->uri->segment(2);//tasks/files/gantt
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Overview';


        $this->data['project_id'] = $this->uri->segment(4);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }
        $this->data['project_details'] = $this->project_m->project_details(intval($this->data['project_id']));
        $this->data['assigned_team'] = $this->project_m->get_assigned_team(intval($this->data['project_id']));
        $this->data['additional_costs'] = $this->project_m->get_additional_costs(intval($this->data['project_id']));
        $this->data['expense_costs'] = $this->project_m->get_expense_costs(intval($this->data['project_id']));
        $this->data['expense_costs2'] = $this->project_m->get_expenses(intval($this->data['project_id']));

        $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);


        $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * add additional costs(expenses)
     * @param null $project_id
     */
    function additional_cost($project_id = null)
    {

        $this->data['title'] = 'Additional Costs';
        $this->data['project_id'] = intval($project_id);
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['categories'] = $this->project_m->categories();
        $this->load->view('modal/additional_costs', $this->data);
    }

    /**
     * add expenses
     * @param null $project_id
     */
    function expense_cost($project_id = null)
    {

        $this->data['title'] = 'Expense Costs';
        $this->data['project_id'] = intval($project_id);
        $this->data['estimaciones'] = $this->project_m->estimaciones();
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['categories'] = $this->project_m->categories();
        $this->load->view('modal/expense_costs', $this->data);
    }

    /**
     * save expense costs to database
     */
    function add_expense_costs()
    {

        if ($this->input->post()) {

            $file_name = '';

            if (file_exists($_FILES['expense_receipt']['tmp_name']) || is_uploaded_file($_FILES['expense_receipt']['tmp_name'])) {
                $file_name = $this->_upload_expense_receipt($_POST);

            }

            $project_id = intval($this->input->post('project_id'));
            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $data = array(
                "project_id" => $project_id,
                "budget_amt" => 0.00,
                "description" => $this->input->post('cost_description'),
                "date_entry" => date('Y-m-d', strtotime($this->input->post('cost_date'))),
                "classification" => $this->input->post('cost_classification'),
                "estimate_cost" => $this->input->post('cost_estimate'),
                "real_cost" => $this->input->post('cost_actual'),
                "cost_type" => 1,
                "receipt" => $file_name,
                "estimacion" => " ",
                "payment_type" => $this->input->post('payment_method')

            );

            if ($this->project_m->update_cost_plan($data)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_cost_save_success'));

                $log_params = array(
                    'activity_user' => $user_id,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => 'Added expense costs to project',
                    'activity_module_id' => $project_id,
                    'activity_status' => "info",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => 'Added expense  costs to project',
                    'activity_icon' => 'fa fa-money'
                );

                $this->__activity_tracker($log_params);
                redirect('project/expenses/preview/' . $project_id);
            }
        }
    }

    /**
     * save expense receipt to directory
     * @param null $image_file
     * @return null|string
     */
    function _upload_expense_receipt($image_file = null)
    {

        $file_name = null;
        $success = false;
        if ($image_file) {
            $config['upload_path'] = './files/receipts/';
            $config['allowed_types'] = $this->config->item('system')->allowed_files;
            $config['remove_spaces'] = true;
            $config['overwrite'] = true;
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('expense_receipt')) {
                $data = $this->upload->data();
                $file_name = $data['file_name'];
                $success = true;
            } else {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', lang('messages_receipt_add_error'));
                redirect('expenses');
            }
        } else {
            $success = false;
        }

        return ($success) ? $file_name : '';
    }

    /**
     * save expense log
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->project_m->add_log($activity_data);

    }

    /**
     * save additional cost
     */
    function add_additional_costs()
    {


        if ($this->input->post()) {

            $file_name = '';

            if (file_exists($_FILES['expense_receipt']['tmp_name']) || is_uploaded_file($_FILES['expense_receipt']['tmp_name'])) {
                $file_name = $this->_upload_expense_receipt($_POST);

            }
            $project_id = intval($this->input->post('project_id'));
            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;

            $data = array(
                "project_id" => $project_id,
                "budget_amt" => 0.00,
                "description" => $this->input->post('cost_description'),
                "date_entry" => date('Y-m-d', strtotime($this->input->post('cost_date'))),
                "classification" => $this->input->post('cost_classification'),
                "estimate_cost" => $this->input->post('cost_estimate'),
                "real_cost" => $this->input->post('cost_actual'),
                "cost_type" => 2,
                "receipt" => $file_name

            );

            if ($this->project_m->update_cost_plan($data)) {
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_cost_save_success'));

                $log_params = array(
                    'activity_user' => $user_id,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => 'Added additional costs to project',
                    'activity_module_id' => $project_id,
                    'activity_status' => "info",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => 'Added additional  costs to project',
                    'activity_icon' => 'fa fa-money'
                );

                $this->__activity_tracker($log_params);
                redirect('project/costs/preview/' . $project_id);

            }

        }
    }
}
