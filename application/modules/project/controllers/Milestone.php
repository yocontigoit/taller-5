<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 6/29/2016
 * Time: 10:40 PM
 */
class Milestone extends Admin_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('project_m');
    }


    /**
     * List all milestones
     */
    function preview()
    {

        $this->data['option'] = $this->uri->segment(2);
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Milestones';


        $this->data['project_id'] = $this->uri->segment(4);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        //check if milestone belong to logged in client
        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }

        $this->data['milestones'] = $this->project_m->get_project_milestone($this->data['project_id']);
        $this->data['concepts'] = $this->project_m->get_project_concepts($this->data['project_id']);


        $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);

        $this->data['show_milestone_progress'] = true;
        $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;

        $this->load->view('_main_layout', $this->data);
    }

    /**
     * add milestone
     * @param null $project_id
     */
    function add_project($project_id = null)
    {
        $project_id = intval($project_id);
        $this->data['project_id'] = $project_id;
        $this->load->view('modal/milestone', $this->data);
    }

    /**
     * add milestone
     * @param null $project_id
     */
    function add_concepto($project_id = null)
    {
        $project_id = intval($project_id);
        $this->data['project_id'] = $project_id;
        $this->load->view('modal/milestone_concepto', $this->data);
    }

    /**
     * edit milestone
     * @param null $project_id
     * @param null $milestone_id
     */
    function view($project_id = null, $milestone_id = null)
    {
        $project_id = intval($project_id);
        $this->data['project_id'] = $project_id;
        $this->data['avances']= $this->project_m->get_avances($this->data['project_id']);
        $this->data['milestone'] = $this->project_m->selected_milestone($project_id, $milestone_id);
        $this->load->view('modal/edit_milestone', $this->data);
    }

    /**
     * add milestone
     */
    function add_milestone_project()
    {

        if ($this->input->post()) {
            $project_id = $this->input->post('project_id');

            $param = array(
                "project_id" => $project_id,
                "milestone_name" => $this->input->post('milestone_name'),
                "importe" => $this->input->post('importe'),
                "description" => $this->input->post('milestone_description'),
                "start_date" => date('Y-m-d', strtotime($this->input->post('start_date'))),
                "due_date" => date('Y-m-d', strtotime($this->input->post('due_date'))),
                "created" => date('Y-m-d H:i:s'),
                "tipo" => 1,
                "newer" => 1
            );

            $milestone_id = $this->project_m->add_milestone($param);
            if ($milestone_id) {
                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => "New Milestone",
                    'activity_module_id' => $milestone_id,
                    'activity_status' => "success",
                    'activity_event' => "Nuevo avance agregado",
                    'activity_icon' => 'fa fa-map-signs'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_milestone_add_success'));
                redirect('project/milestone/preview/' . $project_id);

            }
        }
    }

    /**
     * add milestone
     */
    function add_milestone_concepto()
    {

        if ($this->input->post()) {
            $project_id = $this->input->post('project_id');

            $param = array(
                "project_id" => $project_id,
                "milestone_name" => $this->input->post('milestone_name'),
                "description" => $this->input->post('milestone_description'),
                "importe" => $this->input->post('importe'),
                "start_date" => date('Y-m-d', strtotime($this->input->post('start_date'))),
                "due_date" => date('Y-m-d', strtotime($this->input->post('due_date'))),
                "created" => date('Y-m-d H:i:s'),
                "tipo" => 1,
                "newer" => 0
            );

            $milestone_id = $this->project_m->add_milestone($param);
            if ($milestone_id) {
                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => "New Milestone",
                    'activity_module_id' => $milestone_id,
                    'activity_status' => "success",
                    'activity_event' => "Nuevo avance agregado",
                    'activity_icon' => 'fa fa-map-signs'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_milestone_add_success'));
                redirect('project/milestone/preview/' . $project_id);

            }
        }
    }

    /**
     * save milestone activities
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->project_m->add_log($activity_data);

    }

    /**
     * save milestone
     */
    function edit_milestone()
    {

        if ($this->input->post()) {

            $milestone_id = intval($this->input->post('milestone_id'));
            $project_id = intval($this->input->post('project_id'));

            $param = array(
                "milestone_name" => $this->input->post('milestone_name'),
                "description" => $this->input->post('milestone_description'),
                "importe" => $this->input->post('importe'),
                "start_date" => date('Y-m-d', strtotime($this->input->post('start_date'))),
                "due_date" => date('Y-m-d', strtotime($this->input->post('due_date')))
            );
            if ($this->project_m->update_milestone($param, $milestone_id)) {
                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => "Milestone update",
                    'activity_module_id' => $milestone_id,
                    'activity_status' => "success",
                    'activity_event' => "Avance actualizado exitosamente",
                    'activity_icon' => 'fa fa-map-signs'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_milestone_save_success'));
                redirect('project/milestone/preview/' . $project_id);
            }
        }
    }

    /**
     * delete milestone
     */
    function delete()
    {
        if ($this->input->post()) {
            $project_id = $this->input->post('project_id');
            $milestone_id = $this->input->post('milestone_id');

            if ($this->project_m->delete_milestone($milestone_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_milestone_delete_success'));
                redirect('project/milestone/preview/' . $project_id);
            }
        } else {
            $this->data['milestone_id'] = $this->uri->segment(4);
            $this->data['project_id'] = $this->uri->segment(5);
            $this->load->view('modal/delete_milestone', $this->data);
        }

    }
}
