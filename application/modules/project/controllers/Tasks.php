<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/24
 * Time: 11:12 AM
 */
class Tasks extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();
        $this->data['_task'] = null;
        $this->data['active_tasks'] = null;
        $this->data['closed_tasks'] = null;
        $this->data['all_tasks'] = null;
        $this->data['_task'] = true;

        $this->load->model('project_m');
        $this->data['show_datepicker'] = true;
    }

    /**
     * List all project tasks
     */
    function preview()
    {

        $this->data['option'] = $this->uri->segment(2);
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Tasks';

        $this->data['project_id'] = $this->uri->segment(4);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : null;

        if (!pro_belongs_to_client($this->data['project_id'], $client_id)) {
            redirect('no_access');
        }

        $task_page = $this->input->get('status', true) ? $this->input->get('status', true) : 'all';

        $this->data['active_page'] = $task_page;
        $this->data['assigned_team'] = $this->project_m->get_assigned_team(intval($this->data['project_id']));
        switch ($task_page) {


            case 'all':
                $this->data['all_tasks'] = $this->project_m->get_project_tasks(intval($this->data['project_id']),
                    $client_id);
                $this->dta['task_status'] = $this->project_m->get_task_status_name();
                $this->data['notes'] = $this->project_m->get_notes(intval($this->data['project_id']));
                $this->data['_TASK_PAGE'] = $this->load->view('pages/all_tasks', $this->data, true);
                $this->data['datatable'] = true;
                break;
            case 'active':
                $this->data['active_tasks'] = $this->project_m->get_project_active_tasks(intval($this->data['project_id']),
                    $client_id);
                $this->data['_TASK_PAGE'] = $this->load->view('pages/active_tasks', $this->data, true);
                $this->data['datatable'] = true;

                break;
            case 'closed':
                $this->data['closed_tasks'] = $this->project_m->get_project_closed_tasks(intval($this->data['project_id']),
                    $client_id);
                $this->data['_TASK_PAGE'] = $this->load->view('pages/closed_tasks', $this->data, true);
                $this->data['datatable'] = true;

                break;
        }


        $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);

        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * view task details
     * @param null $project_id
     * @param null $task_id
     */
    function details($project_id = null, $task_id = null)
    {

        $this->data['option'] = 'task_details';
        $this->data['content'] = 'project/project_details';
        $this->data['title'] = 'Tasks';


        $this->data['project_id'] = $project_id;
        $this->data['task_comments'] = $this->project_m->get_task_comments($task_id);
        $this->data['task_attachments'] = $this->project_m->task_attachments($task_id);
        $this->data['task_assigments'] = $this->project_m->get_task_assignment($project_id, $task_id);

        $this->data['show_task_comments'] = true;
        $this->data['show_task_timer'] = true;
        $this->data['show_task_check_list'] = true;
        $this->data['message'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['task_details'] = $this->project_m->get_task_details($project_id, intval($task_id));
        $this->data['_PAGE'] = $this->load->view('groups/' . $this->data['option'], $this->data, true);
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * add task
     * @param null $project_id
     */
    function add_new($project_id = null)
    {

        if ($this->input->post()) {
            $duration = 0;
            $task_options = explode("_", $this->input->post('task_depend'));
            $level = ($this->input->post('task_depend') == "none") ? 1 : $task_options[0] + 1;

            $milestone = ($this->input->post('task_milestone') == 'on') ? 1 : 0;
            $visible = ($this->input->post('task_visible') == 'on') ? 1 : 0;


            $status = "STATUS_" . get_task_status_name($this->input->post('task_status'));
            $duration = duration_diff($this->input->post('task_start'), $this->input->post('task_due'));
            $duration_hours = duration_diff_hours($this->input->post('task_start'), $this->input->post('task_due'));
            $param = array(
                "task_name" => $this->input->post('task_name'),//"Documention",
                "task_code" => $this->input->post('task_code'),//"",
                "task_level" => $level,//1,
                "task_milestone_id" => $this->input->post('task_milestone'),
                "project_id" => intval($project_id),
                "task_visible" => $visible,
                "task_use_checklist" => 1,
                "task_description" => $this->input->post('task_desc'),
                "task_requirements" => $this->input->post('task_req'),
                "task_progress" => $this->input->post('task_percentage'),//"percentage",
                "task_timer" => $this->input->post('task_timer'),//"percentage",

                "task_status" => $status,//"STATUS_ACTIVE",
                "status_task" => 1,
                "task_start" => strtotime($this->input->post('task_start')) * 1000,//1363320000000,
                "task_priority" => $this->input->post('task_priority'),//1363320000000,
                "task_duration" => $duration,//2,
                "task_est_hours" => $duration_hours,//2,
                "task_end" => strtotime($this->input->post('task_due')) * 1000,//1363665599999,
                "task_startIsMilestone" => $milestone,//false,
                "task_endIsMilestone" => $milestone,//false,
                "task_collapsed" => 0,//false,
                "task_assigs" => serialize($this->input->post('task_assign')),//[] serialized data..,
                //"task_depends" =>$this->input->post('task_depend'),
                "task_earlyStart" => 0,//2,
                "task_earlyFinish" => 0,//2,
                "task_latestStart" => 0,//2,
                "task_latestFinish" => 0,//2,
                "task_criticalCost" => 0,//2,
                "task_isCritical" => 0,//2,
                "task_hasChild" => 0,//2,
                "date_added" => date('Y-m-d H:i:s'),//"",
                "user_id" => intval($this->CompanyUser_id)
            );
            $task_param = array();
            //Update this task.. it does have a child
            if ($this->input->post('task_depend') != "none") {

                $task_param = array(
                    "task_id" => $task_options[1],
                    "project_id" => intval($project_id),
                    "task_hasChild" => 1,
                    "task_collapsed" => 1
                );

            }

            $task_id = $this->project_m->add_task($param, $task_param, $this->CompanyUser_id,
                $this->input->post('task_assign'));
            if ($task_id) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'tasks',
                    'activity_details' => $this->input->post('task_name'),
                    'activity_module_id' => $task_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Agregar una nueva tarea ",
                    'activity_icon' => 'fa fa-task'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_task_add_success'));

            }

            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', lang('messages_task_add_success'));
            redirect('project/tasks/preview/' . $project_id . '/tasks');

        }

    }

    /**
     * save task activities to server
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->project_m->add_log($activity_data);

    }

    /**
     * update task
     */
    function update_task()
    {

        if ($this->input->post()) {
            $duration = 0;
            $project_id = intval($this->input->post('project_id'));
            $task_id = intval($this->input->post('task_id'));
            $task_options = explode("_", $this->input->post('task_depend'));
            $level = ($this->input->post('task_depend') == "none") ? 1 : $task_options[0] + 1;

            $milestone = ($this->input->post('task_milestone') == 'on') ? 1 : 0;
            $visible = ($this->input->post('task_visible') == 'on') ? 1 : 0;
            $use_check_list = ($this->input->post('task_check_list') == 'on') ? 1 : 0;

            $status = "STATUS_" . get_task_status_name($this->input->post('task_status'));
            $duration = duration_diff($this->input->post('task_start'), $this->input->post('task_due'));
            $duration_hours = duration_diff_hours($this->input->post('task_start'), $this->input->post('task_due'));
            $param = array(
                "task_name" => $this->input->post('task_name'),//"Documention",
                "task_code" => $this->input->post('task_code'),//"",
                "task_level" => $level,//1,
                "task_milestone_id" => $this->input->post('task_milestone'),
                "project_id" => intval($project_id),
                "task_visible" => $visible,
                "task_use_checklist" => $use_check_list,
                "task_description" => $this->input->post('task_desc'),
                "task_requirements" => $this->input->post('task_req'),
                "task_progress" => $this->input->post('task_percentage'),//"percentage",
                "task_status" => $status,//"STATUS_ACTIVE",
                "task_start" => strtotime($this->input->post('task_start')) * 1000,//1363320000000,
                "task_priority" => $this->input->post('task_priority'),//1363320000000,
                "task_duration" => $duration,//2,
                "task_est_hours" => $duration_hours,//2,
                "task_end" => strtotime($this->input->post('task_due')) * 1000,//1363665599999,
                "task_startIsMilestone" => $milestone,//false,
                "task_endIsMilestone" => $milestone,//false,
                "task_collapsed" => 0,//false,
                "task_assigs" => serialize($this->input->post('task_assign')),//[] serialized data..,
                "task_earlyStart" => 0,//2,
                "task_earlyFinish" => 0,//2,
                "task_latestStart" => 0,//2,
                "task_latestFinish" => 0,//2,
                "task_criticalCost" => 0,//2,
                "task_isCritical" => 0,//2,
                "task_hasChild" => 0,//2,
                "date_added" => date('Y-m-d H:i:s'),//"",
                "user_id" => intval($this->CompanyUser_id)
            );

            //Update this task.. it does have a child
            if ($this->input->post('task_depend') != "none") {

                $task_param = array(
                    "task_id" => $task_options[1],
                    "project_id" => intval($project_id),
                    "task_hasChild" => 1,
                    "task_collapsed" => 1
                );

            }
            $this->project_m->update_milestone_progress($task_id);
            $task_id = $this->project_m->update_task($param, $task_param, $task_id, $this->input->post('task_assign'));
            if ($task_id) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => $this->input->post('task_name'),
                    'activity_module_id' => $task_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Tarea actualizada",
                    'activity_icon' => 'fa fa-task'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_task_add_success'));

            }

            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', lang('messages_task_add_success'));
            redirect('project/tasks/preview/' . $project_id . '/tasks');

        }


    }

    /**
     * update task
     */
    function update_task_status()
    {

        if ($this->input->post()) {

            $project_id = intval($this->input->post('project_id'));
            $task_id = intval($this->input->post('task_id'));
            $status = ($this->input->post('status') == 'on') ? 1 : 0;
            $millestone = intval($this->input->post('avance'));

            if ($status == 0) {
              $param = array(
                  "status_task" => $status,
                  "project_id" => intval($project_id),
                  "task_status" => "STATUS_Terminada"
              );

              $progress = array(
                  'progress' => 100
              );
            }else {
              $param = array(
                  "status_task" => $status,
                  "project_id" => intval($project_id),
                  "task_status" => "STATUS_Activa"
              );

              $progress = array(
                  'progress' => 0
              );
            }


            $task_id = $this->project_m->update_task_status($param, $task_id, $progress, $millestone);
            if ($task_id) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'project',
                    'activity_details' => $this->input->post('task_name'),
                    'activity_module_id' => $task_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Tarea actualizada",
                    'activity_icon' => 'fa fa-task'
                );



            }

            if ($status == 1) {
              $this->session->set_flashdata('msg_status', 'success');
              $this->session->set_flashdata('message', 'Tarea activa.');
              redirect('project/tasks/preview/' . $project_id . '/tasks');
            }else {
              $this->session->set_flashdata('msg_status', 'error');
              $this->session->set_flashdata('message', 'Tarea terminada.');
              redirect('project/tasks/preview/' . $project_id . '/tasks');
            }



        }


    }

    /**
     * load task edit page
     * @param null $project_id
     * @param null $task_id
     */
    function edit_task($project_id = null, $task_id = null)
    {
        $this->data['project_id'] = $project_id;
        $this->data['task_details'] = $this->project_m->get_task_details($project_id, intval($task_id));
        $this->data['project_tasks'] = $this->project_m->get_project_tasks($project_id);
        $this->data['users'] = $this->project_m->get_assigned_team($project_id);
        $this->data['milestones'] = $this->project_m->get_project_milestone($project_id);
        $this->data['view_data']= $this->project_m->view_data($this->data['project_id']);
        $this->data['task_members'] = $this->project_m->get_task_assignment($project_id, $task_id);
        $this->load->view('modal/task_edit', $this->data);

    }

    /**
     * load task add page
     * @param $project_id
     */
    function add_task($project_id)
    {

        $this->data['project_id'] = $project_id;
        $this->data['project_tasks'] = $this->project_m->get_project_tasks($project_id);
        $this->data['project_members'] = $this->project_m->get_assigned_team($project_id);
        $this->data['milestones'] = $this->project_m->get_project_milestone($project_id);
        $this->data['view_data']= $this->project_m->view_data($this->data['project_id']);
        $this->load->view('modal/task', $this->data);
    }

    /**
     * delete task
     */
    function delete_task()
    {

        if ($this->input->post()) {
            $project_id = $this->input->post('project_id');
            $task_id = $this->input->post('task_id');


            if ($this->project_m->delete_task($task_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_task_delete_success'));
                redirect('project/tasks/preview/' . $project_id . '/tasks');
            }
        } else {
            $this->data['task_id'] = $this->uri->segment(5);
            $this->data['project_id'] = $this->uri->segment(4);
            $this->load->view('modal/delete_task', $this->data);
        }
    }

    /**
     * mark checklist as complete
     * @param $listid
     * @param $value
     */
    public function completed_list($listid, $value)
    {

        $this->project_m->mark_check_list_complete($listid, $value);

    }

    /**
     * add checklist
     */
    function add_checklist()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                echo json_encode(array(
                    'success' => $this->project_m->add_checklist_item($this->input->post(), $this->CompanyUser_id)
                ));
            }
        }

    }

    /**
     * get all the check lists
     */
    public function init_checklist()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $post_data = $this->input->post();
                $this->data['task_id'] = $post_data['task_id'];
                $this->data['check_lists'] = $this->project_m->get_checklist_items($post_data['task_id']);
                $this->load->view('project/ajax/checklist_template', $this->data);
            }
        }
    }

    /*
     * update task progress
     */

    /**
     * update checklist
     */
    function update_checklist()
    {

        if ($this->input->is_ajax_request()) {
            if ($this->input->post()) {
                $this->project_m->update_checklist_item($this->input->post('listid'),
                    $this->input->post('description'));
            }
        }


    }

    function update_task_progress($task_id = null, $progress = 0)
    {

        $this->project_m->checklist_update_task_progress($task_id, $progress);

    }

    /**
     * delete checklist
     * @param $id
     */
    function delete_checklist($id)
    {
        if ($this->input->is_ajax_request()) {
            echo json_encode(array('success' => $this->project_m->delete_checklist_item($id)));
        }

    }

    /**
     * mark task as completed
     */
    function mark_complete()
    {
        $task_id = null;
        $task_id = explode('~', $_POST['selectedtask']);
        unset($_POST['selectedtask']);

        for ($k = 0; $k < count($task_id); $k++) {
            $success = null;
            $CustomerName = null;
            $success = $this->project_m->update_selected_tasks((int)$task_id[$k]);

            if ($success) {
                echo json_encode(array(
                    'success' => true,
                    'message' => 'Task :' . (int)$task_id[$k] . ' Updated successfully'
                ));
            }

        }

    }

    /**
     * mark task as completed
     */
    function update_selected_pendiente()
    {
        $task_id = null;
        $task_id = explode('~', $_POST['selectedtask']);
        unset($_POST['selectedtask']);

        for ($k = 0; $k < count($task_id); $k++) {
            $success = null;
            $CustomerName = null;
            $success = $this->project_m->update_selected_pendiente((int)$task_id[$k]);

            if ($success) {
                echo json_encode(array(
                    'success' => true,
                    'message' => 'Task :' . (int)$task_id[$k] . ' Updated successfully'
                ));
            }

        }

    }



    /**
     * add task comment
     * @param null $task_id
     */
    function add_task_comment($task_id = null)
    {

        if ($this->input->post()) {

            $project_id = $this->input->post('project_id');
            $param = array(
                "message_date" => date('Y-m-d H:i:s'),
                "message_text" => $this->input->post('task_message'),
                "task_id" => intval($task_id),
                //"user_type"=>$this->CompanyUser_type,
                "message_by_id" => intval($this->CompanyUser_id)
            );

            $message_id = $this->project_m->add_task_comment($param);
            if ($message_id) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'tasks',
                    'activity_details' => 'added a new comment',
                    'activity_module_id' => $message_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Agregar un nuevo comentario ",
                    'activity_icon' => 'fa fa-task'
                );
                //@TODO: notify assigned users... Maybe the customer as well
                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_task_comment_add_success'));
                redirect('project/tasks/details/' . $project_id . '/' . $task_id . '/tasks');

            }

        }//end if post

    }

    /**
     * add task reply
     * @param null $message_id
     */
    function add_task_reply($message_id = null)
    {


        if ($this->input->post()) {

            $project_id = $this->input->post('project_id');
            $task_id = $this->input->post('task_id');
            $param = array(
                "reply_date" => date('Y-m-d H:i:s'),
                "reply_message" => $this->input->post('message_reply'),
                "message_id" => intval($message_id),
                //"user_type"=>$this->CompanyUser_type,
                "replied_by" => intval($this->CompanyUser_id)
            );

            $reply_id = $this->project_m->add_task_comment_replies($param);
            if ($reply_id) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'tasks',
                    'activity_details' => 'replied to  a comment',
                    'activity_module_id' => $reply_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Respondido al comentario número" . $message_id,
                    'activity_icon' => 'fa fa-task'
                );
                //@TODO: notify assigned users... Maybe the customer as well
                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_task_comment_reply_success'));
                redirect('project/tasks/details/' . $project_id . '/' . $task_id . '/tasks');

            }

        }//end if post

    }

    /**
     * get tasks active timers
     */
    function get_active_timer()
    {

        $task_id = intval($this->input->post('task_id'));
        $project_id = intval($this->input->post('project_id'));
        $time_entry = $this->project_m->get_task_timer($task_id, $project_id, $this->CompanyUser_id);

        if (!empty($time_entry)) {
            $start_time = $time_entry->start_timer;
            $end_time = time();

            $time_clocked = $end_time - $start_time;
            $response = array('has_timer' => true, 'timer_offset' => $time_clocked);
        } else {
            $response = array('has_timer' => false);
        }
        echo json_encode($response);

    }

    /**
     * stop/start task timers
     */
    function task_timer()
    {

        $project_task = $this->input->post('task_id');
        $project_id = $this->input->post('project_id');
        $start_timer = $this->input->post('start_timer');

        if ($start_timer == 'true') {
            $timer_entry = array(
                'task_id' => $project_task,
                'project_id' => $project_id,
                'user_id' => $this->CompanyUser_id,
                'start_timer' => time(),
                'entry_date' => date('Y-m-d,H:i:s'),
                'active' => 1

            );
            $timer_id = $this->project_m->log_task_timesheet($timer_entry);
            $response = array('success' => true);
        } else {
            $timer_entry = array(
                'stop_timer' => time(),
                'active' => 0
            );
            if ($this->project_m->stop_task_timer($timer_entry, $project_task, $this->CompanyUser_id)) {
                $response = array('success' => true);
            }

        }
        echo json_encode($response);


    }
}
