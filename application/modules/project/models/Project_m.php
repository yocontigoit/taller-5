<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/09/11
 * Time: 9:51 AM
 */
class Project_M extends CI_Model
{


    /**
     * Get all users for a company|
     * @param null $id
     * @return mixed
     */
    function get_company_users($id = null)
    {

        $company_id = intval($id);
        $this->db->from('users');
        $this->db->join('employee', 'employee.emp_id  = users.user_id');
        $this->db->where("company_id", $company_id);

        return $this->db->get()->result();

    }

     function get_discounts($project_id)
    {

        $this->db->from('invoice');
       $this->db->where("project_id", $project_id);
       $this->db->where("disc_amount >", '0');

        return $this->db->get()->result();

    }

    function get_invoices($project_id)
   {

       $this->db->from('invoice');
       $this->db->where("project_id", $project_id);

       return $this->db->get()->result();

   }

   function client()
  {

      $this->db->from('client');
      return $this->db->get()->result();

  }



     function get_3percent()
    {

        $this->db->from('est_trans');
        return $this->db->get()->result();

    }

     function get_avances($project_id)
    {
        $this->db->from('invoice_item');

        $this->db->where("project_id", $project_id);

        return $this->db->get()->result();
    }

    /**
     * Get all clients for a company
     * @param null $id
     * @return mixed
     */
    function get_company_clientsn($id = 1)
    {

        $company_id = intval($id);
        $this->db->from('client');

        $this->db->where("client_type", $company_id);

        return $this->db->get()->result();

    }

    /**
     * Get all clients for a company
     * @param null $id
     * @return mixed
     */
    function get_clients_user($id = 28)
    {

        $company_id = intval($id);
        $this->db->from('client_user');

        $this->db->where("client_id", $company_id);

        return $this->db->get()->result();

    }

    function get_all_users($id = null)
    {

        $company_id = intval($id);
        $this->db->from('users');

        return $this->db->get()->result();

    }

    /**
     * Get all clients for a company
     * @param null $id
     * @return mixed
     */
    function get_company_clients($id = null)
    {

        $company_id = intval($id);
        $this->db->from('client');
        $this->db->where("company_id", $company_id);

        return $this->db->get()->result();

    }

    /**
     * Get all projects..
     * @return mixed
     */
    function get_all_projects()
    {

        $this->db->from('projects');

        return $this->db->get()->result();

    }

    function get_settings()
    {

        $this->db->from('project_settings');
        $this->db->where("id > 0");

        return $this->db->get()->result();

    }

    function get_all_assigned_projects($user_id = null)
    {
        $this->db->from('projects');
        $this->db->join('project_assignment', 'project_assignment.project_id  = projects.project_id');
        $this->db->where("project_assignment.user_id", intval($user_id));

        return $this->db->get()->result();

    }

    function get_project_milestone($project_id = null)
    {

        $this->db->from('milestones');
        $this->db->where("project_id", $project_id);

        return $this->db->get()->result();

    }
    function get_project_concepts($project_id = null)
    {
      $project_details = $this->project_details($project_id);
$code = $project_details->project_code;
        $this->db->from('import');
        $this->db->where("codigo_proyecto", $code);

        return $this->db->get()->result();

    }

    /**
     * Get all projects that belong to a client
     * @param null $client_id
     * @return mixed
     */
    function get_client_projects($client_id = null)
    {

        $this->db->from('projects');
        $this->db->where("client_id", intval($client_id));

        return $this->db->get()->result();

    }

    /**
     * Get all projects..
     * @return mixed
     */
    function get_all_project_roles()
    {

        $this->db->from('project_roles');

        return $this->db->get()->result();

    }

    function categories()
    {
        $this->db->from('classification');

        return $this->db->get()->result();

    }

    function estimaciones()
    {
        $this->db->from('estimaciones');

        return $this->db->get()->result();

    }

    /**
     * @param array $data
     * @param null $user_id
     * @param array $assign_data
     * @return project_id / nothing
     */
    function add_project($data = array(), $user_id = null, $assign_data = array())
    {

        $success = false;
        $project_id = null;
        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        if ($user_id) {
            $success = $this->db->insert('projects', $data);
            $project_id = $this->db->insert_id();

            if ($success) {


                $reference_no = explode("-", $data['project_code']);
                $success = $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
                    array('typeid' => 13, 'company_id' => $this->Company_id));

                if ($success) {
                    if (count($assign_data)) {
                        foreach ($assign_data as $key => $user) {
                            $success = $this->db->insert('project_assignment',
                                array(
                                    'project_id' => $project_id,
                                    'user_id' => intval($user),
                                    'role' => 4
                                ));
                        }
                    }
                }
                if ($data['project_progress'] == 100 && $data['project_use_worklog'] == 0) {

                    $this->_update_task_progress($project_id);
                }
            }
        }

        $this->db->trans_complete();

        return $success ? $project_id : null;

    }

    function _update_task_progress($project_id = null)
    {
        $success = false;
        $success = $this->db->update('tasks', array("task_status" => "STATUS_DONE", "task_progress" => 100),
            array('project_id' => $project_id));

        return $success;
    }

    function save_attachment($data = array())
    {

        $success = false;

        $success = $this->db->insert('attachments', $data);
        $attachment_id = $this->db->insert_id();

        return $success ? $attachment_id : null;

    }

    function get_notes($project_id = null)
    {

        $this->db->from('notes');
        $this->db->where("project_id", intval($project_id));

        return $this->db->get()->result();
    }

    function note_details($note_id = null)
    {

        $this->db->from('notes');
        $this->db->where("note_id", intval($note_id));

        return $this->db->get()->row();
    }


    function  add_note($data = array())
    {

        $success = false;

        $success = $this->db->insert('notes', $data);
        $note_id = $this->db->insert_id();

        return $success ? $note_id : null;
    }

    function update_note($data = array(), $param2 = array(), $param3 = array(), $param4 = array(), $param5 = array(), $param6 = array(), $param7 = array(),
     $assign_data = array(), $assign_data1 = array(), $assign_data2 = array(), $assign_data3 = array(), $assign_data4 = array(), $assign_data5 = array(), $project_id = null, $note_id = null)
    {
        $success = false;
        $success = $this->db->update('notes', $data, array('note_id' => $note_id));
        if (isset($param2)) {
          $success = $this->db->insert('tasks', $param2);
              $task_id = $this->db->insert_id();
              if (count($assign_data)) {
                foreach ($assign_data as $key => $user) {
                  $success = $this->db->insert('task_assignment',
                    array(
                        'project_id' => $project_id,
                        'task_id' => $task_id,
                        'user_id' => intval($user)
                    ));
                  }
                }
        }
        if (isset($param3)) {
          $success = $this->db->insert('tasks', $param3);
              $task_id = $this->db->insert_id();
              if (count($assign_data1)) {
                foreach ($assign_data1 as $key => $user) {
                  $success = $this->db->insert('task_assignment',
                    array(
                        'project_id' => $project_id,
                        'task_id' => $task_id,
                        'user_id' => intval($user)
                    ));
                  }
                }
        }
        if (isset($param4)) {
          $success = $this->db->insert('tasks', $param4);
              $task_id = $this->db->insert_id();
              if (count($assign_data2)) {
                foreach ($assign_data2 as $key => $user) {
                  $success = $this->db->insert('task_assignment',
                    array(
                        'project_id' => $project_id,
                        'task_id' => $task_id,
                        'user_id' => intval($user)
                    ));
                  }
                }
        }
        if (isset($param5)) {
          $success = $this->db->insert('tasks', $param5);
              $task_id = $this->db->insert_id();
              if (count($assign_data3)) {
                foreach ($assign_data3 as $key => $user) {
                  $success = $this->db->insert('task_assignment',
                    array(
                        'project_id' => $project_id,
                        'task_id' => $task_id,
                        'user_id' => intval($user)
                    ));
                  }
                }
        }
        if (isset($param6)) {
          $success = $this->db->insert('tasks', $param6);
              $task_id = $this->db->insert_id();
              if (count($assign_data4)) {
                foreach ($assign_data4 as $key => $user) {
                  $success = $this->db->insert('task_assignment',
                    array(
                        'project_id' => $project_id,
                        'task_id' => $task_id,
                        'user_id' => intval($user)
                    ));
                  }
                }
        }
        if (isset($param7)) {
          $success = $this->db->insert('tasks', $param7);
              $task_id = $this->db->insert_id();
              if (count($assign_data5)) {
                foreach ($assign_data5 as $key => $user) {
                  $success = $this->db->insert('task_assignment',
                    array(
                        'project_id' => $project_id,
                        'task_id' => $task_id,
                        'user_id' => intval($user)
                    ));
                  }
                }
        }
        return $success;

    }

    function update_attachment($data = array(), $attachment_id = null, $project_id = null)
    {

        $success = false;
        $success = $this->db->update('attachments', $data,
            array('attachment_id' => $attachment_id, 'project_id' => $project_id));

        return $success;

    }

    function delete_attachment($attachment_id = null)
    {
        $success = false;
        $success = $this->db->delete('attachments', array('attachment_id' => $attachment_id));

        return $success;
    }

    function project_attachments($project_id = null)
    {

        $this->db->from('attachments');
        $this->db->where("project_id", intval($project_id));

        return $this->db->get()->result();
    }

    function attachment_details($attach_id = null)
    {

        $this->db->from('attachments');
        $this->db->where("attachment_id", intval($attach_id));

        return $this->db->get()->row();

    }

    function task_attachments($task_id = null)
    {

        $this->db->from('attachments');
        $this->db->where("task_id", intval($task_id));

        return $this->db->get()->result();
    }

    function project_file_details($attachment_id = null)
    {

        $this->db->from('attachments');
        $this->db->where("attachment_id", intval($attachment_id));

        return $this->db->get()->row();
    }


    function edit_project($data = array(), $assign_data = array(), $project_id = null)
    {

        $success = false;
        $project_id = intval($project_id);
        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();


        $success = $this->db->update('projects', $data, array('project_id' => $project_id));


        if ($success) {

            $success = $this->db->delete('project_assignment', array('project_id' => $project_id));
            if ($success) {
                if (count($assign_data)) {
                    foreach ($assign_data as $key => $user) {
                        $success = $this->db->insert('project_assignment',
                            array(
                                'project_id' => $project_id,
                                'user_id' => intval($user),
                                'role' => 4
                            ));
                    }
                }
            }

            if ($data['project_progress'] == 100 && $data['project_use_worklog'] == 0) {

                $this->_update_task_progress($project_id);
            }
        }


        $this->db->trans_complete();

        return $success;

    }

    function invoice_project($data = array(), $project_id = null)
    {

        $success = false;
        $this->db->trans_start();

        $success = $this->db->insert('invoice', $data);
        $invoice_id = $this->db->insert_id();

        if ($success) {
            $project_details = $this->project_details($project_id);
            $project_cost = project_total_done_costs($project_id);
            $project_hours = get_work_done($project_id);

            $items_data = array(
                'invoice_id' => $invoice_id,
                'item_name' => 'Project Code: ' . $project_details->project_code,
                'item_desc' => $project_details->project_title . ' bill for ' . $project_hours . lang('hours'),
                'price' => $project_cost,
                'quantity' => $project_hours

            );
            $success = $this->db->insert('invoice_item', $items_data);


            if ($success) {
                $reference_no = explode("-", $data['inv_no']);
                $success = $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
                    array('typeid' => 10, 'company_id' => $this->Company_id));
            }

        }

        $this->db->trans_complete();

        return ($success) ? $invoice_id : null;
    }

    function project_details($project_id = null)
    {

        $project_id = intval($project_id);
        $this->db->from('projects');
        $this->db->where("project_id", $project_id);

        return $this->db->get()->row();
    }

    function get_task_timer($task_id = null, $project_id = null, $user_id = null)
    {

        $this->db->from('timer_entries');
        $this->db->where("project_id", intval($project_id));
        $this->db->where("task_id", intval($task_id));
        $this->db->where("user_id", intval($user_id));
        $this->db->where('active', 1);

        return $this->db->get()->row();

    }


    function get_report_data($report_id = null)
    {

        $this->db->from('project_comments');

        $this->db->where("message_id", $report_id);

        return $this->db->get()->row();

    }

    function get_report_items($report_id = null)
    {

        $this->db->from('project_comments_items');

        $this->db->where("comment_id", $report_id);

        return $this->db->get()->result();

    }

    function get_project_timer($project_id = null, $user_id = null)
    {
        $this->db->from('project_timer');
        $this->db->where("project_id", intval($project_id));
        if ($user_id) {
            $this->db->where("timer_by", intval($user_id));
        }
        $this->db->where('timer_active', 1);

        return $this->db->get()->row();

    }

    function log_project_timesheet($data = array())
    {

        $success = false;
        $this->db->set($data);
        $success = $this->db->insert('project_timer');
        $timer_id = $this->db->insert_id();

        return ($success) ? $timer_id : null;
    }

    function stop_project_timer($data = array(), $project_id = null, $user_id = null)
    {
        $success = false;
        $this->db->set($data);
        $this->db->where("project_id", intval($project_id));
        $this->db->where("timer_by", intval($user_id));
        $this->db->where('timer_active', 1);
        $success = $this->db->update('project_timer');

        $this->_project_log($project_id);

        return $success;
    }

    function  _project_log($project_id = null)
    {

        $hours = project_hours($project_id);
        $this->db->update('projects', array('project_time_logged' => $hours), array('project_id' => $project_id));
    }

    function update_project_timesheet($data = array(), $project_id = null, $user_id = null)
    {
        $success = false;
        $this->db->set($data);
        $this->db->where("project_id", intval($project_id));
        $this->db->where("timer_by", intval($user_id));
        $this->db->where('timer_active', 0);
        $success = $this->db->update('project_timer');

        return $success;

    }

    function expense_details($expense_id = null)
    {
        $this->db->from('costs');
        $this->db->where("cost_id", $expense_id);

        return $this->db->get()->row();
    }

    function delete_expense($cost_id = null)
    {

        $success = false;
        $success = $this->db->where('cost_id', $cost_id)->delete('costs');

        return $success;

    }

    function update_cost_receipt($data = array(), $cost_id = null)
    {
        $success = false;
        $success = $this->db->update('costs', $data, array('cost_id' => $cost_id));

        return $success;
    }

    function log_task_timesheet($data = array())
    {

        $success = false;
        $this->db->set($data);
        $success = $this->db->insert('timer_entries');
        $timer_id = $this->db->insert_id();

        return ($success) ? $timer_id : null;
    }


    function stop_task_timer($data = array(), $task_id = null, $user_id = null)
    {
        $success = false;
        $this->db->set($data);
        $this->db->where("user_id", intval($user_id));
        $this->db->where("task_id", intval($task_id));
        $this->db->where('active', 1);
        $success = $this->db->update('timer_entries');

        return $success;
    }


    function get_manual_logged_time($project_id)
    {

        $this->db->from('timer_entries');
        $this->db->select('*,SUM((timer_entries.stop_timer -timer_entries.start_timer)/3600) as hours');
        $this->db->join('tasks', 'tasks.project_id  = timer_entries.project_id');
        $this->db->where("timer_entries.task_id = tasks.task_id");
        $this->db->where("timer_entries.project_id", intval($project_id));
        $this->db->where('manual_entry', 1);
        $this->db->group_by('timer_entries.task_id,timer_entries.user_id');

        return $this->db->get()->result();

    }

    function add_task_comment($data = array())
    {
        $success = false;
        $success = $this->db->insert('task_messages', $data);
        $comment_id = $this->db->insert_id();

        return ($success) ? $comment_id : null;

    }

    function add_task_comment_replies($data = array())
    {

        $success = false;
        $success = $this->db->insert('task_message_replies', $data);
        $reply_id = $this->db->insert_id();

        return ($success) ? $reply_id : null;
    }

    function add_project_comment($data = array(), $items = array())
    {

        $success = false;
        $success = $this->db->insert('project_comments', $data);
        $comment_id = $this->db->insert_id();

        if ($success) {
            //add items
            foreach ($items as $item) {
                if ($item->item_date != '' && $item->item_partida != '' && $item->item_concept != '') {

                    $param = array(
                        "comment_id" => $comment_id,
                        "date" => $item->item_date,
                        "item" => $item->item_partida,
                        "concept" => $item->item_concept
                    );
                    $success = $this->db->insert('project_comments_items', $param);

                }
            }
        }

        return ($success) ? $comment_id : null;
    }

    function add_project_comment_replies($data = array())
    {

        $success = false;
        $success = $this->db->insert('project_comments_replies', $data);
        $reply_id = $this->db->insert_id();

        return ($success) ? $reply_id : null;

    }

    function project_comments($project_id = null)
    {
        $this->db->from('project_comments');
        $this->db->where("project_id", $project_id);

        return $this->db->get()->result();

    }

    function remove_task_log($log_id = null)
    {
        $success = false;
        $success = $this->db->delete('timer_entries', array('timer_id' => $log_id));

        return $success;

    }

    function get_task_comments($task_id = null)
    {

        $this->db->from('task_messages');
        $this->db->where("task_id", $task_id);

        return $this->db->get()->result();

    }

    function  get_assigned_team($project_id = null)
    {

        $this->db->from('project_assignment');
        $this->db->where("project_id", $project_id);

        return $this->db->get()->result();

    }

    function  update_assigned_team($data = array(), $project_id = null, $user_id = null)
    {


        $this->db->update('project_assignment', $data, array('project_id' => $project_id, 'user_id' => $user_id));
    }

    function add_project_risk($data = array())
    {

        $success = false;

        $success = $this->db->insert('project_risks', $data);

        return $success;

    }

    /**
     * @param array $data
     * @return bool
     */
    function update_cost_plan($data = array())
    {
        $success = false;
        $success = $this->db->insert('costs', $data);

        return $success;
    }

    function get_additional_costs($project_id = null)
    {

        $this->db->from('costs');
        $this->db->where("project_id", $project_id);
        $this->db->where("cost_type", 2);

        return $this->db->get()->result();

    }

    function get_expense_costs($project_id = null)
    {

        $this->db->select('SUM(estimate_cost) as total_estimates,SUM(real_cost) as total_actual ', false);
        $this->db->from('costs');
        $this->db->where("project_id", $project_id);
        $this->db->where("cost_type", 1);

        return $this->db->get()->row();
    }

    function get_expenses($project_id = null)
    {


        $this->db->from('costs');
        $this->db->where("project_id", $project_id);
        $this->db->where("cost_type", 1);

        return $this->db->get()->result();
    }

    /**
     * @param array $data
     * @param null $user_id
     * @return task_id_id / nothing
     */
    function add_task($data = array(), $task_param = array(), $user_id = null, $assign_data = array())
    {

        $success = false;
        $task_id = null;
        $task_progress = 0;
        $project_id = 0;
        $project_details = null;
        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        if ($user_id) {
            $project_id = $data['project_id'];

            $task_progress = $data['task_progress'];
            $project_details = $this->project_details($project_id);

            $success = $this->db->insert('tasks', $data);
            $task_id = $this->db->insert_id();


            if ($success) {
                if (count($assign_data)) {
                    foreach ($assign_data as $key => $user) {
                        $success = $this->db->insert('task_assignment',
                            array(
                                'project_id' => $project_id,
                                'task_id' => $task_id,
                                'user_id' => intval($user)
                            ));
                    }
                }
            }
        }


        if ($project_details->project_use_worklog == 0) {
            $success = $this->update_project_progress($project_id, $task_id, $task_progress);

        } elseif ($project_details->project_use_worklog == 0 && $task_progress == 100) {
            $success = $this->update_task_logged_times($task_id);
        }

        if (!empty($task_param)) {
            $task_id = intval($task_param['task_id']);
            $this->db->update('tasks', $task_param, array('task_id' => $task_id));
        }

        $this->db->trans_complete();

        return $success ? $task_id : null;

    }

    function update_project_progress($project_id = null, $task_id = null, $task_progress = null)
    {
        $success = false;

        if ($task_progress == 100) {
            $success = $this->db->update('tasks', array("task_status" => "STATUS_DONE"), array('task_id' => $task_id));

        }

        $percentage = task_complete_perc($project_id);
        $success = $this->db->update('projects', array("project_progress" => $percentage),
            array('project_id' => $project_id));

        return $success;
    }

    function update_task_logged_times($task_id = null)
    {

        $success = false;
        $this->db->from('tasks');
        // $this->db->where("task_progress < 100");
        $this->db->where("task_id", $task_id);
        $query = $this->db->get()->row();
        if ($query) {
            $number_of_user = total_users_assigned_to_task($task_id);
            $estimated_hours = ($query->task_end / 1000) - ($query->task_start / 1000);
            $assigned_users = $this->db->where('task_id', $task_id)->get('task_assignment')->result();
            if ($assigned_users) {

                foreach ($assigned_users as $user) {

                    $this->db->select("SUM(stop_timer - start_timer) AS time_spent,timer_entries.user_id,timer_entries.project_id");
                    $this->db->join('task_assignment', 'task_assignment.task_id = timer_entries.task_id');
                    $this->db->where('task_assignment.task_id', $task_id);

                    $this->db->where('timer_entries.user_id', $user->user_id);
                    $res = $this->db->get('timer_entries')->row();
                    if ($res) {
                        $new_log = ($estimated_hours - $res->time_spent) / $number_of_user;
                        $timer_entry = array(
                            'task_id' => $task_id,
                            'project_id' => $res->project_id,
                            'user_id' => $res->user_id,
                            'start_timer' => 0,
                            'stop_timer' => $new_log,
                            'entry_date' => date('Y-m-d,H:i:s'),
                            'active' => 1

                        );
                        $success = $this->db->insert("timer_entries", $timer_entry);
                    }//end if res
                }

            }
        }

        return $success;
    }

    function update_task($data = array(), $task_param = array(), $task_id = null, $assign_data = array())
    {
        $success = false;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        $project_id = intval($data['project_id']);

        if ($data['task_status'] == "STATUS_DONE") {
            $data['task_progress'] = 100;
        }

        if ($data['task_progress'] == 100) {
            $data['task_status'] = "STATUS_DONE";
        }

        $task_progress = $data['task_progress'];
        $project_details = $this->project_details($project_id);
        $success = $this->db->update('tasks', $data, array('task_id' => $task_id));



        if ($success) {
            $success = $this->db->delete('task_assignment', array('task_id' => $task_id));
            if ($success) {
                if (count($assign_data)) {
                    foreach ($assign_data as $key => $user) {
                        $success = $this->db->insert('task_assignment',
                            array(
                                'project_id' => $project_id,
                                'task_id' => $task_id,
                                'user_id' => intval($user)
                            ));
                    }
                }
            }

        }
        if ($success) {

            if ($project_details->project_use_worklog == 0) {
                $success = $this->update_project_progress($project_id, $task_id, $task_progress);

            } elseif ($project_details->project_use_worklog == 0 && $task_progress == 100) {
                $success = $this->update_task_logged_times($task_id);
            }
        }

        if (isset($task_param)) {

            $this->db->update('tasks', $task_param);

        }
        $this->update_milestone_progress($task_id);
        $this->db->trans_complete();

        return $success ? $task_id : null;

    }

    function update_task_status($data = array(), $task_id = null, $progress = array(), $milestone = null)
    {
        $success = false;

        //Run these queries as a transaction, we want to make sure we do all or nothing
        $this->db->trans_start();

        $project_id = intval($data['project_id']);

        $project_details = $this->project_details($project_id);
        $success = $this->db->update('tasks', $data, array('task_id' => $task_id));

        if ($success) {
          $success = $this->db->update('milestones', $progress, array('id' => $milestone));
        }

        $this->db->trans_complete();

        return $success ? $task_id : null;

    }

    function update_milestone_progress($task_id = null)
    {

        $this->db->from('milestones');
        $this->db->join('tasks', 'milestones.id = tasks.task_milestone_id');
        $this->db->where("task_id", $task_id);
        $query = $this->db->get()->row();
        if ($query) {
            $percentage = 0;
            $all_milestone_tasks = $this->db->where('task_milestone_id', $query->id)->get('tasks')->num_rows();
            $complete_milestone_tasks = $this->db->where(
                array(
                    'task_status' => 'STATUS_DONE',
                    'task_milestone_id' => $query->id
                ))->get('tasks')->num_rows();

            if ($all_milestone_tasks > 0) {
                $percentage = round(($complete_milestone_tasks / $all_milestone_tasks) * 100);
            }
            $this->db->update('milestones', array("progress" => $percentage), array('id' => $query->id));
        }


    }

    function delete_milestone($milestone_id = null)
    {

        $success = false;
        $success = $this->db->delete('milestones', array('id' => $milestone_id));

        return $success;
    }

    function update_selected_tasks($task_id = null)
    {

        $success = false;
        $success = $this->db->update('tasks', array("task_status" => "STATUS_Terminada", "task_progress" => 100),
            array('task_id' => $task_id));
        $this->update_milestone_progress($task_id);
        $task_info = get_task_details($task_id);
        $percentage = task_complete_perc($task_info->project_id);
        $success = $this->db->update('projects', array("project_progress" => $percentage),
            array('project_id' => $task_info->project_id));

        return $success;

    }

    function update_selected_pendiente($task_id = null)
    {

        $success = false;
        $success = $this->db->update('pendientes', array("com_status" => "STATUS_Terminada"),
            array('com_id' => $task_id));


        return $success;

    }

    function  get_task_assignment($project_id = null, $task_id = null)
    {

        $this->db->from('task_assignment');
        $this->db->where("project_id", $project_id);
        $this->db->where("task_id", $task_id);

        return $this->db->get()->result();

    }


    function add_log($data = array())
    {

        $success = false;
        $success = $this->db->insert("activity_log", $data);

        return $success;
    }


    function get_task_details($project_id = null, $task_id = null)
    {

        $this->db->from('tasks');
        $this->db->where("project_id", $project_id);
        $this->db->where("task_id", $task_id);

        return $this->db->get()->row();

    }

    /**
     * Get all the tasks for a selected project..
     * @param null $project_id
     * @return Array of tasks
     */
    function get_project_tasks($project_id = null, $client_id = null)
    {

        $this->db->from('tasks');
        if ($client_id) {
            $this->db->where("task_visible = 1");
        }
        $this->db->where("project_id", $project_id);

        return $this->db->get()->result();
    }

    function get_project_active_tasks($project_id = null, $client_id = null)
    {

        $this->db->from('tasks');
        if ($client_id) {
            $this->db->where("task_visible = 1");
        }
        $this->db->where("task_status", "STATUS_Activa");
        $this->db->where("project_id", $project_id);

        return $this->db->get()->result();
    }

    function get_project_closed_tasks($project_id = null, $client_id = null)
    {

        //$where_au = "(task_status = 'STATUS_DONE' OR task_progress >= 100)";
        $this->db->from('tasks');

        if ($client_id) {
            $this->db->where("task_visible = 1");
        }
        $this->db->where("task_status", "STATUS_Terminada");
        $this->db->where("project_id", $project_id);

        return $this->db->get()->result();
    }

    function get_project_team($project_id = null)
    {

        $this->db->from('project_assignment');
        $this->db->where("project_id", $project_id);

        return $this->db->get()->result();
    }

    function add_issue($data = array())
    {

        $success = false;
        $success = $this->db->insert("issue", $data);
        $issue_id = $this->db->insert_id();

        return ($success) ? $issue_id : null;

    }

    function add_milestone($data = array())
    {
        $success = false;
        $success = $this->db->insert("milestones", $data);
        $issue_id = $this->db->insert_id();

        return ($success) ? $issue_id : null;

    }

    function selected_milestone($project_id = null, $milestone_id = null)
    {

        $this->db->from('milestones');
        $this->db->where("project_id", $project_id);
        $this->db->where("id", $milestone_id);

        return $this->db->get()->row();
    }

    function update_milestone($data = array(), $milestone_id = null)
    {
        $success = false;
        $success = $this->db->where('id', $milestone_id)->update('milestones', $data);

        return $success;
    }

    function update_comments($data = array(), $message_id = null)
    {
        $success = false;
        $success = $this->db->where('message_id', $message_id)->update('project_comments', $data);

        return $success;
    }

    function update_issue_status($issue_id = null, $status_id = null)
    {

        $success = false;
        $success = $this->db->where('issue_id', $issue_id)->update('issue', array('issue_status' => $status_id));

        return $success;

    }

    function update_issue_assignment($data = array(), $issue_id = null)
    {

        $success = false;
        $success = $this->db->where('issue_id', $issue_id)->update('issue', $data);

        return $success;
    }


    function get_project_issues($project_id = null)
    {

        $this->db->from('issue');
        $this->db->where("project_id", $project_id);

        return $this->db->get()->result();
    }

    function get_issue_details($issue_id = null)
    {

        $this->db->from('issue');
        $this->db->where("issue_id", $issue_id);

        return $this->db->get()->row();

    }

    function get_issue_messages($issue_id = null)
    {

        $this->db->from('issue_replies');
        $this->db->where("issue_id", $issue_id);

        return $this->db->get()->result();

    }

    function add_issue_reply($data = array())
    {

        $success = false;
        $success = $this->db->insert('issue_replies', $data);

        return $success;
    }

    public function add_checklist_item($data, $user_id = null)
    {

        $this->db->insert('check_lists', array(
            'task_id' => $data['task_id'],
            'description' => '',
            'date_added' => date('Y-m-d H:i:s'),
            'user_id' => $user_id
        ));

        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            return true;
        }

        return false;
    }

    function remove_project_comment($reply_id = null)
    {

        $this->db->where('reply_id', $reply_id);
        $this->db->delete('project_comments_replies');
        if ($this->db->affected_rows() > 0) {
            return true;
        }

        return false;
    }

    public function delete_checklist_item($id)
    {

        $this->db->where('id', $id);
        $this->db->delete('check_lists');
        if ($this->db->affected_rows() > 0) {
            return true;
        }


        return false;
    }

    function mark_check_list_complete($list_id = null, $value = null)
    {

        $list_id = intval($list_id);
        $this->db->where('id', $list_id);
        $this->db->update('check_lists', array('finished' => $value));

    }
    /**
     * update user image
     * @param $image_name
     * @param $user_id
     * @return bool
     */
    function update_profile_image($image_name, $project_id)
    {
        $success = false;
        $data = array(
            'avatar' => $image_name,
        );
        $success = $this->db->where('project_id', $project_id)->update('projects', $data);

        return $success;
    }
    function checklist_update_task_progress($task_id = null, $percentage = 0)
    {

        $task_id = intval($task_id);
        $success = false;

        if ($percentage == 100) {
            $success = $this->db->update('tasks', array("task_status" => "STATUS_DONE", "task_progress" => $percentage),
                array('task_id' => $task_id));

        } else {
            $success = $this->db->update('tasks',
                array("task_status" => "STATUS_Activa", "task_progress" => $percentage), array('task_id' => $task_id));
        }
        $this->update_milestone_progress($task_id);
        $task_info = get_task_details($task_id);
        $percentage = task_complete_perc($task_info->project_id);
        $success = $this->db->update('projects', array("project_progress" => $percentage),
            array('project_id' => $task_info->project_id));

        return $success;
    }

    public function update_checklist_item($id, $description)
    {
        $this->db->where('id', $id);
        $this->db->update('check_lists', array('description' => nl2br($description)));
    }

    function get_checklist_items($task_id = null)
    {

        $this->db->where('task_id', $task_id);
        $this->db->order_by('list_order', 'asc');

        return $this->db->get('check_lists')->result();
    }


    function delete_task($task_id = null)
    {

        $success = false;

        $this->db->trans_start();
        $success = $this->db->delete('tasks', array('task_id' => $task_id));
        if ($success) {
            $success = $this->db->delete('check_lists', array('task_id' => $task_id));
            if ($success) {
                $msgs = $this->db->where(array('task_id' => $task_id))->get('task_messages')->row();
                if (count($msgs)) {
                    foreach ($msgs as $msg) {
                        $success = $this->db->delete('task_message_replies', array('message_id' => $msg->message_id));
                    }
                }
                if ($success) {
                    $success = $this->db->delete('task_messages', array('task_id' => $task_id));
                    if ($success) {
                        $success = $this->db->delete('timer_entries', array('task_id' => $task_id));
                        if ($success) {
                            $success = $this->db->delete('task_assignment', array('task_id' => $task_id));
                        }
                    }
                }
            }
        }
        $this->db->trans_complete();

        return $success;
    }


    function delete_project($project_id = null)
    {

        $success = false;
        $this->db->trans_start();
        $success = $this->db->delete('project_assignment', array('project_id' => $project_id));
        if ($success) {
            $success = $this->db->delete('project_risks', array('project_id' => $project_id));
            if ($success) {
                $success = $this->db->delete('notes', array('project_id' => $project_id));
                if ($success) {
                    $success = $this->db->delete('costs', array('project_id' => $project_id));
                    if ($success) {
                        $success = $this->db->delete('milestones', array('project_id' => $project_id));
                        if ($success) {//delete tasks
                            $task_d = $this->db->where(array('project_id' => $project_id))->get('tasks')->result();

                            if (count($task_d)) {
                                foreach ($task_d as $tsk) {
                                    $msgs = $this->db->where(array('task_id' => $tsk->task_id))->get('task_messages')->result();
                                    if (count($msgs)) {
                                        foreach ($msgs as $msg) {
                                            $success = $this->db->delete('task_message_replies',
                                                array('message_id' => $msg->message_id));
                                        }
                                    }
                                    if ($success) {
                                        $success = $this->db->delete('task_messages',
                                            array('task_id' => $tsk->task_id));
                                        if ($success) {
                                            $success = $this->db->delete('timer_entries',
                                                array('task_id' => $tsk->task_id));
                                            if ($success) {
                                                $success = $this->db->delete('task_assignment',
                                                    array('task_id' => $tsk->task_id));
                                            }
                                        }
                                    }
                                }//end tasks
                                if ($success) {
                                    $success = $this->db->delete('tasks', array('project_id' => $project_id));
                                }
                            }//end if tasks

                        }
                        if ($success) {//delete aatachments
                            $attachments = $this->db->where(array('project_id' => $project_id))->get('attachments')->result();
                            if (count($attachments)) {
                                foreach ($attachments as $att) {
                                    $fullpath = './files/project_files/' . $att->path . $att->file_name;
                                    if ($att->path == null) {
                                        $fullpath = './files/project_files/' . $att->file_name;
                                    }

                                    if (is_file($fullpath)) {
                                        unlink($fullpath);
                                    }
                                    $success = $this->db->delete('attachments',
                                        array('attachment_id' => $att->attachment_id));
                                }
                            }

                        }


                    }
                }
            }
            $pro_comments = $this->db->where(array('project_id' => $project_id))->get('project_comments')->result();
            if (count($pro_comments)) {
                foreach ($pro_comments as $comm) {
                    $success = $this->db->delete('project_comments_replies', array('message_id' => $comm->message_id));
                }
                if ($success) {
                    $success = $this->db->delete('project_comments', array('project_id' => $project_id));
                }
            }
        }
        $success = $this->db->delete('projects', array('project_id' => $project_id));
        $this->db->trans_complete();

        return $success;

    }


    function getStatusDescription($status)
    {

        $this->db->from('Status');
        $this->db->select(" CONCAT('STATUS_', description) AS Status ", false);
        $this->db->where("statusid = ", 1);

        return $this->db->get()->row();

    }

    function view_data($project_id = null){
	  $this->db->select('*');
      $this->db->from('import');
      $this->db->where('project_id', $project_id);

    return $this->db->get()->result();
    }

    function get_task_status_name(){

        $this->db->select('status_name');
        $this->db->from('task_status');

        return $this->db->get()->result();

    }

    function get_est_trans(){

        $this->db->from('est_trans');


        return $this->db->get()->result();

    }

    function get_est_trans_2($project_id){

        $this->db->from('est_trans');
        $this->db->where('project_id', $project_id);

        return $this->db->get()->result();

    }


}
