<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/07/09
 * Time: 12:18 PM
 */
class Overview_M extends CI_Model
{

    function get_project_activities($project_id, $module_id, $limit = 10)
    {
        $this->db->where('activity_module = "project"');
        $this->db->where('activity_module_id ', $project_id);

        return $this->db->order_by('activity_date', 'DESC')->get('activity_log', $limit)->result();
    }

    function project_details($project_id = null)
    {

        $project_id = intval($project_id);
        $this->db->from('projects');
        $this->db->where("project_id", $project_id);

        return $this->db->get()->row();
    }
    
    function clean_pi($project_id = null)
    {
        $success = false;
        $success = $this->db->delete('milestones', array('project_id' => $project_id));

        return $success;
    }

    function get_project_manager($project_id = null)
    {

        $this->db->from('project_assignment');
        $this->db->where("project_id", $project_id);
        $this->db->where("role", 1);

        return $this->db->get()->result();
    }

	function insertCSV($data, $param)
            {
                $this->db->insert('import', $data);
                $this->db->insert('milestones', $param);
                return TRUE;
            }



    function view_data($project_id = null){
	  $this->db->select('*');
      $this->db->from('import');
      $this->db->where('project_id', $project_id);

    return $this->db->get()->result();
    }

    function get_project_milestone($project_id = null, $tipo = 1)
    {

        $this->db->from('milestones');
        $this->db->where("project_id", $project_id);
        $this->db->where('tipo', $tipo);
        return $this->db->get()->result();

    }
    function get_est_trans($client_id = null)
    {

        $this->db->from('est_trans');
        $this->db->where("client_id", $client_id);
        return $this->db->get()->result();

    }

    function get_import_milestone($project_id = null, $tipo = 2)
    {

        $this->db->from('milestones');
        $this->db->where("project_id", $project_id);
        $this->db->where('tipo', $tipo);
        return $this->db->get()->result();

    }
}
