<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/09
 * Time: 10:47 AM
 */
class Gantt_M extends CI_Model
{


    function get_gantt_data($project_id = null)
    {
        $this->db->from('projects');
        $this->db->where("project_id = " . $project_id);
        $this->db->limit(1);
        $query = $this->db->get();


        $count = 0;
        if ($query->num_rows() > 0) {

            $Prj = $query->row();
            $Project[$count] = new stdClass();
            $start_date = strtotime($Prj->project_start_date) * 1000;
            $due_date = strtotime($Prj->project_due_date) * 1000;
            $duration = duration_diff($Prj->project_start_date, $Prj->project_due_date);
            $Project[$count]->id = $Prj->project_id;
            $Project[$count]->name = $Prj->project_title;
            $Project[$count]->start = floatval($start_date);//3100000223
            $Project[$count]->end = floatval($due_date);
            $Project[$count]->duration = $duration;
            $Project[$count]->description = $Prj->project_desc;
            $Project[$count]->code = $Prj->project_code;
            $Project[$count]->status = "STATUS_ACTIVE";
            $Project[$count]->level = 0; //$Prj->project_level;
            $Project[$count]->startIsMilestone = false;
            $Project[$count]->endIsMilestone = false;
            $Project[$count]->collapsed = false;
            $Project[$count]->assigs = array();
        }

        $task = $this->get_gantt_tasks($Project, $project_id);

        return $task;
    }

    function get_gantt_tasks($Project, $project_id = null)
    {

        $this->db->from('tasks');
        $this->db->where("project_id = " . $project_id);
        $tasks = $this->db->get()->result();
        $count = 0;

        foreach ($tasks as $task) {
            $tasks[$count] = new stdClass();
            $tasks[$count]->id = "tmp_" . $task->task_id;
            $tasks[$count]->code = "";
            $tasks[$count]->depends = (string)$task->task_depends;
            $tasks[$count]->level = intval($task->task_level);
            $tasks[$count]->duration = $task->task_duration;
            $tasks[$count]->status = "STATUS_ACTIVE";//tasks_status($task->status);//$this->getStatusDescription($task->status);
            $tasks[$count]->name = $task->task_name;
            $tasks[$count]->start = floatval($task->task_start);
            $tasks[$count]->end = floatval($task->task_end);
            $tasks[$count]->collapsed = false;
            $tasks[$count]->isCritical = ($task->task_isCritical == 1) ? true : false;
            $tasks[$count]->hasChild = ($task->task_hasChild == 1) ? true : false;
            $tasks[$count]->collapsed = ($task->task_collapsed == 1) ? true : false;
            $tasks[$count]->startIsMilestone = ($task->task_startIsMilestone == 1) ? true : false;
            $tasks[$count]->endIsMilestone = ($task->task_endIsMilestone == 1) ? true : false;
            $tasks[$count]->assigs = array();//unserialize($task->task_assigs);//$this->get_task_assigned_resources($tasks[$count]->task_id);
            $count++;
        }

        //Admin/limit users from writing
        $canWrite = array("selectedRow" => $count, "canWrite" => true, "canWriteOnParent" => true);

        //Get resources
        // $this->load->model('resource_m');
        $resources['resources'] = array();//$this->resource_m->getProjectResources();//All Users

        $Roles['roles'] = array();//$this->roles_m->getProjectRoles();//admin/casual/project_manager

        $result = array_merge($Project, $tasks);
        $array['tasks'] = $result;

        $OtherArrray = array_merge($array, $canWrite);
        $finalArray = array_merge($OtherArrray, $resources, $Roles);

        return $finalArray;
    }

    function get_task_assigned_resources($task_id)
    {

        $this->db->get_where('ass', array('task_id' => $task_id), 1);
        $Project = $this->db->get()->result();
    }

    function get_task_network($task_id, $project_id)
    {

        $this->db->get_where('projects', array('project_id' => $project_id), 1);
        $Project = $this->db->get()->result();

    }

    function get_project_resources($project_id)
    {//All assigned projects

        $this->db->get_where('projects', array('project_id' => $project_id), 1);
        $Project = $this->db->get()->result();

    }

    function get_project_roles($project_id)
    {

        $this->db->get_where('projects', array('project_id' => $project_id), 1);
        $Project = $this->db->get()->result();

    }


}