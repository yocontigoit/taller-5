<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/09
 * Time: 02:29 PM
 */
class Calendar extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();

        $this->load->model('calendar_m');
    }

    /**
     * list all transaction and events
     */
    function index()
    {
        $this->data['content'] = 'calendar/calendar';
        $this->data['title'] = 'Calendario';

        $this->data['tasks'] = $this->calendar_m->tasks();
        $this->data['payments'] = $this->calendar_m->payments();
        $this->data['projects'] = $this->calendar_m->projects();
        $this->data['invoices'] = $this->calendar_m->invoices();
        $this->data['estimates'] = $this->calendar_m->estimates();
        $this->data['quotations'] = $this->calendar_m->quotations();
        $this->data['agenda'] = $this->calendar_m->agenda();;
        $this->data['show_calendar'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);

    }

    /**
     * display transaction /events details via modal
     * @param $type
     * @param $id
     */
    function event($type, $id)
    {

        $id = intval($id);
        switch ($type) {
            case "tasks" :
                $this->data['tasks'] = $this->calendar_m->task_details($id);
                $this->load->view('modal/task_details', $this->data);
                break;
            case "payments" :
                $this->data['payments'] = $this->calendar_m->payment_details($id);
                $this->load->view('modal/payment_details', $this->data);
                break;
            case "projects" :
                $this->data['projects'] = $this->calendar_m->project_details($id);
                $this->load->view('modal/project_details', $this->data);
                break;
            case "invoices" :
                $this->data['invoices'] = $this->calendar_m->invoice_details($id);
                $this->load->view('modal/invoice_details', $this->data);
                break;
            case "estimates" :
                $this->data['estimates'] = $this->calendar_m->estimate_details($id);
                $this->load->view('modal/estimate_details', $this->data);
                break;
            case "quotations" :
                $this->data['quotations'] = $this->calendar_m->quotation_details($id);
                $this->load->view('modal/quotation_details', $this->data);
                break;
            case "events" :
                $this->data['quotations'] = $this->calendar_m->agenda_details($id);
                $this->load->view('modal/event_details', $this->data);
                break;
        }

    }

    /**
     * add new event to calendar
     */
    function add_event()
    {

        if ($this->input->post()) {

            $start_date = $this->input->post('start_date');
            $end_time = $this->input->post('event_time');
            $start_date = date('Y-m-d H:i:s', strtotime($start_date . ' ' . $end_time));

            $end_date = $this->input->post('end_date');
            $end_time = $this->input->post('end_time');
            $end_date = date('Y-m-d H:i:s', strtotime($end_date . ' ' . $end_time));

            $event_color = ($this->input->post('colorPick')) ? $this->input->post('colorPick') : '#78a32d';
            $data = array(
                "start_date" => $start_date,
                "end_date" => $end_date,
                "user_id" => $this->CompanyUser_id,
                "event_title" => $this->input->post('event_title'),
                "event_description" => $this->input->post('event_desc'),
                "event_color" => $event_color
            );
            //add event to database
            if ($this->calendar_m->add_event($data)) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'calendar',
                    'activity_details' => $this->input->post('event_title'),
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Nuevo evento agregado",
                    'activity_icon' => 'fa fa-calendar'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_event_add_success'));
                redirect('calendar');

            }
        } else {
            $this->load->view('modal/new_event', $this->data);
        }

    }

    /**
     * add activity log
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->calendar_m->add_log($activity_data);

    }

    /**
     * add api to integrate with google
     */
    function settings()
    {

        if ($this->input->post()) {
            $data = array(
                "gcal_api_key" => $this->input->post('gcal_api_key'),
                "gcal_id" => $this->input->post('gcal_id')
            );

            if ($this->calendar_m->calendar_settings($data, $this->Company_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_calendar_save_success'));
                redirect('calendar');
            }
        } else {
            $this->load->view('modal/settings', $this->data);
        }

    }
}