<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
                <span aria-hidden="true">
                <i class="fa fa-times"></i>
                </span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_calendar_settings'); ?></h4>
        </div>
        <?php echo form_open('calendar/settings', array()); ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="startDate">Google Calendar API</label>
                        <input type="text" class="form-control"
                               value="<?php echo $this->config->item('system')->gcal_api_key; ?>" name="gcal_api_key">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="startDate">Calendar ID</label>
                        <input type="text" class="form-control"
                               value="<?php echo $this->config->item('system')->gcal_id; ?>" name="gcal_id">
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button class="btn btn-success btn-icon" name="submit" type="input">
                <i class="fa fa-check-square-o"></i>
                Save
            </button>
            <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                <i class="fa fa-times-circle-o"></i>
                Cancel
            </button>
        </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->