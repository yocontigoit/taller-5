<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_task_details'); ?></h4>
        </div>
        <?php echo form_open('', array()); ?>


        <div class="modal-body">
          <table class="infoTable">
            <thead>
            <tr>

                <th><?php echo lang('label_project_name'); ?> </th>
                <th> Tipo de Proyecto </th>
                <th class="hidden-xs">Tareas de la semana </th>
                <th>A cargo</th>
            </tr>
            </thead>
              <tbody>

                  <td class="infoVal"><?php echo $tasks->project_title; ?></td>
                  <td class="infoVal"><?php echo $tasks->project_type; ?></td>
                  <td class="infoVal"><?php echo $tasks->task_name; ?></td>
				  <td class="infoVal">


                        <?php
                        $task_assigned_team = task_assigned($tasks->task_id);

                        if (count($task_assigned_team)): foreach ($task_assigned_team as $team): ?>
                            <?php

                            $user_details = users_details($team->user_id, 1);

                            ?>

                            <a href="">
                                <img data-toggle="tooltip" data-title="<?php echo $user_details->full_name; ?>"
                                     src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                                     class="assigned-image-small" alt="<?php echo $user_details->full_name; ?>"
                                     data-original-title="" title="">
                            </a>
                        <?php endforeach;endif; ?>


                    </td>
              </tr>

              </tbody>
          </table>

        </div>


        <div class="modal-footer">
            <button class="btn btn-success btn-icon" name="submit" type="input">
                <i class="fa fa-check-square-o"></i>
                Save
            </button>
            <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                <i class="fa fa-times-circle-o"></i>
                Cancel
            </button>
        </div>
        </form>
    </div>
</div>
<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>

<script type="text/javascript">

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    if (jQuery().timepicker) {
        $('.timepicker').timepicker({
            //$('.timepicker-24').timepicker({
            autoclose: true,
            minuteStep: 5,
            showSeconds: true,
            showMeridian: false
            // })

        });
    }
    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }
</script>
