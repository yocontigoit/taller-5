<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<style>
    input[type="radio"] {
        display: none;
    }
</style>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_issues'); ?></h4>
        </div>
        <?php echo form_open('calendar/add_event', array()); ?>


        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="startDate"><?php echo "Start Date"; ?></label>
                        <input type="text" class="form-control datepicker" name="start_date"
                               data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                               value="<?php echo date('d-m-Y') ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="eventTime"><?php echo "Start Time"; ?></label>

                        <div class="input-group input-append bootstrap-timepicker">
                            <input type="text" class="form-control timepicker timepicker-24" name="event_time">
                            <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="endDate"><?php echo "End Date"; ?></label>
                        <input type="text" class="form-control datepicker" name="end_date" data-date-format="dd-mm-yyyy"
                               placeholder="dd-mm-yyyy" value="<?php echo date('d-m-Y') ?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="endTime"><?php echo "End Time"; ?></label>

                        <div class="input-group input-append bootstrap-timepicker">
                            <input type="text" class="form-control timepicker timepicker-24" name="end_time">
                            <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="eventTitle"><?php echo "Title"; ?></label>
                        <input type="text" class="form-control" name="event_title" required=""/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="colorPick"><?php echo "Color"; ?></label><br/>
                        <input type="radio" name="colorPick" id="radioPrimary1" class="236b9b" value="#236b9b"/>
                        <label for="radioPrimary1" class="radPrimary"><i class="fa fa-square-o"></i></label>

                        <input type="radio" name="colorPick" id="radioPrimary2" class="1e5d86" value="#1e5d86"/>
                        <label for="radioPrimary2" class="radPrimary2"><i class="fa fa-square-o"></i></label>

                        <input type="radio" name="colorPick" id="radioInfo1" class="4da0d7" value="#4da0d7"/>
                        <label for="radioInfo1" class="radInfo"> <i class="fa fa-square-o"></i></label>

                        <input type="radio" name="colorPick" id="radioInfo2" class="3895d2" value="#3895d2"/>
                        <label for="radioInfo2" class="radInfo2"> <i class="fa fa-square-o"></i></label>

                        <input type="radio" name="colorPick" id="radioSuccess1" class="77c123" value="#77c123"/>
                        <label for="radioSuccess1" class="radSuccess"> <i class="fa fa-square-o"></i></label>

                        <input type="radio" name="colorPick" id="radioSuccess2" class="6aab1f" value="#6aab1f"/>
                        <label for="radioSuccess2" class="radSuccess2"> <i class="fa fa-square-o"></i></label>

                        <input type="radio" name="colorPick" id="radioWarning1" class="e5ad12" value="#e5ad12"/>
                        <label for="radioWarning1" class="radWarning"> <i class="fa fa-square-o"></i></label>

                        <input type="radio" name="colorPick" id="radioWarning2" class="cd9b10" value="#cd9b10"/>
                        <label for="radioWarning2" class="radWarning2"> <i class="fa fa-square-o"></i></label>

                        <input type="radio" name="colorPick" id="radioDanger1" class="d64e18" value="#d64e18"/>
                        <label for="radioDanger1" class="radDanger"> <i class="fa fa-square-o"></i></label>

                        <input type="radio" name="colorPick" id="radioDanger2" class="a83d13" value="#a83d13"/>
                        <label for="radioDanger2" class="radDanger2"> <i class="fa fa-square-o"></i></label>

                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="event_desc"><?php echo "Description"; ?></label>
                <textarea class="form-control" name="event_desc" rows="4"></textarea>

            </div>
        </div>


        <div class="modal-footer">
            <button class="btn btn-success btn-icon" name="submit" type="input">
                <i class="fa fa-check-square-o"></i>
                Save
            </button>
            <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                <i class="fa fa-times-circle-o"></i>
                Cancel
            </button>
        </div>
        </form>
    </div>
</div>
<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>

<script type="text/javascript">

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    if (jQuery().timepicker) {
        $('.timepicker').timepicker({
            //$('.timepicker-24').timepicker({
            autoclose: true,
            minuteStep: 5,
            showSeconds: true,
            showMeridian: false
            // })

        });
    }

</script>
