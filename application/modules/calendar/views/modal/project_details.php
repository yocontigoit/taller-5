<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_project_details'); ?></h4>
        </div>
        <?php echo form_open('', array()); ?>


        <div class="modal-body">
            <div class="user-content">
                <table class="infoTable">
                    <tbody>
                    <tr>
                        <td class="infoKey"><i class="fa fa-user"></i> Project No:</td>
                        <td class="infoVal"><?php echo $projects->project_code; ?></td>
                    </tr>
                    <tr>
                        <td class="infoKey"><i class="fa fa-calendar-o"></i><?php echo lang('label_start_date'); ?>:
                        </td>
                        <td class="infoVal"><strong
                                class="text-success"><?php echo $projects->project_start_date; ?></strong></td>
                    </tr>
                    <tr>
                        <td class="infoKey"><i class="fa fa-info-circle"></i><?php echo lang('label_status'); ?>:</td>
                        <td class="infoVal"><strong
                                class="text-success"><?php echo get_task_status_name($projects->project_status); ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="infoKey"><i class="fa fa-user"></i><?php echo lang("label_estimate_company"); ?>:
                        </td>
                        <td class="infoVal"><?php echo client_company($projects->client_id); ?></td>
                    </tr>

                    <tr>
                        <?php

                        $project_info = get_project_details($projects->project_id);
                        $pro_percentage = 0;
                        if ($project_info->project_use_worklog == 0) {
                            $pro_percentage = $project_info->project_progress;
                        } else {
                            $pro_percentage = calculate_project_percentage($projects->project_id);

                        }

                        ?>
                        <td class="infoKey"><i class="fa fa-money"></i><?php echo lang('label_progress'); ?>:</td>
                        <td class="infoVal more"><?php echo $pro_percentage; ?>%</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="modal-footer">
            <button class="btn btn-success btn-icon" name="submit" type="input">
                <i class="fa fa-check-square-o"></i>
                Save
            </button>
            <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                <i class="fa fa-times-circle-o"></i>
                Cancel
            </button>
        </div>
        </form>
    </div>
</div>

