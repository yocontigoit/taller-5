<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                    <i class="fa fa-times"></i>
                    </span>
                <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title"><?php echo lang('label_payment_details'); ?></h4>
        </div>
        <?php echo form_open('', array()); ?>


        <div class="modal-body">
            <div class="user-content">
                <table class="infoTable">
                    <tbody>
                    <tr>
                        <td class="infoKey"><i class="fa fa-user"></i> Invoice No:</td>
                        <td class="infoVal"><?php echo $payments->inv_no; ?></td>
                    </tr>
                    <tr>
                        <td class="infoKey"><i class="fa fa-calendar-o"></i><?php echo lang('label_date'); ?>:</td>
                        <td class="infoVal"><strong class="text-success"><?php echo $payments->created_at; ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="infoKey"><i class="fa fa-info-circle"></i><?php echo lang('label_payment_method'); ?>
                            :
                        </td>
                        <td class="infoVal"><strong
                                class="text-success"><?php echo payment_method($payments->payment_method); ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="infoKey"><i class="fa fa-user"></i><?php echo lang("label_estimate_company"); ?>:
                        </td>
                        <td class="infoVal"><?php echo client_company($payments->client_id); ?></td>
                    </tr>

                    <tr>
                        <td class="infoKey"><i class="fa fa-money"></i><?php echo lang('label_amount'); ?>:</td>
                        <td class="infoVal more"><?php echo format_currency($payments->amount); ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="modal-footer">
            <button class="btn btn-success btn-icon" name="submit" type="input">
                <i class="fa fa-check-square-o"></i>
                Save
            </button>
            <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                <i class="fa fa-times-circle-o"></i>
                Cancel
            </button>
        </div>
        </form>
    </div>
</div>

