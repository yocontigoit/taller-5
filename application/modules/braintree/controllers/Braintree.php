<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/06/03
 * Time: 12:51 PM
 */
class Braintree extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("braintree_m");
    }

    function index()
    {
        $this->session->set_flashdata('msg_status', 'error');
        $this->session->set_flashdata('message', lang('messages_braintree_cancelled'));
        redirect('client');
    }

    function add_payment($invoice_id = null)
    {

        $reference_no = get_invoice_reference_no($invoice_id);
        $invoice_currency = get_invoice_currency($invoice_id);
        $invoice_amount = invoice_balance($invoice_id);

        if ($invoice_amount <= 0) {
            $invoice_amount = 0.00;
        }

        $invoice_info = new stdClass();
        $invoice_info->item_name = $reference_no;
        $invoice_info->item_number = $invoice_id;
        $invoice_info->amount = $invoice_amount;
        $invoice_info->currency = $invoice_currency;
        $invoice_info->client_id = $this->CompanyClient_id;
        $invoice_info->token = $this->_gen_token();

        $this->data['invoice_info'] = $invoice_info;

        $this->data['title'] = 'Agregar Pago';

        $this->load->view('modal/add_payment', $this->data);
    }

    function _gen_token()
    {

        $braintree_env = ($this->config->item('braintree')->braintree_active == 1) ? 'production' : 'sandbox';
        require_once(APPPATH . 'libraries/braintree/lib/Braintree.php');
        try {
            Braintree_Configuration::environment($braintree_env);
            Braintree_Configuration::merchantId($this->config->item('braintree')->braintree_merchant_id);
            Braintree_Configuration::publicKey($this->config->item('braintree')->braintree_public_key);
            Braintree_Configuration::privateKey($this->config->item('braintree')->braintree_private_key);

            return Braintree_ClientToken::generate();
        } catch (Braintree_Exception_NotFound $e) {
            echo $e->getMessage();
        }
    }

    function process_payment()
    {
        $invoice_id = $this->input->post('item_number', true);
        $reference_no = get_invoice_reference_no($invoice_id);
        $invoice_currency = get_invoice_currency($invoice_id);

        $client_details = get_client_details($this->CompanyClient_id);

        require_once(APPPATH . 'libraries/braintree/lib/Braintree.php');


        $braintree_env = ($this->config->item('braintee')->braintree_active == 1) ? 'production' : 'sandbox';
        Braintree_Configuration::environment($braintree_env);
        Braintree_Configuration::merchantId($this->config->item('braintree')->braintree_merchant_id);
        Braintree_Configuration::publicKey($this->config->item('braintree')->braintree_public_key);
        Braintree_Configuration::privateKey($this->config->item('braintree')->braintree_private_key);

        $nonce = $this->input->post('payment_method_nonce');

        $result = Braintree_Transaction::sale([
            'amount' => $this->input->post('amount'),
            'orderId' => $reference_no,
            'paymentMethodNonce' => $nonce,
            'merchantAccountId' => $this->config->item('braintree')->braintree_merchant_id,
            'customer' => [
                'firstName' => '',
                'lastName' => '',
                'company' => $client_details->client_name,
                'phone' => $client_details->client_phone_no,
                'fax' => $client_details->client_fax,
                'website' => $client_details->client_website,
                'email' => $client_details->client_email
            ],
            'options' => ['submitForSettlement' => true]
        ]);

        if ($result->success) {
            $tr = $result->transaction;
            $transaction = array(
                'invoice_id' => $invoice_id,
                'trans_id' => $tr->id,
                'amount' => $tr->amount,
                'payment_method' => 4,
                'ip' => $this->input->ip_address(),
                'notes' => 'Paid by ' . $this->CompanyUser_FullName . ' via Braintree | Invoice Currency:' . $invoice_currency,
                'creator_email' => $this->CompanyClient_email,
                'created_by' => $client_details->client_name,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );

            if ($this->braintree_m->add_payment($transaction)) {

                $this->notifications->send_payment_notification($this->CompanyClient_id, $invoice_id, $tr->amount);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', 'Payment received and applied to Invoice ' . $reference_no);
                redirect('billing/invoice/' . $invoice_id);

            } else {
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message',
                    'Payment not recorded in the database. Please contact the system Admin.');
                redirect('billing/invoice/' . $invoice_id);
            }

        } else {
            if ($result->transaction) {
                print_r("Error processing transaction:");
                print_r("\n  code: " . $result->transaction->processorResponseCode);
                print_r("\n  text: " . $result->transaction->processorResponseText);
            } else {
                print_r("Validation errors: \n");
                print_r($result->errors->deepAll());
            }
        }

    }
}

