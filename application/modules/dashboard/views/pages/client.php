<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/01
 * Time: 02:26 PM
 */
?>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">
        <article class="col-sm-12">
            <!-- new widget -->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false">

                <header>
                    <span class="widget-icon"> <i class="fa fa-calendar"></i> </span>

                    <h2> Stats </h2>

                    <div class="widget-toolbar">
                        <!-- add: non-hidden - to disable auto hide -->

                    </div>
                </header>

                <!-- widget div-->
                <div>
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>
                                        <?php echo number_format($users); ?>
                                    </h3>

                                    <p>
                                        <?php echo lang('label_users'); ?>
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <a href="<?php echo base_url(); ?>user" class="small-box-footer">
                                    <?php echo lang('label_total_users'); ?> <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <!-- ./col -->

                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>
                                        <?php echo number_format($tickets); ?>
                                    </h3>

                                    <p>
                                        <?php echo lang('label_tickets'); ?>
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-users"></i>
                                </div>

                                <a href="<?php echo base_url(); ?>support" class="small-box-footer">
                                    <?php echo lang('label_total_clients'); ?> <i class="fa fa-arrow-circle-right"></i>
                                </a>

                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>
                                        <?php echo number_format($projects_count); ?>
                                    </h3>

                                    <p>
                                        <?php echo lang('label_total_projects'); ?>
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-tasks"></i>
                                </div>
                                <a href="<?php echo base_url(); ?>project" class="small-box-footer">
                                    <?php echo lang('label_total_projects'); ?><i class="fa fa-arrow-circle-right"></i>
                                </a>

                            </div>
                        </div>
                        <!-- ./col -->


                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>

                                        <?php echo number_format($open_tickets); ?>
                                    </h3>

                                    <p>
                                        <?php echo lang('label_open_tickets'); ?>
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-support"></i>
                                </div>
                                <a href="<?php echo base_url(); ?>support" class="small-box-footer">
                                    <?php echo lang('label_open_tickets'); ?>  <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <!-- ./col -->

                    </div>


                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

        </article>
    </div>


    <!-- end row -->

    <!-- Start another row -->
    <!-- Start another row -->
    <div class="row">
        <article class="col-sm-12">
            <!-- new widget -->
            <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments txt-color-white"></i> </span>

                    <h2><?php echo lang('label_sales'); ?></h2>

                    <div class="widget-toolbar" role="menu">
                        <!-- add: non-hidden - to disable auto hide -->

                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn dropdown-toggle btn-xs btn-info">
                                <?php echo date('Y'); ?> <i class="fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right js-status-update">
                                <li>
                                    <a href="<?php echo base_url('dashboard/filter/' . date('Y')); ?>"><?php echo date('Y'); ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('dashboard/filter/' . date('Y') - 1); ?>"><?php echo date('Y') - 1; ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('dashboard/filter/' . date('Y') - 2); ?>"><?php echo date('Y') - 2; ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('dashboard/filter/' . date('Y') - 3); ?>"><?php echo date('Y') - 3; ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('dashboard/filter/' . date('Y') - 4); ?>"><?php echo date('Y') - 4; ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('dashboard/filter/' . date('Y') - 5); ?>"><?php echo date('Y') - 5; ?></a>
                                </li>

                            </ul>
                        </div>
                    </div>

                </header>

                <!-- widget div-->
                <div>

                    <!-- end widget edit box -->

                    <!-- content goes here -->
                    <div class="row no-space">
                        <div class="col-lg-8 col-xs-8">
                            <div id="client_sales"></div>
                        </div>
                        <div class="col-lg-4 col-xs-4">
                            <!--<div class="panel panel-primary panel-square panel-no-border no-space">-->
                            <div class="panel panel-primary panel-no-border" style="height: 340px;overflow: hidden;">
                                <div class="panel-heading">
                                    <strong><?php echo lang('label_recent_payments'); ?></strong>
                                </div>
                                <ul class="list-group my-tasksticker widget-tasksticker"
                                    style="height: 200px;overflow: hidden;">

                                    <?php if (count($recent_payments)): foreach ($recent_payments as $invoice): ?>

                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-md-4 hidden-xs"><a class="badge badge-success" href="">Paid</a>
                                                </div>
                                                <!-- /.col-cs-4 -->
                                                <div class="col-md-4  hidden-xs "><?php echo $invoice->inv_no ?></div>
                                                <!-- /.col-cs-4 -->
                                                <div class="col-md-2  col-xs-4"><strong
                                                        class="text-danger"><?php echo format_currency($invoice->amount); ?></strong>
                                                </div>
                                                <!-- /.col-cs-4 -->
                                                <div class="col-md-2  hidden-xs"><i
                                                        class="fa fa-caret-up text-success"></i></div>
                                                <!-- /.col-cs-4 -->
                                            </div>
                                            <!-- /.row -->
                                        </li>
                                    <?php endforeach; endif; ?>


                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- end content -->
                    <div style="clear: both"></div>
                    <div class="row show-stat-microcharts">
                        <?php

                        $paid_percentage = percentage_paid($this->CompanyClient_id);
                        $unpaid_percentage = percentage_unpaid($this->CompanyClient_id);
                        $overdue_percentage = percentage_overdue($this->CompanyClient_id);
                        $partial_percentage = percentage_partial_paid($this->CompanyClient_id);


                        ?>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

                           	<span class="chart chart-widget-pie paid-chart-1"
                                  data-percent="<?php echo $paid_percentage; ?>">
											<span
                                                class="percent small-percentage"><?php echo $paid_percentage; ?></span>
							</span>
                            <span class="easy-pie-title"><?php echo lang('label_paid_amount'); ?><i
                                    class="fa fa-caret-up icon-color-bad"></i> </span>

                            <div
                                class="pull-right text-danger"><?php echo format_currency(stats_amount_paid($this->CompanyClient_id)); ?></div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            	<span class="chart chart-widget-pie unpaid-chart-2"
                                      data-percent="<?php echo $unpaid_percentage; ?>">
											<span
                                                class="percent small-percentage"><?php echo $unpaid_percentage; ?></span>
										</span>
                            <span class="easy-pie-title"><?php echo lang('label_unpaid_amount'); ?><i
                                    class="fa fa-caret-down icon-color-good"></i></span>

                            <div
                                class="pull-right text-danger"><?php echo format_currency(stats_amount_unpaid($this->CompanyClient_id)); ?></div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

                            	<span class="chart chart-widget-pie overdue-chart-3"
                                      data-percent="<?php echo $overdue_percentage; ?>">
											<span
                                                class="percent small-percentage"><?php echo $overdue_percentage; ?></span>
								</span>
                            <span class="easy-pie-title"><?php echo lang('label_overdue_amount'); ?><i
                                    class="fa fa-caret-up icon-color-good"></i></span>

                            <div
                                class="pull-right text-danger"><?php echo format_currency(stats_amount_overdue($this->CompanyClient_id)); ?></div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

                          	<span class="chart chart-widget-pie expense-chart-3"
                                  data-percent="<?php echo $partial_percentage; ?>">
											<span
                                                class="percent small-percentage"><?php echo $partial_percentage; ?></span>
								</span>
                            <span class="easy-pie-title"><?php echo lang('label_partially_paid'); ?><i
                                    class="fa fa-caret-down icon-color-good"></i></span>

                            <div
                                class="pull-right text-danger"><?php echo format_currency(stats_amount_partial_paid($this->CompanyClient_id)); ?></div>
                        </div>
                    </div>


                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->
        </article>
    </div>
    <!-- end row -->

    <!-- Start another row -->
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-6">
            <!-- new widget -->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-2" data-widget-editbutton="false"
                 data-widget-fullscreenbutton="false">


                <header>
                    <span class="widget-icon"> <i class="fa fa-bar-chart-o txt-color-white"></i> </span>

                    <h2><?php echo lang('label_recent_projects'); ?> </h2>

                </header>

                <!-- widget div-->
                <div>


                    <div class="widget-body widget-hide-overflow no-padding">
                        <!-- content goes here -->
                        <!-- recent-projects -->
                        <div class="box box-primary">

                            <div class="box-body">
                                <table class="table table-striped table-th-block table-success">
                                    <thead>
                                    <tr>
                                        <th><?php echo lang('label_task_progress'); ?></th>
                                        <th><?php echo lang('label_project_name'); ?> </th>
                                        <th class="hidden-xs"><?php echo lang('label_due_date'); ?> </th>
                                        <th><?php echo lang('label_options'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    if (!empty($projects)) {
                                        foreach ($projects as $project) { ?>
                                            <tr>
                                                <?php

                                                $pro_percentage = 0;
                                                if ($project->project_use_worklog == 0) {
                                                    $pro_percentage = $project->project_progress;
                                                } else {
                                                    $pro_percentage = calculate_project_percentage($project->project_id);

                                                }

                                                ?>
                                                <td>
                                                    <?php if ($pro_percentage >= 100) {
                                                        $bg = 'success';
                                                    } else {
                                                        $bg = 'danger';
                                                    } ?>
                                                    <div class="progress progress-xs progress-striped active">
                                                        <div class="progress-bar progress-bar-<?php echo $bg; ?>"
                                                             data-toggle="tooltip"
                                                             data-original-title="<?php echo $pro_percentage; ?>%"
                                                             style="width: <?php echo $pro_percentage; ?>%">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><?php echo $project->project_code; ?> </td>
                                                <td class="hidden-xs"><?php echo $project->project_due_date; ?></td>
                                                <td>
                                                    <div class="btn-group-action">
                                                        <div class="btn-group">
                                                            <a class=" btn btn-info"
                                                               href="<?php echo base_url('project/overview/preview/' . $project->project_id); ?>"
                                                               title=""><i class="fa fa-pencil"></i> view</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php }
                                    } else { ?>
                                        <tr>
                                            <td>Nothing to display here</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    <?php } ?>


                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body-->
                        </div>
                        <!-- /.box -->


                        <!-- end content -->
                    </div>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

        </article>

        <article class="col-sm-12 col-md-12 col-lg-6">

            <!-- new widget -->
            <div class="jarviswidget" id="wid-id-4" data-widget-colorbutton="false" data-widget-editbutton="false">


                <header>
                    <span class="widget-icon"> <i class="fa fa-map-marker"></i> </span>

                    <h2><?php echo lang('label_recent_activities'); ?></h2>

                </header>

                <!-- widget div-->
                <div>
                    <div class="widget-body widget-hide-overflow no-padding">
                        <!-- content goes here -->
                        <div class="box box-primary">
                            <div class="box-body timeline-scroll">
                                <?php if (count($activities)) : ?>
                                    <ul class="timeline-xs margin-top-15 margin-bottom-15">

                                        <?php foreach ($activities as $activity): ?>
                                            <li class="timeline-item <?php echo $activity->activity_status; //success/danger/info/warning?>">
                                                <div class="margin-left-15">
                                                    <div
                                                        class="text-muted text-small"> <?php echo ago(strtotime($activity->activity_date)) . " " . lang('ago'); ?> </div>
                                                    <p>

                                                    <div class="media-left">
                                                        <span class="activity-avatar avatar-xs">
                                                           <?php $user_details = users_details($activity->activity_user,
                                                               $activity->activity_user_type); ?>
                                                            <img
                                                                src="<?php echo base_url('files/profile_images/' . $user_details->avatar); ?>"
                                                                alt="...">
                                                        </span>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="media-heading">
                                                            <a href=""
                                                               class="text-info strong"><?php echo activity_user($activity->activity_user_type,
                                                                    $activity->activity_user); //Steven?></a>
                                                            <small><span
                                                                    class="text-off"><?php echo $activity->activity_date; ?></span>
                                                            </small>
                                                        </div>
                                                        <p>
                                                            <span
                                                                class="label label-<?php echo $activity->activity_status; ?>"></span>
                                                        </p>

                                                        <p>  <?php echo $activity->activity_event; // has completed his account. ?></p>
                                                    </div>

                                                    </p>
                                                </div>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php else: ?>
                                    <p>No recent activities</p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <!-- end content -->

                </div>

            </div>
            <!-- end widget div -->

            <!-- end widget -->


        </article>

    </div>
    <!--new container -->
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-6">
            <!-- new widget -->
            <div class="jarviswidget jarviswidget-color-blue" id="wid-id-6" data-widget-editbutton="false"
                 data-widget-colorbutton="false">


                <header>
                    <span class="widget-icon"> <i class="fa fa-bar-chart"></i> </span>

                    <h2>Invoice Overview</h2>
                    <!-- <div class="widget-toolbar">
                    add: non-hidden - to disable auto hide

                    </div>-->
                </header>

                <!-- widget div-->
                <div>


                    <div class="widget-body no-padding smart-form">
                        <!-- content goes here -->


                        <div id="panel-invoice-overview" class="panel panel-default overview">

                            <div class="panel-heading">
                                <strong>Status</strong>
                                <span class="pull-right text-muted">This Year</span>
                            </div>
                            <div class="row billing_top_row">
                                <div class="billing_settings">
                                    <ul class="left_box">
                                        <li class="revenue_today">
                                            <div>
                                                <em><?php echo lang('label_sales_this_month'); ?></em>
                                                <span
                                                    class="more"><?php echo format_currency(client_invoice_total($this->CompanyClient_id,
                                                        'month')); ?></span>
                                            </div>
                                        </li>
                                        <li class="revenue_month">
                                            <div>
                                                <em><?php echo lang('label_sales_this_year'); ?></em>
                                                <span
                                                    class="more"><?php echo format_currency(client_invoice_total($this->CompanyClient_id,
                                                        'year')); ?></span>
                                            </div>
                                        </li>
                                        <li class="revenue_year">
                                            <div>
                                                <em><?php echo lang('label_payments_this_year'); ?></em>
                                                <span
                                                    class="more"><?php echo format_currency(client_payments($this->CompanyClient_id,
                                                        'year')); ?></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>


                        </div>
                        <div style="clear: both"></div>
                        <div class="row">
                            <div class="col-lg-12 col-md-6">
                                <div style="margin: 0 5% 0 5%;">
                                    <div class="row">

                                        <div class="panel panel-danger panel-heading">
                                            <a href="" class="text-danger"><i
                                                    class="fa fa-external-link"></i><?php echo lang('label_overdue_invoices'); ?>
                                            </a>
                                <span class="pull-right text-danger">
                                   <?php echo format_currency(client_amount_overdue($this->CompanyClient_id)); ?>
                                </span>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- end content -->
                    </div>

                </div>

                <!-- end widget div -->
            </div>
            <!-- end widget -->

        </article>
        <article class="col-sm-12 col-md-12 col-lg-6">
            <!-- new widget -->
            <div class="jarviswidget jarviswidget-color-blue" id="wid-id-5" data-widget-editbutton="false"
                 data-widget-colorbutton="false">


                <header>
                    <span class="widget-icon"> <i class="fa fa-check txt-color-white"></i> </span>

                    <h2><?php echo lang('label_stats'); ?></h2>

                </header>

                <!-- widget div-->
                <div>


                    <div class="widget-body no-padding smart-form">
                        <!-- content goes here -->
                        <div class="row">
                            <div class="col-md-12">

                                <div class="col-md-6 text-center">
                                    <h4>Tickets - Stats </h4>

                                    <div id="client-tickets-stat" style="height: 240px;"></div>
                                </div>
                                <div class="col-md-6 text-center">
                                    <h4>Projects - Stats </h4>
                                    <?php $percentage = client_project_complete_perc($this->CompanyClient_id); ?>
                                    <div class="chart client-pro-stat text-danger"
                                         data-percent="<?php echo $percentage; ?>" data-line-width="15"
                                         data-rotate="270" data-scale-Color="false" data-size="180" data-animate="2000">
                                        <span class="h2 step font-bold"><?php echo $percentage; ?></span>%
                                        <div
                                            class="easypie-text text-muted"><?php echo lang('label_closed_projects'); ?></div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

        </article>
    </div>
    <!--end Container -->
</section>
<!-- end widget grid -->