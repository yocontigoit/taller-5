<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/01
 * Time: 02:26 PM
 */
?>


    <!-- row -->

    <!-- end row -->
    <!-- Start another row -->

    <!-- end row -->

    <!-- Start another row -->

    <div class="row">
      <article class="col-sm-6 col-md-6 col-lg-6" style="width:49%">
          <!-- new widget -->
          <div class="jarviswidget jarviswidget-color-blue" id="wid-id-5" data-widget-editbutton="false"
               data-widget-colorbutton="false">


              <header>
                  <span class="widget-icon"> <i class="fa fa-check txt-color-white"></i> </span>

                  <h2>TABLA DE PROYECTOS</h2>
                  <!-- <div class="widget-toolbar">
                  add: non-hidden - to disable auto hide

                  </div>-->
              </header>

              <!-- widget div-->
              <div>


                  <div class="widget-body no-padding smart-form">
                      <!-- content goes here -->
                      <?php if (count($my_overdue_tasks)): ?>
                          <?php $count = count($my_overdue_tasks); ?>
                          <h5 class="todo-group-title"><i class="fa fa-warning"></i> Tareas Pendientes (
                              <small class="num-of-tasks"><?php echo $count; ?></small>
                              )
                          </h5>
                      <?php else: ?>
                          <h5 class="todo-group-title"><i class="fa fa-warning"></i> Tareas Pendientes (
                              <small class="num-of-tasks">0</small>
                              )
                          </h5>
                      <?php endif; ?>
                      <ul id="sortable" class="todo">

                          <?php if (count($my_overdue_tasks)): foreach ($my_overdue_tasks as $my_task): ?>
                              <li>
                                             <span class="handle"> <label class="checkbox">
                                                     <?php
                                                     $data = array(
                                                         'name' => 'checkbox-inline[]',
                                                         'id' => 'overdue_' . $my_task->task_id,
                                                         'value' => $my_task->task_id,
                                                         'checked' => false,
                                                     );

                                                     echo form_checkbox($data);
                                                     ?>
                                                     <!-- <input type="checkbox" name="checkbox-inline"> -->
                                                     <i></i> </label> </span>

                                  <p>
                                      <strong><?php echo 'Proyecto: ' .$my_task->project_code . ' (' . $my_task->task_name. ')' ?></strong>
                                      - <?php echo substr($my_task->task_description, 0, 40) . "     "; ?>  <a
                                          href="<?php echo base_url('project/tasks/details/' . $my_task->project_id . '/' . $my_task->task_id . '/tasks'); ?>"
                                          class="font-xs">      Ver Tarea</a> <span class="text-muted"> </span>
                                      <span
                                          class="text-warning"><?php echo date_from_timestamp($my_task->task_end / 1000) . ', Asignado por :'; ?>
                                          <label class="badge badge-info"><?php echo users_details($my_task->user_id,
                                                  1)->full_name; ?></label></span>
                                  </p>
                              </li>
                          <?php endforeach; ?>
                          <?php else: ?>
                              <li>
                                  <span class="handle">  </span>

                                  <p>
                                      No tienes tareas Atrasadas...

                                  </p>
                              </li>
                          <?php endif; ?>


                      </ul>
                      <!-- Outstanding tasks -->
                      <?php if (count($my_outstanding_tasks)): ?>
                          <?php $count = count($my_outstanding_tasks); ?>
                          <h5 class="todo-group-title"><i class="fa fa-exclamation"></i> Tareas Importantes (
                              <small class="num-of-tasks"><?php echo $count; ?></small>
                              )
                          </h5>
                      <?php else: ?>
                          <h5 class="todo-group-title"><i class="fa fa-exclamation"></i> Tareas Importantes (
                              <small class="num-of-tasks">0</small>
                              )
                          </h5>
                      <?php endif; ?>


                      <ul id="sortable2" class="todo">


                          <?php if (count($my_outstanding_tasks)): foreach ($my_outstanding_tasks as $out_tasks): ?>
                              <li>
                                              <span class="handle"> <label class="checkbox">
                                                      <?php
                                                      $data = array(
                                                          'name' => 'checkbox-inline[]',
                                                          'id' => 'ouststandin_' . $out_tasks->task_id,
                                                          'value' => $out_tasks->task_id,
                                                          'checked' => false,
                                                      );

                                                      echo form_checkbox($data);
                                                      ?>
                                                      <i></i> </label> </span>

                                  <p>
                                      <strong><?php echo $out_tasks->project_code . ' #' . $out_tasks->task_name; ?></strong>
                                      - <?php echo substr($out_tasks->task_description, 0, 40) . "....."; ?>  [<a
                                          href="<?php echo base_url('project/tasks/details/' . $out_tasks->project_id . '/' . $out_tasks->task_id . '/tasks'); ?>"
                                          class="font-xs">Mas detalles</a>] <span class="text-muted"> </span>
                                      <span
                                          class="text-success"><?php echo date_from_timestamp($out_tasks->task_end / 1000) . ',Assigned BY :'; ?>
                                          <label
                                              class="badge badge-info"><?php echo users_details($out_tasks->user_id,
                                                  1)->full_name; ?></label></span>
                                  </p>
                              </li>
                          <?php endforeach; ?>
                          <?php else: ?>
                              <li>
                                  <span class="handle">  </span>

                                  <p>
                                      No tienes tareas Importantes

                                  </p>
                              </li>
                          <?php endif; ?>
                      </ul>
                      <!-- end content -->


                      <?php $completedtasks = null;
                      if (count($completedtasks)): ?>
                          <?php $count = count($completedtasks); ?>
                          <h5 class="todo-group-title"><i class="fa fa-exclamation"></i> Tareas Completadas (
                              <small class="num-of-tasks"><?php echo $count; ?></small>
                              )
                          </h5>
                      <?php else: ?>
                          <h5 class="todo-group-title"><i class="fa fa-exclamation"></i> Tareas Completadas (
                              <small class="num-of-tasks">0</small>
                              )
                          </h5>
                      <?php endif; ?>
                      <!-- <h5 class="todo-group-title"><i class="fa fa-check"></i> Completed Tasks (<small class="num-of-tasks">0</small>)</h5> -->
                      <ul id="sortable3" class="todo">

                      </ul>
                  </div>

              </div>
              <!-- end widget div -->
          </div>
          <!-- end widget -->
      </article>
      <!-- //////////////////////////////// start pendientes \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ -->
      <?php $tex = ' '; ?>
        <div class="row">
          <article class="col-sm-6 col-md-6 col-lg-6" style="width:49%">
            <div class="jarviswidget jarviswidget-color-blue" id="pendientes" data-widget-editbutton="false" data-widget-colorbutton="false">
              <header>
                    <span class="widget-icon"> <i class="fa fa-bar-chart-o txt-color-white"></i> </span>

                    <h2>Pendientes Personales</h2>

                </header>

              <div>
                <div class="widget-body no-padding smart-form">
                  <?php if (count($my_overdue_pending)): ?>
                      <?php $count = count($my_overdue_pending); ?>
                      <h5 class="pendiente-group-title"><i class="fa fa-warning"></i> Pendientes (
                          <small class="num-of-tasks"><?php echo $count; ?></small>
                          )
                      </h5>
                  <?php else: ?>
                      <h5 class="pendiente-group-title"><i class="fa fa-warning"></i> Pendientes (
                          <small class="num-of-tasks">0</small>
                          )
                      </h5>
                  <?php endif; ?>
                  <ul id="sortable_pendient" class="pendiente">
                    <?php if (count($my_overdue_pending)): foreach ($my_overdue_pending as $my_pending): ?>
                    <li>
                      <span class="handle">
                        <label class="checkbox">
                          <?php
                            $data = array(
                              'name' => 'checkbox-inline[]',
                              'id' => 'overdue_' . $my_pending->com_id,
                              'value' => $my_pending->com_id,
                              'checked' => false,
                            );
                            echo form_checkbox($data);
                          ?>
                          <!-- <input type="checkbox" name="checkbox-inline"> -->
                          <i></i>
                        </label>
                      </span>
                     <p>
                         <strong><?php echo 'Proyecto: ' .$my_pending->com_obra ?></strong> (<?php echo $my_pending->com_description ?>)
                         <!-- - <?php echo substr($my_task->task_description, 0, 40) . "     "; ?>  <a
                             href="<?php echo base_url('project/tasks/details/' . $my_task->project_id . '/' . $my_task->task_id . '/tasks'); ?>"
                             class="font-xs">      Ver Tarea</a> <span class="text-muted"> </span> -->
                         <span class="text-warning"><?php echo $my_pending->com_date . ','; ?>
                             <!-- <label class="badgebadge-info"><?php echo users_details($my_task->user_id,
                                     1)->full_name; ?></label>  --></span>
                                     <?php
                                        if ($my_pending->com_color == 'danger') {
                                          $tex = 'Urgente';
                                        }
                                        if ($my_pending->com_color == 'warning') {
                                          $tex = 'Intermedio';
                                        }
                                        if ($my_pending->com_color == 'info') {
                                          $tex = 'Normal';
                                        }
                                        if ($my_pending->com_color == 'success') {
                                          $tex = 'Tarea terminada';
                                        }
                                     ?>
                          Nivel de urgencia: <label class="label label-<?php echo $my_pending->com_color; ?>"><?php echo $tex ?></label>

                     </p>
                    </li>
                  <?php endforeach; endif; ?>
                  </ul>
                  <?php $completedtasks = null;
                  if (count($completedtasks)): ?>
                      <?php $count = count($completedtasks); ?>
                      <h5 class="pendiente-group-title"><i class="fa fa-exclamation"></i> Pendientes Completados (
                          <small class="num-of-tasks"><?php echo $count; ?></small>
                          )
                      </h5>
                  <?php else: ?>
                      <h5 class="pendiente-group-title"><i class="fa fa-exclamation"></i> Pendientes Completados (
                          <small class="num-of-tasks">0</small>
                          )
                      </h5>
                  <?php endif; ?>
                  <!-- <h5 class="todo-group-title"><i class="fa fa-check"></i> Completed Tasks (<small class="num-of-tasks">0</small>)</h5> -->
                  <ul id="sortable_pendiente" class="pendiente">

                  </ul>
                  <center><a href="https://proyectosinternos.com/Taller5/support/add_pending" style="color:#fff" class="btn btn-info"><i class="fa fa-plus"></i> Nuevo</a></center>
                </div>
              </div>
            </div>
          </article>
        </div>
      <!-- end pendientes -->
      <!-- widget grid -->

 <!--
        <article class="col-sm-12 col-md-12 col-lg-12">
            <!-- new widget
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-2" data-widget-editbutton="false"
                 data-widget-fullscreenbutton="false">


                <header>
                    <span class="widget-icon"> <i class="fa fa-bar-chart-o txt-color-white"></i> </span>

                    <h2>Proyectos Activos</h2>

                </header>

                <!--
                <div>


                    <div class="widget-body widget-hide-overflow no-padding">
                        <!-- content goes here -->
                        <!-- recent-projects
                        <div class="box box-primary">

                            <div class="the-box">
   <fieldset>
        <legend><i class="fa fa-calendar"></i>Calendar
            <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) { ?>
                <span class="pull-right">
               <a data-toggle="zestModal" class="text-info" href="<?php echo base_url('calendar/add_event'); ?>">
                   <i class="fa fa-plus"></i>
                   Add Event
               </a>

                     <a data-toggle="zestModal" class="text-info" href="<?php echo base_url('calendar/settings'); ?>">
                         <i class="fa fa-plus"></i>
                         Settings
                     </a>
             </span>
            <?php } ?>
        </legend>
                <div class="row">
                    <div class="col-md-12 col-sm-6">
                        <div class="calendar" id="module_calendar"></div>
                    </div>
                </div>
            </fieldset>
        </div>
                            <!-- /.box-body
                        </div>
                        <!-- /.box -->


                        <!-- end content
                    </div>

                </div>
                <!-- end widget div
            </div>
            <!-- end widget

        </article> -->
<section id="widget-grid" class="">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <!-- new widget -->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-2" data-widget-editbutton="false"
                 data-widget-fullscreenbutton="false">


                <header>
                    <span class="widget-icon"> <i class="fa fa-bar-chart-o txt-color-white"></i> </span>

                    <h2>Proyectos Activos</h2>

                </header>

                <!-- widget div-->
                <div>


                    <div class="widget-body widget-hide-overflow no-padding">
                        <!-- content goes here -->
                        <!-- recent-projects -->
                        <div class="box box-primary">

                            <div class="box-body">
                                <table class="table table-striped table-th-block table-success">
                                    <thead>
                                    <tr>

                                        <th><?php echo lang('label_project_name'); ?> </th>
                                        <th> Tipo de Proyecto </th>
                                        <th class="hidden-xs">Tareas de la semana </th>
                                        <th>A cargo</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    if (!empty($projects)) {
                                        foreach ($projects as $project) { ?>
                                            <tr>

                                                <td><a style="margin-right:10px;"
                                                   href="<?php echo base_url('project/overview/preview/' . $project->project_id); ?>"
                                                   title=""><?php echo $project->project_title; ?></a> </td>
                                                <td class="hidden-xs"><?php echo $project->project_type; ?></td>
                                                <td> <?php $counter=1;
                                                if (count($tasks)): foreach ($tasks as $task): if ($task->project_id == $project->project_id):
                                      if ($task->task_progress == 0){  $level =' style="color:#fff;background:#ff0000;padding:3px;" ';  }
                                      if ($task->task_progress >= 100){$level=' style="color:#fff;background:#289102;padding:3px;" ';  }
                                      if ($task->task_progress >= 1 && $task->task_progress < 100){  $level=' style="color:#fff;background:#2a8aba;padding:3px;" ';}
                                                    echo '<center><a'.$level.'href="https://proyectosinternos.com/Taller5/project/tasks/details/'.$task->project_id . '/' . $task->task_id . '/tasks">' . $counter.'. '.$task->task_name. '</a></center></br>';
                                                    $counter++; endif; endforeach; endif; ?>
                                                </td>
<td>
  <?php if (count($assigned_team)): foreach ($assigned_team as $team): if ($team->project_id == $project->project_id):
      $user_details = users_details($team->user_id, 1);
      ?>


                                  <?php echo '<a href="https://proyectosinternos.com/Taller5/user/edit/'.($team->user_id * 99).'">'. $user_details->full_name.'</a></br>'; ?>

                                                  <!-- /.the-box .no-border -->
          <!-- BEGIN USER CARD LONG -->
    <!-- /.col-sm-6 -->
  <?php endif; endforeach; endif; ?>
</td>










                                            </tr>
                                        <?php }
                                    } else {  } ?>


                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body-->
                        </div>
                        <!-- /.box -->


                        <!-- end content -->
                    </div>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

        </article>
    </div>

    <!--new container -->
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <!-- new widget -->
            <div class="jarviswidget jarviswidget-color-blue" id="wid-id-6" data-widget-editbutton="false"
                 data-widget-colorbutton="false">


                <header>
                    <span class="widget-icon"> <i class="fa fa-bar-chart"></i> </span>

                    <h2><?php echo lang('label_invoice_overview'); ?></h2>
                    <!-- <div class="widget-toolbar">
                    add: non-hidden - to disable auto hide

                    </div>-->
                </header>

                <!-- widget div-->
                <div>
                    <div class="widget-body no-padding smart-form">
                        <!-- content goes here -->

                        <div id="panel-invoice-overview" class="panel panel-default overview">

                            <div class="panel-heading">
                                <strong><?php echo lang('label_status'); ?></strong>
                                <span class="pull-right text-muted">This Year</span>
                            </div>
                            <div class="row billing_top_row">
                                <div class="billing_settings">
                                    <ul class="left_box">
                                        <li class="revenue_today">
                                            <div>
                                                <em>Utilidades Mensuales</em>
                                                <span class="more"><?php echo format_currency(client_invoice_total(null,
                                                        'month')); ?></span>
                                            </div>
                                        </li>
                                        <li class="revenue_month">
                                            <div>
                                                <em>Utilidades Anuales</em>
                                                <span class="more"><?php echo format_currency(client_invoice_total(null,
                                                        'year')); ?></span>
                                            </div>
                                        </li>
                                        <li class="revenue_year">
                                            <div>
                                                <em>Gastos Anuales</em>
                                                <span class="more"><?php echo format_currency(client_payments(null,
                                                        'year')); ?></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>


                        </div>
                        <div style="clear: both"></div>
                        <div class="row">
                            <div class="col-lg-12 col-md-6">
                                <div style="margin: 0 5% 0 5%;">
                                    <div class="row">

                                        <div class="panel panel-info panel-heading">
                                            <a href="" class="text-info"><i
                                                    class="fa fa-external-link"></i><?php echo lang('label_overdue_invoices'); ?>
                                            </a>
                                <span class="pull-right text-info">
                                   <?php echo format_currency(stats_amount_overdue()); ?>
                                </span>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end content -->
                    </div>

                </div>

                <!-- end widget div -->
            </div>
            <!-- end widget -->

        </article>


    </div>
    <div class="row">
        <article class="col-sm-12">
            <!-- new widget -->
            <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-money txt-color-white"></i> </span>


                    <ul class="nav nav-tabs  nav-settings pull-right in" id="bs-example-navbar-collapse-1">
                        <li class="active">
                            <a data-toggle="tab" href="#s1"><i class="fa fa-bar-chart"></i> <span
                                    class="hidden-mobile hidden-tablet">Estadísticas de Ingresos</span></a>
                        </li>

                        <li>
                            <a data-toggle="tab" href="#s2"><i class="fa fa-money"></i> <span
                                    class="hidden-mobile hidden-tablet">Ingresos y Gastos</span></a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#s3"><i class="fa fa-plane"></i> <span
                                    class="hidden-mobile hidden-tablet">Estadísticas de Proyecto</span></a>
                        </li>

                    </ul>
                    <div class="widget-toolbar" role="menu">
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn dropdown-toggle btn-xs btn-info">
                                <?php echo date('Y'); ?> <i class="fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right js-status-update">
                                <li>
                                    <a href="<?php echo base_url('dashboard/filter/' . date('Y')); ?>"><?php echo date('Y'); ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('dashboard/filter/' . date('Y') - 1); ?>"><?php echo date('Y') - 1; ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('dashboard/filter/' . date('Y') - 2); ?>"><?php echo date('Y') - 2; ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('dashboard/filter/' . date('Y') - 3); ?>"><?php echo date('Y') - 3; ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('dashboard/filter/' . date('Y') - 4); ?>"><?php echo date('Y') - 4; ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('dashboard/filter/' . date('Y') - 5); ?>"><?php echo date('Y') - 5; ?></a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </header>
                <!-- widget div-->
                <div>
                    <!-- widget div-->
                    <div>

                        <!-- end widget edit box -->
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade active in" id="s1">
                                <!-- content goes here -->

                                <div class="row no-space">

                                    <div class="col-lg-8 col-md-8 col-xs-8">
                                        <div class="the-box no-border contentarea">
                                            <div id="client_sales"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-xs-4">
                                        <!--<div class="panel panel-primary panel-square panel-no-border no-space">-->
                                        <div class="panel panel-primary panel-no-border"
                                             style="height: 340px;overflow: hidden;">
                                            <div class="panel-heading">
                                                <strong><?php echo lang('label_recent_payments'); ?></strong>
                                            </div>
                                            <ul class="list-group my-tasksticker widget-tasksticker"
                                                style="height: 200px;overflow: hidden;">

                                                <?php if (count($recent_payments)): foreach ($recent_payments as $invoice): ?>

                                                    <li class="list-group-item">
                                                        <div class="row">
                                                            <div class="col-md-4 hidden-xs"><a
                                                                    class="badge badge-success" href="/Taller5/billing/invoice/<?php echo $invoice->id ?>">Pagado</a></div>
                                                            <!-- /.col-cs-4 -->
                                                            <div
                                                                class="col-md-4  hidden-xs "><a
                                                                         href="/Taller5/billing/invoice/<?php echo $invoice->id ?>">PAGO-<?php echo $invoice->id ?></a></div>
                                                            <!-- /.col-cs-4 -->
                                                            <div class="col-md-2  col-xs-4"><strong
                                                                    class="text-info"><?php echo format_currency($invoice->amount); ?></strong>
                                                            </div>
                                                            <!-- /.col-cs-4 -->
                                                            <div class="col-md-2  hidden-xs"><i
                                                                    class="fa fa-caret-up text-success"></i></div>
                                                            <!-- /.col-cs-4 -->
                                                        </div>
                                                        <!-- /.row -->
                                                    </li>
                                                <?php endforeach; endif; ?>


                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- end content -->
                                <div style="clear: both"></div>
                                <div class="row show-stat-microcharts">
                                    <?php

                                    $paid_percentage = percentage_paid(null);
                                    $unpaid_percentage = percentage_unpaid(null);
                                    $overdue_percentage = percentage_overdue(null);
                                    $partial_percentage = percentage_partial_paid(null);


                                    ?>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

                            <span class="chart chart-widget-pie paid-chart-1"
                                  data-percent="<?php echo $paid_percentage; ?>">
                      <span
                                                class="percent small-percentage"><?php echo $paid_percentage; ?></span>
              </span>
                                        <span class="easy-pie-title"><?php echo lang('label_paid_amount'); ?><i
                                                class="fa fa-caret-up icon-color-bad"></i> </span>

                                        <div
                                            class="pull-right text-info"><?php echo format_currency(stats_amount_paid()); ?></div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                              <span class="chart chart-widget-pie unpaid-chart-2"
                                      data-percent="<?php echo $unpaid_percentage; ?>">
                      <span
                                                class="percent small-percentage"><?php echo $unpaid_percentage; ?></span>
                    </span>
                                        <span class="easy-pie-title"><?php echo lang('label_unpaid_amount'); ?><i
                                                class="fa fa-caret-down icon-color-good"></i></span>

                                        <div
                                            class="pull-right text-info"><?php echo format_currency(stats_amount_unpaid()); ?></div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

                              <span class="chart chart-widget-pie overdue-chart-3"
                                      data-percent="<?php echo $overdue_percentage; ?>">
                      <span
                                                class="percent small-percentage"><?php echo $overdue_percentage; ?></span>
                </span>
                                        <span class="easy-pie-title"><?php echo lang('label_overdue_amount'); ?><i
                                                class="fa fa-caret-up icon-color-good"></i></span>

                                        <div
                                            class="pull-right text-info"><?php echo format_currency(stats_amount_overdue()); ?></div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

                            <span class="chart chart-widget-pie expense-chart-3"
                                  data-percent="<?php echo $partial_percentage; ?>">
                      <span
                                                class="percent small-percentage"><?php echo $partial_percentage; ?></span>
                </span>
                                        <span class="easy-pie-title"><?php echo lang('label_partially_paid'); ?><i
                                                class="fa fa-caret-down icon-color-good"></i></span>

                                        <div
                                            class="pull-right text-info"><?php echo format_currency(stats_amount_partial_paid()); ?></div>
                                    </div>
                                </div>

                            </div>

                            <div class="tab-pane fade" id="s2">
                                <div class="widget-body-toolbar bg-color-white smart-form" id="rev-toggles">

                                    <div class="inline-group">

                                        <label for="gra-0" class="checkbox" style="margin-top: 0px;">
                                            <input type="checkbox" name="gra-0" id="gra-0" checked="checked">
                                            <i></i> <?php echo lang('label_income'); ?> </label>
                                        <label for="gra-1" class="checkbox" style="margin-top: 0px;">
                                            <input type="checkbox" name="gra-1" id="gra-1" checked="checked">
                                            <i></i> <?php echo lang('label_expenses'); ?></label>

                                    </div>


                                </div>

                                <div class="padding-10">
                                    <div id="flotcontainer" class="chart-large has-legend-unique"></div>
                                </div>

                            </div>

                            <div class="tab-pane fade" id="s3">


                                <!-- content goes here -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3 text-center">
                                            <h4>Proceso de Proyectos </h4>
                                            <?php $percentage = client_project_complete_perc(null); ?>
                                            <div class="chart admin-pro-stat text-info"
                                                 data-percent="<?php echo $percentage; ?>" data-line-width="15"
                                                 data-rotate="270" data-scale-Color="false" data-size="180"
                                                 data-animate="2000">
                                                <span class="h2 step font-bold"><?php echo $percentage; ?></span>%
                                                <div
                                                    class="easypie-text text-muted">Completado de proyectos</div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 text-center">
                                            <h4>Gastos vs Estimaciones</h4>

                                            <div id="salestimate_stats"
                                                 style="min-width:423px; max-width: 800px;height: 290px; margin: 0 auto"></div>
                                            <!--  style="min-width: 240px; height: 240px; max-width: 600px; margin: 0 auto" -->

                                        </div>
                                        <!-- /.col-sm-6 -->

                                        <div class="col-md-3 text-center">
                                            <h4>Transacciones</h4>

                                            <div id="admin-rev-stat" style="height: 240px;"></div>

                                        </div>
                                        <!-- /.col-sm-6 -->


                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>
                    <!-- end widget div -->
                </div>
            </div>
            <!-- end widget -->
        </article>
    </div>
    <div class="row">
      <article class="class="col-sm-12"">
          <!-- new widget -->
          <div class="jarviswidget jarviswidget-color-blueDark" id="comunicados" data-widget-editbutton="false"
               data-widget-fullscreenbutton="false">


              <header>
                  <span class="widget-icon"> <i class="fa fa-comments txt-color-white"></i> </span>

                  <center><h2>Comunicados Generales</h2></center>

              </header>

              <!-- widget div-->
              <div>


                  <div class="widget-body widget-hide-overflow no-padding">
                      <!-- content goes here -->
                      <!-- recent-projects -->
                      <div class="box box-primary">
                          <div class="box-body">
                              <table class="table table-th-block table-success" style="border-radius:5px; border-bottom: 3px solid #fff">
                                  <thead>
                                  <tr>
                                      <th style="text-align:center;border-radius:5px; border-bottom: 3px solid #fff" class="hidden-xs" width="25%">Fecha</th>
                                      <th style="text-align:center;border-radius:5px; border-bottom: 3px solid #fff" class="hidden-xs" width="30%">Obra</th>

                                      <th style="text-align:center;border-radius:5px; border-bottom: 3px solid #fff" width="50%">Comunicado</th>
                                      <th style="text-align:center;border-radius:5px; border-bottom: 3px solid #fff" class="hidden-xs" width="25%">Enviado por</th>
									  <th style="text-align:center;border-radius:5px; border-bottom: 3px solid #fff" class="hidden-xs" width="25%">Enviado para</th>

                                  </tr>
                                  </thead>
                                  <tbody>

                                  <?php
                                  if (!empty($comunicados)) {
                                      foreach ($comunicados as $comunicado) { ?>
                                          <tr>
                                              <td style="border-radius:5px; border-bottom: 3px solid #fff;text-align:center;color:#<?php echo $comunicado->com_colortext; ?>;background:#<?php echo $comunicado->com_color; ?>" class="hidden-xs"><?php echo $comunicado->com_date; ?> </td>
                                              <td style="border-radius:5px; border-bottom: 3px solid #fff;text-align:center;color:#<?php echo $comunicado->com_colortext; ?>;background:#<?php echo $comunicado->com_color; ?>" class="hidden-xs"><?php echo $comunicado->com_obra; ?> </td>

                                              <td style="border-radius:5px; border-bottom: 3px solid #fff;text-align:center;color:#<?php echo $comunicado->com_colortext; ?>;background:#<?php echo $comunicado->com_color; ?>"><?php echo $comunicado->com_description; ?></td>
                                              <td style="border-radius:5px; border-bottom: 3px solid #fff;text-align:center;color:#<?php echo $comunicado->com_colortext; ?>;background:#<?php echo $comunicado->com_color; ?>" class="hidden-xs"><?php echo $comunicado->com_user; ?></td>
											  <td style="border-radius:5px; border-bottom: 3px solid #fff;text-align:center;color:#<?php echo $comunicado->com_colortext; ?>;background:#<?php echo $comunicado->com_color; ?>" class="hidden-xs"><?php echo $comunicado->com_name; ?></td>
									  <?php

										}
								  }
										else {  ?>
                                      <tr>
                                          <td>No hay comunicados</td>
                                          <td></td>
                                          <td></td>
                                      </tr>
                                  <?php } ?>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>

      <td width="" ><center>
      <a href="https://proyectosinternos.com/Taller5/support/add" style="color:#fff" class="btn btn-info"><i class="fa fa-plus"></i> Nuevo</a></center>
      <td></td>

    </td>
      <tr>

                                  </tbody>
                              </table>
                          </div>
                          <!-- /.box-body-->
                      </div>
                      <!-- /.box -->


                      <!-- end content -->
                  </div>

              </div>
              <!-- end widget div -->
          </div>
          <!-- end widget -->

      </article>
    </div>
    <!--end Container -->



</section>
<style media="screen">
  .box .pendiente-list{margin:0;padding:0;list-style:none}.box .pendiente-list > li{-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;padding:10px;background:#f3f4f5;margin-bottom:2px;border-left:2px solid #e6e7e8;color:#444}.box .pendiente-list > li:last-of-type{margin-bottom:0}.box .pendiente-list > li.danger{border-left-color:#f31212}.box .pendiente-list > li.warning{border-left-color:#f31212}.box .pendiente-list > li.info{border-left-color:#1080d0}.box .pendiente-list > li.success{border-left-color:#00a65a}.box .pendiente-list > li.primary{border-left-color:#3c8dbc}.box .pendiente-list > li > input[type='checkbox']{margin:0 10px 0 5px}.box .pendiente-list > li .text{display:inline-block;margin-left:5px;font-weight:600}.box .pendiente-list > li .label{margin-left:10px;font-size:9px}.box .pendiente-list > li .tools{display:none;float:right;color:#f31212}.box .pendiente-list > li .tools > .fa,.box .pendiente-list > li .tools > .glyphicon,.box .pendiente-list > li .tools > .ion{margin-right:5px;cursor:pointer}.box .pendiente-list > li:hover .tools{display:inline-block}.box .pendiente-list > li.done{color:#999}.box .pendiente-list > li.done .text{text-decoration:line-through;font-weight:500}.box .pendiente-list > li.done .label{background:#eaeaec!important}.box .pendiente-list .handle{display:inline-block;cursor:move;margin:0 5px}
  .pendiente-group-title{margin:0;line-height:31px;padding:0 0 0 10px;background:#fafafa;border-bottom:1px solid #e7e7e7;border-top:1px solid #f4f4f4;color:#999;font-size:17px;font-weight:300}.pendiente{margin:0;padding:0;min-height:5px;list-style:none}.pendiente > li{display:block;position:relative;overflow:hidden;border-bottom:1px solid #e7e7e7;margin:0 5px;background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAQCAYAAADagWXwAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo0RkQ1OEY4NTM4NUIxMUUzQjdCMUMxQzJCQUE3MTMxOCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo0RkQ1OEY4NjM4NUIxMUUzQjdCMUMxQzJCQUE3MTMxOCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjRGRDU4RjgzMzg1QjExRTNCN0IxQzFDMkJBQTcxMzE4IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjRGRDU4Rjg0Mzg1QjExRTNCN0IxQzFDMkJBQTcxMzE4Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+pTD+wgAAACtJREFUeNpimDBhwn8gYABhdDYjiIELMDHgAUwTJ06Ea0VnjxpLO2MBAgwAGYZLegQbQ3UAAAAASUVORK5CYII=) 1px 11px no-repeat}.pendiente > li:last-child,.pendiente > li:only-child{border-bottom:none}.pendiente .checkbox{margin-bottom:4px;padding-left:25px;font-size:15px;line-height:25px;color:#404040;cursor:pointer;font-size:13px}.pendiente .checkbox:last-child{margin-bottom:0}.pendiente .checkbox input{position:absolute;left:-9999px}.pendiente .checkbox i{position:absolute;top:3px;left:0;display:block;width:17px;height:17px;outline:none;border-width:1px;border-style:solid;background:#FFF}.pendiente .checkbox input + i:after{position:absolute;opacity:0;transition:opacity .1s;-o-transition:opacity .1s;-ms-transition:opacity .1s;-moz-transition:opacity .1s;-webkit-transition:opacity .1s;content:'\f00c';top:-1px;left:1px;width:15px;height:15px;font:normal 16px/19px FontAwesome;text-align:center}.pendiente .checkbox input:checked:hover + i:after{content:'\f00d'}.pendiente .checkbox input:checked:disabled:hover + i:after{content:'\f00c'}.pendiente .checkbox input:checked + i:after{opacity:1}.pendiente .inline-group{margin:0 -30px -4px 0}.pendiente .inline-group:after{content:'';display:table;clear:both}.pendiente .inline-group .checkbox{float:left;margin-right:30px}.pendiente .inline-group .checkbox:last-child{margin-bottom:4px}.pendiente > li > :first-child{display:block;border-right:1px solid #FFE1EB;height:100%;padding:6px 11px 6px 18px;width:20px;vertical-align:top;position:absolute}.pendiente > li > :first-child:hover{cursor:move}.pendiente > li.complete > :first-child:hover{cursor:default}.pendiente > li.complete{background:none}.pendiente > li > p{height:100%;margin-left:52px;border-left:1px solid #FFE1EB;display:inline-block;padding:8px 0 6px 7px;margin-bottom:0;min-height:37px;line-height:normal;font-size:14px;font-weight:500;color:#333}.pendiente > li > p > span{display:block;line-height:12px;font-size:10px;font-weight:400}.pendiente > li > p > :first-child{margin-top:-5px;color:#999;margin-bottom:4px}.pendiente > li > p > .date{color:#bfbfbf}.pendiente > li.complete > *{text-decoration:line-through;font-style:italic}.pendiente > li.ui-sortable-helper{border-top:1px solid #eee;background:rgba(113,132,63,0.1)}
</style>
<!-- end widget grid -->
