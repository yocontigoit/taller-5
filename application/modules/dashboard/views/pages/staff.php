<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 7/12/2015
 * Time: 10:57 AM
 */
?>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">
        <article class="col-sm-12">
            <!-- new widget -->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-colorbutton="false">

                <!-- widget options:
                usage: <div class="appswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-calendar"></i> </span>

                    <h2> My Events </h2>

                    <div class="widget-toolbar">
                        <!-- add: non-hidden - to disable auto hide -->

                    </div>
                </header>

                <!-- widget div-->
                <div>
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>
                                        <?php echo number_format($users); ?>
                                    </h3>

                                    <p>
                                        Users
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <a href="<?php echo base_url(); ?>user" class="small-box-footer">
                                    Total Users Registered <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <!-- ./col -->

                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>
                                        7 <?php //echo number_format($activity); ?>
                                    </h3>

                                    <p>
                                        Clients
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-users"></i>
                                </div>

                                <a href="<?php echo base_url(); ?>client" class="small-box-footer">
                                    Total Clients Registered <i class="fa fa-arrow-circle-right"></i>
                                </a>

                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>
                                        <?php echo number_format($projects); ?>
                                    </h3>

                                    <p>
                                        Total Projects
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-tasks"></i>
                                </div>
                                <a href="<?php echo base_url(); ?>project" class="small-box-footer">
                                    Total Projects <i class="fa fa-arrow-circle-right"></i>
                                </a>

                            </div>
                        </div>
                        <!-- ./col -->


                        <div class="col-lg-3 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>
                                        <?php
                                        $temp = 150;//$database->row();
                                        ?>
                                        <?php //echo number_format($temp->used,2); ?>4 MB
                                    </h3>

                                    <p>
                                        MySQL: Used
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-database"></i>
                                </div>
	   		<span class="small-box-footer">
	            Database <strong><i><?php //echo $this->db->database; ?></i></strong>
	        </span>
                            </div>
                        </div>
                        <!-- ./col -->

                    </div>


                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

        </article>
    </div>


    <!-- end row -->

    <!-- Start another row -->
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-6">
            <!-- new widget -->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false"
                 data-widget-fullscreenbutton="false">

                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->

                <header>
                    <span class="widget-icon"> <i class="fa fa-bar-chart-o txt-color-white"></i> </span>

                    <h2> Recent Projects </h2>

                </header>

                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <div>
                            <label>Title:</label>
                            <input type="text"/>
                        </div>
                    </div>
                    <!-- end widget edit box -->

                    <div class="widget-body widget-hide-overflow no-padding">
                        <!-- content goes here -->
                        <!-- recent-projects -->
                        <div class="box box-primary">

                            <div class="box-body">
                                <table class="table table-striped table-th-block table-success">
                                    <thead>
                                    <tr>
                                        <th>Progress<? //=lang('progress')?></th>
                                        <th>Project Name<? //=lang('project_name')?> </th>
                                        <th class="hidden-xs">Due Date<? //=lang('project_name')?> </th>
                                        <th>Options<? //=lang('options')?></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php
                                    if (!empty($projects)) {
                                        foreach ($projects as $key => $project) { ?>
                                            <tr>
                                                <?php
                                                $project->auto_progress = 'TRUE';
                                                if ($project->auto_progress == 'FALSE') {
                                                    $progress = $project->progress;
                                                } else {
                                                    $project->time_logged = 200;
                                                    $project->estimate_hours = 8888;
                                                    //$progress = round((($project->time_logged/3600)/$project->estimate_hours)*100,2);
                                                    $progress = 90;
                                                } ?>
                                                <td>
                                                    <?php if ($progress >= 100) {
                                                        $bg = 'success';
                                                    } else {
                                                        $bg = 'danger';
                                                    } ?>
                                                    <div class="progress progress-xs progress-striped active">
                                                        <div class="progress-bar progress-bar-<?= $bg ?>"
                                                             data-toggle="tooltip"
                                                             data-original-title="<?= $progress ?>%"
                                                             style="width: <?= $progress ?>%">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><?= $project->Description ?> </td>
                                                <td class="hidden-xs">2014/12/31</td>
                                                <td>
                                                    <a class="btn  btn-dark btn-xs"
                                                       href="<?= base_url() ?>projects/view/details/<?= $project->worknumber ?>">
                                                        <i class="fa fa-suitcase text"></i> Project</a>
                                                </td>
                                            </tr>
                                        <?php }
                                    } else { ?>
                                        <tr>
                                            <td>Nothing to display here</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    <?php } ?>


                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body-->
                        </div>
                        <!-- /.box -->


                        <!-- end content -->
                    </div>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->
            <!-- new widget -->
            <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-3" data-widget-colorbutton="false">

                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-calendar"></i> </span>

                    <h2>MY TASK</h2>

                    <div class="widget-toolbar">
                        <!-- add: non-hidden - to disable auto hide -->
                        <div class="btn-group">
                            <button class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown">
                                Showing <i class="fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu js-status-update pull-right">
                                <li>
                                    <a href="javascript:void(0);" id="mt">Month</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" id="ag">Agenda</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" id="td">Today</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">

                        <input class="form-control" type="text">

                    </div>
                    <!-- end widget edit box -->

                    <div class="widget-body no-padding">
                        <!-- content goes here -->

                        <!-- BEGIN CURRENCY RATES -->
                        <div class="panel panel-primary panel-square panel-no-border">

                            <ul class="list-group my-tasksticker widget-tasksticker">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-5"><a href="#fakelink">EUR / USD</a></div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-5">1.36786</div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-2"><i class="fa fa-caret-up text-success"></i></div>
                                        <!-- /.col-cs-4 -->
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-5"><a href="#fakelink">USD / JPY</a></div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-5">101.538</div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-2"><i class="fa fa-caret-up text-success"></i></div>
                                        <!-- /.col-cs-4 -->
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-5"><a href="#fakelink">GBP / USD</a></div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-5">1.71523</div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-2"><i class="fa fa-caret-up text-success"></i></div>
                                        <!-- /.col-cs-4 -->
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-5"><a href="#fakelink">USD / CHF</a></div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-5">0.88748</div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-2"><i class="fa fa-caret-down text-danger"></i></div>
                                        <!-- /.col-cs-4 -->
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-5"><a href="#fakelink">USD / CAD</a></div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-5">1.06365</div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-2"><i class="fa fa-caret-up text-success"></i></div>
                                        <!-- /.col-cs-4 -->
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-5"><a href="#fakelink">EUR / JPY</a></div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-5">138.890</div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-2"><i class="fa fa-caret-up text-success"></i></div>
                                        <!-- /.col-cs-4 -->
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-5"><a href="#fakelink">AUD / USD</a></div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-5">0.94975</div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-2"><i class="fa fa-caret-down text-danger"></i></div>
                                        <!-- /.col-cs-4 -->
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-5"><a href="#fakelink">EUR / USD</a></div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-5">1.36786</div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-2"><i class="fa fa-caret-up text-success"></i></div>
                                        <!-- /.col-cs-4 -->
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-5"><a href="#fakelink">USD / JPY</a></div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-5">101.538</div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-2"><i class="fa fa-caret-up text-success"></i></div>
                                        <!-- /.col-cs-4 -->
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-5"><a href="#fakelink">GBP / USD</a></div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-5">1.71523</div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-2"><i class="fa fa-caret-up text-success"></i></div>
                                        <!-- /.col-cs-4 -->
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-5"><a href="#fakelink">USD / CHF</a></div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-5">0.88748</div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-2"><i class="fa fa-caret-down text-danger"></i></div>
                                        <!-- /.col-cs-4 -->
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-5"><a href="#fakelink">USD / CAD</a></div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-5">1.06365</div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-2"><i class="fa fa-caret-up text-success"></i></div>
                                        <!-- /.col-cs-4 -->
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-5"><a href="#fakelink">EUR / JPY</a></div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-5">138.890</div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-2"><i class="fa fa-caret-up text-success"></i></div>
                                        <!-- /.col-cs-4 -->
                                    </div>
                                    <!-- /.row -->
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-5"><a href="#fakelink">AUD / USD</a></div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-5">0.94975</div>
                                        <!-- /.col-cs-4 -->
                                        <div class="col-xs-2"><i class="fa fa-caret-down text-danger"></i></div>
                                        <!-- /.col-cs-4 -->
                                    </div>
                                    <!-- /.row -->
                                </li>
                            </ul>
                        </div>
                        <!-- /.panel panel-default panel-square panel-no-border -->
                        <!-- END MY TASKS -->

                        <!-- end content -->
                    </div>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->
        </article>


        <article class="col-sm-12 col-md-12 col-lg-6">

            <!-- new widget -->
            <div class="jarviswidget" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false">

                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->

                <header>
                    <span class="widget-icon"> <i class="fa fa-map-marker"></i> </span>

                    <h2>Recent Activities</h2>

                </header>

                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <div>
                            <label>Title:</label>
                            <input type="text"/>
                        </div>
                    </div>
                    <!-- end widget edit box -->

                    <div class="widget-body widget-hide-overflow no-padding">
                        <!-- content goes here -->
                        <div class="box box-primary">
                            <div class="box-body timeline-scroll">
                                <?php if (count($activities)) : ?>
                                    <ul class="timeline-xs margin-top-15 margin-bottom-15">

                                        <?php foreach ($activities as $activity): ?>
                                            <li class="timeline-item <?php echo $activity->activity_status; //success/danger/info/warning?>">
                                                <div class="margin-left-15">
                                                    <div
                                                        class="text-muted text-small"> <?php echo $activity->activity_date;//2 minutes ago ?> </div>
                                                    <p>
                                                        <a class="text-info"
                                                           href=""><?php echo $activity->user_id; //Steven?> </a>
                                                        <?php echo $activity->activity_message; // has completed his account. ?>
                                                    </p>
                                                </div>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php else: ?>
                                    <p>No recent activities</p>
                                <?php endif; ?>
                            </div>
                        </div>
                        <!-- end content -->

                    </div>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

            <!-- new widget -->
            <div class="jarviswidget jarviswidget-color-blue" id="wid-id-4" data-widget-editbutton="false"
                 data-widget-colorbutton="false">

                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->

                <header>
                    <span class="widget-icon"> <i class="fa fa-check txt-color-white"></i> </span>

                    <h2> ToDo List </h2>
                    <!-- <div class="widget-toolbar">
                    add: non-hidden - to disable auto hide

                    </div>-->
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <div>
                            <label>Title:</label>
                            <input type="text"/>
                        </div>
                    </div>
                    <!-- end widget edit box -->

                    <div class="widget-body no-padding smart-form">
                        <!-- content goes here -->
                        <?php if (count($overduetodolists)): ?>
                            <?php $count = count($overduetodolists); ?>
                            <h5 class="todo-group-title"><i class="fa fa-warning"></i> Overdue tasks (
                                <small class="num-of-tasks"><?php echo $count; ?></small>
                                )
                            </h5>
                        <?php else: ?>
                            <h5 class="todo-group-title"><i class="fa fa-warning"></i> Overdue tasks (
                                <small class="num-of-tasks">0</small>
                                )
                            </h5>
                        <?php endif; ?>
                        <ul id="sortable1" class="todo">

                            <?php if (count($overduetodolists)): foreach ($overduetodolists as $overduetodolist): ?>
                                <li>
                                               <span class="handle"> <label class="checkbox">
                                                       <?php
                                                       $data = array(
                                                           'name' => 'checkbox-inline[]',
                                                           'id' => 'overdue_' . $overduetodolist->TANUM,
                                                           'value' => $overduetodolist->TANUM,
                                                           'checked' => false,
                                                       );

                                                       echo form_checkbox($data);
                                                       ?>
                                                       <!-- <input type="checkbox" name="checkbox-inline"> -->
                                                       <i></i> </label> </span>

                                    <p>
                                        <strong><?php echo $overduetodolist->ActType . ' #' . $overduetodolist->PNAME; ?></strong>
                                        - <?php echo $overduetodolist->Detail; ?>  [<a href="javascript:void(0);"
                                                                                       class="font-xs">More Details</a>]
                                        <span class="text-muted"> </span>
                                        <span
                                            class="text-warning"><?php echo $overduetodolist->DueDate . ', BY :' . $overduetodolist->InputBy; ?></span>
                                    </p>
                                </li>
                            <?php endforeach; ?>
                            <?php else: ?>
                                <li>
                                    <span class="handle">  </span>

                                    <p>
                                        You have no overdue a tasks...

                                    </p>
                                </li>
                            <?php endif; ?>


                        </ul>
                        <!-- Outstanding tasks -->
                        <?php if (count($ouststandingtodolists)): ?>
                            <?php $count = count($ouststandingtodolists); ?>
                            <h5 class="todo-group-title"><i class="fa fa-exclamation"></i> Outstanding tasks (
                                <small class="num-of-tasks"><?php echo $count; ?></small>
                                )
                            </h5>
                        <?php else: ?>
                            <h5 class="todo-group-title"><i class="fa fa-exclamation"></i> Outstanding tasks (
                                <small class="num-of-tasks">0</small>
                                )
                            </h5>
                        <?php endif; ?>


                        <ul id="sortable2" class="todo">


                            <?php if (count($ouststandingtodolists)): foreach ($ouststandingtodolists as $ouststandingtodolist): ?>
                                <li>
                                                <span class="handle"> <label class="checkbox">
                                                        <?php
                                                        $data = array(
                                                            'name' => 'checkbox-inline[]',
                                                            'id' => 'ouststandin_' . $ouststandingtodolist->TANUM,
                                                            'value' => $ouststandingtodolist->TANUM,
                                                            'checked' => false,
                                                        );

                                                        echo form_checkbox($data);
                                                        ?>
                                                        <i></i> </label> </span>

                                    <p>
                                        <strong><?php echo $ouststandingtodolist->ActType . ' #' . $ouststandingtodolist->PNAME; ?></strong>
                                        - <?php echo $ouststandingtodolist->Detail; ?>  [<a href="javascript:void(0);"
                                                                                            class="font-xs">More
                                            Details</a>] <span class="text-muted"> </span>
                                        <span
                                            class="text-success"><?php echo $ouststandingtodolist->DueDate . ', BY :' . $ouststandingtodolist->InputBy; ?></span>
                                    </p>
                                </li>
                            <?php endforeach; ?>
                            <?php else: ?>
                                <li>
                                    <span class="handle">  </span>

                                    <p>
                                        You have no outstanding tasks

                                    </p>
                                </li>
                            <?php endif; ?>


                        </ul>


                        <!-- end content -->


                        <?php if (count($completedtasks)): ?>
                            <?php $count = count($completedtasks); ?>
                            <h5 class="todo-group-title"><i class="fa fa-exclamation"></i> Completed Tasks (
                                <small class="num-of-tasks"><?php echo $count; ?></small>
                                )
                            </h5>
                        <?php else: ?>
                            <h5 class="todo-group-title"><i class="fa fa-exclamation"></i> Completed Tasks (
                                <small class="num-of-tasks">0</small>
                                )
                            </h5>
                        <?php endif; ?>
                        <!-- <h5 class="todo-group-title"><i class="fa fa-check"></i> Completed Tasks (<small class="num-of-tasks">0</small>)</h5> -->
                        <ul id="sortable3" class="todo">

                        </ul>
                    </div>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

        </article>


    </div>
    <!--new container -->
    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-6">
            <!-- new widget -->
            <div class="jarviswidget jarviswidget-color-blue" id="wid-id-5" data-widget-editbutton="false"
                 data-widget-colorbutton="false">

                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->

                <header>
                    <span class="widget-icon"> <i class="fa fa-check txt-color-white"></i> </span>

                    <h2> ToDo's </h2>
                    <!-- <div class="widget-toolbar">
                    add: non-hidden - to disable auto hide

                    </div>-->
                </header>

                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <div>
                            <label>Title:</label>
                            <input type="text"/>
                        </div>
                    </div>
                    <!-- end widget edit box -->

                    <div class="widget-body no-padding smart-form">
                        <!-- content goes here -->
                        <h5 class="todo-group-title"><i class="fa fa-warning"></i> Critical Tasks (
                            <small class="num-of-tasks">1</small>
                            )
                        </h5>
                        <ul id="sortable1" class="todo">
                            <li>
												<span class="handle"> <label class="checkbox">
                                                        <input type="checkbox" name="checkbox-inline">
                                                        <i></i> </label> </span>

                                <p>
                                    <strong>Ticket #17643</strong> - Hotfix for WebApp interface issue [<a
                                        href="javascript:void(0);" class="font-xs">More Details</a>] <span
                                        class="text-muted">Sea deep blessed bearing under darkness from God air living isn't. </span>
                                    <span class="date">Jan 1, 2014</span>
                                </p>
                            </li>
                        </ul>
                        <h5 class="todo-group-title"><i class="fa fa-exclamation"></i> Important Tasks (
                            <small class="num-of-tasks">3</small>
                            )
                        </h5>
                        <ul id="sortable2" class="todo">
                            <li>
												<span class="handle"> <label class="checkbox">
                                                        <input type="checkbox" name="checkbox-inline">
                                                        <i></i> </label> </span>

                                <p>
                                    <strong>Ticket #1347</strong> - Inbox email is being sent twice
                                    <small>(bug fix)</small>
                                    [<a href="javascript:void(0);" class="font-xs">More Details</a>] <span class="date">Nov 22, 2013</span>
                                </p>

                                <span class="easyPieChart easy-pie-chart-4 pull-right" data-size="40" data-percent="80">

									<span class="percent">80</span>

                                </span>
                            </li>
                            <li>
												<span class="handle"> <label class="checkbox">
                                                        <input type="checkbox" name="checkbox-inline">
                                                        <i></i> </label> </span>

                                <p>
                                    <strong>Ticket #1314</strong> - Call customer support re: Issue <a
                                        href="javascript:void(0);" class="font-xs">#6134</a>
                                    <small>(code review)</small>
                                    <span class="date">Nov 22, 2013</span>
                                </p>
                            </li>
                            <li>
												<span class="handle"> <label class="checkbox">
                                                        <input type="checkbox" name="checkbox-inline">
                                                        <i></i> </label> </span>

                                <p>
                                    <strong>Ticket #17643</strong> - Hotfix for WebApp interface issue [<a
                                        href="javascript:void(0);" class="font-xs">More Details</a>] <span
                                        class="text-muted">Sea deep blessed bearing under darkness from God air living isn't. </span>
                                    <span class="date">Jan 1, 2014</span>
                                </p>
                            </li>
                        </ul>

                        <h5 class="todo-group-title"><i class="fa fa-check"></i> Completed Tasks (
                            <small class="num-of-tasks">1</small>
                            )
                        </h5>
                        <ul id="sortable3" class="todo">
                            <!-- <li class="complete">
                                                 <span class="handle" style="display:none"> <label class="checkbox state-disabled">
                                                         <input type="checkbox" name="checkbox-inline" checked="checked" disabled="disabled">
                                                         <i></i> </label> </span>
                                 <p>
                                     <strong>Ticket #17643</strong> - Hotfix for WebApp interface issue [<a href="javascript:void(0);" class="font-xs">More Details</a>] <span class="text-muted">Sea deep blessed bearing under darkness from God air living isn't. </span>
                                     <span class="date">Jan 1, 2014</span>
                                 </p>
                             </li>-->
                        </ul>

                        <!-- end content -->
                    </div>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->

        </article>
    </div>
    <!--end Container -->
</section>
<!-- end widget grid -->