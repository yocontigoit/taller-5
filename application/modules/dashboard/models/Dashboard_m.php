<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/18
 * Time: 11:11 AM
 */
class Dashboard_M extends CI_Model
{






    /**
     * count all users
     * @return mixed
     */
    function getCountUsers()
    {
        return $this->db->count_all('users');
    }

    /**
     * count all clients
     * @return mixed
     */
    function getCountClients()
    {
        return $this->db->count_all('client');
    }


    /**
     * count all projects
     * @return mixed
     */
    function getCountProjects()
    {

        return $this->db->count_all('projects');
    }


    /*
     * Get recent projects
     */
    function getRecentProjects($limit = 5)
    {

        return $this->db->order_by('project_id', 'DESC')->get('projects', $limit)->result();

    }
    function getRecentComunicados($limit = 5)
    {
        return $this->db->order_by('com_id', 'DESC')->get('comunicados', $limit)->result();


    }
    /**
     * count all open tickets
     * @return mixed
     */
    function get_open_tickets()
    {

        return $this->db->count_all('ticket', 'ticket_status <> 4');

    }

    /**
     * get  all recent activities
     * @param int $limit
     * @return mixed
     */
    function getRecentActivities($limit = 50)
    {

        return $this->db->order_by('activity_date', 'DESC')->get('activity_log', $limit)->result();
    }

    /**
     * get recent activities
     * @param int $limit
     * @param $client_id
     * @return mixed
     */
    function getClientRecentActivities($limit = 5, $client_id)
    {


        $this->db->where('client_id', $client_id);

        return $this->db->order_by('activity_date', 'DESC')->get('project_activities', $limit)->result();
    }

    function getOutstandingInvoice()
    {

        //  return $this->db->order_by('activity_date','DESC')->get('projects',$limit)->result();
    }

    /**
     * Get my overdue tasks
     * @return mixed
     */
    function get_my_overdue_tasks()
    {
        $this->db->from('tasks');
        $this->db->join('projects', 'projects.project_id = tasks.project_id');
        $this->db->where('DATEDIFF(NOW(), from_unixtime(task_end/1000)) > 0');
        $this->db->where('task_progress < 100');
        $this->db->where("task_status != 'STATUS_DONE'");

        return $this->db->get()->result();

    }

    /**
     * get current logged user outstanding tasks
     * @return mixed
     */
    function get_my_outstanding_tasks()
    {
        $this->db->from('tasks');
        $this->db->join('projects', 'projects.project_id = tasks.project_id');
        $this->db->where('DATEDIFF(NOW(), from_unixtime(task_end/1000)) <= 0');
        $this->db->where('task_progress','1');
        $this->db->where("task_status != 'STATUS_DONE'");

        return $this->db->get()->result();

    }

    function get_my_overdue_pending() {

      $this->db->from('pendientes');
      $this->db->where("com_status != 'STATUS_Terminada'");
        return $this->db->get()->result();
    }

      function tasks()
    {
	    $this->db->select('*');
		$this->db->from('tasks');
        $this->db->join('projects', 'projects.project_id  = tasks.project_id');
        return $this->db->get()->result();
    }
    function  get_assigned_team()
    {

        $this->db->from('project_assignment');

        return $this->db->get()->result();

    }
    /**
     * get ten recent payments
     * @return mixed
     */
    function  get_recent_payments()
    {
        $this->db->from("transaction");
        $this->db->join('invoice', 'invoice.id=transaction.invoice_id');
        $this->db->order_by('transaction.created_at', 'DESC');
        $this->db->limit(10);

        return $this->db->get()->result();
    }


    /**
     * count client users
     * @param $client_id
     * @return mixed
     */
    function count_client_users($client_id)
    {

        $this->db->where('client_id', $client_id);
        $this->db->from('client_user');

        return $this->db->count_all_results();
    }

    /**
     * count tickets by client
     * @param $client_id
     * @return mixed
     */
    function  count_client_tickets($client_id)
    {

        $this->db->where('ticket_client_id', $client_id);
        $this->db->from('ticket');

        return $this->db->count_all_results();
    }

    /**
     * count client projects
     * @param $client_id
     * @return mixed
     */
    function  count_client_projects($client_id)
    {

        $this->db->where('client_id', $client_id);
        $this->db->from('projects');

        return $this->db->count_all_results();
    }

    /**
     * get client recent projects
     * @param $client_id
     * @param int $limit
     * @return mixed
     */
    function  get_client_recent_projects($client_id, $limit = 5)
    {

        $this->db->where('client_id', $client_id);

        return $this->db->order_by('project_id', 'DESC')->get('projects', $limit)->result();

    }

    /**
     * get client recent activities
     * @param $user_id
     * @param $user_type
     * @return mixed
     */
    function  get_client_recent_activities($user_id, $user_type)
    {

        $this->db->from('activity_log');

        $this->db->where('activity_user', $user_id);
        $this->db->where('activity_user_type', $user_type);
        $this->db->order_by('activity_date', 'DESC');

        return $this->db->get()->result();
    }

    /**
     * count client open tickets
     * @param $client_id
     * @return mixed
     */
    function  get_client_open_tickets($client_id)
    {

        $this->db->where('ticket_client_id', $client_id);

        return $this->db->count_all('ticket', 'ticket_status <> 4');

    }

    /**
     * get recent paynments by client
     * @param $client_id
     * @return mixed
     */
    function  get_client_recent_payments($client_id)
    {
        $this->db->from("transaction");
        $this->db->join('invoice', 'invoice.id=transaction.invoice_id');
        $this->db->where('invoice.client_id', $client_id);
        $this->db->order_by('transaction.created_at', 'DESC');
        $this->db->limit(10);

        return $this->db->get()->result();
    }

	//Calendario

	function payments()
    {
        $this->db->select('*,transaction.id as payment_id');
        $this->db->from('transaction');
        $this->db->join('invoice', 'invoice.id  = transaction.invoice_id');

		/*$this->db->select('*');
		$this->db->from('tasks');*/
        return $this->db->get()->result();

    }

    /**
     * retrieve all projects
     * @return mixed
     */
    function projects()
    {

        $this->db->from('projects');
        $this->db->join('client', 'client.client_id  = projects.client_id');

        return $this->db->get()->result();
    }

    function invoices()
    {
        $this->db->from('invoice');
        $this->db->join('client', 'invoice.client_id  = client.client_id');

        return $this->db->get()->result();

    }

    function estimates()
    {

        $this->db->from('estimate');
        $this->db->join('client', 'estimate.client_id  = client.client_id');

        return $this->db->get()->result();
    }

    function quotations()
    {

        $this->db->from('quotation');
        $this->db->join('client', 'quotation.client_id  = client.client_id');

        return $this->db->get()->result();
    }

    function agenda()
    {

        $this->db->from("events");
        $this->db->select("
                event_id,
				user_id,
				start_date,
				DATE_FORMAT(start_date,'%Y-%m-%d') AS startsOnDate,
				DATE_FORMAT(start_date,'%m') AS startMonth,
				DATE_FORMAT(start_date,'%H:%i') AS startTime,
				DATE_FORMAT(start_date,'%H:%i') AS timeStart,
				DATE_FORMAT(start_date,'%l:%i %p') AS displaystart,
				DATE_FORMAT(start_date,'%M %d, %Y') AS displayDate,
				end_date,
				DATE_FORMAT(end_date,'%Y-%m-%d') AS endsOnDate,
				DATE_FORMAT(end_date,'%m') AS endMonth,
				DATE_FORMAT(end_date,'%H:%i') AS endTime,
				DATE_FORMAT(end_date,'%Y, %m, %d') AS dateEnd,
				DATE_FORMAT(end_date,'%l:%i %p') AS displayend,
				event_title,
				event_description,
				event_color,
				is_task", false);

        return $this->db->get()->result();

    }

    function add_event($data = array())
    {
        $success = false;
        $success = $this->db->insert('events', $data);

        return $success;
    }

    function update_event($data = array(), $event_id = null)
    {

        $success = false;
        $success = $this->db->update('events', $data, array('event_id' => intval($event_id)));

        return $success;
    }

    function task_details($task_id = null)
    {

        $this->db->from('tasks');
        $this->db->where("task_id", $task_id);

        return $this->db->get()->row();

    }

    function payment_details($trans_id = null)
    {
        $this->db->from('transaction');
        $this->db->join('invoice', 'invoice.id  = transaction.invoice_id');
        $this->db->where("transaction.id", $trans_id);

        return $this->db->get()->row();
    }

    function project_details($project_id = null)
    {
        $this->db->from('projects');
        $this->db->join('client', 'client.client_id  = projects.client_id');
        $this->db->where("projects.project_id", $project_id);

        return $this->db->get()->row();
    }

    function invoice_details($invoice_id = null)
    {
        $this->db->from('invoice');
        $this->db->join('client', 'invoice.client_id  = client.client_id');
        $this->db->where("invoice.id", $invoice_id);

        return $this->db->get()->row();
    }

    function estimate_details($estimate_id = null)
    {

        $this->db->from('estimate');
        $this->db->join('client', 'estimate.client_id  = client.client_id');
        $this->db->where("estimate.est_id", $estimate_id);

        return $this->db->get()->row();
    }

    function quotation_details($quotation_id = null)
    {
        $this->db->from('quotation');
        $this->db->join('client', 'quotation.client_id  = client.client_id');
        $this->db->where("quotation.quote_project_id", $quotation_id);

        return $this->db->get()->row();
    }

    function agenda_details($event_id = null)
    {
        $this->db->from('events');
        $this->db->where("event_id", $event_id);

        return $this->db->get()->row();
    }

    function calendar_settings($data = array(), $company_id = null)
    {

        $success = false;
        $success = $this->db->update('settings_system', $data, array('company_company_id' => intval($company_id)));

        return $success;
    }

    function add_log($data = array())
    {
        $success = false;
        $success = $this->db->insert("activity_log", $data);

        return $success;
    }
}
