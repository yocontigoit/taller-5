<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/05/21
 * Time: 01:46 PM
 */
class Dashboard extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('dashboard_m');
    }

    /**
     * displays dashboard
     */
    public function index()
    {

        $admin = "admin";
        $this->data['content'] = 'dashboard/index';
        $this->data['title'] = 'Escritorio';
        $this->data['tasks'] = $this->dashboard_m->tasks();//Show all
    		$this->data['agenda'] = $this->dashboard_m->agenda();
    		$this->data['show_calendar'] = true;
    		$this->data['SCRIPT_PAGE'] = true;


        //displays admin and staff dashboard
        if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) {//if admin

            $this->data['users'] = $this->dashboard_m->getCountUsers();//Show all
            $this->data['clients'] = $this->dashboard_m->getCountClients();
            $this->data['projects_count'] = $this->dashboard_m->getCountProjects();//Show all
            $this->data['projects'] = $this->dashboard_m->getRecentProjects();
            $this->data['comunicados'] = $this->dashboard_m->getRecentComunicados();//Show all
            $this->data['tasks'] = $this->dashboard_m->tasks();//Show all
            $this->data['assigned_team'] = $this->dashboard_m->get_assigned_team();
              //Show all
            $this->data['activities'] = $this->dashboard_m->getRecentActivities(5);//Show all
            $this->data['open_tickets'] = $this->dashboard_m->get_open_tickets();//Show all
            $this->data['my_overdue_tasks'] = $this->dashboard_m->get_my_overdue_tasks();//My tasks/Show all
            $this->data['my_outstanding_tasks'] = $this->dashboard_m->get_my_outstanding_tasks();//My tasks/Show all
            $this->data['recent_payments'] = $this->dashboard_m->get_recent_payments();//My tasks/Show all
            $this->data['show_dashboard'] = true;
            $this->data['show_admin_dashboard_stats'] = true;
            $this->data['show_calendar'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            //Show pending
            $this->data['my_overdue_pending'] = $this->dashboard_m->get_my_overdue_pending();//My tasks/Show all

            $this->data['invoice_status'] = null;
            $this->data['_dashboard'] = $this->load->view('dashboard/pages/admin_grid', $this->data, true);

        } elseif ($this->CompanyUserRolePermission_id == 3) {//if client...

            $this->data['users'] = $this->dashboard_m->count_client_users($this->CompanyClient_id);//Show all
            $this->data['tickets'] = $this->dashboard_m->count_client_tickets($this->CompanyClient_id);
            $this->data['projects_count'] = $this->dashboard_m->count_client_projects($this->CompanyClient_id);//Show all
            $this->data['projects'] = $this->dashboard_m->get_client_recent_projects($this->CompanyClient_id);//Show all
            $this->data['activities'] = $this->dashboard_m->get_client_recent_activities($this->CompanyClient_user_id,
                $this->CompanyUser_type);//Show all
            $this->data['open_tickets'] = $this->dashboard_m->get_client_open_tickets($this->CompanyClient_id);//Show all
            $this->data['recent_payments'] = $this->dashboard_m->get_client_recent_payments($this->CompanyClient_id);//Show all
            $this->data['show_dashboard'] = true;
            $this->data['show_client_dashboard_stats'] = true;
            $this->data['show_calendar'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->data['tasks'] = $this->dashboard_m->tasks();//Show all

            $this->data['_dashboard'] = $this->load->view('dashboard/pages/client', $this->data, true);
        }

        $this->load->view('_main_layout', $this->data);
    }

	function event($type, $id)
    {

        $id = intval($id);
        switch ($type) {
            case "tasks" :
                $this->data['tasks'] = $this->dashboard_m->task_details($id);
                $this->load->view('_main_layout', $this->data);
                break;
            case "payments" :
                $this->data['payments'] = $this->dashboard_m->payment_details($id);
                $this->load->view('_main_layout', $this->data);
                break;
            case "projects" :
                $this->data['projects'] = $this->dashboard_m->project_details($id);
                $this->load->view('_main_layout', $this->data);
                break;
            case "invoices" :
                $this->data['invoices'] = $this->dashboard_m->invoice_details($id);
                $this->load->view('_main_layout', $this->data);
                break;
            case "estimates" :
                $this->data['estimates'] = $this->dashboard_m->estimate_details($id);
                $this->load->view('_main_layout', $this->data);
                break;
            case "quotations" :
                $this->data['quotations'] = $this->dashboard_m->quotation_details($id);
                $this->load->view('_main_layout', $this->data);
                break;
            case "events" :
                $this->data['quotations'] = $this->dashboard_m->agenda_details($id);
                $this->load->view('_main_layout', $this->data);
                break;
        }

    }

    /**
     * add new event to calendar
     */
    function add_event()
    {

        if ($this->input->post()) {

            $start_date = $this->input->post('start_date');
            $end_time = $this->input->post('event_time');
            $start_date = date('Y-m-d H:i:s', strtotime($start_date . ' ' . $end_time));

            $end_date = $this->input->post('end_date');
            $end_time = $this->input->post('end_time');
            $end_date = date('Y-m-d H:i:s', strtotime($end_date . ' ' . $end_time));

            $event_color = ($this->input->post('colorPick')) ? $this->input->post('colorPick') : '#78a32d';
            $data = array(
                "start_date" => $start_date,
                "end_date" => $end_date,
                "user_id" => $this->CompanyUser_id,
                "event_title" => $this->input->post('event_title'),
                "event_description" => $this->input->post('event_desc'),
                "event_color" => $event_color
            );
            //add event to database
            if ($this->dashboard_m->add_event($data)) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'calendar',
                    'activity_details' => $this->input->post('event_title'),
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "New event added",
                    'activity_icon' => 'fa fa-calendar'
                );

                $this->__activity_tracker($log_params);
                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_event_add_success'));
                redirect('dashboard');

            }
        } else {
            $this->load->view('_main_layout', $this->data);
        }

    }

    /**
     * add activity log
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->dashboard_m->add_log($activity_data);

    }

    /**
     * add api to integrate with google
     */
    function settings()
    {

        if ($this->input->post()) {
            $data = array(
                "gcal_api_key" => $this->input->post('gcal_api_key'),
                "gcal_id" => $this->input->post('gcal_id')
            );

            if ($this->dashboard_m->calendar_settings($data, $this->Company_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_calendar_save_success'));
                redirect('dashboard');
            }
        } else {
            $this->load->view('_main_layout', $this->data);
        }

    }


}
