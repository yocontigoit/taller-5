<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/01
 * Time: 02:52 PM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                        class="fa fa-times"></i></span><span class="sr-only"><?php echo "close"; ?></span></button>
            <h4 class="modal-title"><?php echo "Purchase Summary"; ?></h4>
        </div>

        <?php echo form_open($skrill_url, array('id' => 'skrill_form', 'class' => 'form-horizontal')); ?>

        <input type="hidden" name="pay_to_email" value="<?php echo $this->config->item('skrill')->skrill_mail; ?>"/>
        <input type="hidden" name="return_url"
               value="<?php echo base_url($this->config->item('skrill')->skrill_success); ?>"/>
        <input type="hidden" name="cancel_url"
               value="<?php echo base_url($this->config->item('skrill')->skrill_cancel); ?>"/>
        <input type="hidden" name="status_url"
               value="<?php echo base_url($this->config->item('skrill')->skrill_ipn); ?>"/>
        <input type="hidden" name="merchant_fields" value="session_id, item, custom"/>
        <input type="hidden" name="item" value="<?php echo $invoice_info->item_name; ?>"/>
        <input type="hidden" name="session_id" value="<?php echo md5(time()) ?>"/>
        <input type="hidden" name="custom"
               value="<?php echo $invoice_info->item_number . '_' . $invoice_info->item_name; ?>"/>
        <input type="hidden" name="amount" value="<?php echo $invoice_info->amount ?>"/>
        <input type="hidden" name="currency" value="<?php echo $invoice_info->currency; ?>"/>
        <input type="hidden" name="detail1_description" value="<?php echo $invoice_info->item_name; ?>"/>
        <input type="hidden" name="detail1_text" value="<?php echo $invoice_info->item_name; ?>"/>

        <div class="modal-body">
            <table class="table">

                <tbody>
                <tr>
                    <th>Reference</th>
                    <td><input type="text" class="form-control" readonly
                               value="<?php echo $invoice_info->item_name; ?>"/></td>
                </tr>
                <tr>
                    <th>Amount</th>
                    <td><input type="text" class="form-control" readonly
                               value="<?php echo format_amount($invoice_info->amount); ?>"/></td>
                </tr>

                </tbody>
            </table>
        </div>

        <div class="modal-footer">
            <input type="image" src="<?php echo base_url('files/gateways/skrill/skrill.png'); ?>" name="submit"
                   title="Pay With Skrill" alt="">
        </div>
        </form>

    </div>
</div>