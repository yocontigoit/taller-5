<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/01
 * Time: 04:15 PM
 */
class Skrill extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * add payment
     * @param null $invoice_id
     */
    function add_payment($invoice_id = null)
    {

        $reference_no = get_invoice_reference_no($invoice_id);
        $invoice_currency = get_invoice_currency($invoice_id);
        //$invoice_amount = invoice_total($invoice_id);
        $invoice_amount = invoice_balance($invoice_id);
        if ($invoice_amount <= 0) {
            $invoice_amount = 0.00;
        }
        $invoice_info = new stdClass();
        $invoice_info->item_name = $reference_no;
        $invoice_info->item_number = $invoice_id;
        $invoice_info->amount = $invoice_amount;
        $invoice_info->currency = $invoice_currency;

        $this->data['invoice_info'] = $invoice_info;


        if ($this->config->item('skrill')->skrill_active == 0) {
            $skrill_url = 'https://www.moneybookers.com/app/payment.pl';
        } else {
            $skrill_url = 'https://www.moneybookers.com/app/payment.pl';
        }
        $this->data['skrill_url'] = $skrill_url;

        $this->data['title'] = 'Agregar Pago';

        $this->load->view('modal/add_payment', $this->data);


    }

    function cancel()
    {
        $this->session->set_flashdata('msg_status', 'error');
        $this->session->set_flashdata('message', lang('messages_payment_canceled'));
        redirect('clients');
    }

    function success()
    {
        if ($_POST) {
            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', lang('messages_payment_add_success'));
            redirect('clients');
        } else {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', lang('messages_payment_add_error'));
            redirect('clients');
        }
    }
}