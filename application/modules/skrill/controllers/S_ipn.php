<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/06/09
 * Time: 02:21 PM
 */
class S_ipn extends Admin_Controller
{
    // To handle the IPN post made by skrill
    function ipn()
    {
        $this->load->model("skrill_m");
        $r_fields = array(
            'status',
            'md5sig',
            'merchant_id',
            'pay_to_email',
            'mb_amount',
            'mb_transaction_id',
            'currency',
            'amount',
            'transaction_id',
            'pay_from_email',
            'mb_currency'
        );
        $mb_secret = getValue("extra3", "gateways", "name = 'moneybookers'");

        foreach ($r_fields as $f) {
            if (!isset($_POST[$f])) {
                die();
            }
        }

        /* Check for MD5 signature */
        $md5 = strtoupper(md5($_POST['merchant_id'] . $_POST['transaction_id'] . strtoupper(md5($mb_secret)) . $_POST['mb_amount'] . $_POST['mb_currency'] . $_POST['status']));
        if ($md5 != $_POST['md5sig']) {
            die();
        }

        if (intval($_POST['status']) == 2) {
            $mb_currency = $_POST['mb_currency'];
            $mc_gross = $_POST['amount'];
            $txn_id = $_POST['mb_transaction_id'];

            list($invoice_id, $inv_reference) = explode("_", $_POST['custom']);

            $client_details = get_client_details($this->CompanyClient_id);

            //'ip' => $_SERVER['REMOTE_ADDR'],
            $transaction = array(
                'invoice_id' => $invoice_id,
                'trans_id' => $txn_id,
                'amount' => $mc_gross,
                'payment_method' => 4,
                'ip' => $this->input->ip_address(),
                'notes' => 'Paid by ' . $this->CompanyUser_FullName . ' via Skrill | Invoice Currency:' . $mb_currency,
                'creator_email' => $this->CompanyClient_email,
                'created_by' => $client_details->client_name,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );

            if ($this->skrill_m->add_payment($transaction)) {

                $this->notifications->send_payment_notification($this->CompanyClient_id, $invoice_id, $mc_gross);
            }
        } else {
            log_message('error', "Invalid IPN: $_POST" . PHP_EOL);
        }
    }

}