<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/06/13
 * Time: 12:51 PM
 */
class P_ipn extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('paypal/paypal_m');

    }


    function ipn()
    {
        //log_message('success',"IPN started ");
        // Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
        // Set this to 0 once you go live or don't require logging.
        define("DEBUG", 1);
        // Set to 0 once you're ready to go live
        $sandbox = ($this->config->item('paypal')->paypal_active == 1) ? 0 : 1;
        define("USE_SANDBOX", $sandbox);

        // Read POST data
        // reading posted data directly from $_POST causes serialization
        // issues with array data in POST. Reading raw POST data from input stream instead.

        // $sQueryString = "cmd=_notify-validate";
        //foreach($_POST as $sPostKey => $sPostData) { $sQueryString .= "&{$sPostKey}={$sPostData}"; }
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2) {
                $myPost[$keyval[0]] = urldecode($keyval[1]);
            }
        }
        // read the post from PayPal system and add 'cmd'
        $req = 'cmd=_notify-validate';
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }
        foreach ($raw_post_array as $keyval) {
            $keyval = explode('=', $keyval);
            if (count($keyval) == 2) {
                $myPost[$keyval[0]] = urldecode($keyval[1]);
            }
        }

        // Post IPN data back to PayPal to validate the IPN data is genuine
        // Without this step anyone can fake IPN data
        if (USE_SANDBOX == 1) {
            $paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        } else {
            $paypal_url = "https://www.paypal.com/cgi-bin/webscr";
        }
        $ch = curl_init($paypal_url);
        if ($ch == false) {
            return false;
        }

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        if (DEBUG == true) {
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLINFO_HEADER_OUT, 1);


        }
        // CONFIG: Optional proxy configuration
        //curl_setopt($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        // Set TCP timeout to 30 seconds
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close', 'User-Agent: zestpro'));


        //ensure that cURL is using the server IP address
        curl_setopt($ch, CURLOPT_INTERFACE, $_SERVER['SERVER_ADDR']);

        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
        // CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
        // of the certificate as shown below. Ensure the file is readable by the webserver.
        // This is mandatory for some environments.
        //$cert = __DIR__ . "./cacert.pem";
        //curl_setopt($ch, CURLOPT_CAINFO, $cert);
        //curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
        $res = curl_exec($ch);


        // log_message('error',"before.... IPN: $res" . PHP_EOL);
        if (curl_errno($ch) != 0) // cURL error
        {
            if (DEBUG == true) {
                log_message('error', "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL);
            }
            curl_close($ch);
            exit;
        } else {
            // Log the entire HTTP response if debug is switched on.
            if (DEBUG == true) {
                log_message('error', "HTTP request of validation request:" . curl_getinfo($ch,
                        CURLINFO_HEADER_OUT) . " for IPN payload: $req" . PHP_EOL);
                log_message('error', "HTTP response of validation request: $res" . PHP_EOL);
            }
            curl_close($ch);
        }
        // Inspect IPN validation result and act accordingly
        // Split response headers and payload, a better way for strcmp
        //log_message('error',$res."IPN started");
        $tokens = explode("\r\n\r\n", trim($res));
        $res = trim(end($tokens));
        // log_message('error',"Res.... IPN: $res" . PHP_EOL);
        if (strcmp($res, "VERIFIED") == 0) {

            $invoice_id = $_POST['item_number'];
            $invoice_ref = $_POST['item_name'];
            $client_id = $_POST['custom'];
            $txn_id = $_POST['txn_id'];
            $client_email = $_POST['payer_email'];
            $receiver = $_POST['receiver_email'];
            $first_name = $_POST['first_name'];
            $last_name = $_POST['last_name'];
            $currency = $_POST['mc_currency'];
            $paid_amount = $_POST['mc_gross'];
            //$client_details = client_company($client_id);

            $transaction = array(
                'invoice_id' => $invoice_id,
                'trans_id' => $txn_id,
                'amount' => $paid_amount,
                'payment_method' => 1,
                'ip' => $this->input->ip_address(),
                'notes' => 'Paid by ' . $first_name . ' ' . $last_name . ' to ' . $receiver . ' via Paypal',
                'creator_email' => $client_email,
                'created_by' => $client_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );


            if ($this->paypal_m->add_payment($transaction)) {
                $this->notifications->send_payment_notification($this->CompanyClient_id, $invoice_id, $paid_amount);
            }

            if (DEBUG == true) {
                log_message('INFO', "Verified IPN: $req " . PHP_EOL);
            }
        } else {
            if (strcmp($res, "INVALID") == 0) {

                if (DEBUG == true) {
                    log_message('error', "Invalid IPN: $req" . PHP_EOL);
                }
            }
        }
    }

}