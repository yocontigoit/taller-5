<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/01
 * Time: 02:52 PM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                        class="fa fa-times"></i></span><span class="sr-only"><?php echo "close"; ?></span></button>
            <h4 class="modal-title"><?php echo "Purchase Summary"; ?></h4>
        </div>

        <?php echo form_open($paypal_url, array('id' => 'paypal_form', 'class' => 'form-horizontal')); ?>
        <input name="rm" value="2" type="hidden">
        <input name="cmd" value="_xclick" type="hidden">
        <input name="currency_code" value="<?php $invoice_info->currency; ?>" type="hidden">
        <input name="quantity" value="1" type="hidden">
        <input name="business" value="<?php echo $this->config->item('paypal')->paypal_mail; ?>" type="hidden">
        <input name="return" value="<?php echo base_url($this->config->item('paypal')->paypal_success); ?>"
               type="hidden">
        <input name="cancel_return" value="<?php echo base_url($this->config->item('paypal')->paypal_cancel); ?>"
               type="hidden">
        <input name="notify_url" value="<?php echo base_url($this->config->item('paypal')->paypal_ipn); ?>"
               type="hidden">
        <input name="custom" value="<?php echo $this->CompanyClient_id; ?>" type="hidden">
        <input name="item_name" value="<?php echo $invoice_info->item_name; ?>" type="hidden">
        <input name="item_number" value="<?php echo $invoice_info->item_number; ?>" type="hidden">
        <input name="amount" value="<?php echo format_amount($invoice_info->amount) ?>" type="hidden">

        <div class="modal-body">
            <table class="table">

                <tbody>
                <tr>
                    <th>Reference</th>
                    <td><input type="text" class="form-control" readonly
                               value="<?php echo $invoice_info->item_name; ?>"/></td>
                </tr>
                <tr>
                    <th>Amount</th>
                    <td><input type="text" class="form-control" readonly
                               value="<?php echo format_amount($invoice_info->amount); ?>"/></td>
                </tr>

                </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <input type="image" src="<?php echo base_url('files/gateways/paypal/paypal.png'); ?>" name="submit"
                   title="Pay With PayFast" alt="">
        </div>
        </form>

    </div>
</div>