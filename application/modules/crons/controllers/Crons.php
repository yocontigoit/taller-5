<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Crons extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library(array('email'));
        $this->load->helper('curl', 'file');
        $this->load->model('crons/cron_m');
        $this->invoices_table = 'invoice';
        $this->company_id = 1;
    }

    function run()
    {

        if ($this->config->item('system')->is_cron_active == 1) {

            if ($this->config->item('system')->create_recur == 1) {
                $this->_run_recur();
            }
            if ($this->config->item('system')->email_overdue == 1) {
                $this->_invoices_cron();
            }

            if ($this->config->item('system')->auto_db_backup == 1) {

                $this->_database_backup();
            }
        }

    }


    function _run_recur()
    {

        // Gather a list of recurring invoices to generate
        $invoices_recurring = $this->cron_m->active();

        foreach ($invoices_recurring as $invoice_recurring) {
            $target_id = null;
            // This is the original invoice id
            $source_id = $invoice_recurring->id;

            // This is the original invoice
            $invoice = $this->cron_m->get_invoice($source_id, $this->invoices_table);
            if ($this->config->item('invoice')->invoice_prefix != '') {
                $inv_no = $this->config->item('invoice')->invoice_prefix . "-" . trans_reference(10, $this->company_id);
            } else {
                break;
            }
            // Create the new invoice
            $db_array = array(
                'client_id' => $invoice->client_id,
                'invoice_due' => $this->cron_m->get_date_due($invoice_recurring->recur_next_date),
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d'),
                'status' => 2,
                'inv_no' => $inv_no,
                'currency' => $invoice->currency,
                'notes' => $invoice->notes
            );

            // This is the new invoice id
            $this->db->insert($this->invoices_table, $db_array);
            $target_id = $this->db->insert_id();

            //update next trans no...
            $reference_no = explode("-", $inv_no);
            $this->db->update('settings_systypes', array('typeno' => $reference_no[1]),
                array('typeid' => 10, 'company_id' => $this->company_id));

            // Copy the original invoice to the new invoice
            $this->cron_m->copy_invoice($source_id, $target_id);

            // Update the next recur date for the recurring invoice
            $this->cron_m->set_next_recur_date($source_id);

            // Email the new invoice if applicable
            if ($this->config->item('system')->email_recur == 1) {
                $data['invoice_details'] = $this->cron_m->client_invoice($target_id);
                $data['invoice_items'] = $this->cron_m->get_invoice_items($target_id);
                $this->notifications->send_new_invoice_notification($target_id, $data);

            }
        }
    }


    function _invoices_cron()
    {
        $invoices = $this->cron_m->overdue_invoices();
        if ($invoices) {
            foreach ($invoices as $invoice) {
                $this->notifications->send_invoice_reminder_notification($invoice->id);
            }

            return true;
        } else {
            log_message('error', 'There are no overdue invoices to send emails');

            return true;
        }

    }

    function _database_backup()
    {
        $this->load->helper('file');
        $this->load->dbutil();
        $prefs = array('format' => 'zip', 'filename' => 'Database-auto-full-backup_' . date('Y-m-d_H-i'));
        $backup = $this->dbutil->backup($prefs);

        if (!write_file('./files/backup/Database-auto-full-backup_' . date('Y-m-d_H-i') . '.zip', $backup)) {
            log_message('error', "Error while creating auto database backup!");
        } else {
            log_message('error', "Auto backup has been created.");

        }

    }


}
