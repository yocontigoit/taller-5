<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cron_M extends CI_Model
{

    public $table = 'fx_invoices';
    public $primary_key = 'fx_invoices.inv_id';
    public $recur_frequencies = array(
        '7D' => 'calendar_week',
        '1M' => 'calendar_month',
        '1Y' => 'year',
        '3M' => 'quarter',
        '6M' => 'six_months'
    );


    function overdue_projects()
    {
        $this->db->join('companies', 'companies.co_id = projects.client');
        $query = $this->db->select('company_email')->where(array(
            'proj_deleted' => 'No',
            'due_date' => date("Y-m-d")
        ))->get(Applib::$projects_table);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function overdue_invoices()
    {

        $this->db->where('status <> 1');
        $this->db->where('invoice_due <=  NOW()');
        $invoices = $this->db->get('invoice')->result();

        return $invoices;

    }


    public function stop($invoice_recurring_id)
    {
        $db_array = array(
            'recurring' => 'No',
            'recur_end_date' => date('Y-m-d'),
            'recur_next_date' => '0000-00-00'
        );

        $this->db->where('inv_id', $invoice_recurring_id);
        $this->db->update('fx_invoices', $db_array);

        return true;
    }

    /**
     * Sets filter to only recurring invoices which should be generated now
     * @return To generate invoices..
     */
    public function active()
    {

        $query = $this->db->query("SELECT * FROM invoice  WHERE recur_next_date <= date(NOW()) AND (recur_end_date > date(NOW()) OR recur_end_date = '0000-00-00') AND recur_start_date <= date(NOW()) AND is_recurring = 1")->result();

        return $query;
    }

    function get_invoice($source_id, $invoice_table)
    {
        return $this->db->where('id', $source_id)->get($invoice_table)->row();
    }

    public function set_next_recur_date($invoice_id)
    {
        $invoice_recurring = $this->db->where('id', $invoice_id)->get('invoice')->row();

        $recur_next_date = $this->_increment_date($invoice_recurring->recur_next_date,
            $invoice_recurring->recur_frequency);

        $db_array = array(
            'recur_next_date' => $recur_next_date
        );

        $this->db->where('id', $invoice_id);
        $this->db->update('invoice', $db_array);
    }

    /**
     * Adds interval to yyyy-mm-dd date and returns in same format
     * @param $date
     * @param $increment
     * @return date
     */
    function _increment_date($date, $increment)
    {
        $new_date = new DateTime($date);
        $new_date->add(new DateInterval('P' . $increment));

        return $new_date->format('Y-m-d');
    }

    public function copy_invoice($source_id, $target_id)
    {

        $invoice_items = $this->db->where('invoice_id', $source_id)->get('invoice_item')->result();

        foreach ($invoice_items as $invoice_item) {

            $item_array = array(
                'invoice_id' => $target_id,
                'item_name' => $invoice_item->item_name,
                'item_desc' => $invoice_item->item_desc,
                'discount' => $invoice_item->discount,
                'tax' => $invoice_item->tax,
                'quantity' => $invoice_item->quantity,
                'price' => $invoice_item->price,
            );

            $this->db->insert('invoice_item', $item_array);
        }
    }

    public function get_date_due($invoice_date_created)
    {
        $invoice_date_due = new DateTime($invoice_date_created);
        $invoice_date_due->add(new DateInterval('P' . $this->config->item('invoice')->invoice_due_after . 'D'));

        return $invoice_date_due->format('Y-m-d');
    }

    function  client_invoice($inv_id)
    {

        $this->db->from('invoice');
        $this->db->where("id", $inv_id);

        return $this->db->get()->row();
    }

    function  get_invoice_items($id = null)
    {

        $this->db->from('invoice_item');
        $this->db->where("invoice_id", $id);

        return $this->db->get()->result();
    }
}

/* End of file cron_model.php */