<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/03/16
 * Time: 10:20 AM
 */
?>
<div class="row">
    <div class="noaccess-sec">
        <div class="noaccess-page">
            <h1>No Access</h1>

            <h2>You are not allowed to access the requested page</h2>
            <input type="text" placeholder="Search Here"/>
            <ul>
                <li><a href="#" title="">Go Home</a></li>
                <li><a href="#" title="">Go Back</a></li>
            </ul>
            <p>Contact your system administrator if you have they have been a mistake</p>
        </div>
    </div>
    <!-- Error Sec -->
</div>
