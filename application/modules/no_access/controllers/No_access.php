<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/01/16
 * Time: 10:17 AM
 */
class No_Access extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();
    }


    function index($module_id = '')
    {
        $this->data['content'] = 'no_access/index';
        $this->data['title'] = 'No hay acceso';
        $this->load->view('_main_layout', $this->data);
    }


}