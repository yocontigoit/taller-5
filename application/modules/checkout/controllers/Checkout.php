<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/06/03
 * Time: 03:11 PM
 */
class Checkout extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("checkout_m");
    }

    function add_payment($invoice_id = null)
    {

        $reference_no = get_invoice_reference_no($invoice_id);
        $invoice_currency = get_invoice_currency($invoice_id);
        $invoice_amount = invoice_balance($invoice_id);

        if ($invoice_amount <= 0) {
            $invoice_amount = 0.00;
        }
        $invoice_info = new stdClass();
        $invoice_info->item_name = $reference_no;
        $invoice_info->item_number = $invoice_id;
        $invoice_info->amount = $invoice_amount;
        $invoice_info->currency = $invoice_currency;

        $this->data['invoice_info'] = $invoice_info;

        $this->data['title'] = 'Agregar Pago';

        $this->load->view('modal/add_payment', $this->data);
    }

    function process()
    {

        if ($this->input->post()) {
            $errors = array();
            $invoice_id = $this->input->post('invoice_id');
            if (!isset($_POST['token'])) {
                $errors['token'] = 'The order cannot be processed. Please make sure you have JavaScript enabled and try again.';
            }
            // If no errors, process the order:
            if (empty($errors)) {
                require_once(APPPATH . 'libraries/2checkout/Twocheckout.php');

                Twocheckout::privateKey($this->config->item('checkout')->checkout_private_key);
                Twocheckout::sellerId($this->config->item('checkout')->checkout_seller_id);
                Twocheckout::sandbox(($this->config->item('checkout')->checkout_active == 1) ? false : true);

                // Twocheckout::verifySSL(false);


                $reference_no = get_invoice_reference_no($invoice_id);
                $invoice_currency = get_invoice_currency($invoice_id);
                $client_details = get_client_details($this->CompanyClient_id);

                try {
                    //print_r($_POST);
                    //exit;
                    $charge = Twocheckout_Charge::auth(array(
                        "merchantOrderId" => $invoice_id,
                        "token" => $_POST['token'],
                        "currency" => $invoice_currency,
                        "total" => $this->input->post('amount'),
                        "billingAddr" => array(
                            "name" => $client_details->client_name,
                            "addrLine1" => $client_details->client_address_1,
                            "city" => $client_details->client_city,
                            "state" => $client_details->client_state,
                            "zipCode" => $client_details->client_zip,
                            "country" => $client_details->client_country,
                            "email" => $client_details->client_email,
                            "phoneNumber" => $client_details->client_phone_no
                        )
                    ));

                    if ($charge['response']['responseCode'] == 'APPROVED') {

                        $data = array(
                            'invoice_id' => $invoice_id,
                            'trans_id' => $charge['response']['merchantOrderId'],
                            'amount' => $charge['response']['total'],
                            'payment_method' => 5,
                            'ip' => $this->input->ip_address(),
                            'notes' => 'Paid by ' . $this->CompanyUser_FullName . ' via 2Checkout | Invoice Currency:' . $invoice_currency,
                            'creator_email' => $charge['response']['billingAddr']['email'],
                            'created_by' => $client_details->client_name,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        );


                        if ($this->checkout_m->add_payment($data)) {


                            $this->notifications->send_payment_notification($this->CompanyClient_id, $invoice_id,
                                $charge['response']['total']);


                            $this->session->set_flashdata('msg_status', 'success');
                            $this->session->set_flashdata('message',
                                'Payment received and applied to Invoice ' . $reference_no);
                            redirect('billing/invoice/' . $invoice_id);

                        } else {
                            $this->session->set_flashdata('msg_status', 'success');
                            $this->session->set_flashdata('message',
                                'Payment not recorded in the database. Please contact the system Admin.');
                            redirect('billing/invoice/' . $invoice_id);
                        }

                    }
                } catch (Twocheckout_Error $e) {
                    $this->session->set_flashdata('msg_status', 'error');
                    $this->session->set_flashdata('message', 'Payment declined with error: ' . $e->getMessage());
                    redirect('billing/invoice/' . $invoice_id);
                }
            }
        }
    }


}
