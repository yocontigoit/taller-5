<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 6/8/2015
 * Time: 6:55 PM
 */
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="https://www.2checkout.com/checkout/api/2co.min.js"></script>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Due Amount
                <strong>
                    <?php echo $invoice_info->currency . " " . number_format($invoice_info->amount, 2); ?>
                </strong> for Invoice #<?php echo $invoice_info->item_name; ?> via 2Checkout</h4>
        </div>
        <div class="modal-body">

            <?php echo form_open('checkout/process',
                array('id' => '2checkout', 'class' => 'bs-example form-horizontal')); ?>

            <?php // Show PHP errors, if they exist:
            if (isset($errors) && !empty($errors) && is_array($errors)) {
                echo '<div class="alert alert-error"><h4>Error!</h4>The following error(s) occurred:<ul>';
                foreach ($errors as $e) {
                    echo "<li>$e</li>";
                }
                echo '</ul></div>';
            } ?>

            <div id="payment-errors"></div>
            <input type="hidden" name="invoice_id" value="<?php echo $invoice_info->item_number; ?>">
            <input id="token" name="token" type="hidden">

            <div class="form-group">
                <label class="col-lg-4 control-label"><?= lang('amount') ?></label>

                <div class="col-lg-5">
                    <input type="text" class="form-control input-medium" name="amount" autocomplete="off"
                           value="<?php echo number_format($invoice_info->amount, 2) ?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label">Card Number</label>

                <div class="col-lg-5">
                    <input type="text" id="ccNo" size="20" class="form-control card-number input-medium"
                           autocomplete="off" placeholder="5555555555554444" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label">CVC</label>

                <div class="col-lg-2">
                    <input type="text" id="cvv" size="4" class="form-control card-cvc input-mini" autocomplete="off"
                           placeholder="123" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label">Expiration (MM/YYYY)</label>

                <div class="col-lg-2">
                    <input type="text" size="2" id="expMonth" class="form-control input-mini" autocomplete="off"
                           placeholder="MM" required>

                </div>
                <div class="col-lg-2">
                    <input type="text" size="4" id="expYear" class="form-control input-mini" placeholder="YYYY"
                           required>
                </div>
            </div>

            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></a>
                <input type="image" src="<?php echo base_url('files/gateways/checkout/checkout.png'); ?>" name="submit"
                       title="Pay With checkout" alt="">
            </div>

            </form>


        </div>


    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->


<script>
    // Called when token created successfully.
    var successCallback = function (data) {
        var myForm = document.getElementById('2checkout');

        // Set the token as the value for the token input
        myForm.token.value = data.response.token.token;

        // IMPORTANT: Here we call `submit()` on the form element directly instead of using jQuery to prevent and infinite token request loop.
        myForm.submit();
    };

    // Called when token creation fails.
    var errorCallback = function (data) {
        if (data.errorCode === 200) {
            tokenRequest();
        } else {
            alert(data.errorMsg);
        }
    };

    var tokenRequest = function () {
        // Setup token request arguments
        var args = {
            sellerId: "<?php echo $this->config->item('checkout')->checkout_seller_id; ?>",
            publishableKey: "<?php echo $this->config->item('checkout')->checkout_public_key; ?>",
            ccNo: $("#ccNo").val(),
            cvv: $("#cvv").val(),
            expMonth: $("#expMonth").val(),
            expYear: $("#expYear").val()
        };

        // Make the token request
        TCO.requestToken(successCallback, errorCallback, args);
    };

    $(function () {
        // Pull in the public encryption key for our environment
        <?php $twocheckout_status = ($this->config->item('checkout')->checkout_active == 1) ? 'production' : 'sandbox'; ?>
        TCO.loadPubKey('<?php echo $twocheckout_status; ?>');

        $("#2checkout").submit(function (e) {
            // Call our token request function
            tokenRequest();

            // Prevent form from submitting
            return false;
        });
    });
</script>
