<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/01/22
 * Time: 10:18 AM
 */
?>
<fieldset>


    <ul class="nav nav-tabs mt-10" role="tablist">
        <li class="active"><a href="#estimate" role="tab" data-toggle="tab"><i
                    class="fa fa-tasks"></i><strong></strong> Nueva Orden</stong></a></li>

    </ul>

    <div class="tab-content">
        <!--Start adding new estimate -->
        <div class="tab-pane fade in active" id="estimate">

            <?php echo form_open('estimate/create_estimate',
                array('class' => 'panel form-horizontal form-bordered', 'id' => 'add-estimate')); ?>
            <div class="panel-body">
                <div class="form-group header bgcolor-default">
                    <div class="col-md-12">
                        <h4>Nueva Orden</h4>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tipo de Orden</label>

                    <div class="col-sm-8">

                        <select name="filter" id="filter" onchange="changeValue();" data-placeholder="Compra o trabajo" class="form-control  chosen-select" tabindex="4"
                                name="client">
                            <option id="0" value="0">Seleccionar...</option>
                            <option id="1" value="1">Compra</option>
                            <option id="2" value="2">Trabajo</option>
                        </select>

                    </div>
                </div>
                <script type="text/javascript">
                function changeValue(){
                    var option=document.getElementById('filter').value;

                    if(option=="1"){
                            document.getElementById('est_number').value="COM-0015";
                    }
                        else if(option=="2"){
                            document.getElementById('est_number').value="TRA-0016";
                        }

                }
                </script>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo lang('label_estimate_no'); ?></label>

                    <div class="col-sm-8">
                        <input class="form-control" type="text" id="est_number" name="est_number"
                               value=""/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Obra</label>

                    <div class="col-sm-8">

                        <select data-placeholder="Select client..." class="form-control  chosen-select" tabindex="4"
                                name="project">
                            <option value="Empty">&nbsp;</option>
                            <optgroup label="">
                                <?php if (count($projects)):foreach ($projects as $client): ?>
                                    <option
                                        value="<?php echo $client->project_id; ?>"><?php echo ucfirst($client->project_title) ?></option>
                                <?php endforeach; endif; ?>
                            </optgroup>


                        </select>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Proveedor/Trabajador</label>

                    <div class="col-sm-8">

                        <select data-placeholder="Select client..." class="form-control  chosen-select" tabindex="4"
                                name="client">
                            <option value="Empty">&nbsp;</option>

                            <optgroup label="Proveedores">
                                <?php if (count($clients)):foreach ($clients as $client): ?>
                                    <option
                                        value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name) ?></option>
                                <?php endforeach; endif; ?>
                            </optgroup>
                            <optgroup label="Trabajadores">
                                <?php if (count($clients)):foreach ($clients as $client): ?>
                                    <option
                                        value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name) ?></option>
                                <?php endforeach; endif; ?>
                            </optgroup>
                        </select>
<a href="http://quattrosol.com/dci/client/add">Nuevo Proveedor</a>
                    </div>
                </div>



                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo lang('label_estimate_start'); ?></label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" name="start_date"
                               data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                               value="<?php echo date('d-m-Y') ?>">
                    </div>
                </div>


            </div>
            <hr/>
            <button type="input" name="submit" class="btn btn-success btn-lg btn-icon mt-10"><i
                    class="fa fa-check-square-o"></i> <?php echo lang('form_button_add_estimate'); ?></button>
            </form>
        </div>

    </div>


</fieldset>
