<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/21
 * Time: 12:18 PM
 */
?>
<?php if ($this->CompanyUserRolePermission_id == 1) { ?>
    <div class="table-toolbar">
        <div class="btn-group">

            <?php echo anchor('estimate/create_estimate',  '<h4>Nueva Orden <i class="fa fa-plus"> </i></h4> ',
                array('class' => 'btn btn-info')); ?>



        </div>
    </div>
<?php } ?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
           id="zest_table">
        <thead class="the-box dark full">
        <tr>

            <th><?php echo lang("label_estimate_status"); ?></th>
            <th><?php echo lang("label_estimate_reference"); ?></th>
            <th><?php echo lang("label_estimate_created"); ?></th>
            <th><?php echo lang("label_estimate_due"); ?></th>
            <th>Proveedor</th>
            <th><?php echo lang("label_estimate_amount"); ?></th>
            <th><?php echo lang("label_estimate_options"); ?></th>
        </tr>
        </thead>
        <tbody>

        <?php if (count($estimates)) : foreach ($estimates as $estimate) :
            $estimate_status = get_estimate_status_name($estimate->est_status);
            if ($estimate_status->status_name == 'EN REVISION') {
                $label = "success";
            } elseif ($estimate_status->status_name == 'PAGO PENDIENTE') {
                $label = "success";
            } else {
                $label = "success";
            }
            ?>
            <tr>
    <td><span class="label label-<?php echo $label;?>"><?php echo $estimate_status->status_name; ?></span>
                </td>
                <td><?php echo $estimate->est_code; ?></td>
                <td><?php echo $estimate->est_trans_date; ?></td>
                <td><?php echo $estimate->est_due_date; ?></td>
                <td><?php echo client_company($estimate->client_id); ?></td>
                <td><?php echo format_currency(calculate_estimate_total($estimate->est_id)); ?></td>
                <td class="text-center">


                    <div class="btn-group-action">
                        <div class="btn-group">
                            <a class=" btn btn-info"
                               href="<?php echo site_url('estimate/preview/' . $estimate->est_id . '/yes'); ?>"
                               title=""><i class="fa fa-eye"></i>Ver Orden</a>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret">&nbsp;</span>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <?php if ($estimate_status->status_name == 'PAGADO' && $this->CompanyUserRolePermission_id != 3) { ?>
                                    <li><a href="<?php echo site_url('estimate/convert/' . $estimate->est_id); ?>"><i
                                                class="fa fa-quill2"></i>Convertir a pedido</a></li>
                                <?php } ?>
                                <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                                    <?php if ($estimate_status->status_name != 'PAGO PENDIENTE') { ?>
                                        <li><a href="<?php echo base_url('estimate/update/' . $estimate->est_id); ?>"><i
                                                    class="fa fa-pencil"></i> Editar</a></li>
                                    <?php } ?>
                                    <li><a href="<?php echo base_url('estimate/email/' . $estimate->est_id); ?>"><i
                                                class="fa fa-send"></i> Correo</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>

                </td>
            </tr>
        <?php endforeach; endif; ?>
        </tbody>
    </table>
</div>
