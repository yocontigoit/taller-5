<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 1/22/2016
 * Time: 6:29 PM
 */
?>
<fieldset>
    <legend><i class="fa fa-bar-chart-o"></i><?php echo strtoupper(lang('label_add_estimate')); ?></legend>


    <ul class="nav nav-tabs mt-10" role="tablist">
        <li class="active"><a href="#estimate" role="tab" data-toggle="tab"><i
                    class="fa fa-tasks"></i><?php echo strtoupper(lang('label_add_estimate')); ?></a></li>

    </ul>

    <div class="tab-content">
        <!--Start adding new estimate -->
        <div class="tab-pane fade in active" id="estimate">

            <?php echo form_open('estimate/create_estimate',
                array('class' => 'panel form-horizontal form-bordered', 'id' => 'add-estimate')); ?>
            <input class="form-control" type="hidden" name="quote_id" value="<?php echo $quote_id; ?>"/>

            <div class="panel-body">
                <div class="form-group header bgcolor-default">
                    <div class="col-md-12">
                        <h4><?php echo lang('label_est_detail_setting'); ?></h4>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo lang('label_estimate_no'); ?></label>

                    <div class="col-sm-8">
                        <input class="form-control" type="text" required="" name="est_number"
                               value="<?php echo $est_ref; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo lang('label_project_client'); ?></label>

                    <div class="col-sm-8">

                        <select data-placeholder="Select client..." class="form-control" tabindex="4" name="client">
                            <option value="Empty">&nbsp;</option>

                            <optgroup label="<?php echo lang("label_clients") ?>">
                                <?php if (count($clients)):foreach ($clients as $client): ?>
                                    <?php if ($quote->client_id == $client->client_id): ?>
                                        <option selected
                                                value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name); ?></option>
                                    <?php else: ?>
                                        <option
                                            value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name); ?></option>
                                    <?php endif; ?>
                                <?php endforeach; endif; ?>
                            </optgroup>
                        </select>

                    </div>
                </div>

                <div class="form-group header bgcolor-default mt-20">
                    <div class="col-md-12">
                        <h4><?php echo lang('label_est_setting'); ?></h4>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo lang('label_estimate_priority'); ?></label>

                    <div class="col-sm-8">
                        <select class="form-control" name="estimate_priority">
                            <option value="...">Seleccionar...</option>
                            <?php
                            $estimate_priorities = task_priorities();
                            foreach ($estimate_priorities as $priority) { ?>
                                ?>
                                <option
                                    value="<?php echo $priority->priority_id; ?>"><?php echo ucfirst(strtolower($priority->priority_name)); ?></option>

                            <?php } ?>
                        </select>
                    </div>
                </div>


                <div class="form-group">

                    <div class="col-sm-3 control-label">
                        <label><?php echo lang('label_is_visible'); ?></label>
                    </div>


                    <div class="col-sm-8">
                        <input type="checkbox" name="estimate_visible"
                               class="ios-switch ios-switch-success ios-switch-lg"/>
                    </div>

                </div>


                <div class="form-group header bgcolor-default mt-10">
                    <div class="col-md-12">
                        <h4><?php echo lang('label_est_date_setting'); ?></h4>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo lang('label_estimate_start'); ?></label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" name="start_date"
                               data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                               value="<?php echo date('d-m-Y') ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo lang('label_estimate_end_date'); ?></label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" name="end_date" data-date-format="dd-mm-yyyy"
                               placeholder="dd-mm-yyyy" value="<?php echo date('d-m-Y') ?>">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo lang('label_estimate_note'); ?></label>

                    <div class="col-sm-8">
                        <textarea class="form-control rounded" name="est_notes" style="height:100%"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-9 col-lg-offset-3">
                        <button type="input" name="submit" class="btn btn-success btn-lg btn-icon"><i
                                class="fa fa-check-square-o"></i> <?php echo lang('form_button_add_estimate'); ?>
                        </button>
                    </div>
                </div>
            </div>


            </form>
        </div>
        <!--End adding new estimate -->


    </div>


</fieldset>
