<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/21
 * Time: 12:06 PM
 */
?>

<?php echo form_open('estimate/preview/' . $estimate->est_id,
    array('class' => 'form-horizontal', 'id' => 'add-estimate')); ?>

<?php
if ($estimate->est_invoiced == 1) {
    $display = 'disabled="disabled"';
} else {
    $display = '';
    $editable = '';
}
?>
<?php
$customer_details = get_client_details($estimate->client_id);

$estimate_status = get_estimate_status_name($estimate->est_status);
if ($estimate_status->status_name == 'PENDING') {
    $label = "info";
} elseif ($estimate_status->status_name == 'ACCEPTED') {
    $label = "success";
} else {
    $label = "danger";
}
?>
<?php

if (!$this->session->flashdata('message')) {
    if (strtotime($estimate->est_due_date) < time() AND $estimate_status->status_name == 'PENDING') { ?>
        <div class="alert alert-warning alert-block fade in alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Pendiente!</strong><?php echo lang('messages_estimate_overdue'); ?>
        </div>
    <?php }
} ?>
<div class="the-box full invoice">
    <div class="row">
        <div class="col-md-12">
            <div class="the-box toolbar no-border no-margin">

                <?php //@TODO: if invoice show different buttons ?>
                <div class="btn-toolbar" role="toolbar">
                    <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                        <div class="btn-group pull-left">

                            <a class="btn btn-block btn-info"
                               href="<?php echo base_url('estimate/update/' . $estimate->est_id); ?>">
                                <i class="fa fa-plus"></i>
                                Editar Datos
                            </a>

                        </div>
                    <?php } ?>
                    <div class="btn-group pull-right">

                        <a class="btn btn-block btn-info"
                           href="<?php echo base_url('estimate/print_estimate/' . $estimate->est_id); ?>">
                            <i class="fa fa-print"></i>
                            Crear PDF
                        </a>


                    </div>
                    <?php if ($estimate_status->status_name != 'INVOICED' && $this->CompanyUserRolePermission_id != 1): ?>
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-tag"></i>
                                Mas Opciones
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <?php if ($estimate_status->status_name == 'PENDING' && $estimate_status->status_name != 'INVOICED') : ?>
                                    <li>
                                        <a href="<?php echo base_url('estimate/status/2/' . $estimate->est_id); ?>">Confirmar</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('estimate/status/3/' . $estimate->est_id); ?>">Denegar</a>
                                    </li>
                                <?php endif; ?>
                                <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
                                    <li class="divider"></li>

                                    <li><a href="<?php echo base_url('estimate/email/' . $estimate->est_id); ?>"><i
                                                class="fa fa-send fa-margin"></i>Enviar Correo</a></li>

                                <?php } ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <?php if ($this->CompanyUserRolePermission_id == 1 && $estimate->est_invoiced == 0) { ?>
                        <div class="btn-group pull-right">

                            <a class="btn btn-small btn-success btn-preview-invoice"
                               href="<?php echo base_url('estimate/convert/' . $estimate->est_id); ?>" <?php echo $display; ?>>
                                <i class="fa fa-plus"></i>
                                Hacer pedido
                            </a>


                        </div>

                    <?php } ?>

                </div>
                <!-- /.btn-toolbar -->
            </div>

        </div>
    </div>
    <!-- New invoice template -->
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row invoice-header">
                <div class="col-sm-6">
                    <h3><?php echo $this->config->item('company')->company_name; ?></h3>
                    <span><?php echo $this->config->item('company')->company_slogan; ?></span>
                </div>

                <div class="col-sm-6">
                    <ul class="invoice-details">
                        <li><?php echo lang('label_estimate_no'); ?># <strong
                                class="text-danger"><?php echo $estimate->est_code; ?></strong></li>
                        <li><?php echo lang('label_estimate_date'); ?>: <strong><?php echo date('Y-m-d',
                                    strtotime($estimate->est_trans_date)); ?></strong></li>
                        <li><?php echo lang('label_estimate_due'); ?>: <strong><?php echo date('Y-m-d',
                                    strtotime($estimate->est_due_date)); ?></strong></li>
                    </ul>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-5">
                    <h5><strong><?php echo lang('label_estimate_from'); ?>:</strong></h5>
                    <ul>
                        <li><?php echo lang('label_company_name'); ?>: <strong><a
                                    href="#"><?php echo $this->config->item('company')->company_name; ?></a></strong>
                        </li>
                        <li><?php echo lang('label_location_address_1'); ?>: <strong><a
                                    href="#"><?php echo $this->config->item('company')->company_address_1; ?></a></strong>
                        </li>
                        <li><?php echo lang('label_location_address_2'); ?>: <strong><a
                                    href="#"><?php echo $this->config->item('company')->company_address_2; ?></a></strong>
                        </li>
                        <li><?php echo lang('label_email'); ?>:<strong> <a
                                    href="#"><?php echo $this->config->item('company')->company_email; ?></a></strong>
                        </li>
                        <li><?php echo lang('label_phone'); ?>: <strong><a
                                    href="#"><?php echo $this->config->item('company')->company_telephone; ?></a></strong>
                        </li>
                        <li>Código Fiscal: <strong><a
                                    href="#"><?php echo $this->config->item('company')->company_vat_no; ?></a></strong>
                        </li>
                    </ul>
                </div>


                <div class="col-sm-3">
                    <h5><strong><?php echo lang('label_estimate_to'); ?>:</strong></h5>
                    <ul>

                        <li><a href="#"><?php echo lang('label_estimate_company'); ?>: <strong
                                    class="pull-right"><?php echo $customer_details->client_name; ?></strong></a></li>
                        <li><?php echo lang('label_city'); ?>: <a href="#"
                                                                  class="pull-right"><?php echo $customer_details->client_city; ?></a>
                        </li>

                        <li><?php echo lang('label_phone'); ?>: <strong
                                class="pull-right"><?php echo $customer_details->client_phone_no; ?></strong></li>
                        <li>Código Fiscal: <strong
                                class="pull-right"><?php echo $customer_details->client_vat_no; ?></strong></li>
                        <li class="invoice-status"><strong><?php echo lang('label_status'); ?>:</strong>

                            <div
                                class="label label-<?php echo $label; ?> pull-right"><?php echo $estimate_status->status_name; ?></div>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="table-responsive">
            <table id="item_table" class="table table-striped table-bordered">
                <thead>
                <tr class="blueheader">

                    <th><?php echo lang('label_est_description'); ?></th>

                    <th>Descripción</th>
                    <th><?php echo lang('label_est_item_quantity'); ?></th>
                    <th><?php echo lang('label_est_item_rate'); ?></th>
                    <th><?php echo lang('label_estimate_discount'); ?></th>
                    <th><?php echo lang('label_tax'); ?></th>
                    <th><?php echo lang('label_amount'); ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody class="solsoParent">
                <tr style="display: none !important; " class="updatesolsoChild">
                    <input type="hidden" name="item_id" value="0"/>
                    <td style="display: none !important; " class="crt">1</td>

                    <td class="option row" style="text-align:left;" width="20%">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group col-md-12">
                                                    <span class="input-group-addon warning">
                                                    <a titel="Add custom item" data-toggle="zestModal"
                                                       href="<?php echo base_url('item/add_modal_item/' . $estimate->est_id); ?>"><i
                                                            class="fa fa-plus"></i></a>
                                                    </span>


                                    <select name="item_name"
                                            class="form-control required solsoSelect2 solsoCloneSelect2">
                                        <option value="" selected>Elegir</option>

                                        <?php if (count($concepts)) : foreach ($concepts as $item) : ?>
                                            <option
                                                value="<?php echo $item->temp_name; ?>"> <?php echo $item->temp_name; ?> </option>
                                        <?php endforeach; endif; ?>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </td>

                    <td>
                        <input type="text" name="item_description" class="form-control required solsoEvent"
                               autocomplete="off">
                    </td>

                    <td>
                        <input type="text" name="item_hours_worked" class="form-control required solsoEvent"
                               autocomplete="off">
                    </td>

                    <td>
                        <input type="text" name="hourly_rate" class="form-control required solsoEvent"
                               autocomplete="off">
                    </td>

                    <td>
                        <input type="text" name="item_discount" class="form-control solsoEvent" autocomplete="off">
                    </td>
                    <td>
                        <select name="item_tax" class="form-control required solsoEvent">
                            <option value="" selected>no</option>
                            <option value="0">Si</option>


                        </select>
                    </td>
                    <td>
                        <h4 class="pull-right">
                            <input name="item_subtotal" value="0.00"
                                   class="item_subtotal no-border text-right form-control" readonly/>

                        </h4>
                    </td>

                    <td>
                        <button type="button" class="btn btn-danger disabled removeClone"><i class="fa fa-trash"></i>
                        </button>
                    </td>
                </tr>

                <?php $count = 0;
                if (count($families)) : foreach ($families as $item) : ?>

                    <tr <?php echo ($count == 0) ? 'class="solsoChild"' : ''; ?> >
                        <input type="hidden" name="item_id" value="<?php echo $item->fam_id; ?>"/>
                        <td style="display: none; " class="crt"><?php echo $count; ?></td>

                        <td class="option row" style="text-align:left;" width="20%">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group col-md-12">
                                                        <span class="input-group-addon warning">
                                                        <a titel="Add custom item" data-toggle="zestModal"
                                                           href="<?php echo base_url('item/add_modal_item/' . $estimate->est_id); ?>"><i
                                                                class="fa fa-plus"></i></a>
                                                        </span>


                                        <select name="item_name"
                                                class="form-control required solsoSelect2 solsoCloneSelect2">
                                            <option value="" selected>Elegir</option>

                                            <?php if (count($concepts)) : foreach ($concepts as $item) : ?>
                                                <option
                                                    value="<?php echo $item->concepto; ?>"> <?php echo $item->concepto; ?> </option>
                                            <?php endforeach; endif; ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="option row" style="text-align:left;" width="20%">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="input-group col-md-12">
                                        <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) { ?>
                                            <span class="input-group-addon warning">
                                                    <a titel="Add custom item" data-toggle="zestModal"
                                                       href="<?php echo base_url('item/add_modal_item/' . $estimate->est_id); ?>"><i
                                                            class="fa fa-plus"></i></a>
                                                    </span>

                                        <?php } ?>

                                        <select name="item_name"
                                                class="form-control required solsoSelect2 solsoCloneSelect2">
                                            <option value="">elegir</option>

                                            <?php if (count($items)) : foreach ($items as $product) : ?>
                                                <?php if ($item->temp_family == $product->temp_family) { ?>
                                                    <option selected
                                                            value="<?php echo $product->temp_name; ?>"> <?php echo $product->temp_name; ?> </option>
                                                <?php } else { ?>
                                                    <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) { ?>
                                                        <option
                                                            value="<?php echo $product->temp_name; ?>"> <?php echo $product->temp_name; ?> </option>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php endforeach; endif; ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </td>

  <?php if (count($items)) : foreach ($items as $item) : ?>
                        <td>
                            <input type="text" name="item_description" class="form-control required solsoEvent"
                                   value="<?php echo $item->temp_name; ?>" autocomplete="off">
                        </td>


                        <td>
                            <input type="text" name="item_hours_worked" value="<?php echo $item->temp_quantity; ?>"
                                   class="form-control required solsoEvent" placeholder="0" autocomplete="off">
                        </td>

                        <td>
                            <input type="text" name="hourly_rate" value="<?php echo $item->temp_cost; ?>"
                                   class="form-control required solsoEvent" placeholder="0" autocomplete="off">
                        </td>

                        <td>
                            <input type="text" name="item_discount" value=""
                                   class="form-control solsoEvent" placeholder="0" autocomplete="off">
                        </td>
  <?php endforeach; endif; ?>
                        <td>
                            <select name="item_tax" class="form-control required solsoEvent">
                                <option value="">No</option>
                                <option value="0">Si</option>
                            </select>
                        </td>

                        <td><input name="item_subtotal"
                                   value="<?php echo calculate_line_subtotal($item->temp_id); ?>"
                                   class="no-border text-right form-control" readonly/></td>
                        <td>
                            <button
                                class="btn btn-danger removeClone <?php echo ($estimate_status->status_name == 'INVOICED' || $this->CompanyUserRolePermission_id == 3) ? 'disabled' : ''; ?>"
                                data-id="<?php echo $item->temp_id; ?>"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                    <?php
                    $count++;
                endforeach;
                endif;
                ?>

            </table>


            <table class="table">
                <tr>
                    <td colspan="3" style="border-right-color: #ddd" width="70%"></td>
                    <td width="12%"><?php echo lang('label_sub_total'); ?><span class="pull-right"></span></td>
                    <td style="width:15%;" class="text-right"><input name="invoice_subtotal" id="invoice_subtotal"
                                                                     value="<?php echo format_currency(calculate_estimate_sub_total($estimate->est_id)); ?>"
                                                                     class="no-border text-right form-control"
                                                                     readonly/></td>
                    <td width="3%" class="borderless"></td>
                </tr>
                <tr>
                    <td colspan="3" style="border-color:#fff;border-right-color: #ddd" width="70%"></td>
                    <td width="12%"><?php echo lang('label_tax'); ?><span class="pull-right"></span></td>
                    <td style="width:15%;" class="text-right"><input name="tax_total" id="tax_total"
                                                                     value="<?php echo format_currency(calculate_estimate_tax_total($estimate->est_id)); ?>"
                                                                     class="no-border text-right form-control"
                                                                     readonly/></td>
                    <td width="3%" class="borderless"></td>
                </tr>
                <tr>
                    <td colspan="3" style="border-color:#fff;border-right-color: #ddd" width="70%"></td>
                    <td width="12%"><strong><?php echo lang('label_total'); ?></strong> <span class="pull-right"></span>
                    </td>
                    <td style="width:15%;" class="text-right"><strong><input name="invoice_total_amount"
                                                                             id="invoice_total_amount"
                                                                             value="<?php echo format_currency(calculate_estimate_total($estimate->est_id)); ?>"
                                                                             class="no-border text-right form-control"
                                                                             readonly/></strong></td>
                    <td width="3%" class="borderless"></td>
                </tr>
                </tbody>
            </table>
        </div>
        <?php if ($this->CompanyUserRolePermission_id == 1) { ?>
            <div class="panel-body">
                <div class="col-lg-4">
                    <a href="javascript:void(0)" id="btn_update_add_item" class="btn btn-small btn-danger"><i
                            class="fa fa-plus"></i> Agregar Fila</a>
                </div>

                <div class="col-lg-7 pull-right text-right" style="margin-right: 50px;">
                    <a href="javascript:void(0)" id="save_items" class="btn btn-small btn-info">Guardar</a>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php echo form_close(); ?>
