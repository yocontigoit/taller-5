<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/05/25
 * Time: 02:42 PM
 */
?>
<div class="the-box">
    <div class="row">
        <div class="col-lg-12">
<div class="col-lg-9">
    <!--Ticket details -->
    <div class="box">
        <!-- BEGIN PANEL-->
        <div class="panel panel-info panel-square panel-no-border">
            <div class="panel-heading">
                <h3 class="panel-title">Detalles de Concepto</h3>
            </div>

            <div class="panel-body text-center">
                <table class="table table-bordered table-hover table-extra-padd">
                    <tbody>
                    <tr>
                        <td class="col-sm-4">Fecha:</td>
                        <td class="col-sm-8"><?php echo $expense_details->date_entry; ?></td>
                    </tr>
                    <tr>
                        <td class="col-sm-4"><?php echo lang('label_category'); ?>:</td>
                        <td class="col-sm-8"><?php echo get_category_name($expense_details->classification); ?></td>
                    </tr>
                    <tr>
                        <td class="col-sm-4">Obra:</td>
                          <td class="col-sm-8">  <?php
                            $project_info = get_project_details($expense_details->project_id);

                               echo $project_info->project_title; ?></td>
                    </tr>
                    <tr>
                        <td class="col-sm-4">Importe:</td>
                        <td class="col-sm-8"><?php echo $expense_details->estimate_cost; ?></td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <div class="panel-footer">
                <p><strong>Recibo / Evidencia fisica :</strong></p>
                <?php if (empty($expense_details->receipt)) { ?>
                    <?php echo form_open_multipart('expenses/upload_image'); ?>
                    <input type="hidden" name="cost_id"
                           value="<?php echo $expense_details->cost_id; ?>"/>
                    <div class="input-group">
                        <input type="text" class="form-control" readonly>
                                            <span class="input-group-btn">
                                                <span class="btn btn-primary btn-file">
                                                    Buscar Archivo<input type="file" multiple
                                                                       name="expense_receipt">
                                                </span>
                                            </span>
                    </div><!-- /.input-group -->
                    <button style="float:right;" type="submit" class="btn btn-success"><i
                            class="fa fa-rocket"></i>subir</button>
                    <?php echo form_close(); ?>
                <?php } else { ?>
                    <ul class="attachment-list">
                        <li><?php echo $expense_details->receipt; ?><a
                                href="<?php echo base_url('expenses/download/' . $expense_details->cost_id); ?>"><i
                                    class="fa fa-cloud-download"></i></a></li>

                    </ul>
                <?php } ?>

            </div>
        </div>
        <!-- END PANEL -->

    </div>
    <!--end details-->
</div>
<div class="col-lg-3">
    <div class="row">
        <div class="col-sm-12">
            <div
                style="margin: auto; position: absolute; top: 0%; left: 40%; bottom: 0px; right: 0; transform: translate(-50% -50%);">
                <?php
                $total_expense = tot_expense_amount();
                if ($expense_details->real_cost > 0 && $total_expense > 0) {
                    $percentage = round(($expense_details->real_cost / $total_expense) * 100);
                } else {

                    $percentage = 0;
                }
                ?>
                <div class="chart easy-pie-expense text-danger"
                     data-percent="<?php echo $percentage; ?>" data-line-width="10" data-rotate="270"
                     data-scale-Color="false" data-size="120" data-animate="2000">
                    <span class="h2 step font-bold"><?php echo $percentage; ?></span>%

                    <div
                        class="easypie-text text-muted"><?php echo $expense_details->real_cost . "/" . $total_expense; ?></div>
                </div>
                <div class="font-bold m-t">Se ha gastado /Queda pendiente</div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>

    <div class="row" style="margin-top:40%;">
        <div class="col-lg-12">
            <div class="panel panel-danger panel-square panel-no-border text-center">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo lang('label_expense_amount'); ?></h3>
                </div>
                <div class="panel-body">
                    <h1 class="bolded tiles-number text-danger"><?php echo format_currency($expense_details->real_cost); ?></h1>
                </div>
            </div>

        </div>
    </div>

</div>
</div>
</div>

</div>
