<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 1/22/2016
 * Time: 6:29 PM
 */
?>
<fieldset>
    <legend><i class="fa fa-bar-chart-o"></i>Editar Estimado</legend>


    <ul class="nav nav-tabs mt-10" role="tablist">
        <li class="active"><a href="#estimate" role="tab" data-toggle="tab"><i
                    class="fa fa-tasks"></i><?php echo strtoupper(lang('label_edit_estimate')); ?></a></li>
        <li><a href="#notes" role="tab" data-toggle="tab"><i class="fa fa-comments"></i> <?php echo "ESTIMATE"; ?> <span
                    class="badge pull-right"><?php echo "NOTES"; ?></span></a></li>

    </ul>

    <div class="tab-content">

        <div class="tab-pane fade in active" id="estimate">

            <?php echo form_open('estimate/update/' . $estimate->est_id,
                array('class' => 'panel form-horizontal form-bordered', 'id' => 'add-estimate')); ?>
            <div class="panel-body">
                <div class="form-group header bgcolor-default">
                    <div class="col-md-12">
                        <h4><?php echo lang('label_est_detail_setting'); ?></h4>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo lang('label_estimate_no'); ?></label>

                    <div class="col-sm-8">

                        <input class="form-control" type="text" readonly name="est_number"
                               value="<?php echo $estimate->est_code; ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo lang('label_project_client'); ?></label>

                    <div class="col-sm-8">

                        <select data-placeholder="Select client..." class="form-control" tabindex="4" name="client">
                            <option value="Empty">&nbsp;</option>

                            <optgroup label="<?php echo lang("label_clients") ?>">
                                <?php if (count($clients)):foreach ($clients as $client): ?>
                                    <?php if ($estimate->client_id == $client->client_id): ?>
                                        <option selected
                                                value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name); ?></option>
                                    <?php else: ?>
                                        <option
                                            value="<?php echo $client->client_id; ?>"><?php echo ucfirst($client->client_name); ?></option>
                                    <?php endif; ?>
                                <?php endforeach; endif; ?>
                            </optgroup>
                        </select>

                    </div>
                </div>

                <div class="form-group header bgcolor-default mt-20">
                    <div class="col-md-12">
                        <h4><?php echo lang('label_est_setting'); ?></h4>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo lang('label_currency') ?></label>

                    <div class="col-lg-5">
                        <select name="estimate_currency" class="form-control">
                            <option value="default"><?php echo lang('label_client_currency') ?></option>
                            <optgroup label="<?php echo lang('label_currency'); ?>">
                                <?php
                                foreach ($currencies as $currency): ?>
                                    <?php if ($currency->name == $estimate->est_currency): ?>
                                        <option selected
                                                value="<?php echo $currency->currency_id; ?>"><?php echo ucfirst(strtolower($currency->name)); ?></option>
                                    <?php else: ?>
                                        <option
                                            value="<?php echo $currency->currency_id; ?>"><?php echo $currency->name; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </optgroup>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo lang('label_estimate_priority'); ?></label>

                    <div class="col-sm-8">
                        <select class="form-control" name="estimate_priority">
                            <option value="...">Seleccionar...</option>
                            <?php
                            $estimate_priorities = task_priorities();
                            foreach ($estimate_priorities as $priority) { ?> ?>

                                <?php if ($estimate->est_priority == $priority->priority_id): ?>
                                    <option selected
                                            value="<?php echo $priority->priority_id; ?>"><?php echo ucfirst(strtolower($priority->priority_name)); ?></option>
                                <?php else: ?>
                                    <option
                                        value="<?php echo $priority->priority_id; ?>"><?php echo ucfirst(strtolower($priority->priority_name)); ?></option>
                                <?php endif; ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>


                <div class="form-group">

                    <div class="col-sm-3 control-label">
                        <label><?php echo lang('label_is_visible'); ?></label>
                    </div>


                    <div class="col-sm-8">
                        <input type="checkbox" name="estimate_visible"
                               class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($estimate->est_visible == 1) ? 'checked' : ''; ?>/>
                    </div>

                </div>

                <div class="form-group header bgcolor-default mt-10">
                    <div class="col-md-12">
                        <h4><?php echo lang('label_est_date_setting'); ?></h4>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo lang('label_estimate_start'); ?></label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" name="start_date"
                               data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"
                               value="<?php echo date('d-m-Y', strtotime($estimate->est_start_date)); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo lang('label_estimate_end_date'); ?></label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" name="end_date" data-date-format="dd-mm-yyyy"
                               placeholder="dd-mm-yyyy"
                               value="<?php echo date('d-m-Y', strtotime($estimate->est_due_date)); ?>">

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-9 col-lg-offset-3">
                        <button type="input" name="submit" value="editTask" class="btn btn-success btn-lg btn-icon"><i
                                class="fa fa-check-square-o"></i> <?php echo lang('form_button_update'); ?></button>
                    </div>
                </div>
            </div>


            </form>
        </div>
        <!--End adding new estimate -->


        <div class="tab-pane fade" id="notes">
            <?php echo form_open('estimate/update_note/' . $estimate->est_id,
                array('class' => 'panel form-horizontal form-bordered', 'id' => 'add-estimate')); ?>
            <div class="panel-body">
                <div class="form-group header bgcolor-default mt-20">
                    <div class="col-md-12">
                        <h4><?php echo lang('label_estimate_note'); ?></h4>
                    </div>
                </div>
                <div class="form-group mt-10">
                    <textarea class="form-control" required="" name="est_notes"
                              rows="5"><?php echo $estimate->est_notes; ?></textarea>
                </div>
                <button type="input" name="submit" value="addNote" class="btn btn-success btn-lg btn-icon mt-10"><i
                        class="fa fa-check-square-o"></i> <?php echo "save"; ?></button>
            </div>
            </form>
        </div>
    </div>


</fieldset>
