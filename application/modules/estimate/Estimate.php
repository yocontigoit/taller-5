<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/012/12
 * Time: 10:02 PM
 */
class Estimate extends Admin_Controller
{


    function __construct()
    {

        parent::__construct();
        $this->load->model("estimate_m");

    }

    /**
     * list all estimates
     */
    function index()
    {
        $this->data['content'] = 'estimate/estimates';
        $this->data['title'] = 'Estimados';


        if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) {//if admin

            $this->data['estimates'] = $this->estimate_m->get_all_estimates();

        } else {
            $this->data['estimates'] = $this->estimate_m->get_client_estimates($this->CompanyClient_id);

        }

        $this->data['_EST_PAGE'] = $this->load->view('pages/estimates', $this->data, true);
        $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);

    }


    /**
     * create new estimate
     * @param null $quote_id
     */
    function create_estimate($quote_id = null)
    {
        $this->data['content'] = 'estimate/estimates';
        $estimate_id = null;
        $currency = null;
        if ($this->input->post()) {

            $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $estimate_visible = ($this->input->post('estimate_visible') == 'on') ? 1 : 0;

            if ($this->input->post('estimate_currency') == 'default') {
                $client_currency = get_client_details(intval($this->input->post('client')));
                $currency = get_currency_name($client_currency->client_currency);
            } else {

                $currency = get_currency_name($this->input->post('estimate_currency'));
            }

            $param = array(
                "client_id" => $this->input->post('client'),
                "est_code" => $this->input->post('est_number'),
                "est_due_date" => date('Y-m-d', strtotime($this->input->post('end_date'))),
                "est_discount" => 0.00,
                "est_currency" => $currency,
                "est_tax" => 0.00,
                "user_id" => $user_id,
                "est_visible" => $estimate_visible,
                "company_id" => $this->Company_id,
                "est_trans_date" => date('Y-m-d H:i:s'),
                "est_start_date" => date('Y-m-d', strtotime($this->input->post('start_date'))),

                "est_status" => 1,//pending
                "est_priority" => $this->input->post('filter')

            );


            if ($this->input->post('quote_id')) {
                $quote_id = intval($this->input->post('quote_id'));
                $estimate_id = $this->estimate_m->convert_quote($param, $quote_id);
            } else {
                $estimate_id = $this->estimate_m->add_estimate($param);
            }

            if ($estimate_id) {


                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', 'estimate added successfully');

                $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'estimates',
                    'activity_details' => 'estimates',
                    'activity_module_id' => $estimate_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "add a new estimate ",
                    'activity_icon' => 'fa fa-times'
                );

                $this->__activity_tracker($log_params);
                redirect('estimate/preview/' . $estimate_id);
            }


        } else {

            $this->data['title'] = 'Add Estimate';
            $this->data['est_ref'] = $this->config->item('invoice')->estimate_prefix . "-" . trans_reference(11,
                    $this->Company_id);
            $this->data['clients'] = $this->estimate_m->get_company_clients($this->Company_id);
            $this->data['projects'] = $this->estimate_m->get_company_projects($this->Company_id);
            $this->data['currencies'] = $this->estimate_m->get_currencies();
            if ($quote_id) {
                $this->load->model('quotation/quotation_m');
                $this->data['quote_id'] = intval($quote_id);
                $this->data['quote'] = $this->quotation_m->get_quote_details(intval($quote_id));
                $this->data['clients'] = $this->estimate_m->get_company_clients($this->Company_id);
                $this->data['_EST_PAGE'] = $this->load->view('pages/convert_quote', $this->data, true);
            } else {

                $this->data['_EST_PAGE'] = $this->load->view('pages/add', $this->data, true);
            }

            $this->data['show_datepicker'] = true;
            $this->data['SCRIPT_PAGE'] = true;

        }

        $this->load->view('_main_layout', $this->data);
    }

    /**
     * add estimate activities to database
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->estimate_m->add_log($activity_data);

    }

    /**
     * Line items..
     * @param $est_id
     */
    function update($est_id = null)
    {

        if ($this->input->post()) {

            $estimate_visible = ($this->input->post('estimate_visible') == 'on') ? 1 : 0;

            if ($this->input->post('estimate_currency') == 'default') {
                $client_currency = get_client_details(intval($this->input->post('client')));
                $currency = get_currency_name($client_currency->client_currency);
            } else {
                $currency = get_currency_name($this->input->post('estimate_currency'));
            }

            $param = array(
                "client_id" => $this->input->post('client'),
                "est_code" => $this->input->post('est_number'),
                "est_due_date" => date('Y-m-d', strtotime($this->input->post('end_date'))),
                "est_currency" => $currency,
                "est_visible" => $estimate_visible,
                "est_start_date" => date('Y-m-d', strtotime($this->input->post('start_date'))),
                "est_priority" => $this->input->post('estimate_priority')

            );

            $this->data['title'] = 'Add Estimate';
            if ($this->estimate_m->update_estimate($param, $est_id)) {

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', 'estimate updated successfully');

                $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'estimates',
                    'activity_details' => 'update estimates',
                    'activity_module_id' => $est_id,
                    'activity_status' => "info",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "update estimate ",
                    'activity_icon' => 'fa fa-times'
                );

                $this->__activity_tracker($log_params);
                redirect('estimate/preview/' . $est_id . '/yes');
            }

        } else {

            $this->data['title'] = 'Edit Estimate';
            $this->data['content'] = 'estimate/estimates';
            $this->data['currencies'] = $this->estimate_m->get_currencies();
            $this->data['estimate'] = $this->estimate_m->get_estimate_data($est_id);
            $this->data['clients'] = $this->estimate_m->get_company_clients($this->Company_id);
            $this->data['_EST_PAGE'] = $this->load->view('pages/edit_estimate', $this->data, true);
            $this->data['show_datepicker'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);
        }


    }

    /*
     * display estimate data
     */

    /**
     * delete item
     */
    function delete_item()
    {

        if ($this->input->post()) {
            $item_id = intval($this->input->post('detail_id'));
            $est_id = intval($this->input->post('estimate_id'));

            if ($this->estimate_m->delete_item($item_id, $est_id)) {

                $response = array(
                    'success' => 1,
                    'msg_status' => "success",
                    'message' => lang('messages_item_delete_success')
                );

            } else {
                $response = array(
                    'success' => 0,
                    'msg_status' => "error",
                    'message' => lang('messages_item_delete_error')
                );

            }

            echo json_encode($response);
        }
    }

    function preview($est_id = null, $update = null)
    {

        $this->data['title'] = 'Estimates';
        $this->data['content'] = 'estimate/estimates';
        $this->data['taxes'] = $this->estimate_m->get_taxes();
        $this->data['items'] = $this->estimate_m->get_all_items($this->Company_id);
        if ($update) {

            if ($this->CompanyUserRolePermission_id == 1) {//if admin
                $this->data['estimate'] = $this->estimate_m->get_estimate_data($est_id);
                $this->data['est_items'] = $this->estimate_m->get_estimate_items($est_id);

            } elseif ($this->CompanyUserRolePermission_id == 3) {
                $this->data['estimate'] = $this->estimate_m->get_estimate_data($est_id, $this->CompanyClient_id);
                if (count($this->data['estimate'])) {
                    $this->data['est_items'] = $this->estimate_m->get_estimate_items($est_id, $this->CompanyClient_id);
                } else {
                    redirect('no_access');
                }
            }

            $this->data['estimate_calculations'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->data['_EST_PAGE'] = $this->load->view('pages/preview', $this->data, true);
        } else {

            $this->data['estimate'] = $this->estimate_m->get_estimate_data($est_id);

            $this->data['estimate_calculations'] = true;
            $this->data['SCRIPT_PAGE'] = true;
            $this->data['_EST_PAGE'] = $this->load->view('pages/create_preview', $this->data, true);

        }
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * print estiamte
     * @param null $estimate_id
     */
    public function print_estimate($estimate_id = null)
    {
        $this->load->helper('pdf');


        $this->data['taxes'] = $this->estimate_m->get_taxes();
        $this->data['items'] = $this->estimate_m->get_all_items($this->Company_id);
        $this->data['estimate'] = $this->estimate_m->get_estimate_data($estimate_id);
        $this->data['est_items'] = $this->estimate_m->get_estimate_items($estimate_id);

        generate_estimate_pdf($estimate_id, $this->data, '', $this->data['estimate']->est_code);
    }

    /**
     * email estimate to client
     * @param null $estimate_id
     */
    function email($estimate_id = null)
    {

        $this->data['taxes'] = $this->estimate_m->get_taxes();
        $this->data['items'] = $this->estimate_m->get_all_items($this->Company_id);
        $this->data['estimate'] = $this->estimate_m->get_estimate_data($estimate_id);
        $this->data['est_items'] = $this->estimate_m->get_estimate_items($estimate_id);
        $this->notifications->send_new_estimate_notification($estimate_id, $this->data);

        $this->db->set('est_emailed', 1);
        $this->db->set('date_email', date("Y-m-d H:i:s"));
        $this->db->where('est_id', $estimate_id)->update('estimate');


        $this->session->set_flashdata('msg_status', 'success');
        $this->session->set_flashdata('message', 'estimate emailed successfully');
        redirect('estimate/preview/' . $estimate_id . '/yes');
    }

    /**
     * update estimate data
     * @param null $est_id
     */
    function update_estimate($est_id = null)
    {


        $this->data['title'] = 'Edit Estimate';
        $this->data['content'] = 'estimate/estimates';
        $this->data['estimate'] = $this->estimate_m->get_estimate_data($est_id);
        $this->data['clients'] = $this->estimate_m->get_company_clients($this->Company_id);
        $this->data['show_datepicker'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('modal/estimate', $this->data);
    }

    /**
     *
     * Save items to the database...
     */
    function  save()
    {


        if ($this->input->post()) {
            $success = false;
            $est_id = intval($this->input->post('est_id'));

            $items = json_decode($this->input->post('items'));


            foreach ($items as $item) {
                if ($item->item_hours_worked != '' && $item->hourly_rate != '' && $item->item_name != '') {
                    $param = array(
                        "item_id" => $item->item_name,
                        "item_description" => $item->item_description,
                        "details_hours" => $item->item_hours_worked,
                        "details_discount" => $item->item_discount,
                        "details_tax" => $item->item_tax,
                        "details_rate" => $item->hourly_rate,
                        "est_total" => $item->item_subtotal
                    );

                    if ($item->item_id == 0) {
                        $success = $this->estimate_m->add_estimate_item($param, $est_id);
                    } else {
                        $success = $this->estimate_m->update_estimate_item($param, $est_id, $item->item_id);
                    }
                }

            }

            if ($success) {

                $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'estimates',
                    'activity_details' => 'update estimates items',
                    'activity_module_id' => intval($est_id),
                    'activity_status' => "info",
                    'activity_event' => "update estimate items ",
                    'activity_icon' => 'fa fa-times'
                );

                $this->__activity_tracker($log_params);


                $response = array(
                    'success' => 1,
                    'msg_status' => "success",
                    'message' => lang('messages_estimate_item_save_success')
                );
            } else {

                $response = array(
                    'success' => 0,
                    'msg_status' => "error",
                    'message' => "estimate items  not updated"
                );
            }

            echo json_encode($response);

        }
    }

    /**
     * Convert estimate to invoice.
     * @param null $est_id
     *
     */
    function convert($est_id = null)
    {

        $estimate_data = $this->estimate_m->get_estimate_data($est_id);

        $invoice_number = $this->config->item('invoice')->invoice_prefix . "-" . trans_reference(10, $this->Company_id);

        $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;

        $param = array(
            "inv_no" => $invoice_number,
            "client_id" => $estimate_data->client_id,
            "status" => 2,
            "approved" => 1,
            "currency" => $estimate_data->est_currency,
            "invoice_due" => date('Y-m-d', strtotime($estimate_data->est_due_date)),
            "created_at" => date('Y-m-d H:i:s'),
            "updated_at" => date('Y-m-d H:i:s'),
            "inv_created_by" => $user_id
        );
        $invoice_id = $this->estimate_m->estimate_to_invoice($param, $est_id);
        if ($invoice_id) {

            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', 'estimate invoiced successfully');

            $log_params = array(
                'activity_user' => $user_id,
                'activity_user_type' => $this->CompanyUser_type,
                'activity_module' => 'estimates',
                'activity_details' => 'Converted EST  to Invoice',
                'activity_module_id' => intval($est_id),
                'activity_status' => "info",
                'activity_date' => date('Y-m-d,H:i:s'),
                'activity_event' => "Converted EST  to Invoice ",
                'activity_icon' => 'fa fa-money'
            );

            $this->__activity_tracker($log_params);
            redirect('billing/invoice/' . $invoice_id);
        }

    }

    /**
     * convert estimate to project
     * @param null $est_id
     */
    function convert_to_project($est_id = null)
    {

        //estimate items are tasks...
        $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
        if ($est_id) {
            $est_id = intval($est_id);
            $project_code = $this->config->item('invoice')->project_prefix . "-" . trans_reference(13,
                    $this->Company_id);
            $project_id = $this->estimate_m->estimate_to_project($est_id, $project_code);
        } else {
            $project_id = null;
        }

        if ($project_id) {


            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', 'estimate converted to project successfully');

            $log_params = array(
                'activity_user' => $user_id,
                'activity_user_type' => $this->CompanyUser_type,
                'activity_module' => 'project',
                'activity_details' => 'Converted EST  to Invoice',
                'activity_module_id' => intval($project_id),
                'activity_status' => "info",
                'activity_date' => date('Y-m-d,H:i:s'),
                'activity_event' => "Converted EST  to Project ",
                'activity_icon' => 'fa fa-times'
            );

            $this->__activity_tracker($log_params);
            redirect('project/view/' . $project_id);
        }


    }

    /**
     * update estimate status
     */
    function status()
    {
        $status = $this->uri->segment(3);
        $estimate_id = $this->uri->segment(4);

        if ($this->estimate_m->update_status($status, $estimate_id)) {

            $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $log_params = array(
                'activity_user' => $activity_user,
                'activity_user_type' => $this->CompanyUser_type,
                'activity_module' => 'estimates',
                'activity_details' => 'update estimates status',
                'activity_module_id' => intval($estimate_id),
                'activity_status' => "info",
                'activity_event' => "update estimate status ",
                'activity_icon' => 'fa-paperclip'
            );

            $this->__activity_tracker($log_params);

            $this->session->set_flashdata('response_status', 'success');
            $this->session->set_flashdata('message', lang('messages_estimate_save_success'));
            redirect('estimate/preview/' . $estimate_id . '/yes');
        }
    }
}
