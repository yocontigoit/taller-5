<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/09/11
 * Time: 9:51 AM
 */
class Report_M extends CI_Model
{


    /**
     * get all expenses
     * @return mixed
     */
    function all_expenses()
    {
        $this->db->from('costs');
        $this->db->group_by('classification');

        return $this->db->get()->result();
    }


    /**
     * get invoices by client
     * @param $client_id
     * @return mixed
     */
    function client_all_invoice($client_id)
    {

        $this->db->from('invoice');
        $this->db->where("client_id", $client_id);

        return $this->db->get()->result();

    }

    /**
     * get client invoices
     * @return mixed
     */
    function get_clients_invoices()
    {

        $this->db->from('invoice');
        $this->db->group_by('client_id');

        return $this->db->get()->result();

    }

     function get_invoices()
    {

        $this->db->from('invoice');

        return $this->db->get()->result();

    }

    /**
     * @param null $company_id
     * @return mixed
     */
    function get_company_clients($company_id = null)
    {

        $company_id = intval($company_id);
        $this->db->from('client');
        $this->db->where("company_id", $company_id);

        return $this->db->get()->result();
    }

    function clients(){

      $this->db->select('client_name');
      $this->db->from('client');

      return $this->db->get()->result();
    }

    function insert_prueba($data = array()){

      $this->db->insert('ejemplo', $data);

    }

    function traer_datos(){

      $this->db->from('ejemplo');

      return $this->db->get()->row();
    }

    function update_datos($datos = array(), $id = null){

      $this->db->where('id', $id)->update('ejemplo', $datos);
    }

    function delete_data($datos = array(), $id = null){

      $this->db->where('id', $id)->delete('ejemplo', $datos);
    }


}
