<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/11
 * Time: 12:07 PM
 */
class Report extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('report_m');
    }

    /**
     * list reports dashboard
     */
    function index()
    {
        $this->data['content'] = 'report/dashboard';
        $this->data['title'] = 'Reportes';
        $this->data['tits'] = $this->report_m->get_invoices();
        $this->data['titles'] = $this->report_m->get_invoices();
        $this->data['clients'] = $this->report_m->clients();

        $this->data['datos'] = $this->report_m->traer_datos();

        $this->data['_PAGE'] = $this->load->view('page/dashboard', $this->data, true);
        $this->data['show_dashboard_report'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }


    /**
     * view selected type of a report
     */
    function view()
    {

        $this->data['content'] = 'report/dashboard';
        $this->data['title'] = 'Reports';
        $this->data['SCRIPT_PAGE'] = true;
        $this->data['tits'] = $this->report_m->get_invoices();
        $this->data['titles'] = $this->report_m->get_invoices();
        $report_page = $this->input->get('page', true) ? $this->input->get('page', true) : 'dashboard';
        $this->data['year'] = $this->input->get('period', true) ? $this->input->get('period', true) : date('Y');

        switch ($report_page) {


            case 'all_invoices':
                $this->data['transactions'] = $this->report_m->get_clients_invoices();
                $this->data['_PAGE'] = $this->load->view('page/income_report', $this->data, true);
                $this->data['datatable'] = true;
                $this->data['payments_stats'] = true;
                break;
            case 'client_invoices':
                $this->data['clients'] = $this->report_m->get_company_clients($this->Company_id);
                $this->data['_PAGE'] = $this->load->view('page/client_report', $this->data, true);
                $this->data['show_client_report'] = true;
                break;
            case 'expense_report':
                $this->data['expenses'] = $this->report_m->all_expenses();
                $this->data['_PAGE'] = $this->load->view('page/expense_report', $this->data, true);
                break;

            default:
                $this->data['_PAGE'] = $this->load->view('page/dashboard', $this->data, true);
                $this->data['show_dashboard_report'] = true;
                break;
        }
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * view client report
     */
    public function customer_report_ajax()
    {
        $this->data['client_id'] = $_POST['client_id'];
        $this->data['transactions'] = $this->report_m->client_all_invoice($_POST['client_id']);
        // Load view
        $this->load->view('ajax/client', $this->data);
    }

    function insert(){

      if ($this->input->post()) {
        $campo1 = $this->input->post('campo1');
        $campo2 = $this->input->post('campo2');
        $param = array(
          'campo1' => $campo1,
          'campo2' => $campo2
        );

        $this->report_m->insert_prueba($param);

      }
      redirect('https://proyectosinternos.com/Taller5/report');

    }

    function update(){

      if($this->input->post()){

        $id = $this->input->post('id');
        $param = array(
          'campo1' => $this->input->post('campo1'),
          'campo2' => $this->input->post('campo2')
        );

        $this->report_m->update_datos($param, $id );

      }
      redirect('https://proyectosinternos.com/Taller5/report');

    }

    function delete(){

      if($this->input->post()){

        $id = $this->input->post('id');
        $param = array(
          'campo1' => $this->input->post('campo1'),
          'campo2' => $this->input->post('campo2')
        );

        $this->report_m->delete_data($param, $id );

      }
      redirect('https://proyectosinternos.com/Taller5/report');

    }


}
