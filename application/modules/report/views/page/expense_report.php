<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/11
 * Time: 12:07 PM
 */
?>
<div class="col-md-12">
    <fieldset>
        <legend><?php echo lang('label_statistics'); ?><span class="pull-right"><?php echo $year; ?></span></legend>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
                   id="zest-table">
                <thead class="the-box dark full">
                <tr>
                    <!-- <th><?php echo lang('label_category'); ?></th>
                    <th><?php echo lang('january'); ?></th>
                    <th><?php echo lang('february'); ?></th>
                    <th><?php echo lang('march'); ?></th>
                    <th><?php echo lang('april'); ?></th>
                    <th><?php echo lang('may'); ?></th>
                    <th><?php echo lang('june'); ?></th>
                    <th><?php echo lang('july'); ?></th>
                    <th><?php echo lang('august'); ?></th>
                    <th><?php echo lang('september'); ?></th>
                    <th><?php echo lang('october'); ?></th>
                    <th><?php echo lang('november'); ?></th>
                    <th><?php echo lang('december'); ?></th> -->
                </tr>
                </thead>
                <tbody>
                <?php
                $jan_total = 0;
                $feb_total = 0;
                $mar_total = 0;
                $apr_total = 0;
                $may_total = 0;
                $jun_total = 0;
                $jul_total = 0;
                $aug_total = 0;
                $sep_total = 0;
                $oct_total = 0;
                $nov_total = 0;
                $dec_total = 0;

                if (count($expenses)):


                    foreach ($expenses as $expense): ?>
                        <?php
                        $jan_tot = expense_stats_monthly('01', $year, $expense->classification);
                        $feb_tot = expense_stats_monthly('02', $year, $expense->classification);
                        $mar_tot = expense_stats_monthly('03', $year, $expense->classification);
                        $apr_tot = expense_stats_monthly('04', $year, $expense->classification);
                        $may_tot = expense_stats_monthly('05', $year, $expense->classification);
                        $jun_tot = expense_stats_monthly('06', $year, $expense->classification);
                        $jul_tot = expense_stats_monthly('07', $year, $expense->classification);
                        $aug_tot = expense_stats_monthly('08', $year, $expense->classification);
                        $sep_tot = expense_stats_monthly('09', $year, $expense->classification);
                        $oct_tot = expense_stats_monthly('10', $year, $expense->classification);
                        $nov_tot = expense_stats_monthly('11', $year, $expense->classification);
                        $dec_tot = expense_stats_monthly('12', $year, $expense->classification);
                        ?>
                        <tr>
                            <td> <?php echo get_category_name($expense->classification); ?></td>
                            <td><?php echo format_currency($jan_tot); ?></td>
                            <td><?php echo format_currency($feb_tot); ?></td>
                            <td><?php echo format_currency($mar_tot); ?></td>
                            <td><?php echo format_currency($apr_tot); ?></td>
                            <td><?php echo format_currency($may_tot); ?></td>
                            <td><?php echo format_currency($jun_tot); ?></td>
                            <td><?php echo format_currency($jul_tot); ?></td>
                            <td><?php echo format_currency($aug_tot); ?></td>
                            <td><?php echo format_currency($sep_tot); ?></td>
                            <td><?php echo format_currency($oct_tot); ?></td>
                            <td><?php echo format_currency($nov_tot); ?></td>
                            <td><?php echo format_currency($dec_tot); ?></td>
                            <?php
                            $jan_total += $jan_tot;
                            $feb_total += $feb_tot;
                            $mar_total += $mar_tot;
                            $apr_total += $apr_tot;
                            $may_total += $may_tot;
                            $jun_total += $jun_tot;
                            $jul_total += $jul_tot;
                            $aug_total += $aug_tot;
                            $sep_total += $sep_tot;
                            $oct_total += $oct_tot;
                            $nov_total += $nov_tot;
                            $dec_total += $dec_tot;
                            ?>

                        </tr>
                    <?php endforeach; endif; ?>
                <tr class="text-danger">
                    <td class="bold">Total</td>
                    <td class="bold <?php echo ($jan_total == 0) ? 'text-success' : '' ?>">
                        <?php echo format_currency($jan_total); ?>
                    </td>
                    <td class="bold <?php echo ($feb_total == 0) ? 'text-success' : '' ?>">
                        <?php echo format_currency($feb_total); ?>
                    </td>
                    <td class="bold <?php echo ($mar_total == 0) ? 'text-success' : '' ?>">
                        <?php echo format_currency($mar_total); ?>
                    </td>
                    <td class="bold <?php echo ($apr_total == 0) ? 'text-success' : '' ?>">
                        <?php echo format_currency($apr_total); ?>
                    </td>
                    <td class="bold <?php echo ($may_total == 0) ? 'text-success' : '' ?>">
                        <?php echo format_currency($may_total); ?>
                    </td>
                    <td class="bold <?php echo ($jun_total == 0) ? 'text-success' : '' ?>">
                        <?php echo format_currency($jun_total); ?>
                    </td>
                    <td class="bold <?php echo ($jul_total == 0) ? 'text-success' : '' ?>">
                        <?php echo format_currency($jul_total); ?>
                    </td>
                    <td class="bold <?php echo ($aug_total == 0) ? 'text-success' : '' ?>">
                        <?php echo format_currency($aug_total); ?>
                    </td>
                    <td class="bold <?php echo ($sep_total == 0) ? 'text-success' : '' ?>">
                        <?php echo format_currency($sep_total); ?>
                    </td>
                    <td class="bold <?php echo ($oct_total == 0) ? 'text-success' : '' ?>">
                        <?php echo format_currency($oct_total); ?>
                    </td>
                    <td class="bold <?php echo ($nov_total == 0) ? 'text-success' : '' ?>">
                        <?php echo format_currency($nov_total); ?>
                    </td>
                    <td class="bold <?php echo ($dec_total == 0) ? 'text-success' : '' ?>">
                        <?php echo format_currency($dec_total); ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </fieldset>
</div>
