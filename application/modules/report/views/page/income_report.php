<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/08/17
 * Time: 12:37 PM
 */
?>
<div class="col-md-12">
    <div class="col-md-4">
        <fieldset>
            <legend><?php echo lang('label_statistics'); ?></legend>
            <div id="payments-stats" style="height: 300px;"></div>
        </fieldset>
    </div>
    <div class="col-md-8">
        <fieldset>
            <legend><?php echo lang('label_statistics'); ?></legend>
            <div class="table-responsive">
                <table
                    class="table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                    <thead class="the-box dark full">
                    <tr>
                        <th>
                            <?php echo lang('label_client_name'); ?>

                        </th>

                        <th>
                            <?php echo lang('label_amount'); ?>

                        </th>

                        <th>
                            <?php echo lang('label_amount_due'); ?>

                        </th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php if (count($transactions)) : foreach ($transactions as $transaction) : ?>
                        <?php
                        $invoice_total = client_invoice_total($transaction->client_id, null);
                        $invoice_paid = client_payments($transaction->client_id, null);
                        ?>
                        <tr>
                            <td>
                                <a href="<?php echo base_url('client/view/' . $transaction->client_id); ?>"><?php echo client_company($transaction->client_id); ?></a>
                            </td>
                            <td>
                                <?php echo format_currency($invoice_total); ?>
                            </td>
                            <td>
                                <?php echo format_currency(($invoice_total - $invoice_paid)); ?>
                            </td>


                        </tr>
                    <?php endforeach; endif; ?>
                    </tbody>
                </table>
            </div>
        </fieldset>
    </div>
</div>
