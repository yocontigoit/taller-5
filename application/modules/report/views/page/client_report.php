<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/12
 * Time: 11:28 AM
 */
?>
<div class="col-md-12">
    <center><h3>REPORTE DE GASTOS DE OFICINA</h3>
    <table class="table  table-bordered table-hover table-full-width table-th-block table-success">
  <tr>
    <th>Concepto</th>
    <th >Enero 2017</th>
    <th >Febrero 2017</th>
    <th >Marzo 2017</th>
    <th >Abril 2017</th>
    <th >Mayo 2017</th>
    <th >Junio 2017</th>
    <th >Julio 2017</th>
    <th >Agosto 2017</th>
    <th >Septiembre 2017</th>
      <th >Octubre 2017</th>
    <th >Noviembre 2017</th>
    <th  >Diciembre 2017</th>
  </tr>
  <tr>
      <th>Nómina</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
   <tr>
      <th>Renta Oficina</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
   <tr>
      <th>Renta Bodega</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
   <tr>
      <th>Agua</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
    <tr>
      <th>Luz</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
    <tr>
      <th>Teléfono</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
   <tr>
      <th>Celulares</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
   <tr>
      <th>Papeleria</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
   <tr>
      <th>Consumuibles</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
   <tr>
      <th>Cafeteria</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
   <tr>
      <th>Suscripciones</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
   <tr>
      <th>Seguros</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
    <tr>
      <th>Limpieza</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
     <tr>
      <th style="background: gainsboro;">Total</th>
      <th style="background: gainsboro;">$0.00</th>
      <th style="background: gainsboro;">$0.00</th>
      <th style="background: gainsboro;">$0.00</th>
      <th style="background: gainsboro;">$0.00</th>
      <th style="background: gainsboro;">$0.00</th>
      <th style="background: gainsboro;">$0.00</th>
      <th style="background: gainsboro;">$0.00</th>
      <th style="background: gainsboro;">$0.00</th>
      <th style="background: gainsboro;">$0.00</th>
      <th style="background: gainsboro;">$0.00</th>
      <th style="background: gainsboro;">$0.00</th>
      <th style="background: gainsboro;">$0.00</th>
  </tr>


 
  
  <!--
  <?php $total =0; $tot1=0;$tot2=0;$tot3=0;$tot4=0;$tot5=0;$tot6=0;
  $tot7=0;$tot8=0;$tot9=0;$tot10=0;$tot11=0;$tot12=0;

   foreach($titles as $title):?>
    <tr><td><?php echo client_company($title->client_id); ?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if($title->client_id == $tit->client_id && $month =="1"): echo $tot1 = $tot1 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit):$month = date("m",strtotime($tit->created_at)); if(client_company($tit->client_id) == client_company($title->client_id) && $month =="2"): echo $tot2 = $tot2 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if(client_company($tit->client_id) == client_company($title->client_id) && $month =="3"): echo $tot3 = $tot3 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if(client_company($tit->client_id) == client_company($title->client_id) && $month =="4"): echo $tot4 = $tot4 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if(client_company($tit->client_id) == client_company($title->client_id) && $month =="5"): echo $tot5 = $tot5 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if(client_company($tit->client_id) == client_company($title->client_id) && $month =="6"): echo $tot6 = $tot6 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if(client_company($tit->client_id) == client_company($title->client_id) && $month =="7"): echo $tot7 = $tot7 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if(client_company($tit->client_id) == client_company($title->client_id) && $month =="9"): echo $tot8 = $tot8 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if(client_company($tit->client_id) == client_company($title->client_id) && $month =="8"): echo $tot9 = $tot9 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at));if(client_company($tit->client_id) == client_company($title->client_id) && $month =="10"): echo $tot10 = $tot10 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if(client_company($tit->client_id) == client_company($title->client_id) && $month =="11"): echo $tot11 = $tot11 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if(client_company($tit->client_id) == client_company($title->client_id) && $month =="12"): echo $tot12 = $tot12 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>


<tr>
    <?php endforeach; ?>
  
-->


</table>
</center>
</div>