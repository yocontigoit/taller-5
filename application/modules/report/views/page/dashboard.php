<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/11
 * Time: 12:07 PM
 */

?>
<div class="col-md-12">
    <center><h3>REPORTE DE INGRESOS Y EGRESOS POR FLUJO</h3>
    <table class="table  table-bordered table-hover table-full-width table-th-block table-success">
  <tr>
    <th>Concepto</th>
    <th >Enero 2017</th>
    <th >Febrero 2017</th>
    <th >Marzo 2017</th>
    <th >Abril 2017</th>
    <th >Mayo 2017</th>
    <th >Junio 2017</th>
    <th >Julio 2017</th>
    <th >Agosto 2017</th>
    <th >Septiembre 2017</th>
      <th >Octubre 2017</th>
    <th >Noviembre 2017</th>
    <th  >Diciembre 2017</th>
  </tr>
  <tr>
      <th>Ingreso Cobranza Proyectos</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
   <tr>
      <th>Ingreso Cobranza Obras</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th>$<?php echo format_currency(est_trans(1)) ?></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
   <tr>
      <th style="background: gainsboro;">Total Cobranza</th>
         <?php $total =0; $tot1=0;$tot2=0;$tot3=0;$tot4=0;$tot5=0;$tot6=0;
  $tot7=0;$tot8=0;$tot9=0;$tot10=0;$tot11=0;$tot12=0; ?>
      <th style="background: gainsboro;">$<?php echo format_currency($tot1);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot2);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot3);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot4);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot5);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot6);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot7);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot8);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot9);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot10);?></th>
      <th style="background: gainsboro;"> $<?php echo format_currency($tot11);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot12);?></th>
  </tr>
   <tr>
      <th >Gastos Proyectos</th>
       <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
   <tr>
      <th>Gastos Obras</th>
<td><?php
$total =0; $tot1=0;$tot2=0;$tot3=0;$tot4=0;$tot5=0;$tot6=0;
  $tot7=0;$tot8=0;$tot9=0;$tot10=0;$tot11=0;$tot12=0;
foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="1"): echo $tot1 = $tot1 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit):$month = date("m",strtotime($tit->created_at)); if( $month =="2"): echo $tot2 = $tot2 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="3"): echo $tot3 = $tot3 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="4"): echo $tot4 = $tot4 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="5"): echo $tot5 = $tot5 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="6"): echo $tot6 = $tot6 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="7"): echo $tot7 = $tot7 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="9"): echo $tot8 = $tot8 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="8"): echo $tot9 = $tot9 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td>$<?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at));if( $month =="10"): $tot10 = $tot10 + _get_invoice_sub_total($tit->id);  endif;endforeach; echo format_currency($tot10);?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="11"): echo $tot11 = $tot11 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="12"): echo $tot12 = $tot12 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
  </tr>
   <tr>
      <th style="background: gainsboro;">Total Gastos Arquitectura</th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot1);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot2);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot3);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot4);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot5);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot6);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot7);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot8);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot9);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot10);?></th>
      <th style="background: gainsboro;"> $<?php echo format_currency($tot11);?></th>
      <th style="background: gainsboro;">$<?php echo format_currency($tot12);?></th>
  </tr>
  <tr>
      <th style="background: gainsboro;">Flujo Bruto</th>
      <th style="background: gainsboro;"></th>
      <th style="background: gainsboro;"></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
   <tr>
      <th>Gastos de Oficina</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
  </tr>
   <tr>
      <th style="background: gainsboro;">Flujo Operativo</th>
      <th style="background: gainsboro;"></th>
      <th style="background: gainsboro;"></th>
      <th style="background: gainsboro;"></th>
      <th style="background: gainsboro;"></th>
      <th style="background: gainsboro;"></th>
      <th style="background: gainsboro;"></th>
      <th style="background: gainsboro;"></th>
      <th style="background: gainsboro;"></th>
      <th style="background: gainsboro;"></th>
      <th style="background: gainsboro;"></th>
      <th style="background: gainsboro;"></th>
      <th style="background: gainsboro;"></th>
  </tr>


  <!--
  <?php $total =0; $tot1=0;$tot2=0;$tot3=0;$tot4=0;$tot5=0;$tot6=0;
  $tot7=0;$tot8=0;$tot9=0;$tot10=0;$tot11=0;$tot12=0;

   foreach($titles as $title):?>
    <tr><td><?php echo client_company($title->client_id); ?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="1"): echo $tot1 = $tot1 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit):$month = date("m",strtotime($tit->created_at)); if( $month =="2"): echo $tot2 = $tot2 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="3"): echo $tot3 = $tot3 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="4"): echo $tot4 = $tot4 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="5"): echo $tot5 = $tot5 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="6"): echo $tot6 = $tot6 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="7"): echo $tot7 = $tot7 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="9"): echo $tot8 = $tot8 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="8"): echo $tot9 = $tot9 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at));if( $month =="10"): echo $tot10 = $tot10 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="11"): echo $tot11 = $tot11 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>
        <td><?php foreach($tits as $tit): $month = date("m",strtotime($tit->created_at)); if( $month =="12"): echo $tot12 = $tot12 + _get_invoice_sub_total($tit->id);  endif;endforeach;?></td>


<tr>
    <?php endforeach; ?>

-->


</table>
</center>
