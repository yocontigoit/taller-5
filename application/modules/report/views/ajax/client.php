<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/08/17
 * Time: 04:10 PM
 */
?>
<div class="col-lg-12">
    <fieldset>
        <legend><?php echo lang('label_statistics'); ?></legend>
        <div class="col-lg-4">
            <div class="row">
                <div class="row billing_top_row">
                    <div class="billing_settings">
                        <ul class="left_box">
                            <li class="revenue_today">
                                <div>
                                    <em><?php echo lang('label_sales_this_month'); ?></em>
                                    <span class="more"><?php echo format_currency(client_invoice_total($client_id,
                                            'month')); ?></span>
                                </div>
                            </li>
                            <li class="revenue_month">
                                <div>
                                    <em><?php echo lang('label_sales_this_year'); ?></em>
                                    <span class="more"><?php echo format_currency(client_invoice_total($client_id,
                                            'year')); ?></span>
                                </div>
                            </li>
                            <li class="revenue_year">
                                <div>
                                    <em><?php echo lang('label_total_sales'); ?></em>
                                    <span class="more"><?php echo format_currency(client_invoice_total($client_id,
                                            null)); ?></span>
                                </div>
                            </li>
                            <li class="revenue_year">
                                <div>
                                    <em><?php echo lang('label_payments_this_year'); ?></em>
                                    <span class="more"><?php echo format_currency(client_payments($client_id,
                                            'year')); ?></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-center">
                        <h4 class="small-heading"><?php echo lang('label_sales'); ?></h4>
										<span class="chart chart-widget-pie widget-easy-pie-1"
                                              data-percent="<?php echo client_sales_perc($client_id); ?>">
											<span class="percent"><?php echo client_sales_perc($client_id); ?></span>
										</span>
                    </div>
                    <!-- /.col-sm-4 -->
                    <div class="col-sm-4 text-center">
                        <h4 class="small-heading"><?php echo lang('label_closed_tickets_all'); ?></h4>
										<span class="chart chart-widget-pie widget-easy-pie-2"
                                              data-percent="<?php echo client_tickets_complete_perc($client_id); ?>">
											<span
                                                class="percent"><?php echo client_tickets_complete_perc($client_id); ?></span>
										</span>
                    </div>
                    <!-- /.col-sm-4 -->
                    <div class="col-sm-4 text-center">
                        <h4 class="small-heading"><?php echo lang('label_closed_projects_all'); ?></h4>
										<span class="chart chart-widget-pie widget-easy-pie-3"
                                              data-percent="<?php echo client_project_complete_perc($client_id); ?>">
											<span
                                                class="percent"><?php echo client_project_complete_perc($client_id); ?></span>
										</span>
                    </div>
                    <!-- /.col-sm-4 -->
                </div>
                <div style="margin: 0 5% 0 5%;">
                    <div class="row">

                        <div class="panel panel-danger panel-heading">
                            <a href="" class="text-danger"><i
                                    class="fa fa-external-link"></i><?php echo lang('label_overdue_invoices'); ?></a>
                                <span class="pull-right text-danger">
                                    <?php echo format_currency(client_amount_overdue($client_id)); ?>
                                </span>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <div class="col-lg-8">
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="zest_table">
                    <thead class="the-box dark full">
                    <tr>
                        <th>
                            <?php echo lang('label_invoice_no'); ?>
                        </th>
                        <th>
                            <?php echo lang('label_client_name'); ?>
                        </th>
                        <th>
                            <?php echo lang('label_due_date'); ?>
                        </th>
                        <th>
                            <?php echo lang('label_amount'); ?>
                        </th>
                        <th>
                            <?php echo lang('label_amount_due'); ?>
                        </th>
                        <th>
                            <?php echo lang('label_payment_method'); ?>
                        </th>
                        <th>
                            <?php echo lang('label_status'); ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (count($transactions)) : foreach ($transactions as $transaction) : ?>
                        <tr>
                            <td>
                                <a href="<?php echo base_url('billing/invoice/' . $transaction->id); ?>"><?php echo $transaction->inv_no; ?></a>
                            </td>
                            <td>
                                <a href="<?php echo base_url('client/invoice/' . $transaction->client_id); ?>"><?php echo client_company($transaction->client_id); ?></a>
                            </td>

                            <td><?php echo $transaction->invoice_due; ?></td>
                            <td>
                                <?php echo format_amount(invoice_total($transaction->id)); ?>
                            </td>
                            <td>
                                <?php echo format_amount(invoice_balance($transaction->id)); ?>
                            </td>
                            <?php $invoice_status = get_invoice_status($transaction->status);

                            if ($invoice_status == "PAID") {
                                $label = "success";
                                $_message = invoice_payment_method($transaction->id);
                            } else {
                                $label = "info";
                                $_message = "No payment received";
                            }


                            ?>
                            <td><span class="label label-<?php echo $label; ?>"><?php echo $_message; ?></span></td>

                            <td>
                                <span class="textgreen"><?php echo get_invoice_status($transaction->status); ?></span>
                            </td>
                        </tr>
                    <?php endforeach; endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
</div>
<link href="<?php echo base_url('assets/plugins/datatable/css/bootstrap.datatable.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datatable/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatable/js/bootstrap.datatable.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>

<script type="text/javascript">
    $(function () {
        var oTable = $('#zest_table').dataTable({
            "aoColumnDefs": [
                {"aTargets": [4, 1]}
            ],
            "aaSorting": [[1, 'asc']],
            "aLengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 10
        });


        $('.widget-easy-pie-1').easyPieChart({
            easing: 'easeOutBounce',
            barColor: '#8CC152',
            lineWidth: 10,

            onStep: function (from, to, percent) {
                $(this.el).find('.percent').text(Math.round(percent));
            }
        });

        $('.widget-easy-pie-2').easyPieChart({
            easing: 'easeOutBounce',
            barColor: '#F6BA48',
            lineWidth: 10,

            onStep: function (from, to, percent) {
                $(this.el).find('.percent').text(Math.round(percent));
            }
        });

        $('.widget-easy-pie-3').easyPieChart({
            easing: 'easeOutBounce',
            barColor: '#4acab4',
            lineWidth: 10,


            onStep: function (from, to, percent) {
                $(this.el).find('.percent').text(Math.round(percent));
            }
        });


    });
</script>