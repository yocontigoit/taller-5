<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/11
 * Time: 12:07 PM
 */
?>
<div class="the-box">
    <div class="row">
        <div class="col-md-8">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="btn-group bootstrap-select" style="width: 100%;">
                                    <button type="button" class="btn dropdown-toggle btn-info" data-toggle="dropdown"
                                            style="width: 100%;" title="Select Report Type">
                                        <span>Selecionar tipo de reporte</span>&nbsp;<span class="bs-caret"><span
                                                class="caret"></span></span></button>
                                    <ul class="dropdown-menu" style="width: 100%;">
                                        <li>
                                            <a href="<?php echo base_url('report/view'); ?>">REPORTE DE INGRESOS Y EGRESOS POR FLUJO</a>
                                        </li>
                                        <!-- <li>
                                            <a href="<?php echo base_url('report/view/?page=all_invoices'); ?>"><?php echo lang('label_invoice_report'); ?></a>
                                        </li> -->
                                        <li>
                                            <a href="<?php echo base_url('report/view/?page=client_invoices'); ?>">REPORTE DE GASTOS DE OFICINA<?php echo lang('label_client_invoice_report'); ?></a>
                                        </li>
                                        <!-- <li>
                                            <a href="<?php echo base_url('report/view/?page=expense_report'); ?>"><?php echo lang('label_expense_report'); ?></a>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <?php if ($this->input->get('page')) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="btn-group bootstrap-select" style="width: 100%;">
                                        <button type="button" class="btn dropdown-toggle btn-info"
                                                data-toggle="dropdown" style="width: 100%;" title="Select Year">
                                            <span>Año</span>&nbsp;<span class="bs-caret"><span
                                                    class="caret"></span></span></button>
                                        <ul class="dropdown-menu" style="width: 100%;">
                                            <?php
                                            $a = range(date('Y'), date('Y') - 3);
                                            foreach ($a as $key => $year) { ?>
                                                <li>
                                                    <a href="<?php echo base_url('report/view/?page=' . $this->input->get('page')); ?>&period=<?= $year ?>"><?= $year ?></a>
                                                </li>
                                            <?php }
                                            ?>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php echo $_PAGE; ?>
    </div>
</div>
