<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/03
 * Time: 08:40 AM
 */
?>
<div class="row">
  <div class="col-lg-12">
    <div class="the-box">

    <div class="table-toolbar"><h3>Clientes</h3>
        <div class="btn-group">

            <?php echo anchor('client/add', lang('common_client_add') . ' <i class="fa fa-plus"></i>',
                array('class' => 'btn btn-info')); ?>

        </div>

    </div>

    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
               id="zest_table1">
            <thead>
            <tr>

                <th>
                    Nombre
                </th>
                <th class="hidden-xs">
                    Teléfono
                </th>
                <th>
                    Correo
                </th>

                <th>
                    <?php echo lang("label_options") ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php if (count($clients)): foreach ($clients as $client): ?>
                <tr>

                    <td>
                        <?php echo $client->client_name; ?>
                    </td>
                    <td class="hidden-xs">
                      <?php echo $client->client_phone_no; ?>
                    </td>
                    <td>
                        <?php echo $client->client_email; ?>
                    </td>

                    <td>
                        <div class="btn-group-action">
                            <div class="btn-group">
                                <a class="" style="color:#0f80d0"
                                   href="<?php echo base_url('client/view/' . $client->client_id); ?>" title=""><i
                                        class="fa fa-eye fa-2x"></i></a>
                                <a class="" style="color:#0f80d0"
                                           href="<?php echo base_url('client/delete/' . $client->client_id); ?>"
                                           title=""><i class="fa fa-trash fa-2x"></i></a>
                                <a class="" style="color:#0f80d0"
                                    href="<?php echo base_url('client/pagar_estimacion/'. $client->client_id); ?>"
                                      data-toggle="zestModal"><i class="fa fa-money fa-2x"></i></a>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php endif; ?>


            </tbody>
        </table>
    </div>
</div>
</div>
<div class="col-lg-12">
  <div class="the-box">
<h3>Proveedores</h3>
  <div class="table-toolbar">
      <div class="btn-group">

          <?php echo anchor('client/add2', 'Agregar Proveedor Nuevo <i class="fa fa-plus"></i>',
              array('class' => 'btn btn-info')); ?>

      </div>
      <div class="btn-group">

         <a href="<?php echo base_url('client/client/nota_credito/'); ?>" data-toggle="zestModal" class="btn btn-info">Agregar Nota de Credito</a>
      </div>

  </div>


  <!-- Stats Counter Sec-->

  <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
             id="zest_table2">
          <thead>
          <tr>

              <th>
                  Nombre
              </th>
              <th class="hidden-xs">
                  Teléfono
              </th>
              <th>
                  Correo
              </th>

              <th>
                  <?php echo lang("label_options") ?>
              </th>
          </tr>
          </thead>
          <tbody>
          <?php if (count($clients2)): foreach ($clients2 as $client): ?>
              <tr class="tr-link" data-link="<?php echo base_url("client/view/" . $client->client_id); ?>">

                  <td>
                      <?php echo $client->client_name; ?>
                  </td>
                  <td class="hidden-xs">
                    <?php echo $client->client_phone_no; ?>
                  </td>
                  <td>
                      <?php echo $client->client_email; ?>
                  </td>

                  <td>
                      <div class="btn-group-action">
                          <div class="btn-group">
                            <a class="" style="color:#0f80d0"
                               href="<?php echo base_url('client/view/' . $client->client_id); ?>" title=""><i
                                    class="fa fa-eye fa-2x"></i></a>
                            <a class="" style="color:#0f80d0"
                                       href="<?php echo base_url('client/delete/' . $client->client_id); ?>"
                                       title=""><i class="fa fa-trash fa-2x"></i></a>

                              </ul>
                          </div>
                      </div>
                  </td>
              </tr>
          <?php endforeach; ?>
          <?php endif; ?>


          </tbody>
      </table>
  </div>
</div>
</div>
<div class="col-lg-12">
  <div class="the-box">
  <div class="table-toolbar">
      <div class="btn-group">
<h3>Contratistas</h3>
          <?php echo anchor('client/add3', 'Agregar Contratista Nuevo <i class="fa fa-plus"></i>',
              array('class' => 'btn btn-info')); ?>

      </div>

  </div>

  <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
             id="zest_table3">
          <thead>
          <tr>

              <th>
                  Nombre
              </th>
              <th class="hidden-xs">
                  Teléfono
              </th>
              <th>
                  Correo
              </th>

              <th>
                  <?php echo lang("label_options") ?>
              </th>
          </tr>
          </thead>
          <tbody>
          <?php if (count($clients3)): foreach ($clients3 as $client): ?>
              <tr class="tr-link" data-link="<?php echo base_url("client/view/" . $client->client_id); ?>">


                  <td>
                      <?php echo $client->client_name; ?>
                  </td>
                  <td class="hidden-xs">
                    <?php echo $client->client_phone_no; ?>
                  </td>
                  <td>
                      <?php echo $client->client_email; ?>
                  </td>

                  <td>
                      <div class="btn-group-action">
                          <div class="btn-group">
                            <a class="" style="color:#0f80d0"
                               href="<?php echo base_url('client/view/' . $client->client_id); ?>" title=""><i
                                    class="fa fa-eye fa-2x"></i></a>
                            <a class="" style="color:#0f80d0"
                                       href="<?php echo base_url('client/delete/' . $client->client_id); ?>"
                                       title=""><i class="fa fa-trash fa-2x"></i></a>

                              </ul>
                          </div>
                      </div>
                  </td>
              </tr>
          <?php endforeach; ?>
          <?php endif; ?>


          </tbody>
      </table>
  </div>
</div>
</div>
<div class="col-lg-12">
  <div class="the-box">
  <div class="table-toolbar">
      <div class="btn-group">
<h3>Albañiles</h3>
          <?php echo anchor('client/add4', 'Agregar Albañil Nuevo <i class="fa fa-plus"></i>',
              array('class' => 'btn btn-info')); ?>

      </div>

  </div>

  <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
             id="zest_table3">
          <thead>
          <tr>

              <th>
                  Nombre
              </th>
              <th class="hidden-xs">
                  Teléfono
              </th>
              <th>
                  Correo
              </th>

              <th>
                  <?php echo lang("label_options") ?>
              </th>
          </tr>
          </thead>
          <tbody>
          <?php if (count($clients4)): foreach ($clients4 as $client): ?>
              <tr class="tr-link" data-link="<?php echo base_url("client/view/" . $client->client_id); ?>">


                  <td>
                      <?php echo $client->client_name; ?>
                  </td>
                  <td class="hidden-xs">
                    <?php echo $client->client_phone_no; ?>
                  </td>
                  <td>
                      <?php echo $client->client_email; ?>
                  </td>

                  <td>
                      <div class="btn-group-action">
                          <div class="btn-group">
                            <a class="" style="color:#0f80d0"
                               href="<?php echo base_url('client/view/' . $client->client_id); ?>" title=""><i
                                    class="fa fa-eye fa-2x"></i></a>
                            <a class="" style="color:#0f80d0"
                                       href="<?php echo base_url('client/delete/' . $client->client_id); ?>"
                                       title=""><i class="fa fa-trash fa-2x"></i></a>

                              </ul>
                          </div>
                      </div>
                  </td>
              </tr>
          <?php endforeach; ?>
          <?php endif; ?>


          </tbody>
      </table>
  </div>
</div>
</div>
</div>
