<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/05/29
 * Time: 11:31 AM
 */
?>
<div id="contentarea" class="contentarea">
    <fieldset>
        <legend><i class="fa fa-user"></i><?php echo strtoupper(" Agregar Proveedor"); ?></legend>
        <div class="row">
            <div class="col-sm-12">
                <div class="the-box">
                    <h4 class="small-title"><?php echo lang('label_client_details'); ?></h4>

                    <?php echo form_open('client/add', array('id' => 'clientForm', 'class' => 'form-horizontal')); ?>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Nombre</label>

                        <div class="col-lg-5">
                            <input type="text" class="form-control" name="company_name" placeholder="Nombre Completo"
                                   required/>
                        </div>
                    </div>

                    <input type="hidden" class="form-control" name="company_code" value="0"
                                  />
                                  <input type="hidden" class="form-control" name="client_type" value="2"
                                                />

                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo lang('label_clients_company_phone'); ?></label>

                        <div class="col-lg-5">
                            <input type="text" class="form-control phone_us_masking" name="phone_number"
                                   placeholder="(000) 000-0000">
                                   <input type="hidden" class="form-control" name="company_code" value="0"
                                                 />
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-lg-5">
                            <input class="form-control" name="address1" type="hidden"/>

                            <input class="form-control" name="address2" type="hidden"/>
                        </div>
                    </div>




                    <div class="form-group">
                        <label class="col-lg-3 control-label"><?php echo lang('label_location_city'); ?></label>

                        <div class="col-lg-3">
                            <input type="text" class="form-control" id="client_city" name="city"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Correo del Proveedor</label>

                        <div class="col-lg-5">
                            <input class="form-control" name="email" type="email"
                                   data-bv-emailaddress-message="The input is not a valid email address"/>
                        </div>
                    </div>


                </div>
                <!-- /.the-box -->
            </div>
            <!-- /.col-sm-12-->

            <div class="row">

                <div class="col-lg-12">
                    <button type="submit"
                            class="btn btn-success btn-block btn-lg">Proveedor nuevo</button>
                </div>

            </div>
            </form>
        </div>
        <!-- /.row -->

    </fieldset>
</div>
