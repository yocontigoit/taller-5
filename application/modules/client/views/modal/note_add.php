<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/01
 * Time: 08:28 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                        class="fa fa-times"></i></span><span class="sr-only"><?php echo "close"; ?></span></button>
            <h4 class="modal-title"><?php echo "Add Contact"; ?></h4>
        </div>


        <?php echo form_open('client/contact_add', array('id' => 'contact_form')); ?>
        <div class="modal-body">

            <!--start-->
            <input type="hidden" class="form-control" name="client_id" value="<?php echo $client_id; ?>">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name"><?php echo lang('label_name'); ?></label>
                        <input type="text" class="form-control" name="contact_name">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="surname"><?php echo lang('label_surname'); ?></label>
                        <input type="text" class="form-control" name="contact_username">

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="address"><?php echo lang('label_location_address_1'); ?></label>
                        <input type="text" class="form-control" name="contact_address1">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="address"><?php echo lang('label_location_address_2'); ?></label>
                        <input type="text" class="form-control" name="contact_address2">

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="address"><?php echo lang('label_location_address_3'); ?></label>
                        <input type="text" class="form-control" name="contact_address3">

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="email"><?php echo lang('label_email'); ?></label>
                        <input type="text" class="form-control" name="contact_email">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="phone"><?php echo lang('label_phone'); ?></label>
                        <input type="text" class="form-control" name="contact_phone">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="password"><?php echo lang('label_password'); ?></label>
                        <input type="password" class="form-control" name="password">
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label><?php echo lang('label_active'); ?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="checkbox" name="active"
                                       class="ios-switch ios-switch-success ios-switch-lg"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="modal-footer">
            <button type="input" name="submit" class="btn btn-info btn-icon"><i
                    class="fa fa-check-square-o"></i><?php echo lang('form_button_save'); ?></button>
            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i
                    class="fa fa-times-circle-o"></i><?php echo lang('form_button_cancel'); ?></button>
        </div>
        </form>
    </div>
</div>
<script type="text/javascript">

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

</script>