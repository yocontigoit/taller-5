<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add</h4>
        </div>
        <?php echo form_open('client/add', array('id' => 'clientForm', 'class' => 'form-horizontal')); ?>
        <?php echo validation_errors(); ?>
        <div class="modal-body">

            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_clients_company_name'); ?></label>

                <div class="col-lg-5">
                    <input type="text" class="form-control" name="company_name" placeholder="Company Name" required/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_clients_company_code'); ?></label>

                <div class="col-lg-5">
                    <input type="text" class="form-control" name="company_code" placeholder="Company Code" required/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_clients_company_status'); ?></label>

                <div class="col-lg-5"><?php //@TODO: Change to select / active or pontential?>

                    <select class="form-control" name="company_status">
                        <?php foreach ($company_status as $status): ?>
                            <option value="<?= $company_type->status_id ?>"><?= $company_type->status_name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_clients_company_type'); ?></label>

                <div class="col-lg-5"><?php //@TODO: Change to select/ Software company/ nothing like that ?? ?>
                    <select class="form-control" name="company_type">
                        <?php foreach ($company_types as $company_type): ?>
                            <option value="<?= $company_type->type_id ?>"><?= $company_type->type_name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_clients_company_phone'); ?></label>

                <div class="col-lg-5">
                    <input type="text" class="form-control phone_us_masking" name="phone_number"
                           placeholder="(000) 000-0000">
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_location_address_1'); ?></label>

                <div class="col-lg-5">
                    <input class="form-control" name="address1" type="text"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_location_address_2'); ?></label>

                <div class="col-lg-5">
                    <input class="form-control" name="address2" type="text"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_location_address_3'); ?></label>

                <div class="col-lg-5">
                    <input class="form-control" name="address3" type="text"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_location_country'); ?></label>

                <div class="col-lg-5">
                    <select class="form-control" id="client_country" name="country">

                        <optgroup label="Default Country">
                            <option
                                value="<?= $this->config->item('company_country') ?>"><?= $this->config->item('company_country') ?></option>
                        </optgroup>
                        <optgroup label="Other Countries">
                            <?php foreach ($countries as $country): ?>
                                <option value="<?= $country->value ?>"><?= $country->value ?></option>
                            <?php endforeach; ?>
                        </optgroup>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_location_city'); ?></label>

                <div class="col-lg-3">
                    <input type="text" class="form-control" id="client_city" name="city"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_location_zip_code'); ?></label>

                <div class="col-lg-3">
                    <input type="text" class="form-control" name="zip_code"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_clients_company_website'); ?></label>

                <div class="col-lg-5">
                    <input type="text" class="form-control" name="website" placeholder="http://"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-3 control-label"><?php echo lang('label_clients_company_email'); ?></label>

                <div class="col-lg-5">
                    <input class="form-control" name="email" type="email"
                           data-bv-emailaddress-message="The input is not a valid email address"/>
                </div>
            </div>

        </div>
        <div class="modal-footer"><a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            <button type="submit" class="btn btn-primary">Add</button>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->