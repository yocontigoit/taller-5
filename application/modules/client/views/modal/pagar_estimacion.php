<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/01
 * Time: 08:27 AM
 */
?>
<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button class="close" data-dismiss="modal" type="button">
<span aria-hidden="true">
<i class="fa fa-times"></i>
</span>
                <span class="sr-only">Close</span>

            </button>
            <h4 class="modal-title">Ingreso</h4>
        </div>
        <form method="post" action="<?php echo base_url('client/pay_estimacion/'. $client_id); ?>">
            <input type="hidden" name="client_id"  value"<?php echo $client_id; ?>" />
            <div class="modal-body">
                <div class="row">

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="payment_date"><?php echo lang('label_date'); ?></label>
                        <input type="text" class="form-control datepicker" name="date"
                               data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"
                               value="<?php echo date('Y-m-d') ?>">
                      </div>
                    </div>
                     <div class="col-md-6">
                      <div class="form-group">
                          <label for="taskPriority">Cuenta a la que ingresa</label>

                       <select class="form-control" name="account">
                                <option value="">Ninguno</option>

                                <?php foreach ($this->config->item('payment') as $payment): ?>
                                    <option
                                        value="<?php echo $payment->payment_code; ?>"><?php echo $payment->payment_name; ?></option>
                                <?php endforeach; ?>

                            </select>

                      </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="taskPriority"><?php echo lang('label_payment_method'); ?> </label>

                          <select class="form-control" name="method">
                              <option value="">Ninguno</option>

                                  <option
                                      value="1">Efectivo</option>
                                      <option
                                      value="2">Transaccion</option>
                                      <option
                                      value="2">Cheque</option>


                          </select>
                      </div>
                    </div>
                     <div class="col-md-6">
                      <div class="form-group">
                        <label for="payment_date">descripción</label>
                        <input type="text" class="form-control" name="description" value="">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="payment_date">Monto</label>
                        <input type="text" class="form-control" name="amount" value="">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="payment_date">Avance</label>
                        <select class="form-control" name="avance">
                          <option value="1">Obra</option>
                          <option value="CONCEPTUAL">Conceptual</option>
                          <option value="ANTEPROYECTO">Anteproyecto</option>
                          <option value="PRUEBA ETAPA">Prueba Etapa</option>
                          <option value="PROYECTO EJECUTIVO">Proyecto Ejecutivo</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Obra</label>

                              <select data-placeholder="Select client..." class="form-control  chosen-select" tabindex="4"
                                      name="project" id="project">
                                  <optgroup label="Obras">
                                      <?php if (count($projects)):foreach ($projects as $client): ?>
                                          <option
                                              value="<?php echo $client->project_id; ?>"><?php echo ucfirst($client->project_title) ?></option>
                                      <?php endforeach; endif; ?>
                                  </optgroup>
                              </select>
                      </div>
                    </div>
                  </div>
                </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-icon" name="submit" type="input">
                    <i class="fa fa-check-square-o"></i>
                    Pago Completo
                </button>
                <button class="btn btn-default btn-icon" data-dismiss="modal" type="button">
                    <i class="fa fa-times-circle-o"></i>
                    <?php echo lang('form_button_cancel'); ?>
                </button>
            </div>
        </form>
    </div>
</div>
</div>

<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>

<script type="text/javascript">
    $('#tasks-percent').slider({
        formater: function (value) {
            return value + '% Complete..';
        }
    });

    $("body").delegate(".datepicker", "focusin", function () {
        $(this).datepicker(
            {autoclose: true}
        ).on('changeDate', function (e) {
                $(this).datepicker('hide');
            });
    });

    $(".ios-switch").each(function () {
        mySwitch = new Switch(this);
    });

    var configChosen = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "100%"}
    };
    for (var selector in configChosen) {
        $(selector).chosen(configChosen[selector]);
    }
</script>
