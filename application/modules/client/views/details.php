<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/12
 * Time: 09:59 AM
 */
?>
<div class="the-box">
    <div class="row">
        <div class="col-lg-12">
            <fieldset>
                <legend>
                    <div class="row">
                        <div class="col-lg-12">
                            <i class="fa fa-user"></i><?php echo client_company($client_id); ?>
                            <div class="btn-toolbar pull-right" role="toolbar">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                                            data-target="#bs-example-navbar-collapse-1">
                                        <span><?php echo lang('label_settings'); ?></span>
                                        <span class="fa fa-bars fa-lg"></span>
                                    </button>
                                </div>
                                <div class="collapse navbar-collapse page-tabs" id="bs-example-navbar-collapse-1">
                                    <ul class="nav nav-tabs nav-settings" style="margin-bottom: 0px;">
                                        <li class="<?php echo ($active == 'summary') ? "active" : ""; ?>"><a style="font-size:15px;"
                                                href="<?php echo base_url('client/view/' . $client_id . '/?page=summary'); ?>"><i
                                                    class="fa fa-plane"></i> <?php echo lang('label_summary'); ?></a>
                                        </li>
                                        <li class="<?php echo ($active == 'profile') ? "active" : ""; ?>"><a style="font-size:15px;"
                                                href="<?php echo base_url('client/view/' . $client_id . '/?page=profile'); ?>"><i
                                                    class="fa fa-wrench"></i> <?php echo lang('label_profile'); ?></a>
                                        </li>
                                        <li class="<?php echo ($active == 'contacts') ? "active" : ""; ?>"><a style="font-size:15px;"
                                                href="<?php echo base_url('client/view/' . $client_id . '/?page=contacts'); ?>"><i
                                                    class="fa fa-paper-plane"></i><?php echo lang('label_contacts'); ?>
                                            </a></li>
                                            <?php  if ($client->client_type == 1):
                                            ?>
                                            
                                            <li class="<?php echo ($active == 'estimaciones') ? "active" : ""; ?>"><a style="font-size:15px;"
                                                href="<?php echo base_url('client/view/' . $client_id . '/?page=estimaciones'); ?>"><i
                                                    class="fa fa-sticky-note-o"></i>Estado de Cuenta
                                            </a></li>
                                            
                                            <?php endif; ?>
                                              <?php if ($client->client_type != 1):
                                            ?>
                                            
                                            <li class="<?php echo ($active == 'invoices') ? "active" : ""; ?>"><a style="font-size:15px;"
                                                href="<?php echo base_url('client/view/' . $client_id . '/?page=invoices'); ?>"><i
                                                    class="fa fa-sticky-note-o"></i>Pagos
                                            </a></li>
                                            
                                          
                                              <?php endif; ?>
                                              <?php if ($client->client_type == 1):
                                            ?>
                                            
                                        <li class="<?php echo ($active == 'pagos') ? "active" : ""; ?>"><a style="font-size:15px;"
                                                href="<?php echo base_url('client/view/' . $client_id . '/?page=pagos'); ?>"><i
                                                    class="fa fa-sign-in"></i>Pagos
                                            </a></li>
                                            
                                              <?php endif; ?>
                                              <?php if ($client->client_type != 1):
                                            ?>
                                            
                                             <li class="<?php echo ($active == 'transactions') ? "active" : ""; ?>"><a style="font-size:15px;"
                                                href="<?php echo base_url('client/view/' . $client_id . '/?page=transactions'); ?>"><i
                                                    class="fa fa-sign-in"></i><?php echo lang('label_transactions'); ?>
                                            </a></li>
                                            <?php endif; ?>
                                            <?php if ($client->client_type != 1):
                                            ?>
                                        <li class="<?php echo ($active == 'projects') ? "active" : ""; ?>"><a style="font-size:15px;"
                                                href="<?php echo base_url('client/view/' . $client_id . '/?page=projects'); ?>"><i
                                                    class="fa fa-sign-in"></i> Proyectos </a>
                                        </li>
                                        <?php endif; ?>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                </legend>
                <div class="the-box">

                    <?php echo ($_PAGE) ? $_PAGE : null; ?>
                </div>
            </fieldset>
        </div>
    </div>
</div>
