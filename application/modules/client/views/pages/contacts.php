<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/12
 * Time: 11:28 AM
 */
?>
<div class="row">
    <div class="col-lg-12">
        <fieldset>
            <legend><i class="fa fa-user"></i><?php echo lang('label_contacts'); ?><a
                    class="btn btn-sm btn-info pull-right" data-toggle="zestModal"
                    href="<?php echo base_url('client/contact_add/' . $client_id); ?>">
                    <i class="fa fa-plus"></i>
                    Agregar
                </a></legend>

            <table class="table table-th-block table-success" id="zest_table">
                <thead>
                <tr>
                    <th>
                        <span class="title_box "><?php echo lang('label_name'); ?></span>
                    </th>
                    <th>
                        <span class="title_box "><?php echo lang('label_surname'); ?></span>
                    </th>

                    <th>
                        <span class="title_box "><?php echo lang('label_email'); ?></span>
                    </th>
                    <th>
                        <span class="title_box "><?php echo lang('label_phone'); ?></span>
                    </th>
                    <th>
                        <span class="title_box "><?php echo lang('label_last_login'); ?></span>
                    </th>
                    <th>
                        <span class="title_box "><?php echo lang('label_status'); ?></span>
                    </th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php if (count($client_contacts)): foreach ($client_contacts as $contact) : ?>
                    <tr>
                        <td><?php echo $contact->cl_user_name; ?></td>
                        <td><?php echo $contact->cl_user_surname; ?></td>

                        <td><?php echo $contact->cl_user_email; ?></td>
                        <td><?php echo $contact->cl_user_phone; ?> </td>
                        <td>
                            <i class="fa fa-clock-o"></i> <strong
                                class="text-danger"><?php echo ago(date_to_timestamp($contact->cl_last_login)) . " "; ?></strong><?php echo lang('ago'); ?>
                        </td>
                        <td>

                            <?php

                            if ($contact->cl_user_active == 1) {
                                $label = "success";
                                $active_message = lang('label_active');
                            } else {
                                $label = "danger";
                                $active_message = "Blocked";
                            }
                            ?>
                            <span class="label label-<?php echo $label; ?>"><?php echo $active_message; ?></span>


                        </td>
                        <td class="text-right">
                            <div class="btn-group">
                                <a class="btn btn-info" data-toggle="zestModal"
                                   href="<?php echo base_url('client/edit_contact/' . $contact->cl_user_id); ?>">
                                    <i class="fa fa-edit"></i>
                                    <?php echo lang('label_edit'); ?>
                                </a>
                                <button class="btn  btn-default dropdown-toggle" data-toggle="dropdown" type="button">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="<?php echo base_url('client/mark_primary/' . $contact->cl_user_id . '/' . $client_id); ?>">
                                            <i class="fa fa-key"></i>
                                            <?php echo lang('label_mark_primary'); ?>
                                        </a>
                                    </li>
                                    <?php if ($this->CompanyClient_user_id != $contact->cl_user_id): ?>
                                        <li>
                                            <a href="<?php echo base_url('client/remove_contact/' . $contact->cl_user_id . '/' . $client_id); ?>">
                                                <i class="fa fa-trash-o"></i>
                                                <?php echo lang('label_delete'); ?>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php endforeach;endif; ?>
                </tbody>
            </table>
        </fieldset>
    </div>
</div>
