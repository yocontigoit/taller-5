<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/12
 * Time: 11:28 AM
 */
?>
<div class="row">
    <div class="col-sm-8">
        <fieldset>
            <legend>
                <?php echo lang('label_client_details'); ?>
            </legend>
            <?php echo validation_errors(); ?>
            <?php echo form_open('client/update', array('id' => 'clientForm', 'class' => 'form-horizontal')); ?>

            <input type="hidden" name="client_id" value="<?php echo $client_details->client_id; ?>"/>

            <div class="form-group">
                <label class="col-lg-3 control-label">Nombre:</label>

                <div class="col-lg-5">
                    <input type="text" class="form-control" name="company_name" placeholder="Company Name" required
                           value="<?php echo $client_details->client_name; ?>"/>
                </div>
            </div>



            <?php if ($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2) { ?>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Estatus</label>

                    <div class="col-lg-5">

                        <select class="form-control" name="company_status">
                            <?php foreach ($company_status as $status): ?>
                                <?php if ($client_details->client_status == $status->status_id) { ?>
                                    <option selected
                                            value="<?php echo $status->status_id; ?>"><?php echo $status->status_name; ?></option>
                                <?php } else { ?>
                                    <option
                                        value="<?php echo $status->status_id; ?>"><?php echo $status->status_name; ?></option>
                                <?php } ?>
                            <?php endforeach; ?>

                        </select>
                    </div>
                </div>
            <?php } ?>



            <div class="form-group">
                <label class="col-lg-3 control-label">Teléfono</label>

                <div class="col-lg-5">
                    <input type="text" class="form-control phone_us_masking" name="phone_number"
                           placeholder="(000) 000-0000" value="<?php echo $client_details->client_phone_no; ?>">
                </div>
            </div>



            <div class="form-group">
                <label class="col-lg-3 control-label">Ciudad</label>

                <div class="col-lg-3">
                    <input type="text" class="form-control" id="client_city" name="city"
                           value="<?php echo $client_details->client_city; ?>"/>
                </div>
            </div>


            <div class="form-group">
                <label class="col-lg-3 control-label">Correo del cliente</label>

                <div class="col-lg-5">
                    <input class="form-control" name="email" type="email"
                           value="<?php echo $client_details->client_email; ?>"/>
                </div>
            </div>


        </fieldset>
    </div>
    <!-- /.col-sm-8 -->


    <div class="col-sm-4">

        <fieldset>
            <legend>
                <?php echo lang('label_primary_contact'); ?>
            </legend>
            <?php $primary_info = client_primary_contact_details($client_details->client_id); ?>
            <div class="media user-card-sm">
                <div class="media-left">
                    <a class="pull-left" href="#fakelink">
                        <img class="media-object img-circle"
                             src="<?php echo base_url('files/profile_images/' . $primary_info->cl_user_avatar); ?>"
                             alt="Avatar">
                    </a>
                </div>
                <?php

                ?>
                <div class="media-body">
                    <h4 class="media-heading"><?php echo $primary_info->cl_user_surname; ?></h4>

                    <p>
                        <span
                            class="label label-success"><strong><?php echo get_role_name($primary_info->role_id); ?></strong></span>
                    </p>
                </div>
            </div>
            <div class="user-content">
                <table class="infoTable">
                    <tbody>
                    <tr>
                        <td class="infoKey"><i class="fa fa-info-circle"></i><?php echo lang('label_current_status'); ?>
                            :
                        </td>
                        <td class="infoVal"><strong
                                class="text-success"><?php echo ($primary_info->cl_user_active) ? lang('label_active') : lang('label_inactive'); ?></strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="infoKey"><i class="fa fa-calendar"></i><?php echo lang('label_join_date'); ?>:</td>
                        <td class="infoVal"><?php echo $primary_info->created_on; ?></td>
                    </tr>
                    <tr>
                        <td class="infoKey"><i class="fa fa-calendar"></i>Correo  :</td>
                        <td class="infoVal"><?php echo $primary_info->cl_user_email; ?></td>
                    </tr>
                    <tr>
                        <td class="infoKey"><i class="fa fa-phone"></i> <?php echo lang('label_phone'); ?> :</td>
                        <td class="infoVal"><?php echo $primary_info->cl_user_phone; ?></td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </fieldset>

    </div>
    <!-- /.col-sm-4 -->
    <div class="row">
<center>
        <div class="col-lg-8">
            <button type="submit"
                    class="btn btn-info btn-block"><?php echo lang('form_button_save'); ?></button>
        </div>
        </center>

    </div>
    </form>
</div><!-- /.row -->
