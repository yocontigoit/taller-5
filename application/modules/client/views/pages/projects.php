<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/12
 * Time: 11:28 AM
 */
?>
<div class="row">
    <div class="col-lg-12">
        <fieldset>
            <legend>Proyectos</legend>
<?php $count=1; if (count($clients_projects)) : foreach ($clients_projects as $project): ?>

            <div class="the-box col-lg-6">
                <div class="table-responsive">
                    <center><h3><?php echo $project->project_title; ?></h3></center>
                    <table class="table table-th-block table-success" id="zest_table<?php echo $count;?>">
                        <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Orden/Presupuesto</th>
                            <th>Monto Total</th>
                            <th>Descuento</th>
                            <th>Saldo</th>
                            <th>Método de pago</th>
                            <th>Estatus</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $totmount=0; $totbal=0; $pagos_real = 0; $totbal2 = 0; if (count($clients_orders)) : foreach ($clients_orders as $projects): if ($projects->project_id == $project->project_id): ?>
                            <tr>
                                <td><?php echo substr($projects->created_at,0,-8); ?></td>
                                <td><?php echo $projects->inv_no; ?></td>
                                <td>$
                            <?php $amount = invoice_total($projects->id);
                            echo format_currency($amount); $totmount = $totmount + $amount;?>
                        </td>


                                <td>$<?php echo $projects->disc_amount; ?></td>
                                <td>$<?php $balance = invoice_balance($projects->id);
                                echo format_currency($balance); $totbal = $totbal + $balance;?></td>




                                <td>
                                  <span><a href="/Taller5/expenses/view_statement/<?php echo $projects->paid; ?>"><?php echo payment_method($projects->paid); ?></a></span>
                                </td>
                                <td style="text-align:center;" >
                                  <?php if(payment_metodo($projects->paid) != null){ ?>
                                    <span class="label label-success"><?php echo payment_metodo($projects->paid);  ?></span>
                                  <?php }else{?>
                                  <span class="label label-info">Sin metodo de pago</span>
                                  <?php }  ?>
                              <?php $pagos_real = client_payments($projects->client_id,
                                      'year'); ?>
                            </td>

                            </tr>

                        <?php endif;endforeach; endif; ?>

                        </tbody>
                    </table>
                </div><center>
                <div class="col-md-6"><h4>Monto Total: $<?php echo format_currency($totmount);?></h4></div>
                <div class="col-md-6"><h4>Descuentos: $<?php echo format_currency(0);?></h4></div></center>

                <div class="col-md-6"><h4>Saldo: $<?php $totbal2 = $totmount - $pagos_real;
                 echo format_currency($totbal);?></h4></div></center>
                <!-- /.table-responsive -->
            </div>
            <?php $count++; endforeach; endif; ?>
            <!-- /.the-box -->
        </fieldset>

    </div>
</div>
