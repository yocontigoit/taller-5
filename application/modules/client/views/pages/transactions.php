<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/12
 * Time: 11:29 AM
 */
?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
           id="zest_table">
        <thead class="the-box dark full">
        <tr>
            <th>
                <?php echo lang('label_client_no'); ?>
            </th>
            <th>
                <?php echo lang('label_date'); ?>
            </th>
             <th>
                Obra
            </th>
            <th class="hidden-xs">
                <?php echo lang('label_payment_method'); ?>
            </th>
            <?php if ($client->client_type == 1): ?>
            <th>
                Estimación / pago 
            </th>
            <?php endif; ?>
            <?php if ($client->client_type != 1): ?>
            <th>
                <?php echo lang('label_invoice_no'); ?>
            </th>
            <?php endif; ?>
            <th>
                <?php echo lang('label_amount'); ?>
            </th>
            <th class="hidden-xs">
                <?php echo lang('label_options'); ?>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($transactions)): foreach ($transactions as $transaction): ?>
            <tr class="tr-link" data-link="<?php echo base_url("billing/invoice/" . ($transaction->id)); ?>">
                <td>
                    <?php echo client_company($transaction->client_id); ?>
                </td>
                <td>
                    <?php echo $transaction->created_at; ?>
                </td>
                <td><a href="project/overview/preview/<?php echo $transaction->project_id ?>">
                    <?php $project = get_project_details($transaction->project_id);
                    
                    echo $project->project_title; ?></a>
                </td>

                <?php $invoice_status = get_invoice_status($transaction->status);

                if ($transaction->payment_method == 1) {
                    $label = "success";
                    $_message = "Cuenta Corriente";
                } else if ($transaction->payment_method == 2) {
                    $label = "success";
                    $_message = "transferencia Bancomer";
                } else if ($transaction->payment_method == 3) {
                    $label = "success";
                    $_message = "Cheque Bancomer";
                } else if ($transaction->payment_method == 9) {
                    $label = "success";
                    $_message = "Caja Chica";
                } else {
                    $label = "success";
                    $_message = "";
                }

                ?>
                <td><span class="label label-<?php echo $label; ?>"><?php echo $_message; ?></span></td>

                <td class="hidden-xs">
                    <?php echo $transaction->inv_no; ?>
                </td>
                <td>
                    <?php echo $transaction->amount; ?>
                </td>
                <td class="hidden-xs">

                </td>
            </tr>
        <?php endforeach; ?>

        <?php endif; ?>

        </tbody>
    </table>
</div>