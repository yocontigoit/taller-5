<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/12
 * Time: 11:28 AM
 */
?>
<div class="row">
    <div class="col-lg-12">
        <fieldset>
            <legend><?php echo lang('label_statistics'); ?></legend>
            <div class="col-lg-6">
                <div class="row">


                    <div class="row billing_top_row">
                        <div class="billing_settings">
                            <ul class="left_box">
                                <li class="revenue_today">
                                    <div>
                                        <em>Total este mes</em>
                                        <span class="more"><?php echo format_currency(client_invoice_total($client_id,
                                                'month')); ?></span>
                                    </div>
                                </li>
                                <li class="revenue_month">
                                    <div>
                                        <em>Total de este año</em>
                                        <span class="more"><?php echo format_currency(client_invoice_total($client_id,
                                                'year')); ?></span>
                                    </div>
                                </li>
                                <li class="revenue_year">
                                    <div>
                                        <em>Total</em>
                                        <span class="more"><?php echo format_currency(client_invoice_total($client_id,
                                                null)); ?></span>
                                    </div>
                                </li>
                                <li class="revenue_year">
                                    <div>
                                        <em>Pagos este año</em>
                                        <span class="more"><?php echo format_currency(client_payments($client_id,
                                                'year')); ?></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 text-center">
                            <h4 class="small-heading">Total</h4>
										<span class="chart chart-widget-pie widget-easy-pie-1"
                                              data-percent="<?php echo client_sales_perc($client_id); ?>">
											<span class="percent"><?php echo client_sales_perc($client_id); ?></span>
										</span>
                        </div>
                        <!-- /.col-sm-4 -->

                        <!-- /.col-sm-4 -->
                        <div class="col-sm-4 text-center">
                            <h4 class="small-heading"><?php echo lang('label_closed_projects_all'); ?></h4>
										<span class="chart chart-widget-pie widget-easy-pie-3"
                                              data-percent="<?php echo client_project_complete_perc($client_id); ?>">
											<span
                                                class="percent"><?php echo client_project_complete_perc($client_id); ?></span>
										</span>
                        </div>
                        <!-- /.col-sm-4 -->
                    </div>
                    <div style="margin: 0 5% 0 5%;">
                        <div class="row">

                            <div class="panel panel-danger panel-heading">
                                <a href="" class="text-danger"><i
                                        class="fa fa-external-link"></i><?php echo lang('label_overdue_invoices'); ?>
                                </a>
                                <span class="pull-right text-danger">
                                    <?php echo format_currency(client_amount_overdue($client_id)); ?>
                                </span>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-6">
                <div id="client_statistics"></div>
            </div>
        </fieldset>
    </div>

</div>
