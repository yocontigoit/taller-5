<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/12
 * Time: 11:29 AM
 */
?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
           id="zest_table">
        <thead class="the-box dark full">
        <tr>
            <th>
                <?php echo lang('label_client_no'); ?>
            </th>
            <th>
                <?php echo lang('label_date'); ?>
            </th>
            <th class="hidden-xs">
                <?php echo lang('label_payment_method'); ?>
            </th>
            <th>
                <?php echo lang('label_invoice_no'); ?>
            </th>
            <th>
                <?php echo lang('label_amount'); ?>
            </th>
            <th class="hidden-xs">
                <?php echo lang('label_options'); ?>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php if (count($transactions)): foreach ($transactions as $transaction): ?>
            <tr class="tr-link" data-link="<?php echo base_url("billing/invoice/" . ($transaction->id)); ?>">
                <td>
                    <?php echo client_company($transaction->client_id); ?>
                </td>
                <td>
                    <?php echo $transaction->created_at; ?>
                </td>

                <?php $invoice_status = get_invoice_status($transaction->status);

                if ($invoice_status == "PAID") {
                    $label = "success";
                    $_message = invoice_payment_method($transaction->id);
                } else {
                    $label = "info";
                    $_message = "No payment received";
                }


                ?>
                <td><span class="label label-<?php echo $label; ?>"><?php echo $_message; ?></span></td>

                <td class="hidden-xs">
                    <?php echo $transaction->inv_no; ?>
                </td>
                <td>
                    <?php echo $transaction->amount; ?>
                </td>
                <td class="hidden-xs">

                </td>
            </tr>
        <?php endforeach; ?>

        <?php endif; ?>

        </tbody>
    </table>
</div>