<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/12
 * Time: 11:28 AM
 */
?>
<div class="row">
    <div class="col-lg-12">
        
        <div class="pull-right">
           <a class="btn btn-info" href="<?php echo base_url('client/pagar_estimacion/'. $client_id); ?>" data-toggle="zestModal"><i class="fa fa-money"></i>Ingreso</a> 
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
                   id="zest_table_client">
                <thead class="the-box dark full">
                <tr>
                    <th style="text-align:center;">
                        Estimacion/Pago
                    </th>
                    <th style="text-align:center;">
                        Proyecto/Obra

                    </th>

                    <th style="text-align:center;">
                        Fecha


                    </th>
                    <th style="text-align:center;">
                        Salida

                    </th>

                    <th style="text-align:center;">
                        Entrada

                    </th>
                    <th style="text-align:center;">
                        acumulado Traslados...
                    </th>
                    <th style="text-align:center;">
                        Utilizado Traslados...
                    </th>
                    <th style="text-align:center;">
                        <?php echo lang('label_payment_method'); ?>
                    </th>



                </tr>
                </thead>
                <tbody>
                <?php $tras=0; $count=1; $totest=0; if (count($estimaciones)) : foreach ($estimaciones as $est) : ?>
                    <tr>
                        <td style="text-align:center;">
                           <a style="color:#428BC8" href="<?php echo base_url('billing/billing/estimar_view/' . $est->project_id ."/". $est->number); ?>">Estimacion #<?php echo $est->number ?></a>
                        </td>
                        <td style="text-align:center;">
                            <a href="<?php echo base_url('client/invoice/' . $est->client_id); ?>"><?php echo project_company($est->project_id); ?></a>
                            
                        </td>
                        <td style="text-align:center;">
                           <?php echo substr($est->date, 0,-8); ?>
                        </td>

                        <td style="text-align:center;"><strong><?php echo format_amount($est->amount); $totest = $totest + $est->amount; ?></strong></td>

                        <td style="text-align:center;"></td>
                         <td style="font-size:10px;text-align:center;"><?php  $tras += ($est->amount * .03); echo format_currency($tras); ?></td>

                        <td style="text-align:center;"></td>

                        <?php $invoice_status = $est->status;

                        if ($invoice_status == "1") {
                            $label = "success";
                            $_message = "Pagado";
                        } else {
                            $label = "info";
                            $_message = "No hay pagos";
                        }

                        $count++;
                        ?>
                        <td style="text-align:center;"><span class="label label-<?php echo $label; ?>"><?php echo $_message; ?></span></td>




                    </tr>
                <?php endforeach; endif; ?>
                  <?php $totpag=0; $count=1; if (count($transactions)): foreach ($transactions as $pago): ?>
            <tr class="tr-link" data-link="<?php echo base_url("billing/invoice/" . ($pago->id)); ?>">
                    <td class="hidden-xs">Pago #
                    <?php echo $count;  ?>
                </td>
                <td>
                    <a href="<?php echo base_url('client/invoice/' . $pago->client_id); ?>"><?php echo project_company($pago->project_id); ?></a>
                </td>


                <td>
                    <?php echo $pago->dates; ?>
                </td>

                <?php $invoice_status = $pago->metodo;

                if ($invoice_status == "1") {
                    $label = "success";
                    $_message = "Efectivo";
                } else if ($invoice_status == "2") {
                    $label = "success";
                    $_message = "Transaccion";
                }else if ($invoice_status == "3") {
                    $label = "success";
                    $_message = "Cheque";
                }else {
                    $label = "info";
                    $_message = "No payment received";
                }


                ?>
                <td></td>
                  <td>
                    <?php echo format_currency($pago->monto); $totpag = $totpag + $pago->monto; ?>
                </td>
            <td></td>
            <td></td>
                <td><span class="label label-<?php echo $label; ?>"><?php echo $_message;

                $count++;

                ?></span></td>




            </tr>
        <?php endforeach; ?>

        <?php endif; ?>

        <tr><td style="background:#656565;" ></td><td style="background:#656565;"></td>
        <td style="background:#656565;color:#fff;" >TOTALES:</td>
        <td style="background:#656565;color:#fff;"><?php echo format_currency($totest);?></td>
        <td style="background:#656565;color:#fff;"><?php echo format_currency($totpag);?></td>
        <td style="background:#656565;color:#fff;"><?php echo format_currency($tras); ?> </td>
        <td style="background:#656565;color:#fff;"></td>
        <td style="background:#656565;color:#fff;"><?php echo format_currency($totpag - $totest);?></td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
