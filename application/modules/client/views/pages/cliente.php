<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/03
 * Time: 08:40 AM
 */
?>
<h2>Clientes</h2>
<div class="the-box">
    <div class="table-toolbar">
        <div class="btn-group">

            <?php echo anchor('client/add', lang('common_client_add') . ' <i class="fa fa-plus"></i>',
                array('class' => 'btn btn-danger')); ?>

        </div>

    </div>

    <div class="stats-counter-sec">

        <div class="row">
            <div class="col-md-4">
                <div class="stats-counter">
                    <h3><?php echo lang('label_active_clients'); ?></h3>
                    <span><?php echo active_clients(); ?></span>

                </div>
                <!-- Stats Counter -->
            </div>
            <div class="col-md-4">
                <div class="stats-counter">
                    <h3><?php echo lang('label_clients_with_projects'); ?></h3>
                    <span><?php echo clients_with_projects() ?></span>

                </div>
                <!-- Stats Counter -->
            </div>
            <div class="col-md-4">
                <div class="stats-counter">
                    <h3><?php echo lang('label_inactive_clients'); ?></h3>
                    <span><?php echo inactive_client(); ?> </span>
                </div>
                <!-- Stats Counter -->
            </div>

        </div>

    </div>
    <!-- Stats Counter Sec-->

    <div style="clear: both"></div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
               id="zest_table">
            <thead>
            <tr>
                <th>
                    Estado
                </th>
                <th>
                    Nombre del Cliente
                </th>
                <th class="hidden-xs">
                    Teléfono
                </th>
                <th>
                    Correo
                </th>

                <th>
                    <?php echo lang("label_options") ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php if (count($clients)): foreach ($clients as $client): ?>
                <tr class="tr-link" data-link="<?php echo base_url("client/view/" . $client->client_id); ?>">
                    <td>
                        <?php if ($client->client_status == 1) {

                            $label = "success";
                            $_message = lang('label_active');
                        } else {
                            $label = "danger";
                            $_message = lang('label_inactive_clients');
                        }
                        ?>
                        <span class="label label-<?php echo $label; ?>"><?php echo $_message; ?></span>
                    </td>
                    </td>
                    <td>
                        <?php echo $client->client_name; ?>
                    </td>
                    <td class="hidden-xs">
                      <?php echo $client->client_phone_no; ?>
                    </td>
                    <td>
                        <?php echo $client->client_email; ?>
                    </td>

                    <td>
                        <div class="btn-group-action">
                            <div class="btn-group">
                                <a class=" btn btn-info"
                                   href="<?php echo base_url('client/view/' . $client->client_id); ?>" title=""><i
                                        class="fa fa-eye"></i><?php echo lang('label_view'); ?></a>
                                <a class="btn btn-info"
                                           href="<?php echo base_url('client/delete/' . $client->client_id); ?>"
                                           title=""><i class="fa fa-trash"></i><?php echo lang('label_delete'); ?></a>

                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php endif; ?>


            </tbody>
        </table>
    </div>
</div>
