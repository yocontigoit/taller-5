<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/12
 * Time: 11:29 AM
 */
?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
           id="zest_table">
        <thead class="the-box dark full">
        <tr>
            <th>
                <?php echo lang('label_client_no'); ?>
            </th>
             <th>
                # de estimacion
            </th>
            <th>
                <?php echo lang('label_date'); ?>
            </th>
            <th class="hidden-xs">
                <?php echo lang('label_payment_method'); ?>
            </th>
          
            
            <th>
                <?php echo lang('label_amount'); ?>
            </th>
            
        </tr>
        </thead>
        <tbody>
        <?php if (count($transactions)): foreach ($transactions as $transaction): ?>
            <tr class="tr-link" data-link="<?php echo base_url("billing/invoice/" . ($transaction->id)); ?>">
                <td>
                    <?php echo client_company($transaction->client_id); ?>
                </td>
                 <td class="hidden-xs"># 
                    <?php echo $transaction->estimacion_id; ?>
                </td>
                <td>
                    <?php echo $transaction->dates; ?>
                </td>

                <?php $invoice_status = $transaction->metodo;

                if ($invoice_status == "1") {
                    $label = "success";
                    $_message = "Efectivo";
                } else if ($invoice_status == "2") {
                    $label = "success";
                    $_message = "Transaccion";
                }else if ($invoice_status == "3") {
                    $label = "success";
                    $_message = "Cheque";
                }else {
                    $label = "info";
                    $_message = "No payment received";
                }


                ?>
                <td><span class="label label-<?php echo $label; ?>"><?php echo $_message; ?></span></td>

               
                <td>
                    <?php echo format_currency($transaction->monto); ?>
                </td>
                
            </tr>
        <?php endforeach; ?>

        <?php endif; ?>

        </tbody>
    </table>
</div>