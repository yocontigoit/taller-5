<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/03
 * Time: 08:41 AM
 */
class Client extends Admin_Controller
{


    function __construct()
    {

        parent::__construct('client');

        $this->load->model('client_m');
    }

    /**
     * Get list of clients/customers
     */
    function index()
    {
        //$this->check_action_permission('search');

        if ($this->CompanyClient_id > 0) {
            $this->view($this->CompanyClient_id);
        } else {

            $this->data['content'] = 'client/index';

            $this->data['title'] = 'Terceros';
            $this->data['clients'] = $this->client_m->get_client_list();
            $this->data['clients2'] = $this->client_m->get_proveedor_list();
            $this->data['clients3'] = $this->client_m->get_contratista_list();
            $this->data['clients4'] = $this->client_m->get_albanil_list();
            $this->data['datatable'] = true;
$this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);
        }
    }


    /**
     * view select client
     */
    function view($client_id = null)
    {
        $this->check_action_permission('add_update');

        $client_id = ($client_id) ? $client_id : $this->uri->segment(3);
        $client_id = ($this->CompanyClient_id > 0) ? $this->CompanyClient_id : $client_id;
        $client_page = $this->input->get('page', true) ? $this->input->get('page', true) : 'summary';
        $this->data['client'] = $this->client_m->get_client_details($client_id);
        $this->data['content'] = 'client/details';
        $this->data['title'] = '     ';
        $this->data['active'] = $client_page;


        switch ($client_page) {

            case 'summary':
                $this->data['page_title'] = 'Overview';
                $this->data['client_id'] = intval($client_id);
                $this->data['show_client_chats'] = true;
                $this->data['SCRIPT_PAGE'] = true;
                $this->data['_PAGE'] = $this->load->view('pages/' . $client_page, $this->data, true);
                break;
            case 'invoices':
                $this->data['transactions'] = $this->client_m->client_all_invoice($client_id);
                $this->data['_PAGE'] = $this->load->view('pages/' . $client_page, $this->data, true);
                break;

                case 'estimaciones':
                    $this->data['client_id'] = intval($client_id);
                $this->data['estimaciones'] = $this->client_m->client_all_estimaciones($client_id);
                 $this->data['transactions'] = $this->client_m->get_est_trans($client_id);

                $this->data['_PAGE'] = $this->load->view('pages/' . $client_page, $this->data, true);
                break;

            case 'transactions':
                $this->data['client_id'] = intval($client_id);
                $this->data['transactions'] = $this->client_m->get_payment_trans($client_id);
                $this->data['_PAGE'] = $this->load->view('pages/' . $client_page, $this->data, true);
                break;

                case 'pagos':
                $this->data['client_id'] = intval($client_id);
                $this->data['transactions'] = $this->client_m->get_est_trans($client_id);
                $this->data['_PAGE'] = $this->load->view('pages/' . $client_page, $this->data, true);
                break;

            case 'profile':


                $this->data['client_details'] = $this->client_m->get_client_details($client_id);
                //get state
                //get company .. Type of the comapany
                $this->data['countries'] = $this->client_m->get_countries();
                $this->data['currencies'] = $this->client_m->get_currencies();
                $this->data['company_types'] = $this->client_m->get_company_types();
                $this->data['company_status'] = $this->client_m->get_company_status();
                $this->data['_PAGE'] = $this->load->view('pages/' . $client_page, $this->data, true);
                break;

            case 'contacts':

                $this->data['client_id'] = intval($client_id);
                $this->data['client_contacts'] = $this->client_m->get_client_contacts($client_id);
                $this->data['_PAGE'] = $this->load->view('pages/' . $client_page, $this->data, true);
                break;

            case 'projects':
                $this->data['clients_projects'] = $this->client_m->get_projects();
                $this->data['methods'] = $this->client_m->get_payments();
                $this->data['invoice_transactions'] = $this->client_m->get_invoice_transactions();
                $this->data['SCRIPT_PAGE'] = true;
                $this->data['datatable'] = true;
                $this->data['clients_orders'] = $this->client_m->get_orders($client_id);
                $this->data['_PAGE'] = $this->load->view('pages/' . $client_page, $this->data, true);
                break;

            default:
                $this->data['_PAGE'] = $this->load->view('pages/summary', $this->data, true);
                break;

        }

        $this->data['client_id'] = intval($client_id);
        $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);

    }

    /**
     * add new client
     */
    function add()
    {
        //check add permission
        $this->check_action_permission('add_update');
        $this->data['message'] = null;


        if ($this->input->post()) {




                $param = array(
                  "client_name" => $this->input->post('company_name'),
                  "client_code" => '1',
                  "client_status" => '1',
                  "client_type" => $this->input->post('client_type'),

                  "client_phone_no" => $this->input->post('phone_number'),
                  "client_address_1" => '1',
                  "client_address_2" => '1',
                  "client_vat_no" => '1',
                  "client_city" => $this->input->post('city'),
                  "client_country" => 'México',
                  "client_email" => $this->input->post('email'),
                  "client_currency" => 'MXN',
                  "company_id" => $this->Company_id
                );


                $client_id = $this->client_m->add_new_client($param);
                if ($client_id) {


                    $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                    $log_params = array(
                        'activity_user' => $activity_user,
                        'activity_user_type' => $this->CompanyUser_type,
                        'activity_module' => 'client',
                        'activity_details' => $this->input->post('company_name'),
                        'activity_module_id' => $client_id,
                        'activity_status' => "success",
                        'activity_date' => date('Y-m-d,H:i:s'),
                        'activity_event' => "Se agrego un nuevo cliente ",
                        'activity_icon' => 'fa fa-user'
                    );

                    $this->__activity_tracker($log_params);

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_client_add_success'));

                    redirect('client/view/' . $client_id);

                }


        } else {
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->data['content'] = 'client/add_customer';
            $this->data['title'] = 'Añadir Nuevo Cliente';
            $this->data['countries'] = $this->client_m->get_countries();
            $this->data['currencies'] = $this->client_m->get_currencies();
            $this->data['company_types'] = $this->client_m->get_company_types();
            $this->data['company_status'] = $this->client_m->get_company_status();
            $this->load->view('_main_layout', $this->data);
        }

    }

     function pagar_estimacion($client_id = null)
        {
            $this->data['SCRIPT_PAGE'] = true;
            $this->data['client_id'] = $client_id;
            $this->data['projects'] = $this->client_m->get_client_projects($client_id);
            $this->load->view('modal/pagar_estimacion', $this->data);

        }

 function pay_estimacion($client_id)
    {

        if ($this->input->post()) {




                $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $transaction = array(
                    "client_id" => $client_id,
                    "amount" => $this->input->post('amount'),
                    "method" => $this->input->post('method'),
                    "description" => $this->input->post('description'),
                    "account" => $this->input->post('account'),
                    "date"   => $this->input->post('date'),
                    "avance" => $this->input->post('avance'),
                    "project_id" => $this->input->post('project')

                );


                if ($this->client_m->add_payment($transaction)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', 'El pago ha sido realizado con éxito !');


                }
                if ($this->input->post('payment_send_mail') == 'on') {

                    $this->notifications->send_payment_notification($client_details->client_id, $invoice_id,
                        $this->input->post('payment_amount'));
                }

                redirect('client/view/'.$client_id.'/?page=estimaciones');


        }
    }
    function add2()
    {
        //check add permission
        $this->check_action_permission('add_update');
        $this->data['message'] = null;


        if ($this->input->post()) {


                $param = array(
                  "client_name" => $this->input->post('company_name'),
                  "client_code" => '1',
                  "client_status" => '1',
                  "client_type" => $this->input->post('client_type'),

                  "client_phone_no" => $this->input->post('phone_number'),
                  "client_address_1" => '1',
                  "client_address_2" => '1',
                  "client_vat_no" => '1',
                  "client_city" => $this->input->post('city'),
                  "client_country" => 'México',
                  "client_email" => $this->input->post('email'),
                  "client_currency" => 'MXN',
                  "company_id" => $this->Company_id
                );


                $client_id = $this->client_m->add_new_client($param);
                if ($client_id) {


                    $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                    $log_params = array(
                        'activity_user' => $activity_user,
                        'activity_user_type' => $this->CompanyUser_type,
                        'activity_module' => 'client',
                        'activity_details' => $this->input->post('company_name'),
                        'activity_module_id' => $client_id,
                        'activity_status' => "success",
                        'activity_date' => date('Y-m-d,H:i:s'),
                        'activity_event' => "Se agrego un nuevo cliente ",
                        'activity_icon' => 'fa fa-user'
                    );

                    $this->__activity_tracker($log_params);

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_client_add_success'));

                    redirect('client/view/' . $client_id);

                }


        } else {
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->data['content'] = 'client/add_customer2';
            $this->data['title'] = 'Añadir Nuevo Proveedor';
            $this->data['countries'] = $this->client_m->get_countries();
            $this->data['currencies'] = $this->client_m->get_currencies();
            $this->data['company_types'] = $this->client_m->get_company_types();
            $this->data['company_status'] = $this->client_m->get_company_status();
            $this->load->view('_main_layout', $this->data);
        }

    }
    function add3()
    {
        //check add permission
        $this->check_action_permission('add_update');
        $this->data['message'] = null;


        if ($this->input->post()) {


                $param = array(
                    "client_name" => $this->input->post('company_name'),
                    "client_code" => '1',
                    "client_status" => '1',
                    "client_type" => $this->input->post('client_type'),

                    "client_phone_no" => $this->input->post('phone_number'),
                    "client_address_1" => '1',
                    "client_address_2" => '1',
                    "client_vat_no" => '1',
                    "client_city" => $this->input->post('city'),
                    "client_country" => 'México',
                    "client_email" => $this->input->post('email'),
                    "client_currency" => 'MXN',
                    "company_id" => $this->Company_id
                );


                $client_id = $this->client_m->add_new_client($param);
                if ($client_id) {


                    $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                    $log_params = array(
                        'activity_user' => $activity_user,
                        'activity_user_type' => $this->CompanyUser_type,
                        'activity_module' => 'client',
                        'activity_details' => $this->input->post('company_name'),
                        'activity_module_id' => $client_id,
                        'activity_status' => "success",
                        'activity_date' => date('Y-m-d,H:i:s'),
                        'activity_event' => "Se agrego un nuevo cliente ",
                        'activity_icon' => 'fa fa-user'
                    );

                    $this->__activity_tracker($log_params);

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_client_add_success'));

                    redirect('client/view/' . $client_id);

                }


        } else {
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->data['content'] = 'client/add_customer3';
            $this->data['title'] = 'Añadir Nuevo Contratista';
            $this->data['countries'] = $this->client_m->get_countries();
            $this->data['currencies'] = $this->client_m->get_currencies();
            $this->data['company_types'] = $this->client_m->get_company_types();
            $this->data['company_status'] = $this->client_m->get_company_status();
            $this->load->view('_main_layout', $this->data);
        }

    }
    function add4()
    {
        //check add permission
        $this->check_action_permission('add_update');
        $this->data['message'] = null;


        if ($this->input->post()) {


                $param = array(
                    "client_name" => $this->input->post('company_name'),
                    "client_code" => '1',
                    "client_status" => '1',
                    "client_type" => $this->input->post('client_type'),

                    "client_phone_no" => $this->input->post('phone_number'),
                    "client_address_1" => '1',
                    "client_address_2" => '1',
                    "client_vat_no" => '1',
                    "client_city" => $this->input->post('city'),
                    "client_country" => 'México',
                    "client_email" => $this->input->post('email'),
                    "client_currency" => 'MXN',
                    "company_id" => $this->Company_id
                );


                $client_id = $this->client_m->add_new_client($param);
                if ($client_id) {


                    $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                    $log_params = array(
                        'activity_user' => $activity_user,
                        'activity_user_type' => $this->CompanyUser_type,
                        'activity_module' => 'client',
                        'activity_details' => $this->input->post('company_name'),
                        'activity_module_id' => $client_id,
                        'activity_status' => "success",
                        'activity_date' => date('Y-m-d,H:i:s'),
                        'activity_event' => "Se agrego un nuevo cliente ",
                        'activity_icon' => 'fa fa-user'
                    );

                    $this->__activity_tracker($log_params);

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_client_add_success'));

                    redirect('client/view/' . $client_id);

                }


        } else {
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $this->data['content'] = 'client/add_customer4';
            $this->data['title'] = 'Añadir Nuevo Albañil';
            $this->data['countries'] = $this->client_m->get_countries();
            $this->data['currencies'] = $this->client_m->get_currencies();
            $this->data['company_types'] = $this->client_m->get_company_types();
            $this->data['company_status'] = $this->client_m->get_company_status();
            $this->load->view('_main_layout', $this->data);
        }

    }


    /**
     * add activity log
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->client_m->add_log($activity_data);

    }

    /**
     * update client details
     */
    function update()
    {
        $this->check_action_permission('add_update');
        $client_id = intval($this->input->post('client_id'));

        if ($this->input->post()) {


            $param = array(
                "client_name" => $this->input->post('company_name'),
                "client_code" => $this->input->post('company_code'),
                "client_status" => $this->input->post('company_status'),
                "client_phone_no" => $this->input->post('phone_number'),
                "client_address_1" => $this->input->post('address1'),
                "client_address_2" => $this->input->post('address2'),
                "client_address_3" => $this->input->post('address3'),
                "client_vat_no" => $this->input->post('client_vat_no'),
                "client_zip" => $this->input->post('zip_code'),
                "client_city" => $this->input->post('city'),
                "client_country" => $this->input->post('country'),
                "client_website" => $this->input->post('website'),
                "client_currency" => $this->input->post('currency_name'),
                "client_email" => $this->input->post('email')
            );

            if ($this->client_m->update_client($param, $client_id)) {


                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'client',
                    'activity_details' => $this->input->post('company_name'),
                    'activity_module_id' => $client_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Se actualizo el cliente correctamente",
                    'activity_icon' => 'fa fa-user'
                );

                $this->__activity_tracker($log_params);

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_client_save_success'));

                redirect('client/view/' . $client_id);

            }

        } else {

            redirect('client/view/' . $client_id . '/?page=profile');

        }

    }

    /**
     * edit contact details
     * @param null $user_id
     */
    function edit_contact($user_id = null)
    {
        $this->check_action_permission('add_update');
        if ($this->input->post()) {
            $active_status = ($this->input->post('active') == 'on') ? 1 : 0;
            $client_id = intval($this->input->post('client_id'));
            $user_id = intval($this->input->post('user_id'));

            $user_data["cl_user_name"] = $this->input->post('contact_name');
            $user_data["cl_user_surname"] = $this->input->post('contact_username');
            $user_data["cl_user_address_1"] = $this->input->post('contact_address1');
            $user_data["cl_user_address_2"] = $this->input->post('contact_address2');
            $user_data["cl_user_address_3"] = $this->input->post('contact_address3');
            $user_data["client_email"] = $this->input->post('contact_email');
            $user_data["cl_user_phone"] = $this->input->post('contact_phone');
            // if($this->input->post('password')!='') {
            $user_data["cl_user_password"] = $this->input->post('password');
            // }
            $user_data["cl_user_active"] = $active_status;

            if ($this->ion_auth->add_cl_user($user_id, $user_data, true)) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'client',
                    'activity_details' => $this->input->post('contact_name'),
                    'activity_module_id' => $user_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Se actualizo la informacion del contacto correctamente",
                    'activity_icon' => 'fa fa-user'
                );

                $this->__activity_tracker($log_params);

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_contact_save_success'));

                redirect('client/view/' . $client_id . '/?page=contacts');
            }

        } else {
            $this->data['title'] = 'Edit Contact';
            $this->data['user_id'] = intval($user_id);
            $this->data['user_details'] = $this->client_m->get_contact_details($user_id);
            $this->load->view('modal/contact_edit', $this->data);
        }

    }

    /**
     * add new contact
     * @param null $client_id
     */
    function contact_add($client_id = null)
    {
        $this->check_action_permission('add_update');
        if ($this->input->post()) {

            $client_id = intval($this->input->post('client_id'));

            $active_status = ($this->input->post('active') == 'on') ? 1 : 0;
            $user_data["cl_user_name"] = $this->input->post('contact_name');
            $user_data["role_id"] = $this->input->post('client_type');
            $user_data["cl_user_avatar"] = 'default.jpg';
            $user_data["cl_user_surname"] = $this->input->post('contact_username');
            $user_data["cl_user_address_1"] = '1';
            $user_data["cl_user_address_2"] = '1';
            $user_data["cl_user_address_3"] = '1';
            $user_data["client_email"] = $this->input->post('contact_email');
            $user_data["cl_user_phone"] = $this->input->post('contact_phone');
            $user_data["cl_user_password"] = $this->input->post('password');
            $user_data["created_on"] = date('Y-m-d H:i:s');
            $user_data["cl_user_active"] = $active_status;

            if ($this->ion_auth->add_cl_user($client_id, $user_data, false)) {

                $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                $log_params = array(
                    'activity_user' => $activity_user,
                    'activity_user_type' => $this->CompanyUser_type,
                    'activity_module' => 'client',
                    'activity_details' => $this->input->post('contact_name'),
                    'activity_module_id' => $client_id,
                    'activity_status' => "success",
                    'activity_date' => date('Y-m-d,H:i:s'),
                    'activity_event' => "Se agrego un nuevo contacto correctamente",
                    'activity_icon' => 'fa fa-user'
                );

                $this->__activity_tracker($log_params);

                $this->session->set_flashdata('msg_status', 'success');
                $this->session->set_flashdata('message', lang('messages_contact_add_success'));

                redirect('client/view/' . $client_id . '/?page=contacts');
            }


        } else {
            $this->data['title'] = 'Add Contact';
            $this->data['client_id'] = intval($client_id);
            $this->load->view('modal/contact_add', $this->data);
        }
    }

    /**
     * Delete / remove selected client..
     * @param $client_id
     */
    function delete($client_id = null)
    {
        $this->check_action_permission('delete');
        if ($this->client_m->remove_client($client_id)) {

            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', lang('messages_client_delete_success'));
            $this->data['content'] = 'client/index';
            $this->data['title'] = 'Terceros';
            $this->data['clients'] = $this->client_m->get_client_list();
            $this->data['clients2'] = $this->client_m->get_proveedor_list();
            $this->data['clients3'] = $this->client_m->get_contratista_list();
           $this->data['datatable'] = true;
$this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);

        } else {

            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', 'Something went wrong deleting..');
            $this->data['content'] = 'client/index';
            $this->data['title'] = 'Terceros';
            $this->data['clients'] = $this->client_m->get_client_list();
            $this->data['clients2'] = $this->client_m->get_proveedor_list();
            $this->data['clients3'] = $this->client_m->get_contratista_list();

            $this->load->view('_main_layout', $this->data);
        }
    }

    /**
     * delete contact
     * @param null $user_id
     * @param null $client_id
     */
    function remove_contact($user_id = null, $client_id = null)
    {

        $this->check_action_permission('delete');
        if ($this->client_m->remove_contact($user_id)) {

            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', lang('messages_contact_delete_success'));
            $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $log_params = array(
                'activity_user' => $activity_user,
                'activity_user_type' => $this->CompanyUser_type,
                'activity_module' => 'client',
                'activity_details' => $this->input->post('contact_name'),
                'activity_module_id' => $client_id,
                'activity_status' => "success",
                'activity_date' => date('Y-m-d,H:i:s'),
                'activity_event' => "Se elimino el contacto correctamente",
                'activity_icon' => 'fa fa-user'
            );

            $this->__activity_tracker($log_params);

        } else {

            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', 'Something went wrong deleting..');
        }

        redirect('client/view/' . $client_id . '/?page=contacts');
    }

    /**
     * set contact to primary contact
     * @param null $user_id
     * @param null $client_id
     */
    function mark_primary($user_id = null, $client_id = null)
    {

        $this->check_action_permission('add_update');

        if ($this->client_m->set_primary($user_id, $client_id)) {

            $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
            $log_params = array(
                'activity_user' => $activity_user,
                'activity_user_type' => $this->CompanyUser_type,
                'activity_module' => 'client',
                'activity_details' => "Contact set as primary contact",
                'activity_module_id' => $client_id,
                'activity_status' => "success",
                'activity_date' => date('Y-m-d,H:i:s'),
                'activity_event' => "Contacto asignado como principal",
                'activity_icon' => 'fa fa-user'
            );

            $this->__activity_tracker($log_params);

            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', lang('messages_contact_primary_success'));


        } else {
            $this->session->set_flashdata('msg_status', 'error');
            $this->session->set_flashdata('message', 'Something went wrong deleting..');
        }

        redirect('client/view/' . $client_id . '/?page=contacts');

    }

    /**
     * check for email duplication
     * @param $str
     * @return bool
     */
    public function _unique_email($str)
    {

        //DO NOT validate if email exists
        //UNLESS it the email for the current user

        $email = $this->input->post('email');
        $client = $this->client_m->get_client_email($email, $this->CompanyClient_id);
        if (count($client)) {
            $this->form_validation->set_message('_unique_email', '%s should be unique');

            return false;
        }

        return true;
    }

    function nota_credito()
       {

          $this->data['clients'] = $this->client_m->get_proveedor_list();
           $this->data['SCRIPT_PAGE'] = true;

           $this->load->view('modal/nota_credito', $this->data);

       }

       function add_nota_credito()
          {

              if ($this->input->post()) {

                      $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                      $param = array(
                          "project_id" => 0,
                          "client_id" => $this->input->post('client_id'),
                          "monto" => $this->input->post('amount'),
                          "date"   => $this->input->post('date')
                      );

                      if ($this->client_m->add_nota_credito($param)) {

                          $this->session->set_flashdata('msg_status', 'success');
                          $this->session->set_flashdata('message', 'Nota de credito agregada con éxito !');

                      }
                      // if ($this->input->post('payment_send_mail') == 'on') {
                      //
                      //     $this->notifications->send_payment_notification($client_details->client_id, $invoice_id,
                      //         $this->input->post('payment_amount'));
                      // }
                      redirect('client/');
              }
          }

}
