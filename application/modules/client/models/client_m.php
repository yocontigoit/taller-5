<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/03
 * Time: 08:48 AM
 */
class Client_m extends CI_Model
{


    /**
     * Check if a client exists
     * @param $client_id
     * @return bool
     */
    function client_exists($client_id)
    {

        $this->db->from("customers");
        $this->db->where('customers.client_id', $client_id);
        $query = $this->db->get();

        return ($query->num_rows() == 1);

    }

    /**
     * get currencies
     * @return mixed
     */
    function get_currencies()
    {

        return $this->db->get('settings_currency')->result();
    }
     function get_etapas()
    {
 $this->db->from("milestones");
              $this->db->where('tipo', 1);
               $this->db->where('project_id', 32);

        return $this->db->get()->result();
    }

    /**
     * Check if a client is already registered..
     * @param $email
     * @param $client_id
     * @return mixed
     */
    function get_client_email($email = '', $client_id)
    {


        $this->db->where('client_email', $email);
        if ($client_id > 0) {
            $this->db->where('client_id != ', $client_id);
        }

        return $this->db->get('client')->row();
    }

    /**
     * Add new client
     * @param array $data
     * @return bool
     */
    function add_new_client($data = array())
    {
        $success = false;

        $this->db->trans_start();
        $success = $this->db->insert('client', $data);
        $client_id = $this->db->insert_id();


        $this->db->trans_complete();

        return ($success) ? $client_id : null;
    }

    /**
     * update client information
     * @param array $data
     * @param null $client_id
     * @return bool
     */
    function update_client($data = array(), $client_id = null)
    {

        $success = false;
        $success = $this->db->update('client', $data, array('client_id' => intval($client_id)));

        return $success;

    }

    /**
     * Retrieve all clients or customers
     */
    function get_client_list()
    {

        $this->db->from("client");
              $this->db->where('client_type', 1);

        return $this->db->get()->result();

    }
    function get_proveedor_list()
    {

        $this->db->from("client");
        $this->db->where('client_type', 2);

        return $this->db->get()->result();

    }
  function get_contratista_list()
  {

      $this->db->from("client");
      $this->db->where('client_type', 3);

      return $this->db->get()->result();

  }
  function get_albanil_list()
  {

      $this->db->from("client");
      $this->db->where('client_type', 4);

      return $this->db->get()->result();

  }

    /**
     * Get client details
     * @param $client_id
     * @return stdClass
     */
    function get_client_details($client_id)
    {

        $this->db->from('client');
        $this->db->where('client_id', $client_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {

            $fields = $this->db->list_fields('client');
            $client_obj = new stdClass;

            foreach ($fields as $field) {
                $client_obj->$field = '';
            }

            return $client_obj;

        }
    }



    /**
     * Get client contact
     * @param $client_id
     * @return mixed
     */
    function get_client_contacts($client_id)
    {

        $this->db->from('client_user');
        $this->db->where('client_id', $client_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    /**
     * Get client contact information
     * @param null $user_id
     * @return mixed
     */
    function get_contact_details($user_id = null)
    {

        $this->db->from('client_user');
        $this->db->where('cl_user_id', $user_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }


    /**
     * Add client activities
     * @param array $data
     * @return bool
     */
    function add_log($data = array())
    {

        $success = false;
        $success = $this->db->insert("activity_log", $data);

        return $success;
    }
    function add_payment($data = array())
    {

        $success = false;
        $success = $this->db->insert("est_trans", $data);

        return $success;
    }

    /**
     * get client projects
     * @param $client_id
     * @return mixed
     */
    function get_client_projects($client_id)
    {

        return $this->db->where('client_id', $client_id)->get("projects")->result();
    }

function get_projects()
    {

        return $this->db->get("projects")->result();
    }

    function get_payments()
    {

        return $this->db->get("settings_payment_methods")->result();
    }

     function get_invoice_transactions()
    {
        $this->db->from('transaction');
         $this->db->join('settings_payment_methods', 'settings_payment_methods.setting_id = transaction.payment_method');

        return $this->db->get()->result();

    }

    function get_orders($client_id)
    {

        return $this->db->where('client_id', $client_id)->get("invoice")->result();
    }

    /**
     * get all client payments
     * @param $client_id
     * @return mixed
     */
    function get_payment_trans($client_id)
    {

        $this->db->from("transaction");
        $this->db->join('invoice', 'invoice.id = transaction.invoice_id');
        $this->db->where("invoice.client_id = " . $client_id);

        return $this->db->get()->result();
    }

     function get_est_trans($client_id)
    {
        $this->db->select('*, est_trans.id as ided, est_trans.amount as monto, est_trans.method as metodo, est_trans.date as dates, est_trans.client_id as client_ided, est_trans.client_id as clientid');
        $this->db->from("est_trans");
        $this->db->where("est_trans.client_id = " . $client_id);

        return $this->db->get()->result();
    }

    /**
     * get all client invoices
     * @param $client_id
     * @return mixed
     */
    function client_all_invoice($client_id)
    {

        $this->db->from('invoice');
        $this->db->where("client_id", $client_id);

        return $this->db->get()->result();

    }

    function client_all_estimaciones($client_id)
     {

        $this->db->from('estimaciones');
        $this->db->where("client_id", $client_id);

        return $this->db->get()->result();

    }



    /**
     * remove selected client
     * @param $client_id
     */
    function remove_client($client_id)
    {
        $success = false;

        $this->db->where("client_id = " . $client_id);
        $success = $this->db->delete('client');


        return $success;
    }

    function remove_contact($user_id)
    {

        $success = false;
        $this->db->where("cl_user_id = " . $user_id);
        $success = $this->db->delete("client_user");


        return $success;
    }

    /**
     * set client contact to primary
     * @param $user_id
     * @param $client_id
     * @return bool
     */
    function set_primary($user_id, $client_id)
    {

        $success = false;
        $this->db->trans_start();
        $user_id = intval($user_id);
        $client_id = intval($client_id);

        $success = $this->db->update('client_user', array('cl_user_primary' => 0), array('client_id' => $client_id));
        if ($success) {
            $success = $this->db->update('client_user', array('cl_user_primary' => 1), array('cl_user_id' => $user_id));
        }
        $this->db->trans_complete();

        return $success;
    }

    /**
     * get client type
     * @return mixed
     */
    function get_company_types()
    {

        return $this->db->get("client_type")->result();
    }

    /**
     * get company status
     * @return mixed
     */
    function get_company_status()
    {

        return $this->db->get("client_status")->result();
    }


    function get_countries()
    {
        return $this->db->get('country')->result();
    }

    function add_nota_credito($data = array())
    {

        $success = false;
        $success = $this->db->insert("nota_credito", $data);

        return $success;
    }

}
