<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/08
 * Time: 03:18 PM
 */
?>
<div class="content-page">
    <!-- ============================================================== -->
    <!-- Start Content here -->
    <!-- ============================================================== -->

    <div class="profile-banner"
         style="background-image:url('<?php echo base_url('assets/images/' . $this->config->item('theme')->profile_background); ?>')">

        <div class="col-sm-3 avatar-container">
            <img src="<?php echo base_url('files/profile_images/' . $user_info->cl_user_avatar); ?>"
                 class="img-circle profile-avatar" alt="User avatar">
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-sm-3">
                <!-- Begin user profile -->
                <div class="text-center user-profile-2">
                    <h4><?php echo $user_info->cl_user_name . " " . $user_info->cl_user_surname; ?></b></h4>

                    <h5>Client</h5>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="badge badge-info"> <?php echo $user_info->cl_last_login; ?></span>
                            <?php echo lang('label_last_login'); ?> :
                        </li>

                    </ul>

                    <!-- User button -->
                    <div class="user-button">
                        <div class="the-box">
                            <?php echo form_open_multipart('user/change_image',
                                array('class' => 'form-horizontal', 'role' => 'form')); ?>
                            <h5><?php echo lang('label_change_picture'); ?></h5>

                            <input type="hidden" name="cl_user_id" value="<?php echo($user_info->cl_user_id); ?>">

                            <div class="form-group">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                         style="max-width: 200px; max-height: 150px;">
                                    </div>

                                    <div>
																<span class="btn btn-default btn-file">
																	<span class="fileinput-new">
																		 Select image
																	</span>
																	<span class="fileinput-exists">
																		 Change
																	</span>
																	<input type="file" name="user_picture">
																</span>
                                        <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                                            Remove
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <button type="submit"
                                    class="btn btn-sm btn-success"><?php echo lang('label_change_avatar'); ?></button>

                        </div>
                        <?php echo form_close(); ?>
                    </div>
                    <!-- End div .user-button -->
                </div>
                <!-- End div .box-info -->
                <!-- Begin user profile -->


                <div class="the-box user-content">
                    <?php
                    $customer_details = get_client_details($this->CompanyClient_id);
                    ?>


                    <table class="infoTable">
                        <tbody>
                        <tr>
                            <td class="infoKey"><i class="fa fa-user"></i><?php echo lang('label_client_name'); ?>:</td>
                            <td class="infoVal"><strong
                                    class="text-success"><?php echo $customer_details->client_name; ?></strong></td>
                        </tr>
                        <tr>
                            <td class="infoKey"><i class="fa fa-map"></i><?php echo lang('label_country'); ?>:</td>
                            <td class="infoVal"><strong
                                    class="text-success"><?php echo get_country_name($customer_details->client_country); ?></strong>
                            </td>
                        </tr>
                        <tr>
                            <td class="infoKey"><i class="fa fa-calendar"></i><?php echo lang('label_city'); ?>:</td>
                            <td class="infoVal"><?php echo $customer_details->client_city; ?></td>
                        </tr>

                        <tr>
                            <td class="infoKey"><i class="fa fa-phone"></i> <?php echo lang('label_phone'); ?> :</td>
                            <td class="infoVal"><?php echo $customer_details->client_phone_no; ?></td>
                        </tr>

                        </tbody>
                    </table>

                </div>

                <div class="the-box">
                    <div id="client-user-stats" style="height: 250px;"></div>
                </div>
            </div>
            <!-- End div .col-sm-4 -->

            <div class="col-sm-9">
                <?php //@TODO: Show stats... Check this ?>

                <form role="form">
                    <div class="col-sm-6">
                        <div class="the-box">
                            <h4 class="small-title"><i
                                    class="fa fa-envelope"></i><?php echo lang('label_profile_information'); ?>:</h4>

                            <div class="form-group">
                                <label><?php echo lang('label_emp_name'); ?></label>
                                <input type="text" class="form-control" value="<?php echo $user_info->cl_user_name; ?>"
                                       name="name">

                                <p class="help-block">First name not required</p>
                            </div>
                            <div class="form-group">
                                <label><?php echo lang('label_emp_surname'); ?></label>
                                <input type="text" class="form-control"
                                       value="<?php echo $user_info->cl_user_surname; ?>" name="surname">

                                <p class="help-block">Last name not required</p>
                            </div>
                            <div class="form-group">
                                <label><?php echo lang('label_location_address_1'); ?></label>
                                <input type="text" class="form-control"
                                       value="<?php echo $user_info->cl_user_address_1; ?>" name="emp_address_1">

                                <p class="help-block">Last name not required</p>
                            </div>
                            <div class="form-group">
                                <label><?php echo lang('label_location_address_2'); ?></label>
                                <input type="text" class="form-control"
                                       value="<?php echo $user_info->cl_user_address_2; ?>" name="emp_address_2">

                                <p class="help-block">Last name not required</p>
                            </div>
                            <div class="form-group">
                                <label><?php echo lang('label_location_address_3'); ?></label>
                                <input type="text" class="form-control"
                                       value="<?php echo $user_info->cl_user_address_3; ?>" name="emp_address_3">

                                <p class="help-block">Last name not required</p>
                            </div>
                            <div class="form-group">
                                <label><?php echo lang('label_emp_email_address'); ?></label>
                                <input type="email" class="form-control"
                                       value="<?php echo $user_info->cl_user_email; ?>" name="email">
                            </div>
                            <div class="form-group">
                                <label><?php echo lang('label_location_country'); ?></label>

                                <select class="form-control  chosen-select" id="user_country" name="user_country">
                                    <optgroup label="<?= lang('label_selected_country') ?>">

                                        <?php foreach ($countries as $country): ?>
                                            <?php if ($user_info->cl_user_country == $country->id_country): ?>
                                                <option
                                                    value="<?php echo $country->id_country; ?>"><?php echo $country->country_name; ?></option>
                                            <?php endif ?>
                                        <?php endforeach; ?>
                                    </optgroup>
                                    <optgroup label="<?= lang('label_other_countries') ?>">
                                        <?php foreach ($countries as $country): ?>
                                            <option
                                                value="<?php echo $country->id_country; ?>"><?php echo $country->country_name; ?></option>
                                        <?php endforeach; ?>
                                    </optgroup>

                                </select>

                            </div>
                            <div class="form-group">
                                <label><?php echo lang('label_location_city'); ?></label>
                                <input type="text" class="form-control" value="<?php echo $user_info->cl_user_city; ?>"
                                       name="user_city">
                            </div>
                            <div class="form-group">
                                <label><?php echo lang('label_emp_job_desc'); ?></label>
                                <input type="text" class="form-control"
                                       value="<?php echo $user_info->cl_user_job_title; ?>" name="user_job_title">
                            </div>

                        </div>
                        <!-- /.the-box -->
                    </div>

                    <div class="col-sm-6">
                        <div class="the-box">
                            <h4 class="small-title"><i class="fa fa-envelope"></i> Primary Contact:</h4>

                            <div class="form-group">
                                <label><?php echo lang('label_username'); ?></label>
                                <input type="text" class="form-control" value="<?php echo $user_info->cl_user_name; ?>">
                            </div>

                            <div class="form-group">
                                <label><?php echo lang('label_password'); ?></label>
                                <input type="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Is Primary Contact</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="user_primary"
                                               class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($user_info->cl_user_primary == 1) ? 'checked' : ''; ?> />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Active</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="user_active"
                                               class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($user_info->cl_user_active == 1) ? 'checked' : ''; ?> />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.the-box -->

                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="button" class="btn btn-success btn-block btn-lg"><i
                                    class="fa fa-envelope"></i> <?php echo lang('form_button_update'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->
</div>