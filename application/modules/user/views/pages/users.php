<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/11/17
 * Time: 08:42 AM
 */
?>
<div class="portlet-body">
    <div class="table-toolbar">
        <div class="btn-group">

            <?php echo anchor('user/add',  '<h4>Nuevo Usuario<i class="fa fa-plus"></i></h4>',
                array('class' => 'btn btn-info')); ?>

        </div>
        <div class="btn-group pull-right">
            <button class="btn dropdown-toggle" data-toggle="dropdown">Herramientas <i class="fa fa-angle-down"></i>
            </button>
            <ul class="dropdown-menu pull-right">
                <li>
                    <a href="#">
                        Imprimir
                    </a>
                </li>
                <li>
                    <a href="#">
                        Guardar como PDF
                    </a>
                </li>
                <li>
                    <a href="#">
                        Exportar a Excel
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success"
               id="zest_table">
            <thead class="the-box dark full">

            <tr>
                <th>
                    <?php echo lang("common_name") ?>
                </th>
                <th class="hidden-xs">
                    <?php echo lang("common_surname") ?>
                </th>
                <th class="hidden-xs">
                    <?php echo lang("common_email") ?>
                </th>
                <th class="hidden-xs">
                    <?php echo lang('label_last_login'); ?>
                </th>
                <th>
                    <?php echo lang("common_status") ?>
                </th>
                <th>
                    <?php echo lang("common_edit") ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php if (count($users)): foreach ($users as $user): ?>
                <tr>
                    <td>
                        <?php echo $user->emp_name; ?>
                    </td>
                    <td class="hidden-xs">
                        <?php echo $user->emp_surname; ?>
                    </td>
                    <td class="hidden-xs">
                        <?php echo $user->email; ?>
                    </td>
                    <td class="hidden-xs">

                        <i class="fa fa-clock-o"></i> <strong
                            class="text-info"><?php echo ago(date_to_timestamp($user->last_login)) . " "; ?></strong><?php echo lang('ago'); ?>

                    </td>
                    <td>
                        <?php

                        if ($user->active == 1) {
                            $label = "success";
                            $active_message = "Activo";
                        } else {
                            $label = "danger";
                            $active_message = "Bloqueado";
                        }
                        ?>
                        <span class="label label-<?php echo $label; ?>"><?php echo $active_message; ?></span>
                    </td>
                    <td>

                        <div class="btn-group-action">
                            <div class="btn-group">
                                <a class=" btn btn-info"
                                   href="<?php echo base_url('user/edit/' . ($user->user_id * 99)); ?>" title=""><i
                                        class="fa fa-eye"></i><?php echo lang('label_view'); ?></a>
                                <?php if ($this->CompanyUser_id != $user->user_id): ?>
                                    <button type="button" class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown">
                                        <span class="caret">&nbsp;</span>
                                    </button>
                                    <ul class="dropdown-menu pull-right text-info">
                                        <li><a data-toggle="zestModal"
                                               href="<?php echo base_url('user/delete/' . ($user->user_id * 99)); ?>"
                                               title=""><i class="fa fa-trash"></i><?php echo lang('label_delete'); ?>
                                            </a></li>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="6"><?php echo lang("messages_no_user_found") ?></td>
                </tr>
            <?php endif; ?>

            </tbody>
        </table>
    </div>
</div>
