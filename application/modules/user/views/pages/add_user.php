<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/08
 * Time: 03:18 PM
 */
?>
<div class="content-page">
    <div class="content">
        <div class="row">
            <?php echo form_open_multipart('user/add', array('class' => '', 'role' => 'form')); ?>
            <div class="col-sm-3">
                <div class="the-box">
                    <h5><?php echo lang('label_change_picture'); ?></h5>

                    <div class="form-group">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px;">
                            </div>
                            <div>
																<span class="btn default btn-file">
																	<span class="fileinput-new">
																		 Seleccionar imágen
																	</span>
																	<span class="fileinput-exists">
																		 Cambiar
																	</span>
																	<input type="file" name="user_picture">
																</span>
                                <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                                    Eliminar
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- End div .col-sm-4 -->

            <div class="col-sm-9">
                <?php
                if ($this->session->flashdata('message')) { ?>
                    <div class="alert alert-danger hidden-print">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <i class="fa fa-warning"></i>
                        <?php echo $this->session->flashdata('message'); ?>
                    </div>
                <?php } ?>

                <div class="col-sm-8">

                    <div class="the-box">
                        <h4 class="small-title"><i class="fa fa-envelope"></i>Información de perfil:</h4>

                        <div class="form-group">
                            <label><?php echo lang('label_emp_name'); ?><span class="text-danger">*</span></label>

                            <input type="text" class="form-control" value="" name="emp_name" required>

                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_emp_surname'); ?><span class="text-danger">*</span></label>

                            <input type="text" class="form-control" value="" name="emp_surname" required>

                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_location_address_1'); ?></label>

                            <input type="text" class="form-control" value="" name="emp_address_1">

                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_location_address_2'); ?></label>

                            <input type="text" class="form-control" value="" name="emp_address_2">

                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_location_address_3'); ?></label>

                            <input type="text" class="form-control" value="" name="emp_address_3">

                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_emp_email_address'); ?><span
                                    class="text-danger">*</span></label>

                            <input type="email" class="form-control" value="" name="emp_email">

                        </div>


                        <div class="form-group">
                            <label><?php echo lang('label_location_country'); ?></label>

                            <select class="form-control  chosen-select" id="emp_country" name="emp_country">
                                <optgroup label="<?= lang('label_selected_country') ?>">

                                    <?php foreach ($countries as $country): ?>
                                        <?php if ($user_info->emp_country == $country->id_country): ?>
                                            <option
                                                value="<?php echo $country->id_country; ?>"><?php echo $country->country_name; ?></option>
                                        <?php endif ?>
                                    <?php endforeach; ?>
                                </optgroup>
                                <optgroup label="<?= lang('label_other_countries') ?>">
                                    <?php foreach ($countries as $country): ?>
                                        <option
                                            value="<?php echo $country->id_country; ?>"><?php echo $country->country_name; ?></option>
                                    <?php endforeach; ?>
                                </optgroup>

                            </select>

                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_location_city'); ?></label>

                            <input type="text" class="form-control" name="emp_city">

                        </div>

                        <div class="form-group">
                            <label><?php echo lang('label_language'); ?></label>

                            <input type="text" class="form-control" name="emp_language">

                        </div>

                        <div class="form-group">
                            <label><?php echo lang('label_emp_job_desc'); ?></label>

                            <textarea class="form-control rounded" name="emp_job_desc"></textarea>

                        </div>
                    </div>
                    <!-- /.the-box -->

                </div>

                <div class="col-sm-4">
                    <div class="the-box">
                        <h4 class="small-title"><i class="fa fa-envelope"></i> Información de seguridad:</h4>

                        <div class="form-group">
                            <label><?php echo lang('label_username'); ?><span class="text-danger">*</span></label>
                            <input type="username" name="username" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label><?php echo lang('label_password'); ?></label>
                            <input type="password" name="password" class="form-control">
                        </div>

                        <div class="form-group">
                            <label><?php echo lang('label_roles'); ?></label>

                            <select class="form-control" name="emp_role">
                                <option value="">-- Selecciona un rol --</option>
                                <?php foreach ($roles as $role): ?>
                                    <option
                                        value="<?php echo $role->role_id; ?>"><?php echo $role->role_name; ?></option>
                                <?php endforeach; ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label><?php echo lang('label_active'); ?></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="checkbox" name="active"
                                           class="ios-switch ios-switch-success ios-switch-lg"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.the-box -->
                    <div class="the-box">
                        <h4 class="small-title"><i class="fa fa-envelope"></i> Pago:</h4>

                        <div class="form-group">
                            <label><?php echo lang('label_emp_hourly_cost'); ?></label>
                            <input type="text" class="form-control" name="emp_hourly_cost">
                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_emp_working_hours'); ?></label>
                            <input type="text" class="form-control" name="emp_working_hours">
                        </div>


                    </div>
                    <!-- /.the-box -->
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-success btn-block btn-lg"><i
                                class="fa fa-sign-in"></i> <?php echo lang('form_button_save'); ?></button>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>