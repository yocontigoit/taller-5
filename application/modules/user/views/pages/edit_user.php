<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/08
 * Time: 03:18 PM
 */
?>
<div class="content-page">
    <!-- ============================================================== -->
    <!-- Start Content here -->
    <!-- ============================================================== -->
    <div class="profile-banner"
         style="background-image:url('<?php echo base_url('assets/images/' . $this->config->item('theme')->profile_background); ?>')">
        <div class="col-sm-3 avatar-container">
            <!--256 -->

            <img src="<?php echo base_url('files/profile_images/' . $user_info->emp_avatar); ?>"
                 style="height: 192px;height: 192px" class="img-circle profile-avatar" alt="User avatar">

        </div>
        <?php if ($this->CompanyUser_id != $user_info->emp_id): ?>
            <div class="col-sm-12 profile-actions text-right">
                <div class="btn-group ">
                    <a data-toggle="zestModal" class="btn btn-primary btn-sm"
                       href="<?php echo base_url('user/send_message/' . $user_info->emp_id); ?>"><i
                            class="fa fa-envelope"></i> Enviar Mensaje</a>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="content">
        <div class="row">

            <div class="col-sm-3">
                <!-- Begin user profile -->
                <div class="text-center user-profile-2">
                    <h4><?php echo $user_info->emp_name . " " . $user_info->emp_surname; ?></b></h4>
                    <h5><?php echo lang('label_administrator'); ?></h5>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="badge badge-info"> <?php echo $user_info->last_login; ?></span>
                            <?php echo lang('label_last_login'); ?> :
                        </li>

                    </ul>
                </div>
                <!-- End div .box-info -->
                <!-- Begin user profile -->
                <div class="the-box">
                    <?php echo form_open_multipart('user/change_image',
                        array('class' => 'form-horizontal', 'role' => 'form')); ?>
                    <h5><?php echo lang('label_change_picture'); ?></h5>

                    <input type="hidden" name="user_id" value="<?php echo($user_info->emp_id); ?>">

                    <div class="form-group">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px;">
                            </div>

                            <div>
																<span class="btn btn-info btn-file">
																	<span class="fileinput-new">
																		 Seleccionar Foto
																	</span>
																	<span class="fileinput-exists">
																		 Cambiar
																	</span>
																	<input type="file" name="user_picture">
																</span>
                                <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                                    Eliminar
                                </a>
                            </div>
                        </div>
                    </div>

                    <button type="submit"
                            class="btn btn-sm btn-success" style="background:#828282;text-shadow:0px 0px 0px #fff;border-width:0px;"><?php echo lang('label_change_avatar'); ?></button>

                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- End div .col-sm-4 -->
            <?php echo form_open('user/edit/' . ($user_info->emp_id * 99),
                array('id' => 'userForm', 'role' => 'form')); ?>
            <div class="col-sm-9">
                <?php //@TODO: Show stats... Check this ?>
                <!-- Statistics -->
                <div class="row the-box">
                    <div class="col-sm-12">
                        <div class="col-sm-3">
                            <div class="inline">
                                <?php $percentage = project_completion_percentage($user_info->emp_id); ?>
                                <div class="chart easy-pie-chart-4 text-info"
                                     data-percent="<?php echo $percentage; ?>" data-line-width="5" data-rotate="270"
                                     data-scale-Color="false" data-size="120" data-animate="2000">
                                    <span class="h2 step font-bold"><?php echo $percentage; ?></span>%

                                    <div
                                        class="easypie-text text-muted"><?php echo user_complete_projects($user_info->emp_id) . "/" . user_assigned_projects($user_info->emp_id); ?></div>
                                </div>
                                <div class="font-bold m-t">Trabajo Hecho/estimado</div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="panel panel-info panel-square panel-no-border text-center">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?php echo lang('label_open_projects'); ?></h3>
                                </div>
                                <div class="panel-body">
                                    <h1 class="bolded tiles-number text-info"><?php echo user_open_projects($user_info->emp_id); ?></h1>

                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel panel-success panel-block-color -->
                        </div>
                        <div class="col-sm-3">
                            <div class="panel panel-success panel-square panel-no-border text-center">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?php echo lang('label_complete_projects'); ?></h3>
                                </div>
                                <div class="panel-body">
                                    <h1 class="bolded tiles-number text-info"><?php echo user_complete_projects($user_info->emp_id); ?></h1>

                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel panel-success panel-block-color -->
                        </div>
                        <div class="col-sm-3">
                            <div class="panel panel-info panel-square panel-no-border text-center">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?php echo lang('label_logged_hours'); ?></h3>
                                </div>
                                <div class="panel-body">
                                    <h1 class="bolded tiles-number text-info"><?php echo round(user_logged_hours($user_info->emp_id),
                                            4); ?></h1>

                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel panel-success panel-block-color -->
                        </div>

                    </div>


                </div>
                <!-- /statistics -->



                <div class="col-sm-6">
                  <div class="the-box">
                      <h4 class="small-title"><i
                              class="fa fa-envelope"></i> <?php echo lang('label_profile_information'); ?>:</h4>

                      <div class="form-group">
                          <label><?php echo lang('label_emp_name'); ?></label>
                          <input type="text" class="form-control" value="<?php echo $user_info->emp_name; ?>"
                                 name="emp_name">


                      </div>
                      <div class="form-group">
                          <label><?php echo lang('label_emp_surname'); ?></label>
                          <input type="text" class="form-control" value="<?php echo $user_info->emp_surname; ?>"
                                 name="emp_surname">


                      </div>
                      <div class="form-group">
                          <label><?php echo lang('label_location_address_1'); ?></label>
                          <input type="text" class="form-control" value="<?php echo $user_info->emp_address_1; ?>"
                                 name="emp_address_1">
                          <input type="hidden" class="form-control" value="<?php echo $user_info->emp_address_2; ?>"
                                 name="emp_address_2">
                           <input type="hidden" class="form-control" value="<?php echo $user_info->emp_address_3; ?>"
                                 name="emp_address_3">


                      </div>
                      <div class="form-group">
                          <label><?php echo lang('label_emp_email_address'); ?></label>
                          <input type="email" class="form-control" value="<?php echo $user_info->email; ?>"
                                 name="emp_email">
                      </div>

                      <div class="form-group">
                          <label><?php echo lang('label_location_country'); ?></label>

                          <select class="form-control  chosen-select" id="emp_country" name="emp_country">
                              <optgroup label="<?= lang('label_selected_country') ?>">

                                  <?php foreach ($countries as $country): ?>
                                      <?php if ($user_info->emp_country == $country->id_country): ?>
                                          <option
                                              value="<?php echo $country->id_country; ?>"><?php echo $country->country_name; ?></option>
                                      <?php endif ?>
                                  <?php endforeach; ?>
                              </optgroup>
                              <optgroup label="<?= lang('label_other_countries') ?>">
                                  <?php foreach ($countries as $country): ?>
                                      <option
                                          value="<?php echo $country->id_country; ?>"><?php echo $country->country_name; ?></option>
                                  <?php endforeach; ?>
                              </optgroup>

                          </select>

                      </div>

                      <div class="form-group">
                          <label><?php echo lang('label_location_city'); ?></label>
                          <input type="text" class="form-control" value="<?php echo $user_info->emp_city; ?>"
                                 name="emp_city">
                      </div>
                      <div class="form-group">
                          <label><?php echo lang('label_language'); ?></label>

                          <input type="text" class="form-control" value="<?php echo $user_info->emp_language; ?>"
                                 name="emp_language">

                      </div>
                  </div>


                    <!-- /.the-box -->

                    <!-- /.the-box -->
                </div>
                <div class="col-sm-6">
                  <div class="the-box">
                      <h4 class="small-title"><i
                              class="fa fa-clipboard"></i> Tareas Pendientes:</h4>
                            <center>  <table width="100%">
                                <thead>
                                  <tr>
<th width="30%">Proyecto</th>
<th width="50%">Tarea</th>
<th width="20">Estatus</th>
</tr>
                                </thead>
                                <tbody>
                              <?php
                              $usuario = $user_info->emp_id;
                              if (!empty($tasks)) {
                                  foreach ($tasks as $comunicado) {

                                    $task_assigned_team = task_assigned($comunicado->task_id);

                                    if (count($task_assigned_team)): foreach ($task_assigned_team as $team): if($team->user_id == $usuario): ?>


                                        <tr>  <td width="30%" style="border-radius:5px; border-bottom: 1px solid #d9d9d9;"><?php echo $comunicado->project_title; ?> </td>
                                          <td width="50%" style="border-radius:5px; border-bottom: 1px solid #d9d9d9;"><a href="https://proyectosinternos.com/Taller5/project/tasks/details/<?php echo $comunicado->project_id; ?>/<?php echo $comunicado->task_id; ?>/tasks"> <?php echo $comunicado->task_name; ?></a></td>

                                          <?php if ($comunicado->task_status == "STATUS_DONE")
                                          {
                                            $estado = "Terminada";
                                            $bg = "#d4d4d4";
                                            $cl = "#fff";
                                          }else{

                                          $estado = "Pendiente";
                                          $bg = "blue";
                                          $cl = "#fff";

                                        ?>
                                          <td width="20%" style="text-align:center;color:<?php echo $cl; ?>;background:<?php echo $bg; ?>;border-radius:20px; border-bottom: 1px solid #d9d9d9;">   <?php echo $estado; ?> </td>
                                      </tr>
                                  <?php }
                                  endif; endforeach;endif;
                              }

                            }
                          else { ?>
                                  <tr>
                                      <td>No hay Tareas Pendientes!</td>
                                      <td></td>
                                      <td></td>
                                  </tr>
                              <?php }

                           ?>
</tbody>
                            </table></center>


                </div>
                <div class="the-box">
                    <h4 class="small-title"><i class="fa fa-envelope"></i>Datos de Accesso:</h4>

                    <div class="form-group">
                        <label><?php echo lang('label_username'); ?></label>
                        <input type="username" name="username" class="form-control"
                               value="<?php echo $user_info->username; ?>">
                    </div>

                    <div class="form-group">
                        <label><?php echo lang('label_password'); ?></label>
                        <input type="password" name="password" value="<?php echo $user_info->password; ?>"class="form-control">
                    </div>

                    <div class="form-group">
                        <label><?php echo lang('label_roles'); ?></label>

                        <select class="form-control" name="emp_role">
                            <optgroup label="Roles">
                                <?php foreach ($roles as $role): ?>
                                    <?php if ($user_info->role_id == $role->role_id): ?>
                                        <option selected
                                                value="<?php echo $role->role_id; ?>"><?php echo $role->role_name; ?></option>
                                    <?php else: ?>
                                        <option
                                            value="<?php echo $role->role_id; ?>"><?php echo $role->role_name; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </optgroup>

                        </select>

                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <label><?php echo lang('label_active'); ?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="checkbox" name="active"
                                       class="ios-switch ios-switch-success ios-switch-lg" <?php echo ($user_info->active == 1) ? 'checked' : ''; ?> />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="the-box">
                    <h4 class="small-title"><i class="fa fa-envelope"></i>Descripción de puesto</h4>

                    <div class="form-group">
                        <label><?php echo lang('label_emp_hourly_cost'); ?></label>
                        <input type="text" class="form-control" value="<?php echo $user_info->emp_cost; ?>"
                               name="emp_hourly_cost">
                    </div>
                    <div class="form-group">
                        <label><?php echo lang('label_emp_working_hours'); ?></label>
                        <input type="text" class="form-control" value="<?php echo $user_info->emp_hours; ?>"
                               name="emp_working_hours">
                    </div>

                    <div class="form-group">
                        <label><?php echo lang('label_emp_job_desc'); ?></label>
                        <textarea class="form-control rounded"
                                  name="emp_job_desc"><?php echo $user_info->emp_job_description; ?></textarea>

                    </div>
                </div>
              </div>
                <div class="row">
                    <div class="col-lg-12">
                        <button type="submit" style="background:#828282;text-shadow:0px 0px 0px #fff;border-width:0px;" class="btn btn-success btn-block btn-lg"><i
                                class="fa fa-envelope"></i> <?php echo  lang('form_button_update'); ?></button>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
        </div>


    </div>
    <!-- ============================================================== -->
    <!-- End content here -->
    <!-- ============================================================== -->
</div>
