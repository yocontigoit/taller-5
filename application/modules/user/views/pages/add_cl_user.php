<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/08
 * Time: 03:18 PM
 */
?>
<div class="content-page">
    <div class="content">
        <div class="row">
            <?php echo form_open_multipart('user/add', array('class' => '', 'role' => 'form')); ?>
            <div class="col-sm-3">
                <div class="the-box">
                    <h5><?php echo lang('label_change_picture'); ?></h5>

                    <div class="form-group">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px;">
                            </div>
                            <div>
																<span class="btn default btn-file">
																	<span class="fileinput-new">
																		 Select image
																	</span>
																	<span class="fileinput-exists">
																		 Change
																	</span>
																	<input type="file" name="user_picture">
																</span>
                                <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                                    Remove
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-9">

                <div class="col-sm-8">

                    <div class="the-box">
                        <h4 class="small-title"><i class="fa fa-envelope"></i>Profile information:</h4>

                        <div class="form-group">
                            <label><?php echo lang('label_emp_name'); ?><span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="cl_name">
                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_emp_surname'); ?></label>

                            <input type="text" class="form-control" name="cl_surname">

                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_location_address_1'); ?></label>
                            <input type="text" class="form-control" name="cl_address_1">
                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_location_address_2'); ?></label>

                            <input type="text" class="form-control" name="cl_address_2">

                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_location_address_3'); ?></label>
                            <input type="text" class="form-control" name="cl_address_3">
                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_emp_email_address'); ?><span
                                    class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="email" required="required">
                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_location_country'); ?></label>

                            <select class="form-control  chosen-select" id="user_country" name="user_country">
                                <optgroup label="<?= lang('label_other_countries') ?>">
                                    <?php foreach ($countries as $country): ?>
                                        <option
                                            value="<?php echo $country->id_country; ?>"><?php echo $country->country_name; ?></option>
                                    <?php endforeach; ?>
                                </optgroup>

                            </select>

                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_location_city'); ?></label>

                            <input type="text" class="form-control" name="city">

                        </div>
                        <div class="form-group">
                            <label><?php echo lang('label_emp_job_desc'); ?></label>
                            <input type="text" class="form-control" name="job_title">
                        </div>
                    </div>
                    <!-- /.the-box -->

                </div>

                <div class="col-sm-4">
                    <div class="the-box">
                        <h4 class="small-title"><i class="fa fa-envelope"></i> Security information:</h4>

                        <div class="form-group">
                            <label><?php echo lang('label_username'); ?></label>
                            <input type="email" class="form-control">
                        </div>

                        <div class="form-group">
                            <label><?php echo lang('label_password'); ?></label>
                            <input type="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Is Primary Contact</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="checkbox" name="cl_user_primary"
                                           class="ios-switch ios-switch-success ios-switch-lg"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Active</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="checkbox" name="cl_user_active"
                                           class="ios-switch ios-switch-success ios-switch-lg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.the-box -->

                </div>

                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-success btn-block btn-lg"><i
                                class="fa fa-sign-in"></i> <?php echo lang('form_button_save'); ?></button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

