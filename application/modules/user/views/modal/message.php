<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/03/31
 * Time: 12:56 PM
 */
?>

<div class="modal-dialog">
    <div class="modal-content modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                        class="fa fa-times"></i></span><span
                    class="sr-only"><?php echo lang('form_button_cancel'); ?></span></button>
            <h4 class="modal-title"><?php echo lang('label_send_message'); ?></h4>
        </div>

        <?php echo form_open('message/send', array('id' => 'send-message')); ?>
        <div class="modal-body">
            <input type="hidden" class="form-control " name="send_to[]" placeholder="send to"
                   value="<?php echo $user_id; ?>">

            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label for="name"><?php echo lang('label_to'); ?></label>
                        <input type="text" class="form-control" placeholder="send to"
                               value="<?= ucfirst($user_info->emp_name . " " . $user_info->emp_surname) ?>" readonly>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label><?php echo lang('label_mail_subject'); ?></label>
                        <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject"
                               required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <div class="col-md-10">
                        <textarea name="summer_message" cols="40" rows="10" id="message" class="form-control"
                                  placeholder="Write a message..." required></textarea>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="input" name="submit" value="addNewTask" class="btn btn-success btn-icon"><i
                    class="fa fa-check-square-o"></i> <?php echo "Send"; ?></button>
            <button type="button" class="btn btn-default btn-icon" data-dismiss="modal"><i
                    class="fa fa-times-circle-o"></i> <?php echo "cancel"; ?></button>
        </div>
        </form>

    </div>
</div>
