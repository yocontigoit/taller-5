<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/18
 * Time: 12:00 PM
 */
class User_M extends CI_Model
{

    /**
     * Add company us
     * @param $emp_data
     * @return null
     * @internal param $user_data
     * @internal param $user_login_data
     */
    function add_employee($emp_data)
    {

        $success = false;

        $this->db->trans_start();
        $emp_data['emp_reg_date'] = date('Y-m-d H:i:s');
        $success = $this->db->insert('employee', $emp_data);
        $emp_id = $this->db->insert_id();

        $this->db->trans_complete();

        return ($success) ? $emp_id : null;

    }

    /**
     * update user/employee data
     * @param $user_id
     * @param $emp_data
     * @return bool
     */
    function edit_employee($user_id, $emp_data)
    {
        $success = false;
        $this->db->where('emp_id', $user_id);
        $success = $this->db->update('employee', $emp_data);

        return $success;
    }

    /**
     * update user image
     * @param $image_name
     * @param $user_id
     * @return bool
     */
    function update_profile_image($image_name, $user_id)
    {
        $success = false;
        $data = array(
            'emp_avatar' => $image_name,
        );
        $success = $this->db->where('emp_id', $user_id)->update('employee', $data);

        return $success;
    }


    /**
     * Get user_id
     * @param $user_id
     */
    function get_user_role($user_id)
    {

        $this->db->where('role_id', $user_id);

        return $this->db->get('roles')->row();

    }

    /**
     * Get all roles
     * Return all roles..
     */
    function get_all_roles()
    {

        return $this->db->get('roles')->result();

    }

    /**
     * Get user_id
     * @param $user_id
     */
    function user_exists($user_name)
    {

        $this->db->where('username', $user_name);

        return $this->db->get('users')->row();
    }

    /**
     * Get user_id
     * @param $user_id
     */
    function client_exists($email)
    {
        $this->db->where('email', $email);

        return $this->db->get('clients')->row();
    }

    /**
     * retrieve all the company users
     * @return mixed
     */
    public function get_all_users()
    {

        $this->db->from("users");
        $this->db->join('employee', 'employee.emp_id  = users.user_id');

        return $this->db->get()->result();

    }
    public function get_all_tasks()
    {

        $this->db->from("tasks");
        $this->db->join('projects', 'projects.project_id  = tasks.project_id');
        $this->db->join('users', 'users.user_id  = tasks.user_id');


        return $this->db->get()->result();

    }

    /**
     * retrieve user details
     * @param $id
     * @return mixed
     */
    function get_user_info($id)
    {

        $this->db->from("employee");
        $this->db->join('users', 'users.user_id = employee.emp_id');
        $this->db->where("emp_id = " . $id);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        }
    }

    /**
     * retrieve client user details
     * @param $id
     * @return mixed
     */
    function get_cl_user_info($id)
    {

        $this->db->from("client");
        $this->db->join('client_user', 'client_user.client_id = client.client_id');
        $this->db->where("cl_user_id = " . $id);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        }
    }

    /**
     * get all client users
     * @param null $client_id
     * @return mixed
     */
    function get_all_cl_users($client_id = null)
    {

        $this->db->from("client");
        $this->db->join('client_user', 'client_user.client_id = client.client_id');
        $this->db->where("client.client_id = " . $client_id);

        return $this->db->get()->result();

    }

    /**
     * log user activities
     * @param array $data
     * @return bool
     */
    function add_log($data = array())
    {

        $success = false;
        $success = $this->db->insert("activity_log", $data);

        return $success;
    }

    /**
     * retreve empty user object
     * @return object
     */
    function get_new_info()
    {


        //Get empty base parent object, as $employee_id is NOT an employee
        $emp_obj = $this->get_info_emp(-1);

        //Get all the fields from employee table
        $fields = $this->db->list_fields('employee');

        //append those fields to base parent object, we we have a complete empty object
        foreach ($fields as $field) {
            @$emp_obj->{$field} = '';
        }

        $user_obj = $this->get_info_user(-1);
        $fields2 = $this->db->list_fields('users');
        foreach ($fields2 as $field) {
            @$user_obj->{$field} = '';
        }

        return (object)array_merge((array)$emp_obj, (array)$user_obj);
        // $user_obj;
    }

    /**
     * get user info
     * @param $id
     * @return mixed
     */
    private function get_info_emp($id)
    {

        $this->db->from("employee");
        $this->db->join('users', 'users.user_id = employee.emp_id');
        $this->db->where("emp_id = " . $id);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        }
    }

    private function get_info_user($id)
    {

        $this->db->from("users");
        $this->db->join('employee', 'users.user_id = employee.emp_id');
        $this->db->where("user_id = " . $id);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        }
    }

    function get_new_cl_info()
    {
        //Get empty base parent object, as $employee_id is NOT an employee
        $client_obj = $this->get_cl_info(-1);

        //Get all the fields from employee table
        $fields = $this->db->list_fields('client_user');

        //append those fields to base parent object, we we have a complete empty object
        foreach ($fields as $field) {
            $client_obj->$field = '';
        }

        return $client_obj;//(object) array_merge((array) $emp_obj, (array) $fields2);

    }

    private function get_cl_info($id)
    {

        $this->db->from("client");
        $this->db->join('client_user', 'client_user.client_id = client.client_id');
        $this->db->where("cl_user_id = " . $id);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        }
    }

    public function get_selected_user($id)
    {

        $this->db->from("employee");
        $this->db->select("user_id, first_name, last_name, email", false);
        $this->db->where("user_id = " . $id);

        return $this->db->get()->row();

    }

    /**
     * retrieve all countries
     * @return mixed
     */
    function get_countries()
    {
        return $this->db->get('country')->result();
    }

    /**
     * remove user..
     * @param null $user_id
     * @return bool
     */
    function remove_user($user_id = null)
    {

        $success = false;
        $this->db->where("user_id = " . $user_id);
        $success = $this->db->delete("user");

        return $success;
    }

    function remove_client_user($user_id = null)
    {
        $success = false;
        $this->db->where("cl_user_id = " . $user_id);
        $success = $this->db->delete("client_user");

        return $success;
    }
}
