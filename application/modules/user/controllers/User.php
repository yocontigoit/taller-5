<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/09/15
 * Time: 12:34 PM
 */
class User extends Admin_Controller
{

    function __construct()
    {

        parent::__construct();
        $this->load->model('user_m');

    }

    /**
     * list all users
     */
    function index()
    {

        $this->data['content'] = 'user/users';
        if ($this->CompanyUserRolePermission_id == 1) {

            $this->data['title'] = 'users';
            $this->data['tasks'] = $this->user_m->get_all_tasks();
            $this->data['users'] = $this->user_m->get_all_users();
            $this->data['_USER_PAGE'] = $this->load->view('user/pages/users', $this->data, true);
        } else {
            $this->data['title'] = 'Clients';
            $this->data['users'] = $this->user_m->get_all_cl_users($this->CompanyClient_id);
            $this->data['_USER_PAGE'] = $this->load->view('user/pages/clients', $this->data, true);
        }
        $this->data['datatable'] = true;
        $this->data['SCRIPT_PAGE'] = true;
        $this->load->view('_main_layout', $this->data);
    }

    /**
     * Update a user
     */
    function edit($user_id = null)
    {

        $this->data['content'] = 'user/users';

        $this->data['tasks'] = $this->user_m->get_all_tasks();
        if ($this->input->post()) {//we have input values so we should update
            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            if ($this->CompanyUser_id > 0) {
                $user_id = intval($user_id / 99);

                $user_data = array(
                    'emp_name' => $this->input->post('emp_name'),
                    'emp_surname' => $this->input->post('emp_surname'),
                    'company_id' => $this->Company_id,
                    'emp_address_1' => $this->input->post('emp_address_1'),
                    'emp_address_2' => $this->input->post('emp_address_2'),
                    'emp_address_3' => $this->input->post('emp_address_3'),
                    'emp_phone' => $this->input->post('emp_phone'),
                    'emp_country' => $this->input->post('emp_county'),
                    'emp_city' => $this->input->post('emp_city'),
                    'emp_job_description' => $this->input->post('emp_job_desc'),
                    'emp_language' => $this->input->post('emp_language'),
                    //'emp_avatar' => $this->input->post('emp_avatar'),
                    'emp_cost' => $this->input->post('emp_hourly_cost'),
                    'emp_hours' => $this->input->post('emp_working_hours')
                );


                $this->data['tasks'] = $this->user_m->get_all_tasks();
                if ($this->user_m->edit_employee($user_id, $user_data)) {
                    $active_status = ($this->input->post('active') == 'on') ? 1 : 0;

                    $user_login_data = array(
                        'username' => $this->input->post('username'),
                        'email' => $this->input->post('emp_email'),
                        'role_id' => 1,
                        'password' => $this->input->post('password'),
                        'active' => $active_status
                    );

                    $this->ion_auth->save_user($user_id, $user_login_data, true);

                    $activity_user = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                    $log_params = array(
                        'activity_user' => $activity_user,
                        'activity_user_type' => $this->CompanyUser_type,
                        'activity_module' => 'users',
                        'activity_details' => "company user updated",
                        'activity_module_id' => $user_id,
                        'activity_status' => "info",
                        'activity_event' => "user data updated",
                        'activity_icon' => 'fa fa-user'
                    );

                    $this->data['tasks'] = $this->user_m->get_all_tasks();
                    $this->__activity_tracker($log_params);
                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_user_save_success'));
                    redirect('user/edit/' . ($user_id * 99));
                }
            } else {

                $primary_enable = ($this->input->post('user_primary') == 'on') ? 1 : 0;
                $active_enable = ($this->input->post('user_active') == 'on') ? 1 : 0;
                $cl_user_data = array(
                    'cl_user_name' => $this->input->post('fullname'),
                    'cl_user_surname' => $this->input->post('city'),
                    'cl_user_address_1' => $this->input->post('company'),
                    'cl_user_address_2' => $this->input->post('vat'),
                    'cl_user_address_3' => $this->input->post('phone'),
                    'cl_user_job_title' => $this->input->post('address'),
                    'cl_user_email' => $this->input->post('address'),
                    'cl_user_primary' => $primary_enable,
                    'cl_user_password' => $this->input->post('address'),
                    'cl_user_phone' => $this->input->post('address'),
                    'cl_user_type' => $this->input->post('address'),
                    'cl_user_active' => $active_enable,
                    'cl_user_salt' => $this->input->post('address'),
                    'cl_last_login' => $this->input->post('address'),
                    'remember_code' => $this->input->post('address'),
                    'cl_user_country' => $this->input->post('user_country'),
                    'cl_user_city' => $this->input->post('user_city'),
                    'cl_user_avatar' => $this->input->post('address'),

                );

                $cl_user_id = $this->user_m->edit_client_user($cl_user_data);
                if ($cl_user_id) {

                    $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                    $log_params = array(
                        'activity_user' => $activity_user,
                        'activity_user_type' => $this->CompanyUser_type,
                        'activity_module' => 'users',
                        'activity_details' => "Client user updated",
                        'activity_module_id' => $cl_user_id,
                        'activity_status' => "info",
                        'activity_event' => "Client user data updated",
                        'activity_icon' => 'fa fa-user'
                    );

                    $this->__activity_tracker($log_params);
                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_user_save_success'));
                    redirect('user/edit/' . $cl_user_id);
                }

            }

        } else {

            $id = intval($this->uri->segment(3) / 99);

            if ($this->CompanyUser_id > 0) {

                $this->data['title'] = null;
                $this->data['show_user_data'] = true;
                $this->data['countries'] = $this->user_m->get_countries();
                $this->data['roles'] = $this->user_m->get_all_roles();
                $this->data['user_info'] = $this->user_m->get_user_info($id);
                $this->data['_USER_PAGE'] = $this->load->view('user/pages/edit_user', $this->data, true);


            } else {
                $this->data['title'] = null;
                $this->data['show_client_user_stats'] = true;
                $this->data['countries'] = $this->user_m->get_countries();
                $this->data['user_info'] = $this->user_m->get_cl_user_info($id);
                $this->data['_USER_PAGE'] = $this->load->view('user/pages/edit_cl_user', $this->data, true);
            }

            $this->data['SCRIPT_PAGE'] = true;
            $this->load->view('_main_layout', $this->data);
            $this->data['tasks'] = $this->user_m->get_all_tasks();
        }

    }

    /**
     * save user activities
     * @param array $activity_data
     */
    function __activity_tracker($activity_data = array())
    {
        //add data to database
        $this->user_m->add_log($activity_data);

    }

    /**
     * send message to client/company user
     */
    function send_message()
    {

        $user_id = intval($this->uri->segment(3));
        $data['user_info'] = user_email($user_id);
        $data['user_id'] = $user_id . '*u';
        $this->load->view('modal/message', $data);
    }

    /**
     * Add new user
     */
    function add()
    {
        $this->data['content'] = 'user/users';
        $this->data['title'] = 'add new user';
        if ($this->input->post()) {//we have input values so we should update

            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            if ($this->CompanyUser_id > 0) {
                $rules = $this->form_validator->user_add_rules;
                $this->form_validation->set_rules($rules);
                if ($this->form_validation->run() == true) {
                    $user_data = array(
                        'emp_name' => $this->input->post('emp_name'),
                        'emp_surname' => $this->input->post('emp_surname'),
                        'company_id' => $this->Company_id,
                        'emp_address_1' => $this->input->post('emp_address_1'),
                        'emp_address_2' => $this->input->post('emp_address_2'),
                        'emp_address_3' => $this->input->post('emp_address_3'),
                        'emp_phone' => $this->input->post('emp_phone'),
                        'emp_country' => $this->input->post('emp_county'),
                        'emp_city' => $this->input->post('emp_city'),
                        'emp_job_description' => $this->input->post('emp_job_desc'),
                        'emp_language' => $this->input->post('emp_language'),
                        'emp_avatar' => 'default.jpg',
                        'emp_cost' => $this->input->post('emp_hourly_cost'),
                        'emp_hours' => $this->input->post('emp_working_hours')
                    );


                    $user_id = $this->user_m->add_employee($user_data);
                    if ($user_id) {

                        $active_status = ($this->input->post('active') == 'on') ? 1 : 0;
                        $user_login_data = array(
                            'username' => ($this->input->post('username')) ? $this->input->post('username') : $this->input->post('emp_name'),
                            'email' => $this->input->post('emp_email'),
                            'role_id' => 1,
                            'password' => $this->input->post('password'),
                            'active' => $active_status
                        );

                        $this->ion_auth->save_user($user_id, $user_login_data, false);

                        $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                        $log_params = array(
                            'activity_user' => $activity_user,
                            'activity_user_type' => $this->CompanyUser_type,
                            'activity_module' => 'users',
                            'activity_details' => "new user",
                            'activity_module_id' => $user_id,
                            'activity_status' => "info",
                            'activity_date' => date('Y-m-d,H:i:s'),
                            'activity_event' => "Add a new company user",
                            'activity_icon' => 'fa fa-user'
                        );

                        $this->__activity_tracker($log_params);
                        $this->session->set_flashdata('msg_status', 'success');
                        $this->session->set_flashdata('message', lang('messages_user_add_success'));
                        redirect('user/edit/' . ($user_id * 99));
                    } else {

                        $this->session->set_flashdata('msg_status', 'error');
                        $this->session->set_flashdata('message', lang('messages_user_add_error'));
                        redirect('user');
                    }

                } else {

                    $this->data['message'] = (validation_errors()) ? validation_errors() : 'Fill required fields';
                    $this->session->set_flashdata('message', $this->data['message']);
                }

            } else {
                $primary_enable = ($this->input->post('cl_user_primary') == 'on') ? 1 : 0;
                $active_enable = ($this->input->post('cl_user_active') == 'on') ? 1 : 0;
                $cl_user_data = array(
                    'cl_user_name' => $this->input->post('cl_name'),
                    'cl_user_surname' => $this->input->post('cl_surname'),
                    'cl_user_address_1' => $this->input->post('cl_address_1'),
                    'cl_user_address_2' => $this->input->post('cl_address_2'),
                    'cl_user_address_3' => $this->input->post('cl_address_3'),
                    'cl_user_job_title' => $this->input->post('job_title'),
                    'cl_user_email' => $this->input->post('email'),
                    'client_email' => $this->input->post('email'),
                    'cl_user_primary' => $primary_enable,
                    'client_id' => $this->CompanyClient_id,
                    'cl_user_password' => $this->input->post('password'),
                    'cl_user_type' => $this->CompanyUser_type,
                    'cl_user_active' => $active_enable,
                    'cl_user_country' => $this->input->post('user_country'),
                    'cl_user_city' => $this->input->post('city'),
                    'cl_user_avatar' => 'default.jpg',
                    'role_id' => 3,
                    'cl_user_phone' => '',
                    'created_on' => date('Y-m-d H:i:s')
                );

                if ($this->ion_auth->add_cl_user($this->CompanyClient_id, $cl_user_data, false)) {

                    $activity_user = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id;
                    $log_params = array(
                        'activity_user' => $activity_user,
                        'activity_user_type' => $this->CompanyUser_type,
                        'activity_module' => 'users',
                        'activity_details' => "new client user",
                        'activity_module_id' => $this->CompanyClient_id,
                        'activity_status' => "info",
                        'activity_date' => date('Y-m-d,H:i:s'),
                        'activity_event' => "Add a new client user",
                        'activity_icon' => 'fa fa-user'
                    );

                    $this->__activity_tracker($log_params);
                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_user_add_success'));
                    redirect('user');
                } else {

                    $this->session->set_flashdata('msg_status', 'error');
                    $this->session->set_flashdata('message', lang('messages_user_add_error'));
                    redirect('user');
                }

            }


        } else {

            $this->data['countries'] = $this->user_m->get_countries();
            if ($this->CompanyUser_id > 0) {

                $this->data['roles'] = $this->user_m->get_all_roles();
                $this->data['_USER_PAGE'] = $this->load->view('user/pages/add_user', $this->data, true);
            } else {
                $this->data['_USER_PAGE'] = $this->load->view('user/pages/add_cl_user', $this->data, true);
            }

        }
        $this->load->view('_main_layout', $this->data);

    }

    /**
     * Delete selected user
     * (Check if logged user have permissions to delete)
     * @param null $user_id
     */
    function delete()
    {
        //$this->check_action_permission('delete');
        if ($this->input->post()) {

            if ($this->config->item('system')->is_demo == 1) {
                $this->session->set_flashdata('msg_status', 'error');
                $this->session->set_flashdata('message', 'function disabled in demo mode');
                redirect($_SERVER['HTTP_REFERER']);
            }
            $user_id = intval($this->input->post('user_id')) / 99;
            if ($this->CompanyUser_id > 0) {

                //Don't let employee delete their self
                if ($this->CompanyUser_id == $user_id) {
                    return false;
                }

                if ($this->user_m->remove_user($user_id)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_client_delete_success'));

                } else {

                    $this->session->set_flashdata('msg_status', 'error');
                    $this->session->set_flashdata('message', 'Something went wrong deleting..');
                }
                redirect('user');
            } else {
                //Don't let employee delete their self
                if ($this->CompanyClient_user_id == $user_id) {
                    return false;
                }

                if ($this->user_m->remove_client_user($user_id)) {

                    $this->session->set_flashdata('msg_status', 'success');
                    $this->session->set_flashdata('message', lang('messages_client_delete_success'));

                } else {

                    $this->session->set_flashdata('msg_status', 'error');
                    $this->session->set_flashdata('message', 'Something went wrong deleting..');
                }
                redirect('user');
            }
        } else {

            $this->data['title'] = 'Delete User';
            $this->data['content'] = 'user/users';
            $this->data['user_id'] = intval($this->uri->segment(4));
            $this->load->view('modal/delete', $this->data);
        }

    }

    /**
     * change user image
     */
    function change_image()
    {
        if ($this->input->post()) {
            if ($this->CompanyUser_id) {


                $user_id = intval($this->input->post('user_id'));
                $config['upload_path'] = './files/profile_images/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '10000';
                $config['max_width'] = '8000';
                $config['max_height'] = '8000';
                $config['file_name'] = 'company_user-' . $user_id;
                $config['remove_spaces'] = true;
                $config['overwrite'] = true;

                $this->load->library('upload', $config);


                if (!$this->upload->do_upload('user_picture')) {
                    $this->session->set_flashdata('msg_status', 'error');
                    $this->session->set_flashdata('message', lang('messages_avatar_add_error'));
                    redirect('user/edit/' . ($user_id * 99));
                } else {
                    $data = $this->upload->data();
                    if ($this->user_m->update_profile_image($data['file_name'], $user_id)) {
                        //Set user avatar
                        if ($this->CompanyUser_id == $user_id) {
                            $this->session->set_userdata('user_avatar', $data['file_name']);
                        }
                        $this->session->set_flashdata('msg_status', 'success');
                        $this->session->set_flashdata('message', lang('messages_avatar_add_success'));
                        redirect('user/edit/' . ($user_id * 99));

                    }


                }

            }
        } else {

          $this->data['tasks'] = $this->user_m->get_all_tasks();
            $this->session->set_flashdata('msg_status', 'success');
            $this->session->set_flashdata('message', lang('messages_avatar_add_success'));
            redirect('user/edit/' . ($this->input->post('user_id') * 99));
        }
    }


}
