<?php

/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/18
 * Time: 01:47 PM
 */
class Zest_config_m extends CI_Model
{

    var $company_table = '';
    var $template_table = '';
    var $local_table = '';
    var $system_table = '';
    var $email_table = '';
    var $invoice_table = '';
    var $payments_table = '';
    var $paypal_table = '';
    var $skrill_table = '';
    var $stripe_table = '';
    var $payfast_table = '';
    var $braintree_table = '';
    var $checkout_table = '';
    var $bitcoin_table = '';
    var $support_table = '';
    var $terms_table = '';
    var $bank_table = '';

    public function __construct()
    {
        parent::__construct();
        $this->company_table = 'settings_company';
        $this->template_table = 'settings_email_template';
        $this->local_table = 'settings_local';
        $this->system_table = 'settings_system';
        $this->email_table = 'settings_email';
        $this->invoice_table = 'settings_invoice';
        $this->payments_table = 'settings_payment_methods';
        $this->paypal_table = 'settings_paypal';
        $this->skrill_table = 'settings_skrill';
        $this->stripe_table = 'settings_stripe';
        $this->payfast_table = 'settings_payfast';
        $this->checkout_table = 'settings_2checkout';
        $this->braintree_table = 'settings_braintree';
        $this->bitcoin_table = 'settings_bitcoin';
        $this->support_table = 'settings_support';
        $this->systypes_table = 'settings_systypes';
        $this->theme_table = 'settings_theme';
        $this->bank_table = 'settings_bank';
        $this->terms_table = '';
    }

    function company_setting($company_id)
    {
        $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->company_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {

            $fields = $this->db->list_fields($this->company_table);
            $company_obj = new stdClass;

            foreach ($fields as $field) {
                $company_obj->$field = '';
            }

            return $company_obj;
        }
    }

    function template_setting($company_id)
    {
        $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->template_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {

            $fields = $this->db->list_fields($this->template_table);
            $template_obj = new stdClass;

            foreach ($fields as $field) {
                $template_obj->$field = '';
            }

            return $template_obj;
        }
    }

    function local_setting($company_id)
    {
        $this->db->where('company_company_id', $company_id);
        $query = $this->db->get($this->local_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {


            $fields = $this->db->list_fields($this->local_table);
            $local_obj = new stdClass;

            foreach ($fields as $field) {
                $local_obj->$field = '';
            }

            return $local_obj;
        }
    }

    function theme_setting($company_id){


        $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->theme_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {


            $fields = $this->db->list_fields($this->theme_table);
            $local_obj = new stdClass;

            foreach ($fields as $field) {
                $local_obj->$field = '';
            }

            return $local_obj;
        }

    }
    function system_setting($company_id)
    {
        $this->db->where('company_company_id', $company_id);
        $query = $this->db->get($this->system_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            $fields = $this->db->list_fields($this->system_table);
            $system_obj = new stdClass;

            foreach ($fields as $field) {
                $system_obj->$field = '';
            }

            return $system_obj;
        }
    }

    function email_setting($company_id)
    {
        $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->email_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {

            $fields = $this->db->list_fields($this->email_table);
            $email_obj = new stdClass;

            foreach ($fields as $field) {
                $email_obj->$field = '';
            }

            return $email_obj;
        }
    }

    function terms_setting($company_id)
    {
        /* $this->db->where('company_id', $company_id);
         $query = $this->db->get($this->terms_table);
         if ($query->num_rows() > 0) {
             return $query->row();
         } else {*/

        //$fields = $this->db->list_fields($this->terms_table);
        $terms_obj = new stdClass;

        //foreach ($fields as $field) {
        //    $terms_obj->$field = '';
        //}

        return $terms_obj;
        // }
    }

    function paypal_setting($company_id)
    {
        $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->paypal_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {

            $fields = $this->db->list_fields($this->paypal_table);
            $terms_obj = new stdClass;

            foreach ($fields as $field) {
                $terms_obj->$field = '';
            }

            return $terms_obj;
        }

    }

    function bank_setting($company_id){

        $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->bank_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {

            $fields = $this->db->list_fields($this->bank_table);
            $bank_obj = new stdClass;

            foreach ($fields as $field) {
                $bank_obj->$field = '';
            }

            return $bank_obj;
        }
    }


    function payfast_setting($company_id)
    {
        $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->payfast_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {

            $fields = $this->db->list_fields($this->payfast_table);
            $terms_obj = new stdClass;

            foreach ($fields as $field) {
                $terms_obj->$field = '';
            }

            return $terms_obj;
        }

    }


    function stripe_setting($company_id){

        $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->stripe_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {

            $fields = $this->db->list_fields($this->stripe_table);
            $terms_obj = new stdClass;

            foreach ($fields as $field) {
                $terms_obj->$field = '';
            }

            return $terms_obj;
        }

    }

    function skrill_setting($company_id){

        $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->skrill_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {

            $fields = $this->db->list_fields($this->skrill_table);
            $terms_obj = new stdClass;

            foreach ($fields as $field) {
                $terms_obj->$field = '';
            }

            return $terms_obj;
        }
    }

    function  checkout_setting($company_id){


        $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->checkout_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {

            $fields = $this->db->list_fields($this->checkout_table);
            $terms_obj = new stdClass;

            foreach ($fields as $field) {
                $terms_obj->$field = '';
            }

            return $terms_obj;
        }


    }


    function braintree_setting($company_id){
        $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->braintree_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {

            $fields = $this->db->list_fields($this->braintree_table);
            $terms_obj = new stdClass;

            foreach ($fields as $field) {
                $terms_obj->$field = '';
            }

            return $terms_obj;
        }

    }

    function  bitcoin_setting($company_id){

        $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->bitcoin_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {

            $fields = $this->db->list_fields($this->bitcoin_table);
            $terms_obj = new stdClass;

            foreach ($fields as $field) {
                $terms_obj->$field = '';
            }

            return $terms_obj;
        }
    }


    function invoice_setting($company_id)
    {
        $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->invoice_table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {

            $fields = $this->db->list_fields($this->invoice_table);
            $invoice_obj = new stdClass;

            foreach ($fields as $field) {
                $invoice_obj->$field = '';
            }

            return $invoice_obj;
        }
    }

    /**
     * Payment  gateways..
     * @param $company_id
     * @return stdClass
     */
    function payment_setting($company_id)
    {
        $this->db->where('company_id', $company_id);
        $query = $this->db->get($this->payments_table);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {

            $fields = $this->db->list_fields($this->payments_table);
            $payment_obj = new stdClass;

            foreach ($fields as $field) {
                $payment_obj->$field = '';
            }

            return $payment_obj;
        }
    }

    function support_setting($company_id)
    {
        $this->db->where('company_id', $company_id);

        $query = $this->db->get($this->support_table);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {

            $fields = $this->db->list_fields($this->support_table);
            $support_obj = new stdClass;

            foreach ($fields as $field) {
                $support_obj->$field = '';
            }

            return $support_obj;
        }
    }

    function company_systypes($company_id)
    {
        $this->db->where('company_id', $company_id);

        $query = $this->db->get($this->systypes_table);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {

            $fields = $this->db->list_fields($this->systypes_table);
            $support_obj = new stdClass;

            foreach ($fields as $field) {
                $support_obj->$field = '';
            }

            return $support_obj;
        }
    }

}
