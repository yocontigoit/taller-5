<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/18
 * Time: 01:46 PM
 */
class Module_M extends CI_Model{


    function get_module_name($module_id)
    {
        $query = $this->db->get_where('modules', array('module_id' => $module_id), 1);

        if ($query->num_rows() ==1)
        {
            $row = $query->row();
            return lang($row->name_lang_key);
        }

        return lang('error_unknown');
    }

    function get_module_desc($module_id)
    {
        $query = $this->db->get_where('modules', array('module_id' => $module_id), 1);
        if ($query->num_rows() ==1)
        {
            $row = $query->row();
            return lang($row->desc_lang_key);
        }

        return lang('error_unknown');
    }

    function get_all_modules()
    {
        $this->db->from('modules');
        $this->db->order_by("sort", "asc");
        return $this->db->get();
    }

    function get_allowed_modules($role_id)
    {
        $this->db->from('modules');
        $this->db->join('permissions','permissions.module_id=modules.module_id');
        $this->db->join('roles','roles.role_id = permissions.role_id');
        $this->db->where("permissions.role_id",$role_id);
        $this->db->order_by("sort","parent_id");
        $pages = $this->db->get()->result_array();

        $array = array();
        foreach ($pages as $page) {
            if (!$page['parent_id']) {

                $array[$page['module_parent_id']] = $page;
            }
            else {
                if(count($this->check_sub_modules_permission($page['parent_id'],$role_id))) {
                    $array[$page['parent_id']]['children'][] = $page;
                }
            }
        }
        return $array;

    }

    function check_sub_modules_permission($module_parent_id = null,$role_id = null)
    {
        $this->db->from('modules');
        $this->db->join('permissions','permissions.module_id=modules.module_id');
        $this->db->where("modules.module_parent_id",$module_parent_id);
        $this->db->where("permissions.role_id",$role_id);
        $query = $this->db->get();

        if ($query->num_rows() ==1)
        {
            return $row = $query->row();

        }

    }

    function get_all_roles()
    {
        $this->db->from('roles');
        $this->db->order_by("sort", "asc");
        return $this->db->get();
    }

    public function save_order ($pages)
    {
        if (count($pages)) {
            foreach ($pages as $order => $page) {
                if ($page['id'] != '') {
                    $data = array('sort' => $order);
                    $this->db->set($data)->where('module_id', $page['id'])->update('modules');
                }
            }
        }
    }

    public function get_nested ()
    {
        $this->db->order_by("sort","parent_id");
        $pages = $this->db->get('modules')->result_array();

        $array = array();
        foreach ($pages as $page) {
            if (!$page['parent_id']) {
                // This page has no parent
                $array[$page['module_parent_id']] = $page;
            }
            else {
                // This is a child page
                $array[$page['parent_id']]['children'][] = $page;
            }
        }
        return $array;
    }
}