<?php
class Admin_M extends CI_Model
{


    function setUsersOnline($user_id)
    {
        $date   = date('Y-m-d H:i:s');
        $ip     = $this->input->ip_address();
        $this->load->library('user_agent');
        if ($this->agent->is_browser())
        {
            $type = 'guest';
        }
        elseif ($this->agent->is_robot())
        {
            $type = 'robot';
        }
        elseif ($this->agent->is_mobile())
        {
            $type = 'guest';
        }
        else
        {
            $type = 'other';
        }
        if($user_id>0)
            $type = 'user';

        $this->db->query("DELETE FROM online_users WHERE last_activity < ('$date' - INTERVAL 1 MINUTE)");
        $this->db->query("DELETE FROM online_clients WHERE last_activity < ('$date' - INTERVAL 1 MINUTE)");
        return $this->db->query("INSERT INTO `online_users` (`user_id`, `last_activity`, `ip_address`,`type`)   VALUES ($user_id,'$date','$ip','$type')  ON DUPLICATE KEY UPDATE last_activity = '$date'");
    }

    function setClientsOnline($client_id)
    {
        $date   = date('Y-m-d H:i:s');
        $ip     = $this->input->ip_address();
        $this->load->library('user_agent');
        if ($this->agent->is_browser())
        {
            $type = 'guest';
        }
        elseif ($this->agent->is_robot())
        {
            $type = 'robot';
        }
        elseif ($this->agent->is_mobile())
        {
            $type = 'guest';
        }
        else
        {
            $type = 'other';
        }
        if($client_id > 0)
            $type = 'client';

        $this->db->query("DELETE FROM online_users WHERE last_activity < ('$date' - INTERVAL 1 MINUTE)");
        $this->db->query("DELETE FROM online_clients WHERE last_activity < ('$date' - INTERVAL 1 MINUTE)");
        return $this->db->query("INSERT INTO `online_clients` (`cl_user_id`, `last_activity`, `ip_address`,`type`)   VALUES ($client_id,'$date','$ip','$type')  ON DUPLICATE KEY UPDATE last_activity = '$date'");
    }

}
