<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/18
 * Time: 09:42 AM
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php

    $favicon = ($this->config->item('theme')->site_favicon) ? $this->config->item('theme')->site_favicon: '';
    $ext = substr($favicon, -4);

    ?>
    <?php if ( $ext == '.ico' && $favicon!='') : ?>
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/'.$favicon); ?>">
    <?php endif; ?>
    <?php if ($ext == '.png' && $favicon!='') : ?>
        <link rel="icon" type="image/png" href="<?php echo base_url('assets/images/'.$favicon); ?>">
    <?php endif; ?>
    <?php if (($ext == '.jpg' || $ext == 'jpeg')  && $favicon!='') : ?>
        <link rel="icon" type="image/jpeg" href="<?php echo base_url('assets/images/'.$favicon); ?>">
    <?php endif; ?>
    <title><?php echo $this->config->item('theme')->site_title ?></title>

    <?php foreach(get_css_files() as $js_file) { ?>
    <link href="<?php echo base_url($js_file['path']); ?>" rel="stylesheet" type="text/css" />
    <?php } ?>


    <script type="text/javascript">
        var base_url = "<?php echo base_url(); ?>";
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesnt work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="js/ie/html5shiv.js" cache="false">
    </script>
    <script src="js/ie/respond.min.js" cache="false">
    </script>
    <script src="js/ie/excanvas.js" cache="false">
    </script> <![endif]-->
</head>

<body>

<?php if((isset($project_id))): ?>
<div class="md-modal md-3d-flip-vertical" id="task-progress">
    <?php echo (isset($project_id)) ? get_task_progress($project_id) : ''; ?>
</div>
<?php endif; ?>
<div class="wrapper">
