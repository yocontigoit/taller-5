<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/10
 * Time: 11:44 AM
 */
?>
<!DOCTYPE html>
<html class="bg-black">
<head>
    <meta charset="UTF-8">
    <?php

    $favicon = ($this->config->item('theme')->site_favicon) ? $this->config->item('theme')->site_favicon: '';
    $ext = substr($favicon, -4);

    ?>
    <?php if ( $ext == '.ico' && $favicon!='') : ?>
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/'.$favicon); ?>">
    <?php endif; ?>
    <?php if ($ext == '.png' && $favicon!='') : ?>
        <link rel="icon" type="image/png" href="<?php echo base_url('assets/images/'.$favicon); ?>">
    <?php endif; ?>
    <?php if (($ext == '.jpg' || $ext == 'jpeg')  && $favicon!='') : ?>
        <link rel="icon" type="image/jpeg" href="<?php echo base_url('assets/images/'.$favicon); ?>">
    <?php endif; ?>
    <title><?php echo $this->config->item('theme')->site_title ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="<?php echo base_url('assets/font-awesome-4.5.0/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url('assets/css/login.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/style-responsive.css'); ?>" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
        @media (min-width: 501px) and (max-width: 2800px) {
            .login_wrapper {
                background: url('<?php echo base_url('assets/images/'.$this->config->item('theme')->login_background); ?>');
            }
        }
        @media (max-width: 500px) {
            .login_wrapper {
                background-image:none;
            }
        }

    </style>
</head>
<body class="login_wrapper"  id="login">
<div class="sign-overlay"></div>
