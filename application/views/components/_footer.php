<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/10
 * Time: 11:45 AM
 */
?>

<!-- jQuery 2.0.2 -->
<script src="<?php echo base_url('assets/libs/jquery-2.0.2.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>" type="text/javascript"></script>

</body>
</html>
 <?php
if($this->session->flashdata('message')){
    $message = $this->session->flashdata('message');
    $alert = $this->session->flashdata('response_status');
    ?>
    <script type="text/javascript">
        $(document).ready(function(){
            toastr.<?=$alert?>("<?=$message?>", "<?=lang('response_status')?>");
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        });
    </script>
<?php } ?>