<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/18
 * Time: 09:43 AM
 */
?>
</div>
<!-- container-fluid -->

</div>
<!-- end content page -->

</div>
<!-- /#wrapper -->

<div class="md-overlay"></div>




<?php foreach(get_js_files() as $js_file) { ?>
    <script src="<?php echo base_url($js_file['path']);?>" ></script>
<?php } ?>


<?php if(isset($SCRIPT_PAGE)){
    $this->load->view('scripts/script_page');
}
?>
<!-- START initialization -->
<script type="text/javascript">
    $(document).ready(function(){
        Zest.init(); // initlayout and core plugins
        zestCHAT.initializeBase(); // initialise chat
        $("body").tooltip({ selector: '[data-toggle=tooltip]' });
        $(".ios-switch").each(function(){
            mySwitch = new Switch(this);
        });

        $('.table-responsive').on('shown.bs.dropdown', function (e) {
            var $table = $(this),
                $menu = $(e.target).find('.dropdown-menu'),
                tableOffsetHeight = $table.offset().top + $table.height(),
                menuOffsetHeight = $menu.offset().top + $menu.outerHeight(true);

            if (menuOffsetHeight > tableOffsetHeight)
                $table.css("padding-bottom", menuOffsetHeight + 20 - tableOffsetHeight);
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('#salestimate_stats').highcharts().setSize($('#salestimate_stats').width(),$('#salestimate_stats').height());
        });


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('#category-stats').highcharts().setSize($('#category-stats').width(),$('#category-stats').height());
        });
        $('.table-responsive').on('hide.bs.dropdown', function () {
            $(this).css("padding-bottom", 0);
        });


        /** BEGIN INPUT FILE **/
        if ($('.btn-file').length > 0){
            $(document)
                .on('change', '.btn-file :file', function() {
                    "use strict";
                    var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [numFiles, label]);
                });
            $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

                if( input.length ) {
                    input.val(log);
                } else {
                    // if( log ) alert(log);
                }
            });
        }
    });



</script>

<?php
if ($this->CompanyUserRolePermission_id == 3) { ?>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        Stripe.setPublishableKey('<?php echo $this->config->item('stripe')->stripe_public_key; ?>');
    </script>
<?php } ?>

<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->

<?php
if($this->session->flashdata('message')){
    $message = $this->session->flashdata('message');
    $alert = $this->session->flashdata('msg_status');
    ?>
    <script type="text/javascript">
        $(document).ready(function(){
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            toastr.<?php echo $alert; ?>("<?php echo $message; ?>", "<?php echo "Message"; ?>");
        });
    </script>
<?php } ?>


</body>
</html>