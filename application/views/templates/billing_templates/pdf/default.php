<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 1/23/2016
 * Time: 5:53 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title></title>

    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/invoice.css'); ?>" rel="stylesheet" type="text/css" />
    <style>


        table.table tbody tr.even td {
            background: #f6f8f9;
        }
        .invoice-header ul {
            margin-bottom: 0;
        }
        .invoice-header {
            margin-bottom: 20px;
            padding-bottom: 20px;

        }
        .invoice-header h3 {
            margin-bottom: 0;
            margin-top: 8px;
            font-weight: 400;
        }
        .invoice-header span {
            color: #999;
        }

        .invoice-details {
            float: right;
            width: 260px;
            margin-top: 4px;
            max-width: 100%;
        }
        .invoice-details strong {
            float: right;
            margin-left: 20px;
        }

        .invoice-payment {


            float: right;
            width: 250px;
            border: 1px solid #d6dde2;
            margin: 20px 0 40px 0;
            padding: 0;
            border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;
            text-align: right;

        }
        .invoice-payment .table h6 {
            margin-bottom: 0;
        }
        .chosen-payment span {
            color: #D65C4F;
            text-decoration: underline;
            font-weight: 600;
        }
        .chosen-payment a {
            font-size: 11px;
            color: #999;
        }

        .invoice-status {
            margin-top: 12px;
            padding-top: 12px;
            border-top: 1px solid #eee;
        }



        .text-right {
            text-align: right;
        }

        @media (max-width: 768px) {
            .invoice-details {
                float: none;
                margin-top: 12px;
            }
        }


        .invoice-items{
            border: 1px solid #d6dde2;
            border-radius: 4px; -moz-border-radius: 4px; -webkit-border-radius: 4px;
        }
        .invoice-items td,.invoice-items th{padding:0.2em 0.4em 0.4em}

        .terms {
            clear: both;
            margin-bottom: 0;
            padding:20px 10px;
            border-top: 1px solid #d6dde2;
        }


    </style>


</head>

<body>

<table>
    <tr>
        <td height="90" style="width:70%">
            <img height="80" src="<?php echo base_url('files/transaction_logos/'.$this->config->item('invoice')->invoice_logo); ?>" />
            <div class="invoice-header">
            </div>
        </td>
        <td class="text-left" style="width:30%">
            <h3><strong>ESTIMATE</strong></h3>
            <ul class="invoice-details">
                <li><?php echo lang('label_estimate_date'); ?>: <strong><?php echo date('Y-m-d'); ?></strong></li>
            </ul>
        </td>
    </tr>
</table>
<table>
  <tr>
    <td>
      <?php if (count($projects)) : foreach ($projects as $project):?>
      <h3><?php echo $project->project_title ?></h3>
      <?php endforeach; endif; ?>
    </td>
  </tr>
</table>
<br/>
<div class="invoice-items">
    <table class="table table-striped" style="width: 100%;">
        <thead>
        <tr>
          <th>
              #
          </th>
          <th>
              Orden #
          </th>
          <th>
              <?php echo lang('label_client_name'); ?>

          </th>

          <th>
              Fecha
          </th>
          <th>
              <?php echo lang('label_amount'); ?>

          </th>
        </tr>
        </thead>
        <tbody>
          <?php $counter = 1; ?>
        <?php if (count($transactions)) : foreach ($transactions as $transaction) : if ($transaction->estimate == '1' ): ?>
            <tr>
                <td>
                   <?php echo $counter++; ?>
                </td>
                <td>
                    <a href="<?php echo base_url('billing/invoice/' . $transaction->id); ?>"><?php echo $transaction->inv_no; ?></a>
                </td>
                <td>
                    <a href="<?php echo base_url('client/view/' . $transaction->client_id); ?>"><?php echo client_company($transaction->client_id); ?></a>
                </td>

                <td><?php echo $transaction->invoice_due; ?></td>
                <td>
                    <?php echo invoice_total($transaction->id); ?>
                </td>
            </tr>

        <?php endif; endforeach; endif; ?>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td>Total:</td>
          <td></td>
        </tr>
        </tbody>
    </table>
</div>
<br/>

<div class="invoice-payment">

    <table width="100%">
        <tbody>

        <tr>
            <th><?php echo lang('label_sub_total') ;?>: 7000</th>

            <td class="text-right"><?php //echo ($estimate->est_currency) ? get_currency_symbol_by_name($estimate->est_currency).format_amount(calculate_estimate_sub_total($estimate->est_id)):format_currency(calculate_estimate_sub_total($estimate->est_id)) ;?></td>
        </tr>
        <tr>
            <th>TRASLADOS, ACARREOS HERRAM Y EQUIPO MENOR, ETC. :</th>

            <td class="text-right">7000<?php //echo ($estimate->est_currency) ? get_currency_symbol_by_name($estimate->est_currency).format_amount(calculate_estimate_tax_total($estimate->est_id)):format_currency(calculate_estimate_tax_total($estimate->est_id)) ;?></td>
        </tr>
        <tr>
            <th>HONORARIOS SUPERVISIÓN, ADMINISTRACIÓN Y DIRECCIÓN :</th>
            <td class="text-right text-danger"><h6>7000<?php //echo ($estimate->est_currency) ? get_currency_symbol_by_name($estimate->est_currency).format_amount(calculate_estimate_total($estimate->est_id)):format_currency(calculate_estimate_total($estimate->est_id)) ;?></h6></td>
        </tr>
        </tbody>
    </table>

</div>
</body>
</html>
