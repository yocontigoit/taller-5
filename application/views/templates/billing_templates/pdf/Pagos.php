<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 1/23/2016
 * Time: 5:53 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title></title>

    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/invoice.css'); ?>" rel="stylesheet" type="text/css" />

    <style>


        table.table tbody tr.even td {
            background: #f6f8f9;
        }
        .invoice-header ul {
            margin-bottom: 0;
        }
        .invoice-header {
            margin-bottom: 20px;
            padding-bottom: 20px;

        }
        .invoice-header h3 {
            margin-bottom: 0;
            margin-top: 8px;
            font-weight: 400;
        }
        .invoice-header span {
            color: #999;
        }

        .invoice-details {
            float: right;
            width: 50%;
            margin-top: 4px;
            max-width: 100%;
        }
        .invoice-details strong {
            float: right;
            margin-left: 20px;
        }

        .invoice-payment {


            float: right;
            width: 500px;
            border: 2px solid #d6dde2;
            margin: 20px 0 40px 0;
            padding: 0;
            border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;
            text-align: right;

        }

        .invoice-payment .table h6 {
            margin-bottom: 0;
        }
        .chosen-payment span {
            color: #D65C4F;
            text-decoration: underline;
            font-weight: 600;
        }
        .chosen-payment a {
            font-size: 11px;
            color: #999;
        }

        .invoice-status {
            margin-top: 12px;
            padding-top: 12px;
            border-top: 1px solid #eee;
        }



        .text-right {
            text-align: right;
        }

        @media (max-width: 768px) {
            .invoice-details {
                float: none;
                margin-top: 12px;
            }
        }


        .invoice-items{
            border: 1px solid #d6dde2;
            border-radius: 4px; -moz-border-radius: 4px; -webkit-border-radius: 4px;
        }
        .invoice-items td,.invoice-items th{padding:0.2em 0.4em 0.4em}



        .invoice-itemss{
            border: 1px solid #d6dde2;
            border-radius: 4px; -moz-border-radius: 4px; -webkit-border-radius: 4px;
        }
        .invoice-itemss td,.invoice-itemss th{padding:0.2em 0.4em 0.4em}



        .terms {
            clear: both;
            margin-bottom: 0;
            padding:20px 10px;
            border-top: 1px solid #d6dde2;
        }

        .posicion {
          position:absolute;
          top:0;
          right:0;
        }


    </style>


</head>

<body>

<table>
    <tr>
        <td height="90" style="width:70%">
            <img height="80" src="<?php echo base_url('files/transaction_logos/'.$this->config->item('invoice')->invoice_logo); ?>" />
            <div class="invoice-header">
            </div>
        </td>
        <td class="text-left" style="width:30%">
            <h3><strong>ESTIMACIÓN <?php echo $est_num ?></strong></h3>
            <ul class="invoice-details">
                <li>Fecha: <strong><?php echo date('Y-m-d'); ?></strong></li>
            </ul>
        </td>
    </tr>
</table>
<table>
  <tr>
    <td>
      <?php if (count($projects)) : foreach ($projects as $project):?>
      <h3><?php echo $project->project_title ?></h3>
      <?php endforeach; endif; ?>
    </td>
  </tr>
</table>
<br/>
<div class="invoice-items">
  <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Mano de Obra</h4></div>
        <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success items_table_mano">
<thead class="the-box light full">
<tr>
<th></th>
  <th>
    Concepto
  </th>
  <th>
    Responsable
  </th>

  <th>
    Presupuestos
  </th>
  <th>
    Importe
  </th>
</tr>
</thead>
<tbody>
<?php
    $est_pagom = 0;
    $est_totalm = 0;
    $est_subtotalm = 0;
    $qtym = 0;
    $ivam = 0; $totlam = 0; $pricem = 0; $totalm = 0;
?>
<?php if (count($invoices_items)) : foreach ($invoices_items as $invo) : if($invo->est_project == $project_id && $invo->est_num == $est_num && $invo->est_typre == 1):?>

<tr>
    <td>
      <?php echo $invo->est_invoice; ?>
    </td>
    <td>
      <?php echo $invo->est_concept; ?>
    </td>
    <td>
      <?php echo client_company($invo->est_client); ?>
    </td>
    <td>
      $ <?php echo invoice_total($invo->est_inv); ?>
    </td>
    <td>
      <?php $totalm += $invo->est_amount; ?>
      $<?php echo format_currency($invo->est_amount); ?>
    </td>
</tr>

<?  endif; endforeach; endif;?>
<tr>
<td colspan="4" class="text-right"><strong>Subtotal:</strong></td>
<td >
  $<?php echo format_currency($totalm); ?>
</td>
</tr>
</tbody>
  </table>
  <br>
  <br>
  <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Materiales</h4></div>
  <table class="items_table_material table table-striped table-bordered table-hover table-full-width table-th-block table-success">
       <thead class="the-box light full">
       <tr>
           <th></th>
           <th>
             Proveedor
           </th>
           <th>Concepto </th>
           <th>Descripción</th>
           <th>
               Importe
           </th>

       </tr>
       </thead>
       <tbody>
        <?php
        $est_pagoma = 0;
        $est_totalma = 0;
        $est_subtotalma = 0;
        $qtyma = 0;
        $ivama = 0; $totalma = 0; $pricema = 0; $desc = " "; $concept = " ";
        ?>
       <?php if (count($invoices_items)): foreach ($invoices_items as $invo): if($invo->est_project == $project_id && $invo->est_num == $est_num && $invo->est_typre == 2): ?>
           <tr>
               <td>
                 <?php echo $invo->est_invoice;?>
               </td>
                 <td>
                   <?php echo client_company($invo->est_client); ?>
                 </td>
               <td>
                 <?php echo $invo->est_concept; ?>
               </td>
               <td>
                <?php echo $invo->est_desc; ?>
               </td>
               <td>
                 <?php
                  $totalma += $invo->est_amount;
                 ?>
                $<?php echo format_currency($invo->est_amount); ?>
               </td>
           </tr>

       <? endif; endforeach; endif;  ?>
       </tbody>
       <tr>
         <td colspan="4" class="text-right"><strong>Subtotal:</strong></td>
         <td>
           $<?php echo format_currency($totalma); ?>
         </td>
       </tr>
           </table>
           <br>
           <br>
           <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Materiales IVA</h4></div>
           <table class="items_table_materiali table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                <thead class="the-box light full">
                <tr>

                  <th></th>
                  <th>Proveedor</th>
                  <th># Factura</th>
                  <th>
                      Importe
                  </th>

                </tr>
                </thead>
                <tbody>
                <?php
                    $est_pagomai = 0;
                    $est_totalmai = 0;
                    $est_subtotalmai = 0;
                    $qtymai = 0;
                    $ivamai = 0; $totalmai = 0; $pricemai = 0;
                ?>
                <?php if (count($invoices_items)) : foreach ($invoices_items as $invo) : if($invo->est_project == $project_id && $invo->est_typre == 3 && $invo->est_num == $est_num): ?>

                    <tr>
                      <td>
                        <?php echo $invo->est_invoice;?>
                      </td>
                      <td>
                        <?php echo client_company($invo->est_client); ?>
                      </td>

                      <td>
                        <?php echo $invo->est_fac; ?>
                      </td>
                      <td>
                        <?php
                          $totalmai += $invo->est_amount;
                        ?>
                        $<?php echo format_currency($invo->est_amount); ?>
                      </td>
                    </tr>

                <? endif; endforeach; endif;  ?>
                <tr>
                  <td colspan="3" class="text-right"><strong>Subtotal:</strong></td>
                  <td>
                    $<?php echo format_currency($totalmai);?>
                    <input type="hidden" class="form-control" name="totalmai" value="<?php echo $totalmai; ?>">
                  </td>
                </tr>
                </tbody>
                    </table>
                    <br>
                    <br>
                    <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Subcontratos</h4></div>
                    <table class="items_table_sub table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                         <thead class="the-box light full">
                         <tr>

                             <th></th>
                             <th>
                               Concepto
                             </th>
                             <th>
                                Proveedor

                             </th>

                             <th>
                                 Presupuestos
                             </th>
                             <th>
                                 <?php echo lang('label_amount'); ?>

                             </th>

                         </tr>
                         </thead>
                         <tbody>
                           <?php
                             $est_pagosub = 0;
                             $est_totalsub = 0;
                             $est_subtotalsub = 0;
                             $qtysub = 0;
                             $ivasub = 0; $totalsub = 0; $pricesub = 0;
                            ?>
                         <?php if (count($invoices_items)) : foreach ($invoices_items as $invo) : if($invo->est_project == $project_id && $invo->est_typre == 4 && $invo->est_num == $est_num): ?>

                             <tr>
                                 <td>
                                   <?php echo $invo->est_invoice; ?>
                                 </td>
                                 <td>
                                  <?php echo $invo->est_concept; ?>
                                 </td>
                                 <td>
                                   <?php echo client_company($invo->est_client); ?>
                                 </td>
                                 <td>
                                   <?php echo invoice_total($invo->est_inv); ?>
                                 </td>
                                 <td>
                                   <?php
                                    $totalsub += $invo->est_amount;
                                   ?>
                                   $<?php echo format_currency($invo->est_amount);?>
                                 </td>
                             </tr>

                         <?  endif; endforeach; endif;  ?>
                         <tr>
                           <td colspan="4" class="text-right"><strong>Subtotal:</strong></td>
                           <td >
                             $<?php echo format_currency($totalsub);?>
                           </td>
                         </tr>
                         </tbody>
                             </table>
</div>

        <?php
            $subtotal =0.00;
            $subtotal2 = 0;
            $subtotal1 = 0;
            $iva =0.00;
            $iva2 = 0;
            $ivama = 0;
            $honorarios=0.00;
            $traslados=0.00;
            $total=0.00;
            $error = "";
            $invoice_total1 = 0;
            $est_pago1 = 0;
            $totaltotal = 0;
            $totaltotal2 = 0;
            $tresporciento = 0.00;
            ?>
            <?php if(count($totals)): foreach($totals as $total): if($total->project_id == $project_id):
                $subtotal = $total->mano_obra + $total->materiales + $total->materiales_iva + $total->subcont;
                $ivama = $total->iva;
                $traslados = $total->traslados;
                $honorarios = $total->honorarios;
               endif; endforeach; endif;
              $totaltotal = $subtotal;
              $total = $subtotal;
            ?>


            <!-- aqui empieza  -->
              <div class="col-md-12 col-md-offset-6" style="text-align:right">

              <!-- <div class="row invoice-payment" > -->
              <div align="right">

                  <table style="text-align:right" >
                    <tbody>
                      <tr>
                        <td></td>
                        <td></td>

                        <td ><strong>Subtotal:</strong></td>
                        <td width=10%>$<?php echo format_currency($subtotal); ?></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td></td>
                        <td >
                          <strong>IVA Materiales:</strong>
                        </td>
                        <td>
                          <?php echo format_currency($ivama);?>
                        </td>
                      </tr>
                      <tr>
                        <td></td>
                        <td></td>

                        <td >
                          <strong>Traslados, acarreos, herramientas <br>y  equipo menor, etc., 3% </strong>
                        </td>
                        <td >
                          $<?php echo format_currency($traslados); ?>
                        </td>
                      </tr>
                      <tr>
                        <td></td>
                        <td></td>

                        <td ><strong>Honorarios:</strong></td>
                        <td >
                          $<?php echo format_currency($honorarios); ?>

                        </td>
                      </tr>
                      <tr>
                        <td></td>
                        <td></td>

                        <td ><strong>Total:</strong></td>
                        <td >
                          <div>
                            <?php
                            $totaltotal = $subtotal + $ivama + $traslados + $honorarios;
                            echo format_currency($totaltotal);
                            ?>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>






            <!-- aqui termina -->
            <br>
            <br>
            <div class="col-md-12">
              <div style="background:#d3d3d3;width: 100%;height: 45px;margin-top: 196px;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;">Resumen</h4></div>
             <table class="items_table_sub table table-striped table-bordered table-hover table-full-width table-th-block table-success">
                  <thead class="the-box light full">
                  <tr>
                      <th>
                        Concepto
                      </th>
                      <th>
                        Importe
                      </th>
                      <th>
                        Anterior
                      </th>
                      <th>
                        Acumulado
                      </th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php
                      $anteriormano = 0; $manototal = 0; $materialestotal = 0; $anteriormateriales = 0; $acumualadomateriales = 0;
                      $materialesivatotal = 0; $anteriormaterialesiva = 0; $acumualadomaterialesiva = 0; $subconttotal = 0;
                      $anteriorsubcont = 0; $acumualadosubcont = 0; $honorariostotal = 0; $anteriorhonorarios = 0;
                      $acumualadohonorarios = 0; $trasladostotal = 0; $anteriortraslados = 0; $acumualadotraslados = 0;
                      $importeiva = 0; $anterioiva = 0; $acumuladoiva = 0; $subtotalimporte = 0; $subtotalacumulado = 0; $acumualadomano = 0;
                      $espaciod = 0; $cuentac = 0;
                    ?>
                    <?php if(count($totals)): foreach($totals as $total): if($total->project_id == $project_id):  ?>
                      <tr>
                          <td>
                            I. Mano de obra
                          </td>
                          <td>
                            <?php if(count($totals)): foreach($totals as $totalunico): if($totalunico->project_id == $project_id && $totalunico->id_amount == $est_num):  ?>
                              <?php $manototal = $totalunico->mano_obra; ?>
                             $<?php echo format_currency($manototal); ?>
                            <?php endif; endforeach; endif; ?>
                          </td>
                          <td>
                              <?php if($est_num == 1){ ?>
                                $<?php echo format_currency(0); ?>
                              <?php }elseif($est_num != 1){ ?>
                                <?php $anteriormano += $total->mano_obra; ?>
                               $<?php echo format_currency(mano_obra($total->project_id,$est_numA)); ?>
                             <?php } ?>
                          </td>
                          <td>
                            <?php $acumualadomano = $manototal + $anteriormano; ?>
                             $<?php echo format_currency(mano_obra_a($total->project_id)); ?>
                          </td>
                      </tr>
                      <tr>
                          <td>
                            II. Materiales
                          </td>
                          <td>
                            <?php if(count($totals)): foreach($totals as $totalunico): if($totalunico->project_id == $project_id && $totalunico->id_amount == $est_num):  ?>
                              <?php $materialestotal = $totalunico->materiales; ?>
                             $<?php echo format_currency($materialestotal); ?>
                           <?php endif; endforeach; endif; ?>
                          </td>
                          <td>
                            <?php if($est_num == 1){ ?>
                                $<?php echo format_currency(0); ?>
                            <?php }elseif($est_num != 1){ ?>
                                <?php $anteriormateriales += $total->materiales; ?>
                               $<?php echo format_currency(materiales($total->project_id,$est_numA)); ?>
                             <?php } ?>
                          </td>
                          <td>
                            <?php $acumualadomateriales = $materialestotal + $anteriormateriales; ?>
                             $<?php echo format_currency(materiales_a($total->project_id)); ?>
                          </td>
                      </tr>
                      <tr>
                          <td>
                            III. Materiales IVA
                          </td>
                          <td>
                            <?php if(count($totals)): foreach($totals as $totalunico): if($totalunico->project_id == $project_id && $totalunico->id_amount == $est_num):  ?>
                              <?php $materialesivatotal = $totalunico->materiales_iva; ?>
                             $<?php echo format_currency($materialesivatotal); ?>
                           <?php endif; endforeach; endif; ?>
                          </td>
                          <td>
                            <?php if($est_num == 1){ ?>
                                $<?php echo format_currency(0); ?>
                            <?php }elseif($est_num != 1){ ?>
                              <?php $anteriormaterialesiva = materiales_iva($total->project_id,$est_numA); ?>
                             $<?php echo format_currency(materiales_iva($total->project_id,$est_numA)); ?>
                            <?php } ?>
                          </td>
                          <td>
                            <?php $acumualadomaterialesiva = materiales_iva_a($total->project_id); ?>
                             $<?php echo format_currency(materiales_iva_a($total->project_id)); ?>
                          </td>
                      </tr>
                      <tr>
                          <td>
                            IV. Subcontratos
                          </td>
                          <td>
                            <?php if(count($totals)): foreach($totals as $totalunico): if($totalunico->project_id == $project_id && $totalunico->id_amount == $est_num):  ?>
                              <?php $subconttotal = $totalunico->subcont; ?>
                             $<?php echo format_currency($subconttotal); ?>
                            <?php endif; endforeach; endif; ?>
                          </td>
                          <td>
                            <?php if($est_num == 1){ ?>
                                $<?php echo format_currency(0); ?>
                            <?php }elseif($est_num != 1){ ?>
                              <?php $anteriorsubcont += $total->subcont; ?>
                             $<?php echo format_currency(subcontrato($total->project_id,$est_numA)); ?>
                            <?php } ?>
                          </td>
                          <td>
                            <?php $acumualadosubcont = subcontrato_a($total->project_id); ?>
                             $<?php echo format_currency(subcontrato_a($total->project_id)); ?>
                          </td>
                      </tr>
                      <tr>
                          <td>
                            V. Honorarios
                          </td>
                          <td>
                            <?php if(count($totals)): foreach($totals as $totalunico): if($totalunico->project_id == $project_id && $totalunico->id_amount == $est_num):  ?>
                              <?php $honorariostotal = $totalunico->honorarios; ?>
                             $<?php echo format_currency($honorariostotal); ?>
                           <?php endif; endforeach; endif; ?>
                          </td>
                          <td>
                            <?php if($total->id_amount == 1){ ?>
                                $<?php echo format_currency(0); ?>
                            <?php }else{ ?>
                              <?php $anteriorhonorarios += $total->honorarios; ?>
                             $<?php echo format_currency(honorarios($total->project_id,$est_numA)); ?>
                            <?php } ?>
                          </td>
                          <td>
                            <?php $acumualadohonorarios = $honorariostotal + $anteriorhonorarios; ?>
                             $<?php echo format_currency(honorarios_a($total->project_id)); ?>
                          </td>
                      </tr>
                      <tr>
                          <td>
                            VI. Traslados, Acarreos, Etc.
                          </td>
                          <td>
                            <?php if(count($totals)): foreach($totals as $totalunico): if($totalunico->project_id == $project_id && $totalunico->id_amount == $est_num):  ?>
                              <?php $trasladostotal = $totalunico->traslados; ?>
                             $<?php echo format_currency($trasladostotal); ?>
                           <?php endif; endforeach; endif; ?>
                          </td>
                          <td>
                            <?php if($total->id_amount == 1){ ?>
                                $<?php echo format_currency(0); ?>
                            <?php }else{ ?>
                              <?php $anteriortraslados += $total->traslados; ?>
                             $<?php echo format_currency(traslados($total->project_id,$est_numA)); ?>
                            <?php } ?>
                          </td>
                          <td>
                            <?php $acumualadotraslados = $trasladostotal + $anteriortraslados; ?>
                             $<?php echo format_currency(traslados_a($total->project_id)); ?>
                          </td>
                      </tr>
                    <?php endif; endforeach; endif; ?>
                      <tr>
                        <td colspan="1" class="text-right"><strong>IVA:</strong></td>
                        <td >
                          <?php $importeiva = $materialesivatotal * 0.16; ?>
                          $ <?php echo format_currency($importeiva); ?>
                        </td>
                        <td >
                          <?php $anterioiva = $anteriormaterialesiva * 0.16;  ?>
                          $ <?php echo format_currency($anterioiva); ?>
                        </td>
                        <td >
                          <?php $acumuladoiva = $acumualadomaterialesiva *0.16; ?>
                          $ <?php echo format_currency($acumuladoiva); ?>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="1" class="text-right"><strong>Subtotal:</strong></td>
                        <td >
                          <?php $subtotalimporte = $manototal + $materialestotal + $materialesivatotal + $subconttotal + $honorariostotal + $trasladostotal + $importeiva; ?>
                          $ <?php echo format_currency($subtotalimporte); ?>
                        </td>
                        <td class="text-right"><strong>Total Acumulado:</strong></td>
                        <td >
                          <?php $subtotalacumulado = $acumualadomano + $acumualadomateriales + $acumualadomaterialesiva + $acumualadosubcont + $acumualadohonorarios + $acumualadotraslados + $acumuladoiva; ?>
                          $ <?php echo format_currency($subtotalacumulado); ?>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="3" class="text-right"><strong>Anticipos:</strong></td>
                        <td >$ <?php echo format_currency(0); ?></td>
                      </tr>
                      <tr>
                        <td colspan="3" class="text-right"><strong>Avance de Obra</strong></td>
                        <?php if (count($projects)) : foreach ($projects as $project):?>
                        <td >$ <?php echo format_currency($project->fondo); ?></td>
                      <?php endforeach; endif;?>
                      </tr>
                      <tr>
                        <td colspan="3" class="text-right"><strong>Saldo:</strong></td>
                        <td >$ <?php echo format_currency(0); ?></td>
                      </tr>
                      <tr>
                        <td colspan="3" class="text-right"><strong>Espacio Diafano SA de CV:</strong></td>
                        <td >
                          <?php
                            $espaciod = $materialesivatotal * 0.16;
                            ?>
                          $ <?php echo format_currency($materialesivatotal); ?>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="3" class="text-right"><strong>Cuenta Corriente:</strong></td>
                        <td>
                          <?php $cuentac = $subtotalimporte - $materialesivatotal;?>
                          $ <?php echo format_currency($cuentac); ?>
                        </td>
                      </tr>
                  </tbody>
                  </table>
            </div>

</body>
</html>
