<?php
// $report_id=13;
// $reporte = get_report($report_id);
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 1/23/2016
 * Time: 5:53 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title></title>

    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/invoice.css'); ?>" rel="stylesheet" type="text/css" />
    <style>


        table.table tbody tr.even td {
            background: #f6f8f9;
        }
        .invoice-header ul {
            margin-bottom: 0;
        }
        .invoice-header {
            margin-bottom: 20px;
            padding-bottom: 20px;

        }
        .invoice-header h3 {
            margin-bottom: 0;
            margin-top: 8px;
            font-weight: 400;
        }
        .invoice-header span {
            color: #999;
        }

        .invoice-details {
            float: right;
            width: 260px;
            margin-top: 4px;
            max-width: 100%;
        }
        .invoice-details strong {
            float: right;
            margin-left: 20px;
        }

        .invoice-payment {


            float: right;
            width: 250px;
            border: 1px solid #d6dde2;
            margin: 20px 0 40px 0;
            padding: 0;
            border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;
            text-align: right;

        }
        .invoice-payment .table h6 {
            margin-bottom: 0;
        }
        .chosen-payment span {
            color: #D65C4F;
            text-decoration: underline;
            font-weight: 600;
        }
        .chosen-payment a {
            font-size: 11px;
            color: #999;
        }

        .invoice-status {
            margin-top: 12px;
            padding-top: 12px;
            border-top: 1px solid #eee;
        }



        .text-right {
            text-align: right;
        }

        .text-center {
            text-align: center;
        }

        @media (max-width: 768px) {
            .invoice-details {
                float: none;
                margin-top: 12px;
            }
        }


        .invoice-items{
            border: 1px solid #d6dde2;
            border-radius: 4px; -moz-border-radius: 4px; -webkit-border-radius: 4px;
        }
        .invoice-items td,.invoice-items th{padding:0.2em 0.4em 0.4em}

        .terms {
            clear: both;
            margin-bottom: 0;
            padding:20px 10px;
            border-top: 1px solid #d6dde2;
        }


    </style>


</head>

<body>

<table class="table table-striped">
    <tr>
        <td height="90" style="width:70%">
            <img height="80" src="<?php echo base_url('files/transaction_logos/'.$this->config->item('invoice')->invoice_logo); ?>" />
            <div class="invoice-header">
            </div>
        </td>
        <td class="text-center">
            <h3><strong><?php echo $project_details->project_title ?></strong></h3>
        </td>
        <td class="text-left" style="width:30%">
            <h3><strong>REPORTE <?php echo $reporte->message_id; ?></strong></h3>
            <ul class="invoice-details">
                <li>Fecha: <strong><?php echo date('Y-m-d'); ?></strong></li>
                <li>Realizado el: <strong><?php echo $reporte->message_date;  ?></strong></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="width:50%">

            <h5><strong>Reporte para:</strong></h5>
            <ul>
                <li><?php echo lang('label_company_name'); ?>: <strong><a href="#"><?php echo $this->config->item('company')->company_name; ?></a></strong></li>
                <li><?php echo lang('label_email'); ?>:<strong> <a href="#"><?php echo $this->config->item('company')->company_email; ?></a></strong></li>
                <li><?php echo lang('label_phone'); ?>: <strong><a href="#"><?php echo $this->config->item('company')->company_telephone; ?></a></strong></li>
            </ul>

        </td>
        <td class="text-left" style="width:50%;">
            <h5><strong>Reporte para:</strong></h5>
            <ul>
                <?php


                $customer_details = get_client_details($project_details->client_id);

                $estimate_status = "PENDING";
                if ($estimate_status == 'PENDING') {
                    $label = "info";
                } elseif ($estimate_status == 'ACCEPTED') {
                    $label = "success";
                } else {
                    $label = "danger";
                }
                ?>
                <li><?php echo lang('label_estimate_company'); ?>: <a href="#"><strong class="pull-right"><?php echo $customer_details->client_name; ?></strong></a></li>
                <li><?php echo lang('label_city'); ?>: <a href="#" class="pull-right"><?php echo $customer_details->client_city; ?></a></li>
                <li><?php echo lang('label_phone'); ?>: <strong class="pull-right"><?php echo $customer_details->client_phone_no; ?></strong></li>
            </ul>

        </td>
    </tr>
</table>
<table style="border: 1px solid black; width: 90%; margin-left: 10%">
  <thead>
    <tr>
      <th style="padding: 5px;" class="text-center"><strong>Temas Vistos</strong> </th>
    </tr>
  </thead>
</table>
<br/>
<div class="invoice-items">
    <table class="table table-striped" style="width: 100%;">
        <thead>
        <tr>


            <th>Descripción</th>

        </tr>
        </thead>
        <tbody>

            <tr class="show_border">
                <td>
                  <?php echo $reporte->message_text; ?>
                </td>


            </tr>


        </tbody>
    </table>
</div>
<br/>

<div class="invoice-payment">
  <table class="table" style="width: 100%;">

      <tbody>


          <tr class="show_border">
              <td>Arquitecto SUP. Obra:</td>
              <td>
  <?php

  $user = get_user_details($reporte->message_by_id);

  echo $user->emp_name." ".$user->emp_surname; ?>
              </td>


          </tr>


      </tbody>
  </table>

</div>



<div class="footer">

</div>

</body>
</html>
