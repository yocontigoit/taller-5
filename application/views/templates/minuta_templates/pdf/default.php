<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 1/23/2016
 * Time: 5:53 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title></title>

    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/invoice.css'); ?>" rel="stylesheet" type="text/css" />
    <style>


        table.table tbody tr.even td {
            background: #f6f8f9;
        }
        .invoice-header ul {
            margin-bottom: 0;
        }
        .invoice-header {
            margin-bottom: 20px;
            padding-bottom: 20px;

        }
        .invoice-header h3 {
            margin-bottom: 0;
            margin-top: 8px;
            font-weight: 400;
        }
        .invoice-header span {
            color: #999;
        }

        .invoice-details {
            float: right;
            width: 260px;
            margin-top: 4px;
            max-width: 100%;
        }
        .invoice-details strong {
            float: right;
            margin-left: 20px;
        }

        .invoice-payment {


            float: right;
            width: 250px;
            border: 1px solid #d6dde2;
            margin: 20px 0 40px 0;
            padding: 0;
            border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;
            text-align: right;

        }
        .invoice-payment .table h6 {
            margin-bottom: 0;
        }
        .chosen-payment span {
            color: #D65C4F;
            text-decoration: underline;
            font-weight: 600;
        }
        .chosen-payment a {
            font-size: 11px;
            color: #999;
        }

        .invoice-status {
            margin-top: 12px;
            padding-top: 12px;
            border-top: 1px solid #eee;
        }



        .text-right {
            text-align: right;
        }

        @media (max-width: 768px) {
            .invoice-details {
                float: none;
                margin-top: 12px;
            }
        }


        .invoice-items{
            border: 1px solid #d6dde2;
            border-radius: 4px; -moz-border-radius: 4px; -webkit-border-radius: 4px;
        }
        .invoice-items td,.invoice-items th{padding:0.2em 0.4em 0.4em}

        .terms {
            clear: both;
            margin-bottom: 0;
            padding:20px 10px;
            border-top: 1px solid #d6dde2;
        }


    </style>


</head>

<body>

<table>
    <tr>
        <td height="90" style="width:70%">
            <img height="80" src="<?php echo base_url('files/transaction_logos/'.$this->config->item('invoice')->invoice_logo); ?>" />
            <div class="invoice-header">
            </div>
        </td>
        <td class="text-left" style="width:30%">
            <h3><strong>ESTIMATE</strong></h3>
            <ul class="invoice-details">
                <li><?php echo lang('label_estimate_no'); ?># <strong class="text-danger"><?php echo $estimate->est_code; ?></strong></li>
                <li><?php echo lang('label_estimate_date'); ?>: <strong><?php echo date('Y-m-d',strtotime($estimate->est_trans_date)); ?></strong></li>
                <li><?php echo lang('label_estimate_due'); ?>: <strong><?php echo $estimate->est_due_date; ?></strong></li>

            </ul>
        </td>
    </tr>
    <br/><br/>
    <tr>
        <td style="width:70%">

            <h5><strong><?php echo lang('label_estimate_from'); ?>:</strong></h5>
            <ul>
                <li><?php echo lang('label_company_name'); ?>: <strong><a href="#"><?php echo $this->config->item('company')->company_name; ?></a></strong></li>
                <li><?php echo lang('label_location_address_1'); ?>: <strong><a href="#"><?php echo $this->config->item('company')->company_address_1; ?></a></strong></li>
                <li><?php echo lang('label_location_address_2'); ?>: <strong><a href="#"><?php echo $this->config->item('company')->company_address_2; ?></a></strong></li>
                <li><?php echo lang('label_location_address_3'); ?>: <strong><a href="#"><?php echo $this->config->item('company')->company_address_3; ?></a></strong></li>
                <li><?php echo lang('label_email'); ?>:<strong> <a href="#"><?php echo $this->config->item('company')->company_email; ?></a></strong></li>
                <li><?php echo lang('label_phone'); ?>: <strong><a href="#"><?php echo $this->config->item('company')->company_telephone; ?></a></strong></li>
                <li><?php echo lang('label_tax_ref'); ?>: <strong><a href="#"><?php echo $this->config->item('company')->company_vat_no; ?></a></strong></li>
            </ul>

        </td>
        <td class="text-left" style="width:30%;">
            <h5><strong><?php echo lang('label_estimate_to'); ?>:</strong></h5>
            <ul>
                <?php
                $customer_details = get_client_details($estimate->client_id);

                $estimate_status = get_estimate_status_name($estimate->est_status);
                if ($estimate_status->status_name == 'PENDING') {
                    $label = "info";
                } elseif ($estimate_status->status_name == 'ACCEPTED') {
                    $label = "success";
                } else {
                    $label = "danger";
                }
                ?>
                <li><?php echo lang('label_estimate_company'); ?>: <a href="#"><strong class="pull-right"><?php echo $customer_details->client_name; ?></strong></a></li>
                <li><?php echo lang('label_city'); ?>: <a href="#" class="pull-right"><?php echo $customer_details->client_city; ?></a></li>
                <li><?php echo lang('label_country'); ?>: <a href="#" class="pull-right"><?php echo get_country_name($customer_details->client_country); ?></a></li>
                <li><?php echo lang('label_phone'); ?>: <strong class="pull-right"><?php echo $customer_details->client_phone_no; ?></strong></li>
                <li><?php echo lang('label_tax_ref'); ?>: <strong class="pull-right"><?php echo $customer_details->client_vat_no; ?></strong></li>
                <li class="invoice-status"><strong><?php echo lang('label_status'); ?>:</strong> <div class="label label-<?php echo $label; ?> pull-right"><?php echo $estimate_status->status_name; ?></div></li>
            </ul>

        </td>
    </tr>
</table>
<br/>
<div class="invoice-items">
    <table class="table table-striped" style="width: 100%;">
        <thead>
        <tr>


            <th><?php echo lang('label_est_item'); ?></th>
            <th><?php echo lang('label_est_description'); ?></th>
            <th><?php echo lang('label_est_item_quantity'); ?></th>
            <th><?php echo lang('label_est_item_rate'); ?></th>
            <th><?php echo lang('label_estimate_discount'); ?></th>
            <th><?php echo lang('label_tax'); ?></th>
            <th><?php echo lang('label_amount'); ?></th>
        </tr>
        </thead>
        <tbody>

        <?php  if (count($est_items)) : foreach ($est_items as $item) :  ?>
            <tr class="show_border">
                <td>
                  <?php echo get_item_name($item->item_id) ;?>
                </td>


                <td>
                   <?php echo $item->item_description; ?>
                </td>


                <td>
                   <?php echo $item->details_hours; ?>
                </td>

                <td>
                    <?php echo $item->details_rate; ?>
                </td>

                <td>
                   <?php echo $item->details_discount; ?>
                </td>

                <td>
                  <?php echo $item->details_tax; ?>
                </td>

                <td class="text-right"><strong><?php echo calculate_line_subtotal($item->details_id); ?></strong></td>

            </tr>

        <?php  endforeach; endif ?>

        </tbody>
    </table>
</div>
<br/>

<div class="invoice-payment">

    <table width="100%">
        <tbody>

        <tr>
            <th><?php echo lang('label_sub_total') ;?>:</th>

            <td class="text-right"><?php echo ($estimate->est_currency) ? get_currency_symbol_by_name($estimate->est_currency).format_amount(calculate_estimate_sub_total($estimate->est_id)):format_currency(calculate_estimate_sub_total($estimate->est_id)) ;?></td>
        </tr>
        <tr>
            <th><?php echo lang('label_tax'); ?>:</th>

            <td class="text-right"><?php echo ($estimate->est_currency) ? get_currency_symbol_by_name($estimate->est_currency).format_amount(calculate_estimate_tax_total($estimate->est_id)):format_currency(calculate_estimate_tax_total($estimate->est_id)) ;?></td>
        </tr>
        <tr>
            <th><?php echo lang('label_total'); ?>:</th>
            <td class="text-right text-danger"><h6><?php echo ($estimate->est_currency) ? get_currency_symbol_by_name($estimate->est_currency).format_amount(calculate_estimate_total($estimate->est_id)):format_currency(calculate_estimate_total($estimate->est_id)) ;?></h6></td>
        </tr>
        </tbody>
    </table>

</div>



<div class="footer">
    <h6><strong>Notes &amp; Information:</strong></h6>
    <p>
        <?php echo $estimate->est_notes; ?>
    </p>
</div>

</body>
</html>