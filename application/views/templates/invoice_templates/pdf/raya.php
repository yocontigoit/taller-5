<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/04/18
 * Time: 02:04 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="dompdf.view" content="XYZ,0,0,1" />

    <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url('assets/css/invoice.css'); ?>" rel="stylesheet" type="text/css" />

</head>
<body>
<div id="invoice">
    <table>
        <tr>
            <td class="col-md-8">
                <img height="80" src="<?php echo base_url('files/transaction_logos/'.$this->config->item('invoice')->invoice_logo); ?>" style="margin-top: 45px" />
                <p class="details"><strong>Luis Lozano 309-A Col.Panorama</strong></p>
                <p class="details"><strong>León, Gto.,Mexico   37160</strong></p>
                <p class="details"><strong>Tel. (477) 718-5477     Fax. (477) 773-3277</strong></strong></p>


            </td>
            <td class="col-md-4">
                <span class="h1"><?php echo $invoice_details->inv_no; ?></span>

                <table class="border">
                    <tr>
                        <th class="col-md-6">Raya No.</th>
                        <th class="col-md-6"><?php echo lang('label_date'); ?></th>
                    </tr>

                    <tr>
                        <td class="text-center"><?php echo $invoice_details->inv_no; ?></td>
                        <td class="text-center"><?php echo  date("Y") . " - " . date("m") . " - " . date("d"); ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
<br><br>
    <table>
        <tr>

            <td class="col-md-6">
                <?php

                $customer_details = get_client_details($invoice_details->client_id);
                $invoice_status = get_invoice_status($invoice_details->status);

                if($invoice_status == "PAID")
                {
                    $label_text = "text-success";
                }else{
                    $label_text = "text-danger";
                }

                ?>
                <p class="text-left background-th"><span class="h5"><strong>MAESTRO</strong></span></p>
                <p class="details"> <strong class="pull-right"><?php echo $customer_details->client_name; ?></strong></p>
                <p class="details"><?php echo $customer_details->client_vat_no; ?></p>
                 <!-- <p class="details"><?php echo $customer_details->client_city; ?></p> -->
                <p class="details">Teléfono: <?php echo $customer_details->client_phone_no; ?></p>
                <!-- <p class="details"><strong><?php echo lang('label_status'); ?>:<span class="<?php echo $label_text; ?> "><?php echo $invoice_status; ?></span></strong></p> -->


            </td>
            <td class="col-md-6">
                <?php if ($invoice_details->filter == 1){ ?>
                <h3>RECOGER EN TIENDA:</h3>
<div style="font-size:14px;padding:20px;"><p><?php echo $invoice_details->notes; ?></p></div>
                <?php } ?>
                <?php if ($invoice_details->filter == 2){ ?>
                <h3>ENTREGAR EN:</h3>
<div style="font-size:14px;padding:20px;"><p><?php $project = get_project_details($invoice_details->project_id); echo $project->project_direccion; ?></p></div>

                <?php } ?>


            </td>

        </tr>
    </table>

</div>
<br/>
<div class="invoice-items">
<table class="table table-striped" style="width:100%">
    <tr><th>PERIODO</th><th></th><th></th><th>OBRA</th></tr>
    <tr><td class="text-center"><?php echo $invoice_details->created_at; ?> / <?php echo $invoice_details->invoice_due; ?></td><td class="text-center"></td><td class="text-center"></td><td class="text-center"><?php $project = get_project_details($invoice_details->project_id); echo $project->project_title; ?></td></tr>
</table>
</div><div class="invoice-items">
    <table class="table table-striped" style="width: 100%;">
    <tr>



        <th class="qty text-center">CANTIDAD</th>
        <th class="qty text-center">UNIDAD</th>
        <th class="crt text-center">NOMBRE</th>
        <th class="small text-center">PRECIO</th>
        <th class="small text-center">IMPORTE</th>
        <th class="small text-center">FIRMA</th>

    </tr>


    <?php if (count($invoice_items)) : foreach ($invoice_items as $invoice_item) : ?>

        <tr class="show_border">




            <td class="text-center">
                <?php echo $invoice_item->quantity; ?>
            </td>
            <td class="text-center">
                <?php echo $invoice_item->unit_raya; ?>
            </td>
            <td class="text-center">
                <?php echo $invoice_item->item_name; ?>
            <br>
                <?php echo $invoice_item->item_desc; ?>
            </td>
            <td class="text-center">$
                <?php echo format_currency($invoice_item->price);?>
            </td>
            <td class="text-right">$
                <?php echo format_currency(calculate_invoice_line_subtotal($invoice_item->id)); ?>
            </td>
            <td  width="15%">

            </td>
        </tr>

    <?php endforeach; endif; ?>




</table>
</div>
<br/>
<div class="invoice-items">
    <table width="90%" style="margin-left:20px">
        <tbody>
            <tr><td><strong>Supervisado por:</strong></td><td></td><td></td><td></td><td></td></tr>
            <tr><td><?php echo $invoice_details->autorizado; ?></td><td></td><td></td><td></td><td></td></tr>

        <tr><td></td><td></td><td></td><td></td>
            <th>SUBTOTAL:</th>
            <td class="text-right"><?php echo ($invoice_details->currency) ? get_currency_symbol_by_name($invoice_details->currency).format_amount(invoice_sub_total_total($invoice_details->id)) :format_currency(invoice_sub_total_total($invoice_details->id))  ;?></td>
        </tr>
        <!-- <tr><td></td><td></td><td></td><td></td>
            <th>IVA:</th>
            <td class="text-right"><?php echo ($invoice_details->currency) ? get_currency_symbol_by_name($invoice_details->currency).format_amount(invoice_tax_total($invoice_details->id)) : format_currency(invoice_tax_total($invoice_details->id));?></td>
        </tr> -->
        <tr><td></td><td></td><td></td><td></td>
            <th>TOTAL:</th>
            <td class="h4 top10 text-right text-danger"><strong><?php echo ($invoice_details->currency) ? get_currency_symbol_by_name($invoice_details->currency).format_amount(invoice_total($invoice_details->id)):format_currency(invoice_total($invoice_details->id)) ;?></strong></td>
        </tr>
        </tbody>
    </table>
  </div>
  <div class="invoice-items">
      <table width="90%" style="margin-left:20px">
          <tbody>
              <tr>
                <td>
                  <strong>Detalles:</strong>
                  <p><?php echo $invoice_details->notes; ?></p>
                </td>
              </tr>
          </tbody>
      </table>
    </div>



<div class="footer">
    <?php echo $customer_details->client_name; ?>
</div>


</body>
</html>
