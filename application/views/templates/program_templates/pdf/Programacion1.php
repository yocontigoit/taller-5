<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 1/23/2016
 * Time: 5:53 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title></title>

    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/invoice.css'); ?>" rel="stylesheet" type="text/css" />
    <style>


        table.table tbody tr.even td {
            background: #f6f8f9;
        }
        .invoice-header ul {
            margin-bottom: 0;
        }
        .invoice-header {
            margin-bottom: 20px;
            padding-bottom: 20px;

        }
        .invoice-header h3 {
            margin-bottom: 0;
            margin-top: 8px;
            font-weight: 400;
        }
        .invoice-header span {
            color: #999;
        }

        .invoice-details {
            float: right;
            width: 260px;
            margin-top: 4px;
            max-width: 100%;

        }
        .invoice-details strong {
            float: right;
            margin-left: 20px;
        }

        .invoice-payment {
            float: right;
            width: 250px;
            border: 1px solid #d6dde2;
            margin: 20px 0 40px 0;
            padding: 0;
            border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;
            text-align: right;

        }
        .invoice-payment .table h6 {
            margin-bottom: 0;
        }
        .chosen-payment span {
            color: #D65C4F;
            text-decoration: underline;
            font-weight: 600;
        }
        .chosen-payment a {
            font-size: 11px;
            color: #999;
        }

        .invoice-status {
            margin-top: 12px;
            padding-top: 12px;
            border-top: 1px solid #eee;
        }



        .text-right {
            text-align: right;
        }

        @media (max-width: 768px) {
            .invoice-details {
                float: none;
                margin-top: 12px;
            }
        }


        .invoice-items{
            border: 1px solid #d6dde2;
            border-radius: 4px; -moz-border-radius: 4px; -webkit-border-radius: 4px;
        }
        .invoice-items td,.invoice-items th{padding:0.2em 0.4em 0.4em}

        .terms {
            clear: both;
            margin-bottom: 0;
            padding:20px 10px;
            border-top: 1px solid #d6dde2;
        }


        .contenedor{
          height: 49%;
          margin: 1% 1%;
        }
    </style>


</head>

<body>

                          <?php if(count($projects)): foreach($projects as $project): ?>
                            <?php
                            $total = 0; $subtotal = 0; $Totals = 0; $totaltrans = 0; $Totalpago = 0; $Totalsaldo = 0; $pagotrans = 0;
                            $pago_a = 0; $Totalprov = 0; $totalstrans = 0; $totalsub = 0; $diafano = 0; $corriente = 0; $pago_total = 0;
                            $descuento_prject = 0; $otrototal = 0; $Totalpro_diafanoprov = 0; $Totalprov_corriente = 0; $tota_diafano = 0;
                            $tota_corriente = 0; $eltotal = 0; $nc = 0; $nc_notax = 0; $nc_tax = 0; $invoice_total_diafano = 0;
                            ?>
                            <div class="contenedor">
                            <div style="background:#d3d3d3;height:5%;margin-top:10%">
                                <table>
                                  <tr>
                                    <td style="text-align:center;"><strong>Programación</strong></td>
                                    <td style="text-align:center;"><h3><?php echo $project->project_title ?></h3></td>
                                    <td style="text-align:center;">Fecha: <strong><?php echo date('Y-m-d'); ?></strong></td>
                                  </tr>
                                </table>
                            </div>
                            <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success items_table_mano" heigth >
                              <thead class="the-box light full">
                                <tr>
                                  <th style="width: 221px;">
                                    Proveedor
                                  </th>
                                  <th style="width: 105px;">
                                    Total Presupuesto
                                  </th style="width: 84px">
                                  <th>
                                    Pago autorizado
                                  </th>
                                  <th>
                                    Saldo
                                  </th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php if(count($clients)): foreach($clients as $invo): if($project->project_id == $invo->project_id && $invo->program == 1): ?>
                                  <tr>
                                    <td>
                                      <?php echo client_company($invo->client_id);?>
                                    </td>
                                    <td>
                                      $<?php
                                      echo format_currency(invoice_total_proveedor($invo->project_id,$invo->client_id) - discount($invo->project_id,$invo->client_id,$invo->program));
                                      ?>
                                    </td>
                                    <td>
                                      <?php if($invo->client_type == 3){?>
                                        <?php $eltotal = pago_a_diafano($invo->client_id, $invo->project_id) + pago_a($invo->client_id, $invo->project_id) ?>
                                        $<?php echo format_currency($eltotal); ?>
                                      <?php }else{ ?>
                                        $ <?php echo format_currency(invoice_total_proveedor($invo->project_id,$invo->client_id) - discount($invo->project_id,$invo->client_id,$invo->program)); ?>
                                      <?php } ?>
                                    </td>
                                    <td>
                                      $<?php if($invo->client_type == 3){?>
                                        <?php
                                        $Totalprov_corriente += pago_a($invo->client_id,$invo->project_id);
                                        $Totalpro_diafanoprov += pago_a_diafano($invo->client_id,$invo->project_id);
                                        $Totalprov = $Totalprov_corriente +  $Totalpro_diafanoprov;
                                        if($Totalprov_corriente == 0 && $Totalpro_diafanoprov == 0){
                                          $total = invoice_total_proveedor($invo->project_id,$invo->client_id) - client_payments($invo->client_id,
                                          'year'); ?>
                                        <?php }else{  ?>
                                          <?php $total = invoice_total_proveedor($invo->project_id,$invo->client_id) - $eltotal;
                                        } ?>
                                      <?php } else{ ?>
                                        <?php $total = invoice_total_proveedor($invo->project_id,$invo->client_id) - invoice_total_proveedor($invo->project_id,$invo->client_id); ?>
                                      <?php } ?>
                                      <?php  $subtotal = $total; echo format_currency($subtotal - discount($invo->project_id,$invo->client_id,$invo->program)) ; ?>
                                      <?php $totalsub += $subtotal - discount($invo->project_id,$invo->client_id,$invo->program)?>
                                    </td>
                                  </tr>
                                <?php endif; endforeach; endif; ?>
                                <?php
                                if(count($transactions)): foreach($transactions as $totales): if($totales->project_id == $project->project_id && $totales->program == 1 && $totales->type_pre != 5):
                                // $Totals += pago_a($totales->client_id,$totales->project_id);
                                $descuento_prject = discount_project($totales->project_id,$totales->program);
                                $totalstrans += invoice_total($totales->id);
                                $nc = nc($totales->project_id, $totales->client_id);
                                $otrototal = $totalstrans - $descuento_prject - $nc;
                                if ($totales->pago_a != 1 ) {
                                  $totaltrans += invoice_total($totales->id) - discount($totales->project_id,$totales->client_id,$totales->program);
                                }else {}

                                $pago_total =  $Totalprov;

                                if ($totales->tax == 16 && $totales->pago_a != 1) {
                                  $nc_tax = nc_tax($totales->project_id, $totales->client_id);
                                  $invoice_total_diafano += invoice_total($totales->id);

                                  $tota_diafano = $invoice_total_diafano - $nc_tax;

                                }
                                if ($totales->tax == 0 && $totales->pago_a != 1){
                                  $nc_notax = nc_notax($totales->project_id, $totales->client_id);
                                  $tota_corriente += invoice_total($totales->id) - $nc_notax;
                                }



                                endif; endforeach; endif;
                                $pago_a =  $Totalprov + $totaltrans - $nc;
                                ?>
                                <tr>
                                  <td>
                                    <strong><p class="text-right">Totales:</p></strong>
                                  </td>
                                  <td>
                                    $<?php
                                    echo format_currency($otrototal);
                                    ?>
                                  </td>
                                  <td>
                                    $ <?php echo format_currency($pago_a); ?>
                                  </td>
                                  <td>
                                    $
                                    <?php
                                    echo format_currency($totalsub);
                                    ?>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <center>
                              <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success items_table_mano">
                                <tbody>
                                  <tr>
                                    <th>
                                      Total A Pagar:
                                    </th>
                                    <td>
                                      $<?php echo format_currency($pago_a); ?>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>
                                      Total Cuenta Corriente:
                                    </th>
                                    <td>
                                      $<?php
                                      $corriente = $Totalprov_corriente + $tota_corriente;
                                      echo format_currency($corriente);
                                      ?>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>
                                      Espacio Diafano SA de CV:
                                    </th>
                                    <td>
                                      $<?php
                                      $diafano = $Totalpro_diafanoprov + $tota_diafano;
                                      echo format_currency($diafano);
                                      ?>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </center>
                          </div>
                          <?php endforeach; endif; ?>
</div>
</body>
</html>
