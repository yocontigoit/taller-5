<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 1/23/2016
 * Time: 5:53 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title></title>

    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/invoice.css'); ?>" rel="stylesheet" type="text/css" />
    <style>


        table.table tbody tr.even td {
            background: #f6f8f9;
        }
        .invoice-header ul {
            margin-bottom: 0;
        }
        .invoice-header {
            margin-bottom: 5px;
            padding-bottom: 5px;
        }
        .invoice-header h3 {
            margin-bottom: 0;
            margin-top: 8px;
            font-weight: 400;
        }
        .invoice-header span {
            color: #999;
        }

        .invoice-details {
            float: right;
            width: 260px;
            margin-top: 4px;
            max-width: 100%;
        }
        .invoice-details strong {
            float: right;
            margin-left: 20px;
        }

        .invoice-payment {


            float: right;
            width: 250px;
            border: 1px solid #d6dde2;
            margin: 20px 0 40px 0;
            padding: 0;
            border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;
            text-align: right;

        }
        .invoice-payment .table h6 {
            margin-bottom: 0;
        }
        .chosen-payment span {
            color: #D65C4F;
            text-decoration: underline;
            font-weight: 600;
        }
        .chosen-payment a {
            font-size: 11px;
            color: #999;
        }

        .invoice-status {
            margin-top: 12px;
            padding-top: 12px;
            border-top: 1px solid #eee;
        }



        .text-right {
            text-align: right;
        }

        @media (max-width: 768px) {
            .invoice-details {
                float: none;
                margin-top: 12px;
            }
        }


        .invoice-items{
            border: 1px solid #d6dde2;
            border-radius: 4px; -moz-border-radius: 4px; -webkit-border-radius: 4px;
        }
        .invoice-items td,.invoice-items th{padding:0.2em 0.4em 0.4em}

        .terms {
            clear: both;
            margin-bottom: 0;
            padding:20px 10px;
            border-top: 1px solid #d6dde2;
        }

        .contenedor{
          height: 49%;
          margin: 1% 1%;
        }

    </style>


</head>

<body>


  <?php if(count($all_clients)): foreach($all_clients as $clients):?>
    <?php
      $totalpro = 0; $subtotalpro = 0; $Totalpro = 0; $totaltranspro = 0; $Totalpagopro = 0; $Totalsaldopro = 0; $pagotranspro = 0;
      $pago_apro = 0; $Totalspro = 0; $totalstranspro = 0; $totalsubpro = 0; $corrientepro = 0; $diafanopro = 0; $pago_totalpro = 0;
      $invoice_projecto = 0; $invoice_projecto_tax = 0; $eldescuento = 0; $total_a_corriente = 0; $total_a_diafano = 0; $Totalpro_diafano = 0;
      $Totalpro_corriente = 0; $eltotales = 0; $otrototalpro = 0;
    ?>
    <div class="contenedor">
      <div style="background:#d3d3d3;height:5%;margin-top:10%">
          <table>
            <tr>
              <td style="text-align:center;"><strong>Programación</strong></td>
              <td style="text-align:center;"><strong><h5><?php echo $clients->client_name ?></h5></strong></td>
              <td style="text-align:center;">Fecha: <strong><?php echo date('Y-m-d'); ?></strong></td>
            </tr>
          </table>
      </div>
      <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success items_table_mano">
        <thead class="the-box light full">
          <tr>
            <th>
              Obra
            </th>
            <th>
              Total Presupuesto
            </th>
            <th>
              Pago autorizado
            </th>
            <th>
              Saldo
            </th>
            <th>
              Total Cuenta Corriente:
            </th>
            <th>
              Espacio Diafano SA de CV:
            </th>
          </tr>

          <tbody>
            <?php if(count($projectos)): foreach($projectos as $invoice): if($clients->client_id == $invoice->client_id && $invoice->program == 1): ?>
              <tr>
                <td>
                  <?php echo project_company($invoice->project_id); ?>
                </td>
                <td>
                  $<?php echo format_currency(invoice_total_proveedor($invoice->project_id,$invoice->client_id,$invoice->program) - discount($invoice->project_id,$invoice->client_id,$invoice->program)); ?>

                </td>
                <td>
                  $<?php
                  if($invoice->client_type == 3){
                    $eltotales = pago_a($invoice->client_id, $invoice->project_id) + pago_a_diafano($invoice->client_id, $invoice->project_id);
                    echo format_currency($eltotales);
                  }else{
                    echo format_currency(invoice_total_proveedor($invoice->project_id,$invoice->client_id,$invoice->program) - discount($invoice->project_id,$invoice->client_id,$invoice->program));
                  }
                  ?>
                </td>
                <td>
                  $<?php if($invoice->client_type == 3){?>
                    <?php
                    $Totalpro_corriente += pago_a($invoice->client_id,$invoice->project_id);
                    $Totalpro_diafano += pago_a_diafano($invoice->client_id,$invoice->project_id);
                    $Totalpro = $Totalpro_corriente + $Totalpro_diafano;
                    if($Totalpro_corriente == 0 && $Totalpro_diafano == 0){
                      $totalpro = invoice_total_proveedor($invoice->project_id,$invoice->client_id,$invoice->program) - client_payments($invoice->client_id,
                      'year'); ?>
                    <?php }else{  ?>
                      <?php $totalpro = invoice_total_proveedor($invoice->project_id,$invoice->client_id,$invoice->program) - $eltotales;

                    } ?>
                  <?php } else{ ?>
                    <?php $totalpro = invoice_total_proveedor($invoice->project_id,$invoice->client_id,$invoice->program) - invoice_total_proveedor($invoice->project_id,$invoice->client_id,$invoice->program); ?>
                  <?php } ?>
                  <?php $subtotalpro = $totalpro; echo format_currency($subtotalpro - discount($invoice->project_id,$invoice->client_id,$invoice->program));   ?>
                  <?php $totalsubpro += $subtotalpro - discount($invoice->project_id,$invoice->client_id,$invoice->program)?>
                </td>
                <td>
                  <div class="col-md-6">
                    $<?php
                    if($Totalpro_corriente == 0){
                      $nc_notax = nc_notax($invoice->project_id, $invoice->client_id);
                      $total_a_corriente = invoice_projecto($invoice->project_id,$invoice->program, $invoice->client_id) - discount($invoice->project_id,$invoice->client_id,$invoice->program) - $nc_notax ;
                    }else {
                      $total_a_corriente += pago_a($invoice->client_id,$invoice->project_id);
                    }
                    echo format_currency($total_a_corriente);
                    ?>
                  </div>
                  <div class="col-md-2">
                    <a style="color: #2196F3;" data-toggle="tooltip" title="PDF" href="<?php echo base_url('billing/pdf_download_program3/'.$invoice->project_id.'/'.$invoice->client_id); ?>"><i class="fa fa-print" aria-hidden="true"></i></a>
                  </div>
                  <div class="col-md-2">
                    <a style="color: #2196F3;" title="Editar Concepto" href="<?php echo base_url('billing/concept_edit_modal/'.$invoice->project_id.'/'.$invoice->client_id); ?>" data-toggle="zestModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                  </div>
                </td>
                <td>
                  <div class="col-md-6">
                    $<?php
                    if($Totalpro_diafano == 0){
                      $nc_tax = nc_tax($invoice->project_id, $invoice->client_id);
                      $total_a_diafano = invoice_projecto_tax($invoice->project_id,$invoice->program, $invoice->client_id) - discount_tax($invoice->project_id,$invoice->client_id,$invoice->program) - $nc_tax;
                    }else {
                      $total_a_diafano += pago_a_diafano($invoice->client_id,$invoice->project_id);
                    }
                    echo format_currency($total_a_diafano);
                    ?>
                  </div>
                  <div class="col-md-2">
                    <a style="color: #2196F3;" data-toggle="tooltip" title="PDF" href="<?php echo base_url('billing/pdf_download_program4/'.$invoice->project_id.'/'.$invoice->client_id); ?>"><i class="fa fa-print" aria-hidden="true"></i></a>
                  </div>
                  <div class="col-md-2">
                    <a style="color: #2196F3;" title="Editar Concepto" href="<?php echo base_url('billing/concept_edit_modal/'.$invoice->project_id.'/'.$invoice->client_id); ?>" data-toggle="zestModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                  </div>
                </td>
              </tr>
            <?php endif; endforeach; endif; ?>
            <?php
            if(count($transactions)): foreach($transactions as $totalespro): if($totalespro->client_id == $clients->client_id && $totalespro->program == 1 ):
            // $Totalspro += pago_a($totalespro->client_id,$totalespro->project_id);
            $eldescuentopro = discount_client($totalespro->client_id,$totalespro->program);
            $totalstranspro += invoice_total($totalespro->id);
            $otrototalpro = $totalstranspro - $eldescuentopro;
            if ($totalespro->pago_a != 1 ) {
              $totaltranspro += invoice_total($totalespro->id) - discount_client($totalespro->client_id,$totalespro->program);
            }else {}
            endif; endforeach; endif;
            $pago_apro = $Totalpro + $totaltranspro;
            ?>
            <tr>
              <td>
                <strong><p class="text-right">Totales:</p></strong>
              </td>
              <td>
                $<?php
                echo format_currency($otrototalpro);
                ?>
              </td>
              <td>
                <strong><p class="text-info">$<?php echo format_currency($pago_apro); ?></p></strong>
              </td>
              <td>
                $
                <?php
                echo format_currency($totalsubpro);
                ?>
              </td>
              <td>
                <?php $invoice_projecto += $total_a_corriente; ?>
                $<?php echo format_currency($invoice_projecto); ?>
              </td>
              <td>
                <?php $invoice_projecto_tax += $total_a_diafano; ?>
                $<?php echo format_currency($invoice_projecto_tax); ?>
              </td>
            </tr>
          </tbody>
        </table>
    </div>
  <?php endforeach; endif; ?>
</div>
</body>
</html>
