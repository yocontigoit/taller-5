<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 1/23/2016
 * Time: 5:53 PM
 */
?>
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <title></title>

    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/css/invoice.css'); ?>" rel="stylesheet" type="text/css" />
    <style>


        table.table tbody tr.even td {
            background: #f6f8f9;
        }
        .invoice-header ul {
            margin-bottom: 0;
        }
        .invoice-header {
            margin-bottom: 5px;
            padding-bottom: 5px;
        }
        .invoice-header h3 {
            margin-bottom: 0;
            margin-top: 8px;
            font-weight: 400;
        }
        .invoice-header span {
            color: #999;
        }

        .invoice-details {
            float: right;
            width: 260px;
            margin-top: 4px;
            max-width: 100%;
        }
        .invoice-details strong {
            float: right;
            margin-left: 20px;
        }

        .invoice-payment {


            float: right;
            width: 250px;
            border: 1px solid #d6dde2;
            margin: 20px 0 40px 0;
            padding: 0;
            border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;
            text-align: right;

        }
        .invoice-payment .table h6 {
            margin-bottom: 0;
        }
        .chosen-payment span {
            color: #D65C4F;
            text-decoration: underline;
            font-weight: 600;
        }
        .chosen-payment a {
            font-size: 11px;
            color: #999;
        }

        .invoice-status {
            margin-top: 12px;
            padding-top: 12px;
            border-top: 1px solid #eee;
        }



        .text-right {
            text-align: right;
        }

        @media (max-width: 768px) {
            .invoice-details {
                float: none;
                margin-top: 12px;
            }
        }

        .text-center {
          text-align: center;
        }

        .invoice-items{
            border: 1px solid #d6dde2;
            border-radius: 4px; -moz-border-radius: 4px; -webkit-border-radius: 4px;
        }
        .invoice-items td,.invoice-items th{padding:0.2em 0.4em 0.4em}

        .terms {
            clear: both;
            margin-bottom: 0;
            padding:20px 10px;
            border-top: 1px solid #d6dde2;
        }



    </style>


</head>

<body>

  <table>
      <tr>
          <td class="col-md-8">

              <img height="80" src="<?php echo base_url('files/transaction_logos/'.$this->config->item('invoice')->invoice_logo); ?>" />
              <p class="details"><strong>Tel. (477) 718-5477</strong></strong></p>
          </td>
          <td class="text-right" style="width:30%">
              <ul class="invoice-details">
                  <li><strong><?php echo "LEON, GTO., " . date("d") . " DEL " . date("m") . " DEL " . date("Y"); ?></strong></li>
              </ul>
          </td>
      </tr>
  </table>
<div class="table-responsive">
<br>
<br>
<div class="table-responsive">
    <?php if(count($all_clients)): foreach($all_clients as $clients): if ($clients->client_id == $client_id): ?>
      <?php
        $totalpro = 0; $subtotalpro = 0; $Totalpro = 0; $totaltranspro = 0; $Totalpagopro = 0; $Totalsaldopro = 0; $pagotranspro = 0;
        $pago_apro = 0; $Totalspro = 0; $totalstranspro = 0; $totalsubpro = 0; $corrientepro = 0; $diafanopro = 0; $pago_totalpro = 0;
        $invoice_projecto = 0; $invoice_projecto_tax = 0; $Totalpro_corriente = 0; $total_a_corriente = 0; $nc_notax = 0; $concept = " ";
      ?>
    <div style="background:#d3d3d3;width:100%;text-align:center;"><h4 style="padding:10px;margin-bottom:0px;"><?php echo $clients->client_name ?></h4></div>
    <table class="table table-striped table-bordered table-hover table-full-width table-th-block table-success items_table_mano">
      <tbody>
        <?php if(count($projectos)): foreach($projectos as $invoice): if($invoice->project_id == $project_id && $clients->client_id == $invoice->client_id && $invoice->program == 1): ?>
        <tr>
          <td><strong>CONCEPTO</strong></td>
          <td>
            <?php $concept = $invoice->concept; echo $concept; ?>
          </td>
        </tr>
        <tr>
          <td><strong>METODO DE PAGO</strong></td>
          <td>
            CUENTA CORRIENTE
          </td>
        </tr>
        <tr>
          <td><strong>OBRA</strong></td>
          <td>
            <?php echo project_company($invoice->project_id); ?>
          </td>
        </tr>
        <tr>
          <td><strong>TOTAL</strong></td>
          <td>
            <?php $Totalpro_corriente += pago_a($invoice->client_id,$invoice->project_id); ?>
            $<?php
              if($Totalpro_corriente == 0){
                  $nc_notax = nc_notax($invoice->project_id, $invoice->client_id);
                  $total_a_corriente = invoice_projecto($invoice->project_id,$invoice->program, $invoice->client_id) - discount($invoice->project_id,$invoice->client_id,$invoice->program) - $nc_notax;
              }else {
                  $total_a_corriente += pago_a($invoice->client_id,$invoice->project_id);
              }
              echo format_currency($total_a_corriente);
            ?>
          </td>
        </tr>
      <?php endif; endforeach; endif; ?>
      </tbody>
    </table>
    <br>
    <br>
    <hr align="right" noshade="noshade" size="2" width="50%" />
    <table>
      <tr>
        <td class="text-center" style="width:30%">
          <strong><?php echo $clients->client_name ?></strong>
        </td>
      </tr>
    </table>


  <?php endif; endforeach; endif; ?>
  </div>
</div>
</body>
</html>
