<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/10
 * Time: 11:45 AM
 */

$this->load->view('components/lheader');
$this->load->view($content);
?>
<br /><br />
<div class="modal-footer">
<?php echo ucwords(strtolower($this->config->item('company')->company_name)). " v".$this->config->item('system')->version; ?>
&copy; <?php echo date('Y'); ?>
</div>
<?php
$this->load->view('components/_footer');