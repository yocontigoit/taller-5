<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/18
 * Time: 09:46 AM
 */
?>

<!-- BEGIN SIDEBAR RIGHT HEADING -->
<div class="sidebar-right-heading">

    <ul class="nav nav-tabs square nav-justified">
        <li class="active btn  btn-block  btn-info"><?php echo lang('label_messages'); ?><i class="fa fa-comments"></i></li>
        </ul>

</div><!-- /.sidebar-right-heading -->
<!-- END SIDEBAR RIGHT HEADING -->



<!-- BEGIN SIDEBAR RIGHT -->
<div class="sidebar-right sidebar-nicescroller">

        <div class="tab-pane fade in active" id="online-user-sidebar">


            <ul class="sidebar-menu online-user" id="chatListCollection" style="padding-left: 5px;">

            </ul>
        </div>


</div><!-- /.sidebar-right -->
<!-- END SIDEBAR RIGHT -->

