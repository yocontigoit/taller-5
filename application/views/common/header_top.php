<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/18
 * Time: 09:45 AM
 */
?>
<!-- BEGIN TOP NAV -->
<div class="top-navbar">
    <div class="top-navbar-inner">

        <!-- Begin Logo brand -->
        <div class="logo-brand">
            <a href="<?php echo site_url('dashboard'); ?>">
                <img src="<?php echo base_url('files/company_logo/'.$this->config->item('company')->company_logo); ?>"
                     alt="<?php echo $this->config->item('company')->company_name; ?>">
            </a>
        </div><!-- /.logo-brand -->
        <!-- End Logo brand -->

        <div class="top-nav-content">

            <!-- Begin button sidebar left toggle -->
            <div class="btn-collapse-sidebar-left">
                <i class="fa fa-bars"></i>
            </div><!-- /.btn-collapse-sidebar-left -->
            <!-- End button sidebar left toggle -->

            <!-- Begin button sidebar right toggle -->
            <div class="btn-collapse-sidebar-right">
                <i class="fa fa-bullhorn"></i>
                <div id="chat-notification" class="chat-notification hide"><!-- hide -->

                </div>
            </div><!-- /.btn-collapse-sidebar-right -->
            <!-- End button sidebar right toggle -->

            <!-- Begin button nav toggle -->
            <div class="btn-collapse-nav" data-toggle="collapse" data-target="#main-fixed-nav">
                <i class="fa fa-plus icon-plus"></i>
            </div><!-- /.btn-collapse-sidebar-right -->
            <!-- End button nav toggle -->
            <!-- Languages -->
            <ul class="nav navbar-nav hidden-xs">
                <li class="dropdown">
                    <a href="#"><i class="icon-th"></i></a>
                </li>
           
            </ul>
            <!-- Begin user session nav -->
            <ul class="nav-user navbar-right">

                <li class="hidden-xs" style="font-size:20px; margin-right: 200px" ><!--class=""hidden-sm hidden-xm -->
                    <p class="navbar-text hide" id="navbar_timer" ><!--  hide -->
                        <span id="project-timer">00:00:00</span>
                        <a href="#" class="navbar-link"  id="navbar_stop_timer" data-toggle="tooltip" title="Stop timer"><span class="fa fa-stop"></span></a>
                    </p>
                </li>

                <li class="dropdown">
                    <a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown">
                        <img  src="<?php echo base_url('files/profile_images/'.$this->CompanyUser_avatar); ?>" class="avatar img-circle" alt="Avatar">
                        Hola, <strong><?php echo $this->CompanyUser_FullName;?></strong>
                    </a>
                    <?php $user_id = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id; ?>
                    <ul class="dropdown-menu square primary margin-list-rounded with-triangle">
                        <li><a href="<?php echo site_url('user/edit/'.($user_id*99)) ?>">Ajustes de cuenta</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo site_url('auth/logout'); ?>">Cerrar sesión</a></li>

                    </ul>
                </li>
            </ul>
            <!-- End user session nav -->

            <!-- Begin Collapse menu nav -->
            <div class="collapse navbar-collapse" id="main-fixed-nav">

                <ul class="nav navbar-nav navbar-left">

                    <!-- Begin nav task -->
                    <li class="dropdown">
                        <?php $client_id = ($this->CompanyUser_id > 0) ? null :$this->CompanyClient_id; ?>
                        <?php $tasks = get_task_history($client_id); ?>
                        <a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="badge badge-warning icon-count"><?php echo (count($tasks));?></span>
                            <i class="fa fa-tasks"></i>
                        </a>
                        <ul class="dropdown-menu square margin-list-rounded with-triangle">
                            <li>
                                <div class="nav-dropdown-heading">
                                    Tasks
                                </div><!-- /.nav-dropdown-heading -->
                                <div class="nav-dropdown-content scroll-nav-dropdown">
                                    <ul>
                                        <?php if(count($tasks)):foreach($tasks as $task) : ?>
                                            <?php

                                            $task_due = duration_diff(date('Y-m-d'),date_from_timestamp($task->task_end/1000));
                                            if($task->task_status == "STATUS_DONE"){
                                                $status = '<i class="fa fa-check-circle-o absolute-left-content icon-task completed"></i>';
                                                $cap = 'Completed';
                                            }elseif($task->task_status == "STATUS_ACTIVE"){
                                                $status = '<i class="fa fa-clock-o absolute-left-content icon-task progress"></i>';
                                                $cap = 'Deadline : '.$task_due.' left';
                                            }else{
                                                $status = '<i class="fa fa-exclamation-circle absolute-left-content icon-task uncompleted"></i>';
                                                $cap = 'Deadline : '.$task_due.' left';
                                            }

                                            ?>

                                            <li><a href="<?php echo base_url('project/tasks/details/'.$task->project_id.'/'.$task->task_id.'/tasks'); ?>">
                                                    <?php echo $status; ?>
                                                    <?php echo $task->task_name; ?>
                                                    <span class="small-caps"><?php echo $cap; ?></span>
                                                </a>
                                            </li>
                                        <?php endforeach;endif;  ?>

                                    </ul>
                                </div><!-- /.nav-dropdown-content scroll-nav-dropdown -->
                              
                            </li>
                        </ul>
                    </li>
                    <!-- End nav task -->
                    <!-- Begin nav message -->
                    <li class="dropdown">
                        <?php $user_id = ($this->CompanyUser_id > 0) ? $this->CompanyUser_id : $this->CompanyClient_user_id; ?>
                        <?php $user_messages = get_unread_messages($user_id,$this->CompanyUser_type); ?>

                        <a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="badge badge-success icon-count"><?php echo (count($user_messages));?></span>
                            <i class="fa fa-envelope"></i>
                        </a>
                        <ul class="dropdown-menu square margin-list-rounded with-triangle">

                            <li>
                                <div class="nav-dropdown-heading">
                                    Messages
                                </div><!-- /.nav-dropdown-heading -->
                                <div class="nav-dropdown-content scroll-nav-dropdown">
                                    <ul>
                                        <?php if(count($user_messages)): foreach($user_messages as $messages):?>
                                            <?php  $user_type = ($messages->user_from) ? 1 : 0;
                                            if ($messages->user_from > 0) {

                                                $user_details = users_details($messages->user_from, $user_type);
                                            } else {

                                                $user_details = users_details($messages->client_from, $user_type);
                                            }
                                            ?>
                                            <li class="unread"><a href="<?php echo site_url('message/read_message/inbox/'.$messages->msg_id); ?>">
                                                    <img src="<?php echo base_url('files/profile_images/'.$user_details->avatar); ?>" class="absolute-left-content img-circle" alt="Avatar">
                                                    <?php echo $messages->message; ?>
                                                    <span class="small-caps"><?php echo ago(strtotime($messages->date_sent)); ?> ago</span>
                                                </a></li>
                                        <?php endforeach; endif; ?>
                                    </ul>
                                </div>
                            </li>

                        </ul>
                    </li>
                    <!-- End nav message -->
                    <!-- Begin nav running projects -->
                    <li class="dropdown">
                        <?php $client_id = ($this->CompanyUser_id > 0) ? null :$this->CompanyClient_id; ?>
                        <?php $run_projects = get_running_projects($client_id); ?>

                        <?php if(count($run_projects)): ?>
                        <a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="badge badge-warning icon-count"><?php echo (count($run_projects));?></span>
                            <i class="fa fa-check-circle-o icon-task"></i>
                        </a>
                        <ul class="dropdown-menu square margin-list-rounded with-triangle">
                            <li>
                                <div class="nav-dropdown-heading">
                                    Running Projects
                                </div><!-- /.nav-dropdown-heading -->
                                <div class="nav-dropdown-content scroll-nav-dropdown">
                                    <ul>
                                        <?php if(count($run_projects)):foreach($run_projects as $project) : ?>
                                            <?php
                                                $status = '<i class="fa fa-clock-o  fa-spin"></i>';
                                                $cap = 'Completed';
                                            ?>

                                            <li><a href="<?php echo base_url('project/overview/preview/'.$project->project_id); ?>">
                                                   <span class="absolute-left-content icon-task progress"> <?php echo $status; ?></span>
                                                    <?php echo $project->project_code; ?>
                                                    <span class="small-caps"><?php echo $cap; ?></span>
                                                </a>
                                            </li>
                                        <?php endforeach;endif;  ?>

                                    </ul>
                                </div><!-- /.nav-dropdown-content scroll-nav-dropdown -->
                               
                            </li>
                        </ul>
                        <?php endif; ?>
                    </li>

                </ul>
            </div><!-- /.navbar-collapse -->
            <!-- End Collapse menu nav -->
        </div><!-- /.top-nav-content -->
    </div><!-- /.top-navbar-inner -->
</div><!-- /.top-navbar -->
<!-- END TOP NAV -->

