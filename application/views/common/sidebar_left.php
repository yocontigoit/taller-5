<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/18
 * Time: 09:45 AM
 */
?>
<!-- BEGIN SIDEBAR LEFT -->
<div class="sidebar-left sidebar-nicescroller">

        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <?php $user_id = ($this->CompanyUser_id) ? $this->CompanyUser_id : $this->CompanyClient_user_id; ?>
            <li class="static left-profile-summary">
                <div class="media">
                    <p class="pull-left">
                        <a href="<?php echo site_url('user/edit/'.($user_id*99)); ?>" >
                        <img src="<?php echo base_url('files/profile_images/'.$this->CompanyUser_avatar); ?>" class="avatar img-circle media-object" alt="Avatar">
                            </a>
                    </p>
                    <div class="media-body">
                        <h4>Hola, <br /><strong><?php echo $this->CompanyUser_FullName; ?></strong></h4>
                        <a class="btn btn-danger btn-xs" href="<?php echo site_url('auth/logout'); ?>">Cerrar sesión</a>
                    </div>
                </div>
            </li>
            <li <?php echo $this->uri->segment(1)=='dashboard'  ? 'class="start active"' : ''; ?>><?php echo anchor('dashboard', '<i class="fa fa-home"></i> <span class="title">'.lang("label_dashboard").'</span>'); ?></li>

            <?php echo get_menu($allowed_modules); ?>

            <?php if (isset($project_id)) :?>
                <?php
                $project_info = get_project_details($project_id);
                $pro_percentage = 0;
                if($project_info->project_use_worklog == 0){
                $pro_percentage = $project_info->project_progress;
                }else{
                $pro_percentage= calculate_project_percentage($project_id);

                }
              ?>
            <li class="footer-widget">
                <div class="progress progress-xs"">
                <div class="progress-bar bg-green-1" role="progressbar" aria-valuenow="<?php echo $pro_percentage; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $pro_percentage; ?>%">
                    <span class="progress-precentage"><?php echo $pro_percentage; ?>%</span>
                </div>
                <a data-toggle="tooltip" title="See task progress" class="btn btn-default md-trigger" data-modal="task-progress"><i class="fa fa-inbox"></i></a>

            </li>
            <?php endif; ?>
        </ul>
        <!-- END SIDEBAR MENU -->


</div><!-- /.sidebar-left -->

<!-- END SIDEBAR LEFT -->

