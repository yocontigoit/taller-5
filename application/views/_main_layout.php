<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/18
 * Time: 09:52 AM
 */
$this->load->view('components/header');
$this->load->view('common/header_top');
$this->load->view('common/sidebar_left');
$this->load->view('common/sidebar_right');

// Now display contents
$this->load->view('common/content_page');
?>
<?php if ($title): ?>
    <!-- Begin page heading -->
    <h1 class="page-heading"><?php echo $title; ?>
        
    </h1>
    <!-- End page heading -->
<?php endif; ?>

<?php
if ($content != null) {
    $this->load->view($content);
}
?>
    <div id="zestpage-loader" style="display: none">
        <div class="loader-content">
            <i class="fa fa-cog fa-spin"></i>
        </div>
    </div>
<?php
$this->load->view('components/footer');
