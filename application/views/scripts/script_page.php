<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/03/06
 * Time: 11:10 AM
 */
?>
<?php if (isset($show_datepicker)) { ?>
    <link href="<?php echo base_url('assets/plugins/datepicker/datepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js'); ?>"></script>
<?php } ?>

<?php if (isset($show_switch)) { ?>

    <script src="<?php echo base_url('public/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>"></script>

<?php } ?>

<?php if (isset($show_dashboard)) { ?>
    <script src="<?php echo base_url('assets/plugins/smartwidgets/jarvis.widget.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/newsticker/jquery.newsTicker.min.js'); ?>"></script>

    <script src="<?php echo base_url('assets/plugins/morris-chart/morris.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>
    <script>
        $(document).ready(function() {

            function formatDate(myDate){
                var m_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                var d = new Date(myDate);

                var curr_month = d.getMonth();
                return (m_names[curr_month]);
            }

            <?php if($this->CompanyUserRolePermission_id == 1) {
            $user_check = null;
        }else{
            $user_check = $this->CompanyClient_id;
        }

            ?>
            Morris.Line({
                element: 'client_sales',
                data: [
                    { m: '<?php echo date('Y')."-01"; ?>', a: <?php echo stats_sales_monthly_data('01',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('01',$user_check,$this->Company_id); ?>, c: <?php echo stats_overdue_monthly_data('01',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-02"; ?>', a: <?php echo stats_sales_monthly_data('02',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('02',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('02',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-03"; ?>', a: <?php echo stats_sales_monthly_data('03',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('03',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('03',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-04"; ?>', a: <?php echo stats_sales_monthly_data('04',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('04',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('04',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-05"; ?>', a: <?php echo stats_sales_monthly_data('05',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('05',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('05',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-06"; ?>', a: <?php echo stats_sales_monthly_data('06',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('06',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('06',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-07"; ?>', a: <?php echo stats_sales_monthly_data('07',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('07',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('07',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-08"; ?>', a: <?php echo stats_sales_monthly_data('08',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('08',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('08',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-09"; ?>', a: <?php echo stats_sales_monthly_data('09',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('09',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('09',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-10"; ?>', a: <?php echo stats_sales_monthly_data('10',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('10',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('10',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-11"; ?>', a: <?php echo stats_sales_monthly_data('11',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('11',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('11',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-12"; ?>', a: <?php echo stats_sales_monthly_data('12',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('12',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('12',$user_check,$this->Company_id); ?>  }
                ],
                xkey: 'm',
                ykeys: ['a', 'b','c'],
                labels: ['Sales', 'Payments','Overdue'],
                xLabelFormat: function(str){
                    return formatDate(str);
                },
                resize: true,
                lineColors: ['#8CC152', '#F6BB42','#4acab4']
            });

            if ($('.widget-tasksticker').length > 0){
                $('.widget-tasksticker').newsTicker({
                    row_height: 41,
                    max_rows: 10,
                    speed: 500,
                    direction: 'up',
                    duration: 3000,
                    autostart: 1,
                    pauseOnHover: 1
                });
            }
            //  $(document).ready(function() {
            /*
             * REFRESH WIDGET
             */



            $("#reset-smart-widget")
                .bind("click", function() {
                    $('#refresh')
                        .click();
                    return false;
                });

            $.enableJarvisWidgets = true;

            if ($.fn.jarvisWidgets && $.enableJarvisWidgets) {

                $('#widget-grid').jarvisWidgets({

                    grid : 'article',
                    widgets : '.jarviswidget',
                    localStorage : true,
                    deleteSettingsKey : '#deletesettingskey-options',
                    settingsKeyLabel : 'Reset settings?',
                    deletePositionKey : '#deletepositionkey-options',
                    positionKeyLabel : 'Reset position?',
                    sortable : true,
                    buttonsHidden : false,
                    // toggle button
                    toggleButton : true,
                    toggleClass : 'fa fa-minus | fa fa-plus',
                    toggleSpeed : 200,
                    onToggle : function() {
                    },
                    // delete btn
                    deleteButton : true,
                    deleteClass : 'fa fa-times',
                    deleteSpeed : 200,
                    onDelete : function() {
                    },
                    // edit btn
                    editButton : true,
                    editPlaceholder : '.jarviswidget-editbox',
                    editClass : 'fa fa-cog | fa fa-save',
                    editSpeed : 200,
                    onEdit : function() {
                    },
                    // color button
                    colorButton : true,
                    // full screen
                    fullscreenButton : true,
                    fullscreenClass : 'fa fa fa-expand | fa fa fa-compress',
                    fullscreenDiff : 3,
                    onFullscreen : function() {
                    },
                    // custom btn
                    customButton : false,
                    customClass : 'folder-10 | next-10',
                    customStart : function() {
                        alert('Hello you, this is a custom button...')
                    },
                    customEnd : function() {
                        alert('bye, till next time...')
                    },
                    // order
                    buttonOrder : '%refresh% %custom% %edit% %toggle% %fullscreen% %delete%',
                    opacity : 1.0,
                    dragHandle : '> header',
                    placeholderClass : 'jarviswidget-placeholder',
                    indicator : true,
                    indicatorTime : 600,
                    ajax : true,
                    timestampPlaceholder : '.jarviswidget-timestamp',
                    timestampFormat : 'Last update: %m%/%d%/%y% %h%:%i%:%s%',
                    refreshButton : true,
                    refreshButtonClass : 'fa fa-refresh',
                    labelError : 'Sorry but there was a error:',
                    labelUpdated : 'Last Update:',
                    labelRefresh : 'Refresh',
                    labelDelete : 'Delete widget:',
                    afterLoad : function() {
                    },
                    rtl : false, // best not to toggle this!
                    onChange : function() {

                    },
                    onSave : function() {

                    },
                    ajaxnav : $.navAsAjax // declears how the localstorage should be saved

                });

            }

            //****************************start ********************************************/

            $('.todo .checkbox > input[type="checkbox"]').click(function() {
                //$('.todo .checkbox >  input:checked').click(function() {
                var $this = $(this).parent().parent().parent();

                if ($(this).prop('checked')) {
                    $this.addClass("complete");
                    var selected = "";
                    //oSortable = $('.dd-list').nestedSortable('toArray');
                    var selected = get_selected_values();
                    saveChanges(selected);
                    // remove this if you want to undo a check list once checked
                    //$(this).attr("disabled", true);
                    $(this).parent().hide();

                    // once clicked - add class, copy to memory then remove and add to sortable3
                    $this.slideUp(500, function() {
                        $this.clone().prependTo("#sortable3").effect("highlight", {}, 800);
                        $this.remove();
                        countTasks();
                    });
                } else {
                    // insert undo code here...
                }

            });
            // count tasks
            function countTasks() {

                $('.todo-group-title').each(function() {
                    var $this = $(this);
                    $this.find(".num-of-tasks").text($this.next().find("li").size());
                });

            }

            function get_selected_values()
            {
                var selected_values = [];
                $('.todo .checkbox >  input:checked').each(function()
                {
                    selected_values.push($(this).val());
                });
                //alert("values"+selected_values)
                return selected_values;
            }

            function saveChanges(selected) {
                var that = this;
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('project/tasks/mark_complete'); ?>",
                    dataType: "json",
                    data: {selectedtask: selected.join('~')},
                    cache: true, // (warning: this will cause a timestamp and will call the request twice)
                    success: function(data) {
                        that.reset();
                    }

                });
            }
            $('.paid-chart-1').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#79E846',
                lineWidth: 4,
                size:60,



                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

            $('.unpaid-chart-2').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F6BA48',
                lineWidth: 4,
                size:60,



                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

            $('.overdue-chart-3').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F57D7D',
                lineWidth: 4,
                size:60,


                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });


            $('.expense-chart-3').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#5FB9F5',
                lineWidth: 4,
                size:60,


                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

            //***************************END***********************************************/

            //****************************start pendiente********************************************/

            $('.pendiente .checkbox > input[type="checkbox"]').click(function() {
                //$('.todo .checkbox >  input:checked').click(function() {
                var $this = $(this).parent().parent().parent();

                if ($(this).prop('checked')) {
                    $this.addClass("complete");
                    var selected = "";
                    //oSortable = $('.dd-list').nestedSortable('toArray');
                    var selected = get_selected_values();
                    saveChanges(selected);
                    // remove this if you want to undo a check list once checked
                    //$(this).attr("disabled", true);
                    $(this).parent().hide();

                    // once clicked - add class, copy to memory then remove and add to sortable3
                    $this.slideUp(500, function() {
                        $this.clone().prependTo("#sortable_pendiente").effect("highlight", {}, 800);
                        $this.remove();
                        countTasks();
                    });
                } else {
                    // insert undo code here...
                }

            });
            // count tasks
            function countTasks() {

                $('."pendiente-group-title').each(function() {
                    var $this = $(this);
                    $this.find(".num-of-tasks").text($this.next().find("li").size());
                });

            }

            function get_selected_values()
            {
                var selected_values = [];
                $('.pendiente .checkbox >  input:checked').each(function()
                {
                    selected_values.push($(this).val());
                });
                //alert("values"+selected_values)
                return selected_values;
            }

            function saveChanges(selected) {
                var that = this;
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('project/tasks/update_selected_pendiente'); ?>",
                    dataType: "json",
                    data: {selectedtask: selected.join('~')},
                    cache: true, // (warning: this will cause a timestamp and will call the request twice)
                    success: function(data) {
                        that.reset();
                    }

                });
            }
            $('.paid-chart-1').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#79E846',
                lineWidth: 4,
                size:60,



                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

            $('.unpaid-chart-2').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F6BA48',
                lineWidth: 4,
                size:60,



                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

            $('.overdue-chart-3').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F57D7D',
                lineWidth: 4,
                size:60,


                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });


            $('.expense-chart-3').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#5FB9F5',
                lineWidth: 4,
                size:60,


                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

            //***************************END pendiente***********************************************/
        });


    </script>

<?php } //End dashboard  ?>

<?php if (isset($show_admin_dashboard_stats)) { ?>

    <script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/morris.min.js'); ?>"></script>

    <!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
    <script src="<?php echo base_url('assets/plugins/flot-chart/jquery.flot.cust.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/flot-chart/jquery.flot.resize.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/flot-chart/jquery.flot.tooltip.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/highcharts/highcharts.js'); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
                return {
                    radialGradient: {
                        cx: 0.5,
                        cy: 0.3,
                        r: 0.7
                    },
                    stops: [
                        [0, color],
                        [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                    ]
                };
            });
            var graphStyle =  Morris.Donut({
                element: 'admin-tickets-stat',
                data: <?php echo json_encode(client_ticket_stats(null));?>,
                colors: [ '#F6BB42', '#8CC152', '#E9573F','#4acab4'],
                resize: true,
                parseTime:false
            });

            var graphtrans =  Morris.Donut({
                element: 'admin-rev-stat',
                data: <?php echo json_encode(admin_rev_stats(null));?>,
                colors: [ '#F6BB42', '#8CC152', '#E9573F'],
                resize: true,
                parseTime:false
            });


            $('ul.nav a').on('shown.bs.tab', function (e) {
                graphStyle.redraw();
                graphtrans.redraw();
            });

            $('#salestimate_stats').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: true,
                    type: 'pie'
                },

                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y:.1f}</b>'

                },
                credits: {
                    enabled: false
                },
                title:{
                    text:''
                },
                exporting: { enabled: false },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,

                        depth: 35,

                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'blue'
                            },
                            connectorColor: 'silver'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    data: <?php echo json_encode(estimates_invoice()); ?>
                }]
            });


            $('.admin-pro-stat').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F6BA48',
                lineWidth: 20,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });
            <?php if($this->CompanyUserRolePermission_id == 1) {
            $user_check = null;
        }else{
            $user_check = $this->CompanyClient_id;
        }

            ?>

            $(function() {

                var income = [[<?php echo date_to_timestamp(date('Y')."-01-01")*1000; ?>, <?php echo stats_income_monthly_data('01',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-02-01")*1000; ?>, <?php echo stats_income_monthly_data('02',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-03-01")*1000; ?>, <?php echo stats_income_monthly_data('03',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-04-01")*1000; ?>, <?php echo stats_income_monthly_data('04',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-05-01")*1000; ?>, <?php echo stats_income_monthly_data('05',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-06-01")*1000; ?>, <?php echo stats_income_monthly_data('06',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-07-01")*1000; ?>, <?php echo stats_income_monthly_data('07',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-08-01")*1000; ?>, <?php echo stats_income_monthly_data('08',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-09-01")*1000; ?>, <?php echo stats_income_monthly_data('09',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-10-01")*1000; ?>, <?php echo stats_income_monthly_data('10',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-11-01")*1000; ?>, <?php echo stats_income_monthly_data('11',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-12-01")*1000; ?>, <?php echo stats_income_monthly_data('12',$user_check,$this->Company_id); ?>]
                    ],

                    expense = [[<?php echo date_to_timestamp(date('Y')."-01-01")*1000; ?>, <?php echo expense_monthly_data('01',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-02-01")*1000; ?>, <?php echo expense_monthly_data('02',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-03-01")*1000; ?>, <?php echo expense_monthly_data('03',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-04-01")*1000; ?>, <?php echo expense_monthly_data('04',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-05-01")*1000; ?>, <?php echo expense_monthly_data('05',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-06-01")*1000; ?>, <?php echo expense_monthly_data('06',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-07-01")*1000; ?>, <?php echo expense_monthly_data('07',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-08-01")*1000; ?>, <?php echo expense_monthly_data('08',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-09-01")*1000; ?>, <?php echo expense_monthly_data('09',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-10-01")*1000; ?>, <?php echo expense_monthly_data('10',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-11-01")*1000; ?>, <?php echo expense_monthly_data('11',$user_check,$this->Company_id); ?>],
                        [<?php echo date_to_timestamp(date('Y')."-12-01")*1000; ?>, <?php echo expense_monthly_data('12',$user_check,$this->Company_id); ?>]],

                    toggles = $("#rev-toggles"),
                    target = $("#flotcontainer");

                var data = [{
                    label : "Income",
                    data : income,
                    bars : {
                        show : true,
                        align : "center",
                        barWidth : 30 * 30 * 60 * 1000 * 80
                    }
                }, {
                    label : "Expense",
                    data : expense,
                    color : '#3276B1',
                    lines : {
                        show : true,
                        lineWidth : 3
                    },
                    points : {
                        show : true
                    }
                }
                ];

                var options = {
                    grid : {
                        hoverable : true
                    },
                    tooltip : true,
                    tooltipOpts : {
                        //content: '%x - %y',
                        //dateFormat: '%b %y',
                        defaultTheme : false
                    },
                    xaxis : {
                        mode : "time"
                    },
                    yaxes : {
                        tickFormatter : function(val, axis) {
                            return "$" + val;
                        },
                        max : 1200
                    }

                };

                plot2 = null;

                function plotNow() {
                    var d = [];
                    toggles.find(':checkbox').each(function() {
                        if ($(this).is(':checked')) {
                            d.push(data[$(this).attr("name").substr(4, 1)]);
                        }
                    });
                    if (d.length > 0) {
                        if (plot2) {
                            plot2.setData(d);
                            plot2.draw();
                        } else {
                            plot2 = $.plot(target, d, options);
                        }
                    }

                }
                toggles.find(':checkbox').on('change', function() {
                    plotNow();
                });
                plotNow()

            });

        });
    </script>
<?php  } ?>


<?php
if (isset($use_stripe)) { ?>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        Stripe.setPublishableKey('<?php echo $this->config->item('stripe')->stripe_public_key; ?>');
    </script>
<?php } ?>

<?php if(isset($gantt_show)){ ?>
    <script src="<?php echo base_url('assets/plugins/smartwidgets/jarvis.widget.js'); ?>"></script>
    <script>
        $(document).ready(function() {
            $.enableJarvisWidgets = true;

            if ($.fn.jarvisWidgets && $.enableJarvisWidgets) {

                $('#widget-grid').jarvisWidgets({

                    grid: 'article',
                    widgets: '.jarviswidget',
                    localStorage: true,
                    deleteSettingsKey: '#deletesettingskey-options',
                    settingsKeyLabel: 'Reset settings?',
                    deletePositionKey: '#deletepositionkey-options',
                    positionKeyLabel: 'Reset position?',
                    sortable: true,
                    buttonsHidden: false,
                    // toggle button
                    toggleButton: true,
                    toggleClass: 'fa fa-minus | fa fa-plus',
                    toggleSpeed: 200,
                    onToggle: function () {
                    },
                    // delete btn
                    deleteButton: true,
                    deleteClass: 'fa fa-times',
                    deleteSpeed: 200,
                    onDelete: function () {
                    },
                    // edit btn
                    editButton: true,
                    editPlaceholder: '.jarviswidget-editbox',
                    editClass: 'fa fa-cog | fa fa-save',
                    editSpeed: 200,
                    onEdit: function () {
                    },
                    // color button
                    colorButton: true,
                    // full screen
                    fullscreenButton: true,
                    fullscreenClass: 'fa fa fa-expand | fa fa fa-compress',
                    fullscreenDiff: 3,
                    onFullscreen: function () {
                    },
                    // custom btn
                    customButton: false,
                    customClass: 'folder-10 | next-10',
                    customStart: function () {
                        alert('Hello you, this is a custom button...')
                    },
                    customEnd: function () {
                        alert('bye, till next time...')
                    },
                    // order
                    buttonOrder: '%refresh% %custom% %edit% %toggle% %fullscreen% %delete%',
                    opacity: 1.0,
                    dragHandle: '> header',
                    placeholderClass: 'jarviswidget-placeholder',
                    indicator: true,
                    indicatorTime: 600,
                    ajax: true,
                    timestampPlaceholder: '.jarviswidget-timestamp',
                    timestampFormat: 'Last update: %m%/%d%/%y% %h%:%i%:%s%',
                    refreshButton: true,
                    refreshButtonClass: 'fa fa-refresh',
                    labelError: 'Sorry but there was a error:',
                    labelUpdated: 'Last Update:',
                    labelRefresh: 'Refresh',
                    labelDelete: 'Delete widget:',
                    afterLoad: function () {
                    },
                    rtl: false, // best not to toggle this!
                    onChange: function () {

                    },
                    onSave: function () {

                    },
                    ajaxnav: $.navAsAjax // declears how the localstorage should be saved

                });

            }
        });
    </script>
<?php } //End gantt ?>

<?php  if(isset($show_client_user_stats)){ ?>
    <script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/morris.min.js'); ?>"></script>
    <?php $select_user = ($this->uri->segment(3)/99); ?>
    <script type="text/javascript">
        $(document).ready(function() {

            Morris.Donut({
                element: 'client-user-stats',
                data: <?php echo json_encode(client_user_stats($select_user)); ?>,
                colors: [ '#F6BB42', '#8CC152'],
                resize: true,
                parseTime:false
            });
        });
    </script>
<?php  } ?>


<?php if (isset($show_client_dashboard_stats)) { ?>

    <script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/morris.min.js'); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            Morris.Donut({
                element: 'client-tickets-stat',
                data: <?php echo json_encode(client_ticket_stats($this->CompanyClient_id));?>,

                colors: [ '#F6BB42', '#8CC152', '#E9573F','#A3E135'],
                resize: true,
                parseTime:false
            });

            $('.client-pro-stat').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F6BA48',
                lineWidth: 20,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });


        });
    </script>
<?php  } ?>

<?php if (isset($show_milestone_progress)) { ?>
    <script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('.milestone-progress').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#A3E135',
                lineWidth: 10,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });
        });
    </script>

<?php  } ?>

<?php if (isset($show_project_overview)) { ?>
    <script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/morris.min.js'); ?>"></script>

    <script src="<?php echo base_url('assets/plugins/jquery-knob/jquery.knob.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/jquery-knob/knob.js'); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            Morris.Donut({
                element: 'morris-stats',
                data: <?php echo json_encode(project_stats($project_id));?>,

                colors: [ '#F6BB42', '#8CC152', '#E9573F']
            });

            $('.easy-pie-chart-1').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F6BA48',
                lineWidth: 10,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });
            $('.easy-pie-chart-4').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F6BA48',
                lineWidth: 10,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

            $('.easy-pie-chart-4').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F6BA48',
                lineWidth: 10,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });
            $('.easy-pie-tasks').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#A3E135',
                lineWidth: 10,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });
            $('.easy-pie-budget').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#3F5A58',
                lineWidth: 10,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });
            $('.easy-pie-issues').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#490CC3',
                lineWidth: 10,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

        });
    </script>
<?php } ?>

<?php if (isset($show_expenses_details)) { ?>
    <script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('.easy-pie-expense').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#490CC3',
                lineWidth: 30,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

        });
    </script>


<?php } ?>

<?php if (isset($show_expenses_overview)) { ?>
    <script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/morris.min.js'); ?>"></script>



    <script src="<?php echo base_url('assets/plugins/highcharts/highcharts.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/highcharts/highcharts-3d.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/highcharts/modules/exporting.js'); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            Morris.Donut({
                element: 'expenses-stats',
                data: <?php echo json_encode(expense_stats());?>,

                colors: [ '#F6BB42', '#8CC152', '#E9573F'],
                resize: true,
                parseTime:false
            });

            $('#category-stats').highcharts({
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                },

                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y:.1f}</b>'

                },
                credits: {
                    enabled: false
                },
                title:{
                    text:''
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 35
                    }
                },
                series: [{
                    type: 'pie',
                    data: <?php echo json_encode(expense_category_stats());?>
                }]
            });

            function formatDate(myDate){
                var m_names = ["En", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

                var d = new Date(myDate);

                var curr_month = d.getMonth();
                return (m_names[curr_month]);
            }

            <?php if($this->CompanyUserRolePermission_id == 1) {
            $user_check = null;
        }else{
            $user_check = $this->CompanyClient_id;
        }

            ?>
            Morris.Line({
                element: 'expense_graph',
                data: [
                    { m: '<?php echo date('Y')."-01"; ?>', a: <?php echo expense_monthly_data('01',$user_check,$this->Company_id); ?> },
                    { m: '<?php echo date('Y')."-02"; ?>', a: <?php echo expense_monthly_data('02',$user_check,$this->Company_id); ?> },
                    { m: '<?php echo date('Y')."-03"; ?>', a: <?php echo expense_monthly_data('03',$user_check,$this->Company_id); ?> },
                    { m: '<?php echo date('Y')."-04"; ?>', a: <?php echo expense_monthly_data('04',$user_check,$this->Company_id); ?> },
                    { m: '<?php echo date('Y')."-05"; ?>', a: <?php echo expense_monthly_data('05',$user_check,$this->Company_id); ?> },
                    { m: '<?php echo date('Y')."-06"; ?>', a: <?php echo expense_monthly_data('06',$user_check,$this->Company_id); ?> },
                    { m: '<?php echo date('Y')."-07"; ?>', a: <?php echo expense_monthly_data('07',$user_check,$this->Company_id); ?> },
                    { m: '<?php echo date('Y')."-08"; ?>', a: <?php echo expense_monthly_data('08',$user_check,$this->Company_id); ?> },
                    { m: '<?php echo date('Y')."-09"; ?>', a: <?php echo expense_monthly_data('09',$user_check,$this->Company_id); ?> },
                    { m: '<?php echo date('Y')."-10"; ?>', a: <?php echo expense_monthly_data('10',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-11"; ?>', a: <?php echo expense_monthly_data('11',$user_check,$this->Company_id); ?> },
                    { m: '<?php echo date('Y')."-12"; ?>', a: <?php echo expense_monthly_data('12',$user_check,$this->Company_id); ?> }
                ],
                xkey: 'm',
                ykeys: ['a'],
                labels: ['Expenses'],
                xLabelFormat: function(str){
                    return formatDate(str);
                },
                resize: true,
                lineColors: ['#8CC152']
            });



        });
    </script>
<?php } ?>
<?php if (isset($payments_stats)) { ?>
    <script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/morris.min.js'); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            Morris.Donut({
                element: 'payments-stats',
                data: <?php echo json_encode(payments_stats());?>,

                colors: [ '#F6BB42', '#8CC152', '#E9573F'],
                resize: true,
                parseTime:false
            });
        });
    </script>
<?php } ?>
<?php if(isset($show_dashboard_report)){ ?>
    <script src="<?php echo base_url('assets/plugins/morris-chart/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/morris.min.js'); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            function formatDate(myDate){
                var m_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                var d = new Date(myDate);

                var curr_month = d.getMonth();
                return (m_names[curr_month]);
            }

            <?php if($this->CompanyUserRolePermission_id == 1) {
            $user_check = null;
        }else{
            $user_check = $this->CompanyClient_id;
        }

            ?>
            if ($('#dashboard-report').length > 0) {
                Morris.Line({
                    element: 'dashboard-report',
                    data: [
                        { m: '<?php echo date('Y')."-01"; ?>', a: <?php echo stats_income_monthly_data('01',$user_check,$this->Company_id); ?> },
                        { m: '<?php echo date('Y')."-02"; ?>', a: <?php echo stats_income_monthly_data('02',$user_check,$this->Company_id); ?> },
                        { m: '<?php echo date('Y')."-03"; ?>', a: <?php echo stats_income_monthly_data('03',$user_check,$this->Company_id); ?> },
                        { m: '<?php echo date('Y')."-04"; ?>', a: <?php echo stats_income_monthly_data('04',$user_check,$this->Company_id); ?> },
                        { m: '<?php echo date('Y')."-05"; ?>', a: <?php echo stats_income_monthly_data('05',$user_check,$this->Company_id); ?> },
                        { m: '<?php echo date('Y')."-06"; ?>', a: <?php echo stats_income_monthly_data('06',$user_check,$this->Company_id); ?> },
                        { m: '<?php echo date('Y')."-07"; ?>', a: <?php echo stats_income_monthly_data('07',$user_check,$this->Company_id); ?> },
                        { m: '<?php echo date('Y')."-08"; ?>', a: <?php echo stats_income_monthly_data('08',$user_check,$this->Company_id); ?> },
                        { m: '<?php echo date('Y')."-09"; ?>', a: <?php echo stats_income_monthly_data('09',$user_check,$this->Company_id); ?> },
                        { m: '<?php echo date('Y')."-10"; ?>', a: <?php echo stats_income_monthly_data('10',$user_check,$this->Company_id); ?>  },
                        { m: '<?php echo date('Y')."-11"; ?>', a: <?php echo stats_income_monthly_data('11',$user_check,$this->Company_id); ?> },
                        { m: '<?php echo date('Y')."-12"; ?>', a: <?php echo stats_income_monthly_data('12',$user_check,$this->Company_id); ?> }
                    ],
                    xkey: 'm',
                    ykeys: ['a'],
                    labels: ['Earnings'],
                    xLabelFormat: function(str){
                        return formatDate(str);
                    },
                    resize: true,
                    lineColors: ['#1F91BD'],
                    pointFillColors: ['#fff'],
                    pointStrokeColors: ['#3EAFDB'],
                    gridTextColor: ['#fff'],
                    pointSize: 3,
                    grid: false
                });
            }
        });
    </script>


<?php } ?>

<?php if(isset($show_client_report)) { ?>
    <script type="text/javascript">
        jQuery(document).ready(function() {

            $('body').on('change', '#report_client_id', function (e) {
                e.preventDefault();


                var client_id = $('select[name="client"]').val();
                if (client_id == 'Empty') {
                    return;
                }
                $.post('<?php echo site_url('report/customer_report_ajax'); ?>', {client_id: client_id}, function (data) {
                    $('.client_report').html(data);

                });
            });
        });
    </script>
<?php } ?>

<?php if (isset($show_team_overview)) { ?>
    <script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/raphael.min.js'); ?>"></script>


    <script type="text/javascript">
        $(document).ready(function() {

            $('.easy-pie-chart-1').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F6BA48',
                lineWidth: 10,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });
            $('.easy-pie-chart-4').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F6BA48',
                lineWidth: 10,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

        });
    </script>
<?php } ?>

<?php if (isset($show_user_data)) { ?>
    <script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>


    <script type="text/javascript">
        $(document).ready(function() {

            $('.easy-pie-chart-1').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F6BA48',
                lineWidth: 10,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });
            $('.easy-pie-chart-4').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F6BA48',
                lineWidth: 10,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

        });
    </script>
<?php } ?>

<?php if (isset($show_document_preview)) { ?>
    <link href="<?php echo base_url('assets/plugins/magnific-popup/magnific-popup.min.css'); ?>" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url('assets/plugins/magnific-popup/jquery.magnific-popup.min.js'); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            /** BEGIN MAGNIFIC POPUP **/
            if ($('.magnific-popup-wrap').length > 0){
                $('.magnific-popup-wrap').each(function() {
                    "use strict";
                    $(this).magnificPopup({
                        delegate: 'a.zooming',
                        type: 'image',
                        removalDelay: 300,
                        mainClass: 'mfp-fade',
                        gallery: {
                            enabled:true
                        }
                    });
                });
            }

            if ($('.inline-popups').length > 0){
                $('.inline-popups').magnificPopup({
                    delegate: 'a',
                    removalDelay: 500,
                    callbacks: {
                        beforeOpen: function() {
                            this.st.mainClass = this.st.el.attr('data-effect');
                        }
                    },
                    midClick: true
                });
            }
            $('.magnific-img').magnificPopup({
                type:'image',
                removalDelay: 300,
                mainClass: 'mfp-fade'
            });
            /** END MAGNIFIC POPUP **/
        });
    </script>
<?php } ?>

<?php if (isset($show_billing_overview)) { ?>

    <script src="<?php echo base_url('assets/plugins/morris-chart/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/morris.min.js'); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            function formatDate(myDate){
                var m_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                var d = new Date(myDate);

                var curr_month = d.getMonth();
                return (m_names[curr_month]);
            }

            <?php if($this->CompanyUserRolePermission_id == 1) {
            $user_check = null;
        }else{
            $user_check = $this->CompanyClient_id;
        }

            ?>
            Morris.Line({
                element: 'overview_graph',
                data: [
                    { m: '<?php echo date('Y')."-01"; ?>', a: <?php echo stats_sales_monthly_data('01',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('01',$user_check,$this->Company_id); ?>, c: <?php echo stats_overdue_monthly_data('01',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-02"; ?>', a: <?php echo stats_sales_monthly_data('02',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('02',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('02',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-03"; ?>', a: <?php echo stats_sales_monthly_data('03',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('03',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('03',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-04"; ?>', a: <?php echo stats_sales_monthly_data('04',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('04',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('04',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-05"; ?>', a: <?php echo stats_sales_monthly_data('05',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('05',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('05',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-06"; ?>', a: <?php echo stats_sales_monthly_data('06',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('06',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('06',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-07"; ?>', a: <?php echo stats_sales_monthly_data('07',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('07',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('07',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-08"; ?>', a: <?php echo stats_sales_monthly_data('08',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('08',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('08',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-09"; ?>', a: <?php echo stats_sales_monthly_data('09',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('09',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('09',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-10"; ?>', a: <?php echo stats_sales_monthly_data('10',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('10',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('10',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-11"; ?>', a: <?php echo stats_sales_monthly_data('11',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('11',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('11',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-12"; ?>', a: <?php echo stats_sales_monthly_data('12',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('12',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('12',$user_check,$this->Company_id); ?>  }
                ],
                xkey: 'm',
                ykeys: ['a', 'b','c'],
                labels: ['Sales', 'Payments','Overdue'],
                xLabelFormat: function(str){
                    return formatDate(str);
                },
                resize: true,
                lineColors: ['#8CC152', '#F6BB42','#4acab4']
            });

        });
    </script>
<?php } ?>


<?php if (isset($gantt_show)) { ?>

    <?php foreach (get_gantt_css_files() as $js_file) { ?>
        <link href="<?php echo base_url($js_file['path']); ?>" rel="stylesheet" type="text/css" />
    <?php } ?>

    <?php foreach (get_gantt_js_files() as $js_file) { ?>
        <script src="<?php echo base_url($js_file['path']); ?>" type="text/javascript" language="javascript"
                charset="UTF-8"></script>
    <?php } ?>


    <script type="text/javascript">

        var ge;  //this is the hugly but very friendly global var for the gantt editor
        $(function() {
            //load templates
            $("#ganttemplates").loadTemplates();
            ge = new GanttMaster();

            var workSpace = $("#workSpace");

            ge.init(workSpace);
            $(".ganttButtonBar div").append("<button onclick='clearGantt();' class='button'>clear</button>")
                .append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
                .append("<button onclick='getFile();' class='button'>export</button>");
            //$(".ganttButtonBar h1").html("<a href='http://twproject.com' title='Twproject the friendly project and work management tool' target='_blank'><img width='80%' src='res/twBanner.jpg'></a>");
            $(".ganttButtonBar div").addClass('buttons');
            loadI18n(); //overwrite with localized ones

            ge.splitter.perc = $(window).width() > 1280 ? 50 : 20;
            ge.splitter.resize();

            delete ge.gantt.zoom;


            //inject some buttons (for this demo only)

            //overwrite with localized ones
            // loadI18n();

            //simulate a data load from a server.
            loadGanttFromServer();


            //fill default Teamwork roles if any
            if (!ge.roles || ge.roles.length == 0) {
                setRoles();
            }

            //fill default Resources roles if any
            if (!ge.resources || ge.resources.length == 0) {
                setResource();
            }


            //$('#workSpace').bind('resize', function() {
            //    console.log('resized');
            //});
            $(".page-content").resize(function () {
                workSpace.css({width: "100%", height: $(window).height() - workSpace.position().top});
                workSpace.trigger("resize.gantt");
            }).oneTime(2, "resize", function () {$(this).trigger("resize")});



        });



        function loadGanttFromServer(taskId, callback) {

            var prof = new Profiler("loadServerSide");
            prof.reset();
            //server/server.php?action=load
            //project/project/show/'.$id
            // alert("loading..................."+response);
            $.getJSON("<?php echo base_url('project/gantt/show/' . $project_id) ?>", {}, function(response) {

                //alert("loading..................."+response);
                //console.log(response);
                if (response.ok) {
                    //ge.redraw();
                    prof.stop();

                    ge.loadProject(response.data);
                    ge.checkpoint(); //empty the undo stack

                    // check edit or view mode
                    // hide buttons in view mode
                    if (response.mode != 'edit' && !$(".ganttButtonBar .buttons").attr('data-readmode')) {
                        // switch to read mode
                        $(".ganttButtonBar .buttons")
                            .attr("data-readmode", "true")
                            .empty()
                            .append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
                            .append("You are viewing mode")
                            .append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                        //.append("<button onclick='getFile();' class='button'  data-readmode=\"true\">export</button>")
                        //.append('<button onclick="window.location.href=\'server/signout.php\'" class="button readmode">sign out</button>');
                    }

                    if (typeof (callback) == "function") {
                        callback(response);
                    }
                } else {
                    //jsonErrorHandling(response);

                    var errMsg = "Errors loading project\n\n";
                    if (response.message) {
                        errMsg = errMsg + response.message + "\n";
                    }

                    if (response.errorMessages.length) {
                        errMsg += response.errorMessages.join("\n");
                    }

                    alert(errMsg);

                }

            });
        }


        function saveGanttOnServer() {
            if (!ge.canWrite)
                return;

            var prj = ge.saveProject();
            var prof = new Profiler("saveServerSide");
            prof.reset();

            $.ajax("<?php echo base_url('project/gantt/save_project'); ?>", {
                dataType: "json",
                data: {prj: JSON.stringify(prj)},
                type: "POST",
                success: function(response) {
                    if (response.ok) {
                        prof.stop();
                        if (response.project) {
                            ge.loadProject(response.project); //must reload as "tmp_" ids are now the good ones
                        } else {
                            ge.reset();
                        }
                    } else {
                        var errMsg = "Errors saving project\n";
                        if (response.message) {
                            errMsg = errMsg + response.message + "\n";
                        }

                        if (response.errorMessages.length) {
                            errMsg += response.errorMessages.join("\n");
                        }

                        alert(errMsg);
                    }
                }

            });


            //$.ajax("ganttAjaxController.jsp", {
            $.ajax("<?php echo base_url('project/project/save') ?>", {
                dataType: "json",
                data: {data: JSON.stringify(prj)},
                // data: {CM:"SVPROJECT",prj:JSON.stringify(prj)},
                type: "POST",
                success: function(response) {
                    if (response.ok) {
                        prof.stop();
                        if (response.project) {
                            ge.loadProject(response.project); //must reload as "tmp_" ids are now the good ones
                        } else {
                            ge.reset();
                        }
                    } else {
                        var errMsg = "Errors saving project\n";
                        if (response.message) {
                            errMsg = errMsg + response.message + "\n";
                        }

                        if (response.errorMessages.length) {
                            errMsg += response.errorMessages.join("\n");
                        }

                        alert(errMsg);
                    }
                }

            });
        }

        //-------------------------------------------  Create some demo data ------------------------------------------------------
        function setRoles() {
            ge.roles = [
                {
                    id: "tmp_1",
                    name: "Project Manager"
                },
                {
                    id: "tmp_2",
                    name: "Worker"
                },
                {
                    id: "tmp_3",
                    name: "Stakeholder/Customer"
                }
            ];
        }

        function setResource() {
            var res = [];
            for (var i = 1; i <= 10; i++) {
                res.push({id: "tmp_" + i, name: "Resource " + i});
            }
            ge.resources = res;
        }


        function editResources() {

        }

        function clearGantt() {
            ge.reset();
        }

        function loadI18n() {
            GanttMaster.messages = {
                "CANNOT_WRITE": "CANNOT_WRITE",
                "CHANGE_OUT_OF_SCOPE": "NO_RIGHTS_FOR_UPDATE_PARENTS_OUT_OF_EDITOR_SCOPE",
                "START_IS_MILESTONE": "START_IS_MILESTONE",
                "END_IS_MILESTONE": "END_IS_MILESTONE",
                "TASK_HAS_CONSTRAINTS": "TASK_HAS_CONSTRAINTS",
                "GANTT_ERROR_DEPENDS_ON_OPEN_TASK": "GANTT_ERROR_DEPENDS_ON_OPEN_TASK",
                "GANTT_ERROR_DESCENDANT_OF_CLOSED_TASK": "GANTT_ERROR_DESCENDANT_OF_CLOSED_TASK",
                "TASK_HAS_EXTERNAL_DEPS": "TASK_HAS_EXTERNAL_DEPS",
                "GANTT_ERROR_LOADING_DATA_TASK_REMOVED": "GANTT_ERROR_LOADING_DATA_TASK_REMOVED",
                "ERROR_SETTING_DATES": "ERROR_SETTING_DATES",
                "CIRCULAR_REFERENCE": "CIRCULAR_REFERENCE",
                "CANNOT_DEPENDS_ON_ANCESTORS": "CANNOT_DEPENDS_ON_ANCESTORS",
                "CANNOT_DEPENDS_ON_DESCENDANTS": "CANNOT_DEPENDS_ON_DESCENDANTS",
                "INVALID_DATE_FORMAT": "INVALID_DATE_FORMAT",
                "TASK_MOVE_INCONSISTENT_LEVEL": "TASK_MOVE_INCONSISTENT_LEVEL",
                "GANTT_QUARTER_SHORT": "trim.",
                "GANTT_SEMESTER_SHORT": "sem."
            };
        }



        //-------------------------------------------  Get project file as JSON (used for migrate project from gantt to Teamwork) ------------------------------------------------------
        function getFile() {
            $("#gimBaPrj").val(JSON.stringify(ge.saveProject()));
            $("#gimmeBack").submit();
            $("#gimBaPrj").val("");

            /*  var uriContent = "data:text/html;charset=utf-8," + encodeURIComponent(JSON.stringify(prj));
             neww=window.open(uriContent,"dl");*/
        }


        //-------------------------------------------  LOCAL STORAGE MANAGEMENT (for this demo only) ------------------------------------------------------
        Storage.prototype.setObject = function(key, value) {
            this.setItem(key, JSON.stringify(value));
        };


        Storage.prototype.getObject = function(key) {
            return this.getItem(key) && JSON.parse(this.getItem(key));
        };


        /*function saveInLocalStorage() {
         var prj = ge.saveProject();
         if (localStorage) {
         localStorage.setObject("teamworkGantDemo", prj);
         } else {
         $("#ta").val(JSON.stringify(prj));
         }
         }*/


        //-------------------------------------------  Open a black popup for managing resources. This is only an axample of implementation (usually resources come from server) ------------------------------------------------------

        function editResources() {

            //make resource editor
            var resourceEditor = $.JST.createFromTemplate({}, "RESOURCE_EDITOR");
            var resTbl = resourceEditor.find("#resourcesTable");

            for (var i = 0; i < ge.resources.length; i++) {
                var res = ge.resources[i];
                resTbl.append($.JST.createFromTemplate(res, "RESOURCE_ROW"))
            }


            //bind add resource
            resourceEditor.find("#addResource").click(function() {
                resTbl.append($.JST.createFromTemplate({id: "new", name: "resource"}, "RESOURCE_ROW"))
            });

            //bind save event
            resourceEditor.find("#resSaveButton").click(function() {
                var newRes = [];
                //find for deleted res
                for (var i = 0; i < ge.resources.length; i++) {
                    var res = ge.resources[i];
                    var row = resourceEditor.find("[resId=" + res.id + "]");
                    if (row.size() > 0) {
                        //if still there save it
                        var name = row.find("input[name]").val();
                        if (name && name != "")
                            res.name = name;
                        newRes.push(res);
                    } else {
                        //remove assignments
                        for (var j = 0; j < ge.tasks.length; j++) {
                            var task = ge.tasks[j];
                            var newAss = [];
                            for (var k = 0; k < task.assigs.length; k++) {
                                var ass = task.assigs[k];
                                if (ass.resourceId != res.id)
                                    newAss.push(ass);
                            }
                            task.assigs = newAss;
                        }
                    }
                }

                //loop on new rows
                resourceEditor.find("[resId=new]").each(function() {
                    var row = $(this);
                    var name = row.find("input[name]").val();
                    if (name && name != "")
                        newRes.push(new Resource("tmp_" + new Date().getTime(), name));
                });

                ge.resources = newRes;

                closeBlackPopup();
                ge.redraw();
            });


            var ndo = createBlackPage(400, 500).append(resourceEditor);
        }


    </script>

    <script type="text/javascript">

        $.JST.loadDecorator("ASSIGNMENT_ROW", function(assigTr, taskAssig) {

            var resEl = assigTr.find("[name=resourceId]");
            for (var i in taskAssig.task.master.resources) {
                var res = taskAssig.task.master.resources[i];
                var opt = $("<option>");
                opt.val(res.id).html(res.name);
                if (taskAssig.assig.resourceId == res.id)
                    opt.attr("selected", "true");
                resEl.append(opt);
            }


            var roleEl = assigTr.find("[name=roleId]");
            for (var i in taskAssig.task.master.roles) {
                var role = taskAssig.task.master.roles[i];
                var optr = $("<option>");
                optr.val(role.id).html(role.name);
                if (taskAssig.assig.roleId == role.id)
                    optr.attr("selected", "true");
                roleEl.append(optr);
            }

            if (taskAssig.task.master.canWrite && taskAssig.task.canWrite) {
                assigTr.find(".delAssig").click(function() {
                    var tr = $(this).closest("[assigId]").fadeOut(200, function() {
                        $(this).remove();
                    });
                });
            }


        });
    </script>
<?php }//end if gantt ?>


<?php if (isset($message)) { ?>
    <link href="<?php echo base_url('assets/plugins/summernote/summernote.min.css'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/summernote/summernote.min.js'); ?>"></script>

    <script>

        $(document).ready(function() {
            /** BEGIN SUMMERNOTE **/
            if ($('.summernote-lg').length > 0) {
                $('.summernote-lg').summernote({
                    height: 400,
                    codemirror: { theme: 'monokai' }

                });
            }

            if ($('.summernote-sm').length > 0) {
                $('.summernote-sm').summernote({
                    height: 200,
                    toolbar: [
                        //['style', ['style']], // no style button
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strike']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        //['insert', ['picture', 'link']], // no insert buttons
                        //['table', ['table']], // no table button
                        //['help', ['help']] //no help button
                    ]
                });
            }


        });
    </script>
<?php } ?>
<?php if (isset($show_editor)) { ?>
    <link href="<?php echo base_url('assets/plugins/summernote/summernote.min.css'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/summernote/summernote.min.js'); ?>"></script>
    <script type="text/javascript">
        $('.temp_editor').summernote({
            height: 400,
            codemirror: {
                theme: 'monokai'
            }
        });

    </script>
<?php } ?>

<?php if(isset($sort_menu)) {  ?>

    <link href="<?php echo base_url('assets/plugins/jquery-nestable/jquery.nestable.css'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-nestable/jquery.nestable.js'); ?>"></script>
    <script>
        $(document).ready(function(){

            $('#orderResult').nestable({
                group: 1
            });

            $('.dd-list').nestable({
                handle: 'div',
                items: 'li',
                toleranceElement: '> div',
                maxLevels: 2
            });

        });
        $(function() {
            $.post('<?php echo site_url('settings/order_ajax'); ?>', {}, function(data){
                $('#orderResult').html(data);
            });

            $('#save').click(function(){

                oSortable = $('.dd').nestable('serialize');

                // alert(JSON.stringify(oSortable))
                $('#orderResult').slideUp(function(){
                    $.post('<?php echo site_url('settings/order_ajax'); ?>', { sortable: oSortable }, function(data){
                        $('#orderResult').html(data);
                        $('#orderResult').slideDown();
                    });
                });

            });
        });
    </script>

<?php } ?>
<?php if (isset($datatable)) { ?>

    <link href="<?php echo base_url('assets/plugins/datatable/css/bootstrap.datatable.min.css'); ?>" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url('assets/plugins/datatable/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/datatable/js/bootstrap.datatable.js'); ?>"></script>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            var oTable = $('#zest_table').dataTable({
                "aoColumnDefs": [
                    {"aTargets": [1, 1]}
                ],
                "aaSorting": [[1, 'asc']],
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "Todos"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 20
            });

            $('#inbox-table').dataTable();

        });

    </script>
    <script type="text/javascript">
        jQuery(document).ready(function() {

            var oTable = $('#zest_table1').dataTable({
                "aoColumnDefs": [
                    {"aTargets": [1, 1]}
                ],
                "aaSorting": [[1, 'asc']],
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "Todos"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 4
            });

            $('#inbox-table').dataTable();

        });

    </script>
      <script type="text/javascript">
        jQuery(document).ready(function() {

            var oTable = $('#zest_table20').dataTable({
                "aoColumnDefs": [
                    {"aTargets": [1, 1]}
                ],
                "aaSorting": [[1, 'asc']],
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "Todos"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 20
            });

            $('#inbox-table').dataTable();

        });

    </script>
    <script type="text/javascript">
        jQuery(document).ready(function() {

            var oTable = $('#zest_table2').dataTable({
                "aoColumnDefs": [
                    {"aTargets": [1, 1]}
                ],
                "aaSorting": [[1, 'asc']],
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "Todos"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 4
            });

            $('#inbox-table').dataTable();

        });

    </script>
     <script type="text/javascript">
        jQuery(document).ready(function() {

            var oTable = $('#zest_table_client').dataTable({
                "aoColumnDefs": [
                    {"aTargets": [1, 1]}
                ],
                "aaSorting": [[2, 'asc']],
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "Todos"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 50
            });

            $('#inbox-table').dataTable();

        });

    </script>
    <script type="text/javascript">
       jQuery(document).ready(function() {

           var oTable = $('#zest_table_cuenta').dataTable({
               "aoColumnDefs": [
                   {"aTargets": [1, 1]}
               ],
               "aaSorting": [[0, 'asc']],
               "aLengthMenu": [
                   [5, 15, 20, -1],
                   [5, 15, 20, "Todos"] // change per page values here
               ],
               // set the initial value
               "iDisplayLength": 50
           });

           $('#inbox-table').dataTable();

       });

   </script>
    <script type="text/javascript">
        jQuery(document).ready(function() {

            var oTable = $('#zest_table3').dataTable({
                "aoColumnDefs": [
                    {"aTargets": [1, 1]}
                ],
                "aaSorting": [[1, 'asc']],
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "Todos"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 4
            });

            $('#inbox-table').dataTable();

        });

    </script>

<?php }//end include datatables  ?>


<?php if (isset($inbox_datatable)) { ?>
    <script src="<?php echo base_url('assets/plugins/footable/js/footable.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/footable/js/footable.filter.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/plugins/footable/js/footable.paginate.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/zest_mail.js'); ?>" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.inbox-table-icon input:checkbox').click(function() {
                enableDeleteButton();
            });

            $(".program").click(function() {

                if(confirm("¿Está seguro de mandarlos a programar?"))
                {
                    var id = [];
                    $(':checkbox:checked').each(function(i){
                        id[i] = $(this).val();
                    });
                    if(id.length === 0) //tell you if the array is empty
                    {
                        alert("Favor de seleccionar al menos un checkbox");
                    }
                    else
                    {
                        $.ajax({
                            url:'<?php echo site_url('billing/status_program'); ?>',
                            method:'POST',
                            data:{id:id},
                            success:function()
                            {
                                for(var i=0; i<id.length; i++)
                                {
                                    $('tr#'+id[i]+'').css('background-color', '#ccc');
                                    $('tr#'+id[i]+'').fadeOut('slow');
                                }
                                // location.reload();
                            }
                        });
                    }
                }
                else
                {
                    return false;
                }

            });
            $(".for_estimate").click(function() {

                if(confirm("¿Está seguro de mandalos para estimación?"))
                {
                    var id = [];
                    $(':checkbox:checked').each(function(i){
                        id[i] = $(this).val();
                    });


                    console.log(validar);
                    if(id.length === 0) //tell you if the array is empty
                    {
                        alert("Favor de seleccionar al menos un checkbox");
                    }
                    else
                    {
                        $.ajax({
                            url:'<?php echo site_url('billing/status_stimate'); ?>',
                            method:'POST',
                            data:{id:id},
                            success:function()
                            {
                                for(var i=0; i<id.length; i++)
                                {
                                    $('tr#'+id[i]+'').css('background-color', '#ccc');
                                    $('tr#'+id[i]+'').fadeOut('slow');
                                }
                                location.reload();
                            }
                        });
                    }
                }
                else
                {
                    return false;
                }

            });
            $(".paidd").click(function() {

                if(confirm("¿Está seguro de mandarlos a Pagado?"))
                {
                    var id = [];
                    $(':checkbox:checked').each(function(i){
                        id[i] = $(this).val();
                    });
                    if(id.length === 0) //tell you if the array is empty
                    {
                        alert("Favor de seleccionar al menos un checkbox");
                    }
                    else
                    {
                        $.ajax({
                            url:'<?php echo site_url('billing/status_paid'); ?>',
                            method:'POST',
                            data:{id:id},
                            success:function()
                            {
                                for(var i=0; i<id.length; i++)
                                {
                                    $('tr#'+id[i]+'').css('background-color', '#ccc');
                                    $('tr#'+id[i]+'').fadeOut('slow');
                                }
                                location.reload();
                            }
                        });
                    }
                }
                else
                {
                    return false;
                }

            });
            $(".deliver").click(function() {

                if(confirm("¿Está seguro de mandalos a entregados?"))
                {
                    var id = [];
                    $(':checkbox:checked').each(function(i){
                        id[i] = $(this).val();
                    });
                    if(id.length === 0) //tell you if the array is empty
                    {
                        alert("Favor de seleccionar al menos un checkbox");
                    }
                    else
                    {
                        $.ajax({
                            url:'<?php echo site_url('billing/status_deliver'); ?>',
                            method:'POST',
                            data:{id:id},
                            success:function()
                            {
                                for(var i=0; i<id.length; i++)
                                {
                                    $('tr#'+id[i]+'').css('background-color', '#ccc');
                                    $('tr#'+id[i]+'').fadeOut('slow');
                                }
                                location.reload();
                            }
                        });
                    }
                }
                else
                {
                    return false;
                }

            });
            $(".paid1").click(function() {
                if(confirm("¿Está seguro que es un pago directo?"))
                {

                    var id = [];
                    $(':checkbox:checked').each(function(i){
                        id[i] = $(this).val();
                    });



                    if(id.length === 0) //tell you if the array is empty
                    {
                        alert("Favor de seleccionar al menos un checkbox");
                    }
                    else
                    {
                        $.ajax({
                            url:'<?php echo site_url('billing/status_paid1'); ?>',
                            method:'POST',
                            data:{id:id},
                            success:function()
                            {
                                for(var i=0; i<id.length; i++)
                                {
                                    $('tr#'+id[i]+'').css('background-color', '#ccc');
                                    $('tr#'+id[i]+'').fadeOut('slow');
                                }
                                // location.reload();
                            }
                        });
                    }
                }
                else
                {
                    return false;
                }

            });
            $(".paid2").click(function() {
              if(confirm("¿Está seguro de madarlos a Pago ?")){
                    var id = [];
                    $(':checkbox:checked').each(function(i){
                        id[i] = $(this).val();
                    });



                    if(id.length === 0) //tell you if the array is empty
                    {
                        alert("Favor de seleccionar al menos un checkbox");
                        return false;
                    }
                    else
                    {
                        $.ajax({
                            url:'<?php echo site_url('billing/status_paid2'); ?>',
                            method:'POST',
                            data:{id:id},
                            success:function()
                            {
                                for(var i=0; i<id.length; i++)
                                {
                                    $('tr#'+id[i]+'').css('background-color', '#ccc');
                                    $('tr#'+id[i]+'').fadeOut('slow');
                                }
                                location.reload();
                            }
                        });
                    }
                  }else {
                    return false;
                  }
            });
            $(".paid3").click(function() {

                if(confirm("¿Está seguro que es un pago en cheque?"))
                {
                    var id = [];
                    $(':checkbox:checked').each(function(i){
                        id[i] = $(this).val();
                    });
                    if(id.length === 0) //tell you if the array is empty
                    {
                        alert("Favor de seleccionar al menos un checkbox");
                    }
                    else
                    {
                        $.ajax({
                            url:'<?php echo site_url('billing/status_paid3'); ?>',
                            method:'POST',
                            data:{id:id},
                            success:function()
                            {
                                for(var i=0; i<id.length; i++)
                                {
                                    $('tr#'+id[i]+'').css('background-color', '#ccc');
                                    $('tr#'+id[i]+'').fadeOut('slow');
                                }
                                location.reload();
                            }
                        });
                    }
                }
                else
                {
                    return false;
                }

            });

            $(".emptybutton").click(function() {

                if(confirm("¿Está completamente seguro de eliminar esto?"))
                {
                    var id = [];
                    $(':checkbox:checked').each(function(i){
                        id[i] = $(this).val();
                    });
                    if(id.length === 0) //tell you if the array is empty
                    {
                        alert("Favor de seleccionar al menos un checkbox");
                    }
                    else
                    {
                        $.ajax({
                            url:'<?php echo site_url('message/empty_messages'); ?>',
                            method:'POST',
                            data:{id:id},
                            success:function()
                            {
                                for(var i=0; i<id.length; i++)
                                {
                                    $('tr#'+id[i]+'').css('background-color', '#ccc');
                                    $('tr#'+id[i]+'').fadeOut('slow');
                                }
                            }
                        });
                    }
                }
                else
                {
                    return false;
                }

            });

            function enableDeleteButton() {
                var isChecked = $('.inbox-table-icon input:checkbox').is(':checked');

                if (isChecked) {
                    $(".inbox-checkbox-triggered").addClass('visible');
                } else {
                    $(".inbox-checkbox-triggered").removeClass('visible');
                }
            }


        });

    </script>

<?php }//end include datatables  ?>

<?php if (isset($show_quote_subcategory)) { ?>

    <script type="text/javascript">
        $(document).ready(function() {
            $("<option selected='selected' value=0>&nbsp;</option>").prependTo("#subcategory");
            $('#quote_category').change(function() {
                var opt = '';
                var category_id = '';
                category_id = $(this).find(':selected').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('quotation/get_subcategory'); ?>",
                    data: {'category_id': category_id},
                    dataType: "json", //return type expected as json
                    success: function(subcategories) {
                        $('#subcategory').empty();
                        if (subcategories.length) {
                            $("#subcategory_link").show();
                            $.each(subcategories, function(key, val) {
                                opt = $('<option />');
                                opt.val(val.sub_id);
                                opt.text(val.sub_name);
                                $('#subcategory').append(opt);
                            });
                        } else {

                            $("#subcategory_link").hide();
                        }
                    }
                });
            });
        });
    </script>
<?php } ?>

<?php if (isset($estimate_calculations)) { ?>


    <link href="<?php echo base_url('assets/plugins/select2/select2.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/select2/select2-bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url('assets/plugins/select2/select2.js'); ?>"></script>


    <script type="text/javascript">
    var i = 1;
        jQuery(document).ready(function() {

            if ($('.solsoSelect2').length) {
                $( ".solsoSelect2" ).select2();

            }


            $('#btn_add_item').on('click', function(e) {
                $( '.solsoSelect2.solsoCloneSelect2').select2('destroy');

            //Se manda el ID de la tabla, y se busca por su table Row
            //(Si la tabla tiene titulo se tiene que poner eq(numero de fila a copiar))
              $("#item_table tr").eq(1).clone().find("input,select").each(function() {
            $(this).attr({
            //Para incrementar al id al momento de clonar
            'id': function(_, id) { return id + i },
            //Para incrementar el nombre al momento de clonar
            //'name': function(_, name) { return name + i },
            //Si se quiere cambiar el valor de default
            'value': ''
            });
                }).end().appendTo("#item_table");
            i++; //Se incrementa el valor que se esta agregando al id

                $( '.crt' ).each(function( index ) {

                    $( this ).text(index+1);

                    if (index > 0) {
                        $( this ).parent().find( '.removeClone' ).removeClass('disabled');
                    }
                });
                $('input[name="item_id"]').val('0')
                $('input[name="item_name"]').val('');


                $( ".sub_total" ).last().val('0.00');

                $( '.solsoCloneSelect2' ).select2();

                return false;
            });

            $('#btn_update_add_item').on('click', function(e) {
                $( '.solsoSelect2.solsoCloneSelect2').select2('destroy');

                $('.solsoParent').append('<tr>' + $('tr.updatesolsoChild').html() + '</tr>');



                $( '.crt' ).each(function( index ) {
                    $( this ).text(index+1);

                    if (index > 0) {
                        $( this ).parent().find( '.removeClone' ).removeClass('disabled');
                    }
                });

                $( ".sub_total" ).last().val('0.00');

                $( '.solsoCloneSelect2' ).select2();

                return false;
            });

            $( document ).on('click', '.removeClone', function() {

                $(this).parents().eq(1).remove();

                $( '.crt' ).each(function( index ) {
                    $( this ).text(index+1);
                });

                if ($(this).attr('data-id').length ) {

                    $.post("<?php echo site_url('estimate/delete_item'); ?>", {
                            estimate_id: <?php echo ($estimate->est_id)? $estimate->est_id:null; ?>,
                            detail_id: $(this).attr('data-id')
                        },
                        function(data) {

                            var response = JSON.parse(data);
                            if (response.success == '1') {
                                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "positionClass": "toast-bottom-right",
                                    "onclick": null,
                                    "showDuration": "1000",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                };
                                toastr.success(response.message, "");
                            }
                        });

                }
                re_calculate();
            });

            /* === INVOICE === */
            $( document ).on('change', '.solsoCloneSelect2', function() {
                var item_id = '';
                item_id = $(this).find(':selected').val();
                inputPrice = $(this).closest('tr').find("[name=hourly_rate]");
                inputUnits = $(this).closest('tr').find("[name=item_units]");
                inputDescription = $(this).closest('tr').find("[name=item_description]");
                $.ajax({
                    url: "<?php echo site_url('item/get_selected_item'); ?>",
                    type: 'post',
                    dataType: 'json',
                    data: { item_id: item_id },
                    success:function(data) {
                        inputPrice.val(data['unit_cost']);
                        inputDescription.val(data['item_desc']);
                        inputUnits.val(data['item_unit']);
                        // recalculate();
                    }
                });

            });

            $( document ).on('change', '.solsoCurrencyEvent', function() {
                if ( $(this).val() != '') {
                    $( '.solsoCurrency' ).text( $( "[name='currency'] option[value='" + $(this).val() + "']").text() );
                }
            });

            $( document ).on("click change paste keyup", ".solsoEvent", function() {
                var qty			= $(this).closest('tr').find("[name=item_hours_worked]").val();
                var price		= $(this).closest('tr').find("[name=hourly_rate]").val();
                var tax			= $(this).closest('tr').find("[name=item_tax]").val();
                var discount	= $(this).closest('tr').find("[name=item_discount]").val();

                var subTotal	= 0;
                var total		= 0;
                var taxtotal	= 0;
                var totsubTotal	= 0;


                itemQty			= parseFloat(qty)  > 0 		? parseFloat(qty).toFixed(2) 		: 0;
                itemPrice		= parseFloat(price)  > 0 	? parseFloat(price).toFixed(2) 		: 0;
                itemTax			= parseFloat(tax) > 0 		? parseFloat(tax).toFixed(2) 		: 0;
                itemDiscount	= parseFloat(discount) > 0	? parseFloat(discount).toFixed(2)	: 0;


                solsoValue 			= itemQty * itemPrice;
                solsoTax			= solsoValue * (itemTax / 100);
                solsoPrice			= solsoValue + solsoTax;
                solsoDiscount		= itemDiscount;

                solsoDiscount	= solsoValue * (itemDiscount / 100);
                subTotal		= solsoValue- solsoDiscount;

                $(this).closest("tr").find("input[name=item_subtotal]").val(subTotal.toFixed(2));

                re_calculate();
            });
            /* === END INVOICE === */

            // recalculate();



            function re_calculate() {

                var total		= parseFloat(0);
                var taxtotal	= parseFloat(0);
                var distotal	= parseFloat(0);
                var totsubTotal	= parseFloat(0);
                var sub_Totals	= parseFloat(0);

                $('#item_table > tbody >tr').each(function() {
                    var item_subtotal = parseFloat(0);
                    var tax = parseFloat(0);
                    var discount = parseFloat(0);
                    var qty = parseFloat(0);
                    var price = parseFloat(0);

                    qty	= $(this).find("[name=item_hours_worked]").val();
                    price	= $(this).find("[name=hourly_rate]").val();
                    item_subtotal =  (qty*price);

                    tax			=  $(this).find("[name=item_tax]").val();
                    discount	=  $(this).find("[name=item_discount]").val();

                    distotal +=   item_subtotal* (discount / 100);
                    taxtotal +=   (item_subtotal-distotal) * (tax / 100);
                    totsubTotal += parseFloat(item_subtotal);

                });

                sub_Totals = parseFloat((totsubTotal-distotal));
                total = parseFloat((totsubTotal-distotal)+taxtotal);


                $('#invoice_subtotal').val((sub_Totals).toFixed(2));
                $('#tax_total').val((taxtotal).toFixed(2) );
                $('#invoice_total_amount').val((total).toFixed(2) );
            }




$('#save_items').click(function() {
            //     var items = [];
            //     $('#item_table > tbody >tr').each(function() {
            //         var row = {};
            //         $(this).find('input,select,textarea').each(function() {

            //             row[$(this).attr('name')] = $(this).val();
            //         });

            //         items.push(row);
            //     });

            //     $.post("<?php echo site_url('estimate/save'); ?>", {
            //             est_id: <?php echo $estimate->est_id; ?>,
            //             items: JSON.stringify(items)
            //         },
            //         function(data) {

            //             var response = JSON.parse(data);
            //             if (response.success == '1') {
            //                 toastr.options = {
            //                     "closeButton": true,
            //                     "debug": false,
            //                     "positionClass": "toast-bottom-right",
            //                     "onclick": null,
            //                     "showDuration": "1000",
            //                     "hideDuration": "1000",
            //                     "timeOut": "5000",
            //                     "extendedTimeOut": "1000",
            //                     "showEasing": "swing",
            //                     "hideEasing": "linear",
            //                     "showMethod": "fadeIn",
            //                     "hideMethod": "fadeOut"
            //                 };
            //                 toastr.success(response.message, "");
            //                 $('input[type=text]').val('');
            //                 $('input[type=select]').val('');
            //             }

            //             if (response.success == '0') {
            //                 toastr.options = {
            //                     "closeButton": true,
            //                     "debug": false,
            //                     "positionClass": "toast-bottom-right",
            //                     "onclick": null,
            //                     "showDuration": "1000",
            //                     "hideDuration": "1000",
            //                     "timeOut": "5000",
            //                     "extendedTimeOut": "1000",
            //                     "showEasing": "swing",
            //                     "hideEasing": "linear",
            //                     "showMethod": "fadeIn",
            //                     "hideMethod": "fadeOut"
            //                 };
            //                 toastr.error(response.message, "");
            //                 $('input[type=text]').val('');
            //                 $('input[type=select]').val('');
            //             }
            //             window.location = base_url+"estimate/preview/<?php echo $estimate->est_id; ?>/yes";
            //         });
             });

        });


    </script>
<?php }//end include datatables  ?>
<?php if (isset($invoice_calculations)) { ?>


    <link href="<?php echo base_url('assets/plugins/select2/select2.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/plugins/select2/select2-bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url('assets/plugins/select2/select2.js'); ?>"></script>


    <script type="text/javascript">
        jQuery(document).ready(function() {


            $('body').on('change','#client_id', function(e) {
                e.preventDefault();


                var client_id = $('select[name="client"]').val();
                if (client_id == 'Empty') {
                    return;
                }
                $.post('<?php echo site_url('billing/ajax_client_results'); ?>', { client_id: client_id }, function(data){
                    $('#client_details').html(data);

                });
            });

            $('body').on('change','#client_id', function(e) {
                e.preventDefault();


                var client_id = $('select[name="client"]').val();
                if (client_id == 'Empty') {
                    return;
                }
                $.post('<?php echo site_url('billing/ajax_client_items'); ?>', { client_id: client_id }, function(data){
                    $('#client_items').html(data);

                });
            });

            $('body').on('change','#project', function(e) {
                e.preventDefault();


                var project_id = $('select[name="project"]').val();
                if (project_id == 'Empty') {
                    return;
                }
                $.post('<?php echo site_url('billing/ajax_project_milestone'); ?>', { project_id: project_id }, function(data){
                    $('#project_milestone').html(data);

                });
            });


            if ($('.solsoSelect2').length) {
                $( ".solsoSelect2" ).select2();

            }


            $('#btn_add_item').on('click', function(e) {
                $( '.solsoSelect2.solsoCloneSelect2').select2('destroy');
               // $('.solsoParent').append('<tr>' + $('tr.solsoChild').html() + '</tr>');


                $( '.crt' ).each(function( index ) {
                    $( this ).text(index+1);

                    if (index > 0) {
                        $( this ).parent().find( '.removeClone' ).removeClass('disabled');
                    }
                });
                $('input[name="item_id"]').val('0');
                $('input[name="item_name"]').val('');


                $( ".sub_total" ).last().val('0.00');

                $( '.solsoCloneSelect2' ).select2();

                return false;
            });

            $('#btn_update_add_item').on('click', function(e) {
                $( '.solsoSelect2.solsoCloneSelect2').select2('destroy');

                $('.solsoParent').append('<tr>' + $('tr.updatesolsoChild').html() + '</tr>');



                $( '.crt' ).each(function( index ) {
                    $( this ).text(index+1);

                    if (index > 0) {
                        $( this ).parent().find( '.removeClone' ).removeClass('disabled');
                    }
                });

                $( ".sub_total" ).last().val('0.00');

                $( '.solsoCloneSelect2' ).select2();

                return false;
            });

            $( document ).on('click', '.removeClone', function() {

                $(this).parents().eq(1).remove();

                $( '.crt' ).each(function( index ) {
                    $( this ).text(index+1);
                });

                re_calculate();
            });

            $('#btn_update_add_item2').on('click', function(e) {
                $( '.solsoSelect2.solsoCloneSelect2').select2('destroy');

                $('.solsoParent').append('<tr>' + $('tr.updatesolsoChild').html() + '</tr>');



                $( '.crt' ).each(function( index ) {
                    $( this ).text(index+1);

                    if (index > 0) {
                        $( this ).parent().find( '.removeClone' ).removeClass('disabled');
                    }
                });

                $( ".sub_total" ).last().val('0.00');

                $( '.solsoCloneSelect2' ).select2();

                return false;
            });

            $( document ).on('click', '.removeClone', function() {

                $(this).parents().eq(1).remove();

                $( '.crt' ).each(function( index ) {
                    $( this ).text(index+1);
                });

                re_calculate2();
            });


            /* === INVOICE === */
            $( document ).on('change', '.solsoCloneSelect2', function() {
                var item_id = '';
                item_id = $(this).find(':selected').val();
                inputPrice = $(this).closest('tr').find("[name=hourly_rate]");
                inputUnits = $(this).closest('tr').find("[name=item_units]");
                inputDescription = $(this).closest('tr').find("[name=item_description]");
                $.ajax({
                    url: "<?php echo site_url('item/get_selected_item'); ?>",
                    type: 'post',
                    dataType: 'json',
                    data: { item_id: item_id },
                    success:function(data) {
                        inputPrice.val(data['unit_cost']);
                        inputDescription.val(data['item_desc']);
                        inputUnits.val(data['item_unit']);
                        // recalculate();
                    }
                });

            });

            $( document ).on('change', '.solsoCurrencyEvent', function() {
                if ( $(this).val() != '') {
                    $( '.solsoCurrency' ).text( $( "[name='currency'] option[value='" + $(this).val() + "']").text() );
                }
            });

            $( document ).on("click change paste keyup", ".solsoEvent", function() {
                var qty			= $(this).closest('tr').find("[name=item_hours_worked]").val();
                var price		= $(this).closest('tr').find("[name=hourly_rate]").val();
                var tax			= $(this).closest('tr').find("[name=item_tax]").val();
                var discount	= $(this).closest('tr').find("[name=item_discount]").val();

                var subTotal	= 0;
                var total		= 0;
                var taxtotal	= 0;
                var totsubTotal	= 0;


                itemQty			= parseFloat(qty)  > 0 		? parseFloat(qty).toFixed(2) 		: 0;
                itemPrice		= parseFloat(price)  > 0 	? parseFloat(price).toFixed(2) 		: 0;
                itemTax			= parseFloat(tax) > 0 		? parseFloat(tax).toFixed(2) 		: 0;
                itemDiscount	= parseFloat(discount) > 0	? parseFloat(discount).toFixed(2)	: 0;


                solsoValue 			= itemQty * itemPrice;
                solsoTax			= solsoValue * (itemTax / 100);
                solsoPrice			= solsoValue + solsoTax;
                solsoDiscount		= itemDiscount;

                solsoDiscount	= solsoValue * (itemDiscount / 100);
                subTotal		= solsoValue- solsoDiscount;


                $(this).closest("tr").find("input[name=item_subtotal]").val(subTotal.toFixed(2));

                re_calculate();
            });

            $( document ).change(".solsoEvent2", function() {
              var qty			= $(this).closest('tr').find("[name=item_hours_worked]").val();
              var price		= $(this).closest('tr').find("[name=hourly_rate]").val();
              var tax			= $(this).closest('tr').find("[name=item_tax]").val();
              var discount	= $(this).closest('tr').find("[name=item_discount]").val();

              var subTotal	= 0;
              var total		= 0;
              var taxtotal	= 0;
              var totsubTotal	= 0;


              itemQty			= parseFloat(qty)  > 0 		? parseFloat(qty).toFixed(2) 		: 0;
              itemPrice		= parseFloat(price)  > 0 	? parseFloat(price).toFixed(2) 		: 0;
              itemTax			= parseFloat(tax) > 0 		? parseFloat(tax).toFixed(2) 		: 0;
              itemDiscount	= parseFloat(discount) > 0	? parseFloat(discount).toFixed(2)	: 0;


              solsoValue 			= itemQty * itemPrice;
              solsoTax			= solsoValue * (itemTax / 100);
              solsoPrice			= solsoValue + solsoTax;
              solsoDiscount		= itemDiscount;

              solsoDiscount	= solsoValue * (itemDiscount / 100);
              subTotal		= solsoValue- solsoDiscount;


                $(this).closest("tr").find("input[name=item_subtotal]").val(subTotal.toFixed(2));

                re_calculate2();
            });
            /* === END INVOICE === */


            function re_calculate() {

                var total		= parseFloat(0);
                var taxtotal	= parseFloat(0);
                var distotal	= parseFloat(0);
                var totsubTotal	= parseFloat(0);
                var sub_Totals	= parseFloat(0);

                $('#item_table > tbody >tr').each(function() {
                    var item_subtotal = parseFloat(0);
                    var tax = parseFloat(0);
                    var discount = parseFloat(0);
                    var qty = parseFloat(0);
                    var price = parseFloat(0);
                    qty	= $(this).find("[name=item_hours_worked]").val();
                    price	= $(this).find("[name=hourly_rate]").val();
                    item_subtotal =  (qty*price);
                    tax			=  $(this).find("[name=item_tax]").val();
                    discount	=  $(this).find("[name=item_discount]").val();

                    distotal +=   item_subtotal* (discount / 100);
                    taxtotal +=   (item_subtotal-distotal) * (tax / 100);
                    totsubTotal += parseFloat(item_subtotal);

                });

                sub_Totals = parseFloat((totsubTotal-distotal));
                total = parseFloat((totsubTotal-distotal)+taxtotal);


                $('#invoice_subtotal').val((sub_Totals).toFixed(2));
                $('#tax_total').val((taxtotal).toFixed(2) );
                $('#invoice_total_amount').val((total).toFixed(2) );
            }

            function re_calculate2() {

                var total		= parseFloat(0);
                var taxtotal	= parseFloat(0);
                var distotal	= parseFloat(0);
                var totsubTotal	= parseFloat(0);
                var sub_Totals	= parseFloat(0);

                $('#item_table > tbody >tr').each(function() {
                    var item_subtotal = parseFloat(0);
                    var tax = parseFloat(0);
                    var discount = parseFloat(0);
                    var qty = parseFloat(0);
                    var price = parseFloat(0);
                    qty	= $(this).find("[name=item_hours_worked]").val();
                    price	= $(this).find("[name=hourly_rate]").val();
                    item_subtotal =  (qty*price);
                    tax			=  $(this).find("[name=item_tax]").val();
                    discount	=  $(this).find("[name=item_discount]").val();

                    distotal +=   item_subtotal;
                    taxtotal +=   distotal * (tax / 100);
                    totsubTotal += parseFloat(item_subtotal);

                });

                sub_Totals = parseFloat(totsubTotal);
                total = parseFloat(totsubTotal+taxtotal);


                $('#invoice_subtotal').val((sub_Totals).toFixed(2));
                $('#tax_total').val((taxtotal).toFixed(2) );
                $('#invoice_total_amount').val((total).toFixed(2) );
            }




            $('#save_invoice_items').click(function() {
                var items = [];
                $('#item_table > tbody >tr').each(function() {
                    var row = {};
                    $(this).find('input,select,textarea').each(function() {

                        row[$(this).attr('name')] = $(this).val();
                    });

                    items.push(row);
                });
                var project  = $('select[name="project"]').val();
                var currency_id  = $('select[name="invoice_currency"]').val();
                var client_id  = $('select[name="client"]').val();
                var filter  = $('select[name="filter"]').val();
                var freq_recurring  = $('select[name="freq_recurring"]').val();
                var start_date  = $('input[name="invoice_start_date"]').val();
                var end_date  = $('input[name="invoice_due_date"]').val();
                var solicitante  = $('input[name="solicitante"]').val();
                var autorizado  = $('select[name="autorizado"]').val();
                var tax  = $('select[name="tax"]').val();
                var client_type  = $('input[name="client_type"]').val();
                var details  = $('textarea[name="details"]').val();
                var comment  = $('textarea[name="comment"]').val();

                console.log(items);


                if(client_id == 'Empty'){
                    return;
                }

                $.post("<?php echo site_url('billing/add_invoice'); ?>", {
                        invoice_id: '<?php echo $invoice_number; ?>',
                        project:project,
                        details:details,
                        comment:comment,
                        currency_id:currency_id,
                        client_id:client_id,
                        filter:filter,
                        start_date:start_date,
                        end_date:end_date,
                        solicitante:solicitante,
                        autorizado:autorizado,
                        tax:tax,
                        client_type:client_type,
                        freq_recurring:freq_recurring,
                        items: JSON.stringify(items)
                    },
                    function(data) {

                        var response = JSON.parse(data);
                        if (response.success == '1') {
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "positionClass": "toast-bottom-right",
                                "onclick": null,
                                "showDuration": "1000",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };
                            toastr.success(response.message, "");
                            $('input[type=text]').val('');
                            $('input[type=select]').val('');
                        }

                        if (response.success == '0') {
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "positionClass": "toast-bottom-right",
                                "onclick": null,
                                "showDuration": "1000",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };
                            toastr.error(response.message, "");
                            $('input[type=text]').val('');
                            $('input[type=select]').val('');
                        }
                        window.location.href = "https://proyectosinternos.com/Taller5/billing";
                    });

            });

            $('#save_presupuesto_items').click(function() {
                var items = [];

                $('#item_table > tbody >tr').each(function() {
                    var row = {};
                    $(this).find('input,select,textarea').each(function() {

                        row[$(this).attr('name')] = $(this).val();
                    });

                    items.push(row);
                });

                var invoice_number  = $('input[name="invoice_number"]').val();
                var project  = $('select[name="project"]').val();
                var currency_id  = $('select[name="invoice_currency"]').val();
                var client_id  = $('select[name="client"]').val();
                var tipo_pre = $('select[name="tipo_pre"]').val();
                var freq_recurring  = $('select[name="freq_recurring"]').val();
                var start_date  = $('input[name="invoice_start_date"]').val();
                var end_date  = $('input[name="invoice_due_date"]').val();
                var tax  = $('select[name="tax"]').val();
                var client_type  = $('input[name="client_type"]').val();
                var details  = $('textarea[name="details"]').val();

                console.log(invoice_number);


                if(client_id == 'Empty'){
                    return;
                }

                $.post("<?php echo site_url('billing/add_presupuesto'); ?>", {
                        invoice_id: invoice_number,
                        project:project,
                        currency_id:currency_id,
                        client_id:client_id,
                        tipo_pre:tipo_pre,
                        start_date:start_date,
                        end_date:end_date,
                        freq_recurring:freq_recurring,
                        tax:tax,
                        client_type:client_type,
                        details: details,
                        items: JSON.stringify(items)
                    },
                    function(data) {

                        var response = JSON.parse(data);
                        if (response.success == '1') {
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "positionClass": "toast-bottom-right",
                                "onclick": null,
                                "showDuration": "1000",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };
                            toastr.success(response.message, "");
                            $('input[type=text]').val('');
                            $('input[type=select]').val('');
                        }

                        if (response.success == '0') {
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "positionClass": "toast-bottom-right",
                                "onclick": null,
                                "showDuration": "1000",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };
                            toastr.error(response.message, "");
                            $('input[type=text]').val('');
                            $('input[type=select]').val('');
                        }
                        window.location.href = "https://proyectosinternos.com/Taller5/billing";
                    });

            });

            $('#save_raya_items').click(function() {
                var items = [];
                $('#item_table > tbody >tr').each(function() {
                    var row = {};
                    $(this).find('input,select,textarea').each(function() {

                        row[$(this).attr('name')] = $(this).val();
                    });

                    items.push(row);
                });


                var project  = $('select[name="project"]').val();
                var currency_id  = $('select[name="invoice_currency"]').val();
                var client_id  = $('select[name="client"]').val();
                var filter  = $('select[name="filter"]').val();
                var freq_recurring  = $('select[name="freq_recurring"]').val();
                var start_date  = $('input[name="invoice_start_date"]').val();
                var end_date  = $('input[name="invoice_due_date"]').val();
                var solicitante  = $('input[name="solicitante"]').val();
                var autorizado  = $('select[name="autorizado"]').val();
                var details  = $('textarea[name="details"]').val();


                if(client_id == 'Empty'){
                    return;
                }

                $.post("<?php echo site_url('billing/add_raya'); ?>", {
                        invoice_id: '<?php echo $invoice_number; ?>',
                        project:project,
                        currency_id:currency_id,
                        client_id:client_id,
                        filter:filter,
                        start_date:start_date,
                        end_date:end_date,
                        solicitante:solicitante,
                        autorizado:autorizado,
                        details:details,
                        freq_recurring:freq_recurring,
                        items: JSON.stringify(items)
                    },
                    function(data) {

                        var response = JSON.parse(data);
                        if (response.success == '1') {
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "positionClass": "toast-bottom-right",
                                "onclick": null,
                                "showDuration": "1000",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };
                            toastr.success(response.message, "");
                            $('input[type=text]').val('');
                            $('input[type=select]').val('');
                        }

                        if (response.success == '0') {
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "positionClass": "toast-bottom-right",
                                "onclick": null,
                                "showDuration": "1000",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };
                            toastr.error(response.message, "");
                            $('input[type=text]').val('');
                            $('input[type=select]').val('');
                        }
                        window.location.replace("https://proyectosinternos.com/Taller5/billing");
                    });

            });

            $('#save_oficina_items').click(function() {
                var items = [];
                $('#item_table > tbody >tr').each(function() {
                    var row = {};
                    $(this).find('input,select,textarea').each(function() {

                        row[$(this).attr('name')] = $(this).val();
                    });

                    items.push(row);
                });


                var project  = $('select[name="project"]').val();
                var currency_id  = $('select[name="invoice_currency"]').val();
                var client_id  = $('select[name="client"]').val();
                var filter  = $('select[name="filter"]').val();
                var freq_recurring  = $('select[name="freq_recurring"]').val();
                var start_date  = $('input[name="invoice_start_date"]').val();
                var end_date  = $('input[name="invoice_due_date"]').val();
                var solicitante  = $('input[name="solicitante"]').val();
                var autorizado  = $('select[name="autorizado"]').val();
                var details  = $('textarea[name="details"]').val();


                if(client_id == 'Empty'){
                    return;
                }

                $.post("<?php echo site_url('billing/add_oficina'); ?>", {
                        invoice_id: '<?php echo $invoice_number; ?>',
                        project:project,
                        currency_id:currency_id,
                        client_id:client_id,
                        filter:filter,
                        start_date:start_date,
                        end_date:end_date,
                        solicitante:solicitante,
                        autorizado:autorizado,
                        details:details,
                        freq_recurring:freq_recurring,
                        items: JSON.stringify(items)
                    },
                    function(data) {

                        var response = JSON.parse(data);
                        if (response.success == '1') {
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "positionClass": "toast-bottom-right",
                                "onclick": null,
                                "showDuration": "1000",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };
                            toastr.success(response.message, "");
                            $('input[type=text]').val('');
                            $('input[type=select]').val('');
                        }

                        if (response.success == '0') {
                            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "positionClass": "toast-bottom-right",
                                "onclick": null,
                                "showDuration": "1000",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };
                            toastr.error(response.message, "");
                            $('input[type=text]').val('');
                            $('input[type=select]').val('');
                        }
                        window.location.replace("https://proyectosinternos.com/Taller5/billing");
                    });

            });


        });


    </script>
<?php }//end invoice ?>
<?php if (isset($show_countries)) { ?>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            $('#country_id').change(function () {

                var country_id = '';
                country_id = $(this).find(':selected').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('settings/get_states'); ?>",
                    data: {'country_id': country_id},
                    dataType: "json", //return type expected as json
                    success: function (states) {
                        $.each(states, function (key, val) {
                            var opt = $('<option />');
                            opt.val(val.id_state);
                            opt.text(val.name);
                            $('#states').append(opt);
                        });
                    }
                });
            });
        });
    </script>

<?php }//end include datatables  ?>


<?php if (isset($show_calendar)) { ?>

    <link href="<?php echo base_url('assets/plugins/fullcalendar/fullcalendar/fullcalendar.css'); ?>" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url('assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js'); ?>"></script>
    <script type="text/javascript">
        $(function() {
            var date = new Date();
            var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear();
            $('#module_calendar').fullCalendar({
                header: {
                    left: 'prevYear,prev,next,nextYear today',
                    center: 'title',
                    right: 'newEvent month,agendaWeek,agendaDay'
                },
                buttonText: {
                    prev: "<span class='fa fa-angle-left'></span>",
                    next: "<span class='fa fa-angle-right'></span>",
                    prevYear: "<span class='fa fa-angle-double-left'></span>",
                    nextYear: "<span class='fa fa-angle-double-right'></span>",
                    today: '<?php echo "today"; ?>',
                    newEvent: "<?php echo "New Link"; ?>",
                    month: '<?php echo "Month"; ?>',
                    week: '<?php echo "week"; ?>',
                    day: '<?php echo "day"; ?>'
                },
                <?php if($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2){ ?>
                googleCalendarApiKey: '<?php echo $this->config->item('system')->gcal_api_key; ?>',
                <?php } ?>
                eventAfterRender: function(event, element, view) {
                    $(element).attr('data-toggle', 'zestModal').addClass('zestModal');

                },
                eventSources: [
                    {
                        events: [
                            <?php

                            if (count($tasks)){ foreach ($tasks as $task) {
                            ?>
                            {
                                title  : '<?php echo $task->task_name ?>',
                                start  : '<?php echo date_from_timestamp($task->task_end); ?>',
                                end: '<?php echo date_from_timestamp($task->task_end); ?>',
                                url: '<?php echo  base_url('calendar/event/tasks/' . $task->task_id) ?>'
                            },
                            <?php } }?>
                        ],
                        color: '#E8AE00',
                        textColor: 'white'
                    },
                    {
                        events: [
                            <?php

                            if (count($payments)){foreach ($payments as $pay) { ?>
                            {
                                title  : '<?php echo $pay->inv_no." ".client_company($pay->client_id)?>',
                                start  : '<?php echo  $pay->created_at ?>',
                                end: '<?php echo  $pay->created_at ?>',
                                url: '<?php echo  base_url('calendar/event/payments/' . $pay->payment_id) ?>'
                            },
                            <?php }} ?>
                        ],
                        color: '#E8AE00',
                        textColor: 'white'
                    },
                    {
                        events: [
                            <?php

                            if (count($estimates)){
                            foreach ($estimates as $estimate) { ?>
                            {
                                title  : '<?php echo $estimate->est_code." ".client_company($estimate->client_id)?>',
                                start  : '<?php echo $estimate->est_due_date ?>',
                                end: '<?php echo $estimate->est_due_date ?>',
                                url: '<?php echo base_url('calendar/event/estimates/' . $estimate->est_id) ?>'

                            },
                            <?php }} ?>
                        ],
                        color: '#E8AE00',
                        textColor: 'white'
                    },
                    {
                        events: [
                            <?php

                            if (count($invoices)){
                            foreach ($invoices as $invoice) { ?>
                            {
                                title  : '<?php echo $invoice->inv_no." ".client_company($invoice->client_id)?>',
                                start  : '<?php echo $invoice->invoice_due ?>',
                                end: '<?php echo $invoice->invoice_due ?>',
                                url: '<?php echo base_url('calendar/event/invoices/' . $invoice->id) ?>'

                            },
                            <?php }} ?>
                        ],
                        color: '#E8AE00',
                        textColor: 'white'
                    },
                    {
                        events: [
                            <?php

                            if (count($quotations)){
                            foreach ($quotations as $quote) { ?>
                            {
                                title  : '<?php echo $quote->quote_code." ".client_company($quote->client_id)?>',
                                start  : '<?php echo $quote->quote_end_date ?>',
                                end: '<?php echo $quote->quote_end_date ?>',
                                url: '<?php echo base_url('calendar/event/quotations/' . $quote->quote_project_id) ?>'

                            },
                            <?php }} ?>
                        ],
                        color: '#E8AE00',
                        textColor: 'white'
                    },
                    {
                        events: [
                            <?php

                            if (count($projects)){
                            foreach ($projects as $project) { ?>
                            {
                                title  : '<?php echo $project->project_code." ".client_company($project->client_id)?>',
                                start  : '<?php echo $project->project_due_date ?>',
                                end: '<?php echo $project->project_due_date ?>',
                                url: '<?php echo base_url('calendar/event/projects/' . $project->project_id) ?>'

                            },
                            <?php }} ?>
                        ],
                        color: '#11a7db',
                        textColor: 'white'
                    },
                    {

                        events:[
                            <?php
                            if (count($agenda)){
                            foreach ($agenda as $event) {

                            $eventStartMnth = $event->startMonth;
                            $eventStartMonth = --$eventStartMnth;
                            $eventDate = strtotime($event->start_date);
                            $eventDate = date('Y', $eventDate).', '.$eventStartMonth.', '.date('d, H, i', $eventDate);

                            $eventEndMnth = $event->endMonth;
                            $eventEndMonth = --$eventEndMnth;
                            $eventEnd = strtotime($event->end_date);
                            $eventEnd = date('Y', $eventEnd).', '.$eventEndMonth.', '.date('d, H, i', $eventEnd);

                            // Check for an All Day event
                            if ($event->timeStart!= '00:00') { $allDay = 'allDay: false,'; } else { $allDay = 'allDay: true,'; }
                            if ($event->startTime == '00:00') { $startTime = ''; } else { $startTime = $event->startTime; }
                            if ($event->endTime == '00:00') { $endTime = ''; } else { $endTime = $event->endTime; }
                            // Check for an End Date
                            if ($event->dateEnd != '0000, 00, 00') { $endsOn = "end: new Date(".$eventEnd."),"; } else { $endsOn = ""; }
                            // Set the Times to Display
                            if ($event->displaystart != '12:00 AM') { $displaytime = $event->displaystart.' &mdash; '.$event->displayend; } else { $displaytime = ''; }
                            // Check if it is a Task
                            if ($event->is_task== '1') { $isTask = '<small class="label label-info preview-label">Task</small>'; $setColor = '#469cd5'; } else { $isTask = ''; }


                            ?>
                            {
                                title: '<?php echo addslashes($event->event_title) ?>',
                                start: '<?php echo $event->start_date ?>',
                                end: '<?php echo $event->end_date ?>',
                                startsondate: '<?php echo $event->startsOnDate ?>',
                                endsondate: '<?php echo $event->endsOnDate ?>',
                                starttime: '<?php echo $startTime ?>',
                                endtime: '<?php echo $endTime ?>',
                                <?php echo $endsOn ?>
                                <?php echo $allDay ?>
                                color: '<?php echo $event->event_color ?>',
                                desc: '<?php echo addslashes($event->event_description) ?>',
                                startson: '<?php echo $event->displayDate ?>',
                                displaytime: '<?php echo $displaytime ?>',
                                colorinput: '<?php echo $event->event_color ?>',
                                task: '<?php echo $isTask ?>',
                                url: '<?php echo base_url('calendar/event/agenda/' . $event->event_id) ?>',
                                eventid: '<?php echo $event->event_id ?>',
                                uid: '<?php echo $event->user_id ?>'

                            },
                            <?php }} ?>
                        ]

                    }
                    <?php if($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2){ ?>
                    ,{
                        googleCalendarId: '<?php echo $this->config->item('system')->gcal_id; ?>'
                    }
                    <?php } ?>
                ]
            });
        });
    </script>

<?php }//end show calendar  ?>

<?php if (isset($show_log_calendar)) { ?>


    <link href="<?php echo base_url('assets/plugins/fullcalendar/fullcalendar/fullcalendar.css'); ?>" rel="stylesheet" type="text/css" />
    <script src="<?php echo base_url('assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js'); ?>"></script>
    <script type="text/javascript">
        $(function() {
            var date = new Date();
            var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear();
            $('#calendar').fullCalendar({
                header: {
                    left: 'prevYear,prev,next,nextYear today',
                    center: 'title',
                    right: 'newEvent month,agendaWeek,agendaDay'
                },
                buttonText: {
                    prev: "<span class='fa fa-angle-left'></span>",
                    next: "<span class='fa fa-angle-right'></span>",
                    prevYear: "<span class='fa fa-angle-double-left'></span>",
                    nextYear: "<span class='fa fa-angle-double-right'></span>",
                    today: '<?php echo "today";?>',
                    newEvent: "<?php echo "New Link";  ?>",
                    month: '<?php echo "Month"; ?>',
                    week: '<?php echo "week";?>',
                    day: '<?php echo "day"; ?>'
                },
                eventSources: [
                    {
                        events: [
                            <?php

                            if (count($manual_timelogs)){ foreach ($manual_timelogs as $logs) {
                            ?>
                            {
                                title  : '<?php echo "PRO#".get_project_details($logs->project_id)->project_code; ?>',
                                url: '<?php echo base_url('project/timesheet/logs/'.$logs->entry_date); ?>',
                                start  : '<?php echo $logs->entry_date; ?>',
                                end: '<?php echo $logs->entry_date; ?>'
                            },
                            <?php } }?>
                        ],
                        color: '#E8AE00',
                        textColor: 'white'
                    }
                ]
            });
        });
    </script>

<?php }//end show calendar  ?>

<?php if (isset($show_client_chats)) { ?>

    <script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/morris-chart/morris.min.js'); ?>"></script>

    <script type="text/javascript">
        $(function() {
            function formatDate(myDate){
                var m_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                var d = new Date(myDate);

                var curr_month = d.getMonth();
                return (m_names[curr_month]);
            }

            <?php
            $user_check = (isset($client_id))? $client_id :null;

            ?>
            Morris.Line({
                element: 'client_statistics',
                data: [
                    { m: '<?php echo date('Y')."-01"; ?>', a: <?php echo stats_sales_monthly_data('01',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('01',$user_check,$this->Company_id); ?>, c: <?php echo stats_overdue_monthly_data('01',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-02"; ?>', a: <?php echo stats_sales_monthly_data('02',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('02',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('02',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-03"; ?>', a: <?php echo stats_sales_monthly_data('03',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('03',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('03',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-04"; ?>', a: <?php echo stats_sales_monthly_data('04',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('04',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('04',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-05"; ?>', a: <?php echo stats_sales_monthly_data('05',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('05',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('05',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-06"; ?>', a: <?php echo stats_sales_monthly_data('06',$user_check,$this->Company_id); ?>,  b: <?php echo stats_payment_monthly_data('06',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('06',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-07"; ?>', a: <?php echo stats_sales_monthly_data('07',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('07',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('07',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-08"; ?>', a: <?php echo stats_sales_monthly_data('08',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('08',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('08',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-09"; ?>', a: <?php echo stats_sales_monthly_data('09',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('09',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('09',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-10"; ?>', a: <?php echo stats_sales_monthly_data('10',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('10',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('10',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-11"; ?>', a: <?php echo stats_sales_monthly_data('11',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('11',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('11',$user_check,$this->Company_id); ?>  },
                    { m: '<?php echo date('Y')."-12"; ?>', a: <?php echo stats_sales_monthly_data('12',$user_check,$this->Company_id); ?>, b: <?php echo stats_payment_monthly_data('12',$user_check,$this->Company_id); ?> , c: <?php echo stats_overdue_monthly_data('12',$user_check,$this->Company_id); ?>  }
                ],
                xkey: 'm',
                ykeys: ['a', 'b','c'],
                labels: ['Sales', 'Payments','Overdue'],
                xLabelFormat: function(str){
                    return formatDate(str);
                },
                resize: true,
                lineColors: ['#8CC152', '#F6BB42','#4acab4']
            });

            $('.widget-easy-pie-1').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#8CC152',
                lineWidth: 10,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

            $('.widget-easy-pie-2').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F6BA48',
                lineWidth: 10,

                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

            $('.widget-easy-pie-3').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#4acab4',
                lineWidth: 10,


                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });


        });
    </script>
<?php }//end show client data  ?>

<?php if (isset($show_billing_chats)) { ?>

    <script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>

    <script type="text/javascript">
        $(function() {

            $('.billing-chart-1').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#8CC152',
                lineWidth: 10,


                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

            $('.billing-chart-2').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#F6BA48',
                lineWidth: 10,


                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });

            $('.billing-chart-3').easyPieChart({
                easing: 'easeOutBounce',
                barColor : '#4acab4',
                lineWidth: 10,


                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });


        });
    </script>
<?php }//end show billing data  ?>

<?php if (isset($show_setting_menu)) { ?>

    <script type="text/javascript">
        $(document).ready(function(){

            //Stack menu when collapsed
            $('#bs-example-navbar-collapse-1').on('show.bs.collapse', function() {
                $('.nav-settings').addClass('nav-stacked');
            });

            $('#bs-example-navbar-collapse-1').on('hide.bs.collapse', function() {
                $('.nav-settings').removeClass('nav-stacked');
            });

            $('#cssmenu > li:has(ul)').addClass("has-sub");


            $('#cssmenu >  li > a').click(function() {
                var checkElement = $(this).next();

                $('#cssmenu li').removeClass('active');



                if(checkElement.is(':visible')) {
                    $(this).closest('li').removeClass('active');
                    checkElement.slideUp('normal');
                }

                if(!checkElement.is(':visible')) {
                    $('#cssmenu  ul:visible').slideUp('normal');
                    checkElement.slideDown('normal');
                }

                if (checkElement.is('ul')) {
                    return false;
                } else {
                    return true;
                }
            });
        });
    </script>
<?php }//end show setting menu  ?>

<?php if (isset($show_task_comments)) { ?>
    <script type="text/javascript">
        $(document).ready(function(){

            $('#show_main-reply').click(function() {
                $('#main-reply').toggle();
            });

            $('#show_conversation-reply').click(function() {
                $('#conversation-reply').toggle();
            });

        });
    </script>
<?php }//end task comments  ?>

<?php if (isset($show_nouislider)) { ?>
    <link href="<?php echo base_url('assets/plugins/nouislider/jquery.nouislider.min.css" rel="stylesheet'); ?>"  type="text/css">
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/nouislider/jquery.nouislider.min.js'); ?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var slider2 = document.getElementById('progress-slider');

            if($("#project_worklog").is(':checked')) {
                slider2.setAttribute('disabled', true);
            }else{
                slider2.removeAttribute('disabled');
            }
            var progress = $('#progress').val();
            $('#progress-slider').noUiSlider({
                start: [ progress ],
                step: 10,
                connect: "lower",
                range: {
                    'min': 0,
                    'max': 100
                },
                format: {
                    to: function ( value ) {
                        return Math.floor(value);
                    },
                    from: function ( value ) {
                        return Math.floor(value);
                    }
                }
            });
            $('#progress-slider').on('slide', function() {
                var progress = $(this).val();
                $('#progress').val(progress);
                $('.noUi-handle').attr('title', progress+'%').tooltip('fixTitle').parent().find('.tooltip-inner').text(progress+'%');
            });

            $('#progress-slider').on('change', function() {
                var progress = $(this).val();
                $('#progress').val(progress);
            });

            $('#progress-slider').on('mouseover', function() {
                var progress = $(this).val();
                $('.noUi-handle').attr('title', progress+'%').tooltip('fixTitle').tooltip('show');
            });

            var progress_user_workog = $('#project_worklog');

            progress_user_workog.on('change',function(){
                var _checked = $(this).prop('checked');

                if(_checked) {
                    slider2.setAttribute('disabled', _checked);
                }else{
                    slider2.removeAttribute('disabled');
                }
            });

        });
    </script>
<?php } ?>
<?php if (isset($show_project_comments)) { ?>
    <script type="text/javascript">
        $(document).ready(function(){

            $('#show_main-reply').click(function() {
                $('#main-reply').toggle();
            });

            $('#show_conversation-reply').click(function() {

                $('#conversation-reply').toggle();

            });

        });
    </script>
<?php }//end task comments  ?>


<?php
if($this->CompanyUserRolePermission_id == 1 || $this->CompanyUserRolePermission_id == 2){
    if (isset($show_project_timer)  || ($this->uri->segment(1)=='project' && $this->uri->segment(2)) ) { ?>
        <script src="<?php echo base_url('assets/js/timer.js'); ?>"></script>

        <script type="text/javascript">
            $(document).ready(function(){

                var timer = '';

                $.post(base_url + 'project/get_active_timer', {project_id:<?php echo ($project_id) ? $project_id :''; ?>}, function(timer_response){
                    var response = JSON.parse(timer_response);
                    if (response.has_timer)
                    {
                        $('#navbar_timer').removeClass('hide');
                        button = '';

                        timer_offset = response.timer_offset;
                        timer = $('#project-timer').timer({startVal : timer_offset});
                        timer.startTimer();

                        button = '<a class="btn pro-countdown-timer btn-info" href="#" id="pro-start-stop" data-toggle="tooltip" title="Start / Stop recording"><i class="fa fa-stop"></i></a>';
                        $('.pro-start-stop-div').html(button);

                    }
                    else
                    {

                        timer = $('#project-timer').timer();

                    }
                });


                $(document).on('click', '.pro-countdown-timer', function(e) {
                    id = $(this).attr('id');
                    e.preventDefault();
                    $('.error').remove();
                    toggle_timer(timer, id);
                });


                function toggle_timer(timer, id)
                {

                    if(id == 'pro-start-timer')
                    {

                        $.post(base_url + 'project/project_timer', {start_timer : 'true',project_id:<?php echo ($project_id) ? $project_id :null; ?>}, function(timer_response){
                            var response = JSON.parse(timer_response);
                            if (response.success)
                            {
                                $('#navbar_timer').removeClass('hide');
                                timer.startTimer();
                                button = '';

                                button = '<a class="btn pro-countdown-timer btn-info" href="#" id="pro-stop-timer" data-toggle="tooltip" title="Stop recording"><i class="fa fa-stop"></i></a>';

                                $('.pro-start-stop-div').html(button);
                            }
                            else {
                                alert(response.error);
                            }
                        });
                    }
                    else
                    {
                        $.post(base_url + 'project/project_timer', {project_id :<?php echo ($project_id) ? $project_id :null; ?>, start_timer : 'false'}, function(timer_response){
                            var response = JSON.parse(timer_response);
                            if (response.success)
                            {
                                $('#navbar_timer').addClass('hide');
                                button = '';
                                button = '<a class="btn pro-countdown-timer btn-danger" href="#" id="pro-start-timer" data-toggle="tooltip" title="Start recording"><i class="fa fa-play"></i></a>';

                                $('.pro-start-stop-div').html(button);
                                timer.resetTimer();
                                // window.location.reload(true);
                            }
                            else {
                                alert(response.error);
                            }
                        });
                    }

                }
            });

        </script>
    <?php }}//end project timer  ?>

<?php if (isset($show_task_timer)) { ?>
    <script src="<?php echo base_url('assets/plugins/easypiechart/jquery.easy-pie-chart.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/timer.js'); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function(){

            $('.widget-easy-pie-3').easyPieChart({
                easing: 'easeOutBounce',
                onStep: function(from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                },
                barColor: "#07cafa",
                lineWidth:10,
                lineCap:'butt',
                scaleLength:20,
                size:'146',
                scaleColor:false,
                trackColor: "#CCC"
            });

            var timer = '';
            $.post(base_url + 'project/tasks/get_active_timer', {task_id : <?php echo ($task_details->task_id) ? $task_details->task_id :null; ?>,project_id:<?php echo ($task_details->project_id) ? $task_details->project_id :null; ?>}, function(timer_response){
                var response = JSON.parse(timer_response);
                if (response.has_timer)
                {
                    button = '';

                    timer_offset = response.timer_offset;
                    timer = $('.timer-clock').timer({startVal : timer_offset});
                    timer.startTimer();

                    button = '<li><a class="countdown-timer" href="#" id="stop-timer"><i class="fa fa-stop"></i></a></li>';

                    $('.start-stop-div').html(button);

                }
                else
                {
                    timer = $('.timer-clock').timer();
                }
            });


            $(document).on('click', '.countdown-timer', function(e) {
                id = $(this).attr('id');
                e.preventDefault();
                $('.error').remove();
                toggle_timer(timer, id);
            });


            function toggle_timer(timer, id)
            {

                if(id == 'start-timer')
                {

                    $.post(base_url + 'project/tasks/task_timer', {task_id : <?php echo ($task_details->task_id) ? $task_details->task_id :null; ?>, start_timer : 'true',project_id:<?php echo ($task_details->project_id) ? $task_details->project_id :null; ?>}, function(timer_response){
                        var response = JSON.parse(timer_response);
                        if (response.success)
                        {
                            timer.startTimer();
                            button = '';

                            button = '<li><a class="countdown-timer" href="#" id="stop-timer"><i class="fa fa-stop"></i></a></li>';

                            $('.start-stop-div').html(button);
                        }
                        else {
                            alert(response.error);
                        }
                    });
                }
                else
                {
                    $.post(base_url + 'project/tasks/task_timer', {project_id :<?php echo ($task_details->project_id) ? $task_details->project_id :null; ?>, start_timer : 'false', task_id : <?php echo ($task_details->task_id) ? $task_details->task_id :null; ?>}, function(timer_response){
                        var response = JSON.parse(timer_response);
                        if (response.success)
                        {

                            button = '';

                            button = '<li><a class="countdown-timer" href="#" id="start-timer"><i class="fa fa-play"></i></a></li>';

                            $('.start-stop-div').html(button);
                            timer.resetTimer();
                            // window.location.reload(true);
                        }
                        else {
                            alert(response.error);
                        }
                    });
                }

            }
        });

    </script>
<?php }//end task timer  ?>

<?php if (isset($show_roles_edit)){ ?>
    <script type='text/javascript'>

        $(document).ready(function()
        {
            $(".module_checkboxes").change(function()
            {
                if ($(this).prop('checked'))
                {
                    $(this).parent().find('input[type=checkbox]').not(':disabled').prop('checked', true);
                }
                else
                {
                    $(this).parent().find('input[type=checkbox]').not(':disabled').prop('checked', false);
                }
            });

        });


    </script>
<?php }//end roles edit.. ?>

<?php if (isset($show_task_check_list)) { ?>

    <script type="text/javascript">
        jQuery(document).ready(function() {

            if(use_check_list == 'true') {
                init_tasks_checklist_items(true);
            }

            $('body').on('change', 'input[name="checklist-box"]', function() {
                var checked = $(this).prop('checked');
                if (checked == true) {
                    val = 1;
                } else {
                    val = 0;
                }
                var listid = $(this).parents('.checklist').data('checklist-id');
                $.get(base_url + 'project/tasks/completed_list/' + listid + '/' + val);
                recalculate_checklist_items_progress();
                init_tasks_checklist_items(true);

            });


            $('body').on('blur', 'textarea[name="checklist-description"]', function() {
                var description = $(this).val();
                var listid = $(this).parents('.checklist').data('checklist-id');
                $.post(base_url + 'project/tasks/update_checklist', {
                    description: description,
                    listid: listid
                });
            });

            $(document).on('click', '.add_new_checklist', function(e) {
                $.post(base_url + 'project/tasks/add_checklist', {
                    task_id: task_id
                }).success(function () {
                    init_tasks_checklist_items(true);
                });
            });

            $(document).on('click', '.remove-checklist', function(e) {
                if ($(this).attr('data-id').length ) {
                    $.get(base_url + 'project/tasks/delete_checklist/' + $(this).attr('data-id'), function(response) {
                        if (response.success == true) {
                            $(this).parents('.checklist').remove();
                            recalculate_checklist_items_progress();
                            init_tasks_checklist_items(true);
                        }
                    }, 'json');

                }
            });

            function init_tasks_checklist_items(is_new) {
                $.post(base_url + 'project/tasks/init_checklist', {
                    task_id: task_id
                }).success(function(data) {
                    $('#checklist-items').html(data);
                    if (typeof(is_new) != 'undefined') {
                        $('body').find('.checklist textarea').eq(0).focus();
                    }

                });
            }


            function recalculate_checklist_items_progress() {
                var total_finished = $('input[name="checklist-box"]:checked').length;
                var total_checklist_items = $('input[name="checklist-box"]').length;

                var percent = 0;

                if(total_checklist_items == 0){
                    $('body').find('.chk-heading').remove();
                }

                if (total_checklist_items > 2) {
                    percent = (total_finished * 100) / total_checklist_items;
                } else {
                    $('.task-progress-bar').parents('.progress').addClass('hide');
                    return false;
                }
                $.get(base_url + 'project/tasks/update_task_progress/' + task_id + '/' + percent);
                $('.task-progress-bar').css('width', percent.toFixed(2) + '%');
                $('.task-progress-bar').text(percent.toFixed(2) + '%');
            }


        });
    </script>
<?php } ?>

<?php if (isset($show_emails)) { ?>
    <script type="text/javascript">
        $(document).ready(function(){


            if($('#smtp').is(':checked')){
                $('#configuration_smtp').show();
            }

            if($("#use_test_mail").is(":checked")) {
                $("#test_mail_config").show("fast");
            }

            $('#smtp').click(function (e) {
                e.stopPropagation();
                $('#configuration_smtp').show('slide', {direction: 'up'}, 1400);

            });
            $('#php_mail').click(function (e) {
                e.stopPropagation();
                $('#configuration_smtp').hide();
            });


            $('#never_send').click(function (e) {
                e.stopPropagation();
                $('#configuration_smtp').hide();
            });


            $('#use_test_mail').change(function(){
                //if checked
                if($("#use_test_mail").is(":checked")){
                    $("#test_mail_config").show("fast");
                }else{
                    $("#test_mail_config").hide("fast");
                }
            });
        });
    </script>

<?php }//end emails?>
