<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/02/10
 * Time: 11:45 AM
 */

$this->load->view('components/_header');
$this->load->view($content);
?>
    <br /><br />
    <div class="modal-footer">
        Zestpro &copy; <?php echo date('Y'); ?>
    </div>
<?php
$this->load->view('components/_footer');