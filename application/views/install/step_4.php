<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/06/29
 * Time: 10:33 AM
 */
?>
<div class="container install-container">
    <header>
        <div class="row">
            <div class="col-sm-6">
                <h1 class="pull-left">4<small>/5</small></h1>
                <h3><?php echo 'Evanto Account'; ?><br><small><?php echo 'Purchase Code Settings'; ?></small></h3>
            </div>
            <div class="col-sm-6">
                <div id="logo" class="pull-right hidden-xs">
                    <img src="<?php echo base_url('files/company_logo/company_logo.png'); ?>" alt="ZestPro" title="ZestPro" />
                </div>
            </div>
        </div>
    </header>
    <?php
    if($this->session->flashdata('message')){ ?>
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="fa fa-info-sign"></i><?=$this->session->flashdata('message')?>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-sm-9">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                <p><?php echo 'Evanto Account'; ?></p>
                <fieldset>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label"><?php echo 'Envato Username'; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="evanto_username" class="form-control" />

                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label"><?php echo 'Purchase Code'; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="purchase_code" placeholder="xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"  class="form-control" />

                        </div>
                    </div>


                </fieldset>
                <div class="buttons">
                    <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                    <div class="pull-right">
                        <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-3">
            <ul class="list-group">
                <li class="list-group-item"><?php echo 'Licence'; ?></li>
                <li class="list-group-item"><?php echo "System Check"; ?></li>
                <li class="list-group-item"><?php echo 'Configuration'; ?></li>
                <li class="list-group-item"><b><?php echo 'Envato Account'; ?></b></li>
                <li class="list-group-item"><?php echo 'Company Settings'; ?></li>
            </ul>
        </div>
    </div>
</div>