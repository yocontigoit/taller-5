<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/06/29
 * Time: 10:31 AM
 */
?>
<div class="container install-container">
    <header>
        <div class="row">
            <div class="col-sm-6">
                <h1 class="pull-left">2<small>/5</small></h1>
                <h3><?php echo 'Pre-Installation'; ?><br><small><?php echo 'Check your server is set-up correctly'; ?></small></h3>
            </div>
            <div class="col-sm-6">
                <div id="logo" class="pull-right hidden-xs">
                    <img src="<?php echo base_url('files/company_logo/company_logo.png'); ?>" alt="ZestPro" title="ZestPro" />
                </div>
            </div>
        </div>
    </header>
    <?php if($this->session->flashdata('message')){ ?>
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <i class="fa fa-info-sign"></i><?=$this->session->flashdata('message')?>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-sm-9">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <p><?php echo '1. Please configure your PHP settings to match requirements listed below.'; ?></p>
                <fieldset>
                    <table class="table">
                        <thead>
                        <tr>
                            <td width="35%"><b><?php echo 'PHP Settings'; ?></b></td>
                            <td width="25%"><b><?php echo 'Current Settings'; ?></b></td>
                            <td width="25%"><b><?php echo 'Required Settings'; ?></b></td>
                            <td width="15%" class="text-center"><b><?php echo 'Status'; ?></b></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php echo 'PHP Version'; ?></td>
                            <td><?php echo $php_version; ?></td>
                            <td>5.3+</td>
                            <td class="text-center"><?php if ($php_version >= '5.3') { ?>
                                    <span class="text-success"><i class="fa fa-check-circle"></i></span>
                                <?php } else { ?>
                                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo 'Register Globals'; ?></td>
                            <td><?php if ($register_globals) { ?>
                                    <?php echo 'On'; ?>
                                <?php } else { ?>
                                    <?php echo 'Off'; ?>
                                <?php } ?>
                            </td>
                            <td><?php echo 'Off'; ?></td>
                            <td class="text-center"><?php if (!$register_globals) { ?>
                                    <span class="text-success"><i class="fa fa-check-circle"></i></span>
                                <?php } else { ?>
                                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo 'Magic Quotes GPC'; ?></td>
                            <td><?php if ($magic_quotes_gpc) { ?>
                                    <?php echo 'On'; ?>
                                <?php } else { ?>
                                    <?php echo 'Off'; ?>
                                <?php } ?>
                            </td>
                            <td><?php echo 'Off'; ?></td>
                            <td class="text-center"><?php if (!$magic_quotes_gpc) { ?>
                                    <span class="text-success"><i class="fa fa-check-circle"></i></span>
                                <?php } else { ?>
                                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo 'File Uploads'; ?></td>
                            <td><?php if ($file_uploads) { ?>
                                    <?php echo 'On'; ?>
                                <?php } else { ?>
                                    <?php echo 'Off'; ?>
                                <?php } ?>
                            </td>
                            <td><?php echo 'On'; ?></td>
                            <td class="text-center"><?php if ($file_uploads) { ?>
                                    <span class="text-success"><i class="fa fa-check-circle"></i></span>
                                <?php } else { ?>
                                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo 'Session Auto Start'; ?></td>
                            <td><?php if ($session_auto_start) { ?>
                                    <?php echo 'On'; ?>
                                <?php } else { ?>
                                    <?php echo 'Off'; ?>
                                <?php } ?>
                            </td>
                            <td><?php echo 'Off'; ?></td>
                            <td class="text-center"><?php if (!$session_auto_start) { ?>
                                    <span class="text-success"><i class="fa fa-check-circle"></i></span>
                                <?php } else { ?>
                                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                                <?php } ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </fieldset>
                <p><?php echo '2. Please make sure the PHP extensions listed below are installed.'; ?></p>
                <fieldset>
                    <table class="table">
                        <thead>
                        <tr>
                            <td width="35%"><b><?php echo 'Extension Settings'; ?></b></td>
                            <td width="25%"><b><?php echo 'Current Settings'; ?></b></td>
                            <td width="25%"><b><?php echo 'Required Settings'; ?></b></td>
                            <td width="15%" class="text-center"><b><?php echo 'Status'; ?></b></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php echo 'Database'; ?></td>
                            <td><?php if ($db) { ?>
                                    <?php echo 'On'; ?>
                                <?php } else { ?>
                                    <?php echo 'Off'; ?>
                                <?php } ?>
                            </td>
                            <td><?php echo 'On'; ?></td>
                            <td class="text-center"><?php if ($db) { ?>
                                    <span class="text-success"><i class="fa fa-check-circle"></i></span>
                                <?php } else { ?>
                                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo 'GD'; ?></td>
                            <td><?php if ($gd) { ?>
                                    <?php echo 'On'; ?>
                                <?php } else { ?>
                                    <?php echo 'Off'; ?>
                                <?php } ?>
                            </td>
                            <td><?php echo 'On'; ?></td>
                            <td class="text-center"><?php if ($gd) { ?>
                                    <span class="text-success"><i class="fa fa-check-circle"></i></span>
                                <?php } else { ?>
                                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo 'cURL'; ?></td>
                            <td><?php if ($curl) { ?>
                                    <?php echo 'On'; ?>
                                <?php } else { ?>
                                    <?php echo 'Off'; ?>
                                <?php } ?>
                            </td>
                            <td><?php echo 'On'; ?></td>
                            <td class="text-center"><?php if ($curl) { ?>
                                    <span class="text-success"><i class="fa fa-check-circle"></i></span>
                                <?php } else { ?>
                                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo 'mCrypt'; ?></td>
                            <td><?php if ($mcrypt_encrypt) { ?>
                                    <?php echo 'On'; ?>
                                <?php } else { ?>
                                    <?php echo 'Off'; ?>
                                <?php } ?>
                            </td>
                            <td><?php echo 'On'; ?></td>
                            <td class="text-center"><?php if ($mcrypt_encrypt) { ?>
                                    <span class="text-success"><i class="fa fa-check-circle"></i></span>
                                <?php } else { ?>
                                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo 'ZLIB'; ?></td>
                            <td><?php if ($zlib) { ?>
                                    <?php echo 'On'; ?>
                                <?php } else { ?>
                                    <?php echo 'Off'; ?>
                                <?php } ?>
                            </td>
                            <td><?php echo 'On'; ?></td>
                            <td class="text-center"><?php if ($zlib) { ?>
                                    <span class="text-success"><i class="fa fa-check-circle"></i></span>
                                <?php } else { ?>
                                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo 'ZIP'; ?></td>
                            <td><?php if ($zip) { ?>
                                    <?php echo 'On'; ?>
                                <?php } else { ?>
                                    <?php echo 'Off'; ?>
                                <?php } ?>
                            </td>
                            <td><?php echo 'On'; ?></td>
                            <td class="text-center"><?php if ($zip) { ?>
                                    <span class="text-success"><i class="fa fa-check-circle"></i></span>
                                <?php } else { ?>
                                    <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php if (!$iconv) { ?>
                            <tr>
                                <td><?php echo $text_mbstring; ?></td>
                                <td>
                                    <?php if ($mbstring) { ?>
                                        <?php echo 'On'; ?>
                                    <?php } else { ?>
                                        <?php echo 'Off'; ?>
                                    <?php } ?>
                                </td>
                                <td><?php echo 'On'; ?></td>
                                <td class="text-center">
                                    <?php if ($mbstring) { ?>
                                        <span class="text-success"><i class="fa fa-check-circle"></i></span>
                                    <?php } else { ?>
                                        <span class="text-danger"><i class="fa fa-minus-circle"></i></span>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </fieldset>

                <p><?php echo '3. Please make sure you have set the correct permissions on the directories list below.'; ?></p>
                <fieldset>
                    <table class="table">
                        <thead>
                        <tr>
                            <td align="left"><b><?php echo 'Directories'; ?></b></td>
                            <td align="left"><b><?php echo 'Status'; ?></b></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php echo 'files' . '/'; ?></td>
                            <td><?php if (is_writable('files')) { ?>
                                    <span class="text-success"><?php echo 'Writable'; ?></span>
                                <?php } else { ?>
                                    <span class="text-danger"><?php echo 'Un-Writable'; ?></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo 'Updates' . '/'; ?></td>
                            <td><?php if (is_writable('files/updates')) { ?>
                                    <span class="text-success"><?php echo 'Writable'; ?></span>
                                <?php } else { ?>
                                    <span class="text-danger"><?php echo 'Un-Writable'; ?></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo 'Backup' . '/'; ?></td>
                            <td><?php if (is_writable('files/backup')) { ?>
                                    <span class="text-success"><?php echo 'Writable'; ?></span>
                                <?php } else { ?>
                                    <span class="text-danger"><?php echo 'Un-Writable'; ?></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo 'Archive' . '/'; ?></td>
                            <td><?php if (is_writable('files/archive')) { ?>
                                    <span class="text-success"><?php echo 'Writable'; ?></span>
                                <?php } else { ?>
                                    <span class="text-danger"><?php echo 'Un-Writable'; ?></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo 'Temp' . '/'; ?></td>
                            <td><?php if (is_writable('files/temp')) { ?>
                                    <span class="text-success"><?php echo 'Writable'; ?></span>
                                <?php } else { ?>
                                    <span class="text-danger"><?php echo 'Un-Writable'; ?></span>
                                <?php } ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </fieldset>
                <div class="buttons">
                    <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                    <div class="pull-right">
                        <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-3">
            <ul class="list-group">
                <li class="list-group-item">Licence</li>
                <li class="list-group-item"><b>System Check</b></li>
                <li class="list-group-item">Configuration</li>
                <li class="list-group-item">Envato Account</li>
                <li class="list-group-item">Company Settings</li>
            </ul>
        </div>
    </div>
</div>