<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/06/29
 * Time: 10:33 AM
 */
?>
<div class="container install-container">
    <header>
        <div class="row">
            <div class="col-sm-6">
                <h1 class="pull-left">3<small>/5</small></h1>
                <h3><?php echo 'Database Settings'; ?><br><small><?php echo 'Database Settings'; ?></small></h3>
            </div>
            <div class="col-sm-6">
                <div id="logo" class="pull-right hidden-xs">
                    <img src="<?php echo base_url('files/company_logo/company_logo.png'); ?>" alt="ZestPro" title="ZestPro" />
                </div>
            </div>
        </div>
    </header>
    <?php
    if($this->session->flashdata('message')){ ?>
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="fa fa-info-sign"></i><?=$this->session->flashdata('message')?>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-sm-9">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                <p><?php echo 'Database Connection'; ?></p>
                <fieldset>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-db-hostname"><?php echo 'Database Host'; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="hostname"  id="input-db-hostname" class="form-control" required/>

                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label"><?php echo 'Username'; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="db_username"   class="form-control" required />

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" ><?php echo 'Database Password'; ?></label>
                        <div class="col-sm-10">
                            <input type="password" name="db_password"  class="form-control" />
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label"><?php echo 'Database'; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="db_database"  class="form-control" required />

                        </div>
                    </div>

                </fieldset>
                <div class="buttons">
                    <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                    <div class="pull-right">
                        <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-3">
            <ul class="list-group">
                <li class="list-group-item">Licence</li>
                <li class="list-group-item">System Check</li>
                <li class="list-group-item"><b>Configuration</b></li>
                <li class="list-group-item">Envato Account</li>
                <li class="list-group-item">Company Settings</li>
            </ul>
        </div>
    </div>
</div>