<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/06/29
 * Time: 10:28 AM
 */
?>
<style>
    .terms {
        background: #ffffff none repeat scroll 0 0;
        border: 1px solid #dbdbdb;
        height: 250px;
        margin: 3px 0 10px;
        overflow-y: scroll;
        padding: 4px;
    }
    .terms p, .terms ul, .terms ol {
        color: #000000;
    }
</style>
<div class="container install-container">
    <header>
        <div class="row">
            <div class="col-sm-6">
                <h1 class="pull-left">1<small>/5</small></h1>
                <h3><br></h3>
            </div>
            <div class="col-sm-6">
                <div id="logo" class="pull-right hidden-xs"> <img src="<?php echo base_url('files/company_logo/company_logo.png'); ?>" alt="ZestPro" title="ZestPro" /> </div>
            </div>
        </div>
    </header>
    <div class="row">
        <div class="col-sm-9">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div class="terms">
                    <h1>Welcome!</h1>
                    <br />
                    <p>Thank you for purchasing zestpro!.</p>
                    <h3>Please Note:</h3>
                    <p>We will only be able to provide support to verified buyers.</p>
                    <p>For support email:<a href="mailto:&#115;&#117;&#112;&#112;&#111;&#114;&#116;&#64;&#97;&#112;&#112;&#115;&#100;&#121;&#110;&#101;&#46;&#99;&#111;&#109;">&#115;&#117;&#112;&#112;&#111;&#114;&#116;&#64;&#97;&#112;&#112;&#115;&#100;&#121;&#110;&#101;&#46;&#99;&#111;&#109;</a></p>
                    <p style="color:#B34E00;">More updates coming your way soon!</p>

                </div>
                <div class="buttons">
                    <div class="pull-right">
                        <input type="submit" value="Next" class="btn btn-primary" />
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-3">
            <ul class="list-group">
                <li class="list-group-item"><b>Licence</b></li>
                <li class="list-group-item">System Check</li>
                <li class="list-group-item">Configuration</li>
                <li class="list-group-item">Envato Account</li>
                <li class="list-group-item">Company Settings</li>
            </ul>
        </div>
    </div>
</div>