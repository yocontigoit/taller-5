<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/06/29
 * Time: 10:33 AM
 */
?>
<div class="container install-container">
    <header>
        <div class="row">
            <div class="col-sm-6">
                <h1 class="pull-left">5<small>/5</small></h1>
                <h3><?php echo 'Basic Settings'; ?><br><small><?php echo 'Database Settings'; ?></small></h3>
            </div>
            <div class="col-sm-6">
                <div id="logo" class="pull-right hidden-xs">
                    <img src="<?php echo base_url('files/company_logo/company_logo.png'); ?>" alt="ZestPro" title="ZestPro" />
                </div>
            </div>
        </div>
    </header>
    <?php if($this->session->flashdata('message')){ ?>
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <i class="fa fa-info-sign"></i><?=$this->session->flashdata('message')?>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-sm-9">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                <p><?php echo 'Evanto Account'; ?></p>
                <fieldset>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">First Name</label>
                        <div class="col-lg-7">
                            <input type="text" class="form-control" placeholder="John" name="first_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Surname</label>
                        <div class="col-lg-7">
                            <input type="text" class="form-control" placeholder="Doe" name="last_name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Admin Username</label>
                        <div class="col-lg-7">
                            <input type="text" class="form-control" placeholder="John" name="username">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Admin Password</label>
                        <div class="col-lg-7">
                            <input type="password" class="form-control" placeholder="password" name="password">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-3 control-label">Email</label>
                        <div class="col-lg-7">
                            <input type="email" class="form-control" placeholder="admin@demo.com" name="email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Company Name</label>
                        <div class="col-lg-7">
                            <input type="text" class="form-control" placeholder="demo" name="company_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Address 1</label>
                        <div class="col-lg-7">
                            <input type="text" class="form-control" placeholder="Mars" name="address_1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Address 2</label>
                        <div class="col-lg-7">
                            <input type="text" class="form-control" placeholder="Galaxy" name="address_2">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Company Website</label>
                        <div class="col-lg-7">
                            <input type="text" class="form-control" placeholder="www.demo.com" name="company_website">
                        </div>
                    </div>

                </fieldset>
                <div class="buttons">
                    <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                    <div class="pull-right">
                        <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-3">
            <ul class="list-group">
                <li class="list-group-item"><?php echo 'Licence'; ?></li>
                <li class="list-group-item"><?php echo "System Check"; ?></li>
                <li class="list-group-item"><?php echo 'Configuration'; ?></li>
                <li class="list-group-item"><?php echo 'Envato Account'; ?></li>
                <li class="list-group-item"><b><?php echo 'Company Settings'; ?></b></li>
            </ul>
        </div>
    </div>
</div>