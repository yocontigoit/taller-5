<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/11/18
 * Time: 11:15 AM
 */

class MY_Controller extends CI_Controller
{

	public $CompanyUser_id = -1;
	public $CompanyClient_user_id = -1;
    public $CompanyClient_id = -1;
	public $Company_id = -1;
    public $CompanyUser_type = -1;
	public $CompanyUserRolePermission_id =  -1;
	public $CompanyUserName = null;
    public $CompanyUser_FullName = null;
    public $CompanyUser_avatar = null;
    public $CompanyUser_email = null;
    public $CompanyClient_email = null;
	public $data = array();

	public function __construct()
	{		
		parent::__construct();


        //check if a company user is logged in
        if ($this->CompanyUser_id == -1 && $this->session->userdata('user_id')){
            $this->load->library('gravatar');
			$this->CompanyUser_id = $this->session->userdata('user_id');
            $this->CompanyUser_email = $this->session->userdata('email');
			$this->CompanyUserRolePermission_id = $this->session->userdata('role_id');
			$this->CompanyUser_type  = $this->session->userdata('user_type');
            $this->CompanyUserName = $this->session->userdata('username');
            $this->CompanyUser_FullName = $this->session->userdata('fullname');
            $this->Company_id = $this->session->userdata('company_id');
            $this->CompanyUser_avatar = $this->session->userdata('user_avatar');
            if(!$this->CompanyUser_avatar){
                $this->CompanyUser_avatar = $this->gravatar->get($this->CompanyUser_email);
            }
			$this->set_online();
		}

        //check if a client is logged in
        if ($this->CompanyClient_user_id == -1 && $this->session->userdata('client_users_id')){
            $this->load->library('gravatar');
			$this->CompanyClient_user_id = $this->session->userdata('client_users_id');
            $this->CompanyClient_id = $this->session->userdata('client_id');
			$this->CompanyUserRolePermission_id = $this->session->userdata('role_id');
			$this->CompanyUser_type   = $this->session->userdata('user_type');
            $this->CompanyUserName = $this->session->userdata('client_username');
            $this->CompanyUser_FullName = $this->session->userdata('client_username')." ".$this->session->userdata('client_surname');
            $this->CompanyUser_avatar = $this->session->userdata('client_avatar');
            $this->CompanyClient_email = $this->session->userdata('client_email');
            $this->Company_id = $this->session->userdata('company_id');
            if(!$this->CompanyUser_avatar){
                $this->CompanyUser_avatar = $this->gravatar->get($this->CompanyClient_email);
            }
			$this->set_online();
		}

        $this->set_lang();

	}


    //change application language
	protected function set_lang()
	{		

           $loaded = array('common', 'messages', 'module', 'zest');
    	   	if($this->input->get("lang")){

	        	$this->config->set_item("lang",strtolower($this->input->get("lang")));
	        	$this->session->set_userdata('lang',strtolower($this->input->get("lang")));

                foreach ($loaded as $file) {
                    $this->lang->load($file,strtolower($this->input->get("lang")));
                }
                redirect($_SERVER['HTTP_REFERER']);

	        }else {

	        	if($this->session->userdata('lang') != '') {
                    foreach ($loaded as $file) {
                        $this->lang->load($file, strtolower($this->session->userdata('lang')));
                    }
                }else{
                    $this->config->set_item("lang", 'english');
                    $this->session->set_userdata('lang', 'english');

                    foreach ($loaded as $file) {
                        $this->lang->load($file, strtolower($this->session->userdata('lang')));
                    }
                }
	        }
	}

    
    //set logged users online
	protected function set_online()
	{
        ($this->CompanyUser_id > 0 ) ? $this->admin_m->setUsersOnline(intval($this->CompanyUser_id)) : $this->admin_m->setClientsOnline(intval($this->CompanyClient_user_id));
	}




}
/* End of file my_controller.php */

