<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/02
 * Time: 08:40 AM
 */
class MY_Lang extends MX_Lang{

    function __construct()
    {
        parent::__construct();
    }

    function switch_to($idiom)
    {
        $CI = &get_instance();
        if (is_string($idiom)) {
            $CI->config->set_item('language', $idiom);
            $loaded = $this->is_loaded;
            $this->is_loaded = array();

            foreach ($loaded as $file) {
                $this->load(str_replace('_lang.php', '', $file));
            }
        }
    }
}