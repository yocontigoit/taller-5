<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
if (!class_exists('MX_Loader', false)) {
    require APPPATH.'third_party/MX/Loader.php';
}
//require APPPATH."third_party/MX/Loader.php";

class MY_Loader extends MX_Loader {}