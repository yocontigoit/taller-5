<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/06/29
 * Time: 10:50 AM
 */
class Installer extends CI_Controller {


    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','file'));
    }

    /**
     * Load setup step one
     */
    function index()
    {
        $this->data['action'] = null;
        $this->session->sess_destroy();
        $this->data['content'] = 'install/step_1';
        $this->data['action'] = site_url('installer/step_2');
        $this->load->view('_install_layout', $this->data);

    }

    /**
     * Load setup step two
     */
    function step_2(){
        $this->data['action'] = null;
        $this->data['back'] = 'index';
        $this->data['button_continue'] = 'Continue';
        $this->data['button_back'] = 'Back';


        $this->data['php_version'] = phpversion();
        $this->data['register_globals'] = ini_get('register_globals');
        $this->data['magic_quotes_gpc'] = ini_get('magic_quotes_gpc');
        $this->data['file_uploads'] = ini_get('file_uploads');
        $this->data['session_auto_start'] = ini_get('session_auto_start');

        if (!array_filter(array('mysql', 'mysqli', 'pgsql', 'pdo'), 'extension_loaded')) {
            $this->data['db'] = false;
        } else {
            $this->data['db'] = true;
        }

        $this->data['gd'] = extension_loaded('gd');
        $this->data['curl'] = extension_loaded('curl');
        $this->data['mcrypt_encrypt'] = function_exists('mcrypt_encrypt');
        $this->data['zlib'] = extension_loaded('zlib');
        $this->data['zip'] = extension_loaded('zip');
        $this->data['iconv'] = function_exists('iconv');
        $this->data['mbstring'] = extension_loaded('mbstring');

        $this->data['action'] = site_url('installer/step_3');
        $this->data['content'] = '/install/step_2';
        $this->load->view('_install_layout', $this->data);
    }

    function step_3()
    {

        $this->data['action'] = null;
        $this->data['back'] = 'step_2';
        $this->data['button_continue'] = 'Continue';
        $this->data['button_back'] = 'Back';

        $this->data['action'] = site_url('installer/step_4');
        $this->data['content'] = 'install/step_3';
        $this->load->view('_install_layout', $this->data);
    }

    function step_4()
    {
        $this->data['action'] = null;
        $this->data['back'] = 'step_3';
        $this->data['button_continue'] = 'Continue';
        $this->data['button_back'] = 'Back';

        if($this->session->userdata('step_3_complete') != 'complete'){
            $this->data['action'] = site_url('installer/step_3');
            $this->data['content'] = 'install/step_4';
        }else{
            $this->data['action'] = site_url('installer/step_4');
            $this->data['content'] = 'install/step_3';
        }

        if($this->input->post()){

            $db_connect = $this->verify_db_connection();

            if($db_connect){

               $this->_create_db_config();
                $this->session->set_userdata('step_3_complete','complete');

                $this->data['action'] = 'step_5';
                $this->data['content'] = 'install/step_4';
            }else {
                $this->session->set_flashdata('message','Database connection failed please try again.');
                $this->data['action'] = 'step_4';
                $this->data['content'] = 'install/step_3';
            }
        }
        $this->load->view('_install_layout', $this->data);
    }

    function step_5()
    {
        $this->data['action'] = null;
        $this->data['back'] = 'step_4';
        $this->data['button_continue'] = 'Continue';
        $this->data['button_back'] = 'Back';

        $this->data['action'] = site_url('installer/complete');
        $this->data['content'] = 'install/step_5';

        if($this->session->userdata('step_3_complete') != 'complete'){
            $this->data['action'] = site_url('installer/step_5');
            $this->data['content'] = 'install/step_4';
        }

        if($this->input->post()){

            $this->session->set_userdata('purchase_code', $this->input->post('envato_license'));

                if(!$this->_initialize_db()){
                    $this->session->set_flashdata('message','Database import failed. Check the resource/tmp/install.sql');
                    $this->data['action'] = site_url('installer/step_5');
                    $this->data['content'] = 'install/step_4';
                }
                $this->session->set_userdata('step_4_complete','complete');
        }
        $this->load->view('_install_layout', $this->data);
    }


    /**
     * complete setup wizard
     */
    function complete(){

        $this->_enable_system_access();

        $this->_create_company();

        $this->_update_system_settings();

        $this->_create_admin_account();

        $this->_change_routing();

        $this->_change_htaccess();

        $this->session->sess_destroy();

        if(is_file('./files/temp/install.sql')){
            unlink('./files/temp/install.sql');
        }
        redirect('');
    }

    /**
     * write database credentials to config file
     */
    function _create_db_config(){
        $file_db = "./application/config/database.php";
        $content_db = file_get_contents($file_db);
        $content_db = str_ireplace("#hostname#", $this->input->post('hostname'),  $content_db);
        $content_db = str_ireplace("#username#", $this->input->post('db_username'),  $content_db);
        $content_db = str_ireplace("#password#", $this->input->post('db_password'),  $content_db);
        $content_db = str_ireplace("#database#", $this->input->post('db_database'),  $content_db);
        file_put_contents($file_db, $content_db);

    }


    /**
     * set company details
     */
    function _create_company(){
        $this->load->database();
           $company_name = $this->input->post('company_name');
            $company_owner= $this->input->post('first_name').' '.$this->input->post('last_name');
            $company_address_1 =$this->input->post('address_1');
            $company_address_2 = $this->input->post('address_2');
            $company_email = $this->input->post('email');


        $company_param = array(
          'company_name'=>$company_name,
          'company_owner'=>$company_owner,
          'company_address_1'=> $company_address_1,
          'company_address_2'=> $company_address_2,
          'company_email'=> $company_email
        );

         $this->ion_auth->update_company_settings($company_param);

    }

    /**
     * update purchase code(not a must at ths stage)
     */
    function _update_system_settings(){

        $purchase_code = $this->session->userdata('purchase_code');
                $param = array(
                    'purchase_code'=>$purchase_code
            );

        $this->ion_auth->update_system_settings($param);
    }

    /**
     * create mail user or admin account
     */
    function _create_admin_account(){
        $this->load->database();
        $this->load->library('ion_auth');

        // Prepare system settings
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('first_name');

        $company_address_1 =$this->input->post('address_1');
        $company_address_2 = $this->input->post('address_2');
        $company_email = $this->input->post('email');

        $username = $this->input->post('username');
        $password = $this->input->post('password');



        $user_data = array(
            'emp_name' => $first_name,
            'emp_surname' => $last_name,
            'company_id' =>1,
            'emp_address_1' => $company_address_1,
            'emp_address_2' => $company_address_2,
            'emp_avatar' => 'default.jpg'
        );

        $user_id = $this->ion_auth->add_employee($user_data,$company_email);
        $user_login_data = array(
            'username' => $username,
            'email' => $company_email,
            'role_id' => 1,
            'password' => $password,
            'active' => 1
        );
        if($user_id) {
            $this->ion_auth->save_user($user_id, $user_login_data, false);
        }
        
    }

    /**
     * create database tables
     * @return bool
     */
    function _initialize_db(){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit','2048M');
        $this->output->enable_profiler(TRUE);
        $this->load->database();
        $this->load->dbforge();
        if(file_exists("files/db/ini_install.sql")) {
            $file_content = file_get_contents('files/db/ini_install.sql');

            $this->db->query('USE ' . $this->db->database . ';');
           $this->db->query('SET foreign_key_checks = 0');
            foreach (explode(";\n", $file_content) as $sql) {
                $sql = trim($sql);
                if ($sql) {
                    $this->db->query($sql);
                }
            }
            $this->db->query('SET foreign_key_checks = 1');
            return TRUE;
        }
        return FALSE;

    }

    function _enable_system_access(){


        $lib_data = "./application/config/autoload.php";
        $content_lib = file_get_contents($lib_data);
        $content_lib = str_replace(
            '$autoload[\'libraries\'] = array(\'user_agent\',\'ion_auth\');',
            '$autoload[\'libraries\'] = array(\'user_agent\',\'ion_auth\',\'database\',\'notifications\');',
            $content_lib);
        file_put_contents($lib_data, $content_lib);


        $conf_data = "./application/config/config.php";
        $content_config = file_get_contents($conf_data);
        $content_config = str_ireplace('$config[\'enable_hooks\'] = FALSE;','$config[\'enable_hooks\'] = TRUE;',  $content_config);
        
        $content_config = str_replace(
            '$config[\'index_page\'] = \'index.php\';',
            '$config[\'index_page\'] = \'\';',
            $content_config);
        
        file_put_contents($conf_data, $content_config);


    }

    /**
     * change app default routing
     * @return bool
     */
    function _change_routing(){
        // Replace the default routing controller
        $conf_data = "./application/config/routes.php";
        $content_config = file_get_contents($conf_data);
        $content_config = str_ireplace('installer','auth',  $content_config);
        file_put_contents($conf_data, $content_config);


        $data = 'Software installed';
        if (write_file('./files/temp/installed.txt', $data))
        {
            return TRUE;
        }
    }

    function _change_htaccess(){

        $subfolder = str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
        if(!empty($subfolder)){

            $input = '<IfModule mod_rewrite.c>
                        RewriteEngine On
                        RewriteBase '.$subfolder.'
                        RewriteCond %{REQUEST_URI} ^system.*
                        RewriteRule ^(.*)$ /index.php?/$1 [L]

                        RewriteCond %{REQUEST_URI} ^application.*
                        RewriteRule ^(.*)$ /index.php?/$1 [L]

                        RewriteCond %{REQUEST_FILENAME} !-f
                        RewriteCond %{REQUEST_FILENAME} !-d
                        RewriteRule ^(.*)$ index.php?/$1 [L]
                        </IfModule>

                        <IfModule !mod_rewrite.c>
                        ErrorDocument 404 /index.php
                        </IfModule>';

            $current = @file_put_contents('./.htaccess', $input);
        }
    }


    /*
     * Database validation
     */
    function verify_db_connection()
    {
        $link   =   @mysqli_connect(
            $this->input->post('hostname'),
            $this->input->post('db_username'),
            $this->input->post('db_password'),
            $this->input->post('db_database')
        );
        if(!$link)
        {
            @mysqli_close($link);
            return false;
        }

        @mysqli_close($link);
        return true;
    }

}
