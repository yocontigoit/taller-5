<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 1/23/2016
 * Time: 1:30 PM
 */

/**
 * converts html to pdf
 * @param $invoice_id
 * @param array $data
 * @param null $invoice_template
 * @param null $file_name
 * @param bool $stream
 * @param null $isGuest
 * @return string
 */
if (!function_exists('generate_invoice_pdf')) {
    function generate_invoice_pdf(
        $invoice_id,
        $data = array(),
        $invoice_template = null,
        $file_name = null,
        $stream = true,
        $isGuest = null
    ) {
        $CI = &get_instance();
        $invoice_template = 'zest';

        $html = $CI->load->view('templates/invoice_templates/pdf/' . $invoice_template, $data, true);

        $CI->load->helper('mpdf');

        return pdf_create($html, $file_name, $stream, '');
    }
}

/**
 * converts html to pdf
 * @param $invoice_id
 * @param array $data
 * @param null $invoice_template
 * @param null $file_name
 * @param bool $stream
 * @param null $isGuest
 * @return string
 */
if (!function_exists('generate_invoice_pdf_raya')) {
    function generate_invoice_pdf_raya(
        $invoice_id,
        $data = array(),
        $invoice_template = null,
        $file_name = null,
        $stream = true,
        $isGuest = null
    ) {
        $CI = &get_instance();
        $invoice_template = 'raya';

        $html = $CI->load->view('templates/invoice_templates/pdf/' . $invoice_template, $data, true);

        $CI->load->helper('mpdf');

        return pdf_create($html, $file_name, $stream, '');
    }
}

/**
 * converts html to pdf
 * @param $quote_id
 * @param array $data
 * @param null $quote_template
 * @param null $file_name
 * @return string
 */
if (!function_exists('generate_estimate_pdf')) {
    function generate_estimate_pdf($quote_id, $data = array(), $quote_template = null, $file_name = null)
    {
        $stream = false;
        $CI = &get_instance();

        $quote_template = 'default';
        $html = $CI->load->view('templates/estimate_templates/pdf/' . $quote_template, $data, true);

        $CI->load->helper('mpdf');

        //return pdf_create($html, lang('quote') . '_' . str_replace(array('\\', '/'), '_', $quote->quote_number), $stream,$quote->quote_password);
        return pdf_create($html, $file_name, $stream, '');
    }
}

if (!function_exists('generate_report_pdf')) {
    function generate_report_pdf($report_id, $data, $quote_template = null, $file_name = null)
    {
        $stream = false;
        $CI = &get_instance();
        $file_name = "Reporte";
        $quote_template = 'default';
        $html = $CI->load->view('templates/reporte_templates/pdf/' . $quote_template, $data, true);

        $CI->load->helper('mpdf');

        //return pdf_create($html, lang('quote') . '_' . str_replace(array('\\', '/'), '_', $quote->quote_number), $stream,$quote->quote_password);
        return pdf_create($html, $file_name, $stream, '');
    }
}

if (!function_exists('generate_billing_pdf')) {
    function generate_billing_pdf($project_id, $data, $quote_template = null, $file_name = null)
    {
        $stream = false;
        $CI = &get_instance();
        $file_name = "Pagos";
        $quote_template = 'default';
        $html = $CI->load->view('templates/billing_templates/pdf/' . $file_name, $data, true);

        $CI->load->helper('mpdf');

        //return pdf_create($html, lang('quote') . '_' . str_replace(array('\\', '/'), '_', $quote->quote_number), $stream,$quote->quote_password);
        return pdf_create($html, $file_name, $stream, '');
    }
}

if (!function_exists('generate_program1_pdf')) {
    function generate_program1_pdf($data, $quote_template = null, $file_name = null)
    {
        $stream = false;
        $CI = &get_instance();
        $file_name = "Programacion1";
        $quote_template = 'default';
        $html = $CI->load->view('templates/program_templates/pdf/' . $file_name, $data, true);

        $CI->load->helper('mpdf');

        //return pdf_create($html, lang('quote') . '_' . str_replace(array('\\', '/'), '_', $quote->quote_number), $stream,$quote->quote_password);
        return pdf_create($html, $file_name, $stream, '');
    }
}
if (!function_exists('generate_program2_pdf')) {
    function generate_program2_pdf($data, $quote_template = null, $file_name = null)
    {
        $stream = false;
        $CI = &get_instance();
        $file_name = "Programacion2";
        $quote_template = 'default';
        $html = $CI->load->view('templates/program_templates/pdf/' . $file_name, $data, true);

        $CI->load->helper('mpdf');

        //return pdf_create($html, lang('quote') . '_' . str_replace(array('\\', '/'), '_', $quote->quote_number), $stream,$quote->quote_password);
        return pdf_create($html, $file_name, $stream, '');
    }
}
if (!function_exists('generate_program3_pdf')) {
    function generate_program3_pdf($data, $quote_template = null, $file_name = null)
    {
        $stream = false;
        $CI = &get_instance();
        $file_name = "Programacion3";
        $quote_template = 'default';
        $html = $CI->load->view('templates/program_templates/pdf/' . $file_name, $data, true);

        $CI->load->helper('mpdf');

        //return pdf_create($html, lang('quote') . '_' . str_replace(array('\\', '/'), '_', $quote->quote_number), $stream,$quote->quote_password);
        return pdf_create($html, $file_name, $stream, '');
    }
}
if (!function_exists('generate_program4_pdf')) {
    function generate_program4_pdf($data, $quote_template = null, $file_name = null)
    {
        $stream = false;
        $CI = &get_instance();
        $file_name = "Programacion4";
        $quote_template = 'default';
        $html = $CI->load->view('templates/program_templates/pdf/' . $file_name, $data, true);

        $CI->load->helper('mpdf');

        //return pdf_create($html, lang('quote') . '_' . str_replace(array('\\', '/'), '_', $quote->quote_number), $stream,$quote->quote_password);
        return pdf_create($html, $file_name, $stream, '');
    }
}
if (!function_exists('generate_program5_pdf')) {
    function generate_program5_pdf($data, $quote_template = null, $file_name = null)
    {
        $stream = false;
        $CI = &get_instance();
        $file_name = "Programacion5";
        $quote_template = 'default';
        $html = $CI->load->view('templates/program_templates/pdf/' . $file_name, $data, true);

        $CI->load->helper('mpdf');

        //return pdf_create($html, lang('quote') . '_' . str_replace(array('\\', '/'), '_', $quote->quote_number), $stream,$quote->quote_password);
        return pdf_create($html, $file_name, $stream, '');
    }
}
