<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/09/11
 * Time: 10:18 AM
 */


/**
 * get tasks for a selected project
 * @param null $project_id
 * @return array of task objects
 */
if (!function_exists('get_task_progress')) {
    function get_task_progress($project_id = null)
    {

        $CI =& get_instance();

        $project_tasks = $CI->db->where('project_id', $project_id)->get('tasks')->result();
        //@TODO: Add translation..
        $result = '<div class="md-content">
        <h3><strong>Task Progress</strong> Information</h3>
        <div>';
        if (count($project_tasks)) {
            foreach ($project_tasks as $task) {
                $result .= '<p>' . strtoupper($task->task_name) . '</p>
            <div class="progress progress-xs for-modal">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="' . $task->task_progress . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $task->task_progress . '%">
                    <span >' . $task->task_name . '&#37;' . $task->task_status . '</span>
                </div>
            </div>';
            }
        } else {
            $result .= '<p class="text-center">
                No results returned.. Sorry
            </p>';

        }
        $result .= '<p class="text-center">
                <button class="btn btn-danger btn-sm md-close">Close</button>
            </p>
        </div>
    </div>';

        return $result;
    }
}

/**
 * get roles(project manager, client...)
 * @param $role_id
 * @return stdClass
 */
if (!function_exists('project_role_details')) {
    function project_role_details($role_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('role_id', $role_id)->get('project_roles')->row();
        $Role_class = new stdClass();
        $Role_class->role_name = '----';

        if ($row) {
            return $row;
        }

        return $Role_class;
    }
}

/**
 * get application role names(administrator, staff, client...)
 * @param null $role_id
 * @return string .. role name
 */
if (!function_exists('get_role_name')) {
    function get_role_name($role_id = null)
    {

        $CI =& get_instance();
        $row = $CI->db->where('role_id', intval($role_id))->get('roles')->row();
        if ($row) {
            return $row->role_name;
        }

        return '';
    }
}

/**
 * get product name
 * @param $item_id
 * @return string .. item name
 */
if (!function_exists('get_item_name')) {
    function get_item_name($item_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('item_id', intval($item_id))->get('items')->row();
        if ($row) {
            return $row->item_name;
        }

        return '';

    }
}

/**
 * get product name
 * @param $item_id
 * @return string .. item name
 */
if (!function_exists('get_client_name')) {
    function get_client_name($client_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('client_id', intval($client_id))->get('client')->row();
        if ($row) {
            return $row->client_name;
        }

        return '';

    }
}

/**
 * get currency symbol
 * @param $currency_id
 * @return string .. currency symbol
 */
if (!function_exists('get_currency_symbol')) {
    function get_currency_symbol($currency_id)
    {
        $CI =& get_instance();
        $row = $CI->db->where('currency_id', $currency_id)->get('settings_currency')->row();
        if ($row) {
            return $row->curreny_symbol;
        }

        return '';

    }
}

/**
 * get currency symbol
 * @param $currency_name
 * @return string.. currency symbol
 */
if (!function_exists('get_currency_symbol_by_name')) {
    function get_currency_symbol_by_name($currency_name)
    {

        $CI =& get_instance();
        $row = $CI->db->where('name', $currency_name)->get('settings_currency')->row();
        if ($row) {
            return $row->curreny_symbol;
        }

        return '';
    }
}

/**
 * get currency name
 * @param $currency_id
 * @return string .. currency name
 */
if (!function_exists('get_currency_name')) {
    function get_currency_name($currency_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('currency_id', $currency_id)->get('settings_currency')->row();
        if ($row) {
            return $row->name;
        }

        return '';

    }
}

/**
 * get expense categories
 * @param $category_id
 * @return string .. category name
 */
if (!function_exists('get_category_name')) {
    function get_category_name($category_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('class_id', $category_id)->get('classification')->row();
        if ($row) {
            return $row->class_name;
        }

        return '';

    }
}

/**
 * get currency iso
 * @param $currency_id
 * @return string
 */

if (!function_exists('get_currency_iso')) {
    function get_currency_iso($currency_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('currency_id', $currency_id)->get('settings_currency')->row();
        if ($row) {
            return $row->iso_code;
        }

        return '';

    }
}

/**
 * get tax value
 * @param $tax_id
 * @return string.. tax rate percentage
 */
if (!function_exists('get_tax_value')) {
    function get_tax_value($tax_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('tax_id', $tax_id)->get('settings_tax')->row();
        if ($row) {
            return $row->rate;
        }

        return '';

    }
}

/**
 * get country time zone
 * @param $zone_id
 * @return string .. time zone
 */
if (!function_exists('get_time_zone')) {
    function get_time_zone($zone_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('timezone_id', $zone_id)->get('timezone')->row();
        if ($row) {
            return $row->name;
        }

        return '';

    }
}

/**
 * get invoice reference/transno
 * @param $invoice_id
 * @return string.. trans number
 */
if (!function_exists('get_invoice_reference_no')) {
    function get_invoice_reference_no($invoice_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('id', $invoice_id)->get('invoice')->row();
        if ($row) {
            return $row->inv_no;
        }

        return '';
    }
}

/**
 * get currency used at the time of the invoice
 * @param $invoice_id
 * @return string .. currency
 */
if (!function_exists('get_invoice_currency')) {
    function get_invoice_currency($invoice_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('id', $invoice_id)->get('invoice')->row();
        if ($row) {
            return $row->currency;
        }

        return '';
    }
}

/**
 * get country name
 * @param $country_id
 * @return string.. country name
 */
if (!function_exists('get_country_name')) {
    function get_country_name($country_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('id_country', $country_id)->get('country')->row();
        if ($row) {
            return $row->country_name;
        }

        return '';
    }
}

/**
 * get client company name
 * @param $client_id
 * @return string.. comapany name
 */
if (!function_exists('client_company')) {
    function client_company($client_id)
    {
        $CI =& get_instance();
        $row = $CI->db->where('client_id', $client_id)->get('client')->row();
        if ($row) {
            return $row->client_name;
        }

        return '';
    }
}

/**
 * get client company name
 * @param $client_id
 * @return string.. comapany name
 */
if (!function_exists('project_company')) {
    function project_company($project_id)
    {
        $CI =& get_instance();
        $row = $CI->db->where('project_id', $project_id)->get('projects')->row();
        if ($row) {
            return $row->project_title;
        }

        return '';
    }
}

/**
 * get client company name
 * @param $get_tax
 * @return string.. comapany name
 */
if (!function_exists('invoice_tax')) {
    function invoice_tax($client_id)
    {
        $CI =& get_instance();
        $CI->db->select('distinct(invoice.client_id), invoice.tax');
        $row = $CI->db->where('client_id', $client_id)->get('invoice')->row();
        if ($row) {
            return $row->tax;
        }

        return '';
    }
}


/**
 * get client company name
 * @param $get_tax
 * @return string.. comapany namen
 */
if (!function_exists('invoice_taxpro')) {
    function invoice_taxpro($project_id)
    {
        $CI =& get_instance();
        $row = $CI->db->where('project_id', $project_id)->get('invoice')->row();
        if ($row) {
            return $row->tax;
        }

        return '';
    }
}

/**
 * get selected client details
 * @param $client_id
 * @return mixed .. client data
 */
if (!function_exists('get_client_details')) {
    function get_client_details($client_id)
    {
        $CI =& get_instance();

        return $CI->db->where('client_id', $client_id)->get('client')->row();
    }
}

/**
 * get user details
 * @param $user_id
 * @return mixed
 */
if (!function_exists('get_user_details')) {
    function get_user_details($user_id)
    {

        $CI =& get_instance();

        return $CI->db->where('emp_id', $user_id)->get('employee')->row();
    }
}

/**
 * get user email address
 * @param null $user_id
 * @return mixed
 */
if (!function_exists('user_email')) {
    function user_email($user_id = null)
    {
        $CI =& get_instance();
        $CI->db->join('employee', 'users.user_id = employee.emp_id');

        return $CI->db->where('users.user_id', intval($user_id))->get('users')->row();
    }
}

/**
 * get administrator data
 * @return mixed
 */
if (!function_exists('admin_email')) {
    function admin_email()
    {
        $CI =& get_instance();
        $CI->db->join('employee', 'users.user_id = employee.emp_id');

        return $CI->db->where('users.role_id', 1)->get('users')->result();
    }
}

/**
 * get activity logs user information
 * @param $user_type
 * @param $user_id
 * @return string
 */
if (!function_exists('activity_user')) {
    function activity_user($user_type, $user_id)
    {

        switch ($user_type) {
            case 1:
                return _company_user_name($user_id);
                break;
            case 0:
                return _client_user_name($user_id);
                break;

        }

    }
}

/**
 * get user details depending on user type
 * @param $user_id
 * @param $user_type
 * @return stdClass
 */
if (!function_exists('users_details')) {
    function users_details($user_id, $user_type)
    {

        switch ($user_type) {
            case 1:
                return _company_user_details($user_id);
                break;
            case 0:
                return _client_user_details($user_id);
                break;

        }

    }
}

/**
 * get comapay user details
 * @param $user_id
 * @return user object
 *
 */
if (!function_exists('_company_user_details')) {
    function _company_user_details($user_id)
    {

        $CI =& get_instance();

        $User_class = new stdClass();
        $User_class->full_name = '';
        $User_class->avatar = '';
        $User_class->email = '';
        $CI->db->join('users', 'users.user_id = employee.emp_id');
        $row = $CI->db->where('employee.emp_id', $user_id)->get('employee')->row();
        if ($row) {
            $User_class->full_name = $row->emp_name . " " . $row->emp_surname;
            $User_class->avatar = $row->emp_avatar;
            $User_class->email = $row->email;
        }

        return $User_class;
    }
}

/**
 * get comments message on a task
 * @param null $message_id
 * @return array of objects
 */
if (!function_exists('get_task_conversation')) {
    function get_task_conversation($message_id = null)
    {
        $CI =& get_instance();

        return $CI->db->where('message_id', $message_id)->get('task_message_replies')->result();

    }
}

/**
 * get project comments replies
 * @param null $message_id
 * @return mixed
 */
if (!function_exists('project_comments_replies')) {
    function project_comments_replies($message_id = null)
    {
        $CI =& get_instance();

        return $CI->db->where('message_id', $message_id)->get('project_comments_replies')->result();
    }
}

/**
 * @param $user_id
 * @return mixed
 *
 */
if (!function_exists('_client_user_details')) {
    function _client_user_details($user_id)
    {

        $CI =& get_instance();

        $User_class = new stdClass();
        $User_class->full_name = '';
        $User_class->avatar = '';
        $User_class->email = '';


        $row = $CI->db->where('cl_user_id', $user_id)->get('client_user')->row();
        if ($row) {
            $User_class->full_name = $row->cl_user_name . " " . $row->cl_user_surname;
            $User_class->avatar = $row->cl_user_avatar;
            $User_class->email = $row->cl_user_email;
        }

        return $User_class;
    }
}

/**
 * get client user name
 * @param $user_id
 * @return string .. user name
 */
if (!function_exists('_client_user_name')) {
    function _client_user_name($user_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('cl_user_id', $user_id)->get('client_user')->row();
        if ($row) {
            return $row->cl_user_name . " " . $row->cl_user_surname;
        }

        return '';

    }
}

/**
 * get client primary contact details
 * @param $client_id
 * @return stdClass .. client user details
 */
if (!function_exists('client_primary_contact_details')) {
    function client_primary_contact_details($client_id)
    {

        $CI =& get_instance();
        $CI->db->join('client', 'client.client_id = client_user.client_id');
        $CI->db->where('cl_user_primary = 1');
        $row = $CI->db->where('client_user.client_id', intval($client_id))->get('client_user')->row();

        if ($row) {
            return $row;
        } else {
            $client_obj = new stdClass();
            $client_obj->cl_user_avatar = 'default.jpg';
            $client_obj->cl_user_surname = '----';
            $client_obj->cl_user_name = '----';
            $client_obj->cl_user_email = 'no set';
            $client_obj->cl_user_password = 'no set';
            $client_obj->role_id = '---';
            $client_obj->cl_user_active = 'no user';
            $client_obj->created_on = '1970-01-01';
            $client_obj->cl_last_login = '1970-01-01';
            $client_obj->cl_user_phone = '12345678';

            return $client_obj;
        }


    }
}

/**
 * client user registration data
 * @param $user_id
 * @return stdClass
 */
if (!function_exists('registration_details')) {
    function registration_details($user_id)
    {

        $CI =& get_instance();
        $CI->db->join('client', 'client.client_id = client_user.client_id');
        $CI->db->where('cl_user_primary = 1');
        $row = $CI->db->where('client_user.cl_user_id', intval($user_id))->get('client_user')->row();

        if ($row) {
            return $row;
        } else {
            $client_obj = new stdClass();
            $client_obj->cl_user_avatar = 'default.jpg';
            $client_obj->cl_user_surname = '----';
            $client_obj->client_id = '----';
            $client_obj->cl_user_name = '----';
            $client_obj->cl_user_email = 'no set';
            $client_obj->cl_user_password = 'no set';
            $client_obj->role_id = '---';
            $client_obj->cl_user_active = 'no user';
            $client_obj->created_on = '1970-01-01';
            $client_obj->cl_last_login = '1970-01-01';
            $client_obj->cl_user_phone = '12345678';

            return $client_obj;
        }


    }
}

/**
 * get task status
 * @return mixed
 */
if (!function_exists('task_status')) {
    function task_status()
    {
        $CI =& get_instance();

        return $CI->db->get('task_status')->result();

    }
}

/**
 * Get task priorities
 * @return mixed
 */
if (!function_exists('task_priorities')) {
    function task_priorities()
    {

        $CI =& get_instance();

        return $CI->db->get('priorities')->result();

    }
}

/**
 * Get selected priority name
 * @param $priority_id
 * @return mixed
 */
if (!function_exists('get_priority')) {
    function get_priority($priority_id)
    {
        $CI =& get_instance();
        $row = $CI->db->where('priority_id', intval($priority_id))->get('priorities')->row();
        if ($row) {
            return $row->priority_name;
        }
    }
}

/**
 * get selected task status name
 * @param $status_id
 * @return string
 */
if (!function_exists('get_task_status_name')) {
    function get_task_status_name($status_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('status_id', $status_id)->get('task_status')->row();
        if ($row) {
            return $row->status_name;
        }

        return '';

    }
}

/**
 * get ticket status name
 * @param $status_id
 * @return mixed
 */
if (!function_exists('get_ticket_status')) {
    function get_ticket_status($status_id)
    {


        $CI =& get_instance();
        $row = $CI->db->where('status_id', $status_id)->get('ticket_status')->row();
        if ($row) {
            return $row->status_name;
        }
    }
}
/**
 * get all tickets status
 * @return mixed
 */
if (!function_exists('ticket_status')) {
    function ticket_status()
    {

        $CI =& get_instance();
        $row = $CI->db->get('ticket_status')->result();
        if ($row) {
            return $row;
        }
    }
}

/**
 * get status name
 * @param $status_id
 * @return mixed
 */
if (!function_exists('get_status_name')) {
    function get_status_name($status_id)
    {

        $CI =& get_instance();

        return $CI->db->where('status_id', intval($status_id))->get('status')->row();


    }
}

/**
 * @param $quote_id
 * @return mixed
 */
if (!function_exists('get_quote_status')) {
    function get_quote_status($quote_id)
    {

        $CI =& get_instance();

        return $CI->db->where('status_id', intval($quote_id))->get('quote_status')->row();

    }
}

/**
 * get department description
 * @param $department_id
 * @return mixed
 */

if (!function_exists('get_department')) {
    function get_department($department_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('department_id', intval($department_id))->get('settings_support')->row();
        if ($row) {
            return $row->department_desc;
        }

    }
}

/**
 * @param $estimate_ref
 * @return stdClass
 */
if (!function_exists('get_quote_estimates')) {
    function get_quote_estimates($estimate_ref)
    {

        $CI =& get_instance();
        $quote_stats = $CI->db->where('est_code', $estimate_ref)->get('estimate')->row();
        $Quote_class = new stdClass();
        $Quote_class->amount = '0.00';
        $Quote_class->referece = '';
        $Quote_class->due_date = '00/00/00';
        if ($quote_stats) {
            $Quote_class->amount = calculate_estimate_total($quote_stats->est_id);
            $Quote_class->due_date = $quote_stats->est_due_date;
            $Quote_class->referece = $quote_stats->est_code;
        }

        return $Quote_class;

    }
}

/**
 * get logged risks
 * @param null $project_id
 * @param null $probability
 * @param null $impact_from
 * @param null $impact_to
 * @return mixed
 */
if (!function_exists('get_risk_manager')) {
    function get_risk_manager($project_id = null, $probability = null, $impact_from = null, $impact_to = null)
    {


        $CI =& get_instance();
        $CI->db->where("project_id", $project_id);
        $CI->db->where("probability", $probability);
        $CI->db->where("impact_percentage >=" . $impact_from);
        $CI->db->where("impact_percentage <=" . $impact_to);

        return $CI->db->get('project_risks')->result();

    }
}

/**
 * @return mixed
 */
if (!function_exists('get_estimate_status')) {
    function get_estimate_status()
    {

        $CI =& get_instance();

        return $CI->db->get('est_status')->result();


    }
}

/**
 * @param null $task_id
 * @return mixed
 */
if (!function_exists('get_task_details')) {
    function get_task_details($task_id = null)
    {

        $CI =& get_instance();
        $row = $CI->db->where('task_id', intval($task_id))->get('tasks')->row();
        if ($row) {
            return $row;
        }

    }
}

/**
 * @param null $project_id
 * @return mixed
 */
if (!function_exists('get_project_details')) {
    function get_project_details($project_id = null)
    {

        $CI =& get_instance();
        $row = $CI->db->where('project_id', intval($project_id))->get('projects')->row();
        if ($row) {
            return $row;
        }

    }
}

if(!function_exists('get_estimaciones')){
  function get_estimaciones()
  {

    $CI =& get_instance();
    $row = $CI->db->get('estimaciones')->result();
    if ($row) {
        return $row;
    }

  }
}

if(!function_exists('get_invoice')){
  function get_invoice($inv_id)
  {

    $CI =& get_instance();
    $row = $CI->db->where('id', intval($inv_id))->get('invoice')->result();
    if ($row) {
        return $row;
    }

  }
}

/**
 * get estimate status
 * @param $status_id
 * @return mixed
 */
if (!function_exists('get_estimate_status_name')) {
    function get_estimate_status_name($status_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where('status_id', intval($status_id))->get('est_status')->row();
        if ($row) {
            return $row;
        }

    }
}

/**
 * get invoice status
 * @param $status_id
 * @return string
 */
if (!function_exists('get_invoice_status')) {
    function get_invoice_status($status_id)
    {
        $CI =& get_instance();
        $row = $CI->db->where('status_id', intval($status_id))->get('invoice_status')->row();
        if ($row) {
            return $row->status_name;
        }

        return '';
    }
}

/**
 * get user fullnames
 * @param $id
 * @return string
 */
if (!function_exists('_company_user_name')) {
    function _company_user_name($id)
    {
        $CI =& get_instance();
        $row = $CI->db->where('emp_id', $id)->get('employee')->row();

        return $row->emp_name . " " . $row->emp_surname;
    }
}

/**
 * get payment method name
 * @param $payment_id
 * @return string
 */
if (!function_exists('payment_method')) {
    function payment_method($payment_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where(array('payment_code' => $payment_id))->get('settings_payment_methods')->row();

        if ($row) {
            return $row->payment_name;
        }

        return '';
    }
}

/**
 * get payment method name
 * @param $payment_id
 * @return string
 */
if (!function_exists('payment_metodo')) {
    function payment_metodo($payment_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where(array('payment_code' => $payment_id))->get('settings_payment_methods')->row();

        if ($row) {
            return $row->payment_metod;
        }

        return '';
    }
}

/**
 *  get payment method
 * @param null $inv_id
 * @return null|string
 */
if (!function_exists('invoice_payment_method')) {
    function invoice_payment_method($inv_id = null)
    {
        $CI =& get_instance();
        $row = $CI->db->where('invoice_id ', intval($inv_id))->get('transaction')->row();
        if ($row) {
            return payment_method($row->payment_method);
        }

        return null;

    }
}

/**
 * @param null $line_id
 * @return int
 */
if (!function_exists('calculate_line_subtotal')) {
    function calculate_line_subtotal($line_id = null)
    {
        $CI =& get_instance();
        $row = $CI->db->where('details_id', $line_id)->get('est_details')->row();
        $discount_total = 0;
        if (count($row)) {

            $discount_total = $row->details_hours * $row->details_rate * (1 - ($row->details_discount / 100));

            return ($discount_total);

        }

        return $discount_total;
    }
}

/**
 * calculate item lines totals
 * @param $est_id
 * @return int
 */
if (!function_exists('calculate_estimate_sub_total')) {
    function calculate_estimate_sub_total($est_id)
    {

        $CI =& get_instance();
        $estimate_amounts = $CI->db->where('est_id', $est_id)->get('est_details')->result();
        $estimate_total = 0;
        $Discount_total = 0;
        if ($estimate_amounts) {

            foreach ($estimate_amounts as $estimate_amount) {


                $estimate_total += calculate_subtotal($estimate_amount->details_hours, $estimate_amount->details_rate);

            }
        }
        $Discount_total = calculate_estimate_discount($est_id);

        return ($estimate_total - $Discount_total);
    }
}

/**
 * calculate tax amounts
 * @param $est_id
 * @return int tax amount
 */
if (!function_exists('calculate_estimate_tax_total')) {
    function calculate_estimate_tax_total($est_id)
    {

        $CI =& get_instance();
        $estimate_amounts = $CI->db->where('est_id', $est_id)->get('est_details')->result();
        $tax_total = 0;
        if ($estimate_amounts) {

            foreach ($estimate_amounts as $estimate_amount) {

                $tax_total += calculate_tax(calculate_line_subtotal($estimate_amount->details_id),
                    $estimate_amount->details_tax);
            }
        }

        return $tax_total;
    }
}

/**
 * @param $est_id
 * @return discount amount
 *
 */
if (!function_exists('calculate_estimate_discount')) {
    function calculate_estimate_discount($est_id)
    {
        $CI =& get_instance();
        $estimate_amounts = $CI->db->where('est_id', $est_id)->get('est_details')->result();
        $discount_total = 0;

        if ($estimate_amounts) {

            foreach ($estimate_amounts as $estimate_amount) {

                $discount_total += calculate_discount($estimate_amount->details_hours, $estimate_amount->details_rate,
                    $estimate_amount->details_discount);

            }
        }

        return $discount_total;
    }
}

/**
 * @param $est_id
 * @return discount amount
 *
 */
if (!function_exists('discount')) {
    function discount($project_id = null, $client_id = null, $program = null)
    {
        $CI =& get_instance();
        $estimate_amounts = $CI->db->where(array('project_id' => $project_id, 'client_id' => $client_id, 'program' => $program))->get('invoice')->result();
        $discount_total = 0;

        if ($estimate_amounts) {

            foreach ($estimate_amounts as $estimate_amount) {

                $discount_total += $estimate_amount->disc_amount;

            }
        }

        return $discount_total;
    }
}

if (!function_exists('discount_tax')) {
    function discount_tax($project_id = null, $client_id = null, $program = null)
    {
        $CI =& get_instance();
        $estimate_amounts = $CI->db->where(array('project_id' => $project_id, 'client_id' => $client_id, 'program' => $program, 'tax' => 16))->get('invoice')->result();
        $discount_total = 0;

        if ($estimate_amounts) {

            foreach ($estimate_amounts as $estimate_amount) {

                $discount_total += $estimate_amount->disc_amount;

            }
        }

        return $discount_total;
    }
}

if (!function_exists('discount_project')) {
    function discount_project($project_id = null, $program = null)
    {
        $CI =& get_instance();
        $estimate_amounts = $CI->db->where(array('project_id' => $project_id, 'program' => $program))->get('invoice')->result();
        $discount_total = 0;

        if ($estimate_amounts) {

            foreach ($estimate_amounts as $estimate_amount) {

                $discount_total += $estimate_amount->disc_amount;

            }
        }

        return $discount_total;
    }
}

if (!function_exists('discount_client')) {
    function discount_client($client_id = null, $program = null)
    {
        $CI =& get_instance();
        $estimate_amounts = $CI->db->where(array('client_id' => $client_id, 'program' => $program))->get('invoice')->result();
        $discount_total = 0;

        if ($estimate_amounts) {

            foreach ($estimate_amounts as $estimate_amount) {

                $discount_total += $estimate_amount->disc_amount;

            }
        }

        return $discount_total;
    }
}



/**
 * @param $est_id
 * @return int
 */
if (!function_exists('calculate_estimate_total')) {
    function calculate_estimate_total($est_id)
    {

        $tax = calculate_estimate_tax_total($est_id);

        $estimate_cost = calculate_estimate_sub_total($est_id);

        return (($estimate_cost) + $tax);

    }
}

/**
 * @param int $quantity
 * @param int $price
 * @return int
 */
if (!function_exists('calculate_subtotal')) {
    function calculate_subtotal($quantity = 0, $price = 0)
    {
        $sub_total = 0;
        $sub_total = ($quantity * $price);

        return $sub_total;
    }
}

/**
 * @param int $line_total
 * @param int $tax
 * @return int
 */
if (!function_exists('calculate_tax')) {
    function calculate_tax($line_total = 0, $tax = 0)
    {
        $tax_total = 0;
        $tax_total = ($line_total) * ($tax / 100);

        return $tax_total;

    }
}

/**
 * @param int $line_total
 * @param int $tax
 * @return int
 */
if (!function_exists('calculate_tax_proveedor')) {
    function calculate_tax_proveedor($line_total = 0, $tax = 0)
    {
        $tax_total = 0;
        $tax_total = ($line_total) * ($tax / 100);

        return $tax_total;

    }
}

/**
 * @param int $quantity
 * @param int $price
 * @param int $discount
 * @return int
 */
if (!function_exists('calculate_discount')) {
    function calculate_discount($quantity = 0, $price = 0, $discount = 0)
    {

        $sub_total = 0;
        $discount_total = 0;
        $sub_total = ($quantity * $price);
        $discount_total = $sub_total * ($discount / 100);

        return $discount_total;
    }
}

/**
 * @param null $line_id
 * @return int
 */
if (!function_exists('calculate_invoice_line_subtotal')) {
    function calculate_invoice_line_subtotal($line_id = null)
    {

        $CI =& get_instance();
        $row = $CI->db->where('id', $line_id)->get('invoice_item')->row();
        $discount_total = 0;
        if (count($row)) {

            $discount_total = $row->quantity * $row->price * (1 - ($row->discount / 100));

            return ($discount_total);

        }

        return $discount_total;
    }
}


/**
 * @param $invoice_id
 * @return int
 */
if (!function_exists('_get_invoice_sub_total')) {
    function _get_invoice_sub_total($invoice_id)
    {


        $CI =& get_instance();
        $invoice_amounts = $CI->db->where('invoice_id', $invoice_id)->get('invoice_item')->result();
        $sub_total = 0;
        $Discount_total = 0;
        $sub_total = $CI->db->select_sum('(quantity * price)', 'total_cost')->where('invoice_id',
            $invoice_id)->get('invoice_item')->row()->total_cost;
        if ($invoice_amounts) {

            foreach ($invoice_amounts as $invoice_amount) {

                $Discount_total += calculate_discount($invoice_amount->quantity, $invoice_amount->price,
                    $invoice_amount->discount);

            }

            return ($sub_total - $Discount_total);
        }

        return 0;
    }
}

/**
 * @param $invoice_id
 * @return int
 */
if (!function_exists('_get_invoice_sub_total_proveedor')) {
    function _get_invoice_sub_total_proveedor($project_id = null, $client_id = null)
    {


        $CI =& get_instance();
        $invoice_amounts = $CI->db->where(array('project_id' => $project_id, 'client_id' => $client_id, 'program' => 1))->get('invoice_item')->result();
        $sub_total = 0;
        $Discount_total = 0;
        $sub_total = $CI->db->select_sum('(quantity * price)', 'total_cost')->where(array('project_id' => $project_id, 'client_id' => $client_id, 'program' => 1))->get('invoice_item')->row()->total_cost;
        if ($invoice_amounts) {

            foreach ($invoice_amounts as $invoice_amount) {

                $Discount_total += calculate_discount($invoice_amount->quantity, $invoice_amount->price,
                    $invoice_amount->discount);

            }

            return ($sub_total - $Discount_total);
        }

        return 0;
    }
}

/**
 * @param $invoice_id
 * @return int
 */
if (!function_exists('_get_invoice_sub_total_proveedor_modal')) {
    function _get_invoice_sub_total_proveedor_modal($project_id = null, $client_id = null)
    {


        $CI =& get_instance();
        $invoice_amounts = $CI->db->where(array('project_id' => $project_id, 'client_id' => $client_id))->get('invoice_item')->result();
        $sub_total = 0;
        $Discount_total = 0;
        $sub_total = $CI->db->select_sum('(quantity * price)', 'total_cost')->where(array('project_id' => $project_id, 'client_id' => $client_id))->get('invoice_item')->row()->total_cost;
        if ($invoice_amounts) {

            foreach ($invoice_amounts as $invoice_amount) {

                $Discount_total += calculate_discount($invoice_amount->quantity, $invoice_amount->price,
                    $invoice_amount->discount);

            }

            return ($sub_total - $Discount_total);
        }

        return 0;
    }
}

/**
 * @param $invoice_id
 * @return int
 */
if (!function_exists('_get_invoice_projecto')) {
    function _get_invoice_projecto($project_id = null, $program = nul, $client_id = null)
    {


        $CI =& get_instance();
        $invoice_amounts = $CI->db->where(array('project_id' => $project_id, 'program' => $program, 'tax' => 0, 'client_id' => $client_id, 'nc' => 0))->get('invoice_item')->result();
        $sub_total = 0;
        $Discount_total = 0;
        $sub_total = $CI->db->select_sum('(quantity * price)', 'total_cost')->where(array('project_id' => $project_id, 'program' => $program, 'tax' => 0, 'client_id' => $client_id, 'nc' => 0))->get('invoice_item')->row()->total_cost;
        if ($invoice_amounts) {

            foreach ($invoice_amounts as $invoice_amount) {

                $Discount_total += calculate_discount($invoice_amount->quantity, $invoice_amount->price,
                    $invoice_amount->discount);

            }

            return ($sub_total - $Discount_total);
        }

        return 0;
    }
}

/**
 * @param $invoice_id
 * @return int
 */
if (!function_exists('_get_invoice_projecto_tax')) {
    function _get_invoice_projecto_tax($project_id = null, $program = nul, $client_id = null)
    {


        $CI =& get_instance();
        $invoice_amounts = $CI->db->where(array('project_id' => $project_id, 'program' => $program, 'tax' => 16, 'client_id' => $client_id, 'nc' => 0))->get('invoice_item')->result();
        $sub_total = 0;
        $Discount_total = 0;
        $sub_total = $CI->db->select_sum('(quantity * price)', 'total_cost')->where(array('project_id' => $project_id, 'program' => $program, 'tax' => 16, 'client_id' => $client_id, 'nc' => 0))->get('invoice_item')->row()->total_cost;
        if ($invoice_amounts) {

            foreach ($invoice_amounts as $invoice_amount) {

                $Discount_total += calculate_discount($invoice_amount->quantity, $invoice_amount->price,
                    $invoice_amount->discount);

            }

            return ($sub_total - $Discount_total);
        }

        return 0;
    }
}

/**
 * @param $invoice_id
 * @return float|int
 */
if (!function_exists('_get_invoice_tax_total')) {
    function _get_invoice_tax_total($invoice_id)
    {


        $CI =& get_instance();
        $invoice_amounts = $CI->db->where('invoice_id', $invoice_id)->get('invoice_item')->result();
        $tax_total = 0.00;
        if ($invoice_amounts) {

            foreach ($invoice_amounts as $invoice_amount) {

                $tax_total += calculate_tax(calculate_invoice_line_subtotal($invoice_amount->id), $invoice_amount->tax);


            }
        }

        return $tax_total;
    }
}

/**
 * @param $invoice_id
 * @return float|int
 */
if (!function_exists('_get_invoice_tax_total_proveedor')) {
    function _get_invoice_tax_total_proveedor($project_id = null, $client_id = null)
    {


        $CI =& get_instance();
        $invoice_amounts = $CI->db->where(array('project_id' => $project_id, 'client_id' => $client_id, 'program' => 1))->get('invoice_item')->result();
        $tax_total = 0.00;
        if ($invoice_amounts) {

            foreach ($invoice_amounts as $invoice_amount) {

                $tax_total += calculate_tax_proveedor(calculate_invoice_line_subtotal($invoice_amount->id), $invoice_amount->tax);


            }
        }

        return $tax_total;
    }
}

/**
 * @param $invoice_id
 * @return float|int
 */
if (!function_exists('_get_invoice_tax_total_proveedor_modal')) {
    function _get_invoice_tax_total_proveedor_modal($project_id = null, $client_id = null)
    {


        $CI =& get_instance();
        $invoice_amounts = $CI->db->where(array('project_id' => $project_id, 'client_id' => $client_id))->get('invoice_item')->result();
        $tax_total = 0.00;
        if ($invoice_amounts) {

            foreach ($invoice_amounts as $invoice_amount) {

                $tax_total += calculate_tax_proveedor(calculate_invoice_line_subtotal($invoice_amount->id), $invoice_amount->tax);


            }
        }

        return $tax_total;
    }
}

/**
 * @param $invoice_id
 * @return float|int
 */
if (!function_exists('_get_invoice_tax_project')) {
    function _get_invoice_tax_project($project_id = null, $program = null, $client_id = null)
    {


        $CI =& get_instance();
        $invoice_amounts = $CI->db->where(array('project_id' => $project_id, 'program' => $program, 'client_id' => $client_id))->get('invoice_item')->result();
        $tax_total = 0.00;
        if ($invoice_amounts) {

            foreach ($invoice_amounts as $invoice_amount) {

                $tax_total += calculate_tax_proveedor(calculate_invoice_line_subtotal($invoice_amount->id), $invoice_amount->tax);


            }
        }

        return $tax_total;
    }
}

/**
 * @param $invoice_id
 * @return int
 */
if (!function_exists('_get_invoice_discount_total')) {
    function _get_invoice_discount_total($invoice_id)
    {


        $CI =& get_instance();
        $invoice_amounts = $CI->db->where('invoice_id', $invoice_id)->get('invoice_item')->result();
        $tax_total = 0;
        if ($invoice_amounts) {

            foreach ($invoice_amounts as $invoice_amount) {

                $tax_total += calculate_discount($invoice_amount->quantity, $invoice_amount->price,
                    $invoice_amount->discount);

            }
        }

        return $tax_total;
    }
}

/**
 * @param $invoice_id
 * @return int
 */
if (!function_exists('invoice_sub_total_total')) {
    function invoice_sub_total_total($invoice_id)
    {

        $invoice_total = _get_invoice_sub_total($invoice_id);

        return ($invoice_total);
    }
}

/**
 * @param $invoice_id
 * @return float|int
 */
if (!function_exists('invoice_tax_total')) {
    function invoice_tax_total($invoice_id)
    {
        $invoice_total = 0.00;
        $invoice_total = _get_invoice_tax_total($invoice_id);

        return ($invoice_total);
    }
}


/**
 * calculate invoice totals
 * @param null $invoice_id
 * @return invoice total
 */
if (!function_exists('invoice_total')) {
    function invoice_total($invoice_id = null)
    {
        $tax = 0;
        $invoice_cost = 0;
        $tax = _get_invoice_tax_total($invoice_id);
        $invoice_cost = _get_invoice_sub_total($invoice_id);

        return (($invoice_cost) + $tax);
    }
}

/**
 * calculate invoice totals
 * @param null $invoice_id
 * @return invoice total
 */
if (!function_exists('invoice_total_proveedor')) {
    function invoice_total_proveedor($project_id = null, $client_id = null)
    {
        $tax = 0;
        $invoice_cost = 0;
        $tax = _get_invoice_tax_total_proveedor($project_id, $client_id);
        $invoice_cost = _get_invoice_sub_total_proveedor($project_id, $client_id);

        return (($invoice_cost) + $tax);
    }
}

/**
 * calculate invoice totals
 * @param null $invoice_id
 * @return invoice total
 */
if (!function_exists('invoice_total_proveedor_modal')) {
    function invoice_total_proveedor_modal($project_id = null, $client_id = null)
    {
        $tax = 0;
        $invoice_cost = 0;
        $tax = _get_invoice_tax_total_proveedor_modal($project_id, $client_id);
        $invoice_cost = _get_invoice_sub_total_proveedor_modal($project_id, $client_id);

        return (($invoice_cost) + $tax);
    }
}

/**
 * calculate invoice totals
 * @param null $invoice_id
 * @return invoice total
 */
if (!function_exists('invoice_projecto')) {
    function invoice_projecto($project_id = null, $program = null, $client_id = null)
    {
        $tax = 0;
        $invoice_cost = 0;
        $invoice_cost = _get_invoice_projecto($project_id, $program, $client_id);

        return (($invoice_cost));
    }
}

/**
 * calculate invoice totals
 * @param null $invoice_id
 * @return invoice total
 */
if (!function_exists('invoice_projecto_tax')) {
    function invoice_projecto_tax($project_id = null, $program = null, $client_id = null)
    {
        $tax = 0;
        $invoice_cost = 0;
        $tax = _get_invoice_tax_project($project_id, $program, $client_id);
        $invoice_cost = _get_invoice_projecto_tax($project_id, $program, $client_id);

        return (($invoice_cost) + $tax);
    }
}

/**
 * @param null $client_id
 * @param null $where_condition
 * @return tax_total
 */
if (!function_exists('_get_client_invoice_tax_total')) {
    function _get_client_invoice_tax_total($client_id = null, $where_condition = null)
    {
        $CI =& get_instance();

        $CI->db->from("invoice_item");
        $CI->db->join('invoice', 'invoice.id=invoice_item.invoice_id');

        if ($client_id) {
            $CI->db->where("invoice.client_id = " . intval($client_id));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $invoice_amounts = $CI->db->get()->result();

        $tax_total = 0;
        if ($invoice_amounts) {

            foreach ($invoice_amounts as $invoice_amount) {
                $tax_total += calculate_tax(calculate_invoice_line_subtotal($invoice_amount->id), $invoice_amount->tax);
            }
        }

        return $tax_total;
    }
}

/**
 * @param null $client_id
 * @param string $where_condition
 * @return sub_total
 */
if (!function_exists('_get_client_invoice_sub_total')) {
    function _get_client_invoice_sub_total($client_id = null, $where_condition = '')
    {
        $CI =& get_instance();
        $CI->db->from("invoice_item");
        $CI->db->join('invoice', 'invoice.id=invoice_item.invoice_id');

        if ($client_id) {
            $CI->db->where("invoice.client_id = " . intval($client_id));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $invoice_amounts = $CI->db->get()->result();
        $subs_total = 0;
        if ($invoice_amounts) {

            foreach ($invoice_amounts as $invoice_amount) {
                $sub_total = 0;
                $Discount_total = 0;

                $sub_total = calculate_subtotal($invoice_amount->quantity, $invoice_amount->price);
                $Discount_total = calculate_discount($invoice_amount->quantity, $invoice_amount->price,
                    $invoice_amount->discount);
                $subs_total += ($sub_total - $Discount_total);
            }
        }

        return $subs_total;
    }
}

/**
 * @param null $client_id
 * @param string $where_condition
 * @return sub_total
 */
if (!function_exists('proveedor_total')) {
    function proveedor_total($client_id = null, $project_id = null, $where_condition = '')
    {
        $CI =& get_instance();
        $CI->db->from("invoice_item");
        $CI->db->join('invoice', 'invoice.id=invoice_item.invoice_id');

        if ($project_id) {
            $CI->db->where(array('invoice.client_id' => $client_id, 'invoice.project_id' => $project_id));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $invoice_amounts = $CI->db->get()->result();
        $subs_total = 0;
        if ($invoice_amounts) {

            foreach ($invoice_amounts as $invoice_amount) {
                $sub_total = 0;
                $Discount_total = 0;
                $tax = 0;

                $sub_total = calculate_subtotal($invoice_amount->quantity, $invoice_amount->price);
                $Discount_total = calculate_discount($invoice_amount->quantity, $invoice_amount->price,
                    $invoice_amount->discount);
                $subs_total += ($sub_total - $Discount_total);

            }
        }

        return $subs_total;
    }
}

/**
 * get client invoice amounts
 * @param null $client_id
 * @param string $period
 * @return int
 *
 */
if (!function_exists('client_invoice_total')) {
    function client_invoice_total($client_id = null, $period = ' ')
    {

        switch ($period) {
            case 'month':
                $where = "MONTH(`created_at`) = MONTH(NOW())
					  AND YEAR(`created_at`) = YEAR(NOW())";
                $tax = _get_client_invoice_tax_total($client_id, $where);
                $invoice_cost = _get_client_invoice_sub_total($client_id, $where);

                return (($invoice_cost) + $tax);
            case 'last_month':
                $where = "MONTH(`created_at`) = MONTH(NOW() - INTERVAL 1 MONTH)
					 AND YEAR(`created_at`) = YEAR(NOW() - INTERVAL 1 MONTH)";
                $tax = _get_client_invoice_tax_total($client_id, $where);
                $invoice_cost = _get_client_invoice_sub_total($client_id, $where);

                return (($invoice_cost) + $tax);
                break;
            case 'year':

                $where = "YEAR(`created_at`) = YEAR(NOW())";
                $tax = _get_client_invoice_tax_total($client_id, $where);
                $invoice_cost = _get_client_invoice_sub_total($client_id, $where);

                return (($invoice_cost) + $tax);
                break;

            case 'last_year':

                $where = "YEAR(`created_at`) = YEAR(NOW() - INTERVAL 1 YEAR)";
                $tax = _get_client_invoice_tax_total($client_id, $where);
                $invoice_cost = _get_client_invoice_sub_total($client_id, $where);

                return (($invoice_cost) + $tax);
                break;
            default:
                $where = null;
                $tax = _get_client_invoice_tax_total($client_id, $where);
                $invoice_cost = _get_client_invoice_sub_total($client_id, $where);

                return (($invoice_cost) + $tax);
                break;
        }
    }
}


/**
 * get invoice balance
 * @param $invoice_id
 * @return balance amount
 */
if (!function_exists('invoice_balance')) {
    function invoice_balance($invoice_id)
    {
        $invoice_total = invoice_total($invoice_id);
        $paid_total = _get_invoice_payments($invoice_id);

        return ($invoice_total - $paid_total);
    }
}

/**
 * @param $invoice_id
 * @return paid amount
 */
if (!function_exists('_get_invoice_payments')) {
    function _get_invoice_payments($invoice_id)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $payments = $CI->db->select_sum('amount')->where('invoice_id', $invoice_id)->get('transaction')->row();

        if ($payments) {
            $paid_amount = $payments->amount;
        }

        return $paid_amount;

    }
}

/**
 * get clients payments by period
 * @param null $client_id
 * @param string $period
 * @return int
 */
if (!function_exists('client_payments')) {
    function client_payments($client_id = null, $period = '')
    {
        switch ($period) {
            case 'month':
                $where = "MONTH(`transaction`.`created_at`) = MONTH(NOW())
					AND YEAR(`transaction`.`created_at`) = YEAR(NOW())";

                return _get_client_payments($client_id, $where);
                break;
            case 'last_month':

                $where = "MONTH(`transaction`.`created_at`) = MONTH(NOW() - INTERVAL 1 MONTH)
					AND YEAR(`transaction`.`created_at`) = YEAR(NOW() - INTERVAL 1 MONTH)";

                return _get_client_payments($client_id, $where);
                break;

            case 'year':

                $where = "YEAR(`transaction`.`created_at`) = YEAR(NOW())";

                return _get_client_payments($client_id, $where);
                break;

            case 'last_year':
                $where = "YEAR(`transaction`.`created_at`) = YEAR(NOW() - INTERVAL 1 YEAR)";

                return _get_client_payments($client_id, $where);
                break;

            default:
                $where = null;

                return _get_client_payments($client_id, $where);
                break;
        }

    }
}

/**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('_get_client_payments')) {
    function _get_client_payments($client_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("transaction");
        $CI->db->select_sum('amount');
        if ($client_id) {
            $CI->db->where("client_id = " . intval($client_id));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount = $payments->amount;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('_get_client_payments')) {
    function _get_client_payments($client_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("transaction");
        $CI->db->select_sum('amount');
        if ($client_id) {
            $CI->db->where("client_id = " . intval($client_id));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount = $payments->amount;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('nc')) {
    function nc($project_id = null, $client_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("invoice_item");
        $CI->db->select_sum('price');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id, 'client_id' => $client_id, 'nc' => 1, 'program' => 1));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->price;
        }

        return $paid_amount;

    }
}

if (!function_exists('nc_notax')) {
    function nc_notax($project_id = null, $client_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("invoice_item");
        $CI->db->select_sum('price');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id, 'client_id' => $client_id, 'nc' => 1, 'program' => 1, 'nc_tax' => 0));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->price;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('nc_tax')) {
    function nc_tax($project_id = null, $client_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("invoice_item");
        $CI->db->select_sum('price');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id, 'client_id' => $client_id, 'nc' => 1, 'program' => 1, 'nc_tax' => 16));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->price;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('pago_a')) {
    function pago_a($client_id = null, $project_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("pago_a");
        $CI->db->select_sum('monto');
        if ($client_id) {
            $CI->db->where(array('client_id' => $client_id, 'project_id' => $project_id, 'tax' => 0, 'old' => 0));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->monto;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('est_trans')) {
    function est_trans($avance = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("est_trans");
        $CI->db->select_sum('amount');
        if ($avance) {
            $CI->db->where(array('avance' => $avance));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->amount;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('pago_a_invo')) {
    function pago_a_invo($inv_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("pago_a");
        $CI->db->select_sum('monto');
        if ($inv_id) {
            $CI->db->where(array('inv_id' => $inv_id));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->monto;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('pago_a_est')) {
    function pago_a_est($client_id = null, $project_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("pago_a");
        $CI->db->select_sum('monto');
        if ($client_id) {
            $CI->db->where(array('client_id' => $client_id, 'project_id' => $project_id, 'tax' => 0, 'old_est' => 0));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->monto;
        }

        return $paid_amount;

    }
}
/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('pago_a_diafano')) {
    function pago_a_diafano($client_id = null, $project_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("pago_a");
        $CI->db->select_sum('monto');
        if ($client_id) {
            $CI->db->where(array('client_id' => $client_id, 'project_id' => $project_id, 'tax' => 16, 'old' => 0));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->monto;
        }

        return $paid_amount;

    }
}
/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('pago_a_diafano_est')) {
    function pago_a_diafano_est($client_id = null, $project_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("pago_a");
        $CI->db->select_sum('monto');
        if ($client_id) {
            $CI->db->where(array('client_id' => $client_id, 'project_id' => $project_id, 'tax' => 16, 'old_est' => 0));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->monto;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('estimacion_amount')) {
    function estimacion_amount($invo_id = null, $client_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("estimacion");
        $CI->db->select_sum('est_amount');
        if ($invo_id) {
            $CI->db->where(array('est_inv' => $invo_id, 'est_client' => $client_id));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->est_amount;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('mano_obra')) {
    function mano_obra($project_id = null, $est_numA = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("est_amount");
        $CI->db->select_sum('mano_obra');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id, 'id_amount' => $est_numA ));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->mano_obra;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('materiales')) {
    function materiales($project_id = null, $est_numA = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("est_amount");
        $CI->db->select_sum('materiales');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id, 'id_amount' => $est_numA));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->materiales;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('materiales_iva')) {
    function materiales_iva($project_id = null, $est_numA = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("est_amount");
        $CI->db->select_sum('materiales_iva');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id, 'id_amount' => $est_numA));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->materiales_iva;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('subcontrato')) {
    function subcontrato($project_id = null, $est_numA = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("est_amount");
        $CI->db->select_sum('subcont');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id, 'id_amount' => $est_numA));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->subcont;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('honorarios')) {
    function honorarios($project_id = null, $est_numA = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("est_amount");
        $CI->db->select_sum('honorarios');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id, 'id_amount' => $est_numA));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->honorarios;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('traslados')) {
    function traslados($project_id = null, $est_numA = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("est_amount");
        $CI->db->select_sum('traslados');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id, 'id_amount' => $est_numA));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->traslados;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('mano_obra_a')) {
    function mano_obra_a($project_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("est_amount");
        $CI->db->select_sum('mano_obra');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->mano_obra;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('materiales_a')) {
    function materiales_a($project_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("est_amount");
        $CI->db->select_sum('materiales');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->materiales;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('materiales_iva_a')) {
    function materiales_iva_a($project_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("est_amount");
        $CI->db->select_sum('materiales_iva');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->materiales_iva;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('subcontrato_a')) {
    function subcontrato_a($project_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("est_amount");
        $CI->db->select_sum('subcont');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->subcont;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('honorarios_a')) {
    function honorarios_a($project_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("est_amount");
        $CI->db->select_sum('honorarios');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->honorarios;
        }

        return $paid_amount;

    }
}

/**
**
 * get payments
 * @param null $client_id
 * @param null $where_condition
 * @return paid amount
 */
if (!function_exists('traslados_a')) {
    function traslados_a($project_id = null, $where_condition = null)
    {
        $paid_amount = 0;
        $CI =& get_instance();
        $CI->db->from("est_amount");
        $CI->db->select_sum('traslados');
        if ($project_id) {
            $CI->db->where(array('project_id' => $project_id));
        }
        if ($where_condition) {
            $CI->db->where($where_condition);
        }
        $payments = $CI->db->get()->row();

        if ($payments) {
            $paid_amount += $payments->traslados;
        }

        return $paid_amount;

    }
}


/**
 * Get next trans reference
 * @param $prefix
 * @return mixed
 */
if (!function_exists('trans_reference')) {
    function trans_reference($trans_type, $company_id)
    {

        switch ($trans_type) {
            case 10:
                return _invoice_reference($company_id);
                break;
            case 11:
                return _estimate_reference($company_id);
                break;
            case 12:
                return _quotation_reference($company_id);
                break;

            case 13:
                return _project_reference($company_id);
                break;

            case 14:
                return _orden_reference($company_id);
                break;

            case 15:
                return _raya_reference($company_id);
                break;

            case 16:
                return _trans_reference($company_id);
                break;

            case 17:
                return _nota_reference($company_id);
                break;

          case 18:
              return _gasto_reference($company_id);
              break;

          case 19:
              return _nomina_reference($company_id);
              break;
        }
    }
}

/**
 * get next invoice reference no
 * @param $company_id
 * @return int|string
 */
if (!function_exists('_invoice_reference')) {
    function _invoice_reference($company_id)
    {


        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 10, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            $next_number = intval($row->typeno) + 1;

            if ($next_number < 10000) {
                $next_number = '0000' . $next_number;
            }

            return $next_number;

        } else {
            return '00001';
        }

    }
}

/**
 * get next estimate reference no
 * @param $company_id
 * @return int|string
 */
if (!function_exists('_estimate_reference')) {
    function _estimate_reference($company_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 11, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            $next_number = intval($row->typeno) + 1;

            if ($next_number < 10000) {
                $next_number = '0000' . $next_number;
            }

            return $next_number;

        } else {
            return '00001';
        }
    }
}

/**
 * get next quotation reference no
 * @param $company_id
 * @return int|string
 */
if (!function_exists('_quotation_reference')) {
    function _quotation_reference($company_id)
    {


        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 12, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            $next_number = intval($row->typeno) + 1;

            if ($next_number < 10000) {
                $next_number = '0000' . $next_number;
            }

            return $next_number;

        } else {
            return '00001';
        }
    }
}

/**
 * get next project reference number
 * @param $company_id
 * @return int|string
 */
if (!function_exists('_project_reference')) {
    function _project_reference($company_id)
    {


        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 13, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            $next_number = intval($row->typeno) + 1;

            if ($next_number < 10000) {
                $next_number = '0000' . $next_number;
            }

            return $next_number;

        } else {
            return '00001';
        }


    }
}

/**
 * get next invoice reference no
 * @param $company_id
 * @return int|string
 */
if (!function_exists('_orden_reference')) {
    function _orden_reference($company_id)
    {


        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 14, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            $next_number = intval($row->typeno) + 1;

            if ($next_number < 10000) {
                $next_number = '0000' . $next_number;
            }

            return $next_number;

        } else {
            return '00001';
        }

    }
}

/**
 * get next invoice reference no
 * @param $company_id
 * @return int|string
 */
if (!function_exists('_raya_reference')) {
    function _raya_reference($company_id)
    {


        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 15, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            $next_number = intval($row->typeno) + 1;

            if ($next_number < 10000) {
                $next_number = '0000' . $next_number;
            }

            return $next_number;

        } else {
            return '00001';
        }

    }
}

/**
 * get next invoice reference no
 * @param $company_id
 * @return int|string
 */
if (!function_exists('_trans_reference')) {
    function _trans_reference($company_id)
    {


        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 16, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            $next_number = intval($row->typeno) + 1;

            if ($next_number < 10000) {
                $next_number = '0000' . $next_number;
            }

            return $next_number;

        } else {
            return '00001';
        }

    }
}

/**
 * get next invoice reference no
 * @param $company_id
 * @return int|string
 */
if (!function_exists('_nota_reference')) {
    function _nota_reference($company_id)
    {


        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 17, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            $next_number = intval($row->typeno) + 1;

            if ($next_number < 10000) {
                $next_number = '0000' . $next_number;
            }

            return $next_number;

        } else {
            return '00001';
        }

    }
}

/**
 * get next invoice reference no
 * @param $company_id
 * @return int|string
 */
if (!function_exists('_gasto_reference')) {
    function _gasto_reference($company_id)
    {


        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 18, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            $next_number = intval($row->typeno) + 1;

            if ($next_number < 10000) {
                $next_number = '0000' . $next_number;
            }

            return $next_number;

        } else {
            return '00001';
        }

    }
}

/**
 * get next invoice reference no
 * @param $company_id
 * @return int|string
 */
if (!function_exists('_nomina_reference')) {
    function _nomina_reference($company_id)
    {


        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 1, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            $next_number = intval($row->typeno) + 1;

            if ($next_number < 10000) {
                $next_number = '0000' . $next_number;
            }

            return $next_number;

        } else {
            return '00001';
        }

    }
}

/**
 * get transaction prefix .. INV,QOT,PRO,EST
 * @param $prefix
 * @param $company_id
 */
if (!function_exists('trans_prefix')) {
    function trans_prefix($prefix, $company_id)
    {
        switch ($prefix) {
            case 10:
                return _invoice_prefix($company_id);
                break;

            case 11:
                return _estimate_prefix($company_id);
                break;
            case 12:
                return _quotation_prefix($company_id);
                break;
            case 13:
                return _project_prefix($company_id);
                break;
            case 14:
                return _orden_prefix($company_id);
                break;

            case 15:
                return _raya_prefix($company_id);
                break;

            case 16:
                return _trans_prefix($company_id);
                break;

            case 17:
                return _nota_prefix($company_id);
                break;

            case 18:
                return _gasto_prefix($company_id);
                break;

            case 19:
                return _nomina_prefix($company_id);
                break;
        }
    }
}

/**
 * get invoice prefix
 * @param $company_id
 */
if (!function_exists('_invoice_prefix')) {
    function _invoice_prefix($company_id)
    {
        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 10, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            return $row->prefix;
        }

        return;

    }
}

/**
 * get estimate prefix
 * @param $company_id
 */
if (!function_exists('_estimate_prefix')) {
    function _estimate_prefix($company_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 11, 'company_id' => $company_id))->get('settings_systypes')->row();
        if (count($row)) {
            return $row->prefix;
        }

        return;
    }
}

/**
 * get quotation prefix
 * @param $company_id
 */
if (!function_exists('_quotation_prefix')) {
    function _quotation_prefix($company_id)
    {
        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 12, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            return $row->prefix;
        }

        return;
    }
}

/**
 * get project prefix
 * @param $company_id
 */
if (!function_exists('_project_prefix')) {
    function _project_prefix($company_id)
    {

        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 13, 'company_id' => $company_id))->get('settings_systypes')->row();
        if (count($row)) {
            return $row->prefix;
        }

        return;
    }
}

/**
 * get orden prefix
 * @param $company_id
 */
if (!function_exists('_orden_prefix')) {
    function _orden_prefix($company_id)
    {
        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 14, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            return $row->prefix;
        }

        return;

    }
}

/**
 * get raya prefix
 * @param $company_id
 */
if (!function_exists('_raya_prefix')) {
    function _raya_prefix($company_id)
    {
        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 15, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            return $row->prefix;
        }

        return;

    }
}

/**
 * get raya prefix
 * @param $company_id
 */
if (!function_exists('_trans_prefix')) {
    function _trans_prefix($company_id)
    {
        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 16, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            return $row->prefix;
        }

        return;

    }
}

/**
 * get raya prefix
 * @param $company_id
 */
if (!function_exists('_nota_prefix')) {
    function _nota_prefix($company_id)
    {
        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 17, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            return $row->prefix;
        }

        return;

    }
}

/**
 * get raya prefix
 * @param $company_id
 */
if (!function_exists('_gasto_prefix')) {
    function _gasto_prefix($company_id)
    {
        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 18, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            return $row->prefix;
        }

        return;

    }
}

/**
 * get raya prefix
 * @param $company_id
 */
if (!function_exists('_nomina_prefix')) {
    function _nomina_prefix($company_id)
    {
        $CI =& get_instance();
        $row = $CI->db->where(array('typeid' => 19, 'company_id' => $company_id))->get('settings_systypes')->row();

        if (count($row)) {
            return $row->prefix;
        }

        return;

    }
}

/**
 * get sales amount by months
 * @param $month
 * @param $is_client
 * @param $company_id
 * @return total invoice amount for a month
 */
if (!function_exists('stats_sales_monthly_data')) {
    function stats_sales_monthly_data($month, $is_client, $company_id)
    {

        $CI =& get_instance();

        $CI->db->where('MONTH(created_at)', $month);
        $CI->db->where('YEAR(created_at)', date('Y'));

        if ($is_client) {
            $CI->db->where('client_id', $is_client);
        }

        $invoice_details = $CI->db->get('invoice')->result();
        $invoice_total = 0;
        if ($invoice_details) {

            foreach ($invoice_details as $invoice) {

                $invoice_total += invoice_total($invoice->id);
            }
        }

        return $invoice_total;

    }
}


/**
 * get paid amounts for a month
 * @param $month
 * @param $is_client
 * @param $company_id
 * @return total paid amount for a month
 */
if (!function_exists('stats_income_monthly_data')) {
    function stats_income_monthly_data($month, $is_client, $company_id)
    {

        $CI =& get_instance();
        $CI->db->from("transaction");
        $CI->db->select_sum('amount', 'paid_amount');
        $CI->db->where('MONTH(invoice.created_at)', $month);
        $CI->db->where('YEAR(invoice.created_at)', date('Y'));

        if ($is_client) {
            $CI->db->where('client_id', $is_client);
        }
        $CI->db->join('invoice', 'invoice.id=transaction.invoice_id');
        $invoice_details = $CI->db->get()->result();
        $invoice_total = 0;
        if ($invoice_details) {

            foreach ($invoice_details as $invoice) {

                $invoice_total += $invoice->paid_amount;
            }
        }

        return $invoice_total;

    }
}

/**
 * get expenses total by months
 * @param $month
 * @param $is_client
 * @param $company_id
 * @return int
 */
if (!function_exists('expense_monthly_data')) {
    function expense_monthly_data($month, $is_client, $company_id)
    {

        $CI =& get_instance();

        $CI->db->where('MONTH(date_entry)', $month);
        $CI->db->where('YEAR(date_entry)', date('Y'));

        if ($is_client) {
            //$CI->db->where('client_id', $is_client);
        }
        $CI->db->select_sum('real_cost');
        $amount_details = $CI->db->get('costs')->result();
        $amount_total = 0;
        foreach ($amount_details as $row) {

            $amount_total += $row->real_cost;
        }

        return $amount_total;
    }
}

/**
 * @param null $month
 * @param null $year
 * @param null $type
 * @return int
 */
if (!function_exists('expense_stats_monthly')) {
    function expense_stats_monthly($month = null, $year = null, $type = null)
    {

        $CI =& get_instance();

        $CI->db->where('MONTH(date_entry)', $month);
        if (!$year) {
            $year = date('Y');
        }
        $CI->db->where('YEAR(date_entry)', $year);

        $CI->db->where('classification', $type);
        $CI->db->select_sum('real_cost');
        $amount_details = $CI->db->get('costs')->result();
        $amount_total = 0;
        foreach ($amount_details as $row) {

            $amount_total += $row->real_cost;
        }

        return $amount_total;

    }
}

/**
 * get paid amounts by months
 * @param $month
 * @param $client_id
 * @param $company_id
 * @return float
 */
if (!function_exists('stats_payment_monthly_data')) {
    function stats_payment_monthly_data($month, $client_id, $company_id)
    {

        $amount = 0;
        $CI =& get_instance();
        $CI->db->from("transaction");
        $CI->db->select_sum('amount', 'paid_amount');
        $CI->db->join('invoice', 'invoice.id=transaction.invoice_id');
        if ($client_id) {
            $CI->db->where("invoice.client_id = " . intval($client_id));
        }
        $CI->db->where('MONTH(transaction.created_at)', $month);
        $CI->db->where('YEAR(transaction.created_at)', date('Y'));

        $query = $CI->db->get()->result();
        foreach ($query as $row) {
            $amount += $row->paid_amount;
        }

        return $amount;
    }
}

/**
 * @param $month
 * @param null $client_id
 * @param $company_id
 * @return int
 */
if (!function_exists('stats_overdue_monthly_data')) {
    function stats_overdue_monthly_data($month, $client_id = null, $company_id)
    {
        $amount = 0;
        $CI =& get_instance();

        $CI->db->select('*');
        $CI->db->from('invoice');
        $CI->db->where('invoice.status= 2');

        if ($client_id) {
            $CI->db->where('invoice.client_id', $client_id);
        }

        $date_today = date('Y-m-d');

        $CI->db->where("DATE(invoice.invoice_due) <= '$date_today'");

        $CI->db->where('MONTH(invoice.created_at)', $month);
        $CI->db->where('YEAR(invoice.created_at)', date('Y'));
        $CI->db->where('invoice.id NOT IN (select invoice_id from transaction)');
        $query = $CI->db->get()->result();
        foreach ($query as $row) {
            $amount += invoice_total($row->id);
        }

        return $amount;
    }
}

/**
 * @param null $client_id
 * @return float|int
 *
 */
if (!function_exists('percentage_paid')) {
    function percentage_paid($client_id = null)
    {
        $amount_paid = 0;
        $invoice_amount = 0;
        $amount_paid = stats_amount_paid($client_id);
        $invoice_amount = _stats_amount_all($client_id);

        if ($invoice_amount > 0 && $amount_paid > 0) {
            return round(($amount_paid / $invoice_amount) * 100, 2);
        } else {

            return 0;
        }
    }
}

/**
 * @param null $client_id
 * @return float|int
 */
if (!function_exists('percentage_partial_paid')) {
    function percentage_partial_paid($client_id = null)
    {

        $amount_paid = 0;
        $invoice_amount = 0;
        $amount_paid = stats_amount_partial_paid($client_id);
        $invoice_amount = _stats_amount_all($client_id);

        if ($invoice_amount > 0 && $amount_paid > 0) {
            return round(($amount_paid / $invoice_amount) * 100, 2);
        } else {

            return 0;
        }
    }
}

/**
 * @param null $client_id
 * @return float|int
 *
 */
if (!function_exists('percentage_unpaid')) {
    function percentage_unpaid($client_id = null)
    {
        $amount_unpaid = 0;
        $invoice_amount = 0;
        $amount_unpaid = stats_amount_unpaid($client_id);
        $invoice_amount = _stats_amount_all($client_id);

        if ($invoice_amount > 0 && $amount_unpaid > 0) {
            return round(($amount_unpaid / $invoice_amount) * 100, 2);
        } else {

            return 0;
        }
    }
}

/**
 * @param null $client_id
 * @return float|int
 */
if (!function_exists('percentage_overdue')) {
    function percentage_overdue($client_id = null)
    {

        $amount_overdue = 0;
        $invoice_amount = 0;
        $amount_overdue = stats_amount_overdue($client_id);
        $invoice_amount = _stats_amount_all($client_id);

        if ($invoice_amount > 0 && $amount_overdue > 0) {
            return round(($amount_overdue / $invoice_amount) * 100, 2);
        } else {

            return 0;
        }
    }
}

/**
 * @param null $client_id
 * @return int
 */
if (!function_exists('_stats_amount_all')) {
    function _stats_amount_all($client_id = null)
    {

        $CI =& get_instance();

        if ($client_id) {
            $CI->db->where('client_id', $client_id);
        }
        $invoice_details = $CI->db->get('invoice')->result();
        $invoice_total = 0;
        if ($invoice_details) {

            foreach ($invoice_details as $invoice) {
                $invoice_total += invoice_total($invoice->id);
            }
        }

        return $invoice_total;
    }
}

/**
 * @param null $client_id
 * @return int
 */
if (!function_exists('stats_amount_paid')) {
    function stats_amount_paid($client_id = null)
    {
        $CI =& get_instance();
        if ($client_id) {
            $CI->db->where('client_id', $client_id);
        }
        $CI->db->where('(invoice.status= 1 OR invoice.status= 3)');
        $CI->db->join('transaction', 'transaction.invoice_id = invoice.id');
        $CI->db->group_by('invoice.id');
        $invoice_details = $CI->db->get('invoice')->result();
        $invoice_total = 0;
        if ($invoice_details) {

            foreach ($invoice_details as $invoice) {

                $invoice_total += _get_invoice_payments($invoice->invoice_id);
            }
        }

        return $invoice_total;
    }
}

/**
 * @param null $client_id
 * @return int
 */
if (!function_exists('stats_amount_unpaid')) {
    function stats_amount_unpaid($client_id = null)
    {

        $CI =& get_instance();
        if ($client_id) {
            $CI->db->where('client_id', $client_id);
        }
        $CI->db->where('status= 2');
        $CI->db->where('invoice_due >=  NOW()');
        $invoice_details = $CI->db->get('invoice')->result();
        $invoice_total = 0;
        if ($invoice_details) {
            foreach ($invoice_details as $invoice) {
                $invoice_total += invoice_total($invoice->id);
            }
        }

        return $invoice_total;
    }
}

/**
 * @param null $client_id
 * @return int
 */
if (!function_exists('stats_amount_partial_paid')) {
    function stats_amount_partial_paid($client_id = null)
    {

        $CI =& get_instance();
        $CI->db->select('*, invoice.id AS payment_id');
        if ($client_id) {
            $CI->db->where('client_id', $client_id);
        }
        $CI->db->where('invoice.status=3');
        $CI->db->join('transaction', 'transaction.invoice_id = invoice.id');
        $CI->db->group_by('invoice.id');
        $invoice_details = $CI->db->get('invoice')->result();
        $invoice_total = 0;
        if ($invoice_details) {

            foreach ($invoice_details as $invoice) {

                $invoice_total += _get_invoice_payments($invoice->invoice_id);
            }

        }

        return $invoice_total;
    }
}

/**
 * @param null $client_id
 * @return int
 */
if (!function_exists('stats_amount_overdue')) {
    function stats_amount_overdue($client_id = null)
    {


        $CI =& get_instance();

        if ($client_id) {
            $CI->db->where('client_id', $client_id);
        }
        $CI->db->where('status= 2');
        $date_today = date('Y-m-d');

        $CI->db->where("DATE(invoice_due) <= '$date_today'");

        $invoice_details = $CI->db->get('invoice')->result();
        $invoice_total = 0;
        if ($invoice_details) {

            foreach ($invoice_details as $invoice) {

                $invoice_total += invoice_total($invoice->id);
            }
        }

        return $invoice_total;
    }
}

/**
 * @param null $client_id
 * @return int
 */
if (!function_exists('client_amount_overdue')) {
    function client_amount_overdue($client_id = null)
    {

        $client_id = intval($client_id);
        $CI =& get_instance();
        $CI->db->where('client_id', $client_id);
        $CI->db->where('status= 2');
        $date_today = date('Y-m-d');

        $CI->db->where("DATE(invoice_due) <= '$date_today'");

        $invoice_details = $CI->db->get('invoice')->result();
        $invoice_total = 0;
        if ($invoice_details) {

            foreach ($invoice_details as $invoice) {

                $invoice_total += invoice_total($invoice->id);
            }
        }

        return $invoice_total;
    }
}

/**
 * calculated project estimated completion and completed time percentage
 * @param null $project_id
 * @return float|int
 *
 */
if (!function_exists('calculate_project_percentage')) {
    function calculate_project_percentage($project_id = null)
    {

        $project_id = intval($project_id);

        $estimated_hours = _planned_effort($project_id);
        $logged_hours = _completed_effort($project_id);
        $logged_hours = $logged_hours / 3600;

        if ($logged_hours > 0 && $estimated_hours > 0) {
            return round(($logged_hours / $estimated_hours) * 100);
        } else {

            return 0;
        }

    }
}

/**
 * @param null $project_id
 * @param null $user_id
 * @return mixed
 */
if (!function_exists('resource_planned_effort')) {
    function resource_planned_effort($project_id = null, $user_id = null)
    {

        $CI =& get_instance();
        $CI->db->select_sum('task_est_hours', 'planned_effort');
        $CI->db->join('task_assignment', 'task_assignment.task_id = tasks.task_id');
        $CI->db->where('task_assignment.user_id', $user_id);

        $CI->db->where('tasks.project_id', $project_id);

        $query = $CI->db->get('tasks')->result();
        foreach ($query as $row) {
            return $row->planned_effort;

        }

    }
}

/**
 * @param null $project_id
 * @param null $user_id
 * @return float
 */
if (!function_exists('resource_completed_effort')) {
    function resource_completed_effort($project_id = null, $user_id = null)
    {

        $CI =& get_instance();
        $CI->db->select("SUM(stop_timer - start_timer) AS time_spent");
        $CI->db->join('task_assignment', 'task_assignment.task_id = timer_entries.task_id');

        if($project_id){
            $CI->db->join('tasks', 'tasks.task_id = timer_entries.task_id');
            $CI->db->join('projects', 'tasks.project_id = projects.project_id');
            $CI->db->where('projects.project_id', $project_id);
        }
        $CI->db->where('task_assignment.user_id', $user_id);
        $CI->db->where('timer_entries.user_id', $user_id);

        $query = $CI->db->get('timer_entries');
        foreach ($query->result() as $row) {
            $time_spent = $row->time_spent ? $row->time_spent : 0;

            return ($time_spent / 3600);
        }

    }
}

/**
 * @param null $project_id
 * @return mixed
 */
if (!function_exists('_planned_effort')) {
    function _planned_effort($project_id = null)
    {

        $CI =& get_instance();

        $query = $CI->db->where('project_id', $project_id)->select_sum('task_est_hours',
            'planned_effort')->get('tasks')->result();
        foreach ($query as $row) {
            return $row->planned_effort;

        }
    }
}

/**
 * @param null $project_id
 * @return int
 */
if (!function_exists('_completed_effort')) {
    function _completed_effort($project_id = null)
    {
        $CI =& get_instance();

        $CI->db->select("SUM(stop_timer - start_timer) AS time_spent");
        $CI->db->where('project_id', $project_id);
        $query = $CI->db->get('timer_entries');
        foreach ($query->result() as $row) {
            $time_spent = $row->time_spent ? $row->time_spent : 0;

            return $time_spent;
        }

    }
}

/**
 * @param null $project_id
 * @return float
 */
if (!function_exists('project_hours')) {
    function project_hours($project_id = null)
    {
        $task_time = _completed_effort($project_id);
        $project_time = _calculate_project_time($project_id);
        $logged_time = ($task_time + $project_time) / 3600;

        return round($logged_time, 2);
    }
}

/**
 * @param $project_id
 * @return int|number
 */
if (!function_exists('_calculate_project_time')) {
    function _calculate_project_time($project_id)
    {
        $CI =& get_instance();
        $total_time = "SELECT stop_timer - start_timer AS time_spent FROM project_timer WHERE project_id =" . $project_id;
        $res = $CI->db->query($total_time)->result();
        $a = array();
        foreach ($res as $key => $t) {
            $a[] = $t->time_spent;
        }
        if (is_array($a)) {
            return array_sum($a);
        } else {
            return 0;
        }

    }
}

/**
 * @param null $project_id
 * @param null $user_id
 * @return null
 */
if (!function_exists('timer_already_logged')) {
    function timer_already_logged($project_id = null, $user_id = null)
    {

        $CI =& get_instance();
        $row = $CI->db->where(array(
            'project_id' => $project_id,
            'timer_by' => $user_id,
            'timer_active' => 0
        ))->get('project_timer')->row();
        if ($row) {
            return $row->stop_timer;
        }

        return null;

    }
}

/**
 * @param null $project_id
 * @return float
 */
if (!function_exists('get_work_done')) {
    function get_work_done($project_id = null)
    {

        return _completed_effort($project_id) / 3600;
    }
}

/**
 * @param null $project_id
 * @return mixed
 */
if (!function_exists('get_work_estimate')) {
    function get_work_estimate($project_id = null)
    {

        return _planned_effort($project_id);
    }
}

/**
 * @return mixed
 */
if (!function_exists('company_users_count')) {
    function company_users_count()
    {
        $CI =& get_instance();

        return $CI->db->count_all('users');
    }
}

/**
 * @param null $project_id
 * @return mixed
 */
if (!function_exists('assigned_users_count')) {
    function assigned_users_count($project_id = null)
    {
        $CI =& get_instance();
        $CI->db->where('project_id', $project_id);
        $CI->db->from('project_assignment');

        return $CI->db->count_all_results();
    }
}
/**
 * count all replies associated with selected message/mail
 * @param null $message_id
 * @return mixed
 *
 */
if (!function_exists('mail_replies_count')) {
    function mail_replies_count($message_id = null)
    {

        $CI =& get_instance();
        $CI->db->where('message_id', $message_id);
        $CI->db->from('message_replies');

        return $CI->db->count_all_results();
    }
}

/**
 * @param null $project_id
 * @return int
 */
if (!function_exists('_assigned_hourly_cost')) {
    function _assigned_hourly_cost($project_id = null)
    {

        $CI =& get_instance();
        $query = $CI->db->where('project_id', $project_id)->select_sum('hourly_cost',
            'hourly_rate')->get('project_assignment')->result();
        foreach ($query as $row) {
            return $row->hourly_rate ? $row->hourly_rate : 0;

        }

    }
}

/**
 * @param null $project_id
 * @return int
 */
if (!function_exists('_addition_cost_estimate')) {
    function _addition_cost_estimate($project_id = null)
    {

        $CI =& get_instance();
        $query = $CI->db->where(array('project_id' => $project_id, 'cost_type' => 2))->select_sum('estimate_cost',
            'estimate_cost')->get('costs')->result();
        foreach ($query as $row) {
            return $row->estimate_cost ? $row->estimate_cost : 0;

        }

    }
}

/**
 * @param null $project_id
 * @return int
 */
if (!function_exists('_addition_cost')) {
    function _addition_cost($project_id = null)
    {

        $CI =& get_instance();
        $query = $CI->db->where(array('project_id' => $project_id, 'cost_type' => 2))->select_sum('real_cost',
            'real_cost')->get('costs')->result();
        foreach ($query as $row) {
            return $row->real_cost ? $row->real_cost : 0;

        }

    }
}

/**
 * @param null $project_id
 * @return int
 */
if (!function_exists('_expense_budget')) {
    function _expense_budget($project_id = null)
    {

        $CI =& get_instance();
        $query = $CI->db->where(array('project_id' => $project_id, 'cost_type' => 1))->select_sum('estimate_cost',
            'estimate_cost')->get('costs')->result();
        foreach ($query as $row) {
            return $row->estimate_cost ? $row->estimate_cost : 0;

        }

    }
}

/**
 * @param null $project_id
 * @return int
 */
if (!function_exists('_expense_cost')) {
    function _expense_cost($project_id = null)
    {

        $CI =& get_instance();
        $query = $CI->db->where(array('project_id' => $project_id, 'cost_type' => 1))->select_sum('real_cost',
            'real_cost')->get('costs')->result();
        foreach ($query as $row) {
            return $row->real_cost ? $row->real_cost : 0;

        }

    }
}

/**
 * @param null $project_id
 * @return float
 */
if (!function_exists('project_total_done_costs')) {
    function project_total_done_costs($project_id = null)
    {

        $total_done = ((_completed_effort($project_id) / 3600) * _assigned_hourly_cost($project_id));
        $total_addition_real_cost = _addition_cost($project_id);
        $total_expense_real_cost = _expense_cost($project_id);

        return ($total_done + $total_addition_real_cost + $total_expense_real_cost);

    }
}

/**
 * @param null $project_id
 * @return int
 */
if (!function_exists('project_expense')) {
    function project_expense($project_id = null)
    {

        $total_addition_real_cost = _addition_cost($project_id);
        $total_expense_real_cost = _expense_cost($project_id);

        return ($total_addition_real_cost + $total_expense_real_cost);

    }
}

/**
 * @param null $project_id
 * @return mixed
 */
if (!function_exists('project_planned_costs')) {
    function project_planned_costs($project_id = null)
    {

        $total_estimate = (_planned_effort($project_id) * _assigned_hourly_cost($project_id));
        $total_addition_estimate = _addition_cost_estimate($project_id);
        $total_expense_budget = _expense_budget($project_id);

        return ($total_estimate + $total_addition_estimate + $total_expense_budget);

    }
}

/**
 * count project's completed tasks
 * @param null $project_id
 * @return int
 *
 */
if (!function_exists('complete_tasks')) {
    function complete_tasks($project_id = null)
    {
        $CI =& get_instance();
        $CI->db->where("project_id", $project_id);
        $CI->db->where("task_progress >= 100");
        $query = $CI->db->get('tasks');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
}

/**
 * count all tasks that belong to selected project
 * @param null $project_id
 * @return int
 */
if (!function_exists('project_tasks')) {
    function project_tasks($project_id = null)
    {

        $CI =& get_instance();
        $CI->db->where("project_id", $project_id);
        $query = $CI->db->get('tasks');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
}


/**
 * tasks completion percentage for a project
 * @param $project_id
 * @return float
 */
if (!function_exists('task_complete_perc')) {
    function task_complete_perc($project_id)
    {
        $task_active = complete_tasks($project_id);
        $project_tasks = project_tasks($project_id);
        if ($task_active < 1 OR $project_tasks < 1) {
            $perc_complete = 0;
        } else {
            $perc_complete = ($task_active / $project_tasks) * 100;
        }

        return round($perc_complete);
    }
}

/**
 * @param null $project_id
 * @return int
 */
if (!function_exists('issues_complete')) {
    function issues_complete($project_id = null)
    {
        $CI =& get_instance();
        $CI->db->where("project_id", $project_id);
        $CI->db->where("issue_status", 4);
        $query = $CI->db->get('issue');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
}

/**
 * @param null $project_id
 * @return int
 */
if (!function_exists('project_issues')) {
    function project_issues($project_id = null)
    {

        $CI =& get_instance();
        $CI->db->where("project_id", $project_id);

        $query = $CI->db->get('issue');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
}

/**
 * @param null $project_id
 * @return float
 */
if (!function_exists('issues_complete_perc')) {
    function issues_complete_perc($project_id = null)
    {
        $issues_complete = issues_complete($project_id);
        $project_issues = project_issues($project_id);
        if ($issues_complete < 1 OR $project_issues < 1) {
            $perc_complete = 0;
        } else {
            $perc_complete = ($issues_complete / $project_issues) * 100;
        }

        return round($perc_complete);
    }
}

/**
 * @param null $client_id
 * @return int
 */
if (!function_exists('client_completed_projects')) {
    function client_completed_projects($client_id = null)
    {
        $CI =& get_instance();
        if ($client_id) {
            $CI->db->where("client_id", $client_id);
        }
        $CI->db->where("project_status = 4 OR project_progress >=100");
        $query = $CI->db->get('projects');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
}

/**
 * count all projects that belong to selected client
 * @param null $client_id
 * @return int
 */
if (!function_exists('client_project')) {
    function client_project($client_id = null)
    {

        $CI =& get_instance();
        if ($client_id) {
            $CI->db->where("client_id", $client_id);
        }
        $query = $CI->db->get('projects');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
}

/**
 * @param null $client_id
 * @return float
 */
if (!function_exists('client_project_complete_perc')) {
    function client_project_complete_perc($client_id = null)
    {
        $project_complete = client_completed_projects($client_id);
        $projects = client_project($client_id);
        if ($project_complete < 1 OR $projects < 1) {
            $perc_complete = 0;
        } else {
            $perc_complete = ($project_complete / $projects) * 100;
        }

        return round($perc_complete);
    }
}

/**
 * @param $client_id
 * @return float
 */
if (!function_exists('client_sales_perc')) {
    function client_sales_perc($client_id)
    {

        $sales = client_invoice_total($client_id, ' ');
        $payments = client_payments($client_id, ' ');

        if ($sales < 1 OR $payments < 1) {
            $perc_complete = 0;
        } else {
            $perc_complete = ($payments / $sales) * 100;
        }

        return round($perc_complete);
    }
}


/**
 * count all tickets that belong to selected client
 * @param null $client_id
 * @return int
 */
if (!function_exists('client_tickets')) {
    function client_tickets($client_id = null)
    {

        $CI =& get_instance();
        $CI->db->where("ticket_client_id", $client_id);
        $query = $CI->db->get('ticket');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
}

/**
 * @param null $client_id
 * @return int
 */
if (!function_exists('client_completed_ticket')) {
    function client_completed_ticket($client_id = null)
    {
        $CI =& get_instance();
        $CI->db->where("ticket_client_id", $client_id);
        $CI->db->where("ticket_status", 4);
        $query = $CI->db->get('ticket');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
}

/**
 * @param null $client_id
 * @return float
 */
if (!function_exists('client_tickets_complete_perc')) {
    function client_tickets_complete_perc($client_id = null)
    {
        $tickets_complete = client_completed_ticket($client_id);
        $tickets = client_tickets($client_id);
        if ($tickets_complete < 1 OR $tickets < 1) {
            $perc_complete = 0;
        } else {
            $perc_complete = ($tickets_complete / $tickets) * 100;
        }

        return round($perc_complete);
    }
}


/**
 * count users assigned to selected task
 * @param null $task_id
 * @return int
 */
if (!function_exists('total_users_assigned_to_task')) {
    function total_users_assigned_to_task($task_id = null)
    {

        $CI =& get_instance();
        $CI->db->where("task_id", $task_id);
        $query = $CI->db->get('task_assignment');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }
}


/**
 * get assigned users to selected task
 * @param null $task_id
 * @return mixed
 */
if (!function_exists('task_assigned')) {
    function task_assigned($task_id = null)
    {

        $CI =& get_instance();
        $CI->db->from('task_assignment');
        $CI->db->where("task_id", $task_id);

        return $CI->db->get()->result();

    }
}

/**
 * get logged hours on selected task
 * @param null $task_id
 * @return float
 */
if (!function_exists('task_logged_hours')) {
    function task_logged_hours($task_id = null)
    {

        $CI =& get_instance();
        $CI->db->select("SUM(stop_timer - start_timer) AS time_spent");
        $CI->db->join('tasks', 'tasks.task_id = timer_entries.task_id');
        $CI->db->where('timer_entries.task_id', $task_id);
        $query = $CI->db->get('timer_entries');
        foreach ($query->result() as $row) {
            $time_spent = $row->time_spent ? $row->time_spent : 0;

            return ($time_spent / 3600);
        }

    }
}

/**
 * check if a project belongs to logged client
 * @param null $project_id
 * @param null $client_id
 * @return true/false
 */
if (!function_exists('pro_belongs_to_client')) {
    function pro_belongs_to_client($project_id = null, $client_id = null)
    {

        $CI =& get_instance();
        if ($project_id) {
            $project_id = intval($project_id);
            $CI->db->where("project_id", $project_id);

            if ($client_id) {
                $client_id = intval($client_id);
                $CI->db->where("client_id", $client_id);
            }
            $query = $CI->db->get('projects');
            if ($query->num_rows() > 0) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }
}

/**
 * checks if an invoice belong to a client
 * @param null $invoice_id
 * @param null $client_id
 * @return true/false
 */
if (!function_exists('inv_belongs_to_client')) {
    function inv_belongs_to_client($invoice_id = null, $client_id = null)
    {
        $invoice_id = intval($invoice_id);
        $CI =& get_instance();
        $CI->db->where("id", $invoice_id);

        if ($client_id) {
            $CI->db->where("client_id", $client_id);
        }
        $query = $CI->db->get('invoice');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}

/**
 * count all complete projects
 * @param null $project_id
 * @return int
 */
if (!function_exists('_complete_projects')) {
    function _complete_projects($project_id = null)
    {

        $CI =& get_instance();
        $CI->db->where("project_progress = 100");
        $CI->db->where("project_id", $project_id);
        $query = $CI->db->get('projects');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
}

/**
 * count all open projects
 * @param null $project_id
 * @return int
 */
if (!function_exists('_open_projects')) {
    function _open_projects($project_id = null)
    {

        $CI =& get_instance();
        $CI->db->where("project_progress < 100");
        $CI->db->where("project_id", $project_id);
        $query = $CI->db->get('projects');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
}


/**
 * count all complete projects assigned to selected user
 * @param null $user_id
 * @return int
 */
if (!function_exists('_user_complete_projects')) {
    function _user_complete_projects($user_id = null)
    {
        $user_id = intval($user_id);
        $CI =& get_instance();
        $CI->db->where('user_id', $user_id);

        $query = $CI->db->get('project_assignment');
        $complete = 0;
        foreach ($query->result() as $row) {
            $complete += _complete_projects($row->project_id);
        }

        return $complete;
    }
}

/**
 * count all open projects assigned to selected user
 * @param null $user_id
 * @return int
 */
if (!function_exists('_user_open_projects')) {
    function _user_open_projects($user_id = null)
    {
        $user_id = intval($user_id);
        $CI =& get_instance();
        $CI->db->where('user_id', $user_id);

        $query = $CI->db->get('project_assignment');
        $complete = 0;
        foreach ($query->result() as $row) {
            $complete += _open_projects($row->project_id);
        }

        return $complete;
    }
}

/**
 * count all projects assigned to a user
 * @param null $user_id
 * @return mixed .. total projects
 */
if (!function_exists('_user_assigned_projects')) {
    function _user_assigned_projects($user_id = null)
    {
        $CI =& get_instance();
        $CI->db->where('user_id', $user_id);
        $CI->db->from('project_assignment');

        return $CI->db->count_all_results();
    }
}

/**
 * calculate percentage of complete projects assigned to a user
 * @param null $user_id
 * @return float
 */
if (!function_exists('project_completion_percentage')) {
    function project_completion_percentage($user_id = null)
    {

        $complete_projects = _user_complete_projects($user_id);
        $projects = _user_assigned_projects($user_id);

        if ($complete_projects < 1 OR $projects < 1) {
            $perc_complete = 0;
        } else {
            $perc_complete = ($complete_projects / $projects) * 100;
        }

        return round($perc_complete);
    }
}

/**
 * @param $user_id
 * @return int
 */
if (!function_exists('user_open_projects')) {
    function user_open_projects($user_id)
    {
        return _user_open_projects($user_id);
    }
}

/**
 * @param null $user_id
 * @return int
 */
if (!function_exists('user_complete_projects')) {
    function user_complete_projects($user_id = null)
    {
        return _user_complete_projects($user_id);
    }
}

/**
 * @param null $user_id
 * @return mixed
 */
if (!function_exists('user_assigned_projects')) {
    function user_assigned_projects($user_id = null)
    {
        return _user_assigned_projects($user_id);
    }
}

/**
 * get logged hours
 * @param null $user_id
 * @return float|int
 */
if (!function_exists('_user_projects_logged_hours')) {
    function _user_projects_logged_hours($user_id = null)
    {
        $user_id = intval($user_id);
        $CI =& get_instance();
        $CI->db->where('user_id', $user_id);

        $query = $CI->db->get('project_assignment');
        $complete = 0;
        foreach ($query->result() as $row) {
            $complete += resource_completed_effort($row->project_id, $user_id);
        }

        return $complete;
    }
}

/**
 * user logged hours
 * @param null $user_id
 * @return float|int
 */
if (!function_exists('user_logged_hours')) {
    function user_logged_hours($user_id = null)
    {
        return _user_projects_logged_hours($user_id);
    }
}

/**
 * count all complete tasks checklists
 * @param null $task_id
 * @return mixed
 */
if (!function_exists('_complete_check_list')) {
    function _complete_check_list($task_id = null)
    {
        $CI =& get_instance();
        $CI->db->where('finished = 1');
        $CI->db->where('task_id', $task_id);
        $CI->db->from('check_lists');

        return $CI->db->count_all_results();
    }
}

/**
 * count all tasks checklists
 * @param null $task_id
 * @return mixed
 */
if (!function_exists('_total_check_list')) {
    function _total_check_list($task_id = null)
    {
        $CI =& get_instance();
        $CI->db->where('task_id', $task_id);
        $CI->db->from('check_lists');

        return $CI->db->count_all_results();

    }
}

/**
 * get checklist completion percentage
 * @param null $task_id
 * @return float
 */
if (!function_exists('check_list_complete_percentage')) {
    function check_list_complete_percentage($task_id = null)
    {

        $complete_check_list = _complete_check_list($task_id);
        $tot_check_list = _total_check_list($task_id);

        if ($complete_check_list < 1 OR $tot_check_list < 1) {
            $perc_complete = 0;
        } else {
            $perc_complete = ($complete_check_list / $tot_check_list) * 100;
        }

        return round($perc_complete);
    }
}

/**
 * get project issues statistics
 * @param $project_id
 * @return array
 */
if (!function_exists('_project_issues_stats')) {
    function _project_issues_stats($project_id)
    {

        $issue_array = array();
        $closed_issues = issues_complete($project_id);
        $all_issues = project_issues($project_id);
        $open_issues = ($all_issues - $closed_issues);
        $issue_array['label'] = "Open issues " . $open_issues . "/" . $all_issues;
        $issue_array['value'] = $open_issues;

        return $issue_array;
    }
}

/**
 * get project tasks statistics
 * @param $project_id
 * @return array
 */
if (!function_exists('_project_tasks_stats')) {
    function _project_tasks_stats($project_id)
    {

        $tasks_array = array();
        $task_complete = complete_tasks($project_id);
        $all_tasks = project_tasks($project_id);

        $open_tasks = ($all_tasks - $task_complete);
        $tasks_array['label'] = "Open tasks " . $open_tasks . "/" . $all_tasks;
        $tasks_array['value'] = $open_tasks;

        return $tasks_array;
    }
}


/**
 * get project risks stats
 * @param $project_id
 * @return array
 */
if (!function_exists('_project_risk_stats')) {
    function _project_risk_stats($project_id)
    {

        $CI =& get_instance();
        $CI->db->where("project_id", $project_id);
        $CI->db->where("probability >30");
        $CI->db->where("impact_percentage >=31");
        $CI->db->where("impact_percentage <=100");
        $risks = 0;
        $query = $CI->db->get('project_risks');
        if ($query->num_rows() > 0) {
            $risks = $query->num_rows();
        } else {
            $risks = 0;
        }

        $risks_array = array();

        $risks_array['label'] = "Risks Count ";
        $risks_array['value'] = $risks;

        return $risks_array;

    }
}

/**
 * get project statistics
 * @param null $project_id
 * @return array
 */
if (!function_exists('project_stats')) {
    function project_stats($project_id = null)
    {

        $issue_array = _project_issues_stats($project_id);
        $tasks_array = _project_tasks_stats($project_id);
        $risks_array = _project_risk_stats($project_id);
        $json_data = array();


        array_push($json_data, $issue_array, $tasks_array, $risks_array);

        return $json_data;
    }
}

/**
 * @return array
 */
if (!function_exists('_project_expense_stats')) {
    function _project_expense_stats()
    {

        $CI =& get_instance();
        $CI->db->where("cost_type = 1");
        $costs = $CI->db->select_sum('real_cost')->where('cost_type = 1')->get('costs')->row();
        $real_amount = 0;
        if ($costs) {
            $real_amount = $costs->real_cost;
        }

        $expense_array = array();
        $expense_array['label'] = "Gastos Proyecto";
        $expense_array['value'] = $real_amount;

        return $expense_array;

    }
}

/**
 * @return array
 */
if (!function_exists('_general_expense_stats')) {
    function _general_expense_stats()
    {

        $CI =& get_instance();

        $costs = $CI->db->select_sum('real_cost')->where('cost_type <> 1')->get('costs')->row();
        $real_amount = 0.00;
        if ($costs) {
            $real_amount = $costs->real_cost;
        }
        $expense_array = array();
        $expense_array['label'] = "Gastos Oficina";
        $expense_array['value'] = $real_amount;

        return $expense_array;

    }
}

/**
 * @return array
 */
if (!function_exists('_estimated_expenses_stats')) {
    function _estimated_expenses_stats()
    {

        $CI =& get_instance();

        $costs = $CI->db->select_sum('estimate_cost')->get('costs')->row();
        $estimate_amount = 0;
        if ($costs) {
            $estimate_amount = $costs->estimate_cost;
        }
        $expense_array = array();
        $expense_array['label'] = "Gastos Estimados";
        $expense_array['value'] = $estimate_amount;

        return $expense_array;

    }
}

/**
 * get expense statistics based on projects and estimates
 * @return array
 */
if (!function_exists('expense_stats')) {
    function expense_stats()
    {
        $project_array = _project_expense_stats();
        $general_array = _general_expense_stats();
        $estimation_array = _estimated_expenses_stats();
        $json_data = array();


        array_push($json_data, $project_array, $general_array, $estimation_array);

        return $json_data;
    }
}


/**
 * get payments statistics based on payment methods
 * @return array
 */
if (!function_exists('payments_stats')) {
    function payments_stats()
    {

        $CI =& get_instance();
        $CI->db->select("*,SUM(amount) AS amount");
        $CI->db->group_by('payment_method');
        $query = $CI->db->get('transaction');

        $json_data = array();
        foreach ($query->result() as $row) {
            $pay_array = array();
            $pay_array['label'] = payment_method($row->payment_method);
            $pay_array['value'] = $row->amount;

            array_push($json_data, $pay_array);
        }

        return $json_data;
    }
}


/**
 * get total expense amount
 * @param null $client_id
 * @return int
 */
if (!function_exists('tot_expense_amount')) {
    function tot_expense_amount($client_id = null)
    {
        $CI =& get_instance();

        if ($client_id) {

            $CI->db->join('projects', 'projects.project_id = costs.project_id');
            $CI->db->where('projects.client_id', $client_id);
            $costs = $CI->db->select_sum('real_cost')->get('costs')->row();
        } else {
            $costs = $CI->db->select_sum('real_cost')->get('costs')->row();
        }

        $cost_amount = 0;
        if ($costs) {
            $cost_amount = $costs->real_cost;
        }

        return $cost_amount;
    }
}

/**
 * get expense and invoice percentages
 * @param null $client_id
 * @return float|int .. percentage
 */
if (!function_exists('percentage_expenses')) {
    function percentage_expenses($client_id = null)
    {

        $amount_expense = tot_expense_amount($client_id);
        $invoice_amount = _stats_amount_all($client_id);

        if ($invoice_amount > 0 && $amount_expense > 0) {
            return round(($amount_expense / $invoice_amount) * 100);
        } else {

            return 0;
        }
    }
}

/**
 * calculate expense amounts based on categories
 * @return array
 */
if (!function_exists('expense_category_stats')) {
    function expense_category_stats()
    {
        $CI =& get_instance();
        $CI->db->select("*,SUM(real_cost) AS real_cost");
        $CI->db->group_by('classification');
        $query = $CI->db->get('costs');
        $json_data = array();
        foreach ($query->result() as $row) {

            $class_array = array(get_category_name($row->classification), doubleval($row->real_cost));
            array_push($json_data, $class_array);
        }

        return $json_data;
    }
}

/**
 * calculate expense and budget percentage
 * @param $project_id
 * @return stdClass.. object of percentage,budget and expenses
 *
 */
if (!function_exists('calculate_expense_budget_percentage')) {
    function  calculate_expense_budget_percentage($project_id)
    {

        $expense_amount = _expense_cost($project_id);
        $budget_amount = get_project_details($project_id)->project_budget;
        $perc_complete = 0;
        if ($expense_amount < 1 OR $budget_amount < 1) {
            $perc_complete = 0;
        } else {
            $perc_complete = ($expense_amount / $budget_amount) * 100;
        }

        $expense_info = new stdClass();
        $expense_info->percentage = round($perc_complete);
        $expense_info->budget = $budget_amount;
        $expense_info->exp_amount = $expense_amount;

        return $expense_info;

    }
}

/**
 * get count of all active clients
 * @return int
 */
if (!function_exists('active_clients')) {
    function active_clients()
    {
        $CI =& get_instance();
        $CI->db->where("client_status", 1);
        $query = $CI->db->get('client');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }
    function active_proveedores()
    {
        $CI =& get_instance();
        $CI->db->where("client_status", 1);
        $CI->db->where("client_type", 2);
        $query = $CI->db->get('client');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }
    function active_contratistas()
    {
        $CI =& get_instance();
        $CI->db->where("client_status", 1);
        $CI->db->where("client_type", 3);
        $query = $CI->db->get('client');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }

    }
}

/**
 * get count of all clients that are not active
 * @return int
 *
 */
if (!function_exists('inactive_client')) {
    function inactive_client()
    {

        $CI =& get_instance();
        $CI->db->where("client_status <>1");
        $query = $CI->db->get('client');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
    function inactive_proveedor()
    {

        $CI =& get_instance();
        $CI->db->where("client_status <>1");
          $CI->db->where("client_type", 2);
        $query = $CI->db->get('client');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
    function inactive_contratista()
    {

        $CI =& get_instance();
        $CI->db->where("client_status <>1");
          $CI->db->where("client_type", 3);
        $query = $CI->db->get('client');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
}

/**
 * get all client with projects
 * @return int.. count of clients
 */
if (!function_exists('clients_with_projects')) {
    function clients_with_projects()
    {

        $CI =& get_instance();
        $CI->db->join('client', 'client.client_id = projects.client_id');
        $CI->db->group_by('projects.client_id');
        $query = $CI->db->get('projects');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
    function proveedor_with_projects()
    {

        $CI =& get_instance();
        $CI->db->join('client', 'client.client_id = projects.client_id');
        $CI->db->group_by('projects.client_id');
        $CI->db->where("client_type", 2);
        $query = $CI->db->get('projects');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
    function contratistas_with_projects()
    {

        $CI =& get_instance();
        $CI->db->join('client', 'client.client_id = projects.client_id');
        $CI->db->group_by('projects.client_id');
        $CI->db->where("client_type", 3);
        $query = $CI->db->get('projects');
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return 0;
        }
    }
}

/**
 * Get invoice details
 * @param $invoice_id
 * @return mixed.. invoice info
 */
if (!function_exists('get_client_invoice_details')) {
    function get_client_invoice_details($invoice_id)
    {
        $CI =& get_instance();
        $CI->db->from("invoice");
        $CI->db->join('client', 'client.client_id =invoice.client_id');
        $CI->db->where("invoice.id= " . intval($invoice_id));
        $results = $CI->db->get()->row();

        return $results;
    }
}

/**
 * get client tickets statistics
 * @param null $client_id
 * @return array
 *
 */
if (!function_exists('client_ticket_stats')) {
    function client_ticket_stats($client_id = null)
    {
        $CI =& get_instance();
        $CI->db->select("*,COUNT(*) as rankcount");
        if ($client_id) {
            $CI->db->where("ticket_client_id", intval($client_id));
        }
        $CI->db->group_by('ticket_status');
        $query = $CI->db->get('ticket');
        $json_data = array();
        foreach ($query->result() as $row) {

            if ($row->ticket_status) {
                $class_array = array();
                $class_array['label'] = get_ticket_status($row->ticket_status);
                $class_array['value'] = $row->rankcount;
                array_push($json_data, $class_array);
            }
        }

        return $json_data;
    }
}

/**
 * get task details
 * @param null $client_id
 * @return mixed
 */
if (!function_exists('get_task_history')) {
    function get_task_history($client_id = null)
    {

        $CI =& get_instance();
        $CI->db->from("tasks");
        $CI->db->join('projects', 'tasks.project_id = projects.project_id');
        if ($client_id) {
            $CI->db->where("projects.client_id", $client_id);
        }

        return $CI->db->get()->result();
    }
}

/**
 * @return array
 */
if (!function_exists('_count_estimates')) {
    function _count_estimates()
    {

        $CI =& get_instance();
        $query = $CI->db->get('estimate');
        if ($query->num_rows() > 0) {
            $estimates = $query->num_rows();
        } else {
            $estimates = 0;
        }

        $estimate_array = array();

        $estimate_array['label'] = "Estimates Count ";
        $estimate_array['value'] = $estimates;

        return $estimate_array;

    }
}

/**
 * @return array
 */
if (!function_exists('_count_invoices')) {
    function _count_invoices()
    {

        $CI =& get_instance();
        $query = $CI->db->get('invoice');
        if ($query->num_rows() > 0) {
            $inv = $query->num_rows();
        } else {
            $inv = 0;
        }

        $inv_array = array();

        $inv_array['label'] = "Invoices Count ";
        $inv_array['value'] = $inv;

        return $inv_array;

    }
}

/**
 * @return array
 */
if (!function_exists('_count_quotations')) {
    function _count_quotations()
    {

        $CI =& get_instance();
        $query = $CI->db->get('quotation');
        if ($query->num_rows() > 0) {
            $quote = $query->num_rows();
        } else {
            $quote = 0;
        }

        $quote_array = array();

        $quote_array['label'] = "Quotations Count ";
        $quote_array['value'] = $quote;

        return $quote_array;

    }
}

/**
 * get application total estimates,quotations and invoices
 * @param null $client_id
 * @return array
 */
if (!function_exists('admin_rev_stats')) {
    function admin_rev_stats($client_id = null)
    {

        $count_estimates = _count_estimates();
        $count_invoices = _count_invoices();
        $count_quotations = _count_quotations();

        $json_data = array();


        array_push($json_data, $count_estimates, $count_invoices, $count_quotations);

        return $json_data;

    }
}

/**
 * @param null $user_id
 * @return array
 */
if (!function_exists('_count_client_user_mail')) {
    function _count_client_user_mail($user_id = null)
    {

        $CI =& get_instance();

        $CI->db->where("client_from", $user_id);
        $query = $CI->db->get('messages');
        if ($query->num_rows() > 0) {
            $msg = $query->num_rows();
        } else {
            $msg = 0;
        }

        $mail_array = array();

        $mail_array['label'] = "Sent Mail";
        $mail_array['value'] = $msg;

        return $mail_array;

    }
}

/**
 * @param null $user_id
 * @return array
 */
if (!function_exists('_count_client_user_tickets')) {
    function _count_client_user_tickets($user_id = null)
    {

        $CI =& get_instance();
        $CI->db->where("ticket_by", $user_id);
        $query = $CI->db->get('ticket');
        if ($query->num_rows() > 0) {
            $ticket = $query->num_rows();
        } else {
            $ticket = 0;
        }

        $ticket_array = array();

        $ticket_array['label'] = "Reported  Tickets";
        $ticket_array['value'] = $ticket;

        return $ticket_array;

    }
}

/**
 * get client sent email,created tickets statistics
 * @param null $user_id
 * @return array
 */
if (!function_exists('client_user_stats')) {
    function client_user_stats($user_id = null)
    {

        $sent_emails = _count_client_user_mail($user_id);
        $reported_tickets = _count_client_user_tickets($user_id);

        $json_data = array();

        array_push($json_data, $sent_emails, $reported_tickets);

        return $json_data;

    }
}


/**
 * @return array
 */
if (!function_exists('_estimate_amount')) {
    function _estimate_amount()
    {

        $CI =& get_instance();
        $est_stats = $CI->db->get('estimate')->result();
        $estimate_amount = 0.00;
        if ($est_stats) {
            foreach ($est_stats as $est) {
                $estimate_amount += calculate_estimate_total($est->est_id);
            }

        }

        return array("name" => "Estimate", "y" => doubleval($estimate_amount));
    }
}

/**
 * @return array
 */
if (!function_exists('_income_amount')) {
    function _income_amount()
    {

        $CI =& get_instance();
        $CI->db->from("transaction");
        $CI->db->select_sum('amount', 'paid_amount');
        $CI->db->join('invoice', 'invoice.id=transaction.invoice_id');
        $invoice_details = $CI->db->get()->result();
        $invoice_total = 0;
        if ($invoice_details) {

            foreach ($invoice_details as $invoice) {

                $invoice_total += $invoice->paid_amount;

            }

        }

        return array("name" => "Income", "y" => doubleval($invoice_total), 'sliced' => true, 'selected' => true);
    }
}

/**
 * compare estimate to invoiced(paid) amounts
 * @return array
 */
if (!function_exists('estimates_invoice')) {
    function estimates_invoice()
    {
        $estimate_amount = _estimate_amount();
        $income_amount = _income_amount();
        $json_data = array();
        array_push($json_data, $estimate_amount, $income_amount);

        return $json_data;
    }
}

/**
 * get project settings / permissions
 * @param $setting
 * @param null $project_id
 * @return bool.. true/false
 */

if (!function_exists('project_setting')) {
    function project_setting($setting, $project_id = null)
    {
        $pro_details = get_project_details($project_id);
        if ($pro_details->project_settings == null) {
            $curr_project_settings = '{"settings":"on"}';
        } else {
            $curr_project_settings = $pro_details->project_settings;
        }

        $project_settings = json_decode($curr_project_settings);
        if (array_key_exists($setting, $project_settings)) {
            return true;
        } else {
            return false;
        }
    }
}

/**
 * check for application updates
 * @param bool $remote
 * @return mixed
 */
if (!function_exists('check_updates')) {
    function check_updates($remote = false)
    {

        $CI =& get_instance();

        $version = $CI->config->item('system')->build;
        $last_check = $CI->config->item('system')->last_check;
        $purchase_code = $CI->config->item('system')->purchase_code;


        if ((time() - $last_check) > 20 * 3600) {
            $remote = true;
        }
        if ($remote) {

            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => UPDATE_URL . "updates/updates.php?version=" . $version . "&code=" . $purchase_code,
                CURLOPT_RETURNTRANSFER => true
            ));
            $output = curl_exec($ch);
            curl_close($ch);

            $updates = json_decode($output, true);

            foreach ($updates as $up) {

                $exists = $CI->db->where('build', $up['build'])->get('system_updates')->result();
                if (count($exists) == 0) {
                    $up['tran_date'] = date("Y-m-d H:i:s", time());
                    $up['installed'] = 0;
                    $CI->db->insert('system_updates', $up);
                } elseif (count($exists) == 1) {
                    $CI->db->where('build', $up['build'])->update('system_updates', array(
                        'title' => $up['title'],
                        'description' => $up['description']
                    ));
                }
            }
            $CI->db->where('settings_id', 1)->update('settings_system', array('last_check' => time()));
        }

        return $CI->db->where('installed', '0')->order_by('build', 'desc')->get('system_updates')->result();
    }
}

/**
 * validate user purchase code
 * @return mixed|string
 */
if (!function_exists('valid_license')) {
    function valid_license()
    {
        $CI =& get_instance();
        $purchase_code = $CI->config->item('system')->purchase_code;

        return remote_get_contents(UPDATE_URL . 'check/verify.php?code=' . $purchase_code);
    }
}

/**
 * get all projects with active timer
 * @param null $client_id
 * @return object array of projects
 */
if (!function_exists('get_running_projects')) {
    function get_running_projects($client_id = null)
    {

        $CI =& get_instance();
        $CI->db->from("projects");
        $CI->db->join('project_timer', 'project_timer.project_id = projects.project_id');
        if ($client_id) {
            $CI->db->where("projects.client_id", $client_id);
        }
        $CI->db->where("project_timer.timer_active", 1);

        $row = $CI->db->get()->result();
        if ($row) {
            return $row;
        }

        return null;
    }
}

/**
 * get sales forecast for the month
 * @return float.. forecast value
 */
if (!function_exists('sales_forecast')) {
    function sales_forecast()
    {

        $CI =& get_instance();
        $dbt = date("Y-m-d");
        $Datt = explode("-", $dbt);
        $DB1 = $Datt['0'];
        $DB2 = $Datt['1'];
        $DB3 = $Datt['2'];


        $numberOfDays = cal_days_in_month(CAL_GREGORIAN, $DB2, $DB1); // 31

        $Total_sales = stats_sales_monthly_data($DB2, null, $CI->Company_id);
        $MthAvg = $Total_sales / $DB3;//02/03/04
        $MthExp = $MthAvg * $numberOfDays;// 30;
        $MthExpected = round($MthExp);

        return $MthExpected;
    }
}

?>
