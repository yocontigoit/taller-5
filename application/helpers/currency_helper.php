<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/09/11
 * Time: 10:22 AM
 */


/**
 * convert a number to currency format
 * @param $amount
 * @return string  with currency symbol
 */
if (!function_exists('format_currency')) {
    function format_currency($amount)
    {

        $CI =& get_instance();
        $currency_symbol = get_currency_symbol($CI->config->item('local')->company_currency);
        $currency_symbol_placement = $CI->config->item('system')->currency_symbol_placement;
        $thousands_separator = format_thousands_separator($CI->config->item('system')->company_thousand_separator);
        $decimal_point = $CI->config->item('system')->currency_decimal_point;

        if ($currency_symbol_placement == 'before') {
            return $currency_symbol . number_format($amount, ($decimal_point) ? 2 : 0, $decimal_point,
                $thousands_separator);
        } elseif ($currency_symbol_placement == 'afterspace') {
            return number_format($amount, ($decimal_point) ? 2 : 0, $decimal_point,
                $thousands_separator) . "&nbsp;" . $currency_symbol;
        } else {
            return number_format($amount, ($decimal_point) ? 2 : 0, $decimal_point,
                $thousands_separator) . $currency_symbol;
        }
    }
}


/**
 * convert a number to currency format
 * @param null $amount
 * @return string currency without symbol
 *
 */
if (!function_exists('format_amount')) {
    function format_amount($amount = null)
    {
        $CI =& get_instance();
        $thousands_separator = format_thousands_separator($CI->config->item('system')->company_thousand_separator);
        $decimal_point = $CI->config->item('system')->currency_decimal_point;
        if ($amount) {

            return number_format($amount, ($decimal_point) ? 2 : 0, $decimal_point, $thousands_separator);
        } else {
            return number_format(0, ($decimal_point) ? 2 : 0, $decimal_point, $thousands_separator);
        }

    }
}
/**
 * @param $thousands_separator
 * @return string
 */
if (!function_exists('format_thousands_separator')) {
    function format_thousands_separator($thousands_separator)
    {
        if (preg_match("/\\s+/", $thousands_separator)) {
            $thousands_separator = "&nbsp;";
        }

        return $thousands_separator;
    }
}


/**
 * round amount to avoid 1 cents
 * @param $amount
 * @return float rounded amount
 */
if (!function_exists('round_to_nearest_05')) {
    function round_to_nearest_05($amount)
    {
        return round($amount * 2, 1) / 2;
    }
}

/**
 * validate value
 * @param $val
 * @return int|string
 *
 */
if (!function_exists('to_quantity')) {
    function to_quantity($val)
    {
        if ($val !== null) {
            return $val == (int)$val ? (int)$val : rtrim($val, '0');
        }

        return lang('common_not_set');
    }
}


?>
