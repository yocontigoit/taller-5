<?php
/***
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/03/05
 * Time: 08:54 AM
 */


/**
 * retrieve all the css files to run the application
 * @return array of css files
 */
if (!function_exists('get_css_files')) {

    function get_css_files()
    {
        return array(
            array('path' => 'assets/css/bootstrap.min.css', 'media' => 'all'),
            array('path' => 'assets/font-awesome-4.5.0/css/font-awesome.min.css', 'media' => 'all'),
            array('path' => 'assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css', 'media' => 'all'),
            array('path' => 'assets/css/chat.css', 'media' => 'all'),
            array('path' => 'assets/plugins/morris-chart/morris.min.css', 'media' => 'all'),
            array('path' => 'assets/plugins/nifty-modal/css/component.css', 'media' => 'all'),
            array('path' => 'assets/plugins/ios7-switch/ios7-switch.css', 'media' => 'all'),
            array('path' => 'assets/plugins/chosen/chosen.min.css', 'media' => 'all'),
            array('path' => 'assets/plugins/slider/slider.min.css', 'media' => 'all'),
            array('path' => 'assets/plugins/toastr/toastr.css', 'media' => 'all'),
            array('path' => 'assets/css/dashboard.css', 'media' => 'all'),
            array('path' => 'assets/css/sb-admin.css', 'media' => 'all'),
            array('path' => 'assets/css/default.css', 'media' => 'all'),
            array('path' => 'assets/css/style-responsive.css', 'media' => 'all')
        );
    }
}

/**
 * retrieve all the gantt chats css files to run the application
 * @return array
 */

if (!function_exists('get_gantt_css_files')) {
    function get_gantt_css_files()
    {
        return array(
            array('path' => 'assets/css/gantt/platform.css', 'media' => 'all'),
            array('path' => 'assets/plugins/gantt-chat/dateField/jquery.dateField.css', 'media' => 'all'),
            array('path' => 'assets/css/gantt/gantt.css', 'media' => 'all'),
            array('path' => 'assets/css/gantt/ganttPrint.css', 'media' => 'all'),


        );

    }
}


/**
 * retrieve all the gantt chats js files to run the application
 * @return array
 */
if (!function_exists('get_gantt_js_files')) {
    function get_gantt_js_files()
    {

        return array(
            array('path' => 'assets/plugins/gantt-chat/jquery.livequery.min.js'),
            array('path' => 'assets/plugins/gantt-chat/jquery.timers.js'),
            array('path' => 'assets/plugins/gantt-chat/platform.js'),
            array('path' => 'assets/plugins/gantt-chat/date.js'),
            array('path' => 'assets/plugins/gantt-chat/i18nJs.js'),
            array('path' => 'assets/plugins/gantt-chat/dateField/jquery.dateField.js'),
            array('path' => 'assets/plugins/gantt-chat/JST/jquery.JST.js'),
            array('path' => 'assets/plugins/gantt-chat/jquery.svg.min.js'), // Do we use this?
            array('path' => 'assets/plugins/gantt-chat/jquery.svgdom.1.8.js'),
            array('path' => 'assets/js/gantt/partColorValueChooser.js'),
            array('path' => 'assets/js/gantt/ganttUtilities.js'),
            array('path' => 'assets/js/gantt/ganttTask.js'),
            array('path' => 'assets/js/gantt/ganttDrawerSVG.js'),
            array('path' => 'assets/js/gantt/ganttGridEditor.js'),
            array('path' => 'assets/js/gantt/ganttMaster.js')
        );

    }
}

/**
 * retrieve all the js files to run the application,excluding gantt files
 * @return array
 *
 */
if (!function_exists('get_js_files')) {
    function get_js_files()
    {

        return array(
            array('path' => 'assets/libs/jquery-2.0.2.min.js'),
            array('path' => 'assets/libs/jquery-ui-1.10.3.min.js'),
            array('path' => 'assets/js/jquery-migrate.js'),
            array('path' => 'assets/js/bootstrap.min.js'),
            array('path' => 'assets/plugins/nicescroll/jquery.nicescroll.js'),
            array('path' => 'assets/plugins/chosen/chosen.jquery.min.js'),
            array('path' => 'assets/plugins/ios7-switch/ios7.switch.js'),
            array('path' => 'assets/plugins/nifty-modal/js/classie.js'),
            array('path' => 'assets/plugins/nifty-modal/js/modalEffects.js'),
            array('path' => 'assets/plugins/slider/bootstrap-slider.js'),
            array('path' => 'assets/plugins/toastr/toastr.js'),
            array('path' => 'assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js'),
            array('path' => 'assets/js/app.js'),
            array('path' => 'assets/js/custom.js'),
            array('path' => 'assets/js/zest_chat.js')
        );
    }
}