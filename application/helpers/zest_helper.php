<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/18
 * Time: 02:21 PM
 */

/**
 * check if directory is writable
 * @return true/false
 */
if (!function_exists('is_writable_cache')) {
    function is_writable_cache()
    {
        return is_writable('cache/');
    }
}

/**
 * incode the string
 * @param $text
 * @return encoded string
 */
if (!function_exists('econde')) {
    function econde($text)
    {
        $text = str_ireplace(" ", "+", $text);
        //$text	= str_ireplace("-","%20", ($text));
        $text = str_ireplace("-", "+", ($text));
        $text = str_ireplace("%252c", ",", $text);
        $text = str_ireplace("%2c", ",", $text);
        $text = str_ireplace("%2527", "'", $text);
        $text = str_ireplace("%26", "and", $text);
        $text = str_ireplace("&", "and", $text);

        return $text;

    }
}


/**
 * converts string back to readable format
 * @param $text
 * @return mixed|string
 */
if (!function_exists('decode')) {
    function decode($text)
    {
        $text = str_ireplace(":", " ", $text);
        $text = str_ireplace("+", " ", $text);
        $text = str_ireplace("-", " ", $text);
        $text = str_ireplace("/", " - ", $text);
        $text = str_ireplace("%20", " ", $text);
        $text = urldecode($text);

        return $text;
    }
}

/**
 * secure strings
 * @param $text
 * @return string
 */
if (!function_exists('secure')) {
    function secure($text)
    {
        $text = str_ireplace("'", "", $text);
        $text = str_ireplace("+", "", $text);
        $text = str_ireplace("-", "", $text);
        $text = str_ireplace("=", "", $text);
        $text = str_ireplace("/", "-", $text);
        $text = str_ireplace("%20", "", $text);
        $text = str_ireplace("%", "", $text);
        $text = urldecode($text);
        $text = strip_tags($text);

        return $text;
    }
}

/**
 * get user time difference compared to current time
 * @param $time
 * @return string
 */
if (!function_exists('ago')) {
    function ago($time)
    {
        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
        $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

        $now = time();

        $difference = $now - $time;


        for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);

        if ($difference != 1) {
            $periods[$j] .= "s";
        }

        return "$difference " . $periods[$j];
    }
}

/**
 * get number of days between two dates
 * @param null $start_date
 * @param null $end_date
 * @param bool $week_ends
 * @return int|number
 */
if (!function_exists('duration_diff')) {
    function duration_diff($start_date = null, $end_date = null, $week_ends = false)
    {
        if ($week_ends) {
            $datediff = 0;

            $now = strtotime($start_date);
            $your_date = strtotime($end_date);
            $datediff = $now - $your_date;

            return abs(floor($datediff / (60 * 60 * 24)));
        } else {

            $start = strtotime($start_date);
            $end = strtotime($end_date);

            $count = 0;

            while (date('Y-m-d', $start) < date('Y-m-d', $end)) {
                $count += date('N', $start) < 6 ? 1 : 0;
                $start = strtotime("+1 day", $start);
            }

            return $count;
        }
    }
}

/**
 * get number of days between two dates in hours
 * @param null $start_date
 * @param null $end_date
 * @param bool $week_ends
 * @return int|number
 */
if (!function_exists('duration_diff_hours')) {
    function duration_diff_hours($start_date = null, $end_date = null, $week_ends = false)
    {


        if ($week_ends) {

            return round((strtotime($start_date) - strtotime($end_date)) / 3600, 1);

        } else {
            $timestamp1 = strtotime($start_date);
            $timestamp2 = strtotime($end_date);

            $weekend = array(0, 6);
            $to_remove = 0;
            if (in_array(date("w", $timestamp1), $weekend)) {
                //one of the dates is weekend, return 0?
                // $to_remove++;
            } elseif (in_array(date("w", $timestamp2), $weekend)) {
                // $to_remove++;

            }

            $diff = $timestamp2 - $timestamp1;
            $one_day = 60 * 60 * 24; //number of seconds in the day

            if ($diff < $one_day) {
                return floor($diff / 3600);
            }

            $days_between = floor($diff / $one_day);
            $remove_days = 0;

            for ($i = 1; $i <= $days_between; $i++) {
                $next_day = $timestamp1 + ($i * $one_day);
                if (in_array(date("w", $next_day), $weekend)) {
                    $remove_days++;
                }
            }

            return floor(($diff - (($remove_days + $to_remove) * $one_day)) / 3600);
        }
    }
}

/**
 * get number of that have passed since posted date
 * @param null $date
 * @return string
 */
if (!function_exists('time_elapse')) {
    function  time_elapse($date = null)
    {
        $date1 = new DateTime('now');
        $date2 = new DateTime($date);
        $interval = $date1->diff($date2);

        // shows the total amount of days (not divided into years, months and days like above)
        return $interval->d . " days " . $interval->h . " hours";
    }
}


/**
 * convert a datetime string to relative time
 * @param null $date
 * @return bool|string
 */
if (!function_exists('readable_date_format')) {
    function readable_date_format($date = null)
    {
        // $theDate = date("Y-m-d",strtotime($v));				// 2014-10-31
        // $theDate = date("m-d-Y",strtotime($v));				// 10-31-2014
        $theDate = date("F d, Y", strtotime($date));                // October 31, 2014
        return $theDate;
    }
}

/**
 * convert date from unix to readable date format
 * @param $timestamp
 * @return string
 */
if (!function_exists('date_from_timestamp')) {
    function date_from_timestamp($timestamp = 0)
    {

        $timestamp = intval($timestamp);
        $date = new DateTime();
        $date->setTimestamp($timestamp);

        return $date->format('d-m-Y');
    }
}


/**
 * converts date to unix timestamp
 * @param $dateto
 * @return string
 */
if (!function_exists('date_to_timestamp')) {
    function date_to_timestamp($dateto)
    {

        $date = new DateTime($dateto);

        return $date->format('U');

    }
}


/**
 * format break tags
 * @param $text
 * @return mixed
 */
if (!function_exists('br2n')) {
    function br2n($text)
    {
        $breaks = array("<br />", "<br>", "<br/>");
        $text = str_ireplace($breaks, "", $text);

        return $text;
    }
}

/**
 * format passed sting
 * @param $string
 * @return string
 */
if (!function_exists('e')) {
    function e($string)
    {
        return htmlentities($string);
    }
}

/**
 * get project sidebar menus
 * @param $array
 * @param bool $child
 * @return string
 */
if (!function_exists('get_menu')) {
    function get_menu($array, $child = false)
    {
        $CI =& get_instance();
        $str = '';

        if (count($array)) {

            $str .= $child == false ? '' . PHP_EOL : '<ul class="sub-menu">' . PHP_EOL;
            foreach ($array as $item) {

                $active = $CI->uri->segment(1) == $item['module_id'] ? true : false;
                if (isset($item['children']) && count($item['children'])) {
                    $str .= $active ? '<li class="active">' : '<li class="ajax-loader">';
                    $str .= '<a  href="' . site_url(e($item['module_id'])) . '"><i class="fa fa-' . $item['icon'] . '"></i><span class="title">' . lang("module_" . e($item['module_id']));
                    $str .= '</span><span class="arrow"></span></a>' . PHP_EOL;
                    $str .= get_menu($item['children'], true);
                } else {

                    $str .= $active ? '<li class="start active">' : '<li class="ajax-loader">';
                    $str .= '<a href="' . site_url(e($item['module_id'])) . '"><i class="fa fa-' . $item['icon'] . '"></i><span class="title">' . lang("module_" . e($item['module_id'])) . '</span></a>';

                }
                $str .= '</li>' . PHP_EOL;
            }

            $str .= $child == false ? '' . PHP_EOL : '</ul>' . PHP_EOL;
        }

        return $str;
    }
}
/**
 * get menus for sorting
 * @param $array
 * @param bool $child
 * @return string
 */
if (!function_exists('get_ol')) {
    function get_ol($array, $child = false)
    {
        $str = '';

        if (count($array)) {
            $str .= $child == false ? '<ol class="dd-list">' : '<ol>';

            foreach ($array as $item) {
                $str .= '<li id="list_' . $item['module_id'] . '" class="dd-item" data-id="' . $item['module_id'] . '">';
                $str .= '<div  class="dd-handle">' . lang("module_" . e($item['module_id'])) . '</div>';

                // Do we have any children?
                if (isset($item['children']) && count($item['children'])) {
                    $str .= get_ol($item['children'], true);
                }

                $str .= '</li>' . PHP_EOL;
            }

            $str .= '</ol>' . PHP_EOL;
        }

        return $str;
    }
}

?>