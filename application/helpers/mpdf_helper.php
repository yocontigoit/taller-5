<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 1/11/2015
 * Time: 1:10 PM
 */


function pdf_create($html, $filename, $stream = true, $password = null, $isInvoice = null, $isGuest = null)
{
    require_once(APPPATH . 'helpers/mpdf/mpdf.php');

    $mpdf = new mPDF();
    $mpdf->useAdobeCJK = true;
    $mpdf->SetAutoFont();
    //$mpdf->SetProtection(array('print', 'modify','copy'),'','');
    //$mpdf->SetProtection(array('copy','print'), $password, $password);
    if (!(is_dir('./files/archive/') OR is_link('./files/archive/'))) {
        mkdir('./files/archive/', '0777');
    }

    if (strpos($filename, lang('invoice')) !== false) {
        $CI = &get_instance();
        $mpdf->setAutoBottomMargin = 'stretch';

    }
    $invoice_array = array();
    $mpdf->WriteHTML($html);

    if ($stream) {
        if (!$isInvoice) {
            return $mpdf->Output($filename . '.pdf', 'I');
        }

        foreach (glob('./files/archive/*' . $filename . '.pdf') as $file) {
            array_push($invoice_array, $file);
        }

        if (!empty($invoice_array) AND $isGuest) {
            rsort($invoice_array);
            header('Content-type: application/pdf');

            return readfile($invoice_array[0]);
        } else {
            if ($isGuest) {
                redirect('guest/view/invoice/' . end($CI->uri->segment_array()));
            }
        }
        $mpdf->Output('./files/archive/' . date('Y-m-d') . '_' . $filename . '.pdf', 'F');

        return $mpdf->Output($filename . '.pdf', 'I');
    } else {

        if ($isInvoice) {

            foreach (glob('./files/archive/*' . $filename . '.pdf') as $file) {
                array_push($invoice_array, $file);
            }
            if (!empty($invoice_array) && !is_null($isGuest)) {
                rsort($invoice_array);

                return $invoice_array[0];
            }
            $mpdf->Output('./files/archive/' . date('Y-m-d') . '_' . $filename . '.pdf', 'F');

            return './files/archive/' . date('Y-m-d') . '_' . $filename . '.pdf';
        }
        $mpdf->Output('./files/temp/' . $filename . '.pdf', 'F');


        // DELETE OLD TEMP FILES - Housekeeping
        // Delete any files in temp/ directory that are >1 hrs old
        $interval = 3600;
        if ($handle = @opendir(preg_replace('/\/$/', '', './files/temp/'))) {
            while (false !== ($file = readdir($handle))) {
                if (($file != "..") && ($file != ".") && !is_dir($file) && ((filemtime('./files/temp/' . $file) + $interval) < time()) && (substr($file,
                            0, 1) !== '.') && ($file != 'remove.txt')
                ) { // mPDF 5.7.3
                    unlink('./files/temp/' . $file);
                }
            }
            closedir($handle);
        }

        return $mpdf->Output($filename . '.pdf', 'D');
    }
}