<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/09/11
 * Time: 10:26 AM
 */


/**
 * retrieve all the users that are online, including clients users
 * @return string
 */
if (!function_exists('getUsersOnline')) {
    function getUsersOnline()
    {
        $CI =& get_instance();

        //select all the company users online
        $query = $CI->db->query("SELECT * from online_users
                                      LEFT JOIN employee ON ( employee.emp_id =online_users.user_id)
                                      LEFT JOIN users ON ( users.user_id =online_users.user_id)
                                      GROUP BY employee.emp_id
                                      ");

        $a = '<li class="static btn  btn-block  btn-success" >ONLINE USERS</li>';
        foreach ($query->result() as $row) {
            if ($CI->CompanyUser_id != $row->user_id) {
                $user_details = users_details($row->user_id, 1);
                $a .= "<li  data-event=\"initialize-chat\" data-parameter=" . "u" . $row->user_id . ">
            <a href=\"#fakelink\">
                        <span class=\"user-status " . (checkUsersOnline($row->user_id) == true ? "success" : "danger") . "\"></span>
                        <img src=\"" . base_url('files/profile_images/' . $user_details->avatar) . "\" class=\"ava-sidebar img-circle\" alt=\"Avatar\">

           " . e($row->emp_name . ' ' . $row->emp_surname) . "<span class=\"small-caps\">" . $row->emp_name . "</span>

                    </a></li>";
            }
        }

        //select all the client users online
        $query = $CI->db->query("SELECT * from online_clients
                                      LEFT JOIN client_user ON ( client_user.cl_user_id =online_clients.cl_user_id)
                                      GROUP BY online_clients.cl_user_id
                                      ");
        $a .= '<li class="static btn  btn-block  btn-success">ONLINE CLIENTS</li>';
        foreach ($query->result() as $row) {
            if ($CI->CompanyClient_user_id != $row->cl_user_id) {
                $user_details = users_details($row->cl_user_id, 0);
                $a .= "<li  data-event=\"initialize-chat\" data-parameter=" . "c" . $row->cl_user_id . ">
            <a href=\"#fakelink\">
                        <span class=\"user-status " . (checkClientsOnline($row->cl_user_id) == true ? "success" : "danger") . "\"></span>
                        <img src=\"" . base_url('files/profile_images/' . $user_details->avatar) . "\" class=\"ava-sidebar img-circle\" alt=\"Avatar\">

           " . e($row->cl_user_name . ' ' . $row->cl_user_surname) . "<span class=\"small-caps\">" . $row->cl_user_name . "</span>

                    </a></li>";
            }
        }

        return $a;
    }
}

/**
 * get chat messages
 * @param $mReceiverUser
 * @param $rows
 * @return mixed
 */
if (!function_exists('get_chat_messages')) {
    function get_chat_messages($mReceiverUser, $rows)
    {
        $CI =& get_instance();

        $selected_user_id = ($CI->CompanyUser_id > 0) ? $CI->CompanyUser_id : $CI->CompanyClient_user_id;
        $a["chatContent"] = array();
        $option = substr($mReceiverUser, 0, 1);
        foreach ($rows as $row) {
            $is_from_me = false;
            $user_details = null;
            $user_type = ($row->user_from > 0) ? 1 : 0;
            if ($CI->CompanyUser_id == $row->user_from) {
                $is_from_me = true;
                $user_details = users_details($row->user_from, $user_type);

            } elseif ($CI->CompanyClient_user_id == $row->client_from) {

                $is_from_me = true;
                $user_details = users_details($row->client_from, $user_type);
            } else {
                $is_from_me = false;
                $sender_id = ($row->client_from > 0) ? $row->client_from : $row->user_from;
                $CI->db->update('messages', array('status' => 1), array('msg_id' => $row->msg_id));
                $user_details = users_details($sender_id, $user_type);

            }

            $avatar = base_url('files/profile_images/' . $user_details->avatar);
            //all messages by current user
            if ($is_from_me) {

                array_push($a["chatContent"], "<div class=\"row msg_container base_sent\">
							<div class=\"col-md-10 col-xs-10\">
								<div class=\"messages msg_sent\">
									<p data-location=\"chat-message\" class=\"messageContent-" . $mReceiverUser . "\">" . $row->message . "<div class=\"tinyTime\">" . ago(strtotime($row->date_sent)) . " " . lang('ago') . "</div></p>
								</div>
							</div>
							<div class=\"col-md-2 col-xs-2 avatar\">
								<a href=\"page.profile.php?profile-id=" . $selected_user_id . "\"><img src=\"" . $avatar . "\" class=\"with-border img-responsive \"></a>
							</div>
						</div>");
            } else {
                //all messages to current user
                array_push($a["chatContent"], "<div class=\"row msg_container base_receiver\">
							<div class=\"col-md-2 col-xs-2 avatar\">
								<a href=\"page.profile.php?profile-id=" . $selected_user_id . "\"><img src=\"" . $avatar . "\" class=\"with-border img-responsive \"></a>
							</div>
							<div class=\"col-md-10 col-xs-10\">
								<div class=\"messages msg_receive\">
									<p data-location=\"chat-message\" class=\"messageContent-" . $mReceiverUser . "\">" . $row->message . "<div class=\"tinyTime\">" . ago(strtotime($row->date_sent)) . " " . lang('ago') . "</div></p>
								</div>
							</div>
						</div>");
            }
        }

        return $a;
    }
}


/**
 * checks if a user is online
 * @param int $user_id .. user id
 * @return true/false
 */
if (!function_exists('checkUsersOnline')) {
    function checkUsersOnline($user_id)
    {
        $CI =& get_instance();
        $user_id = intval($user_id);
        $data = $CI->db->query("SELECT * from online_users where user_id=$user_id");
        if ($data->num_rows() > 0) {
            return true;
        }

        return false;
    }
}


/**
 * checks if a client user is online
 * @param $user_id
 * @return true/false
 */
if (!function_exists('checkClientsOnline')) {
    function checkClientsOnline($user_id)
    {
        $CI =& get_instance();
        $user_id = intval($user_id);
        $data = $CI->db->query("SELECT * from online_clients where cl_user_id=$user_id");
        if ($data->num_rows() > 0) {
            return true;
        }

        return false;
    }
}


