<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/09/11
 * Time: 10:27 AM
 */


/**
 * get chat notifications
 * @return string.. chat message
 */
if (!function_exists('getMessageNotifications')) {
    function getMessageNotifications()
    {
        $CI =& get_instance();
        $option = '';
        if ($CI->CompanyUser_id > 0) {


            $option = " WHERE messages.user_to = " . $CI->CompanyUser_id;
        } else {

            $option = " WHERE messages.client_to = " . $CI->CompanyClient_user_id;
        }
        $query = $CI->db->query("SELECT * from messages
                                      $option
                                      AND status = 0
                                      ORDER BY msg_id  DESC 
                                      LIMIT 1
                                      ");
        $row = $query->row();
        if ($row) {
            $a = '';
            $user_type = ($row->user_from) ? 1 : 0;
            if ($row->user_from > 0) {

                $user_details = users_details($row->user_from, $user_type);
            } else {

                $user_details = users_details($row->client_from, $user_type);
            }
            $a = '<div class="chat-notification-inner">
                        <h6 class="title">
                                <span class=\"thumb-xs\">
                                    <img src="' . base_url('files/profile_images/' . $user_details->avatar) . '" class="avatar img-circle img-circle mr-xs pull-left">
                                </span>
                           ' . $user_details->full_name . '
                        </h6><p class=\"text\">' . html_entity_decode(substr($row->message, 0, 10)) . '..</p>
            </div>';

            return $a;

        } else {

            return '';
        }

    }
}

/**
 * @param null $user_id
 * @param null $user_type
 * @return array.. user messages
 *
 */
if (!function_exists('get_unread_messages')) {
    function get_unread_messages($user_id = null, $user_type = null)
    {

        $user_id = intval($user_id);

        $CI =& get_instance();

        //get user messages if type is 1
        if ($user_type == 1) {
            return $CI->db->where(array('user_to' => $user_id, 'status' => 0))->get('messages')->result();
        } else {
            return $CI->db->where(array('client_to' => $user_id, 'status' => 0))->get('messages')->result();
        }

        return null;


    }
}
?>
