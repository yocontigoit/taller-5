<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth
*
* Author: Ben Edmunds
*		  ben.edmunds@gmail.com
*         @benedmunds
*
* Added Awesomeness: Phil Sturgeon
*
* Location: http://github.com/benedmunds/CodeIgniter-Ion-Auth
*
* Created:  10.01.2009
*
* Description:  Modified auth system based on redux_auth with extensive customization.  This is basically what Redux Auth 2 should be.
* Original Author name has been kept but that does not mean that the method has not been modified.
*
* Requirements: PHP5 or above
*
*/

class Ion_auth
{
	/**
	 * account status ('not_activated', etc ...)
	 *
	 * @var string
	 **/
	protected $status;

	/**
	 * extra where
	 *
	 * @var array
	 **/
	public $_extra_where = array();

	/**
	 * extra set
	 *
	 * @var array
	 **/
	public $_extra_set = array();

	/**
	 * __construct
	 *
	 * @return void
	 * @author Ben
	 **/
	public function __construct()
	{
        $this->ci =& get_instance();
        $this->ci->load->config('ion_auth', TRUE);
        $config['useragent'] = 'Zest project manager';
        $config['mailtype'] = "html";//$this->config->item('company_email')->mail_type
        $config['newline'] = "\r\n";
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;

        $this->ci->load->library('email',$config);
        //$this->ci->load->library('session');
		//$this->lang->load('auth/ion_auth');
      // $this->ci->load->model('auth/ion_auth_model');

        $this->ci->load->helper('cookie');

		// Load IonAuth MongoDB model if it's set to use MongoDB,
		// We assign the model object to "ion_auth_model" variable.
       $this->ci->config->item('use_mongodb', 'ion_auth') ?
          $this->ci->load->model('ion_auth_mongodb_model', 'ion_auth_model') :
           $this->ci->load->model('auth/ion_auth_model');

		//auto-login the user if they are remembered
		if (!$this->logged_in() && get_cookie('identity') && get_cookie('remember_code'))
		{
			$this->ion_auth_model->login_remembered_user();
		}


	}
	

	/**
	 * forgotten password feature
	 *
	 * @return mixed  boolian / array
	 * @author Mathew
	 **/
	public function forgotten_password($identity)    //changed $email to $identity
	{
        $user = $this->ci->ion_auth_model->forgotten_password($identity);
		if ($user)   //changed
		{
           // print_r($user);
					$message = $this->ci->load->view($this->ci->config->item('email_templates', 'ion_auth').$this->ci->config->item('email_forgot_password', 'ion_auth'), $user, true);
                    $this->ci->email->clear();
                    $this->ci->email->from($this->ci->config->item('admin_email', 'ion_auth'), $this->ci->config->item('site_title', 'ion_auth'));
                    $this->ci->email->to($user['email']);
                    $this->ci->email->subject($this->ci->config->item('site_title', 'ion_auth') . ' - ' );
                    $this->ci->email->message($message);

					if ($this->ci->email->send())
					{
						return TRUE;
					}
					else
					{
						return FALSE;
					}


		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * forgotten_password_complete
	 *
	 * @return void
	 * @author Mathew
	 **/
	public function forgotten_password_complete($code)
	{
		$this->ion_auth_model->trigger_events('pre_password_change');

		$identity = $this->config->item('identity', 'ion_auth');
		$profile  = $this->where('forgotten_password_code', $code)->users()->row(); //pass the code to profile

		if (!$profile)
		{
			$this->ion_auth_model->trigger_events(array('post_password_change', 'password_change_unsuccessful'));
			$this->set_error('password_change_unsuccessful');
			return FALSE;
		}

		$new_password = $this->ion_auth_model->forgotten_password_complete($code, $profile->salt);

		if ($new_password)
		{
			$data = array(
				'identity'     => $profile->{$identity},
				'new_password' => $new_password
			);
			if(!$this->config->item('use_ci_email', 'ion_auth'))
			{
				$this->set_message('password_change_successful');
				$this->ion_auth_model->trigger_events(array('post_password_change', 'password_change_successful'));
					return $data;
			}
			else
			{
				$message = $this->load->view($this->config->item('email_templates', 'ion_auth').$this->config->item('email_forgot_password_complete', 'ion_auth'), $data, true);

				$this->email->clear();
				$this->email->from($this->config->item('admin_email', 'ion_auth'), $this->config->item('site_title', 'ion_auth'));
				$this->email->to($profile->email);
				$this->email->subject($this->config->item('site_title', 'ion_auth') . ' - ' . $this->lang->line('email_new_password_subject'));
				$this->email->message($message);

				if ($this->email->send())
				{
					$this->set_message('password_change_successful');
					$this->ion_auth_model->trigger_events(array('post_password_change', 'password_change_successful'));
					return TRUE;
				}
				else
				{
					$this->set_error('password_change_unsuccessful');
					$this->ion_auth_model->trigger_events(array('post_password_change', 'password_change_unsuccessful'));
					return FALSE;
				}

			}
		}

		$this->ion_auth_model->trigger_events(array('post_password_change', 'password_change_unsuccessful'));
		return FALSE;
	}

	/**
	 * forgotten_password_check
	 *
	 * @return void
	 * @author Michael
	 **/
	public function forgotten_password_check($code)
	{
		$profile = $this->ci->ion_auth_model->forgot_password_user($code); //pass the code to profile

		if (!is_object($profile))
		{
			return FALSE;
		}
		else
		{
			if ($this->ci->config->item('forgot_password_expiration', 'ion_auth') > 0) {
				//Make sure it isn't expired
				$expiration = $this->ci->config->item('forgot_password_expiration', 'ion_auth');
				if (time() - $profile->forgotten_password_time > $expiration) {
					//it has expired
                    $this->ci->ion_auth_model->clear_forgotten_password_code($code);
					return FALSE;
				}
			}
			return $profile;
		}
	}

	/**
	 * register
	 *
	 * @return void
	 * @author Mathew
	 **/
	public function register($first_name,$last_name,$username, $password, $email, $additional_data = array()) //need to test email activation
	{

		$email_activation = $this->ci->config->item('email_activation', 'ion_auth');

		if (!$email_activation)
		{
			$user_id = $this->ci->ion_auth_model->register($first_name,$last_name,$username, $password, $email, $additional_data);
			if ($user_id)
			{

				$this->ci->ion_auth_model->trigger_events(array('post_account_creation', 'post_account_creation_successful'));
				return $user_id;
			}
			else
			{

				$this->ci->ion_auth_model->trigger_events(array('post_account_creation', 'post_account_creation_unsuccessful'));
				return FALSE;
			}
		}
		else
		{
            $user_id = $this->ci->ion_auth_model->register($first_name,$last_name,$username, $password, $email, $additional_data);

			if (!$user_id)
			{

				return FALSE;
			}

			$deactivate = $this->ci->ion_auth_model->deactivate($user_id);

			if (!$deactivate)
			{
				$this->set_error('deactivate_unsuccessful');
				$this->ion_auth_model->trigger_events(array('post_account_creation', 'post_account_creation_unsuccessful'));
				return FALSE;
			}

			$activation_code = $this->ci->ion_auth_model->activation_code;
			$identity        = $this->ci->config->item('identity', 'ion_auth');
			$user            = $this->ci->ion_auth_model->user($user_id)->row();

			$data = array(
				'identity'   => $user->{$identity},
				'id'         => $user->id,
				'email'      => $email,
				'activation' => $activation_code,
			);
			if(!$this->ci->config->item('use_ci_email', 'ion_auth'))
			{

					return $data;
			}
			else
			{
				$message = $this->ci->load->view($this->config->item('email_templates', 'ion_auth').$this->config->item('email_activate', 'ion_auth'), $data, true);

                $this->ci->email->clear();
                $this->ci->email->from($this->config->item('admin_email', 'ion_auth'), $this->ci->config->item('site_title', 'ion_auth'));
                $this->ci->email->to($email);
                $this->ci->email->subject($this->config->item('site_title', 'ion_auth') . ' - ' . $this->lang->line('email_activation_subject'));
                $this->ci->email->message($message);

				if ($this->ci->email->send() == TRUE)
				{
					$this->ci->ion_auth_model->trigger_events(array('post_account_creation', 'post_account_creation_successful', 'activation_email_successful'));
					return $user_id;
				}
			}

			return FALSE;
		}
	}

	/**
	 * logout
	 *
	 * @return void
	 * @author Mathew
	 **/
	public function logout()
	{

		$identity =$this->ci->config->item('identity', 'ion_auth');
        $this->ci->session->unset_userdata($identity);
        $this->ci->session->unset_userdata("email");
        $this->ci->session->unset_userdata('id');
        $this->ci->session->unset_userdata('user_id');
        $this->ci->session->sess_destroy();
		//delete the remember me cookies if they exist
		if (get_cookie('identity'))
		{
			delete_cookie('identity');
		}
		if (get_cookie('remember_code'))
		{
			delete_cookie('remember_code');
		}

		return TRUE;
	}

    public function save_user($user_id = null,$user_data = array(),$is_update= false){


        if($is_update){
            //its an update
            $id = $this->ci->ion_auth_model->update_user($user_id,$user_data);
        }else{

            $id = $this->ci->ion_auth_model->add_user($user_id,$user_data);

        }

    }

    public function  add_cl_user($client_id,$user_data,$is_update= false){

        if($is_update){
            //its an update
			 $this->ci->ion_auth_model->update_cl_user($client_id,$user_data);
			return true;
        }else{

			 $this->ci->ion_auth_model->add_client_user($client_id,$user_data);
			return true;

        }
		return false;
    }

	public function add_employee($data = array(), $email = null){

		return $this->ci->ion_auth_model->add_employee($data,$email);
	}

	public function  update_company_settings($data = array()){

		$this->ci->ion_auth_model->update_company_settings($data);
		
	}

	public function update_system_settings($data = array()){
		
		$this->ci->ion_auth_model->update_system_settings($data);

	}
	/**
	 * logged_in
	 *
	 * @return bool
	 * @author Mathew
	 **/
	public function logged_in()
	{

		return (bool) $this->ci->session->userdata("identity");

	}

}

/* End of file ion_auth.php */ 
