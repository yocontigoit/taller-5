<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/11/18
 * Time: 11:23 AM
 */

class Admin_Controller extends MY_Controller{

    protected $module_id = null;


    function __construct($module_id=null)
    {

        parent::__construct();

        $this->load->library('form_validation');
        $this->load->library('form_validator');
        $this->load->library('export_excel');
        $this->__check_user_login();
        $this->data['allowed_modules']  = NULL;
        $this->data['project_id']  = NULL;
        $this->module_id = $module_id;

        //check if user is allowed to access a module
        if($this->CompanyUserName  && $this->module_id != null ){
            if(!$this->ion_auth_model->has_module_permission($this->module_id,$this->CompanyUserRolePermission_id))
            {
                redirect('no_access/'.$this->module_id);
            }
        }

        //get allowed modules
        if($this->CompanyUserRolePermission_id > -1) {
            $this->data['allowed_modules'] = $this->module_m->get_allowed_modules($this->CompanyUserRolePermission_id);
        }
    }

    /**
     * function check_action_permission($module_id,$action_id)
     * @param $action_id
     */
    function check_action_permission($action_id)
    {

        if ($this->CompanyUserRolePermission_id != 1) {
            if (!$this->ion_auth_model->has_module_action_permission($this->module_id, $action_id, $this->CompanyUserRolePermission_id)) {
                redirect('no_access?module=' . $this->module_id);
            }
        }
    }

    /**
     * check if user is logged in
     */
    protected function __check_user_login()
    {

        $exception_uris = array(
            'auth/login',
            'auth/logout',
            'auth/forgot_password',
            'auth/register',
            'auth/register_account',
            'auth/reset_password'
        ) ;

        if (in_array(uri_string(), $exception_uris) == FALSE) {
            if (!$this->ion_auth->logged_in()) {
                $this->session->set_flashdata('success_message', "Login Required!");
                redirect('auth/login', 'refresh');
            }
        }


    }
}
