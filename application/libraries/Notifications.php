<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2016/02/25
 * Time: 03:26 PM
 */

class Notifications
{

    function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->database();

        $this->invoice_table = 'invoice';
        $this->ticket_table = 'ticket';
        $this->payments_table = 'payments';
        $this->estimates_table = 'estimate';
        $this->milestones_table = 'milestones';
        $this->projects_table = 'projects';
        $this->profile_table = 'account_details';
        $this->email_table = 'settings_email_template';
        $this->client_user_table = 'client_user';

    }

    /**
     * query selection according to passed parameters
     * @param null $table_fields
     * @param array $where_condition
     * @param null $table_name
     * @return mixed
     */
    function  _get_table_fields($table_fields = null,$where_condition = array(),$table_name = null){

        $query = $this->ci->db->select($table_fields) -> where($where_condition)->get($table_name)->row();
        if ($query)
        {
            return $query;

        }
    }


    /**
     * send client registration to client
     * @param null $user_id
     * @param null $pass_word
     */
    function send_client_registration_notification($user_id = null,$pass_word = null){

        $user_details  = registration_details($user_id);

        $message = $this->_get_table_fields('template_body,template_title',array('template_email' => 'registration'),$this->email_table);
        $site_name = str_replace("{SITE_NAME}",$this->ci->config->item('company')->company_name,$message->template_body);
        $client_name = str_replace("{CLIENT_NAME}",client_company($user_details->client_id),$site_name);
        $user_name = str_replace("{USERNAME}",$user_details->cl_user_email,$client_name);
        $password =  str_replace("{PASSWORD}",$pass_word,$user_name);
        $message = str_replace("{SITE_NAME}",base_url('client/view/'.$user_details->client_id),$password);

        $params['recipient'] = $user_details->cl_user_email;
        $params['subject'] = '[ Account Details]';
        $params['message'] = $message;

        $params['attached_file'] = '';

        $this->_send_email($params);

    }


    /**
     * send payment notification email
     * @param $client_id
     * @param $invoice_id
     * @param $paid_amount
     */
    function send_payment_notification($client_id,$invoice_id,$paid_amount){

        $message = $this->_get_table_fields('template_body,template_title',array('template_email' => 'payment_message'),$this->email_table);
        $invoice_details = $this->_get_table_fields('inv_no,currency',array('id' =>intval($invoice_id)),$this->invoice_table);
        $client_details  = client_primary_contact_details($client_id);

        $invoice_ref = str_replace("{INVOICE_REF}",$invoice_details->inv_no,$message->template_body);
        $client_name= str_replace("{CLIENT}",client_company($client_id),$invoice_ref);
        $currency = str_replace("{CURRENCY}",$invoice_details->currency, $client_name);
        $amount = str_replace("{AMOUNT}",$paid_amount,$currency);
        $message = str_replace("{SITE_NAME}",$this->ci->config->item('company')->company_name,$amount);

        $params['recipient'] = $client_details->cl_user_email;

        $params['subject'] = '[ '.$this->ci->config->item('company')->company_name.' ]'.' Payment Received';
        $params['message'] = $message;
        $params['attached_file'] = '';

        $this->_send_email($params);
    }

    /**
     * send new invoice notification
     * @param null $invoice_id
     * @param array $data
     */
    function send_new_invoice_notification($invoice_id = null, $data = array()){

        $invoice_details = $this->_get_table_fields('id,inv_no,client_id,currency',array('id' =>intval($invoice_id)),$this->invoice_table);
        $client_details  = client_primary_contact_details($invoice_details->client_id);
        $message = $this->_get_table_fields('template_body,template_title',array('template_email' => 'invoice_message'),$this->email_table);


        $inv_reference = str_replace("{REF}",$invoice_details->inv_no,$message->template_body);
        $client_name = str_replace("{CLIENT}",client_company($invoice_details->client_id),$inv_reference);
        $currency = str_replace("{CURRENCY}",$invoice_details->currency,$client_name);
        $amount = str_replace("{AMOUNT}",invoice_balance($invoice_id),$currency);
        $invoice_link = str_replace("{INVOICE_LINK}",base_url('billing/invoice/'.$invoice_id),$amount);
        $message = str_replace("{SITE_NAME}",$this->ci->config->item('company')->company_name,$invoice_link);

        $params['recipient'] = $client_details->cl_user_email;

        $params['subject'] = '[ '.$this->ci->config->item('company')->company_name.' ] - '.$data["invoice_details"]->inv_no;

        $params['message'] = $message;


        $inv_template = 'zest';
        $html = $this->ci->load->view('templates/invoice_templates/pdf/' . $inv_template, $data, TRUE);

        $this->ci->load->helper('mpdf');

        $filename = "INV".'_'.$data["invoice_details"]->inv_no;
        $attachment = pdf_create($html, $filename, FALSE,null,TRUE);


        $params['attached_file'] = $attachment;

        $this->_send_email($params);

    }


    /**
     * send email reminder notification
     * @param null $invoice_id
     */
    function send_invoice_reminder_notification($invoice_id = null){

        $invoice_details = $this->_get_table_fields('id,inv_no,client_id,currency',array('id' =>intval($invoice_id)),$this->invoice_table);
        $client_details  = client_primary_contact_details($invoice_details->client_id);
        $message = $this->_get_table_fields('template_body,template_title',array('template_email' => 'invoice_reminder'),$this->email_table);

        $inv_reference = str_replace("{REF}",$invoice_details->inv_no,$message->template_body);
        $client_name = str_replace("{CLIENT}",client_company($invoice_details->client_id),$inv_reference);
        $currency = str_replace("{CURRENCY}",$invoice_details->currency,$client_name);
        $amount = str_replace("{AMOUNT}",invoice_balance($invoice_id),$currency);
        $invoice_link = str_replace("{INVOICE_LINK}",base_url('billing/invoice/'.$invoice_id),$amount);
        $message = str_replace("{SITE_NAME}",$this->ci->config->item('company')->company_name,$invoice_link);


        $params['recipient'] = $client_details->cl_user_email;

        $params['subject'] = '[ '.$this->ci->config->item('company')->company_name.' ] - '.$invoice_details->inv_no;

        $params['message'] = $message;

        $params['attached_file'] = '';

        $this->_send_email($params);

    }


    /**
     * send new estimate notification
     * @param null $estimate_id
     * @param array $data
     */
    function send_new_estimate_notification($estimate_id = null,$data = array()){

        $estimate_details = $this->_get_table_fields('est_id,est_code,client_id ,est_start_date ,est_due_date,est_priority,est_status ',array('est_id' =>intval($estimate_id)),$this->estimates_table);
        $client_details  = client_primary_contact_details($estimate_details->client_id);
        $message = $this->_get_table_fields('template_body,template_title',array('template_email' => 'estimate_mail'),$this->email_table);


        $estimate_reference = str_replace("{ESTIMATE_REF}",$estimate_details->est_code,$message->template_body);
        $client_name = str_replace("{CLIENT}",client_company($estimate_details->client_id),$estimate_reference);
        $currency = str_replace("{CURRENCY}","",$client_name);
        $amount = str_replace("{AMOUNT}",calculate_estimate_total($estimate_id),$currency);
        $message = str_replace("{SITE_NAME}",$this->ci->config->item('company')->company_name,$amount);



        $params['recipient'] = $client_details->cl_user_email;

        $params['subject'] = '[ '.$this->ci->config->item('company')->company_name.' ] - '.$data["estimate"]->est_code;;

        $params['message'] = $message;


        $quote_template = 'default';
        $html = $this->ci->load->view('templates/estimate_templates/pdf/' . $quote_template, $data, TRUE);

        $this->ci->load->helper('mpdf');

        $filename = "ESTIMATE".'_'.$data["estimate"]->est_code;
        $attachment = pdf_create($html, $filename,  FALSE,null,TRUE);


        $params['attached_file'] = $attachment;

        $this->_send_email($params);

    }


    /**
     * send new project notifications
     * @param null $project_id
     */
    function send_new_project_notification($project_id = null){


        $project_details = $this->_get_table_fields('project_code,project_title,project_desc,client_id,project_start_date,project_due_date',array('project_id' =>intval($project_id)),$this->projects_table);
        $message = $this->_get_table_fields('template_body,template_title',array('template_email' => 'project_new'),$this->email_table);
        $client_details  = client_primary_contact_details($project_details->client_id);

        $project_title = str_replace("{PROJECT_TITLE}",$project_details->project_title,$message->template_body);
        $project_code = str_replace("{PROJECT_NO}",$project_details->project_code,$project_title);
        $client_name = str_replace("{CLIENT_NAME}",client_company($project_details->client_id),$project_code);
        $date_create = str_replace("{DATE_CREATED}",$project_details->project_start_date,$client_name);
        $due_date  = str_replace("{DATE_DUE}",$project_details->project_due_date,$date_create);
        $site_name = str_replace("{SITE_NAME}",$this->ci->config->item('company')->company_name,$due_date);
        $link =  str_replace("{PROJECT_URL}",base_url('project/overview/preview/'.$project_id),$site_name);
        $message = str_replace("{PROJECT_ID}",$project_id,$link);

        $params['recipient'] = $client_details->cl_user_email;
        $params['subject'] = '[ '.$this->ci->config->item('company')->company_name.' ]';

        $params['message'] = $message;

        $params['attached_file'] = '';

        $this->_send_email($params);
    }


    /**
     * send users assigned to project a notification
     * @param null $project_id
     * @param $assigned_to
     * @param null $assigned_by
     */
    function send_project_assigned_notification($project_id = null,$assigned_to,$assigned_by = null){

        $project_details = $this->_get_table_fields('project_code,project_title,project_desc,client_id,project_start_date,project_due_date',array('project_id' =>intval($project_id)),$this->projects_table);
        $message = $this->_get_table_fields('template_body,template_title',array('template_email' => 'project_assigned'),$this->email_table);

        $project_title = str_replace("{PROJECT_TITLE}",$project_details->project_title,$message->template_body);
        $assigned_by = str_replace("{ASSIGNED_BY}",$assigned_by,$project_title);
        $project_code = str_replace("{PROJECT_NO}",$project_details->project_code,$assigned_by);
        $client_name = str_replace("{CLIENT_NAME}",client_company($project_details->client_id),$project_code);
        $date_create = str_replace("{DATE_CREATED}",$project_details->project_start_date,$client_name);
        $due_date  = str_replace("{DATE_DUE}",$project_details->project_due_date,$date_create);
        $message = str_replace("{SITE_NAME}",$this->ci->config->item('company')->company_name,$due_date);

        if (!empty($assigned_to)) {
            foreach ($assigned_to  as $key => $user) {
                $params['recipient'] = user_email($user)->email;

                $params['subject'] = '[ '.$this->ci->config->item('company')->company_name.' ] - ASSIGNMENT';
                $params['message'] = $message;

                $params['attached_file'] = '';

                $this->_send_email($params);
            }
        }
    }



    /**
     * send ticket reply notication email
     * @param null $ticket_id
     */
    function send_ticket_reply_notification($ticket_id = null){

        $ticket_details = $this->_get_table_fields('ticket_id,ticket_subject,ticket_date,ticket_client_id,ticket_by,ticket_user_type',array('ticket_id' =>intval($ticket_id)),$this->ticket_table);
        $message = $this->_get_table_fields('template_body,template_title',array('template_email' => 'ticket_reply_email'),$this->email_table);
        $client_details  = users_details($ticket_details->ticket_by,$ticket_details->ticket_user_type);

        $ticket_code = str_replace("{TICKET_CODE}",$ticket_id,$message->template_body);
        $ticket_subject = str_replace("{SUBJECT}",$ticket_details->ticket_subject,$ticket_code);
        $ticket_status = str_replace("{TICKET_STATUS}", get_ticket_status($ticket_details->ticket_status),$ticket_subject);
        $ticket_link = str_replace("{TICKET_LINK}",base_url('support/view/'.$ticket_id),$ticket_status);
        $message = str_replace("{SITE_NAME}",$this->ci->config->item('company')->company_name,$ticket_link);


            $params['recipient'] = $client_details->email;
            $params['subject'] = 'Ticket #'.$ticket_id.' - Replied to';
            $params['message'] = $message;
            $params['attached_file'] = '';

            $this->_send_email($params);

    }

    /**
     * send new ticket notification to client
     * @param null $ticket_id
     */
    function send_new_ticket_notification($ticket_id = null)
    {

        $ticket_details = $this->_get_table_fields('ticket_id,ticket_subject,ticket_date,ticket_client_id,ticket_by,ticket_user_type',array('ticket_id' =>intval($ticket_id)),$this->ticket_table);
        $message = $this->_get_table_fields('template_body,template_title',array('template_email' => 'ticket_client_email'),$this->email_table);
        $client_details  = users_details($ticket_details->ticket_by,0);

        $client_email = str_replace("{CLIENT_EMAIL}",$client_details->email,$message->template_body);
        $ticket_code = str_replace("{TICKET_CODE}",$ticket_id,$client_email);
        $ticket_subject = str_replace("{SUBJECT}",$ticket_details->ticket_subject,$ticket_code);
        $ticket_link = str_replace("{TICKET_LINK}",base_url('support/view/'.$ticket_id),$ticket_subject);
        $message = str_replace("{SITE_NAME}",$this->ci->config->item('company')->company_name,$ticket_link);

        $params['recipient'] = $client_details->email;
        $params['subject'] = 'Ticket #'.$ticket_id.' Created';
        $params['message'] = $message;
        $params['attached_file'] = '';

        $params['attached_file'] = '';

        $this->_send_email($params);

    }

    /**
     * send email to ticket assigned users
     * @param null $ticket_id
     * @param $assigned_to
     * @param null $assigned_by
     */
    function send_ticket_assigned_notification($ticket_id = null,$assigned_to,$assigned_by = null){

        $ticket_details = $this->_get_table_fields('ticket_id,ticket_subject,ticket_date,ticket_client_id,ticket_by,ticket_user_type',array('ticket_id' =>intval($ticket_id)),$this->ticket_table);
        $message = $this->_get_table_fields('template_body,template_title',array('template_email' => 'ticket_staff_email'),$this->email_table);
        $client_details  = users_details($ticket_details->ticket_by,$ticket_details->ticket_user_type);

        $ticket_code = str_replace("{TICKET_CODE}",$ticket_id,$message->template_body);
        $ticket_subject = str_replace("{SUBJECT}",$ticket_details->ticket_subject,$ticket_code);
        $ticket_subject = str_replace("{REPORTER_EMAIL}",$client_details->email,$ticket_subject);
        $ticket_link = str_replace("{TICKET_LINK}",base_url('support/view/'.$ticket_id),$ticket_subject);
        $message = str_replace("{SITE_NAME}",$this->ci->config->item('company')->company_name,$ticket_link);

            $params['recipient'] = user_email($assigned_to)->email;

            $params['subject'] = 'Ticket #'.$ticket_id.' - Assigned';
            $params['message'] = $message;
            $params['attached_file'] = '';

            $this->_send_email($params);


    }

    /**
     * send ticket email to client
     * @param null $ticket_id
     */
    function send_ticket_email_to_staff($ticket_id = null)
    {

        $ticket_details = $this->_get_table_fields('ticket_id,ticket_subject,ticket_date,ticket_client_id,ticket_by,ticket_user_type',array('ticket_id' =>intval($ticket_id)),$this->ticket_table);
        $message = $this->_get_table_fields('template_body,template_title',array('template_email' => 'ticket_staff_email'),$this->email_table);
        $client_details  = users_details($ticket_details->ticket_by,$ticket_details->ticket_user_type);

        $ticket_code = str_replace("{TICKET_CODE}",$ticket_id,$message->template_body);
        $ticket_subject = str_replace("{SUBJECT}",$ticket_details->ticket_subject,$ticket_code);
        $ticket_subject = str_replace("{REPORTER_EMAIL}",$client_details->email,$ticket_subject);
        $ticket_link = str_replace("{TICKET_LINK}",base_url('support/view/'.$ticket_id),$ticket_subject);
        $message = str_replace("{SITE_NAME}",$this->ci->config->item('company')->company_name,$ticket_link);

        foreach (admin_email() as $user)
        {
            $params['recipient'] = $user->email;
            $params['subject'] = 'Ticket #'.$ticket_id.' Created';
            $params['message'] = $message;
            $params['attached_file'] = '';
            $this->_send_email($params);
        }
    }


    /**
     * send email
     * @param $params
     */
    function _send_email($params)
    {
        // If using SMTP
        if ($this->ci->config->item('company_email')->mail_protocol  == 'smtp') {
            $this->ci->load->library('encrypt');
            $raw_smtp_pass =  $this->encrypt->decode($this->ci->config->item('company_email')->smtp_password);
            $config = array(
                'smtp_host' => $this->ci->config->item('company_email')->smtp_hostname,
                'smtp_port' => $this->ci->config->item('company_email')->smtp_port,
                'smtp_user' => $this->ci->config->item('company_email')->smtp_username,
                'smtp_pass' => $raw_smtp_pass,
                'crlf' 		=> "\r\n",
                'protocol'	=> $this->ci->config->item('company_email')->mail_protocol ,
            );
        }
        // Send email
        $config['useragent'] = 'Zest project manager';
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;

        $this->ci->load->library('email',$config);

        if($this->ci->config->item('company_email')->mail_outgoing !='') {
            $this->ci->email->from($this->ci->config->item('company_email')->mail_outgoing);

            if ($this->ci->config->item('company_email')->is_test == 1) {
                $this->ci->email->to($this->ci->config->item('company_email')->test_email);
            } else {
                $this->ci->email->to($params['recipient']);

            }

            $this->ci->email->subject($params['subject']);
            $this->ci->email->message($params['message']);
            if ($params['attached_file'] != '') {
                $this->ci->email->attach($params['attached_file']);
            }

            if (!$this->ci->email->send()) {
                //echo $this->ci->email->print_debugger();
            }
        }//outgoing mail not set....
    }
}