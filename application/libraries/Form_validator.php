<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2015/06/08
 * Time: 04:45 PM
 */

class Form_Validator{


    //validate input values on a project
    public $project_add_rules = array(

        'project_code' => array (
            'field' => 'project_code',
            'label' => 'Project Code',
            'rules' => 'required'
        ),
        'client' => array (
            'field' => 'client',
            'label' => 'Client',
            'rules' => 'trim|required|xss_clean'
        ),
        'project_title' => array (
            'field' => 'project_title',
            'label' => 'Project title',
            'rules' => 'required'
        ),
        'project_desc' => array (
            'field' => 'project_desc',
            'label' => 'Project Description',
            'rules' => 'required'
        ),
        'start_date' => array (
            'field' => 'project_desc',
            'label' => 'Project Description',
            'rules' => 'required'
        ),
        'due_date' => array (
            'field' => 'project_desc',
            'label' => 'Project Description',
            'rules' => 'required'
        )
    );

    public $client_add_rules = array(
            'company_name' => array (
                'field' => 'company_name',
                'label' => 'Company Name',
                'rules' => 'trim|required|xss_clean'
            ),
            'company_code' => array (
                'field' => 'company_code',
                'label' => 'Company Code',
                'rules' => 'trim|required|xss_clean'
            ),
            'email' => array (
                'field' => 'email',
                'label' => 'Company Email',
                'rules' => 'trim|required|valid_email|callback__unique_email|xss_clean'
            ),
            'company_status' => array (
                'field' => 'company_status',
                'label' => 'Company Status',
                'rules' => 'trim|numeric|xss_clean'
            ),
            'company_type' => array (
                'field' => 'company_type',
                'label' => 'Company Type',
                'rules' => 'trim|numeric|xss_clean'
             ),
            'phone_number' => array (
                'field' => 'phone_number',
                'label' => 'Phone Number',
                'rules' => 'trim|numeric|xss_clean'
            ),
            'address1' => array (
                'field' => 'address1',
                'label' => 'Address 1',
                'rules' => 'trim|required|xss_clean'
            ),
            'address2' => array (
                'field' => 'address2',
                'label' => 'Address 2',
                'rules' => 'trim|xss_clean'
            ),

            'address3' => array (
                'field' => 'address3',
                'label' => 'Address 2',
                'rules' => 'trim|xss_clean'
            ),
            'zip_code' => array (
                'field' => 'zip_code',
                'label' => 'Zip Code',
                'rules' => 'trim|numeric|xss_clean'
            ),
            'city' => array (
                'field' => 'city',
                'label' => 'City',
                'rules' => 'trim|xss_clean'
            ),
            'country' => array (
                'field' => 'country',
                'label' => 'Country',
                'rules' => 'trim|xss_clean'
            )

           

    );

    public $user_add_rules = array(

        'emp_name' => array (
            'field' => 'emp_name',
            'label' => 'Employee Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'emp_surname' => array (
            'field' => 'emp_surname',
            'label' => 'Employee Surname',
            'rules' => 'trim|required|xss_clean'
        )
    );
    public $paypal_add_rules = array(

        'paypal_mail' => array (
            'field' => 'paypal_mail',
            'label' => 'Paypal Email',
            'rules' => 'required'
        ),
        'paypal_cancel' => array (
            'field' => 'paypal_cancel',
            'label' => 'Paypal Cancel',
            'rules' => 'required'
        ),
        'paypal_ipn' => array (
            'field' => 'paypal_ipn',
            'label' => 'Paypal IPN',
            'rules' => 'required'
        ),
        'paypal_success' => array (
            'field' => 'paypal_success',
            'label' => 'Paypal Success',
            'rules' => 'required'
        )
    );

    public $bank_add_rules = array(

        'bank_name' => array (
            'field' => 'bank_name',
            'label' => 'Bank Name',
            'rules' => 'required'
        ),
        'bank_acc' => array (
            'field' => 'bank_acc',
            'label' => 'Bank Account',
            'rules' => 'trim|numeric|xss_clean'
        )
    );



    public $payfast_add_rules = array(

        'payfast_merchant_id' => array (
            'field' => 'payfast_merchant_id',
            'label' => 'Merchant Id',
            'rules' => 'required'
        ),
        'payfast_merchant_key' => array (
            'field' => 'payfast_merchant_key',
            'label' => 'Merchant Key',
            'rules' => 'required'
        ),
        'payfast_ipn' => array (
            'field' => 'payfast_ipn',
            'label' => 'Payfast IPN',
            'rules' => 'required'
        ),
        'payfast_success' => array (
            'field' => 'payfast_success',
            'label' => 'Payfast Success',
            'rules' => 'required'
        )
    );

    public $skrill_add_rules = array(

        'skrill_mail' => array (
            'field' => 'skrill_mail',
            'label' => 'Skrill Email',
            'rules' => 'required'
        ),
        'skrill_cancel' => array (
            'field' => 'skrill_cancel',
            'label' => 'Skrill Cancel',
            'rules' => 'required'
        ),
        'skrill_ipn' => array (
            'field' => 'skrill_ipn',
            'label' => 'Skrill IPN',
            'rules' => 'required'
        ),
        'skrill_success' => array (
            'field' => 'skrill_success',
            'label' => 'Skrill Success',
            'rules' => 'required'
        )
    );

    public $stripe_add_rules= array(

        'stripe_private_key' => array (
            'field' => 'stripe_private_key',
            'label' => 'Stripe Private Key',
            'rules' => 'required'
        ),
        'stripe_public_key' => array (
            'field' => 'stripe_public_key',
            'label' => 'Stripe Public Key',
            'rules' => 'required'
        )
    );

    public $bitcoin_add_rules= array(

        'bitcoin_root_url' => array (
            'field' => 'bitcoin_root_url',
            'label' => 'Root Url',
            'rules' => 'required'
        ),
        'bitcoin_root_receive' => array (
            'field' => 'bitcoin_root_receive',
            'label' => 'Receive Url',
            'rules' => 'required'
        )
    );

    public $checkout_add_rules = array(

        'checkout_seller_id' => array (
            'field' => 'checkout_seller_id',
            'label' => 'Seller ID',
            'rules' => 'required'
        ),
        'checkout_private_key' => array (
            'field' => 'checkout_private_key',
            'label' => 'Private Key',
            'rules' => 'required'
        )


    );

    public $invoice_add_rules = array(

        'invoice_id' => array (
            'field' => 'invoice_id',
            'label' => 'Invoice Ref',
            'rules' => 'required|is_unique[invoice.inv_no]'
        )


    );
    
}