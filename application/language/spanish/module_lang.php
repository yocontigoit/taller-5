<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/10
 * Time: 10:36 AM
 */

$lang['module_home']='Escritorio';

$lang['module_project']='Proyectos';
$lang['module_projects_desc']='Agregar, Actualizar, Eliminar, y/o buscar proyectos';

$lang['module_client']='Clientes';
$lang['module_customers_desc']='Agregar, Actualizar, Eliminar, y/o buscar clientes';

$lang['module_user']='Usuarios';
$lang['module_users_desc']='Agregar, Actualizar, Eliminar, y/o buscar usuarios';

$lang['module_message']='Mensajes';
$lang['module_messages_desc']='Agregar, Actualizar, Eliminar';

$lang['module_billing']='Pagos';
$lang['module_billing_desc']='Agregar, Actualizar, Eliminar';

$lang['module_settings']='Ajustes';
$lang['module_settings_desc']='view';

$lang['module_item']='Catalogo de Productos';
$lang['module_item_desc']='Agregar, Actualizar, Eliminar';

$lang['module_quotation']='Cotización';
$lang['module_quotations_desc']='Agregar, Actualizar, Eliminar';

$lang['module_support']='Soporte';
$lang['module_support_desc']='Agregar, Actualizar, Eliminar';

$lang['module_estimate']='Orden de compra o trabajo';
$lang['module_estimate_desc']='Agregar, Actualizar, Eliminar';

$lang['module_altasybajas']='Partidas y Conceptos';
$lang['module_altasybajas_desc']='Agregar, Actualizar, Eliminar';


$lang['module_expenses']='Partidas y Conceptos';
$lang['module_expenses_desc']='Agregar, Actualizar, Eliminar';


$lang['module_calendar']='Calendario';
$lang['module_report']='Reports';


$lang['module_action_Agregar_Actualizar'] = 'Agregar, Actualizar';
$lang['module_action_Eliminar'] = 'Eliminar';
