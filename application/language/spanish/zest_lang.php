<?php

$lang['label_remove_folder'] = 'Delete Folder';
$lang['label_outgoing_email'] = 'System Email';
$lang['label_use_test_mail'] = 'Use Test Email';
$lang['error_email_nofound'] = 'Email Not Found!';
$lang['label_format_html'] = 'HTML FORMAT';
$lang['label_format_text'] = 'TEXT FORMAT';
$lang['label_format_both'] = 'BOTH';
$lang['label_php_mail'] = 'PHP Mail';
$lang['label_smtp'] = 'SMTP';
$lang['label_never'] = 'NEVER';
$lang['label_preview'] = 'Preview';
$lang['label_filesize'] = 'Filesize';

$lang['label_send_message'] = 'Enviar Mensaje';
$lang['label_change_password'] = 'Cambiar contraseña';
$lang['label_password_repeat'] = 'Repetir contraseña nueva';
$lang['label_change_password_button'] = 'Cambiar contraseña';
$lang['error_password_match'] = 'Las contraseñas no coinciden';
$lang['error_password_min'] = 'La contraseña requiere 5 caracteres minimo.';
$lang['msg_password_updated'] = '¡Contraseña Actualizada!';
$lang['admin_dashboard'] = 'Escritorio';
$lang['msg_email_already_registered'] = 'Correo ya registrado';
$lang['msg_email_not_valid'] = 'Correo no valido';
$lang['msg_password_doesnt_match'] = 'Las contraseñas no coinciden';
$lang['msg_password_min_characters'] = 'La contraseña requiere 5 caracteres minimo.';
$lang['admin_items_top'] = 'Limitar Productos';
$lang['admin_items_search'] = 'Limitar Busqueda';
$lang['admin_custom_pages'] = 'paginas customizadas';
$lang['label_page'] = 'Paginas';
$lang['label_company_logo'] = 'Logo de la Compañoa';
$lang['label_income'] = 'Ingreo';
$lang['label_profile'] = 'Perfil';
$lang['label_projects'] = 'Proyecto';
$lang['label_name'] = 'Nombre';
$lang['label_surname'] = 'Apellido';
$lang['label_title'] = 'Titulo';
$lang['label_city'] = 'Ciudad';
$lang['label_country'] = 'Nombre de País';
$lang['label_phone'] = 'Numero de teléfono';
$lang['label_email'] = 'Correo';
$lang['label_company_name'] = 'Compañia';
$lang['label_owner'] = 'Dueño';
$lang['label_registration_no'] = 'No. Registro';
$lang['label_vat_no'] = 'Vat No';
$lang['label_slogan'] = 'Slogan';
$lang['label_to'] = 'Para';
$lang['label_none'] = 'Ninguno';
$lang['label_view'] = 'Ver';
$lang['label_reply'] = 'Responder';
$lang['label_attachment'] = 'Adjunto';
$lang['label_attached_files'] = 'Archivos adjuntos';
$lang['label_mail_subject'] = 'Title del Correo';
$lang['label_browse_files'] = 'Buscar Archivo';
$lang['label_tax_ref'] = 'Referencia de impuesto';
$lang['label_date'] = 'Fecha';
$lang['label_options'] = 'Opciones';
$lang['label_contacts'] = 'Contactos';
$lang['label_open'] = 'Abierto';
$lang['label_closed'] = 'Cerrado';
$lang['label_answered'] = 'Respondido';
$lang['label_progress'] = 'En proceso';
$lang['label_project_progress'] = 'Progreso';
$lang['label_tickets'] = 'Tickets de soprte';
$lang['label_classification'] = 'Clasificación';
$lang['label_estimate_cost']= 'Costo Estimado';
$lang['label_real_cost']= 'Costo real';
$lang['label_actual_cost']= 'Costo Actual';
$lang['label_description']= 'Descripción';
$lang['label_additional_costs']= 'Costos adicionales';
$lang['label_add_additional_costs']= 'Agregar Costos adicionales';
$lang['label_estimate_hours']= 'Horas estimadas';
$lang['label_done']= 'Listo';
$lang['label_brief']= 'Breve';
$lang['label_risk_analysis'] = 'Analisis de riesgo';
$lang['label_total_estimate'] = 'Estimado total';
$lang['label_total_done'] = 'Listo Total';
$lang['label_cost_delta'] = 'Calculo';
$lang['label_category'] = 'Categoria';
$lang['label_date_created'] = 'Fecha creación';
$lang['label_details'] = 'Detalles';
$lang['label_settings'] = 'Ajustes';
$lang['label_file_name'] = 'Nombre de archivo';
$lang['label_file_format'] = 'formato de archivo';
$lang['label_backup'] = 'Respaldo';
$lang['label_symbol'] = 'Simbolo';
$lang['label_enabled'] = 'Activado';
$lang['label_exchange_rate'] = 'Tipo de Cambio';
$lang['label_state'] = 'Region / State';
$lang['label_default_currency'] = 'Default Currency';
$lang['label_default_tax'] = 'Default Tax';
$lang['label_time_zone'] = 'Time zone';
$lang['label_templates'] = 'templates';
$lang['label_categories'] = 'Categories';
$lang['label_payments'] = 'Payments';
$lang['label_system'] = 'System';
$lang['label_cron'] = 'Crons';
$lang['label_updates'] = 'Updates';

/*
  |--------------------------------------------------------------------------
  | Project Permissions
  |--------------------------------------------------------------------------
 */
$lang['label_view_team_members'] = 'Permitir al cliente ver miembros del equipo';
$lang['label_view_milestones'] = 'Permitir al cliente ver los avances del proyecto';
$lang['label_view_tasks'] = 'Permitir al cliente ver las tareas del proyecto';
$lang['label_comment_on_tasks'] = 'Permitir al cliente comentar en las tareas del proyecto';
$lang['label_view_gantt'] = 'Permitir al cliente ver la gráfica de poryecto';
$lang['label_view_project_files'] = 'Permitir al cliente ver los archivos del proyecto';
$lang['label_view_project_comments'] = 'Permitir al cliente ver los comentarios del proyecto';
$lang['label_comment_on_project'] = 'Permitir al cliente comentar en un proyecto';
$lang['label_view_issues'] = 'Permitir al cliente ver los errores/dificultades del proyecto';
$lang['label_view_timesheets'] = 'Permitir al cliente ver la minuta del proyecto';
$lang['label_show_project_comments'] = 'Permitir al cliente ver los comentarios del proyecto';
$lang['label_view_milestones'] = 'Permitir al cliente ver los avances';
$lang['label_view_expenses'] = 'Permitir al cliente ver los gastos de proyecto';
$lang['label_view_costs'] = 'Permitir al cliente ver los costos del proyecto';
/*
  |--------------------------------------------------------------------------
  | Invoice
  |--------------------------------------------------------------------------
 */
$lang['label_invoice_due_after'] = "Todas las facturas debidas despúes";
$lang['label_invoice_item'] = "Trabajo completado";
$lang['label_invoice_description'] = "Descripción";
$lang['label_invoice_item_quantity'] = "Horas/Cantidad";
$lang['label_invoice_item_rate'] = "tarifa/Precio";
$lang['label_change_avatar'] = 'Cambiar foto de Perfil';
$lang['label_change_picture'] = 'Cambiar imagen';
$lang['label_invoices'] = 'Facturas';
$lang['label_invoice_to'] = 'Facturar a';
$lang['label_balance'] = 'Balance';
$lang['label_invoice'] = 'Factura';
$lang['label_invoice_date'] = 'Fecha de factura';
$lang['label_invoice_no'] = 'Factura #';
$lang['label_invoice_items'] = 'detalles de Factura';
$lang['label_quantity'] = 'Cantidad';
$lang['label_due_date'] = 'Fecha de vencimiento';
$lang['label_amount_due'] = 'Monto debido';
$lang['label_payment_method'] = 'Metodo de Pago';
$lang['label_transaction_reference'] = 'Referencia de Transacción';
$lang['label_download'] = 'Descargar';
$lang['label_documents'] = 'Documentos';
$lang['label_delete'] = 'Eliminar';
$lang['label_disable'] = 'Desactivar';
$lang['label_edit'] = 'Editar';
$lang['label_print'] = 'Imprimir';
$lang['label_paid'] = 'Pagado';
$lang['label_unpaid'] = 'Pendiente';
$lang['label_overdue'] = 'Atrasado';
$lang['label_version'] = 'version';
$lang['label_Build'] = 'tipo';
$lang['label_partially_paid'] = 'Parcialmente pagado';
$lang['label_accept'] = 'Aceptar';
$lang['label_decline'] = 'Denegar';
$lang['label_paid_amount'] = 'Monto pagado';
$lang['label_unpaid_amount'] = 'Monto pendiente';
$lang['label_overdue_amount'] = 'Monto Atrasado';
$lang['label_expense_amount'] = 'Monto de gasto';
$lang['label_expense_cost'] = 'Costo de gasto';
$lang['label_expense_date'] = 'Fecha de gasto';

$lang['label_overdue_invoices'] = 'Facturas atrasadas';
$lang['label_overview'] = 'Vista rapida';
$lang['label_billing'] = 'Facturación';
$lang['label_transaction'] = 'Transacción';
$lang['label_transaction_id'] = 'ID de Transacción';
$lang['label_list_all_invoices'] = 'Listar todas las facturas';
$lang['label_add_department'] = 'Agregar departmentos';
$lang['label_email_inbox'] = 'Bandeja de Entrada';
$lang['label_email_sent'] = 'Enviado';
$lang['label_email_trash'] = 'Basura';
$lang['label_summary'] = "Resumen";
$lang['label_add_payment'] = "Agregar Pago";
$lang['label_notes'] = 'Notas';
$lang['label_notes_log'] = 'Log de Notas';

$lang['label_add_project'] = 'Agregar projecto nuevo';
$lang['label_create_invoice'] = 'Crear Factura';
$lang['label_create_estimate'] = 'Nueva Orden';
$lang['label_edit_project'] = 'Editar Proyecto';
$lang['label_client_replies'] = 'Respuestas de clientes';
$lang['label_user_replies'] = 'Respuestas de staff';
$lang['label_open_tickets'] = 'Tickets abiertos';
$lang['label_closed_tickets'] = 'Tickets cerrados';
$lang['label_in_progress_tickets'] = 'Tickets en proceso';
$lang['label_answered_tickets'] = 'Tickets resueltos';

$lang['label_transactions'] = 'Transacciones';
$lang['label_invoice_setting'] = 'Ajustes de factura';
$lang['label_invoice_prefix'] = 'Prefijo de factura';
$lang['label_invoice_footer'] = 'Texto de pie de página';

$lang['label_estimates_setting'] = 'Ajustes de Orden';
$lang['label_estimate_prefix'] = 'Prefijo de Orden';

$lang['label_quotation_setting'] = 'Ajustes de cotizaciones';
$lang['label_quotation_prefix'] = 'Prefijo de cotizaciones';

$lang['label_project_setting'] = 'Ajustes de Proyecto';
$lang['label_project_permission_setting'] = 'Ajustes de permisos';
$lang['label_project_prefix'] = 'Prefijo de proyecto';


$lang['label_update_resource'] = 'Actualizar datos de recursos';
$lang['seconds'] = 'segundos';
$lang['minutes'] = 'minutos';
$lang['hours'] = 'horas';
$lang['days'] = 'dias';
$lang['weeks'] = 'semanas';
$lang['months'] = 'moeses';
$lang['years'] = 'años';
$lang['decades'] = 'decadas';
$lang['second'] = 'segundo';
$lang['minute'] = 'minuto';
$lang['hour'] = 'hora';
$lang['day'] = 'dia';
$lang['week'] = 'semana';
$lang['month'] = 'mes';
$lang['year'] = 'año';
$lang['decade'] = 'decada';
$lang['ago'] = 'hace';


/******RECUR********/
$lang['label_recur_bill'] = '¿Factura recurrente?';
$lang['label_quarter'] = 'Trimestre';
$lang['label_six_months'] = 'Seis Meses';
$lang['label_invoice_discount'] = 'Descuento';


/*
  |--------------------------------------------------------------------------
  | Buttons
  |--------------------------------------------------------------------------
 */
$lang['form_button_continue'] = "Continuar";
$lang['form_button_cancel'] = "Cancelar";
$lang['form_button_send'] = "Enviar";
$lang['form_button_save'] = "Guardar";
$lang['form_button_register'] = "Registrar";
$lang['form_button_login'] = "Iniciar Sesión";
$lang['form_button_logout'] ="Cerrar Sesión";
$lang['form_button_post'] = "Postear";
$lang['form_button_answer'] = "Responder";
$lang['form_button_save_profile'] = "Guardar Perfil";
$lang['form_button_password_back'] = "Recuperar contraseña";
$lang['form_button_create_account'] = "¿Crear Cuenta?";
$lang['form_button_back_login'] = "Iniciar sesión";
$lang['form_button_add_project'] = "Nuevo Proyecto";
$lang['form_button_edit_project'] = "Editar Proyecto";
$lang['form_button_update'] = "Actualizar";
$lang['form_button_add_client'] = "Cliente nuevo";
$lang['form_button_add_ticket'] = "Ticket nuevo";
$lang['form_button_add_item'] = "Producto nuevo";
$lang['form_button_add_estimate'] = "Nueva Orden";
$lang['form_button_add_department'] = "Departmento nuevo";
$lang['form_button_add_quotation'] = "cotización nueva";
$lang['form_button_save_quotation'] = "Guardar Cotización";
$lang['form_button_save_item'] = "Guardar producto";

$lang['placeholder_username'] = "usuario@dci.com";
$lang['placeholder_password'] = "contraseña";
/*
  |--------------------------------------------------------------------------
  | Labels
  |--------------------------------------------------------------------------
 */
$lang['label_milestone'] = 'Avance';
$lang['label_milestone_name'] = 'Nombre de Avance';
$lang['label_notify_assigned'] = 'Notificar usuarios asignados';
$lang['label_notify_client'] = 'Notificar cliente';
$lang['label_assign'] = 'Tarea';
$lang['label_closed_projects'] = 'proyectos cerrados';
$lang['label_selected_country'] = 'País seleccionado';
$lang['label_other_countries'] = 'Otros paises';
$lang['label_selected_currency'] = 'Tipo de cambio seleccionado';
$lang['label_currency'] = 'Tipo de cambio';
$lang['label_iso_code'] = 'Código ISO';
$lang['label_iso_number'] = 'Numero de Código ISO';
$lang['label_client_currency'] = 'Usar tipo de cambio del cliente';
$lang['label_profile_information'] = 'Información de perfil';
$lang['label_current_status'] = 'Estado actual';
$lang['label_join_date'] = 'Fecha de adisión';
$lang['label_last_login'] = 'Ultima sesión';
$lang['label_clients_with_projects'] = 'Cliente con proyectos';
$lang['label_inactive_clients'] = 'Clientes Inactivos';
$lang['label_active_clients'] = 'Clientes Activos';




$lang['label_active_payments_gateways'] = 'Pago activo';
$lang['label_paypal'] = 'Paypal';


$lang['label_paypal_settings'] = 'Paypal ajustes';
$lang['label_paypal_email'] = 'Paypal correo';
$lang['label_paypal_ipn'] = 'Paypal IPN';
$lang['label_paypal_success'] = 'Paypal Success Url';
$lang['label_paypal_cancel'] = 'Paypal Cancel Url';
$lang['label_paypal_active'] = 'Paypal Gateway Active';

$lang['label_skrill'] =  'Skrill';
$lang['label_skrill_settings'] = 'Skrill Settings';
$lang['label_skrill_email'] = 'Skrill Email Address';
$lang['label_skrill_ipn'] = 'Skrill IPN';
$lang['label_skrill_success'] = 'Skrill Success Url';
$lang['label_skrill_cancel'] = 'Skrill Cancel Url';
$lang['label_skrill_active'] = 'Skrill Gateway Active';

$lang['label_stripe'] =  'Stripe';
$lang['label_stripe_settings'] = 'Stripe Settings';
$lang['label_stripe_private_key'] = 'Stripe Private Key';
$lang['label_stripe_stripe_public_key'] = 'Stripe Public Key';

$lang['label_estimate_details'] = 'Detalles de Orden';
$lang['label_invoice_details'] = 'Detalles de factura';
$lang['label_payment_details'] = 'Detalles de pago';
$lang['label_quotation_details'] = 'Detalles de cotización';
$lang['label_project_details'] = 'Detalles de proyecto';
$lang['label_event_details'] = 'Detalles de evento';

$lang['label_expenses'] = 'Gastos';
$lang['label_expense_costs'] = 'Costos de gastos';
$lang['label_expense_budget'] = 'Costos presupuestos';
$lang['label_expense_real_cost'] = 'Coste gasto real';
$lang['label_assign_to'] = 'Asignado a';
$lang['label_task'] = "Tarea";
$lang['label_do_on'] = "Hacer en";
$lang['label_use_checklist'] = "Usar lista de comprobación?";
$lang['label_re_assign'] = "Re-asignar";
$lang['label_statistics'] = "Estadisticas";
$lang['label_closed_tickets_all'] = "Tickets: cerrados/todos";
$lang['label_closed_projects_all'] = "Projects: cerrados/todos";
$lang['label_payments_this_year'] = "Pagos este año";
$lang['label_sales_this_year'] = "ventas este año";
$lang['label_sales_this_month'] = "Ventas este mes";
$lang['label_recent_payments'] = "Pagos Recientes";
$lang['label_use_worklog'] = "Progreso : Usar log de trabajo?";
$lang['label_complete_projects'] = "Proyectos completos";
$lang['label_open_projects'] = "Proyectos Abiertos";
$lang['label_stats'] = "Estadisticas";
$lang['label_sales'] = "Ventas";
$lang['label_recent_projects'] = "Proyectos Recientes";
$lang['label_recent_activities'] = 'Actividades Recientes';
$lang['label_logged_hours'] = 'Horas logeadas';
$lang['label_requirements'] = 'Requerimientos';
$lang['label_add_customer']  = "Agregar Cliente";
$lang['label_client_details']  = "Detalles de Cliente";
$lang['label_issue_no'] = "Detalle #";
$lang['label_issue_details'] = "Detalles";
$lang['label_issue_assigned_to'] = "Detalle asignado a";
$lang['label_start_date'] = "Fecha de inicio";
$lang['label_issues'] = "Detalles";
$lang['label_total_projects'] = "Proyectos Totales";
$lang['label_open_tickets'] = "Tickets abiertos";
$lang['label_total_clients'] = " Total Clientes Registrados";
$lang['label_total_users'] = "Total Usuarios Registerados";
$lang['label_my_tasks'] = "Mis tareas";
$lang['label_id'] = 'ID';
$lang['label_name'] = 'Nombre';
$lang['label_surname'] = 'Apellido';
$lang['label_email'] = 'Correo';
$lang['label_general'] = 'General';
$lang['label_local'] = 'Local';
$lang['label_support'] = 'Soporte';
$lang['label_status'] = 'Estado';
$lang['label_edit'] = 'Editar';
$lang['label_added_by'] = 'Agregado por';
$lang['label_due_by'] = 'Terminar en';
$lang['label_added_on'] = 'Agregado en';
$lang['label_add_comment'] = 'Agregar comentario';
$lang['label_comment'] = 'Comentario';
$lang['label_comments'] = 'Comentarios';
$lang['label_task_timer'] = 'Temporizador de tarea';
$lang['label_assigned_users'] = 'Usuarios asignados';
$lang['label_tick_to_send_mail_confirmation'] = "Enviar correo de confirmación";
$lang['label_what_we_need'] = 'Lo que se necesita';
$lang['label_rate'] = 'calificación';
$lang['label_enable'] = 'Activar';
$lang['label_edit_tax'] = 'Editar Impuesto';
$lang['label_dashboard'] = 'Escritorio';
$lang['label_add_check_list'] = 'Agregar Lista de verificación';


$lang['label_company_id'] = 'ID de compañia';
$lang['label_company_name'] = 'Nombre de Compañia';
$lang['label_primary_contact'] = 'Contacto Principal';
$lang['label_mark_primary'] = 'Marcar como principal';
$lang['label_website'] = 'Sitio Web';

$lang['label_client_add'] = 'Agregar Cliente';
$lang['label_project_add'] = 'Agregar Proyecto';
$lang['label_user_add'] = 'Agregar usuario';
$lang['label_item_add'] = 'Agregar producto';
$lang['label_add_expense'] = 'Agregar costo';
$lang['label_quotation_add'] = 'Pedir cotización';
$lang['label_estimate_add'] = 'Nueva Orden de Compra/trabajo';

$lang['label_user_logout'] = 'Cerrar Sesión';





$lang['label_smtp_setting'] = "Ajustes SMTP";
$lang['label_emails'] = "Correos";
$lang['label_emails_setting'] = "Ajustes de Correo";
$lang['label_email_type'] = "Ajustes de tipo de correo";
$lang['label_smtp_domain_name'] = "Nombre de dominio";
$lang['label_smtp_server'] = "Servidor SMTP";
$lang['label_smtp_user'] = "Usuario SMTP";
$lang['label_smtp_password'] = "Contraseña SMTP";
$lang['label_smtp_encryption'] = "Tipo de encriptación SMTP";
$lang['label_smtp_port'] = "Puerto SMTP";
$lang['label_test_email_name'] = "Probar Cuenta de Correo";
$lang['label_test_email_setting'] = "Probar configuración de correo";
$lang['label_outgoing_email_setting'] = "configuración de correo de salida";
$lang['label_recent_activities'] = "Actividades recientes";
$lang['label_priorities'] = "Prioridad";
$lang['label_status'] = "Estatus";
$lang['label_tax_ref'] = "Referencia de Impuesto";
$lang['label_departments'] = "Departamento";

$lang['label_est_item'] = "Trabajo completado/objeto";
$lang['label_est_description'] = "Descripción";
$lang['label_est_item_quantity'] = "Horas/Cantidad";
$lang['label_est_item_rate'] = "Rango/Precio";
$lang['label_tax'] = "IVA";
$lang['label_amount'] = "Monto";
$lang['label_sub_total'] = "Sub Total";
$lang['label_total'] = "Total";
$lang['label_tax_setting'] = "Ajustes de Impuesto";
$lang['label_item_setting'] = "Ajustes de objeto";
$lang['label_est_setting'] = "Ajustes de estimación";
$lang['label_est_date_setting'] = "Ajustes de fechas estimadas";
$lang['label_est_detail_setting'] = "Ajustes de detalles estimados";
$lang['label_project_detail_setting'] = "Ajustes de detalles de proyecto";
$lang['label_project_setting'] = "Ajustes de proyecto";
$lang['label_project_date_setting'] = "Ajustes de fechas de proyecto";
$lang['label_project_budget'] = "Presupuesto";


$lang['label_active'] = "Activo";
$lang['label_name'] = "Nombre";
$lang['label_role'] = "Rol";
$lang['label_role_id'] = "Rol ID";
$lang['label_role_name'] = "Nombre de Rol";

$lang['label_est_worklog'] = "Log Estimado";

$lang['label_work_done'] = "Trabajo hecho";

$lang['label_hourly_cost'] = "Costo por hora";


//@TODO: users labels language
$lang['label_username'] = 'Correo o Usuario';
$lang['label_password'] = 'Contraseña';
$lang['label_remember_password'] = 'Recordarme';
$lang['label_users'] = 'Usuarios';
$lang['label_users_online'] = 'Usuarios en linea';
$lang['label_profile_comments'] = 'Comentarios';
$lang['label_title_search_user'] = 'Buscar Usuarios';
$lang['label_show_sidebar'] = 'Ocultar/Mostrar barra lateral';
$lang['label_emp_name'] = 'Nombre';
$lang['label_emp_surname'] = 'Apellido';
$lang['label_emp_email_address'] = 'Correo Electronico';
$lang['label_emp_hourly_cost'] = 'Costo por hora';
$lang['label_emp_working_hours'] = 'Horas trabajadas por día';
$lang['label_emp_job_desc'] = 'Descripción de trabajo';
$lang['label_roles'] = 'Rol';
$lang['label_language'] = 'Lenguaje';
$lang['label_default_language'] = 'Lenguaje predeterminado';
$lang['label_complete'] = 'Completado';
$lang['label_administrator'] = 'Administrador';


//@TODO: project labels language
$lang['label_project_name'] = 'Nombre de proyecto';
$lang['label_project_description'] = 'Descripción de proyecto';
$lang['label_project_end_date'] = 'Fecha de termino';
$lang['label_project_start_date'] = 'Fecha de inicio';
$lang['label_project_assign_to'] = 'Asignar a';
$lang['label_project_code'] = 'Codigo de Proyecto';
$lang['label_project_title'] = 'Titulo de Proyecto';
$lang['label_project_client'] = 'Cliente';
$lang['label_project_status'] = 'Estatus de proyecto';
$lang['label_project_activities'] = 'Actividades de proyecto';
$lang['label_project_duration'] = 'Duración de proyecto';
$lang['msg_no_found_users'] = 'no se encontraron usuarios.';

//@TODO: PROJECT OVERWIEW
$lang['label_project_overview'] = 'Resumen de Proyecto';
$lang['label_project_wbs'] = 'Desglose de proyecto';
$lang['label_project_gantt'] = 'Chat con Gantt';
$lang['label_project_milestone'] = 'Avance';
$lang['label_project_assignment'] = 'Tareas';
$lang['label_project_documents'] = 'Documentos';
$lang['label_project_comments'] = 'Commentarios';
$lang['label_project_time_sheet'] = 'Tiempo';
$lang['label_project_tasks'] = 'Tareas';
$lang['label_project_notes'] = 'Notas';
$lang['label_project_costs'] = 'Costos';
$lang['label_project_issues'] = 'Detalles';
$lang['label_project_expenses'] = 'Gastos';


//@TODO: clients labels language
$lang['label_clients'] = 'Clientes';
$lang['label_client_name'] = 'Nombre de Cliente';
$lang['label_client_no'] = 'Cliente #';
$lang['label_clients_company_name'] = 'Compañia';
$lang['label_clients_company_code'] = 'Codigo/Abreviación';
$lang['label_clients_company_status'] = 'Estatus de la Compañia';
$lang['label_clients_company_type'] = 'Tipo de Compañia';
$lang['label_clients_company_phone'] = 'Teléfono';
$lang['label_clients_company_website'] = 'Sitio Web';
$lang['label_clients_company_email'] = 'Correo de compañia';
$lang['label_clients_company_primary_contact'] = 'Contacto principal';

//@TODO: locations labels language
$lang['label_location_address_1'] = 'Dirección 1';
$lang['label_location_address_2'] = 'Dirección 2';
$lang['label_location_address_3'] = 'Dirección 3';
$lang['label_location_zip_code'] = 'Codigo Postal';
$lang['label_location_country'] = 'País';
$lang['label_location_city'] = 'Ciudad';


$lang['msg_box_login_title'] = 'Inicio de Sesión';

$lang['msg_user_offline'] = 'Usuario no en linea';

$lang['label_chat_send'] = 'Enviar';
$lang['label_public_chat'] = 'Chat publico';
$lang['label_chat_history'] = 'Historial de Chat';
$lang['msg_box_register_title'] = 'Crear cuenta';
$lang['box_register_title'] = 'iniciar sesión o Crear cuenta';


$lang['label_notifications'] = 'Notificaciónes';

//@TODO: Tasks language
$lang['t'] = 'segundos';

$lang['label_task_description'] = 'Descripción';
$lang['label_task_priority'] = 'Prioridad';
$lang['label_task_date'] = 'Fecha de adición';
$lang['label_task_progress'] = 'Progreso';
$lang['label_task_due'] = 'Fecha limite';
$lang['label_task_options'] = 'Herramientas';
$lang['label_task_edit'] = 'Editar tarea';
$lang['label_task_title'] = 'Titulo de tarea';
$lang['label_task_status'] = 'Estado de tarea';
$lang['label_task_percentage_complete'] = 'Porcentaje Completo';
$lang['label_task_start_date'] = 'Fecha de inicio';
$lang['label_task_due_date'] = 'Fecha de termino';
$lang['label_task_requirements'] = 'Requerimiento de tarea';

$lang['label_add_task'] = 'Agregar una nueva tarea';
$lang['label_edit_task'] = 'Editar tarea seleccionada';
$lang['label_task_assign_to'] = 'Asignar a ';
$lang['label_task_dependant'] = 'Tarea dependiente de';
$lang['label_task_duration'] = 'Duración de tarea';
$lang['label_code'] = 'Codigo';
$lang['label_is_milestone'] = 'Tarea es avance';
$lang['label_is_visible'] = 'Visible';
$lang['label_client_visible'] = "Visible al cliente";
$lang['label_estimated_hours'] = "horas estimadas de tarea";
//@TODO: Tasks Messages
//@TODO: support labels language
$lang['label_support_ticket_no'] = 'No. Ticket';
$lang['label_support_ticket_priority'] = 'Prioridad';
$lang['label_support_ticket_department'] = 'Departamento';
$lang['label_support_ticket_assigned_to'] = 'Ticket asignado a';
$lang['label_support_ticket_subject'] = 'Sujeto';
$lang['label_support_ticket_reporter'] = 'Reported por';



//@TODO: quotes labels language

$lang['label_quotation_status'] = 'Estado';
$lang['label_quotation_created'] = 'Creado';
$lang['label_quotation_budget'] = 'Presupuesto';
$lang['label_quotation_company'] = 'Compañia';
$lang['label_quotation_assign'] = 'Asignado a';
$lang['label_quotation_options'] = 'Opciones';

//@TODO: estimate labels language
$lang['label_add_estimate'] = 'Nuevas Ordenes';
$lang['label_edit_estimate'] = 'Editar Ordenes';
$lang['label_estimate_priority'] = 'Prioridad';
$lang['label_estimate_status'] = 'Estatus';

$lang['label_estimate_status'] = 'Estatus';
$lang['label_estimate_created'] = 'Creado';
$lang['label_estimate_start'] = 'Fecha de pedido';
$lang['label_estimate_due'] = 'Fecha limite';
$lang['label_estimate_company'] = 'Nombre de compañia';
$lang['label_estimate_reference'] = 'Orden';

$lang['label_estimate_amount'] = 'Monto';
$lang['label_estimate_options'] = 'Opciones';
$lang['label_estimate_no'] = 'Orden No.';
$lang['label_estimate_from'] = 'Orden de';
$lang['label_estimate_to'] = 'Orden para';
$lang['label_estimate_date'] = 'Fecha de Orden';
$lang['label_estimate_date'] = 'Fecha de Orden';

$lang['label_estimate_discount'] = 'Descuento';
$lang['label_estimate_tax'] = 'impuesto en %';
$lang['label_estimate_note'] = 'Notas de Orden';
$lang['label_estimate_end_date'] = 'Fecha Limite de Orden';


//@TODO: item labels language
$lang['label_edit_item'] = 'Editar Objeto';
$lang['label_add_item'] = 'Nuevo objeto';
$lang['label_item_name'] = 'Nombre de objeto';
$lang['label_item_description'] = 'Descripción de objeto';
$lang['label_item_quantity'] = 'Cantidad de objeto';
$lang['label_item_price'] = 'Precio de objeto';
$lang['label_item_tax'] = 'Clase de Impuesto';

/*
  |--------------------------------------------------------------------------
  | System Labels
  |--------------------------------------------------------------------------
 */

$lang['label_cron_setting'] = 'Cron Settings';
$lang['label_active_cron'] = 'ACTIVATE  CRONJOB';
$lang['label_create_recur'] = 'Create Recurring Invoice';
$lang['label_invoice_reminder'] = 'Auto  Send Invoice Reminder';
$lang['label_email_recur'] = 'Auto send recurring Invoice';
$lang['label_auto_database_backup'] = 'AUTOMATIC DATABASE BACKUP';


$lang['label_category_setting'] = 'Ajustes de categoria';


$lang['label_general_setting'] = 'Ajustes generales';
$lang['label_tax_setting'] = 'Ajustes de IVA';
$lang['label_local_setting'] = 'Ajustes locales';
$lang['label_support_setting'] = 'Ajustes de soporte';

$lang['label_system_setting'] = 'Ajustes de sistema';
$lang['label_purchase_code'] = 'Codigo de compra';
$lang['label_upload_size'] = 'Tamaño maximo de archivos';
$lang['label_allowed_files'] = 'Archivos permitidos';
$lang['label_decimal_separator'] = 'Separador decimal';
$lang['label_thousand_separator'] = 'Separador de mil';
$lang['label_symbol_position'] = 'Posición de simbolo';
$lang['label_demo_mode'] = 'Modo demo';
$lang['label_google_code'] = 'Codigo de Google Analytics';


$lang['label_system_updates'] = 'Actualizaciones de sistema';

$lang['label_php'] = 'PHP';
$lang['label_codeigniter_version'] = 'Codeigniter Version';
?>
