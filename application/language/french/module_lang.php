<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/10
 * Time: 10:36 AM
 */

$lang['module_home'] = 'Tableau de bord';

$lang['module_project'] = 'Projets';
$lang['module_projects_desc'] = 'Ajouter, Update, Delete, et la recherche de projets';

$lang['module_client'] = 'Clients';
$lang['module_customers_desc'] = 'Ajouter, Update, Delete, et la recherche des clients';

$lang['module_user'] = 'Utilisateurs';
$lang['module_users_desc'] = 'Ajouter, Update, Delete, et recherche les utilisateurs';

$lang['module_message'] = 'Messages';
$lang['module_messages_desc'] = 'Ajouter, Update, Delete';

$lang['module_billing'] = 'Facturation';
$lang['module_billing_desc'] = 'Ajouter, Update, Delete';

$lang['module_settings'] = 'Paramètres' ;
$lang['module_settings_desc'] = 'vue';

$lang['module_item'] = 'Articles';
$lang['module_item_desc'] = 'Ajouter, Update, Delete';

$lang['module_quotation'] = 'Citations';
$lang['module_quotations_desc'] = 'Ajouter, Update, Delete';

$lang['module_support'] = 'Support';
$lang['module_support_desc'] = 'Ajouter, Update, Delete';

$lang['module_estimate'] = 'Estimation';
$lang['module_estimate_desc'] = 'Ajouter, Update, Delete';


$lang['module_expenses'] =  'Dépenses';
$lang['module_expenses_desc'] = 'Ajouter, Update, Delete';


$lang['module_calendar'] = 'Calendrier';
$lang['module_report']='Reports';


$lang['module_action_add_update'] = 'Ajouter, Mettre à jour';
$lang['module_action_delete'] = 'Supprimer';

