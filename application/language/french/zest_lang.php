<?php
$lang['label_remove_folder'] = 'Supprimer le dossier';
$lang['label_outgoing_email'] = 'Système Email';
$lang['label_use_test_mail'] = 'Utiliser mail test';
$lang['error_email_nofound'] = 'Email Not Found!';
$lang['label_format_html'] = 'FORMAT HTML';
$lang['label_format_text'] = 'FORMAT TEXT';
$lang['label_format_both'] = 'DEUX';
$lang['label_php_mail'] = 'PHP Mail';
$lang['label_smtp'] = 'SMTP';
$lang['label_never'] = 'JAMAIS';
$lang['label_preview'] = 'Preview';
$lang['label_filesize'] = 'Taille du fichier';

$lang['label_send_message'] = 'Envoyer Messages';
$lang['label_change_password'] = 'Change Password';
$lang['label_password_repeat'] = 'Répéter nouveau mot de passe';
$lang['label_change_password_button'] = 'Change Password';
$lang['error_password_match'] = 'Mot de passe n \' correspond! ';
$lang['error_password_min'] = 'Mot de passe nécessaire min 5 caractères.';
$lang['msg_password_updated'] = 'Mot de passe Mise à jour!';
$lang['admin_dashboard'] = 'Dashboard';
$lang['msg_email_already_registered'] = 'Email Déjà inscrit';
$lang['msg_email_not_valid'] = 'Email pas valide';
$lang['msg_password_doesnt_match'] = 'Mot de passe n \' correspond! ';
$lang['msg_password_min_characters'] = 'Mot de passe nécessaire min 5 caractères.';
$lang['admin_items_top'] = 'Limit Top Articles';
$lang['admin_items_search'] = 'Limiter la recherche Articles';
$lang['admin_custom_pages'] = 'Custom Pages';
$lang['label_page'] = 'Pages';
$lang['label_company_logo'] = 'Company Logo';
$lang['label_income'] = 'Revenu';
$lang['label_profile'] = 'Profil';
$lang['label_projects'] = 'Projets';
$lang['label_name'] = 'Nom';
$lang['label_surname'] = 'Nom';
$lang['LABEL_TITLE'] = 'Titre';
$lang['label_city'] = 'City';
$lang['label_country'] = 'Country Name';
$lang['label_phone'] = 'numéro de téléphone';
$lang['label_email'] = 'Email';
$lang['label_company_name'] = 'Company';
$lang['label_owner'] = 'propriétaire';
$lang['label_registration_no'] = 'Enregistrement';
$lang['label_vat_no'] = 'Vat Non';
$lang['label_slogan'] = 'Slogan';
$lang['label_to'] = 'Pour';
$lang['label_none'] = 'None';
$lang['label_view'] = 'View';

$lang['label_reply'] = 'Reply';
$lang['label_attachment'] = 'Attachment';
$lang['label_attached_files'] = 'Attached Files';
$lang['label_mail_subject'] = 'Mail subject';
$lang['label_browse_files'] = 'Browse files';
$lang['label_tax_ref'] = 'Tax Ref';
$lang['label_date'] = 'Date';
$lang['label_options'] = 'Options';
$lang['label_contacts'] = 'Contacts';
$lang['label_open'] = 'Open';
$lang['label_closed'] = 'Closed';
$lang['label_answered'] = 'Answered';
$lang['label_progress'] = 'In progress';
$lang['label_project_progress'] = 'Progress';
$lang['label_tickets'] = 'Support Tickets';
$lang['label_classification'] = 'Classification';
$lang['label_estimate_cost']= 'Estimated Cost';
$lang['label_real_cost']= 'Real Cost';
$lang['label_actual_cost']= 'Actual Cost';
$lang['label_description']= 'Description';
$lang['label_additional_costs']= 'Additional Costs';
$lang['label_add_additional_costs']= 'Add Additional Costs';
$lang['label_estimate_hours']= 'Estimated Hours';
$lang['label_done']= 'Done';
$lang['label_brief']= 'Brief';
$lang['label_risk_analysis'] = 'Risk Analysis';
$lang['label_total_estimate'] = 'Total Estimation';
$lang['label_total_done'] = 'Total Done';
$lang['label_cost_delta'] = 'Calculation';
$lang['label_category'] = 'Category';
$lang['label_date_created'] = 'Date Created';
$lang['label_details'] = 'Details';
$lang['label_settings'] = 'Settings';
$lang['label_file_name'] = 'File name';
$lang['label_file_format'] = 'file format';
$lang['label_backup'] = 'Backup';
$lang['label_symbol'] = 'Symbol';
$lang['label_enabled'] = 'Enabled';
$lang['label_exchange_rate'] = 'Exchange rate';
$lang['label_state'] = 'Region / State';
$lang['label_default_currency'] = 'Default Currency';
$lang['label_default_tax'] = 'Default Tax';
$lang['label_time_zone'] = 'Time zone';
$lang['label_templates'] = 'templates';
$lang['label_categories'] = 'Categories';
$lang['label_payments'] = 'Payments';
$lang['label_system'] = 'System';
$lang['label_cron'] = 'Crons';
$lang['label_updates'] = 'Updates';


$lang['label_reply'] = 'Répondre';
$lang['label_attachment'] = 'attachement';
$lang['label_attached_files'] = 'Fichiers attachés';
$lang['label_mail_subject'] = 'objet Mail';
$lang['label_browse_files'] = 'Parcourir les fichiers';
$lang['label_tax_ref'] = 'Tax Ref';
$lang['label_date'] = 'Date';
$lang['label_options'] = 'Options';
$lang['label_contacts'] = 'Contacts';
$lang['label_open'] = 'Ouvrir';
$lang['label_closed'] = 'Closed';
$lang['label_answered'] = 'Answered';
$lang['label_progress'] = 'En cours';
$lang['label_project_progress'] = 'Progress';
$lang['label_tickets'] = 'Billets de soutien';
$lang['label_classification'] = 'Classification';
$lang['label_estimate_cost'] = 'Coût estimatif';
$lang['label_real_cost'] = 'Real Cost';
$lang['label_actual_cost'] = 'Coût réel';
$lang['label_description'] = 'Description';
$lang['label_additional_costs'] = 'Les coûts supplémentaires';
$lang['label_add_additional_costs'] = 'Ajouter des coûts supplémentaires';
$lang['label_estimate_hours'] = 'Heures estimées';
$lang['label_done'] = 'Done';
$lang['label_brief'] = 'Brief';
$lang['label_risk_analysis'] = 'Analyse des risques';
$lang['label_total_estimate'] = 'total Estimation';
$lang['label_total_done'] = 'total Fait';
$lang['label_cost_delta'] = 'Calcul';
$lang['label_category'] = 'Catégorie';
$lang['label_date_created'] = 'Date de création';
$lang['label_details'] = 'Details';
$lang['label_settings'] = 'Paramètres';
$lang['label_file_name'] = 'Nom du fichier';
$lang['label_file_format'] = 'format de fichier';
$lang['label_backup'] = 'sauvegarde';
$lang['label_symbol'] = 'Symbol';
$lang['label_enabled'] = 'Enabled';
$lang['label_exchange_rate'] = 'Taux de change';
$lang['label_state'] = 'Région / État';
$lang['label_default_currency'] = 'Devise par défaut';
$lang['label_default_tax'] = 'impôt par défaut';
$lang['label_time_zone'] = 'zone Time';
$lang['label_templates'] = 'templates';
$lang['label_categories'] = 'Catégories';
$lang['label_payments'] = 'Paiements';
$lang['label_system'] = 'Système';
$lang['label_cron'] = 'Crons';
$lang['label_updates'] = 'Mises à jour';

/*
  |--------------------------------------------------------------------------
  | Project Permissions
  |--------------------------------------------------------------------------
 */

$lang['label_view_team_members'] = 'Autoriser client pour afficher les membres de léquipe';
$lang['label_view_milestones'] = 'Autoriser client pour afficher les étapes du projet';
$lang['label_view_tasks'] = 'Autoriser client pour afficher les tâches du projet';
$lang['label_comment_on_tasks'] = 'Autoriser client de se prononcer sur les tâches du projet';
$lang['label_view_gantt'] = 'Autoriser client pour afficher gantt du projet';
$lang['label_view_project_files'] = 'Autoriser client pour afficher les fichiers de projet';
$lang['label_view_project_comments'] = 'Permettre aux clients de visualiser les commentaires de projet';
$lang['label_comment_on_project'] = 'Permettre aux clients de faire des commentaires sur le projet';
$lang['label_view_issues'] = 'Autoriser client pour afficher les questions du projet';
$lang['label_view_timesheets'] = 'Permettre aux clients de visualiser la feuille de temps du projet';
$lang['label_show_project_comments'] = 'Permettre aux clients de visualiser les commentaires de projet';
$lang['label_view_milestones'] = 'Autoriser client pour afficher des jalons';
$lang['label_view_expenses'] = 'Autoriser client pour afficher les dépenses du projet';
$lang['label_view_costs'] = 'Autoriser client pour afficher les coûts du projet';
/*
  |--------------------------------------------------------------------------
  | Invoice
  |--------------------------------------------------------------------------
 */
$lang['label_invoice_due_after'] = "Toutes les factures Due Après";
$lang['label_invoice_item'] = "Fin des travaux / Item";
$lang['label_invoice_description'] = "Description";
$lang['label_invoice_item_quantity'] = "Heures / QTY";
$lang['label_invoice_item_rate'] = "Taux / prix";
$lang['label_change_avatar'] = 'Modifier le profil Picture';
$lang['label_change_picture'] = 'Modifier l image';
$lang['label_invoices'] = 'Factures';
$lang['label_invoice_to'] = 'Invoice To';
$lang['label_balance'] = 'Balance';
$lang['label_invoice'] = 'Invoice';
$lang['label_invoice_date'] = 'Date de la facture';
$lang['label_invoice_no'] = 'Invoice #';
$lang['label_invoice_items'] = 'éléments de la facture';
$lang['label_quantity'] = 'Quantité';
$lang['label_due_date'] = 'Date de';
$lang['label_amount_due'] = 'Montant dû';
$lang['label_payment_method'] = 'Mode de paiement';
$lang['label_transaction_reference'] = 'Transaction Ref';
$lang['label_download'] = 'Télécharger';
$lang['label_documents'] = 'Documents';
$lang['label_delete'] = 'Supprimer';
$lang['label_disable'] = 'Désactiver';
$lang['label_edit'] = 'Edit';
$lang['label_print'] = 'Imprimer';
$lang['label_paid'] = 'payé';
$lang['label_unpaid'] = 'non rémunéré';
$lang['label_overdue'] = 'Overdue';
$lang['label_version'] = 'version';
$lang['label_Build'] = 'Build';
$lang['label_partially_paid'] = 'Partiellement payé';
$lang['label_accept'] = 'Accepter';
$lang['label_decline'] = 'Déclin';
$lang['label_paid_amount'] = 'Montant payé';
$lang['label_unpaid_amount'] = 'Montant non rémunéré';
$lang['label_overdue_amount'] = 'Overdue Montant';
$lang['label_expense_amount'] = 'Dépenses Montant';
$lang['label_expense_cost'] = 'Frais Coût';
$lang['label_expense_date'] = 'Date Expense';

$lang['label_overdue_invoices'] = 'Les factures en souffrance';
$lang['label_overview'] = 'Vue d\'ensemble';
$lang['label_billing'] = 'Facturation';
$lang['label_transaction'] = 'Transaction';
$lang['label_transaction_id'] = 'Transaction ID';
$lang['label_list_all_invoices'] = 'Liste toutes les factures';
$lang['label_add_department'] = 'Ajouter départements';
$lang['label_email_inbox'] = 'Inbox';
$lang['label_email_sent'] = 'Envoyé';
$lang['label_email_trash'] = 'Trash';
$lang['label_summary'] = "Résumé";
$lang['label_add_payment'] = "Ajouter paiement";
$lang['label_notes'] = 'Notes';
$lang['label_notes_log'] = 'Journal Notes';

$lang['label_add_project'] = 'Ajouter un nouveau projet';
$lang['label_create_invoice'] = 'Créer une facture';
$lang['label_create_estimate'] = 'Créer Estimation';
$lang['label_edit_project'] = 'Modifier Project';
$lang['label_client_replies'] = 'client Réponses';
$lang['label_user_replies'] = 'Personnel Réponses';
$lang['label_open_tickets'] = 'Les billets ouverts';
$lang['label_closed_tickets'] = 'Les billets fermés';
$lang['label_in_progress_tickets'] = 'des billets en cours';
$lang['label_answered_tickets'] = 'Billets Answered';

$lang['label_transactions'] = 'Transactions';
$lang['label_invoice_setting'] = 'Paramètres des factures';
$lang['label_invoice_prefix'] = 'Invoice Prefix';
$lang['label_invoice_footer']  =  'Pied de page de texte ';

$lang['label_estimates_setting'] = 'Paramètres Estimer';
$lang['label_estimate_prefix'] = 'Estimation Prefix';

$lang['label_quotation_setting'] = 'Paramètres Devis';
$lang['label_quotation_prefix'] = 'offre Prefix';

$lang['label_project_setting'] = 'Paramètres du projet';
$lang['label_project_permission_setting'] = 'Paramètres d\'autorisation';
$lang['label_project_prefix'] = 'Projet Prefix';


$lang['label_update_resource'] = 'Mise à jour des ressources de données' ;
$lang['seconds']  = 'secondes' ;
$lang['minutes'] = ' minutes ' ;
$lang['hours'] = ' heures ' ;
$lang['days']  = 'jours';
$lang['weeks'] = 'semaines' ;
$lang['months'] = ' mois' ;
$lang['years'] = ' années' ;
$lang['decades'] = 'décennies';
$lang['second'] = 'second' ;
$lang['minute'] = ' minute' ;
$lang['hour'] = ' heure' ;
$lang['day'] = 'jour' ;
$lang['week']  = 'de la semaine';
$lang['month'] = ' mois ' ;
$lang['year'] = 'année' ;
$lang['decade'] = 'décennie' ;
$lang['ago'] = 'ily';


/******RECUR********/
$lang['label_recur_bill'] = 'Invoice récurrent?' ;
$lang['label_quarter'] = 'Quarter' ;
$lang['label_six_months'] = 'Six mois' ;
$lang['label_invoice_discount'] = 'Remise' ;
/*
  |--------------------------------------------------------------------------
  | Buttons
  |--------------------------------------------------------------------------
 */
$lang['form_button_continue'] = "Continuer";
$lang['form_button_cancel'] = "Annuler";
$lang['form_button_send'] = "Envoyer";
$lang['form_button_save'] = "Enregistrer";
$lang['form_button_register'] = "Register";
$lang['form_button_login'] = "Connectez-vous";
$lang['form_button_logout'] = "Déconnexion";
$lang['form_button_post'] = "Post";
$lang['form_button_answer'] = "réponse";
$lang['form_button_save_profile'] = "Enregistrer le profil";
$lang['form_button_password_back'] = "Obtenir un mot de passe back";
$lang['form_button_create_account'] = "Créer un compte?";
$lang['form_button_back_login'] = "Connectez-vous";
$lang['form_button_add_project'] = "Ajouter un nouveau projet";
$lang['form_button_edit_project'] = "Editer le projet";
$lang['form_button_update'] = "Mise à jour";
$lang['form_button_add_client'] = "Nouveau client";
$lang['form_button_add_ticket'] = "Ajouter un nouveau ticket";
$lang['form_button_add_item'] = "Ajouter un nouvel élément";
$lang['form_button_add_estimate'] = 'Ajouter une nouvelle estimation';
$lang['form_button_add_department'] = "Ajouter un nouveau département";
$lang['form_button_add_quotation'] = "Ajouter une nouvelle offre";
$lang['form_button_save_quotation'] = "Enregistrer Citation";
$lang['form_button_save_item'] = "Enregistrer Item";

$lang['placeholder_username'] = "username@doe.com";
$lang['placeholder_password'] = "mot de passe";
/*
  |--------------------------------------------------------------------------
  | Labels
  |--------------------------------------------------------------------------
 */
$lang['label_milestone'] = 'Milestone' ;
$lang['label_milestone_name'] = 'Milestone Name' ;
$lang['label_notify_assigned'] = 'Notifier utilisateur (s ) Assigned' ;
$lang['label_notify_client'] = 'Notifier client ' ;
$lang['label_assign'] = 'Affectation' ;
$lang['label_closed_projects'] = 'projets fermés' ;
$lang['label_selected_country'] = 'Pays sélectionné' ;
$lang['label_other_countries'] = 'Autres pays';
$lang['label_selected_currency'] = 'Devise choisie';
$lang['label_currency'] = 'devise' ;
$lang['label_iso_code'] = 'ISO code' ;
$lang['label_iso_number'] = 'ISO numéro de code' ;
$lang['label_client_currency'] = 'utilisation client Monnaie' ;
$lang['label_profile_information'] = 'Les informations de profil' ;
$lang['label_current_status'] = 'Statut actuel' ;
$lang['label_join_date'] = 'Date d\'inscription' ;
$lang['label_last_login'] = 'Dernière connexion' ;
$lang['label_clients_with_projects'] = 'client avec des projets';
$lang['label_inactive_clients'] = 'Les clients inactifs';
$lang['label_active_clients'] = 'Clients actifs' ;


$lang['label_active_payments_gateways'] = 'paiement Active';
$lang['label_paypal'] = 'Paypal';


$lang['label_paypal_settings'] = 'Paramètres Paypal';
$lang['label_paypal_email'] = 'Paypal Adresse e-mail';
$lang['label_paypal_ipn'] = 'Paypal IPN';
$lang['label_paypal_success'] = 'Paypal Succès Url';
$lang['label_paypal_cancel'] = 'Paypal Annuler Url';
$lang['label_paypal_active'] = 'Paypal Passerelle Active';

$lang['label_skrill'] = 'Skrill';
$lang['label_skrill_settings'] = 'Paramètres Skrill';
$lang['label_skrill_email'] = 'Skrill Adresse e-mail';
$lang['label_skrill_ipn'] = 'Skrill IPN';
$lang['label_skrill_success'] = 'Skrill Succès Url';
$lang['label_skrill_cancel'] = 'Skrill Annuler Url';
$lang['label_skrill_active'] = 'Skrill passerelle active';

$lang['label_stripe'] = 'Stripe';
$lang['label_stripe_settings'] = 'Paramètres Stripe';
$lang['label_stripe_private_key'] = 'Stripe clé privée';
$lang['label_stripe_stripe_public_key'] = 'Stripe clé publique';

$lang['label_estimate_details'] = 'Estimation Details';
$lang['label_invoice_details'] = 'Invoice Details';
$lang['label_payment_details'] = 'Détails de paiement';
$lang['label_quotation_details'] = 'offre Détails';
$lang['label_project_details'] = 'Détails du projet';
$lang['label_event_details'] = 'Détails de l\'événement';

$lang['label_expenses'] = 'Dépenses' ;
$lang['label_expense_costs'] = 'Dépenses Coûts';
$lang['label_expense_budget'] = 'Dépenses Budget';
$lang['label_expense_real_cost'] = 'Expense coût réel';
$lang['label_assign_to'] = 'Assigné à';
$lang['label_task'] = "Tâche";
$lang['label_do_on'] = "Do sur";
$lang['label_use_checklist'] = "Utiliser liste?";
$lang['label_re_assign'] = "Re-assign";
$lang['label_statistics'] = "Statistiques";
$lang['label_closed_tickets_all'] = "Billets: fermé / all";
$lang['label_closed_projects_all'] = "Projets: fermé / all";
$lang['label_payments_this_year'] = 'Les paiements cette année';
$lang['label_sales_this_year'] = 'Les ventes de cette année';
$lang['label_sales_this_month'] = 'Les ventes ce mois-ci';
$lang['label_recent_payments'] = 'Les paiements récents';
$lang['label_use_worklog'] = 'Progrès: Utiliser Worklog?';
$lang['label_complete_projects'] = "Projets complets";
$lang['label_open_projects'] = "Projets ouverts";
$lang['label_stats'] = "Statistiques";
$lang['label_sales'] = "Sales";
$lang['label_recent_projects'] = "Projets récents";
$lang['label_recent_activities'] = 'Activités récentes';
$lang['label_logged_hours'] = 'heures de vol';
$lang['label_requirements'] = 'Exigences';
$lang['label_add_customer'] = "Ajouter Client";
$lang['label_client_details'] = "Détails du client";
$lang['label_issue_no'] = "Issue #";
$lang['label_issue_details'] = "Détails Issue";
$lang['label_issue_assigned_to'] = "Problème Attribuer à";
$lang['label_start_date'] = "Date de début";
$lang['label_issues'] = "Issues";
$lang['label_total_projects'] = "Nombre total de projets";
$lang['label_open_tickets'] = "Les billets ouverts" ;
$lang['label_total_clients'] = "Nombre de clients inscrits";
$lang['label_total_users'] = "Nombre total d'utilisateurs enregistrés";
$lang['label_my_tasks'] = "Mes tâches";


$lang['label_id'] = 'ID' ;
$lang['label_name'] = 'Nom' ;
$lang['label_surname'] = 'Nom';
$lang['label_email'] = 'Email';
$lang['label_general'] = 'général' ;
$lang['label_local'] = 'Local' ;
$lang['label_support'] = 'Support' ;
$lang['label_status'] = 'Status';
$lang['label_edit'] = ' Edit';
$lang['label_added_by'] = ' Ajouté par' ;
$lang['label_due_by'] = 'Due Par' ;
$lang['label_added_on'] = 'Ajouté le' ;
$lang['label_add_comment'] = 'Ajouter un commentaire ' ;
$lang['label_comment'] = 'Commentaire' ;
$lang['label_comments'] = 'Commentaires' ;
$lang['label_task_timer'] = 'Task Timer' ;
$lang['label_assigned_users'] = 'Les utilisateurs affectés ' ;
$lang['label_tick_to_send_mail_confirmation'] = " Cochez pour envoyer un courriel de confirmation" ;
$lang['label_what_we_need'] = 'Ce que nous avons besoin';
$lang['label_rate'] = 'Rate' ;
$lang['label_enable'] = ' Activer' ;
$lang['label_edit_tax'] = 'Modifier Tax' ;
$lang['label_dashboard'] = 'Dashboard';
$lang['label_add_check_list'] = 'Ajouter Liste de contrôle';


$lang['label_company_id'] = 'Entreprise ID' ;
$lang['label_company_name'] = ' Nom de la société ';
$lang['label_primary_contact'] = 'Contact principal';
$lang['label_mark_primary'] = ' Marquer comme primaire';
$lang['label_website'] = 'Site';

$lang['label_client_add'] = 'Ajouter un nouveau client';
$lang['label_project_add'] = 'Ajouter un nouveau projet';
$lang['label_user_add'] = ' Ajouter un nouvel utilisateur';
$lang['label_item_add'] = ' Ajouter un nouvel élément';
$lang['label_add_expense'] = 'Ajouter Expense' ;
$lang['label_quotation_add'] = 'Demande de devis' ;
$lang['label_estimate_add'] = 'Ajouter une nouvelle estimation';
$lang['label_user_logout'] = 'Log Out' ;


$lang['label_smtp_setting'] = "Paramètres SMTP";
$lang['label_emails'] = "Emails";
$lang['label_emails_setting'] = "Paramètres de messagerie";
$lang['label_email_type'] = "Email Paramètres de type";
$lang['label_smtp_domain_name'] = "Nom de domaine";
$lang['label_smtp_server'] = "Serveur SMTP";
$lang['label_smtp_user'] = "Nom d'utilisateur SMTP";
$lang['label_smtp_password'] = "Mot de passe SMTP";
$lang['label_smtp_encryption'] = "SMTP Type de chiffrement";
$lang['label_smtp_port'] = "Port SMTP";
$lang['label_test_email_name'] = "Test Adresse e-mail";
$lang['label_test_email_setting'] = "Test de configuration e-mail";
$lang['label_outgoing_email_setting'] = "Configuration de courrier sortant";
$lang['label_recent_activities'] = "Activités récentes";
$lang['label_priorities'] = "Priority";
$lang['label_status'] = "Etat";
$lang['label_tax_ref'] = "Tax Ref";
$lang['label_departments'] = "Département";

$lang['label_est_item'] = "Fin des travaux / Item";
$lang['label_est_description'] = "Description";
$lang['label_est_item_quantity'] = "Heures / QTY";
$lang['label_est_item_rate'] = "Taux / prix";
$lang['label_tax'] = "impôt";
$lang['label_amount'] = "Montant";
$lang['label_sub_total'] = "Sous-total";
$lang['label_total'] = "Total";
$lang['label_tax_setting'] = "Paramètres fiscaux";
$lang['label_item_setting'] = "Paramètres de l'article";
$lang['label_est_setting'] = "Paramètres Estimation";
$lang['label_est_date_setting'] = "Dates Estimer Réglage";
$lang['label_est_detail_setting'] = "Estimation Détails Réglage";
$lang['label_project_detail_setting'] = "Détails du projet Configuration";
$lang['label_project_setting'] = "Paramètres du projet";
$lang['label_project_date_setting'] = "Dates du projet Configuration";
$lang['label_project_budget'] = "Budget";

/***************************************************************/
$lang['label_active'] = "Active" ;
$lang['label_name'] = "Nom" ;
$lang['label_role'] = "Rôle " ;
$lang['label_role_id'] = "ID de rôle";
$lang['label_role_name'] = "Nom de rôle " ;
$lang['label_est_worklog'] = "Connexion estimée" ;
$lang['label_work_done'] = "Work Done" ;
$lang['label_hourly_cost'] = "Coût horaire" ;

//@TODO: users labels language
$lang['label_username'] = ' Email ou Nom d\'utilisateur' ;
$lang['label_password'] = 'Mot de passe' ;
$lang['label_remember_password'] = 'Keep me rappelle' ;
$lang['label_users'] = 'Utilisateurs';
$lang['label_users_online'] = 'Utilisateurs en ligne ';
$lang['label_profile_comments'] = 'Commentaires' ;
$lang['label_title_search_user'] = 'Recherche utilisateur ' ;
$lang['label_show_sidebar'] = 'Masquer / Afficher la barre latérale' ;
$lang['label_emp_name'] = 'Nom' ;
$lang['label_emp_surname'] = 'Nom de famille' ;
$lang['label_emp_email_address'] = 'Adresse e-mail' ;
$lang['label_emp_hourly_cost'] = 'Coût horaire' ;
$lang['label_emp_working_hours'] = 'heures par jour de travail';
$lang['label_emp_job_desc'] = ' Description de l\'emploi';
$lang['label_roles'] = ' Rôles' ;
$lang['label_language'] = 'Langue' ;
$lang['label_default_language'] = 'Langue par défaut' ;
$lang['label_complete'] = 'Complete ' ;
$lang['label_administrator'] = 'administrateur' ;


//@TODO: project labels language

$lang['label_project_name'] = 'Nom du projet' ;
$lang['label_project_description'] =  'Description du projet';
$lang['label_project_end_date'] = 'Date de fin' ;
$lang['label_project_start_date'] = 'Date de début' ;
$lang['label_project_assign_to'] = 'Assign To' ;
$lang['label_project_code'] = ' Code du projet';
$lang['label_project_title'] = ' Titre du projet';
$lang['label_project_client'] =  'client' ;
$lang['label_project_status']='Statut du projet ';
$lang['label_project_activities'] = 'Activités du projet';
$lang['label_project_duration'] = 'Durée du projet';
$lang['msg_no_found_users'] = ' Aucun utilisateur fons !';

//@TODO: PROJECT OVERWIEW
$lang['label_project_overview'] = 'Aperçu du projet';
$lang['label_project_wbs'] = 'répartition du travail';
$lang['label_project_gantt'] = 'Gantt chat' ;
$lang['label_project_milestone'] = 'Milestone' ;
$lang['label_project_assignment'] = 'Affectation' ;
$lang['label_project_documents'] = 'Documents';
$lang['label_project_comments'] = 'Commentaires' ;
$lang['label_project_time_sheet'] = 'Time Sheet' ;
$lang['label_project_tasks'] = 'Tâches';
$lang['label_project_notes'] = 'Notes';
$lang['label_project_costs'] = 'Coûts';
$lang['label_project_issues'] =' Enjeux';
$lang['label_project_expenses'] =' Dépenses' ;


//@TODO: clients labels language

$lang['label_clients'] = 'Clients' ;
$lang['label_client_name'] = 'Nom du client' ;
$lang['label_client_no'] = '# client' ;
$lang['label_clients_company_name'] = 'Nom de la société';
$lang['label_clients_company_code'] =  'Société / Nom abrégé ';
$lang['label_clients_company_status'] = ' Statut Company ';
$lang['label_clients_company_type'] = ' Type d\'entreprise';
$lang['label_clients_company_phone'] = 'numéro de téléphone';
$lang['label_clients_company_website'] = 'Site' ;
$lang['label_clients_company_email'] = 'Société Email' ;
$lang['label_clients_company_primary_contact'] = 'Contact principal';

//@TODO: locations labels language

$lang['label_location_address_1'] = 'Adresse 1';
$lang['label_location_address_2'] = 'Adresse 2' ;
$lang['label_location_address_3'] = 'Adresse 3';
$lang['label_location_zip_code'] = 'Code postal' ;
$lang['label_location_country'] = 'Country' ;
$lang['label_location_city'] = 'City' ;


$lang['msg_box_login_title'] = 'Connexion' ;

$lang['msg_user_offline'] = 'hors ligne' ;

$lang['label_chat_send'] = 'Envoyer' ;
$lang['label_public_chat'] = 'chat public' ;
$lang['label_chat_history'] = 'Histoire chat';
$lang['msg_box_register_title'] = 'Créer un compte' ;
$lang['box_register_title'] = 'Connexion ou Créer un compte';


$lang['label_notifications'] = 'Notifications' ;

//@TODO: Tasks language
$lang['t'] = 'secondes';
$lang['label_task_description'] = 'Description';
$lang['label_task_priority'] = 'Priority';
$lang['label_task_date'] = 'Date';
$lang['label_task_progress'] = 'Progress';
$lang['label_task_due'] = 'Date limite';
$lang['label_task_options'] = 'Outils';
$lang['label_task_edit'] = 'Modifier tâche';
$lang['label_task_title'] = 'Titre de la tâche';
$lang['label_task_status'] = 'Task Status';
$lang['label_task_percentage_complete'] = 'Pourcentage achevé';
$lang['label_task_start_date'] = 'Date de début';
$lang['label_task_due_date'] = 'Date de';
$lang['label_task_requirements'] = 'Exigences de la tâche';

$lang['label_add_task'] = 'Ajouter une nouvelle tâche';
$lang['label_edit_task'] = 'Modifier la tâche sélectionnée';
$lang['label_task_assign_to'] = 'Assign To';
$lang['label_task_dependant'] = 'Task Dépend';
$lang['label_task_duration'] = 'Tâche Durée';
$lang['label_code'] = 'Code';
$lang['label_is_milestone'] = 'Groupe est jalon';
$lang['label_is_visible'] = 'Visible au client';
$lang['label_client_visible'] = "visibilité client";
$lang['label_estimated_hours'] = "Tâche Heures estimées";
//@TODO: Tasks Messages
//@TODO: support labels language

$lang['label_support_ticket_no'] = 'Ticket No';
$lang['label_support_ticket_priority'] = 'Priority';
$lang['label_support_ticket_department'] = 'Département' ;
$lang['label_support_ticket_assigned_to'] = 'Ticket Assignée à ';
$lang['label_support_ticket_subject'] = 'Objet';
$lang['label_support_ticket_reporter'] = 'Rapporté par' ;


//@TODO: quotes labels language

$lang['label_quotation_status'] = 'Statut';
$lang['label_quotation_created'] = 'Crée' ;
$lang['label_quotation_budget'] = 'Budget' ;
$lang['label_quotation_company'] = 'Compagnie' ;
$lang['label_quotation_assign'] = 'Assigné à' ;
$lang['label_quotation_options'] = 'Options' ;

//@TODO: estimate labels language

$lang['label_add_estimate'] = 'nouvelles estimations' ;
$lang['label_edit_estimate'] = 'Modifier Estimations' ;
$lang['label_estimate_priority'] = 'Est Priority' ;
$lang['label_estimate_status'] = 'Est Statut' ;

$lang['label_estimate_status'] = 'Statut';
$lang['label_estimate_created'] = 'Crée' ;
$lang['label_estimate_start'] = 'Date de début Est' ;
$lang['label_estimate_due'] = ' Date de' ;
$lang['label_estimate_company'] = ' Nom de la société';
$lang['label_estimate_reference'] = 'Estimation ' ;

$lang['label_estimate_amount'] = 'Montant' ;
$lang['label_estimate_options'] = 'Options' ;
$lang['label_estimate_no'] = ' EST PAS ' ;
$lang['label_estimate_from'] = ' EST FROM' ;
$lang['label_estimate_to'] = ' EST A' ;
$lang['label_estimate_date'] = 'Date d\'estimation' ;


$lang['label_estimate_discount'] = 'Remise' ;
$lang['label_estimate_tax'] = 'impôt sur mesure en%';
$lang['label_estimate_note'] = ' Remarques Est' ;
$lang['label_estimate_end_date'] = 'Due Est Date';

//@TODO: item labels language

$lang['label_edit_item'] = 'Modifier l\'élément' ;
$lang['label_add_item'] = ' New Item' ;
$lang['label_item_name'] = ' Nom de l\'article' ;
$lang['label_item_description'] = 'Description de l\'objet';
$lang['label_item_quantity'] = 'Article Quantité' ;
$lang['label_item_price'] = 'Prix de l\'article' ;
$lang['label_item_tax'] = 'Catégorie fiscale';

/*
  |--------------------------------------------------------------------------
  | System Labels
  |--------------------------------------------------------------------------
 */
$lang['label_cron_setting'] = 'Paramètres Cron';
$lang['label_active_cron'] = 'ACTIVER cronjob';
$lang['label_create_recur'] = 'Créer une facture récurrente';
$lang['label_invoice_reminder'] = 'Auto Envoyer facture Rappel';
$lang['label_email_recur'] = 'Auto envoyer des factures récurrentes';
$lang['label_auto_database_backup'] = 'DATABASE AUTOMATIQUE BACKUP';


$lang['label_category_setting'] = 'Paramètres de catégorie';


$lang['label_general_setting'] = 'Paramètres généraux';
$lang['label_tax_setting'] = 'Paramètres fiscaux';
$lang['label_local_setting'] = 'Local Settings';
$lang['label_support_setting'] = 'Paramètres de soutien';

$lang['label_system_setting'] = 'Paramètres système';
$lang['label_purchase_code'] = 'Code d\'Achat';
$lang['label_upload_size'] = 'File Upload Max Taille';
$lang['label_allowed_files'] = 'Fichiers autorisés';
$lang['label_decimal_separator'] = 'Séparateur décimal';
$lang['label_thousand_separator'] = 'Thousand Separator';
$lang['label_symbol_position'] = 'Symbole Position';
$lang['label_demo_mode'] = 'Mode Demo';
$lang['label_google_code'] = 'Google Analytics code';


$lang['label_system_updates'] = 'Mises à jour du système';

$lang['label_php'] = 'PHP';
$lang['label_codeigniter_version'] = 'Codeigniter Version';
?>