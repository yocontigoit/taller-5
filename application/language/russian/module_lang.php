<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/10
 * Time: 10:36 AM
 */

$lang['module_home']='Dashboard';

$lang['module_project']='Projects';
$lang['module_projects_desc']='Add, Update, Delete, and Search projects';

$lang['module_client']='Clients';
$lang['module_customers_desc']='Add, Update, Delete, and Search customers';

$lang['module_user']='Users';
$lang['module_users_desc']='Add, Update, Delete, and Search users';

$lang['module_message']='Messages';
$lang['module_messages_desc']='Add, Update, Delete';

$lang['module_billing']='Billing';
$lang['module_billing_desc']='Add, Update, Delete';

$lang['module_settings']='Settings';
$lang['module_settings_desc']='view';

$lang['module_item']='Items';
$lang['module_item_desc']='Add, Update, Delete';

$lang['module_quotation']='Quotations';
$lang['module_quotations_desc']='Add, Update, Delete';

$lang['module_support']='Support';
$lang['module_support_desc']='Add, Update, Delete';

$lang['module_estimate']='Estimate';
$lang['module_estimate_desc']='Add, Update, Delete';


$lang['module_expenses']='Expenses';
$lang['module_expenses_desc']='Add, Update, Delete';


$lang['module_calendar']='Calendar';
$lang['module_report']='Reports';


$lang['module_action_add_update'] = 'Add, Update';
$lang['module_action_delete'] = 'Delete';


