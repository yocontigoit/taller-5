<?php

$lang['label_remove_folder'] = 'Delete Folder';
$lang['label_outgoing_email'] = 'System Email';
$lang['label_use_test_mail'] = 'Use Test Email';
$lang['error_email_nofound'] = 'Email Not Found!';
$lang['label_format_html'] = 'HTML FORMAT';
$lang['label_format_text'] = 'TEXT FORMAT';
$lang['label_format_both'] = 'BOTH';
$lang['label_php_mail'] = 'PHP Mail';
$lang['label_smtp'] = 'SMTP';
$lang['label_never'] = 'NEVER';
$lang['label_preview'] = 'Preview';
$lang['label_filesize'] = 'Filesize';

$lang['label_send_message'] = 'Send Messages';
$lang['label_change_password'] = 'Change Password';
$lang['label_password_repeat'] = 'Repeat New Password';
$lang['label_change_password_button'] = 'Change Password';
$lang['error_password_match'] = 'Password doesn\'t match!';
$lang['error_password_min'] = 'Password need min 5 characters.';
$lang['msg_password_updated'] = 'Password Updated!';
$lang['admin_dashboard'] = 'Dashboard';
$lang['msg_email_already_registered'] = 'Email Already Registered';
$lang['msg_email_not_valid'] = 'Email not Valid';
$lang['msg_password_doesnt_match'] = 'Password doesn\'t match!';
$lang['msg_password_min_characters'] = 'Password need min 5 characters.';
$lang['admin_items_top'] = 'Limit Top Items';
$lang['admin_items_search'] = 'Limit Search Items';
$lang['admin_custom_pages'] = 'Custom Pages';
$lang['label_page'] = 'Pages';
$lang['label_company_logo'] = 'Company Logo';
$lang['label_income'] = 'Income';
$lang['label_profile'] = 'Profile';
$lang['label_projects'] = 'Projects';
$lang['label_name'] = 'Name';
$lang['label_surname'] = 'Surname';
$lang['label_title'] = 'Title';
$lang['label_city'] = 'City';
$lang['label_country'] = 'Country Name';
$lang['label_phone'] = 'Phone Number';
$lang['label_email'] = 'Email';
$lang['label_company_name'] = 'Company';
$lang['label_owner'] = 'Owner';
$lang['label_registration_no'] = 'Registration No';
$lang['label_vat_no'] = 'Vat No';
$lang['label_slogan'] = 'Slogan';
$lang['label_to'] = 'To';
$lang['label_none'] = 'None';
$lang['label_view'] = 'View';
$lang['label_reply'] = 'Reply';
$lang['label_attachment'] = 'Attachment';
$lang['label_attached_files'] = 'Attached Files';
$lang['label_mail_subject'] = 'Mail subject';
$lang['label_browse_files'] = 'Browse files';
$lang['label_tax_ref'] = 'Tax Ref';
$lang['label_date'] = 'Date';
$lang['label_options'] = 'Options';
$lang['label_contacts'] = 'Contacts';
$lang['label_open'] = 'Open';
$lang['label_closed'] = 'Closed';
$lang['label_answered'] = 'Answered';
$lang['label_progress'] = 'In progress';
$lang['label_project_progress'] = 'Progress';
$lang['label_tickets'] = 'Support Tickets';
$lang['label_classification'] = 'Classification';
$lang['label_estimate_cost']= 'Estimated Cost';
$lang['label_real_cost']= 'Real Cost';
$lang['label_actual_cost']= 'Actual Cost';
$lang['label_description']= 'Description';
$lang['label_additional_costs']= 'Additional Costs';
$lang['label_add_additional_costs']= 'Add Additional Costs';
$lang['label_estimate_hours']= 'Estimated Hours';
$lang['label_done']= 'Done';
$lang['label_brief']= 'Brief';
$lang['label_risk_analysis'] = 'Risk Analysis';
$lang['label_total_estimate'] = 'Total Estimation';
$lang['label_total_done'] = 'Total Done';
$lang['label_cost_delta'] = 'Calculation';
$lang['label_category'] = 'Category';
$lang['label_date_created'] = 'Date Created';
$lang['label_details'] = 'Details';
$lang['label_settings'] = 'Settings';
$lang['label_file_name'] = 'File name';
$lang['label_file_format'] = 'file format';
$lang['label_backup'] = 'Backup';
$lang['label_symbol'] = 'Symbol';
$lang['label_enabled'] = 'Enabled';
$lang['label_exchange_rate'] = 'Exchange rate';
$lang['label_state'] = 'Region / State';
$lang['label_default_currency'] = 'Default Currency';
$lang['label_default_tax'] = 'Default Tax';
$lang['label_time_zone'] = 'Time zone';
$lang['label_templates'] = 'templates';
$lang['label_categories'] = 'Categories';
$lang['label_payments'] = 'Payments';
$lang['label_system'] = 'System';
$lang['label_cron'] = 'Crons';
$lang['label_updates'] = 'Updates';

/*
  |--------------------------------------------------------------------------
  | Project Permissions
  |--------------------------------------------------------------------------
 */
$lang['label_view_team_members'] = 'Allow client to view team members';
$lang['label_view_milestones'] = 'Allow client to view project milestones';
$lang['label_view_tasks'] = 'Allow client to view project tasks';
$lang['label_comment_on_tasks'] = 'Allow client to comment on project tasks';
$lang['label_view_gantt'] = 'Allow client to view project gantt';
$lang['label_view_project_files'] = 'Allow client to view project files';
$lang['label_view_project_comments'] = 'Allow clients to view project comments';
$lang['label_comment_on_project'] = 'Allow clients to comment on project';
$lang['label_view_issues'] = 'Allow client to view project issues';
$lang['label_view_timesheets'] = 'Allow clients to view project timesheet';
$lang['label_show_project_comments'] = 'Allow clients to view project comments';
$lang['label_view_milestones'] = 'Allow client to view milestones';
$lang['label_view_expenses'] = 'Allow client to view project expenses';
$lang['label_view_costs'] = 'Allow client to view project costs';
/*
  |--------------------------------------------------------------------------
  | Invoice
  |--------------------------------------------------------------------------
 */
$lang['label_invoice_due_after'] = "All Invoice Due After";
$lang['label_invoice_item'] = "Work Completed/Item";
$lang['label_invoice_description'] = "Description";
$lang['label_invoice_item_quantity'] = "Hours/QTY";
$lang['label_invoice_item_rate'] = "Rate/Price";
$lang['label_change_avatar'] = 'Change Profile Picture';
$lang['label_change_picture'] = 'Change Picture';
$lang['label_invoices'] = 'Invoices';
$lang['label_invoice_to'] = 'Invoice To';
$lang['label_balance'] = 'Balance';
$lang['label_invoice'] = 'Invoice';
$lang['label_invoice_date'] = 'Invoice Date';
$lang['label_invoice_no'] = 'Invoice #';
$lang['label_invoice_items'] = 'Invoice Items';
$lang['label_quantity'] = 'Quantity';
$lang['label_due_date'] = 'Due Date';
$lang['label_amount_due'] = 'Amount Due';
$lang['label_payment_method'] = 'Payment Method';
$lang['label_transaction_reference'] = 'Transaction Ref';
$lang['label_download'] = 'Download';
$lang['label_documents'] = 'Documents';
$lang['label_delete'] = 'Delete';
$lang['label_disable'] = 'Disable';
$lang['label_edit'] = 'Edit';
$lang['label_print'] = 'Print';
$lang['label_paid'] = 'paid';
$lang['label_unpaid'] = 'unpaid';
$lang['label_overdue'] = 'Overdue';
$lang['label_version'] = 'version';
$lang['label_Build'] = 'Build';
$lang['label_partially_paid'] = 'Partially Paid';
$lang['label_accept'] = 'Accept';
$lang['label_decline'] = 'Decline';
$lang['label_paid_amount'] = 'Paid Amount';
$lang['label_unpaid_amount'] = 'Unpaid Amount';
$lang['label_overdue_amount'] = 'Overdue Amount';
$lang['label_expense_amount'] = 'Expense Amount';
$lang['label_expense_cost'] = 'Expense Cost';
$lang['label_expense_date'] = 'Expense Date';

$lang['label_overdue_invoices'] = 'Overdue Invoices';
$lang['label_overview'] = 'Overview';
$lang['label_billing'] = 'Billing';
$lang['label_transaction'] = 'Transaction';
$lang['label_transaction_id'] = 'Transaction ID';
$lang['label_list_all_invoices'] = 'List All Invoices';
$lang['label_add_department'] = 'Add Departments';
$lang['label_email_inbox'] = 'Inbox';
$lang['label_email_sent'] = 'Sent';
$lang['label_email_trash'] = 'Trash';
$lang['label_summary'] = "Summary";
$lang['label_add_payment'] = "Add Payment";
$lang['label_notes'] = 'Notes';
$lang['label_notes_log'] = 'Notes Log';

$lang['label_add_project'] = 'Add New Project';
$lang['label_create_invoice'] = 'Create Invoice';
$lang['label_create_estimate'] = 'Create Estimate';
$lang['label_edit_project'] = 'Edit Project';
$lang['label_client_replies'] = 'Client Replies';
$lang['label_user_replies'] = 'Staff Replies';
$lang['label_open_tickets'] = 'Open Tickets';
$lang['label_closed_tickets'] = 'Closed Tickets';
$lang['label_in_progress_tickets'] = 'Tickets In progress';
$lang['label_answered_tickets'] = 'Answered  Tickets';

$lang['label_transactions'] = 'Transactions';
$lang['label_invoice_setting'] = 'Invoice Settings';
$lang['label_invoice_prefix'] = 'Invoice Prefix';
$lang['label_invoice_footer'] = 'Footer Text';

$lang['label_estimates_setting'] = 'Estimate Settings';
$lang['label_estimate_prefix'] = 'Estimate Prefix';

$lang['label_quotation_setting'] = 'Quotation Settings';
$lang['label_quotation_prefix'] = 'Quotation Prefix';

$lang['label_project_setting'] = 'Project Settings';
$lang['label_project_permission_setting'] = 'Permission Settings';
$lang['label_project_prefix'] = 'Project Prefix';


$lang['label_update_resource'] = 'Update Resources Data';
$lang['seconds'] = 'seconds';
$lang['minutes'] = 'minutes';
$lang['hours'] = 'hours';
$lang['days'] = 'days';
$lang['weeks'] = 'weeks';
$lang['months'] = 'months';
$lang['years'] = 'years';
$lang['decades'] = 'decades';
$lang['second'] = 'second';
$lang['minute'] = 'minute';
$lang['hour'] = 'hour';
$lang['day'] = 'day';
$lang['week'] = 'week';
$lang['month'] = 'month';
$lang['year'] = 'year';
$lang['decade'] = 'decade';
$lang['ago'] = 'ago';


/******RECUR********/
$lang['label_recur_bill'] = 'Recurring Invoice?';
$lang['label_quarter'] = 'Quarter';
$lang['label_six_months'] = 'Six Months';
$lang['label_invoice_discount'] = 'Discount';


/*
  |--------------------------------------------------------------------------
  | Buttons
  |--------------------------------------------------------------------------
 */
$lang['form_button_continue'] = "Continue";
$lang['form_button_cancel'] = "Cancel";
$lang['form_button_send'] = "Send";
$lang['form_button_save'] = "Save";
$lang['form_button_register'] = "Register";
$lang['form_button_login'] = "Sign in";
$lang['form_button_logout'] = "Logout";
$lang['form_button_post'] = "Post";
$lang['form_button_answer'] = "Answer";
$lang['form_button_save_profile'] = "Save Profile";
$lang['form_button_password_back'] = "Get password back";
$lang['form_button_create_account'] = "Create account?";
$lang['form_button_back_login'] = "Sign in";
$lang['form_button_add_project'] = "Add New Project";
$lang['form_button_edit_project'] = "Edit Project";
$lang['form_button_update'] = "Update";
$lang['form_button_add_client'] = "Add New Client";
$lang['form_button_add_ticket'] = "Add New Ticket";
$lang['form_button_add_item'] = "Add New Item";
$lang['form_button_add_estimate'] = "Add New Estimate";
$lang['form_button_add_department'] = "Add New Department";
$lang['form_button_add_quotation'] = "Add New Quotation";
$lang['form_button_save_quotation'] = "Save Quotation";
$lang['form_button_save_item'] = "Save Item";

$lang['placeholder_username'] = "username@doe.com";
$lang['placeholder_password'] = "password";
/*
  |--------------------------------------------------------------------------
  | Labels
  |--------------------------------------------------------------------------
 */
$lang['label_milestone'] = 'Milestone';
$lang['label_milestone_name'] = 'Milestone Name';
$lang['label_notify_assigned'] = 'Notify Assigned User(s)';
$lang['label_notify_client'] = 'Notify Client';
$lang['label_assign'] = 'Assignment';
$lang['label_closed_projects'] = 'closed projects';
$lang['label_selected_country'] = 'Selected Country';
$lang['label_other_countries'] = 'Other Countries';
$lang['label_selected_currency'] = 'Selected Currency';
$lang['label_currency'] = 'Currency';
$lang['label_iso_code'] = 'ISO code';
$lang['label_iso_number'] = 'ISO code number';
$lang['label_client_currency'] = 'Use Client Currency';
$lang['label_profile_information'] = 'Profile information';
$lang['label_current_status'] = 'Current Status';
$lang['label_join_date'] = 'Join Date';
$lang['label_last_login'] = 'Last Login';
$lang['label_clients_with_projects'] = 'Client with projects';
$lang['label_inactive_clients'] = 'Inactive Clients';
$lang['label_active_clients'] = 'Active Clients';




$lang['label_active_payments_gateways'] = 'Active payment';
$lang['label_paypal'] = 'Paypal';


$lang['label_paypal_settings'] = 'Paypal Settings';
$lang['label_paypal_email'] = 'Paypal Email Address';
$lang['label_paypal_ipn'] = 'Paypal IPN';
$lang['label_paypal_success'] = 'Paypal Success Url';
$lang['label_paypal_cancel'] = 'Paypal Cancel Url';
$lang['label_paypal_active'] = 'Paypal Gateway Active';

$lang['label_skrill'] =  'Skrill';
$lang['label_skrill_settings'] = 'Skrill Settings';
$lang['label_skrill_email'] = 'Skrill Email Address';
$lang['label_skrill_ipn'] = 'Skrill IPN';
$lang['label_skrill_success'] = 'Skrill Success Url';
$lang['label_skrill_cancel'] = 'Skrill Cancel Url';
$lang['label_skrill_active'] = 'Skrill Gateway Active';

$lang['label_stripe'] =  'Stripe';
$lang['label_stripe_settings'] = 'Stripe Settings';
$lang['label_stripe_private_key'] = 'Stripe Private Key';
$lang['label_stripe_stripe_public_key'] = 'Stripe Public Key';

$lang['label_estimate_details'] = 'Estimate Details';
$lang['label_invoice_details'] = 'Invoice Details';
$lang['label_payment_details'] = 'Payment Details';
$lang['label_quotation_details'] = 'Quotation Details';
$lang['label_project_details'] = 'Project Details';
$lang['label_event_details'] = 'Event Details';

$lang['label_expenses'] = 'Expenses';
$lang['label_expense_costs'] = 'Expenses Costs';
$lang['label_expense_budget'] = 'Expenses Budget';
$lang['label_expense_real_cost'] = 'Expense Real Cost';
$lang['label_assign_to'] = 'Assigned to';
$lang['label_task'] = "Task";
$lang['label_do_on'] = "Do on";
$lang['label_use_checklist'] = "Use checklist?";
$lang['label_re_assign'] = "Re-assign";
$lang['label_statistics'] = "Statistics";
$lang['label_closed_tickets_all'] = "Tickets: closed/all";
$lang['label_closed_projects_all'] = "Projects: closed/all";
$lang['label_payments_this_year'] = "Payments this year";
$lang['label_sales_this_year'] = "Sales this year";
$lang['label_sales_this_month'] = "Sales this month";
$lang['label_recent_payments'] = "Recent Payments";
$lang['label_use_worklog'] = "Progress : Use Worklog?";
$lang['label_complete_projects'] = "Complete Projects";
$lang['label_open_projects'] = "Open Projects";
$lang['label_stats'] = "Stats";
$lang['label_sales'] = "Sales";
$lang['label_recent_projects'] = "Recent Projects";
$lang['label_recent_activities'] = 'Recent Activities';
$lang['label_logged_hours'] = 'Logged Hours';
$lang['label_requirements'] = 'Requirements';
$lang['label_add_customer']  = "Add Customer";
$lang['label_client_details']  = "Client Details";
$lang['label_issue_no'] = "Issue #";
$lang['label_issue_details'] = "Issue Details";
$lang['label_issue_assigned_to'] = "Issue Assign To";
$lang['label_start_date'] = "Start Date";
$lang['label_issues'] = "Issues";
$lang['label_total_projects'] = "Total Projects";
$lang['label_open_tickets'] = "Open Tickets";
$lang['label_total_clients'] = " Total Clients Registered";
$lang['label_total_users'] = "Total Users Registered";
$lang['label_my_tasks'] = "My Tasks";
$lang['label_id'] = 'ID';
$lang['label_name'] = 'Name';
$lang['label_surname'] = 'Surname';
$lang['label_email'] = 'Email';
$lang['label_general'] = 'General';
$lang['label_local'] = 'Local';
$lang['label_support'] = 'Support';
$lang['label_status'] = 'Status';
$lang['label_edit'] = 'Edit';
$lang['label_added_by'] = 'Added By';
$lang['label_due_by'] = 'Due By';
$lang['label_added_on'] = 'Added On';
$lang['label_add_comment'] = 'Add Comment';
$lang['label_comment'] = 'Comment';
$lang['label_comments'] = 'Comments';
$lang['label_task_timer'] = 'Task Timer';
$lang['label_assigned_users'] = 'Assigned Users';
$lang['label_tick_to_send_mail_confirmation'] = "Tick to Send Confirmation Email";
$lang['label_what_we_need'] = 'What we need';
$lang['label_rate'] = 'Rate';
$lang['label_enable'] = 'Enable';
$lang['label_edit_tax'] = 'Edit Tax';
$lang['label_dashboard'] = 'Dashboard';
$lang['label_add_check_list'] = 'Add Checklist';


$lang['label_company_id'] = 'Company ID';
$lang['label_company_name'] = 'Company Name';
$lang['label_primary_contact'] = 'Primary Contact';
$lang['label_mark_primary'] = 'Mark As Primary';
$lang['label_website'] = 'Website';

$lang['label_client_add'] = 'Add New Client';
$lang['label_project_add'] = 'Add New Project';
$lang['label_user_add'] = 'Add New User';
$lang['label_item_add'] = 'Add New Item';
$lang['label_add_expense'] = 'Add Expense';
$lang['label_quotation_add'] = 'Request Quote';
$lang['label_estimate_add'] = 'Add New Estimate';

$lang['label_user_logout'] = 'Log Out';





$lang['label_smtp_setting'] = "SMTP Settings";
$lang['label_emails'] = "Emails";
$lang['label_emails_setting'] = "Email Settings";
$lang['label_email_type'] = "Email Type Settings";
$lang['label_smtp_domain_name'] = "Domain Name";
$lang['label_smtp_server'] = "SMTP Server";
$lang['label_smtp_user'] = "SMTP Username";
$lang['label_smtp_password'] = "SMTP Password";
$lang['label_smtp_encryption'] = "SMTP Encryption Type";
$lang['label_smtp_port'] = "SMTP Port";
$lang['label_test_email_name'] = "Test  Email Address";
$lang['label_test_email_setting'] = "Test Email Configuration";
$lang['label_outgoing_email_setting'] = "Outgoing Email Configuration";
$lang['label_recent_activities'] = "Recent Activities";
$lang['label_priorities'] = "Priority";
$lang['label_status'] = "Status";
$lang['label_tax_ref'] = "Tax Ref";
$lang['label_departments'] = "Department";

$lang['label_est_item'] = "Work Completed/Item";
$lang['label_est_description'] = "Description";
$lang['label_est_item_quantity'] = "Hours/QTY";
$lang['label_est_item_rate'] = "Rate/Price";
$lang['label_tax'] = "Tax";
$lang['label_amount'] = "Amount";
$lang['label_sub_total'] = "Sub Total";
$lang['label_total'] = "Total";
$lang['label_tax_setting'] = "Tax Settings";
$lang['label_item_setting'] = "Item Settings";
$lang['label_est_setting'] = "Estimate Settings";
$lang['label_est_date_setting'] = "Estimate Dates Setting";
$lang['label_est_detail_setting'] = "Estimate Details Setting";
$lang['label_project_detail_setting'] = "Project Details Setting";
$lang['label_project_setting'] = "Project Settings";
$lang['label_project_date_setting'] = "Project Dates Setting";
$lang['label_project_budget'] = "Budget";


$lang['label_active'] = "Active";
$lang['label_name'] = "Name";
$lang['label_role'] = "Role";
$lang['label_role_id'] = "Role ID";
$lang['label_role_name'] = "Role Name";

$lang['label_est_worklog'] = "Estimated Log";

$lang['label_work_done'] = "Work Done";

$lang['label_hourly_cost'] = "Hourly Cost";


//@TODO: users labels language
$lang['label_username'] = 'Email or Username';
$lang['label_password'] = 'Password';
$lang['label_remember_password'] = 'Keep me reminded ';
$lang['label_users'] = 'Users';
$lang['label_users_online'] = 'Users Online';
$lang['label_profile_comments'] = 'Comments';
$lang['label_title_search_user'] = 'Search User';
$lang['label_show_sidebar'] = 'Hide/Show Sidebar';
$lang['label_emp_name'] = 'Name';
$lang['label_emp_surname'] = 'Surname';
$lang['label_emp_email_address'] = 'Email address';
$lang['label_emp_hourly_cost'] = 'Hourly Cost';
$lang['label_emp_working_hours'] = 'Working hours per day';
$lang['label_emp_job_desc'] = 'Job Description';
$lang['label_roles'] = 'Roles';
$lang['label_language'] = 'Language';
$lang['label_default_language'] = 'Default Language';
$lang['label_complete'] = 'Complete';
$lang['label_administrator'] = 'Administrator';


//@TODO: project labels language
$lang['label_project_name'] = 'Project Name';
$lang['label_project_description'] = 'Project Description';
$lang['label_project_end_date'] = 'End Date';
$lang['label_project_start_date'] = 'Start Date';
$lang['label_project_assign_to'] = 'Assign To';
$lang['label_project_code'] = 'Project Code';
$lang['label_project_title'] = 'Project Title';
$lang['label_project_client'] = 'Client';
$lang['label_project_status'] = 'Project Status';
$lang['label_project_activities'] = 'Project Activities';
$lang['label_project_duration'] = 'Project Duration';
$lang['msg_no_found_users'] = 'No users founds!';

//@TODO: PROJECT OVERWIEW
$lang['label_project_overview'] = 'Project Overview';
$lang['label_project_wbs'] = 'Work breakdown';
$lang['label_project_gantt'] = 'Gantt Chat';
$lang['label_project_milestone'] = 'Milestone';
$lang['label_project_assignment'] = 'Assignment';
$lang['label_project_documents'] = 'Documents';
$lang['label_project_comments'] = 'Comments';
$lang['label_project_time_sheet'] = 'Time Sheet';
$lang['label_project_tasks'] = 'Tasks';
$lang['label_project_notes'] = 'Notes';
$lang['label_project_costs'] = 'Costs';
$lang['label_project_issues'] = 'Issues';
$lang['label_project_expenses'] = 'Expenses';


//@TODO: clients labels language
$lang['label_clients'] = 'Clients';
$lang['label_client_name'] = 'Client Name';
$lang['label_client_no'] = 'Client #';
$lang['label_clients_company_name'] = 'Company Name';
$lang['label_clients_company_code'] = 'Company Code/Short Name';
$lang['label_clients_company_status'] = 'Company Status';
$lang['label_clients_company_type'] = 'Company Type';
$lang['label_clients_company_phone'] = 'Phone Number';
$lang['label_clients_company_website'] = 'Website';
$lang['label_clients_company_email'] = 'Company Email';
$lang['label_clients_company_primary_contact'] = 'Primary Contact';

//@TODO: locations labels language
$lang['label_location_address_1'] = 'Address 1';
$lang['label_location_address_2'] = 'Address 2';
$lang['label_location_address_3'] = 'Address 3';
$lang['label_location_zip_code'] = 'Zip Code';
$lang['label_location_country'] = 'Country';
$lang['label_location_city'] = 'City';


$lang['msg_box_login_title'] = 'Login';

$lang['msg_user_offline'] = 'User Offline';

$lang['label_chat_send'] = 'Send';
$lang['label_public_chat'] = 'Public Chat';
$lang['label_chat_history'] = 'History Chat';
$lang['msg_box_register_title'] = 'Create account';
$lang['box_register_title'] = 'Login or Create account';


$lang['label_notifications'] = 'Notifications';

//@TODO: Tasks language
$lang['t'] = 'seconds';

$lang['label_task_description'] = 'Description';
$lang['label_task_priority'] = 'Priority';
$lang['label_task_date'] = 'Date Added';
$lang['label_task_progress'] = 'Progress';
$lang['label_task_due'] = 'Deadline';
$lang['label_task_options'] = 'Tools';
$lang['label_task_edit'] = 'Edit task';
$lang['label_task_title'] = 'Task Title';
$lang['label_task_status'] = 'Task Status';
$lang['label_task_percentage_complete'] = 'Percent Complete';
$lang['label_task_start_date'] = 'Start Date';
$lang['label_task_due_date'] = 'Due Date';
$lang['label_task_requirements'] = 'Task Requirements';

$lang['label_add_task'] = 'Add a New Task';
$lang['label_edit_task'] = 'Edit Selected Task';
$lang['label_task_assign_to'] = 'Assign To';
$lang['label_task_dependant'] = 'Task Dependant  On';
$lang['label_task_duration'] = 'Task Duration';
$lang['label_code'] = 'Code';
$lang['label_is_milestone'] = 'Task is  milestone';
$lang['label_is_visible'] = 'Visible to client';
$lang['label_client_visible'] = "Client visibility";
$lang['label_estimated_hours'] = "Task Estimated Hours";
//@TODO: Tasks Messages
//@TODO: support labels language
$lang['label_support_ticket_no'] = 'Ticket No';
$lang['label_support_ticket_priority'] = 'Priority';
$lang['label_support_ticket_department'] = 'Department';
$lang['label_support_ticket_assigned_to'] = 'Ticket Assigned To';
$lang['label_support_ticket_subject'] = 'Subject';
$lang['label_support_ticket_reporter'] = 'Reported By';



//@TODO: quotes labels language

$lang['label_quotation_status'] = 'Status';
$lang['label_quotation_created'] = 'Created';
$lang['label_quotation_budget'] = 'Budget';
$lang['label_quotation_company'] = 'Company';
$lang['label_quotation_assign'] = 'Assigned to';
$lang['label_quotation_options'] = 'Options';

//@TODO: estimate labels language
$lang['label_add_estimate'] = 'New Estimates';
$lang['label_edit_estimate'] = 'Edit Estimates';
$lang['label_estimate_priority'] = 'Est Priority';
$lang['label_estimate_status'] = 'Est Status';

$lang['label_estimate_status'] = 'Status';
$lang['label_estimate_created'] = 'Created';
$lang['label_estimate_start'] = 'Est Start Date';
$lang['label_estimate_due'] = 'Due Date';
$lang['label_estimate_company'] = 'Company Name';
$lang['label_estimate_reference'] = 'Estimate';

$lang['label_estimate_amount'] = 'Amount';
$lang['label_estimate_options'] = 'Options';
$lang['label_estimate_no'] = 'EST NO';
$lang['label_estimate_from'] = 'EST FROM';
$lang['label_estimate_to'] = 'EST TO';
$lang['label_estimate_date'] = 'Date of Estimate';
$lang['label_estimate_date'] = 'Date of Estimate';

$lang['label_estimate_discount'] = 'Discount';
$lang['label_estimate_tax'] = 'Custom Tax in %';
$lang['label_estimate_note'] = 'Est Notes';
$lang['label_estimate_end_date'] = 'Est Due Date';


//@TODO: item labels language
$lang['label_edit_item'] = 'Edit Item';
$lang['label_add_item'] = 'New Item';
$lang['label_item_name'] = 'Item Name';
$lang['label_item_description'] = 'Item Description';
$lang['label_item_quantity'] = 'Item Quantity';
$lang['label_item_price'] = 'Item Price';
$lang['label_item_tax'] = 'Tax Class';

/*
  |--------------------------------------------------------------------------
  | System Labels
  |--------------------------------------------------------------------------
 */

$lang['label_cron_setting'] = 'Cron Settings';
$lang['label_active_cron'] = 'ACTIVATE  CRONJOB';
$lang['label_create_recur'] = 'Create Recurring Invoice';
$lang['label_invoice_reminder'] = 'Auto  Send Invoice Reminder';
$lang['label_email_recur'] = 'Auto send recurring Invoice';
$lang['label_auto_database_backup'] = 'AUTOMATIC DATABASE BACKUP';


$lang['label_category_setting'] = 'Category Settings';


$lang['label_general_setting'] = 'General Settings';
$lang['label_tax_setting'] = 'Tax Settings';
$lang['label_local_setting'] = 'Local Settings';
$lang['label_support_setting'] = 'Support Settings';

$lang['label_system_setting'] = 'System Settings';
$lang['label_purchase_code'] = 'Purchase Code';
$lang['label_upload_size'] = 'File Upload Max Size';
$lang['label_allowed_files'] = 'Allowed Files';
$lang['label_decimal_separator'] = 'Decimal Separator';
$lang['label_thousand_separator'] = 'Thousand Separator';
$lang['label_symbol_position'] = 'Symbol Position';
$lang['label_demo_mode'] = 'Demo Mode';
$lang['label_google_code'] = 'Google Analytics Code';


$lang['label_system_updates'] = 'System Updates';

$lang['label_php'] = 'PHP';
$lang['label_codeigniter_version'] = 'Codeigniter Version';
?>