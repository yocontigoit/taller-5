<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/04
 * Time: 12:09 PM
 */

$lang['messages_login_need_account']= 'Need an account?';
$lang['messages_login_have_account'] = 'Already registered?';

$lang['messages_no_user_found']     = 'We could not find any users.';
$lang['messages_no_client_found']   = 'We could not find any customers.';
$lang['messages_no_projects_found'] = 'We could not find any projects.';


// Account Creation
$lang['messages_account_creation_successful'] 	  	        = 'Account Successfully Created';
$lang['messages_account_creation_unsuccessful'] 	 	    = 'Unable to Create Account';
$lang['messages_account_creation_duplicate_email'] 	        = 'Email Already Used or Invalid';
$lang['messages_account_creation_duplicate_username']       = 'Username Already Used or Invalid';

// Password
$lang['messages_password_change_successful'] 	 	 = 'Password Successfully Changed';
$lang['messages_password_change_unsuccessful'] 	  	 = 'Unable to Change Password';
$lang['messages_forgot_password_successful'] 	 	 = 'Password Reset Email Sent';
$lang['messages_forgot_password_unsuccessful'] 	 	 = 'Unable to Reset Password';
$lang['messages_error_login']			             =  'User or Password Incorrect';

// Activation
$lang['messages_activate_successful'] 		  	     = 'Account Activated';
$lang['messages_activate_unsuccessful'] 		 	     = 'Unable to Activate Account';
$lang['messages_deactivate_successful'] 		  	     = 'Account De-Activated';
$lang['messages_deactivate_unsuccessful'] 	  	     = 'Unable to De-Activate Account';
$lang['messages_activation_email_successful'] 	  	 = 'Activation Email Sent';
$lang['messages_activation_email_unsuccessful']   	 = 'Unable to Send Activation Email';

// Login / Logout
$lang['messages_login_successful'] 		  	             = 'Logged In Successfully';
$lang['messages_login_unsuccessful'] 		  	         = 'Incorrect Login';
$lang['messages_login_unsuccessful_not_active'] 		 = 'Account is inactive';
$lang['messages_logout_successful'] 		 	         = 'Logged Out Successfully';

// Account Changes
$lang['messages_update_successful'] 		 	         = 'Account Information Successfully Updated';
$lang['messages_update_unsuccessful'] 		 	         = 'Unable to Update Account Information';
$lang['messages_delete_successful'] 		 	         = 'User Deleted';
$lang['messages_delete_unsuccessful'] 		 	         = 'Unable to Delete User';

// Email Subjects
$lang['messages_email_forgotten_password_subject']        = 'Forgotten Password Verification';
$lang['messages_email_new_password_subject']              = 'New Password';
$lang['messages_email_activation_subject']                = 'Account Activation';


$lang['messages_ticket_reply_success'] = "Ticket has been replied to!";
$lang['messages_mail_reply_success'] = "Mail has been replied to!";
$lang['messages_mail_sent_success'] = "New Mail has been sent!";

$lang['messages_invoice_overdue'] = 'This Invoice is overdue! Please pay it now!';
$lang['messages_estimate_overdue'] = 'This estimate is overdue!';
/*************************************************************************************************/
//add
/*************************************************************************************************/
$lang['messages_client_add_success'] = "New client has been added!";
$lang['messages_invoice_add_success'] = "New Invoice has been created!";
$lang['messages_item_add_success'] = "Item has been added!";
$lang['messages_project_add_success'] = "New project has been created!";
$lang['messages_task_add_success'] = "Task has been added!";
$lang['messages_media_add_success'] = "Media file has been saved!";
$lang['messages_user_add_success'] = "The user has been created!";
$lang['messages_quotation_add_success'] = "Quotation has been added!";
$lang['messages_ticket_add_success'] = "Ticket has been added!";
$lang['messages_template_add_success'] = "Template has been added!";
$lang['messages_client_null_error'] = "Client not selected";
$lang['messages_note_add_success'] = "Note has been added";
$lang['messages_tax_add_success'] = "Tax has been added";
$lang['messages_avatar_add_success'] = "Profile picture added!";
$lang['message_department_add_success'] = "Department has been added!";
$lang['messages_currency_add_success'] = "Currency has been added!";
$lang['messages_tax_add_success'] = "Tax has been added!";
$lang['message_transaction_settings_add_success'] = "Transactions settings has been added!";
$lang['messages_issue_add_success'] = "New issue has been added!";
$lang['messages_milestone_add_success'] = "New milestone has been added!";
$lang['messages_contact_add_success'] = "New contact has been added!";
$lang['messages_invoice_add_success'] = "New invoice has been created!";
/*************************************************************************************************/
//update
/*************************************************************************************************/
$lang['messages_client_save_success'] = "Client has been updated!";
$lang['messages_invoice_save_success'] = "Invoice has been updated!";
$lang['messages_item_save_success'] = "Item has been updated!";
$lang['messages_project_save_success'] = "Project has been updated!";
$lang['messages_task_save_success'] = "Task has been added!";
$lang['messages_media_save_success'] = "Media file has been saved!";
$lang['messages_user_save_success'] = "Your changes have been saved!";
$lang['messages_quotation_save_success'] = "Quotation has been updated";
$lang['messages_ticket_save_success'] = "Ticket has been saved!";
$lang['messages_settings_save_success'] = "Changes saved!";
$lang['messages_note_save_success'] =  "Note  updated successfully";
$lang['messages_estimate_item_save_success'] = "Estimate items  updated successfully";
$lang['messages_currency_save_success'] = "Currency has been  updated successfully";
$lang['messages_system_save_success'] = "System Settings has been  updated successfully";
$lang['message_general_save_success'] = "General Settings  has been  updated successfully";
$lang['message_local_save_success'] = "Local Settings  has been  updated successfully";
$lang['message_email_save_success'] = "Email Settings  has been  updated successfully";
$lang['messages_template_save_success'] = "Template Settings  has been  updated successfully";
$lang['messages_tax_save_success'] = "Tax Settings  has been  updated successfully";
$lang['message_transaction_settings_save_success'] = "Transactions settings  has been  updated successfully";
$lang['message_role_save_success'] = "Role permissions updated successfully";
$lang['messages_cost_save_success'] = "Costs updated successfully";
$lang['messages_issue_save_success'] = "Issue has been updated successfully";
$lang['messages_contact_save_success'] = "Contact has been updated successfully!";
$lang['messages_paypal_save_success'] = "Paypal updated successfully!";
$lang['messages_payfast_save_success'] = "Payfast updated successfully!";
$lang['messages_skrill_save_success'] = "Skrill updated successfully!";
$lang['messages_stripe_save_success'] = "Stripe updated successfully!";
$lang['messages_milestone_save_success'] = "Milestone updated successfully!";
$lang['messages_estimate_save_success'] = "Estimate updated successfully!";
/*************************************************************************************************/
//Delete
/*************************************************************************************************/
$lang['messages_client_delete_success'] = "Client has been deleted!";
$lang['messages_invoice_delete_success'] = "Invoice has been deleted!";
$lang['messages_item_delete_success'] = "Item has been deleted!";
$lang['messages_project_delete_success'] = "Project has been deleted!";
$lang['messages_task_delete_success'] = "Task has been deleted!";
$lang['messages_media_delete_success'] = "Media has been deleted!";
$lang['messages_user_delete_success'] = "User has been deleted!";
$lang['messages_quotation_delete_success'] = "Quotation has been deleted";
$lang['messages_ticket_delete_success'] = "Ticket has been deleted!";
$lang['messages_template_delete_success'] = "Template has been deleted!";
$lang['messages_contact_delete_success'] = "Contact has been deleted!";
$lang['messages_client_delete_success'] = "Client has been deleted!";
$lang['messages_payment_add_success'] = "Payment added successfully!";
$lang['messages_comment_delete_success'] = "Comment has been deleted successfully!";
$lang['messages_invoice_delete_success'] = "Invoice has been deleted successfully!";
$lang['messages_payment_delete_success'] = "Payment has been deleted successfully!";
$lang['messages_expense_delete_success'] = "Expense has been deleted successfully!";
/*************************************************************************************************/
//Error
/*************************************************************************************************/
$lang['messages_receipt_add_error'] = "Could not upload receipt!";
$lang['messages_invoice_overpaid_error'] = "Invoice payment amount is greater that due amount";
$lang['messages_avatar_add_error'] = "Could not upload profile picture!";
$lang['messages_project_save_error'] = "Project could not be updated!";
$lang['messages_logo_add_error'] = "Could not upload logo!";
$lang['messages_project_add_error'] = "Project could not be added!";
$lang['messages_item_delete_error'] = "Item could not be  deleted!";
$lang['messages_file_add_error'] = "Could not upload file!";
$lang['messages_paypal_save_error'] = "Could not update paypal settings!";
$lang['messages_payfast_save_error'] = "Could not update payfast settings!";
$lang['messages_skrill_save_error'] = "Could not update skrill settings!";
$lang['messages_stripe_save_error'] = "Could not update stripe settings!";
$lang['messages_braintree_save_error'] = "Could not update braintree settings!";
$lang['messages_payment_add_error'] = "Could add payment!";
/*************************************************************************************************/
//Cancel
/*************************************************************************************************/
$lang['messages_paypal_cancelled'] = "Paypal payments cancelled!";
$lang['messages_payfast_cancelled'] = "Payfast payments cancelled!";
$lang['messages_skrill_cancelled'] = "Skrill payments cancelled!";
$lang['messages_stripe_cancelled'] = "Stripe payments cancelled!";
$lang['messages_braintree_cancelled'] = "Braintree payments cancelled!";
/*************************************************************************************************/
//Warning
/*************************************************************************************************/
$lang['messages_delete_user_warning'] = "This action will delete the selected  user. Proceed?";
$lang['messages_delete_item_warning'] = "This action will delete the selected  item. Proceed?";
$lang['messages_delete_comment_warning'] = "This action will delete the selected  comment. Proceed?";
$lang['messages_delete_invoice_warning'] = "This action will delete the selected  invoice. Proceed?";
$lang['messages_delete_payment_warning'] = "This action will delete the selected  payment. Proceed?";
$lang['messages_delete_task_warning'] = "This action will delete the selected  task data. Proceed?";
$lang['messages_delete_expense_warning'] = "This action will delete the selected  expense data. Proceed?";


