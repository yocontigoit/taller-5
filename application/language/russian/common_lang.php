<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/04
 * Time: 12:06 PM
 */
$lang['common_id'] = 'ID';
$lang['common_name'] = 'Name';
$lang['common_surname'] = 'Surname';
$lang['common_email'] = 'Email';
$lang['common_status'] = 'Status';
$lang['common_edit'] = 'Edit';


$lang['common_dashboard'] = 'Dashboard';


$lang['common_company_id'] = 'Company ID';
$lang['common_company_name'] = 'Company Name';
$lang['common_primary_contact'] = 'Primary Contact';
$lang['common_website'] = 'Website';

$lang['common_client_add'] = 'Add New Client';
$lang['common_project_add'] = 'Add New Project';
$lang['common_user_add'] = 'Add New User';
$lang['common_item_add'] = 'Add New Item';
$lang['common_quotation_add'] = 'Request Quote';
$lang['common_estimate_add'] = 'Add New Estimate';

$lang['common_user_logout'] = 'Log Out';