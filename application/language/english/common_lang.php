<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/12/04
 * Time: 12:06 PM
 */
$lang['common_id'] = 'ID';
$lang['common_name'] = 'Nombre';
$lang['common_surname'] = 'Apellido';
$lang['common_email'] = 'Correo';
$lang['common_status'] = 'Estado';
$lang['common_edit'] = 'Editar';


$lang['common_dashboard'] = 'Escritorio';


$lang['common_company_id'] = 'ID de compañia';
$lang['common_company_name'] = 'Nombre de la Compañia';
$lang['common_primary_contact'] = 'Contacto Primario';
$lang['common_website'] = 'Página Web';

$lang['common_client_add'] = 'Agregar Cliente nuevo';
$lang['common_project_add'] = 'Agregar Proyecto nuevo';
$lang['common_user_add'] = 'Agregar nuevo usario';
$lang['common_item_add'] = 'Agregar nuevo producto';
$lang['common_quotation_add'] = 'Requerir cuota';
$lang['common_estimate_add'] = 'Agregar nuevo estimado';

$lang['common_user_logout'] = 'Cerrar Sesión';