<?php
/**
 * creado by PhpStorm.
 * usuario: appsdyne
 * Date: 2014/12/04
 * Time: 12:09 PM
 */

$lang['messages_login_need_account']= 'Necesitas una cuenta?';
$lang['messages_login_have_account'] = 'ya registrado?';

$lang['messages_no_user_found']     = 'No encontramos ningun usuario.';
$lang['messages_no_client_found']   = 'No encontramos ningun cliente.';
$lang['messages_no_projects_found'] = 'No encontramos ningun proyecto.';


// CuentaCreation
$lang['messages_account_creation_successful'] 	  	        = 'Cuenta creada Satisfactoriamente';
$lang['messages_account_creation_unsuccessful'] 	 	    = 'No se pudo crear la cuenta';
$lang['messages_account_creation_duplicate_email'] 	        = 'Correo ya utlizado o invalido';
$lang['messages_account_creation_duplicate_username']       = 'Usuario ya utilizado o Invalido';

// contraseña
$lang['messages_password_change_successful'] 	 	 = 'Contraseña cambiada satisfactoriamente';
$lang['messages_password_change_unsuccessful'] 	  	 = 'No se pudo cambiar la contraseña';
$lang['messages_forgot_password_successful'] 	 	 = 'Correo de recuperación de contraseña enviado';
$lang['messages_forgot_password_unsuccessful'] 	 	 = 'No se pudo resetear la contraseña';
$lang['messages_error_login']			             =  'Usuario o contraseña incorrecta';

// Activation
$lang['messages_activate_successful'] 		  	     = 'cuenta Activada';
$lang['messages_activate_unsuccessful'] 		 	     = 'No se pudo activar la cuenta';
$lang['messages_deactivate_successful'] 		  	     = 'Cuenta desactivada';
$lang['messages_deactivate_unsuccessful'] 	  	     = 'No se pudo desactivar la cuenta';
$lang['messages_activation_email_successful'] 	  	 = 'Correo de activación enviado';
$lang['messages_activation_email_unsuccessful']   	 = 'no se pudo enviar correo de activación';

// Login / Logout
$lang['messages_login_successful'] 		  	             = 'Sesión iniciada correctamente';
$lang['messages_login_unsuccessful'] 		  	         = 'Sesión incorrecta';
$lang['messages_login_unsuccessful_not_active'] 		 = 'Cuenta inactiva';
$lang['messages_logout_successful'] 		 	         = 'Sesión cerrada correctamente';

// CuentaChanges
$lang['messages_update_successful'] 		 	         = 'CuentaInformation satisfactoriamente Updated';
$lang['messages_update_unsuccessful'] 		 	         = 'no se pudo actualizar CuentaInformation';
$lang['messages_delete_successful'] 		 	         = 'Usuario eliminado';
$lang['messages_delete_unsuccessful'] 		 	         = 'no se pudo eliminar usuario';

// Email Subjects
$lang['messages_email_forgotten_password_subject']        = 'Verificación de contraseña olvidada';
$lang['messages_email_new_password_subject']              = 'nueva contraseña';
$lang['messages_email_activation_subject']                = 'Activación de cuenta';


$lang['messages_ticket_reply_success'] = "Ticket ha sido respondido!";
$lang['messages_mail_reply_success'] = "Mail ha sido respondido!";
$lang['messages_mail_sent_success'] = "nuevo Mail ha sido enviado!";

$lang['messages_invoice_overdue'] = 'Esta Factura esta atrasada! Favor de pagarla ahora!';
$lang['messages_estimate_overdue'] = 'Estimado atrasado!';
/*************************************************************************************************/
//add
/*************************************************************************************************/
$lang['messages_client_add_success'] = "nuevo cliente ha sido agregado!";
$lang['messages_invoice_add_success'] = "La Factura ha sido creado!";
$lang['messages_item_add_success'] = "objeto ha sido agregado!";
$lang['messages_project_add_success'] = "nuevo proyecto ha sido creado!";
$lang['messages_task_add_success'] = "Tarea ha sido agregado!";
$lang['messages_media_add_success'] = "Archivo multimedia ha sido guardado!";
$lang['messages_user_add_success'] = "El usuario ha sido creado!";
$lang['messages_quotation_add_success'] = "Cotización ha sido agregado!";
$lang['messages_ticket_add_success'] = "Ticket ha sido agregado!";
$lang['messages_template_add_success'] = "Template ha sido agregado!";
$lang['messages_client_null_error'] = "Cliente no seleccionado";
$lang['messages_note_add_success'] = "Nota ha sido agregada";
$lang['messages_tax_add_success'] = "Impuesto ha sido agregado";
$lang['messages_avatar_add_success'] = "Foto de perfil agregada!";
$lang['message_department_add_success'] = "El Departamento ha sido agregado!";
$lang['messages_currency_add_success'] = "La moneda ha sido agregada!";
$lang['messages_tax_add_success'] = "Impuesto ha sido agregado!";
$lang['message_transaction_settings_add_success'] = "Ajustes de transacción han sido agregado!";
$lang['messages_issue_add_success'] = "nuevo detalle ha sido agregado!";
$lang['messages_milestone_add_success'] = "nuevo avance ha sido agregado!";
$lang['messages_contact_add_success'] = "El contacto ha sido agregado!";
$lang['messages_invoice_add_success'] = "La Factura ha sido creado!";
/*************************************************************************************************/
//update
/*************************************************************************************************/
$lang['messages_client_save_success'] = "Client ha sido Actualizado!";
$lang['messages_invoice_save_success'] = "La Factura ha sido Actualizado!";
$lang['messages_item_save_success'] = "objeto ha sido Actualizado!";
$lang['messages_project_save_success'] = "proyecto ha sido Actualizado!";
$lang['messages_task_save_success'] = "Tarea ha sido agregado!";
$lang['messages_media_save_success'] = "Media file ha sido guardado!";
$lang['messages_user_save_success'] = "Your changes have been guardado!";
$lang['messages_quotation_save_success'] = "Quotation ha sido updated";
$lang['messages_ticket_save_success'] = "Ticket ha sido guardado!";
$lang['messages_settings_save_success'] = "Changes guardado!";
$lang['messages_note_save_success'] =  "Note  updated satisfactoriamente";
$lang['messages_estimate_item_save_success'] = "Estimate items  updated satisfactoriamente";
$lang['messages_currency_save_success'] = "Currency ha sido  updated satisfactoriamente";
$lang['messages_system_save_success'] = "System ajustes ha sido  updated satisfactoriamente";
$lang['message_general_save_success'] = "General ajustes  ha sido  updated satisfactoriamente";
$lang['message_local_save_success'] = "Local ajustes  ha sido  updated satisfactoriamente";
$lang['message_email_save_success'] = "Email ajustes  ha sido  updated satisfactoriamente";
$lang['messages_template_save_success'] = "Template ajustes  ha sido  updated satisfactoriamente";
$lang['messages_tax_save_success'] = "Tax ajustes  ha sido  updated satisfactoriamente";
$lang['message_transaction_settings_save_success'] = "Transactions ajustes  ha sido  updated satisfactoriamente";
$lang['message_role_save_success'] = "Role permissions updated satisfactoriamente";
$lang['messages_cost_save_success'] = "Costs updated satisfactoriamente";
$lang['messages_issue_save_success'] = "Issue ha sido updated satisfactoriamente";
$lang['messages_contact_save_success'] = "El contactoha sido updated satisfactoriamente!";
$lang['messages_paypal_save_success'] = "Paypal updated satisfactoriamente!";
$lang['messages_payfast_save_success'] = "Payfast updated satisfactoriamente!";
$lang['messages_skrill_save_success'] = "Skrill updated satisfactoriamente!";
$lang['messages_stripe_save_success'] = "Stripe updated satisfactoriamente!";
$lang['messages_milestone_save_success'] = "Milestone updated satisfactoriamente!";
$lang['messages_estimate_save_success'] = "Estimate updated satisfactoriamente!";
/*************************************************************************************************/
//Delete
/*************************************************************************************************/
$lang['messages_client_delete_success'] = "Client ha sido eliminado!";
$lang['messages_invoice_delete_success'] = "La Factura ha sido eliminado!";
$lang['messages_item_delete_success'] = "objeto ha sido eliminado!";
$lang['messages_project_delete_success'] = "proyecto ha sido eliminado!";
$lang['messages_task_delete_success'] = "Tarea ha sido eliminado!";
$lang['messages_media_delete_success'] = "Media ha sido eliminado!";
$lang['messages_user_delete_success'] = "Usuario ha sido eliminado!";
$lang['messages_quotation_delete_success'] = "Cotización ha sido eliminado";
$lang['messages_ticket_delete_success'] = "Ticket ha sido eliminado!";
$lang['messages_template_delete_success'] = "Plantilla ha sido eliminada!";
$lang['messages_contact_delete_success'] = "El contactoha sido eliminado!";
$lang['messages_client_delete_success'] = "Cliente ha sido eliminado!";
$lang['messages_payment_add_success'] = "pago agregado satisfactoriamente!";
$lang['messages_comment_delete_success'] = "El comentario ha sido eliminado satisfactoriamente!";
$lang['messages_invoice_delete_success'] = "La Factura ha sido eliminado satisfactoriamente!";
$lang['messages_payment_delete_success'] = "pago ha sido eliminado satisfactoriamente!";
$lang['messages_expense_delete_success'] = "Expense ha sido eliminado satisfactoriamente!";
/*************************************************************************************************/
//Error
/*************************************************************************************************/
$lang['messages_receipt_add_error'] = "no se pudo subir el recibo!";
$lang['messages_invoice_overpaid_error'] = "El monto de pago de la factura es mayor al monto pendiente";
$lang['messages_avatar_add_error'] = "no se pudo subir Foto de perfil!";
$lang['messages_project_save_error'] = "proyecto no se pudo actualizar!";
$lang['messages_logo_add_error'] = "no se pudo subir logo!";
$lang['messages_project_add_error'] = "proyecto no se pudo agregar!";
$lang['messages_item_delete_error'] = "objeto no se pudo eliminar!";
$lang['messages_file_add_error'] = "no se pudo subir archivo!";
$lang['messages_paypal_save_error'] = "no se pudo actualizar paypal ajustes!";
$lang['messages_payfast_save_error'] = "no se pudo actualizar payfast ajustes!";
$lang['messages_skrill_save_error'] = "no se pudo actualizar skrill ajustes!";
$lang['messages_stripe_save_error'] = "no se pudo actualizar stripe ajustes!";
$lang['messages_braintree_save_error'] = "no se pudo actualizar braintree ajustes!";
$lang['messages_payment_add_error'] = "pudo agregar pago!";
/*************************************************************************************************/
//Cancel
/*************************************************************************************************/
$lang['messages_paypal_cancelled'] = "Paypal payments cancelados!";
$lang['messages_payfast_cancelled'] = "Payfast payments cancelados!";
$lang['messages_skrill_cancelled'] = "Skrill payments cancelados!";
$lang['messages_stripe_cancelled'] = "Stripe payments cancelados!";
$lang['messages_braintree_cancelled'] = "Braintree payments cancelados!";
/*************************************************************************************************/
//Warning
/*************************************************************************************************/
$lang['messages_delete_user_warning'] = "esta accion va a eliminar el  usuario seleccionado. continuar?";
$lang['messages_delete_item_warning'] = "esta accion va a eliminar el  item seleccionado. continuar?";
$lang['messages_delete_comment_warning'] = "esta accion va a eliminar el  comment seleccionado. continuar?";
$lang['messages_delete_invoice_warning'] = "esta accion va a eliminar el  invoice seleccionado. continuar?";
$lang['messages_delete_payment_warning'] = "esta accion va a eliminar el  payment seleccionado. continuar?";
$lang['messages_delete_task_warning'] = "esta accion va a eliminar el  Tarea data seleccionado. continuar?";
$lang['messages_delete_expense_warning'] = "esta accion va a eliminar el  expense data seleccionado. continuar?";
