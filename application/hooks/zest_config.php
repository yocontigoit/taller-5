<?php
/**
 * Created by PhpStorm.
 * User: appsdyne
 * Date: 2014/11/02
 * Time: 09:31 AM
 */

function Zest_Config()
{
    $CI =& get_instance();

    $company_id = ($CI->Company_id > 0)? $CI->Company_id : 1;
    $CI->config->set_item('company',$CI->zest_config_m->company_setting($company_id));
    $CI->config->set_item('template',$CI->zest_config_m->template_setting($company_id));
    $CI->config->set_item('local',$CI->zest_config_m->local_setting($company_id));
    $CI->config->set_item('system',$CI->zest_config_m->system_setting($company_id));
    $CI->config->set_item('company_email',$CI->zest_config_m->email_setting($company_id));
    $CI->config->set_item('invoice',$CI->zest_config_m->invoice_setting($company_id));
    $CI->config->set_item('terms',$CI->zest_config_m->terms_setting($company_id));
    $CI->config->set_item('theme',$CI->zest_config_m->theme_setting($company_id));
    $CI->config->set_item('payment',$CI->zest_config_m->payment_setting($company_id));
    $CI->config->set_item('paypal',$CI->zest_config_m->paypal_setting($company_id));
    $CI->config->set_item('payfast',$CI->zest_config_m->payfast_setting($company_id));
    $CI->config->set_item('skrill',$CI->zest_config_m->skrill_setting($company_id));
    $CI->config->set_item('stripe',$CI->zest_config_m->stripe_setting($company_id));
    $CI->config->set_item('checkout',$CI->zest_config_m->checkout_setting($company_id));
    $CI->config->set_item('braintree',$CI->zest_config_m->braintree_setting($company_id));
    $CI->config->set_item('bitcoin',$CI->zest_config_m->bitcoin_setting($company_id));
    $CI->config->set_item('support',$CI->zest_config_m->support_setting($company_id));
    $CI->config->set_item('bank',$CI->zest_config_m->bank_setting($company_id));
    $CI->config->set_item('systypes',$CI->zest_config_m->company_systypes($company_id));


    if (get_time_zone($CI->config->item('local')->company_time_zone))
    {
        date_default_timezone_set(get_time_zone($CI->config->item('local')->company_time_zone));
    }
    else
    {
        if (!ini_get('date.timezone')) {
            date_default_timezone_set('UTC');
        }else{
            date_default_timezone_set('America/New_York');
        }

    }
    
}